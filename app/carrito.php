<?php
session_start();

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php
$titulo = "Mi carrito";
?>
    <title><?php echo $titulo; ?></title>
    <meta name="description" content="Almacena los productos que desees comprar en un futuro en tu carrito de compras, así podrás comprar más de 1 en un solo pedido.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, carrito de compras, productos, comprar">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-comprar-fondo">
          <h1 class="p-titulo"><?php echo $titulo;?></h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero p-carrito-section">
          <div id="id-carrito-contenedor_info">
            <div class="p-acordeon-contenedor">
              <div id="id-carrito-contenedor_elementos">
                <div class="p-loading-general p-busqueda-loading_padding" style="display: block;">
                  <div></div>
                  <p>Espere por favor...</p>
                </div>
              </div>
            </div>
            <div id="id-carrito-contenedor_button_resumen"></div>
            <div class="p-modal-derecho_contenedor p-modal-derecho_contenedor_carrito" id="id-carrito-resumen_contenedor">
              <div class="p-modal-derecho_contenedor_fondo g-carrito-resumen_cerrar"></div>
              <div class="p-modal-derecho_contenedor_contenido" id="id-carrito-contenedor_resumen_1"></div>
            </div>
          </div>
        </div>
        
        <div class="p-carrito-div_fijo_contenedor" id="id-carrito-contenedor_resumen_2">
          <div class="p-loading-general p-busqueda-loading_padding" style="display: block;">
            <div></div>
            <p>Espere por favor...</p>
          </div>
        </div>
      </div>
    </section>
    
    <div class="p-notification_contenedor" id="id-carrito_status_error">
      <div class="p-notification p-notification_error p-notification_fixed_disabled" id="id-carrito_status_error_div">
        <span>
          <i class="fas fa-times-circle"></i>
        </span>
        <p class="p-notification_p">
          <span>Producto eliminado</span>
        </p>
      </div>
    </div>

<?php
  $footer_carrito = true;
  $botonArriba_carrito = true;
?>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

    <script src="scripts/carrito/scripts_carrito.js?v=3.2"></script>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>