<?php
session_start();
include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>¿Quiénes somos?</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 7;
$op_menu = 1;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-quienesSomos-fondo">
          <h1 class="p-titulo">¿Quiénes somos?</h1>
          <h2 class="p-subtitulo p-text_p"><b>MXcomp</b> es una división de negocio de <b>MEXIcomp</b> que ofrece competitivamente soluciones en tecnología por medio de la red.</h2>
        </div>

        <div class="p-section-div_formularios p-contenido-hero p-light_fondo">
          <div class="p-columnas">
            <div class="p-columna p-tres_columnas">
              <figure class="p-quienesSomos-figure">
                <img src="images/nuestro_objetivo.png">
              </figure>
            </div>
            <div class="p-columna">
              <h3>Nuestro objetivo</h3>
              <p class="p-text_p">Específicamente en el ámbito tecnológico, el objetivo general de <b>MXcomp</b> es el de facilitar tu vida ofreciéndote una amplia gama de productos a través de un sitio web sumamente intuitivo y amigable en donde además podrás revisar y comparar las características técnicas de todos los productos de nuestro catálogo, consultar precios y promociones, recibir asesoría de expertos, tener atención personalizada en todo el proceso de compra, desde la búsqueda hasta la entrega de tus productos en la puerta de tu domicilio, y sobre todo, comprar de manera segura y confiable.</p>
            </div>
          </div>
        </div>

        <div class="p-section-div_formularios p-contenido-hero p-dark_fondo">
          <h3>Ventaja competitiva</h3>
          <p class="p-text_p">A diferencia de los ofertantes actuales, <b>MXcomp</b> te ofrece como principal diferenciador o ventaja competitiva la asistencia técnica especializada en los productos publicados en el sitio web, con lo que tendrás la certeza de estar adquiriendo el artículo ideal para tus necesidades. Esto debido a la complejidad que implica tomar una decisión de compra en el competido mundo de la tecnología. Nos aseguramos de que nuestros clientes obtengan el mayor valor por su inversión.</p>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-columnas">
            <div class="p-columna p-dos_columnas">
              <article class="p-media-contenedor">
                <figure class="p-media-div_left">
                  <span>
                    <i class="fas fa-lg fa-fw fa-flag p-quienesSomos-icon_mision"></i>
                  </span>
                </figure>
                <div class="p-media-div_info">
                  <h4>Misión</h4>
                  <p>
                    <span>Ser el punto de comercio electrónico que ofrezca cómoda e intuitivamente una amplia gama de soluciones en tecnología con un servicio integral eficiente y confiable.</span>
                  </p>
                </div>
              </article>
            </div>
            <div class="p-columna">
              <article class="p-media-contenedor">
                <figure class="p-media-div_left">
                  <span>
                    <i class="fas fa-lg fa-fw fa-eye p-quienesSomos-icon_vision"></i>
                  </span>
                </figure>
                <div class="p-media-div_info">
                  <h4>Visión</h4>
                  <p>
                    <span>Llegar a ser el sitio web por excelencia preferido por los consumidores que buscan soluciones en tecnología.</span>
                  </p>
                </div>
              </article>
            </div>
          </div>
          <div class="p-columnas">
            <div class="p-columna p-dos_columnas">
              <article class="p-media-contenedor">
                <figure class="p-media-div_left">
                  <span>
                    <i class="fas fa-lg fa-fw fa-handshake p-quienesSomos-icon_valores"></i>
                  </span>
                </figure>
                <div class="p-media-div_info">
                  <h4>Valores</h4>
                  <p>
                    <span>Lealtad. En <b>MXcomp</b> tenemos la convicción de que cualquier valor está relegado a la lealtad, si una persona es leal, naturalmente será responsable, honesta, respetuosa, puntual, etc.</span>
                  </p>
                </div>
              </article>
            </div>
            <div class="p-columna">
              <article class="p-media-contenedor">
                <figure class="p-media-div_left">
                  <span>
                    <i class="fas fa-lg fa-fw fa-lightbulb p-quienesSomos-icon_filosofia"></i>
                  </span>
                </figure>
                <div class="p-media-div_info">
                  <h4>Filosofía</h4>
                  <p>
                    <span>Servir con profundo sentido humano, buscando en todo momento el bien personal y colectivo para convertir la iniciativa individual en una fuerza de equipo vital.</span>
                  </p>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>