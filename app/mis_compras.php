<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.location = "/"; </script>';
}

require_once 'funciones/fecha_hora_formatos.php';
require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Mis compras</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-cuenta-fondo">
          <h1 class="p-titulo">Mis compras</h1>
          <h2 class="p-subtitulo p-text_p">Selecciona una compra para ver sus detalles.</h2>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
$idUsuario = desencriptar(trim($_SESSION['__id__']));
$codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

if(validar_campo_numerico($idUsuario) && validar_codigoUsuario($codigoUsuario)){
  try{
    $sql = "SELECT COUNT(id) FROM __ordenes_compra WHERE idCliente = :idCliente AND codigoCliente = :codigoCliente";
    $stmt = $conexion_admin->prepare($sql);
    $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
    $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_INT);
    $stmt->execute();
    $pedidos_existen = (int) $stmt->fetchColumn();
    
    if($pedidos_existen > 0){
      $sql = "SELECT id, numeroEstadoCompra, ordenCompra, productosComprados, totalPagado, monedaPedido, numeroEstadoPago, fechaCompra FROM __ordenes_compra WHERE idCliente = :idCliente AND codigoCliente = :codigoCliente ORDER BY id DESC";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_INT);
      $stmt->execute();
      
      while($datos_pedido = $stmt->fetch(PDO::FETCH_ASSOC)){
        $fechaCompra = (string) $datos_pedido['fechaCompra'];
?>
          <div class="p-box-productos_contenedor_elementos p-cuenta-divLargo_contenedor_elementos p-cuenta-divLargo_contenedor_elementos_hover">
            <a class="p-cuenta-divLargo_contenedor_div" href="compra-detalles/<?php echo $datos_pedido['id']; ?>/<?php echo $datos_pedido['ordenCompra']; ?>" title="Ver detalles de la compra">
              <div class="p-box-productos_title">
                <h3 class="p-box-productos_title_h3 p-cuenta-divLargo_titulo">Compra #<?php echo $datos_pedido['ordenCompra']; ?></h3>
              </div>
              <div class="p-box-productos_info p-cuenta-divLargo_box_info">
                <div class="p-box-productos_info_div_1 p-cuenta-divLargo_box_info_div">
                  <p class="p-text_p"><b>Se compró el</b> <?php echo fecha_con_hora($fechaCompra); ?></p>
<?php
        $textoUnidades = (int) trim($datos_pedido['productosComprados']) === 1 ? 'unidad' : 'unidades';
        $numeroEstadoPago = (int) $datos_pedido['numeroEstadoPago'];
        
        switch($numeroEstadoPago){
          case 1: // PENDIENTE
?>
                  <p class="p-text_p"><b>A pagar</b> <span class="p-text_p p-text p-text_help">$ <?php echo number_format(round($datos_pedido['totalPagado'], 2), 2, '.', ',') . ' ' . $datos_pedido['monedaPedido']; ?></span> <b>por <?php echo $datos_pedido['productosComprados'] . ' ' . $textoUnidades; ?></b></p>
<?php
            break;
            
          case 2: // CANCELADO
?>
                  <p class="p-text_p"><b>Se canceló el pago de</b> <span class="p-text_p p-text p-text_help">$ <?php echo number_format(round($datos_pedido['totalPagado'], 2), 2, '.', ',') . ' ' . $datos_pedido['monedaPedido']; ?></span> <b>por <?php echo $datos_pedido['productosComprados'] . ' ' . $textoUnidades; ?></b></p>
<?php
            break;
            
          case 3: // ACREDITADO
?>
                  <p class="p-text_p"><b>Pagaste</b> <span class="p-text_p p-text p-text_help">$ <?php echo number_format(round($datos_pedido['totalPagado'], 2), 2, '.', ',') . ' ' . $datos_pedido['monedaPedido']; ?></span> <b>por <?php echo $datos_pedido['productosComprados'] . ' ' . $textoUnidades; ?></b></p>
<?php
            break;
        }
            
        $numeroEstadoCompra = (int) $datos_pedido['numeroEstadoCompra'];
        
        switch($numeroEstadoCompra){
          case 1: // PENDIENTE
?>
                  <div class="p-notification p-notification_letter_info">
                    <span>
                      <i class="fas fa-spinner"></i>
                    </span>
                    <span class="p-notification_p">
                      <b>En proceso</b>
                    </span>
                  </div>
<?php
            break;
          case 2: // CANCELADA
?>
                  <div class="p-notification p-notification_letter_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span class="p-notification_p">
                      <b>Cancelada</b>
                    </span>
                  </div>
<?php
            break;
          case 3: // FINALIZADA
?>
                  <div class="p-notification p-notification_letter_success">
                    <span>
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span class="p-notification_p">
                      <b>Finalizada</b>
                    </span>
                  </div>
<?php
            break;
        }
?>
                </div>
              </div>
            </a>
          </div>
<?php
      }
    }else{
?>
          <p class="p-text_p">
            <b>No tienes compras por el momento.</b>
          </p>
          
          <div class="p-buttons p-cuenta-contenedor_botones">
            <button class="p-button p-button_delete" onclick="history.go(-1)">
              <span>
                <i class="fas fa-undo"></i>
              </span>
              <span><b>Regresar</b></span>
            </button>
          </div>
<?php
    }

    $stmt = null;
  }catch(PDOException $error){
    //$msg_error = "Error: " . $error->getMessage();
    $msg_error = "Hubo un problema al buscar la información de tus compras.";
?>
          <p class="p-notification p-notification_letter_error">
            <span><?php echo $msg_error; ?></span>
          </p>
<?php
  }
}else{
?>
          <p class="p-notification p-notification_letter_error">
            <span>Se detectó que los datos recibidos no son correctos para mostrar la información de tus compras.</span>
          </p>
<?php
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>