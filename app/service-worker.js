
// Asignar un nombre y versión al cache
const CACHE_NAME = 'v2_cache_mxcomp',
      urlsToCache = [
        'https://fonts.googleapis.com/css?family=Montserrat|Ubuntu:700&display=swap',
        'https://use.fontawesome.com/releases/v5.11.2/js/all.js',
        './styles/main.min.css',
        './scripts/main.js',
        './images/touch/chrome-touch-icon-192x192.png',
        './images/touch/apple-touch-icon-57x57.png',
        './images/touch/apple-touch-icon-60x60.png',
        './images/touch/apple-touch-icon-72x72.png',
        './images/touch/apple-touch-icon-76x76.png',
        './images/touch/apple-touch-icon-114x114.png',
        './images/touch/apple-touch-icon-120x120.png',
        './images/touch/apple-touch-icon-144x144.png',
        './images/touch/apple-touch-icon-152x152.png',
        './images/touch/apple-touch-icon-180x180.png',
        './images/touch/ms-touch-icon-144x144-precomposed.png',
        './images/touch/icon-16x16.png',
        './images/touch/icon-32x32.png',
        './images/touch/icon-96x96.png'
      ]

// Durante la fase de instalación, generalmente se almacena en caché los activos estáticos
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(CACHE_NAME)
    .then(cache => {
      return cache.addAll(urlsToCache)
      .then(() => self.skipWaiting())
    })
    .catch(err => console.log('Falló registro de cache', err))
  )
})

// Una vex que se instala el SW, se activa y busca los recursos para hacer que funcione sin conexión
self.addEventListener('activate', e => {
  const cacheWhitelist = [CACHE_NAME]
  
  e.waitUntil(
    caches.keys()
    .then(cachesNames => {
      cachesNames.map(cacheName => {
        // Eliminamos lo que ya no se necesita en cache
        if(cacheWhitelist.indexOf(cacheName) === -1){
          return caches.delete(cacheName)
        }
      })
    })
    
    // Le indica al SW activar el cache actual
    .then(() => self.clients.claim())
  )
})

// Cuando el navegador recupera una URL
self.addEventListener('fetch', e => {
  // Responde ya sea con el objeto en caché o continuar y buscar la URL real
  e.respondWith(
    caches.match(e.request)
    .then(res => {
      if(res){
        // Recuperar del cache
        return res
      }
      
      // Recuperar de la petición a la URL
      return fetch(e.request)
    })
  )
})
