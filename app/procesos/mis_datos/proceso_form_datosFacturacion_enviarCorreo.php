<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'enviar'){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $ordenCompra = desencriptar(trim($_POST['orden_compra']));
  $seccion = trim($_POST['seccion']);
  $nombreRazonSocial = trim($_POST['nombre_razon_social']);
  $RFC = trim($_POST['RFC']);
  $id_usoFactura = trim($_POST['uso_factura']);
  $usoFactura = "";
  $formaPago = trim($_POST['forma_pago']);
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $tipoVialidad = "";
  $nombreVialidad = trim($_POST['nombre_vialidad']);
  $noExterior = trim($_POST['no_exterior']);
  $noExteriorEstadoCheck = trim($_POST['no_exterior_estado_check']);
  $noInterior = trim($_POST['no_interior']);
  $noInteriorEstadoCheck = trim($_POST['no_interior_estado_check']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $idEstado = trim($_POST['estado']);
  $nombreEstado = "";
  $correoFactura = trim($_POST['correo_factura']);
  $noTelefonico = trim($_POST['no_telefonico']);

  $array_formaPago = ['Efectivo', 'Transferencia electrónica de fondos', 'Tarjeta de crédito', 'Tarjeta de débito'];

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $Conn_MXcomp = new Conexion_mxcomp();
  $Conn_Admin = new Conexion_admin();
  $proceso_correcto = false;
  $mensaje = '';

  // REVISA EL ID DE CLIENTE
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;

    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        $proceso_correcto = true;
      }else{
        $respuesta = '0';
        $mensaje = 'El usuario no existe';
        $proceso_correcto = false;
      }

      $stmt = null;
    }catch(PDOException $error){
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Problema al buscar el usuario';
      $proceso_correcto = false;
    }
  }else{
    $respuesta = '0';
    $mensaje = 'No es numérico';
    $proceso_correcto = false;
  }

  // REVISA LA ORDEN DE COMPRA
  if($proceso_correcto){
    if($ordenCompra !== "" && validar_campo_numerico($ordenCompra)){
      $ordenCompra = (int) $ordenCompra;
      $proceso_correcto = true;
    }else{
      $respuesta = '0';
      $mensaje = 'Error: Problema con la orden de compra.';
      $proceso_correcto = false;
    }
  }

  // REVISA LA SECCION
  if($proceso_correcto){
    if($seccion === 'agregar' || $seccion === 'editar'){
      $seccion = (string) $seccion;
      $proceso_correcto = true;
    }else{
      $respuesta = '0';
      $mensaje = 'Error: Problema con la seccion de la facturacion.';
      $proceso_correcto = false;
    }
  }

  // REVISA EL NOMBRE O RAZON SOCIAL
  if($proceso_correcto){
    if($nombreRazonSocial !== "" && validar_campo_letras_espacios($nombreRazonSocial)){
      $nombreRazonSocial = (string) $nombreRazonSocial;
      $proceso_correcto = true;
    }else if($nombreRazonSocial === ""){
      $respuesta = '1'; // El campo "Nombre o razón social" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '2'; // El Nombre o razón social no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL RFC
  if($proceso_correcto){
    if($RFC !== "" && validar_rfc($RFC)){
      if(mb_strlen($RFC) === 13){
        $RFC = (string) $RFC;
        $proceso_correcto = true;
      }else{
        $respuesta = '5'; // El RFC no contiene 13 caracteres
        $proceso_correcto = false;
      }
    }else if($RFC === ""){
      $respuesta = '3'; // El campo "RFC" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = '4'; // El RFC no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL USO DE LA FACTURA
  if($proceso_correcto){
    if($id_usoFactura !== "" && validar_campo_numerico($id_usoFactura)){
      $id_usoFactura = (int) $id_usoFactura;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, clave, nombreUsoFactura FROM __uso_factura WHERE id = :id";
        $stmt = $Conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_usoFactura, PDO::PARAM_INT);
        $stmt->execute();
        $datos_usoFactura = $stmt->fetch(PDO::FETCH_ASSOC);
        $usoFactura_existe = (int) $datos_usoFactura['conteo'];

        if($usoFactura_existe === 1){
          $clave = (string) $datos_usoFactura['clave'];
          $usoFactura = (string) $datos_usoFactura['nombreUsoFactura'];
          $usoFactura = $clave . ' ' . $usoFactura;
          $proceso_correcto = true;
        }else{
          $respuesta = '7'; // Esta opcion no existe
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = '0';
        //$mensaje = 'Error: ' . $error->getMessage();
        $mensaje = 'Problema al buscar los usos de factura.';
        $proceso_correcto = false;
      }
    }else if($id_usoFactura === ""){
      $respuesta = '6'; // No se ha seleccionado una opcion
      $proceso_correcto = false;
    }else{
      $respuesta = '7'; // Esta opcion no existe
      $proceso_correcto = false;
    }
  }

  // REVISA LA FORMA DE PAGO
  if($proceso_correcto){
    if($formaPago !== "" && in_array($formaPago, $array_formaPago)){
      $formaPago = (string) $formaPago;
      $proceso_correcto = true;
    }else if($formaPago === ""){
      $respuesta = '8'; // No se ha seleccionado una opción
      $proceso_correcto = false;
    }else{
      $respuesta = '9'; // Esta opcion no existe
      $proceso_correcto = false;
    }
  }

  // REVISA EL TIPO DE VIALIDAD
  if($proceso_correcto){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreTipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $Conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];

        if($tipoVialidad_existe === 1){
          $tipoVialidad = (string) $datos_tipoVialidad['nombreTipoVialidad'];
          $proceso_correcto = true;
        }else{
          $respuesta = '11'; // Este tipo de vialidad no existe
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = '0';
        //$mensaje = 'Error: ' . $error->getMessage();
        $mensaje = 'Problema al buscar los tipos de vialidad.';
        $proceso_correcto = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = '10'; // No se ha seleccionado un tipo de vialidad
      $proceso_correcto = false;
    }else{
      $respuesta = '11'; // Este tipo de vialidad no existe
      $proceso_correcto = false;
    }
  }

  // REVISA EL NOMBRE DE LA VIALIDAD
  if($proceso_correcto){
    if($nombreVialidad !== "" && validar_campo_letras_espacios_simbolos($nombreVialidad)){
      $nombreVialidad = (string) $nombreVialidad;
      $proceso_correcto = true;
    }else if($nombreVialidad === ""){
      $respuesta = '12'; // El campo "Nombre de vialidad" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '13'; // El Nombre de vialidad no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL NUMERO EXTERIOR
  if($proceso_correcto){
    if(($noExterior !== "" && $noExteriorEstadoCheck === 'false' && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === 'true')){
      $noExterior = (string) $noExterior;
      $proceso_correcto = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === 'false'){
      $respuesta = '14'; // El campo "No. exterior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '15'; // El No. exterior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_correcto = false;
    }
  }

  // REVISA EL NUMERO INTERIOR
  if($proceso_correcto){
    if(($noInterior !== "" && $noInteriorEstadoCheck === 'false' && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === 'true')){
      $noInterior = (string) $noInterior;
      $proceso_correcto = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === 'false'){
      $respuesta = '16'; // El campo "No. interior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '17'; // El No. interior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_correcto = false;
    }
  }

  // REVISA EL CODIGO POSTAL
  if($proceso_correcto){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(mb_strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_correcto = true;
      }else{
        $respuesta = '20'; // El Código Postal no tiene 5 numeros
        $proceso_correcto = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = '18'; // El campo "Código Postal" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '19'; // El Código Postal no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LA COLONIA
  if($proceso_correcto){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_correcto = true;
    }else if($colonia === ""){
      $respuesta = '21'; // El campo "Colonia" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '22'; // El Colonia no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_correcto){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_correcto = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = '23'; // El campo "Ciudad o municipio" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '24'; // El Ciudad o municipio no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL ESTADO
  if($proceso_correcto){
    if($idEstado !== "" && validar_campo_numerico($idEstado)){
      $idEstado = (int) $idEstado;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $Conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $idEstado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];

        if($estado_existe === 1){
          $nombreEstado = (string) $datos_estado['nombreEstado'];
          $proceso_correcto = true;
        }else{
          $respuesta = '26'; // El Estado no existe
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = '0';
        //$mensaje = 'Error: ' . $error->getMessage();
        $mensaje = 'Problema al buscar el Estado.';
        $proceso_correcto = false;
      }
    }else if($idEstado === ""){
      $respuesta = '25'; // No se ha seleccionado un Estado
      $proceso_correcto = false;
    }else{
      $respuesta = '26'; // El Estado no existe
      $proceso_correcto = false;
    }
  }

  // REVISA EL CORREO PARA EL ENVIO DE LA FACTURA
  if($proceso_correcto){
    if($correoFactura !== "" && validar_correo($correoFactura)){
      $correoFactura = (string) $correoFactura;
      $proceso_correcto = true;
    }else if($correoFactura === ""){
      $respuesta = '27'; // El campo "Correo electronico para envio de factura" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '28'; // El Correo electronico no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL NÚMERO TELEFÓNICO
  if($proceso_correcto){
    if($noTelefonico !== "" && validar_campo_numerico($noTelefonico)){
      $noTelefonico = (string) $noTelefonico;

      if(mb_strlen($noTelefonico) === 10){
        $proceso_correcto = true;
      }else{
        $respuesta = '31'; // El No. telefónico no tiene 10 dígitos
        $proceso_correcto = false;
      }
    }else if($noTelefonico === ""){
      $respuesta = '29'; // El campo "No. telefónico" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = '30'; // El No. telefónico no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REALIZA TODO EL PROCESO PARA INSERTAR LOS DATOS DE FACTURACION EN LA TABLA DE DIRECCIONES
  if($proceso_correcto){
    $noExterior = $noExterior === '' ? 'S/N' : $noExterior;
    $noInterior = $noInterior === '' ? 'S/N' : $noInterior;

    $fechaProceso = date("Y-m-d H:i:s");

    try{
      $Conn_MXcomp->pdo->beginTransaction();

      // ENCRIPTAMOS EL CODIGO DE USUARIO
      $codigoUsuario_encriptado = encriptar($codigoUsuario);

      $direccionCliente = $tipoVialidad . ' ' . $nombreVialidad . ' No. Ext. ' . $noExterior . ', No. Int. ' . $noInterior . ', C.P. ' . $codigoPostal . ', ' . $colonia . ', ' . $ciudadMunicipio . ', ' . $nombreEstado;

      // ENCRIPTAMOS LOS CAMPOS
      $nomRazonSocial_encriptado = encriptar_con_clave($nombreRazonSocial, $codigoUsuario_encriptado);
      $RFC_encriptado = encriptar_con_clave($RFC, $codigoUsuario_encriptado);
      $usoFactura_encriptado = encriptar_con_clave($usoFactura, $codigoUsuario_encriptado);
      $nombreVialidad = encriptar_con_clave($nombreVialidad, $codigoUsuario_encriptado);
      $noExterior = encriptar_con_clave($noExterior, $codigoUsuario_encriptado);
      $noInterior = encriptar_con_clave($noInterior, $codigoUsuario_encriptado);
      $codigoPostal = encriptar_con_clave($codigoPostal, $codigoUsuario_encriptado);
      $colonia = encriptar_con_clave($colonia, $codigoUsuario_encriptado);
      $ciudadMunicipio = encriptar_con_clave($ciudadMunicipio, $codigoUsuario_encriptado);
      $nombreEstado = encriptar_con_clave($nombreEstado, $codigoUsuario_encriptado);
      $correoFactura_encriptado = encriptar_con_clave($correoFactura, $codigoUsuario_encriptado);
      $noTelefonico_encriptado = encriptar_con_clave($noTelefonico, $codigoUsuario_encriptado);

      $nombreCliente = desencriptar_con_clave($_SESSION['__nombre_usu__'], $codigoUsuario_encriptado);
      $tipoDireccion = 'facturacion';

      switch($seccion){
        case 'agregar':
          $sql = "INSERT INTO __direcciones(idUsuario, codigoUsuario, tipoDireccion, nombreRazonSocial, RFC, idUsoFactura, usoFactura, idTipoVialidad, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, idEstado, nombreEstado, correoFactura, noTelefonico, fechaCreacion) VALUES(:idUsuario, :codigoUsuario, :tipoDireccion, :nombreRazonSocial, :RFC, :idUsoFactura, :usoFactura, :idTipoVialidad, :tipoVialidad, :nombreVialidad, :noExterior, :noInterior, :codigoPostal, :colonia, :ciudadMunicipio, :idEstado, :nombreEstado, :correoFactura, :noTelefonico, :fechaCreacion)";
          $stmt = $Conn_MXcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
          $stmt->bindParam(':nombreRazonSocial', $nomRazonSocial_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':RFC', $RFC_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':idUsoFactura', $id_usoFactura, PDO::PARAM_INT);
          $stmt->bindParam(':usoFactura', $usoFactura_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
          $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
          $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
          $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
          $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
          $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
          $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
          $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
          $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
          $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
          $stmt->bindParam(':correoFactura', $correoFactura_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':noTelefonico', $noTelefonico_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':fechaCreacion', $fechaProceso, PDO::PARAM_STR);
          $stmt->execute();
          break;

        case 'editar':
          $sql = "UPDATE __direcciones SET nombreRazonSocial = :nombreRazonSocial, RFC = :RFC, idUsoFactura = :idUsoFactura, usoFactura = :usoFactura, idTipoVialidad = :idTipoVialidad, tipoVialidad = :tipoVialidad, nombreVialidad = :nombreVialidad, noExterior = :noExterior, noInterior = :noInterior, codigoPostal = :codigoPostal, colonia = :colonia, ciudadMunicipio = :ciudadMunicipio, idEstado = :idEstado, nombreEstado = :nombreEstado, correoFactura = :correoFactura, noTelefonico = :noTelefonico, fechaActualizacion = :fechaActualizacion WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = :tipoDireccion";
          $stmt = $Conn_MXcomp->pdo->prepare($sql);
          $stmt->bindParam(':nombreRazonSocial', $nomRazonSocial_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':RFC', $RFC_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':idUsoFactura', $id_usoFactura, PDO::PARAM_INT);
          $stmt->bindParam(':usoFactura', $usoFactura_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
          $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
          $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
          $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
          $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
          $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
          $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
          $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
          $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
          $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
          $stmt->bindParam(':correoFactura', $correoFactura_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':noTelefonico', $noTelefonico_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':fechaActualizacion', $fechaProceso, PDO::PARAM_STR);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
          $stmt->execute();
          break;
      }

      $proceso_correcto = true;

      $Conn_MXcomp->pdo->commit();
    }catch(PDOException $error){
      $Conn_MXcomp->pdo->rollBack();
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Problema al registrar los datos de facturacion.';
      $proceso_correcto = false;
    }
  }

  // ENVIA EL CORREO CON LOS DATOS DE FACTURACION
  if($proceso_correcto){
    try{
      $Conn_Admin->pdo->beginTransaction();

      ///////////////////////////////////////////////////////////////////////////////////////
      // OBTENER LOS DATOS PARA CORREO DE FACTURACION

      $sql = "SELECT totalPagado, costoEnvio FROM __ordenes_compra WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_ordenCompra = $stmt->fetch(PDO::FETCH_ASSOC);

      $totalPagado = (float) $datos_ordenCompra['totalPagado'];
      $costoEnvio = (float) $datos_ordenCompra['costoEnvio'] === 'Envío gratis' ? 0.00 : $datos_ordenCompra['costoEnvio'];

      $sql = "SELECT codigoProducto, descripcion, precioUnitarioMXcomp, cantidadComprada FROM __orden_compra_productos WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_productos = $stmt->fetchAll();

      // SE GENERA UN ARRAY CON TODOS LOS CÓDIGOS DE PRODUCTOS SIN REPETIRSE
      $array_codigos_producto = [];
      $array_codigos_producto = array_unique(array_column($datos_productos, 'codigoProducto'));

      $total_cantidad = 0.00;
      $total_valorUnitario = 0.00;
      $total_importe = 0.00;
      $total_IVA = 0.00;
      $gran_total = 0.00;

      $productos_factura = [];
      $array_costoEnvio = [];
      $array_granTotal = [];

      foreach($array_codigos_producto as $codigo_producto){
        $codigoProducto = "";
        $descripcion = "";
        $precioUnitario_conIVA = 0.00;
        $precioTotal = 0.00;
        $cantidad = 0;

        foreach($datos_productos as $datos_producto){
          if($codigo_producto === $datos_producto['codigoProducto']){
            $codigoProducto = (string) $datos_producto['codigoProducto'];
            $descripcion = (string) $datos_producto['descripcion'];
            $precioUnitario_conIVA = (float) $datos_producto['precioUnitarioMXcomp'];
            $cantidadComprada = (int) $datos_producto['cantidadComprada'];

            $cantidad += $cantidadComprada;
          }
        }

        $precioTotal = (float) round($precioUnitario_conIVA * $cantidad, 2);

        $valor_unitario = (float) round($precioUnitario_conIVA / 1.16, 2);
        $importe = (float) round($valor_unitario * $cantidad, 2);
        $IVA = (float) round($precioTotal - $importe, 2);
        $IVA = (float) round($IVA, 2);

        $total_cantidad += $cantidad;
        $total_valorUnitario += $valor_unitario;
        $total_importe += $importe;
        $total_IVA += $IVA;
        $gran_total += $precioTotal;

        $productos_factura[count($productos_factura)] = [
          'codigoProducto' => (string) $codigoProducto,
          'descripcion' => (string) $descripcion,
          'cantidad' => (int) $cantidad,
          'valor_unitario' => (float) round($valor_unitario, 2),
          'importe' => (float) round($importe, 2),
          'IVA' => (float) round($IVA, 2),
          'total' => (float) round($precioTotal, 2)
        ];
      }

      $envio_costoTotal = (float) $costoEnvio;
      $envio_costoUnitario = (float) round($envio_costoTotal / 1.16, 2);
      $envio_importe = (float) $envio_costoUnitario * 1;
      $envio_IVA = (float) $envio_costoTotal - $envio_importe;
      $envio_IVA = (float) round($envio_IVA, 2);

      $total_cantidad += 1;
      $total_valorUnitario += $envio_costoUnitario;
      $total_importe += $envio_importe;
      $total_IVA += $envio_IVA;
      $gran_total += $envio_costoTotal;

      $array_costoEnvio = [
        'codigoProducto' => (string) '-',
        'descripcion' => (string) 'Costo total del envío',
        'cantidad' => (int) 1,
        'valor_unitario' => (float) round($envio_costoUnitario, 2),
        'importe' => (float) round($envio_importe, 2),
        'IVA' => (float) round($envio_IVA, 2),
        'total' => (float) round($envio_costoTotal, 2)
      ];

      $array_granTotal = [
        'total_cantidad' => (int) $total_cantidad,
        'total_valorUnitario' => (float) round($total_valorUnitario, 2),
        'total_importe' => (float) round($total_importe, 2),
        'total_IVA' => (float) round($total_IVA, 2),
        'gran_total' => (float) round($gran_total, 2)
      ];

      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/enviar_datosFacturacion.php';

      $fechaproceso = fecha_con_hora($fechaProceso);

      $correo_enviarDatosFacturacion = new Correo_enviar_datosFacturacion($ordenCompra, $fechaproceso, $nombreCliente, $codigoUsuario, $nombreRazonSocial, $RFC, $usoFactura, $formaPago, $direccionCliente, $correoFactura, $noTelefonico, $productos_factura, $array_costoEnvio, $array_granTotal);

      if($correo_enviarDatosFacturacion->enviarCorreo()){
        $respuesta = '33'; //TODO BIEN
        $mensaje = 'Se envió el correo.';

        $necesitaFactura = '1';

        $sql = "UPDATE __ordenes_compra SET necesitaFactura = :necesitaFactura WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
        $stmt = $Conn_Admin->pdo->prepare($sql);
        $stmt->bindParam(':necesitaFactura', $necesitaFactura, PDO::PARAM_STR);
        $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
        $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
      }else{
        $respuesta = '32'; // NO SE ENVIO EL CORREO
        $mensaje = 'No se envió el correo.';
      }

      $Conn_Admin->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_Admin->pdo->rollBack();
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Problema al buscar la informacion de la compra.';
    }
  }

  unset($Conn_MXcomp);
  unset($Conn_Admin);
  unset($_POST['accion']);
  unset($_POST['orden_compra']);
  unset($_POST['seccion']);
  unset($_POST['nombre_razon_social']);
  unset($_POST['RFC']);
  unset($_POST['uso_factura']);
  unset($_POST['forma_pago']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nombre_vialidad']);
  unset($_POST['no_exterior']);
  unset($_POST['no_exterior_estado_check']);
  unset($_POST['no_interior']);
  unset($_POST['no_interior_estado_check']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  unset($_POST['correo_factura']);
  unset($_POST['no_telefonico']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>