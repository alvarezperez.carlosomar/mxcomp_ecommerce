<?php
if(isset($_POST['accion']) && $_POST['accion'] === "editar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $nombreRazonSocial = trim($_POST['nombre_razon_social']);
  $RFC = trim($_POST['RFC']);
  $id_usoFactura = trim($_POST['uso_factura']);
  $usoFactura = "";
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $tipoVialidad = "";
  $nombreVialidad = trim($_POST['nombre_vialidad']);
  $noExterior = trim($_POST['no_exterior']);
  $noExteriorEstadoCheck = trim($_POST['no_exterior_estado_check']);
  $noInterior = trim($_POST['no_interior']);
  $noInteriorEstadoCheck = trim($_POST['no_interior_estado_check']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $idEstado = trim($_POST['estado']);
  $nombreEstado = "";
  $correoFactura = trim($_POST['correo_factura']);
  $noTelefonico = trim($_POST['no_telefonico']);

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        $proceso_correcto = true;
      }else{
        $respuesta = "0";
        $mensaje = "El usuario no existe";
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar el usuario";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }
  
  // REVISA EL NOMBRE O RAZON SOCIAL
  if($proceso_correcto){
    if($nombreRazonSocial !== "" && validar_campo_letras_espacios($nombreRazonSocial)){
      $nombreRazonSocial = (string) $nombreRazonSocial;
      $proceso_correcto = true;
    }else if($nombreRazonSocial === ""){
      $respuesta = "1"; // El campo "Nombre o razón social" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "2"; // El Nombre o razón social no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL RFC
  if($proceso_correcto){
    if($RFC !== "" && validar_rfc($RFC)){
      if(mb_strlen($RFC) === 13){
        $RFC = (string) $RFC;
        $proceso_correcto = true;
      }else{
        $respuesta = "5"; // El RFC no contiene 13 caracteres
        $proceso_correcto = false;
      }
    }else if($RFC === ""){
      $respuesta = "3"; // El campo "RFC" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // El RFC no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL USO DE LA FACTURA
  if($proceso_correcto){
    if($id_usoFactura !== "" && validar_campo_numerico($id_usoFactura)){
      $id_usoFactura = (int) $id_usoFactura;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, clave, nombreUsoFactura FROM __uso_factura WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_usoFactura, PDO::PARAM_INT);
        $stmt->execute();
        $datos_usoFactura = $stmt->fetch(PDO::FETCH_ASSOC);
        $usoFactura_existe = (int) $datos_usoFactura['conteo'];
        
        if($usoFactura_existe === 1){
          $clave = (string) $datos_usoFactura['clave'];
          $usoFactura = (string) $datos_usoFactura['nombreUsoFactura'];
          $usoFactura = $clave . ' ' . $usoFactura;
          $proceso_correcto = true;
        }else{
          $respuesta = "7"; // Esta opcion no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al buscar los usos de factura.";
        $proceso_correcto = false;
      }
    }else if($id_usoFactura === ""){
      $respuesta = "6"; // No se ha seleccionado una opcion
      $proceso_correcto = false;
    }else{
      $respuesta = "7"; // Esta opcion no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL TIPO DE VIALIDAD
  if($proceso_correcto){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreTipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];
        
        if($tipoVialidad_existe === 1){
          $tipoVialidad = (string) $datos_tipoVialidad['nombreTipoVialidad'];
          $proceso_correcto = true;
        }else{
          $respuesta = "9"; // Este tipo de vialidad no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al buscar los tipos de vialidad.";
        $proceso_correcto = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = "8"; // No se ha seleccionado un tipo de vialidad
      $proceso_correcto = false;
    }else{
      $respuesta = "9"; // Este tipo de vialidad no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NOMBRE DE LA VIALIDAD
  if($proceso_correcto){
    if($nombreVialidad !== "" && validar_campo_letras_espacios_simbolos($nombreVialidad)){
      $nombreVialidad = (string) $nombreVialidad;
      $proceso_correcto = true;
    }else if($nombreVialidad === ""){
      $respuesta = "10"; // El campo "Nombre de vialidad" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "11"; // El Nombre de vialidad no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO EXTERIOR
  if($proceso_correcto){
    if(($noExterior !== "" && $noExteriorEstadoCheck === "false" && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === "true")){
      $noExterior = (string) $noExterior;
      $proceso_correcto = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === "false"){
      $respuesta = "12"; // El campo "No. exterior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "13"; // El No. exterior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO INTERIOR
  if($proceso_correcto){
    if(($noInterior !== "" && $noInteriorEstadoCheck === "false" && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === "true")){
      $noInterior = (string) $noInterior;
      $proceso_correcto = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === "false"){
      $respuesta = "14"; // El campo "No. interior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "15"; // El No. interior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CODIGO POSTAL
  if($proceso_correcto){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(mb_strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_correcto = true;
      }else{
        $respuesta = "18"; // El Código Postal no tiene 5 numeros
        $proceso_correcto = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = "16"; // El campo "Código Postal" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "17"; // El Código Postal no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA COLONIA
  if($proceso_correcto){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_correcto = true;
    }else if($colonia === ""){
      $respuesta = "19"; // El campo "Colonia" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "20"; // El Colonia no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_correcto){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_correcto = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = "21"; // El campo "Ciudad o Municipio" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "22"; // El Ciudad o Municipio no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL ESTADO
  if($proceso_correcto){
    if($idEstado !== "" && validar_campo_numerico($idEstado)){
      $idEstado = (string) $idEstado;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $idEstado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];
        
        if($estado_existe === 1){
          $nombreEstado = (string) $datos_estado['nombreEstado'];
          $proceso_correcto = true;
        }else{
          $respuesta = "24"; // El Estado no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al buscar el Estado.";
        $proceso_correcto = false;
      }
    }else if($idEstado === ""){
      $respuesta = "23"; // No se ha seleccionado un Estado
      $proceso_correcto = false;
    }else{
      $respuesta = "24"; // El Estado no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CORREO PARA EL ENVIO DE LA FACTURA
  if($proceso_correcto){
    if($correoFactura !== "" && validar_correo($correoFactura)){
      $correoFactura = (string) $correoFactura;
      $proceso_correcto = true;
    }else if($correoFactura === ""){
      $respuesta = "25"; // El campo "Correo electronico para envio de factura" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "26"; // El Correo electronico no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NÚMERO TELEFÓNICO
  if($proceso_correcto){
    if($noTelefonico !== "" && validar_campo_numerico($noTelefonico)){
      $noTelefonico = (string) $noTelefonico;
      
      if(mb_strlen($noTelefonico) === 10){
        $proceso_correcto = true;
      }else{
        $respuesta = "29"; // El No. telefónico no tiene 10 dígitos
        $proceso_correcto = false;
      }
    }else if($noTelefonico === ""){
      $respuesta = "27"; // El campo "No. telefónico" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "28"; // El No. telefónico no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REALIZA TODO EL PROCESO PARA ACTUALIZAR LOS DATOS DE FACTURACION EN LA TABLA DE DIRECCIONES
  if($proceso_correcto){
    $noExterior = $noExterior === '' ? 'S/N' : $noExterior;
    $noInterior = $noInterior === '' ? 'S/N' : $noInterior;
    
    $fechaActualizacion = date("Y-m-d H:i:s");

    try{
      $Conn_mxcomp->pdo->beginTransaction();
      
      // ENCRIPTAMOS EL CODIGO DE USUARIO
      $codigoUsuario_encriptado = encriptar($codigoUsuario);
      
      // ENCRIPTAMOS LOS CAMPOS
      $nombreRazonSocial = encriptar_con_clave($nombreRazonSocial, $codigoUsuario_encriptado);
      $RFC = encriptar_con_clave($RFC, $codigoUsuario_encriptado);
      $usoFactura = encriptar_con_clave($usoFactura, $codigoUsuario_encriptado);
      $nombreVialidad = encriptar_con_clave($nombreVialidad, $codigoUsuario_encriptado);
      $noExterior = encriptar_con_clave($noExterior, $codigoUsuario_encriptado);
      $noInterior = encriptar_con_clave($noInterior, $codigoUsuario_encriptado);
      $codigoPostal = encriptar_con_clave($codigoPostal, $codigoUsuario_encriptado);
      $colonia = encriptar_con_clave($colonia, $codigoUsuario_encriptado);
      $ciudadMunicipio = encriptar_con_clave($ciudadMunicipio, $codigoUsuario_encriptado);
      $nombreEstado = encriptar_con_clave($nombreEstado, $codigoUsuario_encriptado);
      $correoFactura = encriptar_con_clave($correoFactura, $codigoUsuario_encriptado);
      $noTelefonico = encriptar_con_clave($noTelefonico, $codigoUsuario_encriptado);
      
      $tipoDireccion = 'facturacion';

      $sql = "UPDATE __direcciones SET nombreRazonSocial = :nombreRazonSocial, RFC = :RFC, idUsoFactura = :idUsoFactura, usoFactura = :usoFactura, idTipoVialidad = :idTipoVialidad, tipoVialidad = :tipoVialidad, nombreVialidad = :nombreVialidad, noExterior = :noExterior, noInterior = :noInterior, codigoPostal = :codigoPostal, colonia = :colonia, ciudadMunicipio = :ciudadMunicipio, idEstado = :idEstado, nombreEstado = :nombreEstado, correoFactura = :correoFactura, noTelefonico = :noTelefonico, fechaActualizacion = :fechaActualizacion WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = :tipoDireccion";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':nombreRazonSocial', $nombreRazonSocial, PDO::PARAM_STR);
      $stmt->bindParam(':RFC', $RFC, PDO::PARAM_STR);
      $stmt->bindParam(':idUsoFactura', $id_usoFactura, PDO::PARAM_INT);
      $stmt->bindParam(':usoFactura', $usoFactura, PDO::PARAM_STR);
      $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
      $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
      $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
      $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
      $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
      $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
      $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
      $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
      $stmt->bindParam(':correoFactura', $correoFactura, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico', $noTelefonico, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
      $stmt->execute();

      $respuesta = "30"; //TODO BIEN

      $Conn_mxcomp->pdo->commit();
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al actualizar los datos de facturacion.";
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['nombre_razon_social']);
  unset($_POST['RFC']);
  unset($_POST['uso_factura']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nombre_vialidad']);
  unset($_POST['no_exterior']);
  unset($_POST['no_exterior_estado_check']);
  unset($_POST['no_interior']);
  unset($_POST['no_interior_estado_check']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  unset($_POST['correo_factura']);
  unset($_POST['no_telefonico']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>