<?php
if(isset($_POST['accion']) && $_POST['accion'] === "modificar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_password.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/funciones/creacion_token.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $passwordActual = trim($_POST['password_actual']);
  $passwordNueva = trim($_POST['password_nueva']);
  $confirmarPasswordNueva = trim($_POST['confirmar_password_nueva']);
  
  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA LA PASSWORD ACTUAL
  if($passwordActual !== "" && validar_password($passwordActual)){
    $passwordActual = (string) $passwordActual;
    $proceso_correcto = true;
  }else if($passwordActual === ""){
    $respuesta = "1"; // El campo "Contraseña actual" se encuentra vacio
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // La Contraseña actual no cumple con la expresion regular
    $proceso_correcto = false;
  }
  
  // REVISA LA NUEVA PASSWORD
  if($proceso_correcto){
    if($passwordNueva !== "" && validar_password($passwordNueva)){
      $passwordNueva = (string) $passwordNueva;
      $proceso_correcto = true;
    }else if($passwordNueva === ""){
      $respuesta = "3"; // El campo "Nueva contraseña" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // La Nueva contraseña no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CONFIRMACION DE LA NUEVA PASSWORD
  if($proceso_correcto){
    if($confirmarPasswordNueva !== "" && validar_password($confirmarPasswordNueva)){
      $confirmarPasswordNueva = (string) $confirmarPasswordNueva;
      $proceso_correcto = true;
    }else if($confirmarPasswordNueva === ""){
      $respuesta = "5"; // El campo "Confirmar nueva contraseña" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "6"; // La Nueva contraseña a confirmar no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  if($proceso_correcto){
    if($passwordNueva === $confirmarPasswordNueva){
      $idUsuario = desencriptar(trim($_SESSION['__id__']));
      $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
      
      if(validar_campo_numerico($idUsuario)){
        $idUsuario = (int) $idUsuario;
        
        try{
          $Conn_mxcomp->pdo->beginTransaction();

          $sql = "SELECT COUNT(id) AS conteo, codigoUsuario, nombreS, correo, pass_word, ssap_revision FROM __usuarios WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
          $usuario_existe = (int) $datos_usuario['conteo'];

          if($usuario_existe === 1){
            ///////////// OBTENER LA PASSWORD ENCRIPTADA /////////////
            // UNIMOS LAS PARTES DE LAS CONTRASEÑAS DE LA BASE DE DATOS
            $passwordBD_token = (string) trim($datos_usuario['pass_word']) . trim($datos_usuario['ssap_revision']);
            $passwordBD_original = mb_substr($passwordBD_token, 4);
            
            // ENCRIPTAMOS EL CODIGO DE USUARIO
            $codigoUsuario_encriptado = encriptar($codigoUsuario);
            // ENCRIPTAMOS LA PASSWORD DEL FORMULARIO CON EL CODIGO DE USUARIO ENCRIPTADO
            $passwordActual_encriptada = encriptar_con_clave($passwordActual, $codigoUsuario_encriptado);
            ///////////// FINALIZAMOS OBTENER LA PASSWORD ENCRIPTADA /////////////

            //SI COINCIDEN LAS CONTRASEÑAS
            if($passwordActual_encriptada === $passwordBD_original){
              $token_Password = token_4();

              // ENCRIPTAMOS LA PASSWORD DEL FORMULARIO CON EL CODIGO DE USUARIO ENCRIPTADO
              $passwordForm_encriptada = encriptar_con_clave($passwordNueva, $codigoUsuario_encriptado);
              $unionTokenPass = $token_Password . $passwordForm_encriptada;
              
              $pass = (string) mb_substr($unionTokenPass, 0, mb_strlen($unionTokenPass) / 2);
              $salt = (string) mb_substr($unionTokenPass, mb_strlen($unionTokenPass) / 2, mb_strlen($unionTokenPass));
              $fechaActualizacion = date("Y-m-d H:i:s");

              $sql = "UPDATE __usuarios SET pass_word = :pass_word, ssap_revision = :ssap_revision, fechaActualizacion = :fechaActualizacion WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':pass_word', $pass, PDO::PARAM_STR);
              $stmt->bindParam(':ssap_revision', $salt, PDO::PARAM_STR);
              $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->execute();
              
              $nombreS = desencriptar_con_clave(trim($datos_usuario['nombreS']), $codigoUsuario_encriptado);
              $correo = desencriptar(trim($datos_usuario['correo']));
              
              require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/restablecer_password_confirmacion.php';

              $linkAtencionClientes = HOST_LINK . 'atencion-a-clientes';

              $correo_restablecerPassword_confirmacion = new Correo_restablecerPassword_confirmacion($nombreS, $linkAtencionClientes, $correo);
              $correo_restablecerPassword_confirmacion->enviarCorreo();

              $respuesta = "9"; // TODO BIEN
              $mensaje = '
              <div class="p-notification_contenedor g-aviso-contenedor_mensaje">
                <div class="p-notification p-notification_success">
                  <span>
                    <i class="fas fa-check-circle"></i>
                  </span>
                  <p class="p-notification_p">
                    <b>Contraseña cambiada correctamente. La sesión se cerrará en: <span class="g-contenedor-tiempo_span">5 segundos</span></b>
                  </p>
                </div>
              </div>';
            }else{
              $respuesta = "8"; // La contraseña es incorrecta
            }
          }else{
            $respuesta = "0"; // El usuario no existe
            $mensaje = 'No existe el usuario';
          }
          $Conn_mxcomp->pdo->commit();
          $stmt = null;
        }catch(PDOException $error){
          $Conn_mxcomp->pdo->rollBack();
          $respuesta = "0";
          //$mensaje = "Error: " . $error->getMessage();
          $mensaje = "Problema al buscar al usuario.";
        }
      }else{
        $respuesta = "0";
        $mensaje = "Usuario no encontrado.";
      }
    }else{
      $respuesta = "7"; // Las contraseñas no son identicas
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['password_actual']);
  unset($_POST['password_nueva']);
  unset($_POST['confirmar_password_nueva']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>