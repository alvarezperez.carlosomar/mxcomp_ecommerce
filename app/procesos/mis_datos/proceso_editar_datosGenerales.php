<?php
if(isset($_POST['accion']) && $_POST['accion'] === "modificar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $nombres = trim($_POST['nombres']);
  $apellidoPaterno = trim($_POST['apellido_paterno']);
  $apellidoMaterno = trim($_POST['apellido_materno']);
  $sexo = trim($_POST['sexo']);
  $fechaNacimiento = trim($_POST['fecha_nacimiento']);
  $noTelefonico1 = trim($_POST['no_telefonico_1']);
  $noTelefonico2 = trim($_POST['no_telefonico_2']);
  $noTelefonico3 = trim($_POST['no_telefonico_3']);
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $tipoVialidad = "";
  $nombreVialidad = trim($_POST['nombre_vialidad']);
  $noExterior = trim($_POST['no_exterior']);
  $noExteriorEstadoCheck = trim($_POST['no_exterior_estado_check']);
  $noInterior = trim($_POST['no_interior']);
  $noInteriorEstadoCheck = trim($_POST['no_interior_estado_check']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $idEstado = trim($_POST['estado']);
  $nombreEstado = "";
  $entreCalle1 = trim($_POST['entre_calle_1']);
  $entreCalle2 = trim($_POST['entre_calle_2']);
  $referenciasAdicionales = trim($_POST['referencias_adicionales']);

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';

  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();
      
      if($usuario_existe === 1){
        $proceso_correcto = true;
      }else{
        $respuesta = "0";
        $mensaje = "El usuario no existe";
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar el usuario";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }
  
  // REVISA LOS NOMBRES
  if($proceso_correcto){
    if($nombres !== "" && validar_campo_letras_espacios($nombres)){
      $nombres = (string) $nombres;
      $proceso_correcto = true;
    }else if($nombres === ""){
      $respuesta = "1"; // El campo "Nombre(s)" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "2"; // El Nombre(s) no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL APELLIDO PATERNO
  if($proceso_correcto){
    if($apellidoPaterno !== "" && validar_campo_letras_espacios($apellidoPaterno)){
      $apellidoPaterno = (string) $apellidoPaterno;
      $proceso_correcto = true;
    }else if($apellidoPaterno === ""){
      $respuesta = "3"; // El campo "Apellido paterno" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // El Apellido paterno no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL APELLIDO MATERNO
  if($proceso_correcto){
    if($apellidoMaterno !== "" && validar_campo_letras_espacios($apellidoMaterno)){
      $apellidoMaterno = (string) $apellidoMaterno;
      $proceso_correcto = true;
    }else if($apellidoMaterno === ""){
      $respuesta = "5"; // El campo "Apellido materno" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "6"; // El Apellido materno no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL SEXO
  if($proceso_correcto){
    if($sexo !== "" && ($sexo === "Hombre" || $sexo === "Mujer")){
      $sexo = (string) $sexo;
      $proceso_correcto = true;
    }else if($sexo === ""){
      $respuesta = "7"; // No se selecciono el Sexo
      $proceso_correcto = false;
    }else{
      $respuesta = "8"; // Este tipo de Sexo no es ninguna opcion permitida
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA FECHA DE NACIMIENTO
  if($proceso_correcto){
    if($fechaNacimiento !== "" && validar_campo_fecha($fechaNacimiento)){
      $fechaNacimiento = (string) $fechaNacimiento;
      $proceso_correcto = true;
    }else{
      $respuesta = "9"; // No se ha seleccionado una Fecha de nacimiento o no respeta la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LOS NUMEROS TELEFONICOS
  if($proceso_correcto){
    if($noTelefonico1 !== "" && validar_campo_numerico($noTelefonico1)){
      $noTelefonico1 = (string) $noTelefonico1;
      
      if(mb_strlen($noTelefonico1) === 10){
        $noTelefonico2_correcto = false;
        $noTelefonico3_correcto = false;

        if($noTelefonico2 !== ""){
          if(validar_campo_numerico($noTelefonico2)){
            $noTelefonico2 = (string) $noTelefonico2;
            
            if(mb_strlen($noTelefonico2) === 10){
              $noTelefonico2_correcto = true;
            }else{
              $respuesta = "14"; // El No. telefonico #2 no tiene 10 dígitos
              $proceso_correcto = false;
            }
          }else{
            $respuesta = "13"; // El No. telefonico #2 no cumple con la expresion regular
            $proceso_correcto = false;
          }
        }else{
          $noTelefonico2_correcto = true;
        }

        if($noTelefonico2_correcto){
          if($noTelefonico3 !== ""){
            if(validar_campo_numerico($noTelefonico3)){
              $noTelefonico3 = (string) $noTelefonico3;
              
              if(mb_strlen($noTelefonico3) === 10){
                $noTelefonico3_correcto = true;
              }else{
                $respuesta = "16"; // El No. telefonico #3 no tiene 10 dígitos
                $proceso_correcto = false;
              }
            }else{
              $respuesta = "15"; // El No. telefonico #3 no cumple con la expresion regular
              $proceso_correcto = false;
            }
          }else{
            $noTelefonico3_correcto = true;
          }
        }

        if($noTelefonico2_correcto && $noTelefonico3_correcto){
          if($noTelefonico2 === "" && $noTelefonico3 !== ""){
            $noTelefonico2 = $noTelefonico3;
            $noTelefonico3 = "";
          }
          if($noTelefonico2 === ""){
            $noTelefonico2 = NULL;
          }
          if($noTelefonico3 === ""){
            $noTelefonico3 = NULL;
          }

          $proceso_correcto = true;
        }
      }else{
        $respuesta = "12"; // El No. telefonico #1 no tiene 10 dígitos
        $proceso_correcto = false;
      }
    }else if($noTelefonico1 === ""){
      $respuesta = "10"; // El campo "No. Telefonico #1" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "11"; // El No. telefonico #1 no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL TIPO DE VIALIDAD
  if($proceso_correcto){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreTipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];
        
        if($tipoVialidad_existe === 1){
          $tipoVialidad = (string) $datos_tipoVialidad['nombreTipoVialidad'];
          $proceso_correcto = true;
        }else{
          $respuesta = "18"; // El Tipo de vialidad no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar los tipos de vialidad";
        $proceso_correcto = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = "17"; // No se ha seleccionado un Tipo de vialidad
      $proceso_correcto = false;
    }else{
      $respuesta = "18"; // El Tipo de vialidad no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NOMBRE DE VIALIDAD
  if($proceso_correcto){
    if($nombreVialidad !== "" && validar_campo_letras_espacios_simbolos($nombreVialidad)){
      $nombreVialidad = (string) $nombreVialidad;
      $proceso_correcto = true;
    }else if($nombreVialidad === ""){
      $respuesta = "19"; // El campo "Nombre de vialidad" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "20"; // El Nombre de vialidad no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO EXTERIOR
  if($proceso_correcto){
    if(($noExterior !== "" && $noExteriorEstadoCheck === "false" && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === "true")){
      $noExterior = (string) $noExterior;
      $proceso_correcto = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === "false"){
      $respuesta = "21"; // El campo "No. exterior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "22"; // El No. exterior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO INTERIOR
  if($proceso_correcto){
    if(($noInterior !== "" && $noInteriorEstadoCheck === "false" && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === "true")){
      $noInterior = (string) $noInterior;
      $proceso_correcto = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === "false"){
      $respuesta = "23"; // El campo "No. interior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "24"; // El No. interior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CÓDIGO POSTAL
  if($proceso_correcto){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(mb_strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_correcto = true;
      }else{
        $respuesta = "27"; // El Código Postal no tiene 5 numeros
        $proceso_correcto = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = "25"; // El campo "Código Postal" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "26"; // El Código Postal no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA COLONIA
  if($proceso_correcto){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_correcto = true;
    }else if($colonia === ""){
      $respuesta = "28"; // El campo "Colonia" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "29"; // La Colonia no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_correcto){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_correcto = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = "30"; // El campo "Ciudad o Municipio" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "31"; // La Ciudad o Municipio no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL ESTADO
  if($proceso_correcto){
    if($idEstado !== "" && validar_campo_numerico($idEstado)){
      $idEstado = (int) $idEstado;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $idEstado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];
        
        if($estado_existe === 1){
          $nombreEstado = (string) $datos_estado['nombreEstado'];
          $proceso_correcto = true;
        }else{
          $respuesta = "33"; // El Estado no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar el Estado";
        $proceso_correcto = false;
      }
    }else if($idEstado === ""){
      $respuesta = "32"; // No se ha seleccionado un Estado
      $proceso_correcto = false;
    }else{
      $respuesta = "33"; // El Estado no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA LOS CAMPOS ENTRE CALLES #1 Y #2
  if($proceso_correcto){
    $entreCalle1_correcto = false;
    $entreCalle2_correcto = false;

    if($entreCalle1 === "" && $entreCalle2 === ""){
      $entreCalle1 = NULL;
      $entreCalle2 = NULL;
      $proceso_correcto = true;
    }else{
      if($entreCalle1 !== ""){
        if(validar_campo_letras_espacios_simbolos($entreCalle1)){
          $entreCalle1 = (string) $entreCalle1;
          $entreCalle1_correcto = true;
        }else{
          $respuesta = "35"; // La Calle #1 no cumple con la expresion regular
          $entreCalle1_correcto = false;
        }
      }else{
        $entreCalle1 = NULL;
        $respuesta = "34"; // La Calle #1 esta vacía
        $entreCalle1_correcto = false;
      }

      if($entreCalle1_correcto){
        if($entreCalle2 !== ""){
          if(validar_campo_letras_espacios_simbolos($entreCalle2)){
            $entreCalle2 = (string) $entreCalle2;
            $entreCalle2_correcto = true;
          }else{
            $respuesta = "37"; // La Calle #2 no cumple con la expresion regular
            $entreCalle2_correcto = false;
          }
        }else{
          $entreCalle2 = NULL;
          $respuesta = "36"; // La Calle #2 esta vacía
          $entreCalle2_correcto = false;
        }
      }

      if($entreCalle1_correcto && $entreCalle2_correcto){
        $proceso_correcto = true;
      }else{
        $proceso_correcto = false;
      }
    }
  }

  // REVISA LAS REFERENCIAS ADICIONALES
  if($proceso_correcto){
    if($referenciasAdicionales !== ""){
      if(validar_campo_letras_espacios_simbolos($referenciasAdicionales)){
        $referenciasAdicionales = (string) $referenciasAdicionales;
        $proceso_correcto = true;
      }else{
        $respuesta = "38"; // Las Referencias adicionales no cumplen con la expresion regular
        $proceso_correcto = false;
      }
    }else{
      $referenciasAdicionales = NULL;
      $proceso_correcto = true;
    }
  }

  // REALIZA TODO EL PROCESO PARA EDITAR LOS DATOS GENERALES
  if($proceso_correcto){
    $noExterior = $noExterior === '' ? 'S/N' : $noExterior;
    $noInterior = $noInterior === '' ? 'S/N' : $noInterior;
    
    $fechaActualizacion = date("Y-m-d H:i:s");

    try{
      $Conn_mxcomp->pdo->beginTransaction();
      
      // ENCRIPTAMOS EL CODIGO DE USUARIO
      $codigoUsuario_encriptado = encriptar($codigoUsuario);
      
      $nombres = encriptar_con_clave($nombres, $codigoUsuario_encriptado);
      $apellidoPaterno = encriptar_con_clave($apellidoPaterno, $codigoUsuario_encriptado);
      $apellidoMaterno = encriptar_con_clave($apellidoMaterno, $codigoUsuario_encriptado);
      $fechaNacimiento = encriptar_con_clave($fechaNacimiento, $codigoUsuario_encriptado);
      $noTelefonico1 = encriptar_con_clave($noTelefonico1, $codigoUsuario_encriptado);
      
      if(!is_null($noTelefonico2)){
        $noTelefonico2 = encriptar_con_clave($noTelefonico2, $codigoUsuario_encriptado);
      }
      
      if(!is_null($noTelefonico3)){
        $noTelefonico3 = encriptar_con_clave($noTelefonico3, $codigoUsuario_encriptado);
      }
      
      $sql = "UPDATE __usuarios SET nombreS = :nombres, apellidoPaterno = :apellidoPaterno, apellidoMaterno = :apellidoMaterno, sexo = :sexo, fechaNacimiento = :fechaNacimiento, noTelefonico1 = :noTelefonico1, noTelefonico2 = :noTelefonico2, noTelefonico3 = :noTelefonico3, fechaActualizacion = :fechaActualizacion WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':nombres', $nombres, PDO::PARAM_STR);
      $stmt->bindParam(':apellidoPaterno', $apellidoPaterno, PDO::PARAM_STR);
      $stmt->bindParam(':apellidoMaterno', $apellidoMaterno, PDO::PARAM_STR);
      $stmt->bindParam(':sexo', $sexo, PDO::PARAM_STR);
      $stmt->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico1', $noTelefonico1, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico2', $noTelefonico2, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico3', $noTelefonico3, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      
      // ENCRIPTAMOS LOS CAMPOS
      $nombreVialidad = encriptar_con_clave($nombreVialidad, $codigoUsuario_encriptado);
      $noExterior = encriptar_con_clave($noExterior, $codigoUsuario_encriptado);
      $noInterior = encriptar_con_clave($noInterior, $codigoUsuario_encriptado);
      $codigoPostal = encriptar_con_clave($codigoPostal, $codigoUsuario_encriptado);
      $colonia = encriptar_con_clave($colonia, $codigoUsuario_encriptado);
      $ciudadMunicipio = encriptar_con_clave($ciudadMunicipio, $codigoUsuario_encriptado);
      $nombreEstado = encriptar_con_clave($nombreEstado, $codigoUsuario_encriptado);
      
      if(!is_null($entreCalle1)){
        $entreCalle1 = encriptar_con_clave($entreCalle1, $codigoUsuario_encriptado);
      }
      
      if(!is_null($entreCalle2)){
        $entreCalle2 = encriptar_con_clave($entreCalle2, $codigoUsuario_encriptado);
      }
      
      if(!is_null($referenciasAdicionales)){
        $referenciasAdicionales = encriptar_con_clave($referenciasAdicionales, $codigoUsuario_encriptado);
      }

      $tipoDireccion = 'particular';

      $sql = "UPDATE __direcciones SET idTipoVialidad = :idTipoVialidad, tipoVialidad = :tipoVialidad, nombreVialidad = :nombreVialidad, noExterior = :noExterior, noInterior = :noInterior, codigoPostal = :codigoPostal, colonia = :colonia, ciudadMunicipio = :ciudadMunicipio, idEstado = :idEstado, nombreEstado = :nombreEstado, entreCalle1 = :entreCalle1, entreCalle2 = :entreCalle2, referenciasAdicionales = :referenciasAdicionales, fechaActualizacion = :fechaActualizacion WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = :tipoDireccion";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
      $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
      $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
      $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
      $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
      $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
      $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
      $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle1', $entreCalle1, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle2', $entreCalle2, PDO::PARAM_STR);
      $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
      $stmt->execute();
      
      $Conn_mxcomp->pdo->commit();

      $respuesta = "39"; //TODO BIEN
      $mensaje = '
      <div class="p-notification_contenedor g-contenedor-aviso_success">
        <div class="p-notification p-notification_success">
          <span>
            <i class="fas fa-check-circle"></i>
          </span>
          <p class="p-notification_p">
            <span><b>Cambios guardados correctamente.</b></span>
          </p>
        </div>
      </div>';
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al editar los datos generales del usuario";
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['nombres']);
  unset($_POST['apellido_paterno']);
  unset($_POST['apellido_materno']);
  unset($_POST['sexo']);
  unset($_POST['fecha_nacimiento']);
  unset($_POST['no_telefonico_1']);
  unset($_POST['no_telefonico_2']);
  unset($_POST['no_telefonico_3']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nombre_vialidad']);
  unset($_POST['no_exterior']);
  unset($_POST['no_exterior_estado_check']);
  unset($_POST['no_interior']);
  unset($_POST['no_interior_estado_check']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  unset($_POST['entre_calle_1']);
  unset($_POST['entre_calle_2']);
  unset($_POST['referencias_adicionales']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>