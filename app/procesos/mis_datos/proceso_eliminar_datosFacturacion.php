<?php
if(isset($_POST['accion']) && $_POST['accion'] === "eliminar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_password.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $password = trim($_POST['password']);
  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';

  // REVISA LA PASSWORD
  if($password !== "" && validar_password($password)){
    $idUsuario = desencriptar(trim($_SESSION['__id__']));
    $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
    $proceso_correcto = true;
  }else if($password === ""){
    $respuesta = "1"; // El campo "Contraseña" se encuentra vacio
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // La Contraseña contiene un simbolo no permitido.
    $proceso_correcto = false;
  }

  // REALIZA LOS PROCESOS PARA ELIMINAR LOS DATOS DE FACTURACIÓN
  if($proceso_correcto){
    if(validar_campo_numerico($idUsuario)){
      $idUsuario = (int) $idUsuario;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, pass_word, ssap_revision FROM __usuarios WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
        $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
        $usuarioPassword_existe = (int) $datos_usuario['conteo'];

        if($usuarioPassword_existe === 1){
          ///////////// OBTENER LA PASSWORD ENCRIPTADA /////////////
          // UNIMOS LAS PARTES DE LAS CONTRASEÑAS DE LA BASE DE DATOSssap_revision
          $passwordBD_token = (string) trim($datos_usuario['pass_word']) . trim($datos_usuario['ssap_revision']);
          // QUITAMOS LOS PRIMEROS 4 CARACTERES
          $passwordBD_original = mb_substr($passwordBD_token, 4);
          // ENCRIPTAMOS EL CODIGO DE USUARIO
          $codigoUsuario_encriptado = encriptar($codigoUsuario);
          // ENCRIPTAMOS LA PASSWORD DEL FORMULARIO CON EL CODIGO DE USUARIO ENCRIPTADO
          $passwordForm_encriptada = encriptar_con_clave($password, $codigoUsuario_encriptado);
          ///////////// FINALIZAMOS OBTENER LA PASSWORD ENCRIPTADA /////////////
          
          // SI COINCIDEN LAS CONTRASEÑAS
          if($passwordForm_encriptada === $passwordBD_original){
            try{
              $sql = "SELECT COUNT(id) FROM __direcciones WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->execute();
              $direccionFacturacion_existe = (int) $stmt->fetchColumn();

              if($direccionFacturacion_existe === 1){
                $sql = "DELETE FROM __direcciones WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
                $stmt->execute();
                $filas_afectadas = $stmt->rowCount();

                if($filas_afectadas > 0){
                  $respuesta = "5"; // TODO BIEN
                }
              }else{
                $respuesta = "4"; // La datos de facturacion ya han sido eliminados
              }
            }catch(PDOException $error){
              $respuesta = "0";
              //$mensaje = "Error: " . $error->getMessage();
              $mensaje = "Problema al buscar la direccion de facturacion";
            }
          }else{
            $respuesta = "3"; // La Contraseña es incorrecta
          }
        }else{
          $respuesta = "0"; // El usuario no existe
          $mensaje = 'No existe el usuario';
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al buscar al usuario";
      }
    }else{
      $respuesta = "0";
      $mensaje = "Usuario no encontrado.";
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['password']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>