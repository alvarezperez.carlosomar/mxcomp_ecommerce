<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'guardar'){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  $Conn_admin = new Conexion_admin();

  $ordenCompra = desencriptar(trim($_POST['orden_compra']));
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $proceso_correcto = false;
  $mensaje = '';

  // REVISA LA ORDEN DE COMPRA
  if($ordenCompra !== "" && validar_campo_numerico($ordenCompra)){
    $ordenCompra = (int) $ordenCompra;
    $proceso_correcto = true;
  }else{
    $respuesta = '0';
    $mensaje = 'La orden de compra no es válida.';
    $proceso_correcto = false;
  }

  // MODIFICAR ESTADO: NECESITA FACTURA
  if($proceso_correcto){
    try{
      $Conn_admin->pdo->beginTransaction();

      $sql = "SELECT COUNT(id) FROM __ordenes_compra WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
      $stmt = $Conn_admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $ordenCompra_existe = (int) $stmt->fetchColumn();

      if($ordenCompra_existe === 1){
        $necesitaFactura = '0';

        $sql = "UPDATE __ordenes_compra SET necesitaFactura = :necesitaFactura WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
        $stmt = $Conn_admin->pdo->prepare($sql);
        $stmt->bindParam(':necesitaFactura', $necesitaFactura, PDO::PARAM_STR);
        $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
        $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();

        $respuesta = '1';
      }else{
        $respuesta = '0';
        $mensaje = 'La orden de compra no existe.';
      }

      $Conn_admin->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_admin->pdo->rollBack();
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Hubo un error al realizar la consulta.';
    }
  }

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>