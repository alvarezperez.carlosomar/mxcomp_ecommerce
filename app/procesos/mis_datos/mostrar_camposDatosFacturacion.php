<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  $Conn_mxcomp = new Conexion_mxcomp();

  $ordenCompra = desencriptar(trim($_POST['orden_compra']));
  $metodoPago = desencriptar(trim($_POST['metodo_pago']));
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $proceso_correcto = false;
  $datosFacturacion_existen = false;
  $mensaje = '';

  // REVISA LA ORDEN DE COMPRA
  if($ordenCompra !== "" && validar_campo_numerico($ordenCompra)){
    $ordenCompra = (int) $ordenCompra;
    $proceso_correcto = true;
  }else{
    $respuesta = '0';
    $mensaje = 'La orden de compra no es válida.';
    $proceso_correcto = false;
  }

  // REVISA SI LOS DATOS DE FACTURACION EXISTEN
  if($proceso_correcto){
    //ENCRIPTAMOS EL CODIGO DE USUARIO
    $codigoUsuario_encriptado = encriptar(trim($codigoUsuario));

    try{
      $sql = "SELECT COUNT(id) FROM __direcciones WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_existen = (int) $stmt->fetchColumn();

      $datosFacturacion_existen = $datos_existen === 1 ? true : false;
      $proceso_correcto = true;
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Problema al buscar los datos de facturación.';
      $proceso_correcto = false;
    }
  }

  if($proceso_correcto){
    try{
      // SI EXISTEN LOS DATOS DE FACTURACION
      if($datosFacturacion_existen){
        $sql = "SELECT nombreRazonSocial, RFC, usoFactura, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, nombreEstado, correoFactura, noTelefonico
        FROM __direcciones
        WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
        $datos_domicilio = $stmt->fetch(PDO::FETCH_ASSOC);

        // DESENCRIPTAR LOS DATOS DE FACTURACION
        $nombreRazonSocial = desencriptar_con_clave(trim($datos_domicilio['nombreRazonSocial']), $codigoUsuario_encriptado);
        $RFC = desencriptar_con_clave(trim($datos_domicilio['RFC']), $codigoUsuario_encriptado);
        $usoFactura = desencriptar_con_clave(trim($datos_domicilio['usoFactura']), $codigoUsuario_encriptado);
        $tipoVialidad = trim($datos_domicilio['tipoVialidad']);
        $nombreVialidad = desencriptar_con_clave(trim($datos_domicilio['nombreVialidad']), $codigoUsuario_encriptado);
        $noExterior = desencriptar_con_clave(trim($datos_domicilio['noExterior']), $codigoUsuario_encriptado);
        $noInterior = desencriptar_con_clave(trim($datos_domicilio['noInterior']), $codigoUsuario_encriptado);
        $codigoPostal = desencriptar_con_clave(trim($datos_domicilio['codigoPostal']), $codigoUsuario_encriptado);
        $colonia = desencriptar_con_clave(trim($datos_domicilio['colonia']), $codigoUsuario_encriptado);
        $ciudadMunicipio = desencriptar_con_clave(trim($datos_domicilio['ciudadMunicipio']), $codigoUsuario_encriptado);
        $nombreEstado = desencriptar_con_clave(trim($datos_domicilio['nombreEstado']), $codigoUsuario_encriptado);
        $correoFactura = desencriptar_con_clave(trim($datos_domicilio['correoFactura']), $codigoUsuario_encriptado);
        $noTelefonico = desencriptar_con_clave(trim($datos_domicilio['noTelefonico']), $codigoUsuario_encriptado);
      }else{
        $sql = "SELECT Direcciones.tipoVialidad, Direcciones.nombreVialidad, Direcciones.noExterior, Direcciones.noInterior, Direcciones.codigoPostal, Direcciones.colonia, Direcciones.ciudadMunicipio, Direcciones.nombreEstado, Usuarios.nombreS, Usuarios.apellidoPaterno, Usuarios.apellidoMaterno, Usuarios.correo, Usuarios.noTelefonico1 AS noTelefonico
        FROM __usuarios Usuarios
        INNER JOIN __direcciones Direcciones ON Direcciones.idUsuario = Usuarios.id
        WHERE Usuarios.id = :idUsuario AND Usuarios.codigoUsuario = :codigoUsuario AND Direcciones.tipoDireccion = 'particular'";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
        $datos_domicilio = $stmt->fetch(PDO::FETCH_ASSOC);

        // DESENCRIPTAR LOS DATOS GENERALES
        $nombreS = desencriptar_con_clave(trim($datos_domicilio['nombreS']), $codigoUsuario_encriptado);
        $apellidoPaterno = desencriptar_con_clave(trim($datos_domicilio['apellidoPaterno']), $codigoUsuario_encriptado);
        $apellidoMaterno = desencriptar_con_clave(trim($datos_domicilio['apellidoMaterno']), $codigoUsuario_encriptado);
        $correoFactura = desencriptar(trim($datos_domicilio['correo']));
        $noTelefonico = desencriptar_con_clave(trim($datos_domicilio['noTelefonico']), $codigoUsuario_encriptado);

        $nombreRazonSocial = $nombreS . ' ' . $apellidoPaterno . ' ' . $apellidoMaterno;
        $RFC = '';
        $usoFactura = '';

        // DESENCRIPTAR EL DOMICILIO PARTICULAR
        $tipoVialidad = trim($datos_domicilio['tipoVialidad']);
        $nombreVialidad = desencriptar_con_clave(trim($datos_domicilio['nombreVialidad']), $codigoUsuario_encriptado);
        $noExterior = desencriptar_con_clave(trim($datos_domicilio['noExterior']), $codigoUsuario_encriptado);
        $noInterior = desencriptar_con_clave(trim($datos_domicilio['noInterior']), $codigoUsuario_encriptado);
        $codigoPostal = desencriptar_con_clave(trim($datos_domicilio['codigoPostal']), $codigoUsuario_encriptado);
        $colonia = desencriptar_con_clave(trim($datos_domicilio['colonia']), $codigoUsuario_encriptado);
        $ciudadMunicipio = desencriptar_con_clave(trim($datos_domicilio['ciudadMunicipio']), $codigoUsuario_encriptado);
        $nombreEstado = desencriptar_con_clave(trim($datos_domicilio['nombreEstado']), $codigoUsuario_encriptado);
      }

      $noExterior = $noExterior === 'S/N' ? NULL : $noExterior;
      $noInterior = $noInterior === 'S/N' ? NULL : $noInterior;

      // SI EXISTEN LOS DATOS DE FACTURACION
      $html = '
  <h4>' . ( $datosFacturacion_existen ? 'EDITAR' : 'AGREGAR' ) . ' DATOS DE FACTURACIÓN</h4>';

      $html .= '
  <div class="p-comprar-modal_cont_campos">';

      // SI EXISTEN LOS DATOS DE FACTURACION
      $texto_help = $datosFacturacion_existen ? 'Modifica la información de facturación en caso de ser necesario y revísala bien antes de confirmar.' : 'Los datos cargados por defecto son del domicilio particular. Revísalos por si la información de facturación es diferente.';

      $html .= '
    <p class="p-text p-text_help p-text_help_margin">
      <span>' . $texto_help . '</span>
    </p>

    <p class="p-text p-text_help p-text_help_margin p-text-align_right">
      <span>* Obligatorio</span>
    </p>

    <div class="p-loading-general g-facturacion-loading" style="padding: 0; margin-top: 1.5em;">
      <div></div>
    </div>

    <form id="id-facturacion-form_correoDatos">
      <input type="hidden" class="g-facturacion-seccion_input" value="' . ( $datosFacturacion_existen ? 'editar' : 'agregar' ) . '" autocomplete="off" readonly disabled>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Nombre o razón social:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-nombreRazonSocial_input" placeholder="Nombre o razón social" value="' . $nombreRazonSocial . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-facturacion-nombreRazonSocial_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo letras y espacios, por favor.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-nombreRazonSocial_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>RFC:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-RFC_input" placeholder="RFC" value="' . $RFC . '" autocomplete="off" maxlength="13" style="text-transform: uppercase;">
            </div>
            <p class="p-text p-text_info g-facturacion-RFC_alert_info_1">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo números y letras, por favor.</span>
            </p>
            <p class="p-text p-text_info g-facturacion-RFC_alert_info_2">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Debe de contener 13 caracteres.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-RFC_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Uso de la factura:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-facturacion-usoFactura_div">
                <select class="p-input g-form_select g-facturacion-usoFactura_select">
                  <option value="">Selecciona una opción</option>';

      $sql = "SELECT id, clave, nombreUsoFactura FROM __uso_factura";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_usoFactura = $stmt->fetch(PDO::FETCH_ASSOC)){
        $usoFactura_id = trim($datos_usoFactura['id']);
        $usoFactura_clave = trim($datos_usoFactura['clave']);
        $usoFactura_nombre = trim($datos_usoFactura['nombreUsoFactura']);

        $nombreCompleto_usoFactura = $usoFactura_clave . ' ' . $usoFactura_nombre;
        $selected = $usoFactura === $nombreCompleto_usoFactura ? 'selected="selected"' : '';

        $html .= '
                  <option ' . $selected . ' value="' . $usoFactura_id . '">' . $nombreCompleto_usoFactura . '</option>';
      }

      $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-facturacion-usoFactura_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Esta opción no existe.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-usoFactura_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado una opción.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Forma de pago:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-facturacion-formaPago_div">
                <select class="p-input g-form_select g-facturacion-formaPago_select">
                  <option value="">Selecciona una opción</option>';

      switch($metodoPago){
        case 'Depósito/Transferencia':
          $html .= '
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia electrónica de fondos">Transferencia electrónica de fondos</option>';
          break;

        case 'Tarjeta':
          $html .= '
                  <option value="Tarjeta de crédito">Tarjeta de crédito</option>
                  <option value="Tarjeta de débito">Tarjeta de débito</option>';
          break;
      }

      $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-facturacion-formaPago_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Esta opción no existe.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-formaPago_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado una opción.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Tipo de vialidad:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-facturacion-tipoVialidad_div">
                <select class="p-input g-form_select g-facturacion-tipoVialidad_select">
                  <option value="">Selecciona el tipo</option>';

      $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
        $tipoVialidad_id = trim($datos_tipoVialidad['id']);
        $tipoVialidad_nombre = trim($datos_tipoVialidad['nombreTipoVialidad']);
        $selected = $tipoVialidad === $tipoVialidad_nombre ? 'selected="selected"' : '';

        $html .= '
                        <option ' . $selected . ' value="' . $tipoVialidad_id . '">' . $tipoVialidad_nombre . '</option>';
      }

      $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-facturacion-tipoVialidad_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Este tipo de vialidad no existe.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-tipoVialidad_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado un tipo de vialidad.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Nombre de vialidad:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-nombreVialidad_input" placeholder="Nombre de vialidad" value="' . $nombreVialidad . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-facturacion-nombreVialidad_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-facturacion-nombreVialidad_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. exterior:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">';

      if(is_null($noExterior)){
        $html .= '
              <input type="text" class="p-input g-form-noExterior_input g-facturacion-noExterior_input" placeholder="S/N" autocomplete="off" disabled>';
      }else{
        $html .= '
              <input type="text" class="p-input g-form-noExterior_input g-facturacion-noExterior_input" placeholder="No. exterior" value="' . $noExterior . '" autocomplete="off">';
      }

      $html .= '
            </div>
            <p class="p-text p-text_info g-form-noExterior_alert_info g-facturacion-noExterior_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
            </p>
            <p class="p-text p-text_error g-form-noExterior_alert_error g-facturacion-noExterior_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>';

      if(is_null($noExterior)){
        $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-facturacion-noExterior_checkbox" checked>
            <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Agregar no. exterior</label>';
      }else{
        $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-facturacion-noExterior_checkbox">
            <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>';
      }

      $html .= '
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. interior:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">';

      if(is_null($noInterior)){
        $html .= '
              <input type="text" class="p-input g-form-noInterior_input g-facturacion-noInterior_input" placeholder="S/N" autocomplete="off" disabled>';
      }else{
        $html .= '
              <input type="text" class="p-input g-form-noInterior_input g-facturacion-noInterior_input" placeholder="No. interior" value="' . $noInterior . '" autocomplete="off">';
      }

      $html .= '
            </div>
            <p class="p-text p-text_info g-form-noInterior_alert_info g-facturacion-noInterior_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
            </p>
            <p class="p-text p-text_error g-form-noInterior_alert_error g-facturacion-noInterior_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>';

      if(is_null($noInterior)){
        $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-facturacion-noInterior_checkbox" checked>
            <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Agregar no. interior</label>';
      }else{
        $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-facturacion-noInterior_checkbox">
            <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>';
      }

      $html .= '
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Código postal:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-codigoPostal_input" placeholder="Código postal" value="' . $codigoPostal . '" autocomplete="off" maxlength="5">
            </div>
            <p class="p-text p-text_info g-facturacion-codigoPostal_alert_info_1">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo números, por favor.</span>
            </p>
            <p class="p-text p-text_info g-facturacion-codigoPostal_alert_info_2">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>El código postal debe de contener 5 números.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-codigoPostal_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Colonia:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-colonia_input" placeholder="Colonia" value="' . $colonia . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-facturacion-colonia_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-facturacion-colonia_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Ciudad o municipio:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-ciudadMunicipio_input" placeholder="Ciudad o municipio" value="' . $ciudadMunicipio . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-facturacion-ciudadMunicipio_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo letras y espacios, por favor.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-ciudadMunicipio_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Estado:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-facturacion-estado_div">
                <select class="p-input g-form_select g-facturacion-estado_select">
                  <option value="">Selecciona tu estado</option>';

      $sql = "SELECT id, nombreEstado FROM __estados_codigos";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
        $estado_id = trim($datos_estado['id']);
        $estado_nombre = trim($datos_estado['nombreEstado']);
        $selected = $nombreEstado === $estado_nombre ? 'selected="selected"' : '';

        $html .= '
                  <option ' . $selected . ' value="' . $estado_id . '">' . $estado_nombre . '</option>';
      }

      $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-facturacion-estado_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Este estado no existe.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-estado_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado un estado.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Correo para enviar tu factura:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-correoFactura_input" placeholder="Correo para enviar tu factura" value="' . $correoFactura . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-facturacion-correoFactura_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>El correo no cumple con el formato estándar, revísalo por favor.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-correoFactura_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. telefónico:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <p class="p-text p-text_help">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
            <div class="p-control">
              <input type="text" class="p-input g-facturacion-noTelefonico_input" placeholder="No. telefónico" value="' . $noTelefonico . '" autocomplete="off" maxlength="10">
            </div>
            <p class="p-text p-text_info g-facturacion-noTelefonico_alert_info_1">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo números, por favor.</span>
            </p>
            <p class="p-text p-text_info g-facturacion-noTelefonico_alert_info_2">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>El número telefónico debe de contener 10 dígitos.</span>
            </p>
            <p class="p-text p-text_error g-facturacion-noTelefonico_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>';

      // SI EXISTEN LOS DATOS DE FACTURACION
      $texto_info = $datosFacturacion_existen ? 'modificar tus datos (si hubo un cambio) y enviarnos un correo con la información.' : 'guardar tus datos y enviarnos un correo con la información.';

      $html .= '
      <div class="p-notification p-notification_letter_info">
        <span>Da clic en <b><i>Confirmar</i></b> para ' . $texto_info . '</span>
      </div>

      <div class="p-field_marginTop p-buttons p-buttons_right p-comprar-modal_derecho_buttons">
        <button type="submit" class="p-button p-button_success g-facturacion-submit">
          <span>
            <i class="fas fa-check"></i>
          </span>
          <span><b>Confirmar</b></span>
        </button>

        <button type="button" class="p-button p-button_delete g-modal-faturacion_cerrar">
          <span>
            <i class="fas fa-times"></i>
          </span>
          <span><b>Cancelar</b></span>
        </button>
      </div>
    </form>
  </div>';

      $respuesta = '1';
      $mensaje = $html;

      $stmt = null;
    }catch(PDOException $error){
      $respuesta = '0';
      //$mensaje = 'Error: ' . $error->getMessage();
      $mensaje = 'Problema al buscar los datos de facturación';
    }
  }

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>