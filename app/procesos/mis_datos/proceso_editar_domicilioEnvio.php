<?php
if(isset($_POST['accion']) && $_POST['accion'] === "editar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  require_once dirname(__DIR__, 2) . '/clases/usuario/metodos_usuario.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $nombreDestinatario = trim($_POST['nombre_destinatario']);
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $nombre_tipoVialidad = "";
  $nombreVialidad = trim($_POST['nombre_vialidad']);
  $noExterior = trim($_POST['no_exterior']);
  $noExteriorEstadoCheck = trim($_POST['no_exterior_estado_check']);
  $noInterior = trim($_POST['no_interior']);
  $noInteriorEstadoCheck = trim($_POST['no_interior_estado_check']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $idEstado = trim($_POST['estado']);
  $nombreEstado = "";
  $entreCalle1 = trim($_POST['entre_calle_1']);
  $entreCalle2 = trim($_POST['entre_calle_2']);
  $referenciasAdicionales = trim($_POST['referencias_adicionales']);
  $noTelefonico = trim($_POST['no_telefonico']);
  $horaEntregaEnvio1 = trim($_POST['horario_entrega_1']);
  $horaEntregaEnvio2 = trim($_POST['horario_entrega_2']);

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_exitoso = false;
  $mensaje = '';
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario = new Usuario($idUsuario, $codigoUsuario);

    if($usuario->buscarUsuario()){
      $proceso_exitoso = true;
    }else{
      $respuesta = "0";
      $mensaje = (string) $usuario->buscarUsuario_mensaje;
      $proceso_exitoso = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_exitoso = false;
  }

  // REVISA EL NOMBRE DEL DESTINATARIO
  if($proceso_exitoso){
    if($nombreDestinatario !== "" && validar_campo_letras_espacios($nombreDestinatario)){
      $nombreDestinatario = (string) $nombreDestinatario;
      $proceso_exitoso = true;
    }else if($nombreDestinatario === ""){
      $respuesta = "1"; // El campo "Nombre del destinatario" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "2"; // El nombre del destinatario no cumplen con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA EL TIPO DE VIALIDAD
  if($proceso_exitoso){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreTipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];

        if($tipoVialidad_existe === 1){
          $nombre_tipoVialidad = (string) $datos_tipoVialidad['nombreTipoVialidad'];
          $proceso_exitoso = true;
        }else{
          $respuesta = "4"; // Este tipo de vialidad no existe
          $proceso_exitoso = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar los tipos de vialidad";
        $proceso_exitoso = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = "3"; // No se ha seleccionado un tipo de vialidad
      $proceso_exitoso = false;
    }else{
      $respuesta = "4"; // Este tipo de vialidad no existe
      $proceso_exitoso = false;
    }
  }

  // REVISA EL NOMBRE DE VIALIDAD
  if($proceso_exitoso){
    if($nombreVialidad !== "" && validar_campo_letras_espacios_simbolos($nombreVialidad)){
      $nombreVialidad = (string) $nombreVialidad;
      $proceso_exitoso = true;
    }else if($nombreVialidad === ""){
      $respuesta = "5"; // El campo "Nombre de vialidad" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "6"; // El Nombre de vialidad no cumple con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA EL NUMERO EXTERIOR
  if($proceso_exitoso){
    if(($noExterior !== "" && $noExteriorEstadoCheck === "false" && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === "true")){
      $noExterior = (string) $noExterior;
      $proceso_exitoso = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === "false"){
      $respuesta = "7"; // El campo "No. exterior" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "8"; // El No. exterior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_exitoso = false;
    }
  }

  // REVISA EL NUMERO INTERIOR
  if($proceso_exitoso){
    if(($noInterior !== "" && $noInteriorEstadoCheck === "false" && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === "true")){
      $noInterior = (string) $noInterior;
      $proceso_exitoso = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === "false"){
      $respuesta = "9"; // El campo "No. interior" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "10"; // El No. interior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_exitoso = false;
    }
  }

  // REVISA EL CODIGO POSTAL
  if($proceso_exitoso){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(mb_strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_exitoso = true;
      }else{
        $respuesta = "13"; // El Código Postal debe de contener 5 numeros
        $proceso_exitoso = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = "11"; // El campo "Código Postal" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "12"; // El Código Postal no cumple con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA LA COLONIA
  if($proceso_exitoso){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_exitoso = true;
    }else if($colonia === ""){
      $respuesta = "14"; // El campo "Colonia" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "15"; // La Colonia no cumple con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_exitoso){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_exitoso = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = "16"; // El campo "Ciudad o Municipio" se encuentra vacio
      $proceso_exitoso = false;
    }else{
      $respuesta = "17"; // La Ciudad o Municipio no cumple con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA EL ESTADO
  if($proceso_exitoso){
    if($idEstado !== "" && validar_campo_numerico($idEstado)){
      $idEstado = (int) $idEstado;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $idEstado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];

        if($estado_existe === 1){
          $nombreEstado = (string) $datos_estado['nombreEstado'];
          $proceso_exitoso = true;
        }else{
          $respuesta = "19"; // Este Estado no existe
          $proceso_exitoso = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar el Estado";
        $proceso_exitoso = false;
      }
    }else if($idEstado === ""){
      $respuesta = "18"; // No se ha seleccionado un Estado
      $proceso_exitoso = false;
    }else{
      $respuesta = "19"; // Este Estado no existe
      $proceso_exitoso = false;
    }
  }

  // REVISA LOS CAMPOS ENTRE CALLES #1 Y #2
  if($proceso_exitoso){
    $entreCalle1_correcto = false;
    $entreCalle2_correcto = false;

    if($entreCalle1 === "" && $entreCalle2 === ""){
      $entreCalle1 = NULL;
      $entreCalle2 = NULL;
      $proceso_exitoso = true;
    }else{
      if($entreCalle1 !== ""){
        if(validar_campo_letras_espacios_simbolos($entreCalle1)){
          $entreCalle1 = (string) $entreCalle1;
          $entreCalle1_correcto = true;
        }else{
          $respuesta = "21"; // La Calle #1 no cumple con la expresion regular
          $entreCalle1_correcto = false;
        }
      }else{
        $entreCalle1 = NULL;
        $respuesta = "20"; // La Calle #1 esta vacia
        $entreCalle1_correcto = false;
      }

      if($entreCalle1_correcto){
        if($entreCalle2 !== ""){
          if(validar_campo_letras_espacios_simbolos($entreCalle2)){
            $entreCalle2 = (string) $entreCalle2;
            $entreCalle2_correcto = true;
          }else{
            $respuesta = "23"; //La Calle #2 no cumple con la expresion regular
            $entreCalle2_correcto = false;
          }
        }else{
          $entreCalle2 = NULL;
          $respuesta = "22"; //La Calle #2 esta vacia
          $entreCalle2_correcto = false;
        }
      }

      if($entreCalle1_correcto && $entreCalle2_correcto){
        $proceso_exitoso = true;
      }else{
        $proceso_exitoso = false;
      }
    }
  }

  // REVISA LAS REFERENCIAS ADICIONALES
  if($proceso_exitoso){
    if($referenciasAdicionales !== ""){
      if(validar_campo_letras_espacios_simbolos($referenciasAdicionales)){
        $referenciasAdicionales = (string) $referenciasAdicionales;
        $proceso_exitoso = true;
      }else{
        $respuesta = "24"; // Las Referencias adicionales no cumplen con la expresion regular
        $proceso_exitoso = false;
      }
    }else{
      $referenciasAdicionales = NULL;
      $proceso_exitoso = true;
    }
  }

  // REVISA EL NUMERO TELEFONICO
  if($proceso_exitoso){
    if($noTelefonico !== "" && validar_campo_numerico($noTelefonico)){
      $noTelefonico = (string) $noTelefonico;

      if(mb_strlen($noTelefonico) === 10){
        $proceso_exitoso = true;
      }else{
        $respuesta = "27"; // El campo "No. telefónico (Contacto)" no tiene 10 dígitos
        $proceso_exitoso = false;
      }
    }else if($noTelefonico === ""){
      $respuesta = "25"; // El campo "No. telefónico (Contacto)" se encuentra vacío
      $proceso_exitoso = false;
    }else{
      $respuesta = "26"; // El No. telefónico (Contacto) no cumple con la expresion regular
      $proceso_exitoso = false;
    }
  }

  // REVISA LOS CAMPOS DEL RANGO HORARIO DE ENTREGA
  if($proceso_exitoso){
    $horario_entrega_1_correcto = false;
    $horario_entrega_2_correcto = false;

    if($horaEntregaEnvio1 === "" && $horaEntregaEnvio2 === ""){
      $horaEntregaEnvio1 = NULL;
      $horaEntregaEnvio2 = NULL;
      $proceso_exitoso = true;
    }else{
      if($horaEntregaEnvio1 !== ""){
        if(validar_campo_hora($horaEntregaEnvio1)){
          $horaEntregaEnvio1 = (string) $horaEntregaEnvio1;
          $horario_entrega_1_correcto = true;
        }else{
          $respuesta = "29"; // La hora #1 no cumple con la expresion regular
          $horario_entrega_1_correcto = false;
        }
      }else{
        $horaEntregaEnvio1 = NULL;
        $respuesta = "28"; // La hora #1 esta vacia
        $horario_entrega_1_correcto = false;
      }

      if($horario_entrega_1_correcto){
        if($horaEntregaEnvio2 !== ""){
          if(validar_campo_hora($horaEntregaEnvio2)){
            $horaEntregaEnvio2 = (string) $horaEntregaEnvio2;
            $horario_entrega_2_correcto = true;
          }else{
            $respuesta = "31"; //La hora #2 no cumple con la expresion regular
            $horario_entrega_2_correcto = false;
          }
        }else{
          $horaEntregaEnvio2 = NULL;
          $respuesta = "30"; //La hora #2 esta vacia
          $horario_entrega_2_correcto = false;
        }
      }

      if($horario_entrega_1_correcto && $horario_entrega_2_correcto){
        $proceso_exitoso = true;
      }else{
        $proceso_exitoso = false;
      }
    }
  }

  // REALIZA TODO EL PROCESO PARA EDITAR EL DOMICILIO DE ENVÍO
  if($proceso_exitoso){
    $noExterior = $noExterior === '' ? 'S/N' : $noExterior;
    $noInterior = $noInterior === '' ? 'S/N' : $noInterior;

    $fechaActualizacion = date("Y-m-d H:i:s");

    try{
      $sql = "SELECT COUNT(id) FROM __direcciones WHERE BINARY tipoDireccion = 'envio' AND idUsuario = :idUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->execute();
      $direccion_existe = (int) $stmt->fetchColumn();

      if($direccion_existe === 1){
        $Conn_mxcomp->pdo->beginTransaction();

        // ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);

        // ENCRIPTAMOS LOS CAMPOS
        $nombreVialidad = encriptar_con_clave($nombreVialidad, $codigoUsuario_encriptado);
        $noExterior = encriptar_con_clave($noExterior, $codigoUsuario_encriptado);
        $noInterior = encriptar_con_clave($noInterior, $codigoUsuario_encriptado);
        $codigoPostal = encriptar_con_clave($codigoPostal, $codigoUsuario_encriptado);
        $colonia = encriptar_con_clave($colonia, $codigoUsuario_encriptado);
        $ciudadMunicipio = encriptar_con_clave($ciudadMunicipio, $codigoUsuario_encriptado);
        $nombreEstado = encriptar_con_clave($nombreEstado, $codigoUsuario_encriptado);

        if(!is_null($entreCalle1)){
          $entreCalle1 = encriptar_con_clave($entreCalle1, $codigoUsuario_encriptado);
        }

        if(!is_null($entreCalle2)){
          $entreCalle2 = encriptar_con_clave($entreCalle2, $codigoUsuario_encriptado);
        }

        if(!is_null($referenciasAdicionales)){
          $referenciasAdicionales = encriptar_con_clave($referenciasAdicionales, $codigoUsuario_encriptado);
        }

        $noTelefonico = encriptar_con_clave($noTelefonico, $codigoUsuario_encriptado);
        $nombreDestinatario = encriptar_con_clave($nombreDestinatario, $codigoUsuario_encriptado);

        if(!is_null($horaEntregaEnvio1)){
          $horaEntregaEnvio1 = encriptar_con_clave($horaEntregaEnvio1, $codigoUsuario_encriptado);
        }

        if(!is_null($horaEntregaEnvio2)){
          $horaEntregaEnvio2 = encriptar_con_clave($horaEntregaEnvio2, $codigoUsuario_encriptado);
        }

        $tipoDireccion = 'envio';

        $sql = "UPDATE __direcciones SET idTipoVialidad = :idTipoVialidad, tipoVialidad = :tipoVialidad, nombreVialidad = :nombreVialidad, noExterior = :noExterior, noInterior = :noInterior, codigoPostal = :codigoPostal, colonia = :colonia, ciudadMunicipio = :ciudadMunicipio, idEstado = :idEstado, nombreEstado = :nombreEstado, entreCalle1 = :entreCalle1, entreCalle2 = :entreCalle2, referenciasAdicionales = :referenciasAdicionales, noTelefonico = :noTelefonico, nombreDestinatario = :nombreDestinatario, horaEntregaEnvio1 = :horaEntregaEnvio1, horaEntregaEnvio2 = :horaEntregaEnvio2, fechaActualizacion = :fechaActualizacion WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = :tipoDireccion";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->bindParam(':tipoVialidad', $nombre_tipoVialidad, PDO::PARAM_STR);
        $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
        $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
        $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
        $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
        $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
        $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
        $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
        $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
        $stmt->bindParam(':entreCalle1', $entreCalle1, PDO::PARAM_STR);
        $stmt->bindParam(':entreCalle2', $entreCalle2, PDO::PARAM_STR);
        $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
        $stmt->bindParam(':noTelefonico', $noTelefonico, PDO::PARAM_STR);
        $stmt->bindParam(':nombreDestinatario', $nombreDestinatario, PDO::PARAM_STR);
        $stmt->bindParam(':horaEntregaEnvio1', $horaEntregaEnvio1, PDO::PARAM_STR);
        $stmt->bindParam(':horaEntregaEnvio2', $horaEntregaEnvio2, PDO::PARAM_STR);
        $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
        $stmt->execute();

        $Conn_mxcomp->pdo->commit();

        $respuesta = "32"; //TODO BIEN
      }else{
        $respuesta = "0";
        $mensaje = "No existe la direccion de envio";
      }
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al editar los datos generales del usuario";
    }
  }

  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['nombre_destinatario']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nombre_vialidad']);
  unset($_POST['no_exterior']);
  unset($_POST['no_exterior_estado_check']);
  unset($_POST['no_interior']);
  unset($_POST['no_interior_estado_check']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  unset($_POST['entre_calle_1']);
  unset($_POST['entre_calle_2']);
  unset($_POST['referencias_adicionales']);
  unset($_POST['no_telefonico']);
  unset($_POST['horario_entrega_1']);
  unset($_POST['horario_entrega_2']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>