<?php
if(isset($_POST['accion']) && $_POST['accion'] === "validar"){
  include dirname(__DIR__, 1) . '/funciones/validaciones_correo.php';
  include dirname(__DIR__, 1) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 1) . '/global/config.php';

  $correo = trim($_POST['correo']);
  $mensaje = '';

  if(validar_correo($correo)){
    include dirname(__DIR__, 1) . '/conn.php';

    $Conn_mxcomp = new Conexion_mxcomp();
    $correo = (string) $correo;

    try{
      // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
      $correo = encriptar($correo);

      $sql = "SELECT COUNT(id) FROM __usuarios WHERE correo = :correo";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':correo', $correo, PDO::PARAM_STR);
      $stmt->execute();
      $correo_existe = (int) $stmt->fetchColumn();

      $respuesta = $correo_existe === 0 ? "4" : "3";

      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "2";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar el correo.";
    }
  }else{
    $respuesta = "1";
  }
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>