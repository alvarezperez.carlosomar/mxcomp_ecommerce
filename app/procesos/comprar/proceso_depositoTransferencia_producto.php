<?php
if (isset($_POST['accion']) && $_POST['accion'] === "comprar") {
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  require_once dirname(__DIR__, 2) . '/clases/direcciones/metodos_direcciones.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $proceso_correcto = false;
  $mensaje = "";
  $link = "";
  $correo_cliente = "";

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE VALIDA QUE EL USUARIO EXISTA
  if (validar_campo_numerico($idUser)) {
    $idUser = (int) $idUser;

    // PREGUNTAMOS SI EL USUARIO EXISTE
    try {
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario, correo FROM __usuarios WHERE id = :id AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];

      if ($usuario_existe === 1) {
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];

        // ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);

        $correo = (string) trim($datos_usuario['correo']);
        $correo_cliente = desencriptar($correo);

        $proceso_correcto = true;
      } else {
        $respuesta = "0";
        $mensaje = "El usuario no existe";
        $proceso_correcto = false;
      }

      $stmt = null;
    } catch (PDOException $error) {
      $respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Problema al buscar la información del usuario";
      $proceso_correcto = false;
    }
  } else {
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE OBTIENEN LOS DATOS DE POST Y OTRAS VARIABLES
  if ($proceso_correcto) {
    $seccion = trim($_POST['seccion']);
    $skuProducto = desencriptar(trim($_POST['sku_prod']));
    $metodoPago = trim($_POST['metodo_pago']);
    $metodoEnvio = trim($_POST['metodo_envio']);
    $costoEnvio_almacen_1 = trim($_POST['costo_envio_almacen_1']);
    $costoEnvio_almacen_7 = trim($_POST['costo_envio_almacen_7']);
    $costoEnvio_almacen_56 = trim($_POST['costo_envio_almacen_56']);
    $costoEnvio_almacen_74 = trim($_POST['costo_envio_almacen_74']);
    $costoEnvio_total = trim($_POST['costo_envio_total']);
    $precioPedido = trim($_POST['precio_pedido']);
    $totalPagar = trim($_POST['total_pagar']);

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA TODOS LOS CAMPOS
  if ($proceso_correcto) {
    $op = "";

    // REVISA LA VARIABLE SECCION
    if ($seccion !== "" && validar_campo_letras_espacios($seccion)) {
      $seccion = (string) $seccion;
      $proceso_correcto = true;
    } else {
      $proceso_correcto = false;
      $op = "1";
    }

    // REVISA LA SKU
    if ($proceso_correcto) {
      if ($skuProducto !== "" && validar_sku_caracteres($skuProducto)) {
        $skuProducto = (string) $skuProducto;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "2";
      }
    }

    // REVISA EL METODO DE PAGO
    if ($proceso_correcto) {
      if ($metodoPago !== "" && validar_campo_letras_espacios_simbolos($metodoPago)) {
        $metodoPago = (string) $metodoPago;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "3";
      }
    }

    // REVISA EL METODO DE ENVIO
    if ($proceso_correcto) {
      if ($metodoEnvio !== "" && validar_campo_letras_espacios_simbolos($metodoEnvio)) {
        $metodoEnvio = (string) $metodoEnvio;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "4";
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 1
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_1 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_1) || validar_campo_letras_espacios($costoEnvio_almacen_1)) {
          $costoEnvio_almacen_1 = (string) $costoEnvio_almacen_1;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "5";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 7
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_7 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_7) || validar_campo_letras_espacios($costoEnvio_almacen_7)) {
          $costoEnvio_almacen_7 = (string) $costoEnvio_almacen_7;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "6";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 56
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_56 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_56) || validar_campo_letras_espacios($costoEnvio_almacen_56)) {
          $costoEnvio_almacen_56 = (string) $costoEnvio_almacen_56;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "7";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 74
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_74 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_74) || validar_campo_letras_espacios($costoEnvio_almacen_74)) {
          $costoEnvio_almacen_74 = (string) $costoEnvio_almacen_74;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "8";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO TOTAL
    if ($proceso_correcto) {
      if ($costoEnvio_total !== "" && validar_precio_producto($costoEnvio_total)) {
        if ($costoEnvio_total === "0.00" || $costoEnvio_total === "0") {
          if ($metodoEnvio === "Paqueteria") {
            $costoEnvio_total = "Envío gratis";
          } else {
            $costoEnvio_total = "0.00";
          }
        } else {
          $costoEnvio_total = (float) $costoEnvio_total;
        }

        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "9";
      }
    }

    // REVISA EL PRECIO DEL PEDIDO
    if ($proceso_correcto) {
      if ($precioPedido !== "" && validar_precio_producto($precioPedido)) {
        $precioPedido = (float) $precioPedido;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "10";
      }
    }

    // REVISA EL TOTAL A PAGAR
    if ($proceso_correcto) {
      if ($totalPagar !== "" && validar_precio_producto($totalPagar)) {
        $totalPagar = (float) $totalPagar;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "11";
      }
    }

    // SI ES FALSE, SE IMPRIME LO SIGUIENTE
    if ($proceso_correcto === false) {
      $respuesta = "1"; // MENSAJE DE INFO
      $mensaje = "Ocurrió un problema, recarga y vuelve a realizar tu compra.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // SE BUSCAN LOS DATOS DEL DOMICILIO DE ENVIO

  // PARA CORREO DE PCH Y REGISTRO EN ORDEN DE COMPRAS
  $calleNumero_encriptado = "";
  $codigoPostal_encriptado = "";
  $colonia_encriptado = "";
  $ciudadMunicipio_encriptado = "";
  $estado_encriptado = "";
  $entreCalle1_encriptado = "";
  $entreCalle2_encriptado = "";
  $horaEntregaEnvio_1_encriptado = "";
  $horaEntregaEnvio_2_encriptado = "";

  if($proceso_correcto) {
    if($metodoEnvio === "Paqueteria") {
      $direccionUsuario = new Direccion($idUser, $codigoUsuario);

      if($direccionUsuario->ver_domicilioEnvio()){
        // REGISTRO EN ORDEN DE COMPRAS y CORREO DE PCH DEL DOMICILIO DE ENVIO
        $calleNumero_encriptado = $direccionUsuario->ver_domEnv_tipoVialidad . " " . $direccionUsuario->ver_domEnv_nombreVialidad . " No. Ext. " . $direccionUsuario->ver_domEnv_noExterior . ", No. Int. " . $direccionUsuario->ver_domEnv_noInterior;
        $calleNumero_encriptado = encriptar_con_clave($calleNumero_encriptado, $codigoUsuario_encriptado);
        $codigoPostal_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_codigoPostal, $codigoUsuario_encriptado);
        $colonia_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_colonia, $codigoUsuario_encriptado);
        $ciudadMunicipio_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_ciudadMunicipio, $codigoUsuario_encriptado);
        $estado_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_nombreEstado, $codigoUsuario_encriptado);

        if (is_null($direccionUsuario->ver_domEnv_entreCalle1)) {
          $entreCalle1_encriptado = NULL;
        } else {
          $entreCalle1_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_entreCalle1, $codigoUsuario_encriptado);
        }

        if (is_null($direccionUsuario->ver_domEnv_entreCalle2)) {
          $entreCalle2_encriptado = NULL;
        } else {
          $entreCalle2_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_entreCalle2, $codigoUsuario_encriptado);
        }

        if (is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio1)) {
          $horaEntregaEnvio_1_encriptado = NULL;
        } else {
          $horaEntregaEnvio_1_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_horaEntregaEnvio1, $codigoUsuario_encriptado);
        }

        if (is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio2)) {
          $horaEntregaEnvio_2_encriptado = NULL;
        } else {
          $horaEntregaEnvio_2_encriptado = encriptar_con_clave($direccionUsuario->ver_domEnv_horaEntregaEnvio2, $codigoUsuario_encriptado);
        }

        // REGISTRO ORDEN DE COMPRA Y CORREO REMISIONES PCH
        $direccionCompleta = $direccionUsuario->ver_domEnv_direccionCompleta_encriptado; // ENCRIPTADA
        $nomDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_encriptado; // ENCRIPTADO
        $telefono = $direccionUsuario->ver_domEnv_noTelefonico_encriptado; // ENCRIPTADO
        $entreCalles = $direccionUsuario->ver_domEnv_entreCalles_encriptado; // ENCRIPTADO
        $referenciasAdicionales = $direccionUsuario->ver_domEnv_referenciasAdicionales_encriptado; // ENCRIPTADO
        $horarioEntrega = $direccionUsuario->ver_domEnv_horarioEntrega; // ENCRIPTADO
        $proceso_correcto = true;
      } else {
        $respuesta = $direccionUsuario->ver_domEnv_respuesta; // MENSAJE DE INFO, TRAE RESPUESTA 1
        $mensaje = $direccionUsuario->ver_domEnv_mensaje;
        $proceso_correcto = false;
      }
    } else {
      $direccionCompleta = NULL;
      $nomDestinatario = NULL;
      $telefono = NULL;
      $entreCalles = NULL;
      $referenciasAdicionales = NULL;
      $horarioEntrega = NULL;
      $proceso_correcto = true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA SI EXISTE LA SECCION PRODUCTO
  if ($proceso_correcto) {
    if ($seccion === "producto") {
      $proceso_correcto = true;
    } else {
      $respuesta = "0";
      $mensaje = "No existe esta seccion";
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA LAS EXISTENCIAS DEL PRODUCTO
  if ($proceso_correcto) {
    try {
      $sql = "SELECT COUNT(id) AS conteo, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, existenciaAlmacen_1, existenciaAlmacen_7, existenciaAlmacen_56, existenciaAlmacen_74, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __productos WHERE sku = :sku AND existenciaTotal != 0";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $productos_existe = (int) $datos_producto['conteo'];

      if ($productos_existe === 1) {
        // UNIDADES QUE COMPRARÁ DEL PRODUCTO
        $unidadesAlmacen_1 = (int) $_SESSION['__seccion_comprar_producto__'][0]['unidades_almacen_1'];
        $unidadesAlmacen_7 = (int) $_SESSION['__seccion_comprar_producto__'][0]['unidades_almacen_7'];
        $unidadesAlmacen_56 = (int) $_SESSION['__seccion_comprar_producto__'][0]['unidades_almacen_56'];
        $unidadesAlmacen_74 = (int) $_SESSION['__seccion_comprar_producto__'][0]['unidades_almacen_74'];

        $descripcion = (string) $datos_producto['descripcion'];
        $descripcionURL = (string) $datos_producto['descripcionURL'];
        $nombreMarca = (string) $datos_producto['nombreMarca'];
        $precioProveedor = (float) $datos_producto['precioProveedor'];
        $monedaProveedor = (string) $datos_producto['monedaProveedor'];
        $precioMXcomp = (float) $datos_producto['precioMXcomp'];
        $monedaMXcomp = (string) $datos_producto['monedaMXcomp'];
        $existenciaAlmacen_1 = (int) $datos_producto['existenciaAlmacen_1'];
        $existenciaAlmacen_7 = (int) $datos_producto['existenciaAlmacen_7'];
        $existenciaAlmacen_56 = (int) $datos_producto['existenciaAlmacen_56'];
        $existenciaAlmacen_74 = (int) $datos_producto['existenciaAlmacen_74'];
        $tieneImagen = (string) $datos_producto['tieneImagen'];
        $numeroUbicacionImagen = (string) $datos_producto['numeroUbicacionImagen'];
        $nombreImagen = (string) $datos_producto['nombreImagen'];
        $versionImagen = (string) $datos_producto['versionImagen'];

        if ($unidadesAlmacen_1 <= $existenciaAlmacen_1 || $unidadesAlmacen_7 <= $existenciaAlmacen_7 || $unidadesAlmacen_56 <= $existenciaAlmacen_56 || $unidadesAlmacen_74 <= $existenciaAlmacen_74) {
          $proceso_correcto = true;
        } else {
          $respuesta = "1"; //MENSAJE DE INFO
          $mensaje = "Algunas unidades exceden las disponibles para uno o varios almacenes, revisa tu pedido y vuelve a realizar tu compra.";
          $proceso_correcto = false;
        }
      } else {
        $respuesta = "1"; // MENSAJE DE INFO
        $mensaje = "Lo sentimos, este producto ya no se encuentra en nuestros registros, revisa tu pedido y vuelve a realizar tu compra.";
        $proceso_correcto = false;
      }
    } catch (PDOException $error) {
      $respuesta = "2"; // MENSAJE DE ERROR
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Hubo un problema al revisar las existencias del producto, vuelve a intentar el proceso de compra. Si este mensaje sigue apareciendo contacta con atención a clientes.";
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE REMISIONES Y/O ORDEN DE COMPRA
  $errorOrdenCompra_existe = false;
  $errorOrdenCompra_mensaje_error = "";
  $errorOrdenCompra_seccion = "";
  $errorOrdenCompra_proveedor = "";
  $errorOrdenCompra_numero_almacen = "";
  $errorOrdenCompra_nombre_almacen = "";
  $errorOrdenCompra_moneda_remision = "";
  $errorOrdenCompra_cliente_id = "";
  $errorOrdenCompra_codigo_cliente = "";
  $errorOrdenCompra_remisiones_eliminar = "";

  $acumulacion_remisiones = "";
  $id_ordenCompra = "";

  // SE REGISTRA LA ORDEN DE COMPRA EN SUS RESPECTIVAS TABLAS, SE MANEJA COMO UNA TRANSACCION
  if ($proceso_correcto) {
    $tipo_error = "";
    $proceso_correcto = false; // Para los siguientes pasos se vuelve a resetear a false

    try {
      $conexion_admin->beginTransaction();
      $fechaCompra = date("Y-m-d H:i:s");

      // SE GENERA LA ORDEN DE COMPRA
      $tipo_error = "No se pudo consultar la existencia de las ordenes de compras.";

      $sql = "SELECT COUNT(id) FROM __ordenes_compra";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->execute();
      $ordenesCompras_existen = (int) $stmt->fetchColumn();

      if ($ordenesCompras_existen === 0) {
        $ordenCompra = 1;
      } else {
        $tipo_error = "No se pudo generar la orden de compra.";

        $sql = "SELECT ordenCompra FROM __ordenes_compra ORDER BY id DESC LIMIT 1";
        $stmt = $conexion_admin->prepare($sql);
        $stmt->execute();
        $numeracion = (int) $stmt->fetchColumn();
        $ordenCompra = $numeracion + 1;
      }

      $tipo_error = "";

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // SE GENERAN LAS REMISIONES NECESARIAS PARA LA ORDEN DE COMPRA

      $remisiones_correctas_almacen_1 = false; // ALMACEN GUADALAJARA
      $remisiones_correctas_almacen_7 = false; // ALMACEN DF
      $remisiones_correctas_almacen_56 = false; // ALMACEN PUEBLA
      $remisiones_correctas_almacen_74 = false; // ALMACEN LEON

      $remisiones_almacen_1 = [];
      $remisiones_almacen_7 = [];
      $remisiones_almacen_56 = [];
      $remisiones_almacen_74 = [];

      //require_once dirname(__DIR__, 2) . '/clases/proveedor/procesos_PCH.php';
      //$proveedorPCH = new Proveedor_PCH();

      /////////////////////////////////////////////////////////////////
      // REMISIONES DE ALMACEN GUADALAJARA - 1
      if ($unidadesAlmacen_1 > 0) {
        // TEMPORAL

        $remisiones_almacen_1[0] = array(
          'sku' => $skuProducto,
          'remision' => '12345678',
        );

        $acumulacion_remisiones = '12345678';

        $remisiones_correctas_almacen_1 = true;
        $proceso_correcto = true;
        $errorOrdenCompra_existe = false;

        /* $arrayProductosRemision[0] = array(
      "strSku" => (string) $skuProducto,
      "iCantidad" => (int) $unidadesAlmacen_1,
      );

      $noAlmacen = 1;

      if ($proveedorPCH->crear_remision($noAlmacen, $monedaProveedor, $arrayProductosRemision, $ordenCompra)) {
      $remision_PCH = (string) $proveedorPCH->remisionPCH;

      if ($remision_PCH !== "") {
      $remisiones_almacen_1[0] = array(
      'sku' => $skuProducto,
      'remision' => $remision_PCH,
      );

      $acumulacion_remisiones = $remision_PCH;

      $remisiones_correctas_almacen_1 = true;
      $proceso_correcto = true;
      $errorOrdenCompra_existe = false;
      } else {
      $errorOrdenCompra_existe = true;

      $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
      }
      } else {
      $errorOrdenCompra_existe = true;

      switch ($proveedorPCH->opcion) {
      case '1': // WEB SERVICES
      $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
      break;

      case '2': // PROVEEDOR
      $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
      break;
      }
      }

      if ($errorOrdenCompra_existe) {
      $remisiones_correctas_almacen_1 = false;
      $respuesta = "1"; // MENSAJE DE INFO
      $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
      $proceso_correcto = false;

      $errorOrdenCompra_seccion = "Remisiones (Producto - Depósito/Transferencia)";
      $errorOrdenCompra_proveedor = "PCH Mayoreo";
      $errorOrdenCompra_numero_almacen = "1";
      $errorOrdenCompra_nombre_almacen = "Guadalajara";
      $errorOrdenCompra_moneda_remision = $monedaProveedor;
      $errorOrdenCompra_cliente_id = $idUser;
      $errorOrdenCompra_codigo_cliente = $codigoUsuario;
      $errorOrdenCompra_remisiones_eliminar = "----";
      } */
      } else {
        $remisiones_correctas_almacen_1 = true;
        $proceso_correcto = true;
        $errorOrdenCompra_existe = false;
      }

      /////////////////////////////////////////////////////////////////
      // REMISIONES DE ALMACEN DF - 7
      if ($proceso_correcto) {
        if ($unidadesAlmacen_7 > 0) {
          $arrayProductosRemision[0] = array(
            "strSku" => (string) $skuProducto,
            "iCantidad" => (int) $unidadesAlmacen_7,
          );

          $noAlmacen = 7;

          if ($proveedorPCH->crear_remision($noAlmacen, $monedaProveedor, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              $remisiones_almacen_7[0] = array(
                'sku' => $skuProducto,
                'remision' => $remision_PCH,
              );

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_7 = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_7 = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Producto - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "7";
            $errorOrdenCompra_nombre_almacen = "DF";
            $errorOrdenCompra_moneda_remision = $monedaProveedor;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_7 = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////
      // REMISIONES DE ALMACEN PUEBLA - 56
      if ($proceso_correcto) {
        if ($unidadesAlmacen_56 > 0) {
          $arrayProductosRemision[0] = array(
            "strSku" => (string) $skuProducto,
            "iCantidad" => (int) $unidadesAlmacen_56,
          );

          $noAlmacen = 56;

          if ($proveedorPCH->crear_remision($noAlmacen, $monedaProveedor, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              $remisiones_almacen_56[0] = array(
                'sku' => $skuProducto,
                'remision' => $remision_PCH,
              );

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_56 = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_56 = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Producto - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "56";
            $errorOrdenCompra_nombre_almacen = "Puebla";
            $errorOrdenCompra_moneda_remision = $monedaProveedor;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_56 = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////
      // REMISIONES DE ALMACEN LEON - 74
      if ($proceso_correcto) {
        if ($unidadesAlmacen_74 > 0) {
          $arrayProductosRemision[0] = array(
            "strSku" => (string) $skuProducto,
            "iCantidad" => (int) $unidadesAlmacen_74,
          );

          $noAlmacen = 74;

          if ($proveedorPCH->crear_remision($noAlmacen, $monedaProveedor, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              $remisiones_almacen_74[0] = array(
                'sku' => $skuProducto,
                'remision' => $remision_PCH,
              );

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_74 = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_74 = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Producto - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "74";
            $errorOrdenCompra_nombre_almacen = "León";
            $errorOrdenCompra_moneda_remision = $monedaProveedor;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_74 = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // SE GUARDAN LOS REGISTROS EN LA TABLA QUE LE CORRESPONDE, POR CADA ALMACEN
      if ($proceso_correcto) {
        $tipo_error = "Se canceló la compra porque surgió un problema durante un proceso, vuelve a realizar la compra. Si sigue apareciendo este mensaje, ponte en contacto con atención a clientes.";

        // EN CASO DE HABER UN ERROR EN UNA CONSULTA
        $errorOrdenCompra_existe = true;
        $errorOrdenCompra_mensaje_error = "Orden de compra: Surgió un problema al tratar de ingresar un registro en una tabla. - ";
        $errorOrdenCompra_seccion = "Orden de compra (Producto - Depósito/Transferencia)";
        $errorOrdenCompra_proveedor = "----";
        $errorOrdenCompra_numero_almacen = "----";
        $errorOrdenCompra_nombre_almacen = "----";
        $errorOrdenCompra_moneda_remision = "----";
        $errorOrdenCompra_cliente_id = $idUser;
        $errorOrdenCompra_codigo_cliente = $codigoUsuario;

        $proceso_correcto = false;

        if ($acumulacion_remisiones === "") {
          $errorOrdenCompra_remisiones_eliminar = "----";
        } else {
          $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
        }

        // SE INSERTA LA ORDEN DE COMPRA EN LA TABLA __ORDENES_COMPRA
        $numeroEstadoCompra = "1";
        $estadoCompra = "Pendiente";
        $productosComprados = (int) $unidadesAlmacen_1 + $unidadesAlmacen_7 + $unidadesAlmacen_56 + $unidadesAlmacen_74;
        $numeroEstadoPago = "1";
        $estadoPago = "Pendiente";

        if ($productosComprados > 0) {
          $sql = "INSERT INTO __ordenes_compra(numeroEstadoCompra, estadoCompra, ordenCompra, clienteID, codigoCliente, productosComprados, totalPagado, precioPedido, monedaPedido, numeroEstadoPago, estadoPago, metodoEnvio, costoEnvio, direccionCompleta, calleNumero, codigoPostal, colonia, ciudadMunicipio, estado, nomDestinatario, telefono, entreCalles, entreCalle1, entreCalle2, referenciasAdicionales, horarioEntrega, horaEntregaEnvio_1, horaEntregaEnvio_2, seccion, fechaCompra) VALUES(:numeroEstadoCompra, :estadoCompra, :ordenCompra, :clienteID, :codigoCliente, :productosComprados, :totalPagado, :precioPedido, :monedaPedido, :numeroEstadoPago, :estadoPago, :metodoEnvio, :costoEnvio, :direccionCompleta, :calleNumero, :codigoPostal, :colonia, :ciudadMunicipio, :estado, :nomDestinatario, :telefono, :entreCalles, :entreCalle1, :entreCalle2, :referenciasAdicionales, :horarioEntrega, :horaEntregaEnvio_1, :horaEntregaEnvio_2, :seccion, :fechaCompra)";
          $stmt = $conexion_admin->prepare($sql);
          $stmt->bindParam(':numeroEstadoCompra', $numeroEstadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':estadoCompra', $estadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':productosComprados', $productosComprados, PDO::PARAM_STR);
          $stmt->bindParam(':totalPagado', $totalPagar, PDO::PARAM_STR);
          $stmt->bindParam(':precioPedido', $precioPedido, PDO::PARAM_STR);
          $stmt->bindParam(':monedaPedido', $monedaMXcomp, PDO::PARAM_STR);
          $stmt->bindParam(':numeroEstadoPago', $numeroEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':metodoEnvio', $metodoEnvio, PDO::PARAM_STR);
          $stmt->bindParam(':costoEnvio', $costoEnvio_total, PDO::PARAM_STR);
          $stmt->bindParam(':direccionCompleta', $direccionCompleta, PDO::PARAM_STR);
          $stmt->bindParam(':calleNumero', $calleNumero_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':codigoPostal', $codigoPostal_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':colonia', $colonia_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':estado', $estado_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':nomDestinatario', $nomDestinatario, PDO::PARAM_STR);
          $stmt->bindParam(':telefono', $telefono, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalles', $entreCalles, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalle1', $entreCalle1_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalle2', $entreCalle2_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
          $stmt->bindParam(':horarioEntrega', $horarioEntrega, PDO::PARAM_STR);
          $stmt->bindParam(':horaEntregaEnvio_1', $horaEntregaEnvio_1_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':horaEntregaEnvio_2', $horaEntregaEnvio_2_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':seccion', $seccion, PDO::PARAM_STR);
          $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          $id_ordenCompra = (int) $conexion_admin->lastInsertId();

          // SE INSERTAN LOS DATOS DE PAGO PARA LA ORDEN DE COMPRA EN LA TABLA __ORDEN_COMPRA_DATOS_PAGO
          $noEstadoPago = "1";
          $estadoPago = "Pago pendiente";
          $comprobantePago = 0;
          $oportunidadesComprobante = 3;

          $sql = "INSERT INTO __orden_compra_datos_pago(ordenCompra, clienteID, codigoCliente, metodoPago, noEstadoPago, estadoPago, comprobantePago, oportunidadesComprobante, fechaRegistro) VALUES(:ordenCompra, :clienteID, :codigoCliente, :metodoPago, :noEstadoPago, :estadoPago, :comprobantePago, :oportunidadesComprobante, :fechaRegistro)";
          $stmt = $conexion_admin->prepare($sql);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':metodoPago', $metodoPago, PDO::PARAM_STR);
          $stmt->bindParam(':noEstadoPago', $noEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':comprobantePago', $comprobantePago, PDO::PARAM_INT);
          $stmt->bindParam(':oportunidadesComprobante', $oportunidadesComprobante, PDO::PARAM_INT);
          $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          // SE INSERTAN REGISTROS DE LA COMPRA PARA ALMACEN GUADALAJARA - 1
          if ($unidadesAlmacen_1 > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "1";
            $nombreAlmacen = "Guadalajara";
            $remision_almacen = $remisiones_almacen_1[0]['remision'];

            // ANTERIORES
            /*$importeRemision = (float) $precioProveedor * $unidadesAlmacen_1;*/

            $precio_remision = (float) round($precioMXcomp / 1.32, 2);
            $importeRemision = (float) $precio_remision * $unidadesAlmacen_1;

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $remision_almacen, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $unidadesAlmacen_1, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_1, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "1";
            $nombreAlmacen = "Guadalajara";
            $precioTotalMXcomp = (float) $precioMXcomp * $unidadesAlmacen_1;

            // ANTERIOR
            //$precioUnitarioRemision = (float) $precioProveedor;
            //$monedaRemision = (string) $monedaProveedor;
            //$costoRemison = (float) $precioProveedor * $unidadesAlmacen_1;

            $precioUnitarioRemision = (float) round($precioMXcomp / 1.32, 2);
            $monedaRemision = "MN";
            $costoRemison = (float) $precioUnitarioRemision * $unidadesAlmacen_1;

            $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':nombreMarca', $nombreMarca, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioMXcomp', $precioMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioRemision', $precioUnitarioRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':cantidadComprada', $unidadesAlmacen_1, PDO::PARAM_STR);
            $stmt->bindParam(':precioTotalMXcomp', $precioTotalMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
            $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':costoRemison', $costoRemison, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();
          }

          // SE INSERTAN REGISTROS DE LA COMPRA PARA ALMACEN DF - 7
          if ($unidadesAlmacen_7 > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "7";
            $nombreAlmacen = "DF";
            $remision_almacen = $remisiones_almacen_7[0]['remision'];

            // ANTERIORES
            //$importeRemision = (float) $precioProveedor * $unidadesAlmacen_7;

            $precio_remision = (float) round($precioMXcomp / 1.32, 2);
            $importeRemision = (float) $precio_remision * $unidadesAlmacen_7;

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $remision_almacen, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $unidadesAlmacen_7, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_7, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "7";
            $nombreAlmacen = "DF";
            $precioTotalMXcomp = (float) $precioMXcomp * $unidadesAlmacen_7;

            // ANTERIOR
            //$precioUnitarioRemision = (float) $precioProveedor;
            //$monedaRemision = (string) $monedaProveedor;
            //$costoRemison = (float) $precioProveedor * $unidadesAlmacen_7;

            $precioUnitarioRemision = (float) round($precioMXcomp / 1.32, 2);
            $monedaRemision = "MN";
            $costoRemison = (float) $precioUnitarioRemision * $unidadesAlmacen_7;

            $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':nombreMarca', $nombreMarca, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioMXcomp', $precioMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioRemision', $precioUnitarioRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':cantidadComprada', $unidadesAlmacen_7, PDO::PARAM_STR);
            $stmt->bindParam(':precioTotalMXcomp', $precioTotalMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
            $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':costoRemison', $costoRemison, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();
          }

          // SE INSERTAN REGISTROS DE LA COMPRA PARA ALMACEN PUEBLA - 56
          if ($unidadesAlmacen_56 > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "56";
            $nombreAlmacen = "Puebla";
            $remision_almacen = $remisiones_almacen_56[0]['remision'];

            // ANTERIORES
            //$importeRemision = (float) $precioProveedor * $unidadesAlmacen_56;

            $precio_remision = (float) round($precioMXcomp / 1.32, 2);
            $importeRemision = (float) $precio_remision * $unidadesAlmacen_56;

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $remision_almacen, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $unidadesAlmacen_56, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_56, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "56";
            $nombreAlmacen = "Puebla";
            $precioTotalMXcomp = (float) $precioMXcomp * $unidadesAlmacen_56;

            // ANTERIOR
            //$precioUnitarioRemision = (float) $precioProveedor;
            //$monedaRemision = (string) $monedaProveedor;
            //$costoRemison = (float) $precioProveedor * $unidadesAlmacen_56;

            $precioUnitarioRemision = (float) round($precioMXcomp / 1.32, 2);
            $monedaRemision = "MN";
            $costoRemison = (float) $precioUnitarioRemision * $unidadesAlmacen_56;

            $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':nombreMarca', $nombreMarca, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioMXcomp', $precioMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioRemision', $precioUnitarioRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':cantidadComprada', $unidadesAlmacen_56, PDO::PARAM_STR);
            $stmt->bindParam(':precioTotalMXcomp', $precioTotalMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
            $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':costoRemison', $costoRemison, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();
          }

          // SE INSERTAN REGISTROS DE LA COMPRA PARA ALMACEN LEON - 74
          if ($unidadesAlmacen_74 > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "74";
            $nombreAlmacen = "León";
            $remision_almacen = $remisiones_almacen_74[0]['remision'];

            // ANTERIORES
            //$importeRemision = (float) $precioProveedor * $unidadesAlmacen_74;

            $precio_remision = (float) round($precioMXcomp / 1.32, 2);
            $importeRemision = (float) $precio_remision * $unidadesAlmacen_74;

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $remision_almacen, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $unidadesAlmacen_74, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_74, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "74";
            $nombreAlmacen = "León";
            $precioTotalMXcomp = (float) $precioMXcomp * $unidadesAlmacen_74;

            // ANTERIOR
            //$precioUnitarioRemision = (float) $precioProveedor;
            //$monedaRemision = (string) $monedaProveedor;
            //$costoRemison = (float) $precioProveedor * $unidadesAlmacen_74;

            $precioUnitarioRemision = (float) round($precioMXcomp / 1.32, 2);
            $monedaRemision = "MN";
            $costoRemison = (float) $precioUnitarioRemision * $unidadesAlmacen_74;

            $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':nombreMarca', $nombreMarca, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioMXcomp', $precioMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':precioUnitarioRemision', $precioUnitarioRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':cantidadComprada', $unidadesAlmacen_74, PDO::PARAM_STR);
            $stmt->bindParam(':precioTotalMXcomp', $precioTotalMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
            $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':costoRemison', $costoRemison, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();
          }

          //////////////////////////////////////////////////////////
          // SE RESETEAN LAS VARIABLES DE ERROR DE ORDEN DE COMPRA

          $errorOrdenCompra_existe = false;
          $errorOrdenCompra_mensaje_error = "";
          $errorOrdenCompra_seccion = "";
          $errorOrdenCompra_proveedor = "";
          $errorOrdenCompra_numero_almacen = "";
          $errorOrdenCompra_nombre_almacen = "";
          $errorOrdenCompra_moneda_remision = "";
          $errorOrdenCompra_cliente_id = "";
          $errorOrdenCompra_codigo_cliente = "";
          $errorOrdenCompra_remisiones_eliminar = "";

          $proceso_correcto = true;
        } else {
          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, no fue posible concretar la compra, el proveedor ya no cuenta con existencias para el/los producto(s).";
          $proceso_correcto = false;

          $errorOrdenCompra_existe = true;
          $errorOrdenCompra_mensaje_error = "Orden de compra: Se detectó que no hay productos en el proveedor PCH, se cancela la compra.";
          $errorOrdenCompra_seccion = "Orden de compra (Producto - Depósito/Transferencia)";
          $errorOrdenCompra_proveedor = "----";
          $errorOrdenCompra_numero_almacen = "----";
          $errorOrdenCompra_nombre_almacen = "----";
          $errorOrdenCompra_moneda_remision = "----";
          $errorOrdenCompra_cliente_id = $idUser;
          $errorOrdenCompra_codigo_cliente = $codigoUsuario;
        }
      }

      $conexion_admin->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      $respuesta = "2"; // MENSAJE DE ERROR
      $errorOrdenCompra_mensaje_error .= $error->getMessage();
      $mensaje = $tipo_error;
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE MODIFICAR EXISTENCIAS
  $errorModificarExistencias_existe = false;
  $errorModificarExistencias_mensaje_error = "";
  $errorModificarExistencias_seccion = "";
  $errorModificarExistencias_proveedor = "";
  $errorModificarExistencias_numero_almacen = "";
  $errorModificarExistencias_nombre_almacen = "";
  $errorModificarExistencias_moneda_remision = "";
  $errorModificarExistencias_cliente_id = "";
  $errorModificarExistencias_codigo_cliente = "";
  $errorModificarExistencias_remisiones_eliminar = "";

  // SE MODIFICAN LAS EXISTENCIAS DE CADA ALMACEN PARA EL PRODUCTO COMPRADO
  if ($proceso_correcto) {
    try {
      $existenciaAlmacen_1 = (int) $existenciaAlmacen_1 - $unidadesAlmacen_1;
      $existenciaAlmacen_7 = (int) $existenciaAlmacen_7 - $unidadesAlmacen_7;
      $existenciaAlmacen_56 = (int) $existenciaAlmacen_56 - $unidadesAlmacen_56;
      $existenciaAlmacen_74 = (int) $existenciaAlmacen_74 - $unidadesAlmacen_74;
      $existenciaTotal = (int) $existenciaAlmacen_1 + $existenciaAlmacen_7 + $existenciaAlmacen_56 + $existenciaAlmacen_74;

      if ($existenciaTotal === 0) {
        $activo = 0;
      } else {
        $activo = 1;
      }

      $conexion->beginTransaction();

      $sql = "UPDATE __productos SET existenciaTotal = :existenciaTotal, existenciaAlmacen_1 = :existenciaAlmacen_1, existenciaAlmacen_7 = :existenciaAlmacen_7, existenciaAlmacen_56 = :existenciaAlmacen_56, existenciaAlmacen_74 = :existenciaAlmacen_74, activo = :activo WHERE BINARY sku = :sku";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
      $stmt->bindParam(':existenciaAlmacen_1', $existenciaAlmacen_1, PDO::PARAM_INT);
      $stmt->bindParam(':existenciaAlmacen_7', $existenciaAlmacen_7, PDO::PARAM_INT);
      $stmt->bindParam(':existenciaAlmacen_56', $existenciaAlmacen_56, PDO::PARAM_INT);
      $stmt->bindParam(':existenciaAlmacen_74', $existenciaAlmacen_74, PDO::PARAM_INT);
      $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
      $stmt->bindParam(':sku', $skuProducto, PDO::PARAM_STR);
      $stmt->execute();

      $conexion->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion->rollBack();

      // EN CASO DE HABER UN ERROR EN LA CONSULTA CONSULTA
      $errorModificarExistencias_existe = true;
      $errorModificarExistencias_mensaje_error = "Existencias producto: Se generó la orden de compra, pero las existencias del producto no se modificaron. - " . $error->getMessage();
      $errorModificarExistencias_seccion = "Existencias producto (Producto - Depósito/Transferencia)";
      $errorModificarExistencias_proveedor = "----";
      $errorModificarExistencias_numero_almacen = "----";
      $errorModificarExistencias_nombre_almacen = "----";
      $errorModificarExistencias_moneda_remision = "----";
      $errorModificarExistencias_cliente_id = $idUser;
      $errorModificarExistencias_codigo_cliente = $codigoUsuario;
      $errorModificarExistencias_remisiones_eliminar = "----";
    }

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE MANDAN LOS CORREOS AL USUARIO Y A LAS CUENTAS DE MXCOMP
  if ($proceso_correcto) {
    $no_correos_no_enviados = 0;
    $correos_no_enviados = "";

    $fechaCompra_correo = fecha_con_hora($fechaCompra);

    $array_almacenRemision = [];

    // REMISION ALMACÉN GUADALAJARA - 1
    if (count($remisiones_almacen_1) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Guadalajara',
        'moneda' => $monedaProveedor,
        'remision' => $remisiones_almacen_1[0]['remision'],
      );
    }

    // REMISION ALMACÉN DF - 7
    if (count($remisiones_almacen_7) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'DF',
        'moneda' => $monedaProveedor,
        'remision' => $remisiones_almacen_7[0]['remision'],
      );
    }

    // REMISION ALMACÉN PUEBLA - 56
    if (count($remisiones_almacen_56) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Puebla',
        'moneda' => $monedaProveedor,
        'remision' => $remisiones_almacen_56[0]['remision'],
      );
    }

    // REMISION ALMACÉN LEÓN - 74
    if (count($remisiones_almacen_74) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'León',
        'moneda' => $monedaProveedor,
        'remision' => $remisiones_almacen_74[0]['remision'],
      );
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO CLIENTE DE MXCOMP
    require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/ticket_compra.php';

    $link_detallesCompra = HOST_LINK . "compra-detalles/" . $id_ordenCompra . "/" . $ordenCompra;

    $correo_ticketCompra = new Correo_ticketCompra($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesCompra, $correo_cliente);

    if ($correo_ticketCompra->enviarCorreo()) {
      $proceso_correcto = true;
    } else {
      $proceso_correcto = true;

      $correos_no_enviados = "Cliente";
      $no_correos_no_enviados++;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO PARA USUARIOS INTERNOS DE MXCOMP - MESA DE CONTROL, FINANZAS Y COMPRAS
    if ($proceso_correcto) {
      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/remisiones_mxcomp.php';

      $link_detallesVenta = HOST_LINK_ADMIN . "ordenes-compra/detalles/" . $id_ordenCompra . "/" . $ordenCompra . "/" . $codigoUsuario;

      $correo_remisionesMXcomp = new Correo_remisionesMXcomp($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesVenta, $array_almacenRemision);

      if ($correo_remisionesMXcomp->enviarCorreo()) {
        $proceso_correcto = true;
      } else {
        $proceso_correcto = true;

        if ($no_correos_no_enviados === 0) {
          $correos_no_enviados .= "Internos MXcomp";
        } else {
          $correos_no_enviados .= ", Internos MXcomp";
        }

        $no_correos_no_enviados++;
      }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO PARA DOMICILIO DE ENVIO A PCH
    if ($proceso_correcto) {
      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/domicilioEnvio_PCH.php';

      $correo_domicilioEnvio_PCH = new Correo_domicilioEnvio_PCH(
        $ordenCompra,
        desencriptar_con_clave($calleNumero_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($codigoPostal_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($colonia_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($ciudadMunicipio_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($estado_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($nomDestinatario, $codigoUsuario_encriptado),
        desencriptar_con_clave($telefono, $codigoUsuario_encriptado),
        desencriptar_con_clave($entreCalle1_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($entreCalle2_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($horaEntregaEnvio_1_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($horaEntregaEnvio_2_encriptado, $codigoUsuario_encriptado)
      );

      if ($correo_domicilioEnvio_PCH->enviarCorreo()) {
        $proceso_correcto = true;
      } else {
        $proceso_correcto = true;

        if ($no_correos_no_enviados === 0) {
          $correos_no_enviados .= "Domicilio Envio PCH";
        } else {
          $correos_no_enviados .= ", Domicilio Envio PCH";
        }
      }
    }

    // SI SE MANDARON TODOS LOS CORREOS DE FORMA EXITOSA
    if ($no_correos_no_enviados === 0) {
      $errorOrdenCompra_existe = false;
      $errorOrdenCompra_mensaje_error = "";
      $errorOrdenCompra_seccion = "";
      $errorOrdenCompra_proveedor = "";
      $errorOrdenCompra_numero_almacen = "";
      $errorOrdenCompra_nombre_almacen = "";
      $errorOrdenCompra_moneda_remision = "";
      $errorOrdenCompra_cliente_id = "";
      $errorOrdenCompra_codigo_cliente = "";
      $errorOrdenCompra_remisiones_eliminar = "";
    } else {
      $errorOrdenCompra_existe = true;
      $errorOrdenCompra_mensaje_error = "Correos: Uno o más correos no se enviaron en la orden de compra #" . $ordenCompra . " - Correo de " . $correos_no_enviados;
      $errorOrdenCompra_seccion = "Correos (Producto - Depósito/Transferencia)";
      $errorOrdenCompra_proveedor = "----";
      $errorOrdenCompra_numero_almacen = "----";
      $errorOrdenCompra_nombre_almacen = "----";
      $errorOrdenCompra_moneda_remision = "----";
      $errorOrdenCompra_cliente_id = $idUser;
      $errorOrdenCompra_codigo_cliente = $codigoUsuario;
      $errorOrdenCompra_remisiones_eliminar = "----";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE REGRESA LA URL PORQUE TODO SALIÓ BIEN
  if ($proceso_correcto) {
    $respuesta = "3"; // EXITO
    $link = "estado-compra/1/" . $ordenCompra;

    // ELIMINAR LA VARIABLE DE SESION CON LOS DATOS DEL PRODUCTO COMPRADO
    unset($_SESSION['__seccion_comprar_producto__']);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL GENERAR ALGUNA REMISION O AL HACER UNA CONSULTA, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if ($errorOrdenCompra_existe) {
    try {
      $conexion_admin->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, proveedor, numeroAlmacen, nombreAlmacen, monedaRemision, clienteID, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :proveedor, :numeroAlmacen, :nombreAlmacen, :monedaRemision, :clienteID, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorOrdenCompra_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorOrdenCompra_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':proveedor', $errorOrdenCompra_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $errorOrdenCompra_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorOrdenCompra_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorOrdenCompra_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $errorOrdenCompra_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorOrdenCompra_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorOrdenCompra_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $conexion_admin->commit();
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de las remisiones.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL MODIFICAR LAS EXISTENCIAS, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if ($errorModificarExistencias_existe) {
    try {
      $conexion_admin->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, proveedor, numeroAlmacen, nombreAlmacen, monedaRemision, clienteID, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :proveedor, :numeroAlmacen, :nombreAlmacen, :monedaRemision, :clienteID, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorModificarExistencias_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorModificarExistencias_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':proveedor', $errorModificarExistencias_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $errorModificarExistencias_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorModificarExistencias_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorModificarExistencias_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $errorModificarExistencias_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorModificarExistencias_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorModificarExistencias_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $conexion_admin->commit();
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de modificar las existencias.";
    }
  }

  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'link' => $link,
  ];
  echo json_encode($json);
}
?>