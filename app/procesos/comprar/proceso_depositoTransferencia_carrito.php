<?php
if (isset($_POST['accion']) && $_POST['accion'] === "comprar") {
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  require_once dirname(__DIR__, 2) . '/clases/direcciones/metodos_direcciones.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $proceso_correcto = false;
  $mensaje = "";
  $link = "";
  $correo_cliente = "";

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE VALIDA QUE EL USUARIO EXISTA
  if (validar_campo_numerico($idUser)) {
    $idUser = (int) $idUser;

    // PREGUNTAMOS SI EL USUARIO EXISTE
    try {
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario, correo FROM __usuarios WHERE id = :id AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];

      if ($usuario_existe === 1) {
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];

        // ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);

        $correo = (string) trim($datos_usuario['correo']);
        $correo_cliente = desencriptar($correo);

        $proceso_correcto = true;
      } else {
        $respuesta = "0";
        $mensaje = "El usuario no existe";
        $proceso_correcto = false;
      }

      $stmt = null;
    } catch (PDOException $error) {
      $respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Problema al buscar la información del usuario";
      $proceso_correcto = false;
    }
  } else {
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE OBTIENE LOS DATOS DE POST Y OTRAS VARIABLES
  if ($proceso_correcto) {
    $seccion = trim($_POST['seccion']);
    $metodoPago = trim($_POST['metodo_pago']);
    $metodoEnvio = trim($_POST['metodo_envio']);
    $costoEnvio_almacen_1 = trim($_POST['costo_envio_almacen_1']);
    $costoEnvio_almacen_7 = trim($_POST['costo_envio_almacen_7']);
    $costoEnvio_almacen_56 = trim($_POST['costo_envio_almacen_56']);
    $costoEnvio_almacen_74 = trim($_POST['costo_envio_almacen_74']);
    $costoEnvio_total = trim($_POST['costo_envio_total']);
    $precioPedido = trim($_POST['precio_pedido']);
    $totalPagar = trim($_POST['total_pagar']);

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA TODOS LOS CAMPOS
  if ($proceso_correcto) {
    $op = "";

    // REVISA LA VARIABLE SECCION
    if ($seccion !== "" && validar_campo_letras_espacios($seccion)) {
      $seccion = (string) $seccion;
      $proceso_correcto = true;
    } else {
      $proceso_correcto = false;
      $op = "1";
    }

    // REVISA EL METODO DE PAGO
    if ($proceso_correcto) {
      if ($metodoPago !== "" && validar_campo_letras_espacios_simbolos($metodoPago)) {
        $metodoPago = (string) $metodoPago;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "2";
      }
    }

    // REVISA EL METODO DE ENVIO
    if ($proceso_correcto) {
      if ($metodoEnvio !== "" && validar_campo_letras_espacios_simbolos($metodoEnvio)) {
        $metodoEnvio = (string) $metodoEnvio;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "3";
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 1
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_1 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_1) || validar_campo_letras_espacios($costoEnvio_almacen_1)) {
          $costoEnvio_almacen_1 = (string) $costoEnvio_almacen_1;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "4";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 7
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_7 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_7) || validar_campo_letras_espacios($costoEnvio_almacen_7)) {
          $costoEnvio_almacen_7 = (string) $costoEnvio_almacen_7;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "5";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 56
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_56 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_56) || validar_campo_letras_espacios($costoEnvio_almacen_56)) {
          $costoEnvio_almacen_56 = (string) $costoEnvio_almacen_56;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "6";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO ALMACEN 74
    if ($proceso_correcto) {
      if ($costoEnvio_almacen_74 !== "") {
        if (validar_precio_producto($costoEnvio_almacen_74) || validar_campo_letras_espacios($costoEnvio_almacen_74)) {
          $costoEnvio_almacen_74 = (string) $costoEnvio_almacen_74;
          $proceso_correcto = true;
        } else {
          $proceso_correcto = false;
          $op = "7";
        }
      } else {
        $proceso_correcto = true;
      }
    }

    // REVISA EL COSTO DE ENVIO TOTAL
    if ($proceso_correcto) {
      if ($costoEnvio_total !== "" && validar_precio_producto($costoEnvio_total)) {
        if ($costoEnvio_total === "0.00" || $costoEnvio_total === "0") {
          if ($metodoEnvio === "Paqueteria") {
            $costoEnvio_total = "Envío gratis";
          } else {
            $costoEnvio_total = "0.00";
          }
        } else {
          $costoEnvio_total = (float) $costoEnvio_total;
        }

        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "8";
      }
    }

    // REVISA EL PRECIO DEL PEDIDO
    if ($proceso_correcto) {
      if ($precioPedido !== "" && validar_precio_producto($precioPedido)) {
        $precioPedido = (float) $precioPedido;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "9";
      }
    }

    // REVISA EL TOTAL A PAGAR
    if ($proceso_correcto) {
      if ($totalPagar !== "" && validar_precio_producto($totalPagar)) {
        $totalPagar = (float) $totalPagar;
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
        $op = "10";
      }
    }

    // SI ES FALSE, SE IMPRIME LO SIGUIENTE
    if ($proceso_correcto === false) {
      $respuesta = "1"; // MENSAJE DE INFO
      $mensaje = "Ocurrió un problema, recarga y vuelve a realizar tu compra.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE BUSCAN LOS DATOS DEL DOMICILIO DE ENVIO
  if($proceso_correcto){
    if($metodoEnvio === "Paqueteria"){
      $direccionUsuario = new Direccion($idUser, $codigoUsuario);

      if($direccionUsuario->ver_domicilioEnvio()){
        $direccionCompleta = $direccionUsuario->ver_domEnv_direccionCompleta_encriptado; // ENCRIPTADA
        $nombreDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_encriptado; // ENCRIPTADO
        $telefono = $direccionUsuario->ver_domEnv_noTelefonico_encriptado; // ENCRIPTADO
        $entreCalles = $direccionUsuario->ver_domEnv_entreCalles_encriptado; // ENCRIPTADO
        $referenciasAdicionales = $direccionUsuario->ver_domEnv_referenciasAdicionales_encriptado; // ENCRIPTADO
        $horarioEntrega = $direccionUsuario->ver_domEnv_horarioEntrega; // ENCRIPTADO
        $proceso_correcto = true;
      }else{
        $respuesta = $direccionUsuario->ver_domEnv_respuesta; // MENSAJE DE INFO, TRAE RESPUESTA 1
        $mensaje = $direccionUsuario->ver_domEnv_mensaje;
        $proceso_correcto = false;
      }
    }else{
      $direccionCompleta = NULL;
      $nombreDestinatario = NULL;
      $telefono = NULL;
      $entreCalles = NULL;
      $referenciasAdicionales = NULL;
      $horarioEntrega = NULL;
      $proceso_correcto = true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA SI EXISTE LA SECCION CARRITO
  if ($proceso_correcto) {
    if ($seccion === "carrito") {
      $proceso_correcto = true;
    } else {
      $respuesta = "0";
      $mensaje = "No existe esta seccion";
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE REVISA QUE LOS PRODUCTOS A COMPRAR TENGAN UNIDADES VALIDAS Y EL PRECIO SEA EL MISMO
  $datos_carrito = [];
  $skus_carrito = [];

  if ($proceso_correcto) {
    try {
      $tieneExistencias = '1';
      $guardado = '0';
      $registroExiste = '1';

      $conexion->beginTransaction();

      $sql = "SELECT sku, descripcion, descripcionURL, nombreMarca, precio, moneda, precioProveedor, monedaProveedor, unidades, numeroAlmacen, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __carrito WHERE userID = :userID AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste ORDER BY numeroAlmacen ASC";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':userID', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
      $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
      $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
      $stmt->execute();
      $datos_carrito = $stmt->fetchAll();

      // SE GENERA UN ARRAY CON TODOS LOS SKUS SIN REPETIRSE
      $array_skus = [];
      $array_skus = array_unique(array_column($datos_carrito, 'sku'));

      $contador = 0;
      foreach ($array_skus as $sku_array) {
        $almacen_1 = false;
        $almacen_7 = false;
        $almacen_56 = false;
        $almacen_74 = false;
        $almacen_1_unidades = 0;
        $almacen_7_unidades = 0;
        $almacen_56_unidades = 0;
        $almacen_74_unidades = 0;
        $descripcion = "";
        $descripcionURL = "";
        $precio = 0.00;
        $proveedor = "";
        $unidades_comprar = 0;

        foreach ($datos_carrito as $datos_producto) {
          if ($sku_array === $datos_producto['sku']) {
            $descripcion = (string) $datos_producto['descripcion'];
            $descripcionURL = (string) $datos_producto['descripcionURL'];
            $precio = (float) $datos_producto['precio'];

            $unidades = (int) $datos_producto['unidades'];
            $numero_almacen = (int) $datos_producto['numeroAlmacen'];

            switch ($numero_almacen) {
            case 1: // ALMACEN GUADALAJARA
              $almacen_1 = true;
              $almacen_1_unidades = $unidades;
              break;

            case 7: // ALMACEN DF
              $almacen_7 = true;
              $almacen_7_unidades = $unidades;
              break;

            case 56: // ALMACEN PUEBLA
              $almacen_56 = true;
              $almacen_56_unidades = $unidades;
              break;

            case 74: // ALMACEN LEON
              $almacen_74 = true;
              $almacen_74_unidades = $unidades;
              break;
            }

            $unidades_comprar += $unidades;
          }
        }

        $data = array(
          'sku' => $sku_array,
          'descripcion' => $descripcion,
          'descripcionURL' => $descripcionURL,
          'precio' => $precio,
          'almacen_1' => $almacen_1,
          'almacen_1_unidades' => $almacen_1_unidades,
          'almacen_7' => $almacen_7,
          'almacen_7_unidades' => $almacen_7_unidades,
          'almacen_56' => $almacen_56,
          'almacen_56_unidades' => $almacen_56_unidades,
          'almacen_74' => $almacen_74,
          'almacen_74_unidades' => $almacen_74_unidades,
          'unidades_comprar' => $unidades_comprar,
        );

        $skus_carrito[$contador] = $data;
        $contador++;
      }

      ///////////

      // SE RECORRE EL ARRAY OBTENIDO PARA REVISAR QUE LAS UNIDADES DE CADA PRODUCTO SEAN MENOR O IGUAL A LAS EXISTENCIAS DE SU REGISTRO EN LA TABLA __productos
      $proceso_producto_correcto = false;

      foreach ($skus_carrito as $datos_array) {
        $Carrito_sku = (string) $datos_array['sku'];
        $Carrito_precio = (float) $datos_array['precio'];
        $Carrito_bandera_almacen_1 = (bool) $datos_array['almacen_1'];
        $Carrito_bandera_almacen_7 = (bool) $datos_array['almacen_7'];
        $Carrito_bandera_almacen_56 = (bool) $datos_array['almacen_56'];
        $Carrito_bandera_almacen_74 = (bool) $datos_array['almacen_74'];
        $Carrito_almacen_1_unidades = (int) $datos_array['almacen_1_unidades'];
        $Carrito_almacen_7_unidades = (int) $datos_array['almacen_7_unidades'];
        $Carrito_almacen_56_unidades = (int) $datos_array['almacen_56_unidades'];
        $Carrito_almacen_74_unidades = (int) $datos_array['almacen_74_unidades'];

        $sql = "SELECT COUNT(id) AS conteo, precioMXcomp, existenciaAlmacen_1, existenciaAlmacen_7, existenciaAlmacen_56, existenciaAlmacen_74 FROM __productos WHERE sku = :sku AND productoPCH = 1";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':sku', $Carrito_sku, PDO::PARAM_STR);
        $stmt->execute();
        $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
        $producto_existe = (int) $datos_producto['conteo'];

        if ($producto_existe === 1) {
          $Producto_precioMXcomp = (float) trim($datos_producto['precioMXcomp']);
          $Producto_existenciaAlmacen_1 = (int) trim($datos_producto['existenciaAlmacen_1']);
          $Producto_existenciaAlmacen_7 = (int) trim($datos_producto['existenciaAlmacen_7']);
          $Producto_existenciaAlmacen_56 = (int) trim($datos_producto['existenciaAlmacen_56']);
          $Producto_existenciaAlmacen_74 = (int) trim($datos_producto['existenciaAlmacen_74']);

          // REVISAMOS EL PRECIO DEL PRODUCTO
          if ($Carrito_precio === $Producto_precioMXcomp) {
            $proceso_producto_correcto = true;
          } else {
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "El precio de un producto ya no es el mismo, revisa tu pedido y vuelve a realizar tu compra.";
            $proceso_producto_correcto = false;
            break;
          }

          // REVISA LAS UNIDADES DE LOS ALMACENES
          if ($proceso_producto_correcto) {
            // ALMACEN GUADALAJARA
            if ($Carrito_bandera_almacen_1) {
              if ($Carrito_almacen_1_unidades > 0 && $Carrito_almacen_1_unidades <= $Producto_existenciaAlmacen_1) {
                $proceso_producto_correcto = true;
              } else {
                $respuesta = "1"; // MENSAJE DE INFO
                $mensaje = "Algunas unidades exceden las disponibles de un producto en un almacén o es 0, revisa tu pedido y vuelve a realizar tu compra.";
                $proceso_producto_correcto = false;
                break;
              }
            }

            // ALMACEN DF
            if ($proceso_producto_correcto) {
              if ($Carrito_bandera_almacen_7) {
                if ($Carrito_almacen_7_unidades > 0 && $Carrito_almacen_7_unidades <= $Producto_existenciaAlmacen_7) {
                  $proceso_producto_correcto = true;
                } else {
                  $respuesta = "1"; // MENSAJE DE INFO
                  $mensaje = "Algunas unidades exceden las disponibles de un producto en un almacén o es 0, revisa tu pedido y vuelve a realizar tu compra.";
                  $proceso_producto_correcto = false;
                  break;
                }
              }
            }

            // ALMACEN PUEBLA
            if ($proceso_producto_correcto) {
              if ($Carrito_bandera_almacen_56) {
                if ($Carrito_almacen_56_unidades > 0 && $Carrito_almacen_56_unidades <= $Producto_existenciaAlmacen_56) {
                  $proceso_producto_correcto = true;
                } else {
                  $respuesta = "1"; // MENSAJE DE INFO
                  $mensaje = "Algunas unidades exceden las disponibles de un producto en un almacén o es 0, revisa tu pedido y vuelve a realizar tu compra.";
                  $proceso_producto_correcto = false;
                  break;
                }
              }
            }

            // ALMACEN LEON
            if ($proceso_producto_correcto) {
              if ($Carrito_bandera_almacen_74) {
                if ($Carrito_almacen_74_unidades > 0 && $Carrito_almacen_74_unidades <= $Producto_existenciaAlmacen_74) {
                  $proceso_producto_correcto = true;
                } else {
                  $respuesta = "1"; // MENSAJE DE INFO
                  $mensaje = "Algunas unidades exceden las disponibles de un producto en un almacén o es 0, revisa tu pedido y vuelve a realizar tu compra.";
                  $proceso_producto_correcto = false;
                  break;
                }
              }
            }

          }
        } else {
          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, un producto ya no se encuentra en nuestros registros, revisa tu pedido y vuelve a realizar tu compra.";
          $proceso_producto_correcto = false;
          break;
        }
      }

      if ($proceso_producto_correcto) {
        $proceso_correcto = true;
      } else {
        $proceso_correcto = false;
      }

      $conexion->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion->rollBack();
      $respuesta = "2"; // MENSAJE DE ERROR
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Hubo un problema al revisar las existencias de algunos productos en el carrito, vuelve a intentar el proceso de compra. Si este mensaje sigue apareciendo contacta con atención a clientes.";
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE GENERAN ARRAYS PARA CADA ALMACEN DEL CARRITO DIVIDOS POR SU MONEDA
  $array_carritoAlmacen_1_USD = [];
  $array_carritoAlmacen_1_MN = [];
  $array_carritoAlmacen_7_USD = [];
  $array_carritoAlmacen_7_MN = [];
  $array_carritoAlmacen_56_USD = [];
  $array_carritoAlmacen_56_MN = [];
  $array_carritoAlmacen_74_USD = [];
  $array_carritoAlmacen_74_MN = [];

  if ($proceso_correcto) {
    foreach ($datos_carrito as $datos_prod) {
      $monedaProveedor = (string) $datos_prod['monedaProveedor'];
      $numeroAlmacen = (int) $datos_prod['numeroAlmacen'];

      $prod_carritoAlmacen = array(
        'sku' => (string) $datos_prod['sku'],
        'descripcion' => (string) $datos_prod['descripcion'],
        'descripcionURL' => (string) $datos_prod['descripcionURL'],
        'nombreMarca' => (string) $datos_prod['nombreMarca'],
        'precio' => (float) $datos_prod['precio'],
        'moneda' => (string) $datos_prod['moneda'],
        'precioProveedor' => (float) $datos_prod['precioProveedor'],
        'monedaProveedor' => $monedaProveedor,
        'unidades' => (int) $datos_prod['unidades'],
        'numeroAlmacen' => $numeroAlmacen,
        'tieneImagen' => (string) $datos_prod['tieneImagen'],
        'numeroUbicacionImagen' => (string) $datos_prod['numeroUbicacionImagen'],
        'nombreImagen' => (string) $datos_prod['nombreImagen'],
        'versionImagen' => (string) $datos_prod['versionImagen'],
        'remision' => "",
      );

      switch ($numeroAlmacen) {
      case 1: // ALMACEN GUADALAJARA
        if ($monedaProveedor === 'USD') {
          $posicion = count($array_carritoAlmacen_1_USD);
          $array_carritoAlmacen_1_USD[$posicion] = $prod_carritoAlmacen;
        }

        if ($monedaProveedor === 'MN') {
          $posicion = count($array_carritoAlmacen_1_MN);
          $array_carritoAlmacen_1_MN[$posicion] = $prod_carritoAlmacen;
        }
        break;

      case 7: // ALMACEN DF
        if ($monedaProveedor === 'USD') {
          $posicion = count($array_carritoAlmacen_7_USD);
          $array_carritoAlmacen_7_USD[$posicion] = $prod_carritoAlmacen;
        }

        if ($monedaProveedor === 'MN') {
          $posicion = count($array_carritoAlmacen_7_MN);
          $array_carritoAlmacen_7_MN[$posicion] = $prod_carritoAlmacen;
        }
        break;

      case 56: // ALMACEN PUEBLA
        if ($monedaProveedor === 'USD') {
          $posicion = count($array_carritoAlmacen_56_USD);
          $array_carritoAlmacen_56_USD[$posicion] = $prod_carritoAlmacen;
        }

        if ($monedaProveedor === 'MN') {
          $posicion = count($array_carritoAlmacen_56_MN);
          $array_carritoAlmacen_56_MN[$posicion] = $prod_carritoAlmacen;
        }
        break;

      case 74: // ALMACEN LEON
        if ($monedaProveedor === 'USD') {
          $posicion = count($array_carritoAlmacen_74_USD);
          $array_carritoAlmacen_74_USD[$posicion] = $prod_carritoAlmacen;
        }

        if ($monedaProveedor === 'MN') {
          $posicion = count($array_carritoAlmacen_74_MN);
          $array_carritoAlmacen_74_MN[$posicion] = $prod_carritoAlmacen;
        }
        break;
      }
    }

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE REMISIONES Y/O ORDEN DE COMPRA
  $errorOrdenCompra_existe = false;
  $errorOrdenCompra_mensaje_error = "";
  $errorOrdenCompra_seccion = "";
  $errorOrdenCompra_proveedor = "";
  $errorOrdenCompra_numero_almacen = "";
  $errorOrdenCompra_nombre_almacen = "";
  $errorOrdenCompra_moneda_remision = "";
  $errorOrdenCompra_cliente_id = "";
  $errorOrdenCompra_codigo_cliente = "";
  $errorOrdenCompra_remisiones_eliminar = "";

  $acumulacion_remisiones = "";
  $id_ordenCompra = "";

  // SE REGISTRA LA ORDEN DE COMPRA EN SUS RESPECTIVAS TABLAS, SE MANEJA COMO UNA TRANSACCION
  if ($proceso_correcto) {
    $tipo_error = "";
    $proceso_correcto = false; // Para los siguientes pasos se vuelve a resetear a false

    try {
      $conexion_admin->beginTransaction();
      $fechaCompra = date("Y-m-d H:i:s");

      // SE GENERA LA ORDEN DE COMPRA
      $tipo_error = "No se pudo consultar la existencia de las ordenes de compras.";

      $sql = "SELECT COUNT(id) FROM __ordenes_compra";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->execute();
      $ordenesCompras_existen = (int) $stmt->fetchColumn();

      if ($ordenesCompras_existen === 0) {
        $ordenCompra = 1;
      } else {
        $tipo_error = "No se pudo generar la orden de compra.";

        $sql = "SELECT ordenCompra FROM __ordenes_compra ORDER BY id DESC LIMIT 1";
        $stmt = $conexion_admin->prepare($sql);
        $stmt->execute();
        $numeracion = (int) $stmt->fetchColumn();
        $ordenCompra = $numeracion + 1;
      }

      $tipo_error = "";

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // SE GENERAN LAS REMISIONES NECESARIAS PARA LA ORDEN DE COMPRA

      $remisiones_correctas_almacen_1_USD = false; // ALMACEN GUADALAJARA - USD
      $remisiones_correctas_almacen_1_MN = false; // ALMACEN GUADALAJARA - MN
      $remisiones_correctas_almacen_7_USD = false; // ALMACEN DF - USD
      $remisiones_correctas_almacen_7_MN = false; // ALMACEN DF - MN
      $remisiones_correctas_almacen_56_USD = false; // ALMACEN PUEBLA - USD
      $remisiones_correctas_almacen_56_MN = false; // ALMACEN PUEBLA - MN
      $remisiones_correctas_almacen_74_USD = false; // ALMACEN LEON - USD
      $remisiones_correctas_almacen_74_MN = false; // ALMACEN LEON - MN

      $productosComprados_almacen_1_USD = 0; // PRODUCTOS A COMPRAR ALMACEN GUADALAJARA - USD
      $productosComprados_almacen_1_MN = 0; // PRODUCTOS A COMPRAR ALMACEN GUADALAJARA - MN
      $productosComprados_almacen_7_USD = 0; // PRODUCTOS A COMPRAR ALMACEN DF - USD
      $productosComprados_almacen_7_MN = 0; // PRODUCTOS A COMPRAR ALMACEN DF - MN
      $productosComprados_almacen_56_USD = 0; // PRODUCTOS A COMPRAR ALMACEN PUEBLA - USD
      $productosComprados_almacen_56_MN = 0; // PRODUCTOS A COMPRAR ALMACEN PUEBLA - MN
      $productosComprados_almacen_74_USD = 0; // PRODUCTOS A COMPRAR ALMACEN LEON - USD
      $productosComprados_almacen_74_MN = 0; // PRODUCTOS A COMPRAR ALMACEN LEON - MN

      require_once dirname(__DIR__, 2) . '/clases/proveedor/procesos_PCH.php';
      $proveedorPCH = new Proveedor_PCH();

      /////////////////////////////////////////////////////////////////////////////
      // ALMACÉN 1 - GUADALAJARA

      // REMISIÓN PARA LA MONEDA USD
      if (count($array_carritoAlmacen_1_USD) > 0) {
        $posicion = 0;
        $arrayProductosRemision = [];

        foreach ($array_carritoAlmacen_1_USD as $indice => $datos_almacen) {
          $arrayProductosRemision[$posicion] = array(
            "strSku" => (string) $datos_almacen['sku'],
            "iCantidad" => (int) $datos_almacen['unidades'],
          );

          $posicion++;
        }

        $noAlmacen = 1;
        $moneda_remision = "USD";

        if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
          $remision_PCH = (string) $proveedorPCH->remisionPCH;

          if ($remision_PCH !== "") {
            // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
            foreach ($array_carritoAlmacen_1_USD as $indice => $dat_alm) {
              $array_carritoAlmacen_1_USD[$indice]['remision'] = $remision_PCH;

              $productosComprados_almacen_1_USD += (int) $dat_alm['unidades'];
            }

            $acumulacion_remisiones = $remision_PCH;

            $remisiones_correctas_almacen_1_USD = true;
            $proceso_correcto = true;
            $errorOrdenCompra_existe = false;
          } else {
            $errorOrdenCompra_existe = true;

            $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
          }
        } else {
          $errorOrdenCompra_existe = true;

          switch ($proveedorPCH->opcion) {
          case '1': // WEB SERVICES
            $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
            break;

          case '2': // PROVEEDOR
            $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
            break;
          }
        }

        if ($errorOrdenCompra_existe) {
          $remisiones_correctas_almacen_1_USD = false;
          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
          $proceso_correcto = false;

          $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
          $errorOrdenCompra_proveedor = "PCH Mayoreo";
          $errorOrdenCompra_numero_almacen = "1";
          $errorOrdenCompra_nombre_almacen = "Guadalajara";
          $errorOrdenCompra_moneda_remision = $moneda_remision;
          $errorOrdenCompra_cliente_id = $idUser;
          $errorOrdenCompra_codigo_cliente = $codigoUsuario;
          $errorOrdenCompra_remisiones_eliminar = "----";
        }
      } else {
        $remisiones_correctas_almacen_1_USD = true;
        $proceso_correcto = true;
        $errorOrdenCompra_existe = false;
      }

      // REMISIÓN PARA LA MONEDA MN
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_1_MN) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_1_MN as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 1;
          $moneda_remision = "MN";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_1_MN as $indice => $dat_alm) {
                $array_carritoAlmacen_1_MN[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_1_MN += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_1_MN = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_1_MN = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "1";
            $errorOrdenCompra_nombre_almacen = "Guadalajara";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_1_MN = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////////////////
      // ALMACÉN 7 - DF

      // REMISIÓN PARA LA MONEDA USD
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_7_USD) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_7_USD as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 7;
          $moneda_remision = "USD";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_7_USD as $indice => $dat_alm) {
                $array_carritoAlmacen_7_USD[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_7_USD += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_7_USD = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_7_USD = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "7";
            $errorOrdenCompra_nombre_almacen = "DF";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_7_USD = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      // REMISIÓN PARA LA MONEDA MN
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_7_MN) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_7_MN as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 7;
          $moneda_remision = "MN";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_7_MN as $indice => $dat_alm) {
                $array_carritoAlmacen_7_MN[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_7_MN += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_7_MN = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_7_MN = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "7";
            $errorOrdenCompra_nombre_almacen = "DF";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_7_MN = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////////////////
      // ALMACÉN 56 - PUEBLA

      // REMISIÓN PARA LA MONEDA USD
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_56_USD) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_56_USD as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 56;
          $moneda_remision = "USD";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_56_USD as $indice => $dat_alm) {
                $array_carritoAlmacen_56_USD[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_56_USD += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_56_USD = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_56_USD = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "56";
            $errorOrdenCompra_nombre_almacen = "Puebla";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_56_USD = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      // REMISIÓN PARA LA MONEDA MN
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_56_MN) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_56_MN as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 56;
          $moneda_remision = "MN";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_56_MN as $indice => $dat_alm) {
                $array_carritoAlmacen_56_MN[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_56_MN += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_56_MN = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_56_MN = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "56";
            $errorOrdenCompra_nombre_almacen = "Puebla";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_56_MN = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      /////////////////////////////////////////////////////////////////////////////
      // ALMACÉN 74 - LEÓN

      // REMISIÓN PARA LA MONEDA USD
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_74_USD) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_74_USD as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 74;
          $moneda_remision = "USD";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_74_USD as $indice => $dat_alm) {
                $array_carritoAlmacen_74_USD[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_74_USD += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_74_USD = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_74_USD = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "74";
            $errorOrdenCompra_nombre_almacen = "León";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_74_USD = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      // REMISIÓN PARA LA MONEDA MN
      if ($proceso_correcto) {
        if (count($array_carritoAlmacen_74_MN) > 0) {
          $posicion = 0;
          $arrayProductosRemision = [];

          foreach ($array_carritoAlmacen_74_MN as $indice => $datos_almacen) {
            $arrayProductosRemision[$posicion] = array(
              "strSku" => (string) $datos_almacen['sku'],
              "iCantidad" => (int) $datos_almacen['unidades'],
            );

            $posicion++;
          }

          $noAlmacen = 74;
          $moneda_remision = "MN";

          if ($proveedorPCH->crear_remision($noAlmacen, $moneda_remision, $arrayProductosRemision, $ordenCompra)) {
            $remision_PCH = (string) $proveedorPCH->remisionPCH;

            if ($remision_PCH !== "") {
              // SE GUARDA EL NUMERO DE REMISION EN CADA PRODUCTO, PARA SU POSTERIOR USO
              foreach ($array_carritoAlmacen_74_MN as $indice => $dat_alm) {
                $array_carritoAlmacen_74_MN[$indice]['remision'] = $remision_PCH;

                $productosComprados_almacen_74_MN += (int) $dat_alm['unidades'];
              }

              if ($acumulacion_remisiones === "") {
                $acumulacion_remisiones = $remision_PCH;
              } else {
                $acumulacion_remisiones = $acumulacion_remisiones . ", " . $remision_PCH;
              }

              $remisiones_correctas_almacen_74_MN = true;
              $proceso_correcto = true;
              $errorOrdenCompra_existe = false;
            } else {
              $errorOrdenCompra_existe = true;

              $errorOrdenCompra_mensaje_error = "Datos: Se generó una remisión vacía.";
            }
          } else {
            $errorOrdenCompra_existe = true;

            switch ($proveedorPCH->opcion) {
            case '1': // WEB SERVICES
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_webServices;
              break;

            case '2': // PROVEEDOR
              $errorOrdenCompra_mensaje_error = $proveedorPCH->mensajeError_Proveedor;
              break;
            }
          }

          if ($errorOrdenCompra_existe) {
            $remisiones_correctas_almacen_74_MN = false;
            $respuesta = "1"; // MENSAJE DE INFO
            $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";
            $proceso_correcto = false;

            $errorOrdenCompra_seccion = "Remisiones (Carrito - Depósito/Transferencia)";
            $errorOrdenCompra_proveedor = "PCH Mayoreo";
            $errorOrdenCompra_numero_almacen = "74";
            $errorOrdenCompra_nombre_almacen = "León";
            $errorOrdenCompra_moneda_remision = $moneda_remision;
            $errorOrdenCompra_cliente_id = $idUser;
            $errorOrdenCompra_codigo_cliente = $codigoUsuario;

            if ($acumulacion_remisiones === "") {
              $errorOrdenCompra_remisiones_eliminar = "----";
            } else {
              $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
            }
          }
        } else {
          $remisiones_correctas_almacen_74_MN = true;
          $proceso_correcto = true;
          $errorOrdenCompra_existe = false;
        }
      }

      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // SE GUARDAN LOS REGISTROS EN LA TABLA QUE LE CORRESPONDE, POR CADA ALMACEN
      if ($proceso_correcto) {
        $tipo_error = "Se canceló la compra porque surgío un problema durante un proceso, vuelve a generar la compra. Si sigue apareciendo este mensaje, ponte en contacto con atención a clientes.";

        // EN CASO DE HABER UN ERROR EN UNA CONSULTA
        $errorOrdenCompra_existe = true;
        $errorOrdenCompra_mensaje_error = "Orden de compra: Surgió un problema al tratar de ingresar un registro en una tabla. - ";
        $errorOrdenCompra_seccion = "Orden de compra (Carrito - Depósito/Transferencia)";
        $errorOrdenCompra_proveedor = "----";
        $errorOrdenCompra_numero_almacen = "----";
        $errorOrdenCompra_nombre_almacen = "----";
        $errorOrdenCompra_moneda_remision = "----";
        $errorOrdenCompra_cliente_id = $idUser;
        $errorOrdenCompra_codigo_cliente = $codigoUsuario;

        $proceso_correcto = false;

        if ($acumulacion_remisiones === "") {
          $errorOrdenCompra_remisiones_eliminar = "----";
        } else {
          $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones;
        }

        // SE INSERTA LA ORDEN DE COMPRA EN LA TABLA __ORDENES_COMPRA
        $numeroEstadoCompra = "1";
        $estadoCompra = "Pendiente";
        $productosComprados = (int) $productosComprados_almacen_1_USD + $productosComprados_almacen_1_MN + $productosComprados_almacen_7_USD + $productosComprados_almacen_7_MN + $productosComprados_almacen_56_USD + $productosComprados_almacen_56_MN + $productosComprados_almacen_74_USD + $productosComprados_almacen_74_MN;
        $numeroEstadoPago = "1";
        $estadoPago = "Pendiente";
        $monedaPedido = "MXN";

        if ($productosComprados > 0) {
          $sql = "INSERT INTO __ordenes_compra(numeroEstadoCompra, estadoCompra, ordenCompra, clienteID, codigoCliente, productosComprados, totalPagado, precioPedido, monedaPedido, numeroEstadoPago, estadoPago, metodoEnvio, costoEnvio, direccionCompleta, nombreDestinatario, telefono, entreCalles, referenciasAdicionales, horarioEntrega, seccion, fechaCompra) VALUES(:numeroEstadoCompra, :estadoCompra, :ordenCompra, :clienteID, :codigoCliente, :productosComprados, :totalPagado, :precioPedido, :monedaPedido, :numeroEstadoPago, :estadoPago, :metodoEnvio, :costoEnvio, :direccionCompleta, :nombreDestinatario, :telefono, :entreCalles, :referenciasAdicionales, :horarioEntrega, :seccion, :fechaCompra)";
          $stmt = $conexion_admin->prepare($sql);
          $stmt->bindParam(':numeroEstadoCompra', $numeroEstadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':estadoCompra', $estadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':productosComprados', $productosComprados, PDO::PARAM_STR);
          $stmt->bindParam(':totalPagado', $totalPagar, PDO::PARAM_STR);
          $stmt->bindParam(':precioPedido', $precioPedido, PDO::PARAM_STR);
          $stmt->bindParam(':monedaPedido', $monedaPedido, PDO::PARAM_STR);
          $stmt->bindParam(':numeroEstadoPago', $numeroEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':metodoEnvio', $metodoEnvio, PDO::PARAM_STR);
          $stmt->bindParam(':costoEnvio', $costoEnvio_total, PDO::PARAM_STR);
          $stmt->bindParam(':direccionCompleta', $direccionCompleta, PDO::PARAM_STR);
          $stmt->bindParam(':nombreDestinatario', $nombreDestinatario, PDO::PARAM_STR);
          $stmt->bindParam(':telefono', $telefono, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalles', $entreCalles, PDO::PARAM_STR);
          $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
          $stmt->bindParam(':horarioEntrega', $horarioEntrega, PDO::PARAM_STR);
          $stmt->bindParam(':seccion', $seccion, PDO::PARAM_STR);
          $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          $id_ordenCompra = (int) $conexion_admin->lastInsertId();

          // SE INSERTAN LOS DATOS DE PAGO PARA LA ORDEN DE COMPRA EN LA TABLA __ORDEN_COMPRA_DATOS_PAGO
          $noEstadoPago = "1";
          $estadoPago = "Pago pendiente";
          $comprobantePago = 0;
          $oportunidadesComprobante = 3;

          $sql = "INSERT INTO __orden_compra_datos_pago(ordenCompra, clienteID, codigoCliente, metodoPago, noEstadoPago, estadoPago, comprobantePago, oportunidadesComprobante, fechaRegistro) VALUES(:ordenCompra, :clienteID, :codigoCliente, :metodoPago, :noEstadoPago, :estadoPago, :comprobantePago, :oportunidadesComprobante, :fechaRegistro)";
          $stmt = $conexion_admin->prepare($sql);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':metodoPago', $metodoPago, PDO::PARAM_STR);
          $stmt->bindParam(':noEstadoPago', $noEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':comprobantePago', $comprobantePago, PDO::PARAM_INT);
          $stmt->bindParam(':oportunidadesComprobante', $oportunidadesComprobante, PDO::PARAM_INT);
          $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // (USD) - ALMACEN GUADALAJARA - 1

          if (count($array_carritoAlmacen_1_USD) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "1";
            $nombreAlmacen = "Guadalajara";
            $numeroRemision = $array_carritoAlmacen_1_USD[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_1_USD as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "USD";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_1_USD, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_1, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_1_USD as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //  (MN) - ALMACEN GUADALAJARA - 1

          if (count($array_carritoAlmacen_1_MN) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "1";
            $nombreAlmacen = "Guadalajara";
            $numeroRemision = $array_carritoAlmacen_1_MN[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_1_MN as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "MN";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_1_MN, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_1, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_1_MN as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // (USD) - ALMACEN DF - 7

          if (count($array_carritoAlmacen_7_USD) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "7";
            $nombreAlmacen = "DF";
            $numeroRemision = $array_carritoAlmacen_7_USD[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_7_USD as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "USD";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_7_USD, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_7, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_7_USD as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //  (MN) - ALMACEN DF - 7

          if (count($array_carritoAlmacen_7_MN) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "7";
            $nombreAlmacen = "DF";
            $numeroRemision = $array_carritoAlmacen_7_MN[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_7_MN as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "MN";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_7_MN, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_7, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_7_MN as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // (USD) - ALMACEN PUEBLA - 56

          if (count($array_carritoAlmacen_56_USD) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "56";
            $nombreAlmacen = "Puebla";
            $numeroRemision = $array_carritoAlmacen_56_USD[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_56_USD as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "USD";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_56_USD, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_56, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_56_USD as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //  (MN) - ALMACEN PUEBLA - 56

          if (count($array_carritoAlmacen_56_MN) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "56";
            $nombreAlmacen = "Puebla";
            $numeroRemision = $array_carritoAlmacen_56_MN[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_56_MN as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "MN";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_56_MN, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_56, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_56_MN as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // (USD) - ALMACEN LEÓN - 74

          if (count($array_carritoAlmacen_74_USD) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "74";
            $nombreAlmacen = "León";
            $numeroRemision = $array_carritoAlmacen_74_USD[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_74_USD as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "USD";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_74_USD, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_74, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_74_USD as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          //  (MN) - ALMACEN LEÓN - 74

          if (count($array_carritoAlmacen_74_MN) > 0) {
            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $proveedor = "PCH Mayoreo";
            $numeroAlmacen = "74";
            $nombreAlmacen = "León";
            $numeroRemision = $array_carritoAlmacen_74_MN[0]['remision'];

            $importeRemision = 0.00;
            foreach ($array_carritoAlmacen_74_MN as $dat_producto) {
              // ANTERIOR
              //$importeRemision += (float) $dat_producto['precioProveedor'] * $dat_producto['unidades'];

              $precio_remision = (float) round($dat_producto['precio'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $dat_producto['unidades'];
            }

            $monedaRemision = "MN";

            $sql = "INSERT INTO __remisiones(numeroEstadoRemision, estadoRemision, clienteID, codigoCliente, ordenCompra, proveedor, numeroAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, costoEnvio, fechaRegistro) VALUES(:numeroEstadoRemision, :estadoRemision, :clienteID, :codigoCliente, :ordenCompra, :proveedor, :numeroAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :costoEnvio, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $numeroRemision, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprados_almacen_74_MN, PDO::PARAM_STR);
            $stmt->bindParam(':costoEnvio', $costoEnvio_almacen_74, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $id_remision = (int) $conexion_admin->lastInsertId();

            // TABLA DE __remisiones_estados
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";

            $sql = "INSERT INTO __remision_estados(clienteID, codigoCliente, ordenCompra, id_remision, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
            $stmt = $conexion_admin->prepare($sql);
            $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            // TABLA DE __orden_compra_productos
            $monedaRemision = "MN";

            foreach ($array_carritoAlmacen_74_MN as $dat_producto) {
              $Producto_sku = (string) $dat_producto['sku'];
              $Producto_descripcion = (string) $dat_producto['descripcion'];
              $Producto_descripcionURL = (string) $dat_producto['descripcionURL'];
              $Producto_nombreMarca = (string) $dat_producto['nombreMarca'];
              $Producto_precioUnitarioMXcomp = (float) $dat_producto['precio'];
              $Producto_monedaMXcomp = (string) $dat_producto['moneda'];

              // ANTERIOR
              //$Producto_precioUnitarioRemision = (float) $dat_producto['precioProveedor'];

              $Producto_precioUnitarioRemision = (float) round($Producto_precioUnitarioMXcomp / 1.32, 2);

              $Producto_cantidadComprada = (int) $dat_producto['unidades'];
              $Producto_tieneImagen = (string) $dat_producto['tieneImagen'];
              $Producto_numeroUbicacionImagen = (string) $dat_producto['numeroUbicacionImagen'];
              $Producto_nombreImagen = (string) $dat_producto['nombreImagen'];
              $Producto_versionImagen = (string) $dat_producto['versionImagen'];

              $Producto_precioTotalMXcomp = (float) $Producto_precioUnitarioMXcomp * $Producto_cantidadComprada;
              $Producto_costoRemison = (float) $Producto_precioUnitarioRemision * $Producto_cantidadComprada;

              $sql = "INSERT INTO __orden_compra_productos(clienteID, codigoCliente, ordenCompra, id_remision, proveedor, numeroAlmacen, nombreAlmacen, sku, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:clienteID, :codigoCliente, :ordenCompra, :id_remision, :proveedor, :numeroAlmacen, :nombreAlmacen, :sku, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $conexion_admin->prepare($sql);
              $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':id_remision', $id_remision, PDO::PARAM_INT);
              $stmt->bindParam(':proveedor', $proveedor, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':sku', $Producto_sku, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $Producto_precioUnitarioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $Producto_precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $Producto_cantidadComprada, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $Producto_precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $Producto_costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          //////////////////////////////////////////////////////////
          // SE RESETEAN LAS VARIABLES DE ERROR DE ORDEN DE COMPRA

          $errorOrdenCompra_existe = false;
          $errorOrdenCompra_mensaje_error = "";
          $errorOrdenCompra_seccion = "";
          $errorOrdenCompra_proveedor = "";
          $errorOrdenCompra_numero_almacen = "";
          $errorOrdenCompra_nombre_almacen = "";
          $errorOrdenCompra_moneda_remision = "";
          $errorOrdenCompra_cliente_id = "";
          $errorOrdenCompra_codigo_cliente = "";
          $errorOrdenCompra_remisiones_eliminar = "";

          $proceso_correcto = true;
        } else {
          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, no fue posible concretar la compra, el proveedor ya no cuenta con existencias para el/los producto(s).";
          $proceso_correcto = false;

          $errorOrdenCompra_existe = true;
          $errorOrdenCompra_mensaje_error = "Orden de compra: Se detectó que no hay productos en el proveedor PCH, se cancela la compra.";
          $errorOrdenCompra_seccion = "Orden de compra (Carrito - Depósito/Transferencia)";
          $errorOrdenCompra_proveedor = "----";
          $errorOrdenCompra_numero_almacen = "----";
          $errorOrdenCompra_nombre_almacen = "----";
          $errorOrdenCompra_moneda_remision = "----";
          $errorOrdenCompra_cliente_id = $idUser;
          $errorOrdenCompra_codigo_cliente = $codigoUsuario;
        }
      }

      $conexion_admin->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      $respuesta = "2"; // MENSAJE DE ERROR
      $errorOrdenCompra_mensaje_error .= $error->getMessage();
      $mensaje = $tipo_error;
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE MODIFICAR EXISTENCIAS
  $errorModificarExistencias_existe = false;
  $errorModificarExistencias_mensaje_error = "";
  $errorModificarExistencias_seccion = "";
  $errorModificarExistencias_proveedor = "";
  $errorModificarExistencias_numero_almacen = "";
  $errorModificarExistencias_nombre_almacen = "";
  $errorModificarExistencias_moneda_remision = "";
  $errorModificarExistencias_cliente_id = "";
  $errorModificarExistencias_codigo_cliente = "";
  $errorModificarExistencias_remisiones_eliminar = "";

  // SE MODIFICAN LAS EXISTENCIAS DE CADA ALMACEN PARA LOS PRODUCTOS COMPRADOS DEL CARRITO
  if ($proceso_correcto) {
    try {
      $conexion->beginTransaction();

      foreach ($skus_carrito as $datos_array) {
        $Carrito_sku = (string) $datos_array['sku'];
        $Carrito_almacen_1_unidades = (int) $datos_array['almacen_1_unidades'];
        $Carrito_almacen_7_unidades = (int) $datos_array['almacen_7_unidades'];
        $Carrito_almacen_56_unidades = (int) $datos_array['almacen_56_unidades'];
        $Carrito_almacen_74_unidades = (int) $datos_array['almacen_74_unidades'];

        $sql = "SELECT COUNT(id) as conteo, existenciaAlmacen_1, existenciaAlmacen_7, existenciaAlmacen_56, existenciaAlmacen_74 FROM __productos WHERE sku = :sku";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':sku', $Carrito_sku, PDO::PARAM_STR);
        $stmt->execute();
        $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
        $producto_existe = (int) $datos_producto['conteo'];

        if ($producto_existe > 0) {
          // SE OBTIENEN LAS EXISTENCIAS
          $existenciaAlmacen_1 = (int) $datos_producto['existenciaAlmacen_1'];
          $existenciaAlmacen_7 = (int) $datos_producto['existenciaAlmacen_7'];
          $existenciaAlmacen_56 = (int) $datos_producto['existenciaAlmacen_56'];
          $existenciaAlmacen_74 = (int) $datos_producto['existenciaAlmacen_74'];

          // SE MODIFICAN LAS EXISTENCIAS
          $existenciaAlmacen_1 = (int) $existenciaAlmacen_1 - $Carrito_almacen_1_unidades;
          $existenciaAlmacen_7 = (int) $existenciaAlmacen_7 - $Carrito_almacen_7_unidades;
          $existenciaAlmacen_56 = (int) $existenciaAlmacen_56 - $Carrito_almacen_56_unidades;
          $existenciaAlmacen_74 = (int) $existenciaAlmacen_74 - $Carrito_almacen_74_unidades;
          $existenciaTotal = (int) $existenciaAlmacen_1 + $existenciaAlmacen_7 + $existenciaAlmacen_56 + $existenciaAlmacen_74;

          if ($existenciaTotal === 0) {
            $activo = 0;
          } else {
            $activo = 1;
          }

          $sql = "UPDATE __productos SET existenciaTotal = :existenciaTotal, existenciaAlmacen_1 = :existenciaAlmacen_1, existenciaAlmacen_7 = :existenciaAlmacen_7, existenciaAlmacen_56 = :existenciaAlmacen_56, existenciaAlmacen_74 = :existenciaAlmacen_74, activo = :activo WHERE BINARY sku = :sku";
          $stmt = $conexion->prepare($sql);
          $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
          $stmt->bindParam(':existenciaAlmacen_1', $existenciaAlmacen_1, PDO::PARAM_INT);
          $stmt->bindParam(':existenciaAlmacen_7', $existenciaAlmacen_7, PDO::PARAM_INT);
          $stmt->bindParam(':existenciaAlmacen_56', $existenciaAlmacen_56, PDO::PARAM_INT);
          $stmt->bindParam(':existenciaAlmacen_74', $existenciaAlmacen_74, PDO::PARAM_INT);
          $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
          $stmt->bindParam(':sku', $Carrito_sku, PDO::PARAM_STR);
          $stmt->execute();
        }
      }

      $conexion->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion->rollBack();

      // EN CASO DE HABER UN ERROR EN LA CONSULTA CONSULTA
      $errorModificarExistencias_existe = true;
      $errorModificarExistencias_mensaje_error = "Existencias producto: Se generó la orden de compra, pero las existencias de algún producto no se modificaron. - " . $error->getMessage();
      $errorModificarExistencias_seccion = "Existencias producto (Carrito - Depósito/Transferencia)";
      $errorModificarExistencias_proveedor = "----";
      $errorModificarExistencias_numero_almacen = "----";
      $errorModificarExistencias_nombre_almacen = "----";
      $errorModificarExistencias_moneda_remision = "----";
      $errorModificarExistencias_cliente_id = $idUser;
      $errorModificarExistencias_codigo_cliente = $codigoUsuario;
      $errorModificarExistencias_remisiones_eliminar = "----";
    }

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE ELIMINAR PRODUCTOS DEL CARRITO
  $errorEliminarProductosCarrito_existe = false;
  $errorEliminarProductosCarrito_mensaje_error = "";
  $errorEliminarProductosCarrito_seccion = "";
  $errorEliminarProductosCarrito_proveedor = "";
  $errorEliminarProductosCarrito_numero_almacen = "";
  $errorEliminarProductosCarrito_nombre_almacen = "";
  $errorEliminarProductosCarrito_moneda_remision = "";
  $errorEliminarProductosCarrito_cliente_id = "";
  $errorEliminarProductosCarrito_codigo_cliente = "";
  $errorEliminarProductosCarrito_remisiones_eliminar = "";

  // SE ELIMINAN LOS PRODUCTOS COMPRADOS DEL CARRITO DEL USUARIO
  if ($proceso_correcto) {
    try {
      $conexion->beginTransaction();

      $tieneExistencias = '1';
      $guardado = '0';
      $registroExiste = '1';

      $sql = "DELETE FROM __carrito WHERE userID = :userID AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':userID', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
      $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
      $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
      $stmt->execute();

      $conexion->commit();
      $stmt = null;
    } catch (PDOException $error) {
      $conexion->rollBack();

      // EN CASO DE HABER UN ERROR EN LA CONSULTA CONSULTA
      $errorEliminarProductosCarrito_existe = true;
      $errorEliminarProductosCarrito_mensaje_error = "Eliminación productos: Se generó la orden de compra, pero no se eliminaron los productos del carrito. - " . $error->getMessage();
      $errorEliminarProductosCarrito_seccion = "Eliminación productos (Carrito - Depósito/Transferencia)";
      $errorEliminarProductosCarrito_proveedor = "----";
      $errorEliminarProductosCarrito_numero_almacen = "----";
      $errorEliminarProductosCarrito_nombre_almacen = "----";
      $errorEliminarProductosCarrito_moneda_remision = "----";
      $errorEliminarProductosCarrito_cliente_id = $idUser;
      $errorEliminarProductosCarrito_codigo_cliente = $codigoUsuario;
      $errorEliminarProductosCarrito_remisiones_eliminar = "----";
    }

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE MANDAN LOS CORREOS AL USUARIO Y A LAS CUENTAS DE MXCOMP
  if ($proceso_correcto) {
    $no_correos_no_enviados = 0;
    $correos_no_enviados = "";

    $fechaCompra_correo = fecha_con_hora($fechaCompra);

    $array_almacenRemision = [];

    ////////////////////////////////////////////////////////
    // ALMACÉN 1 - GUADALAJARA

    // MONEDA USD
    if (count($array_carritoAlmacen_1_USD) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Guadalajara',
        'moneda' => 'USD',
        'remision' => $array_carritoAlmacen_1_USD[0]['remision'],
      );
    }

    // MONEDA MN
    if (count($array_carritoAlmacen_1_MN) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Guadalajara',
        'moneda' => 'MN',
        'remision' => $array_carritoAlmacen_1_MN[0]['remision'],
      );
    }

    ////////////////////////////////////////////////////////
    // ALMACÉN 7 - DF

    // MONEDA USD
    if (count($array_carritoAlmacen_7_USD) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'DF',
        'moneda' => 'USD',
        'remision' => $array_carritoAlmacen_7_USD[0]['remision'],
      );
    }

    // MONEDA MN
    if (count($array_carritoAlmacen_7_MN) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'DF',
        'moneda' => 'MN',
        'remision' => $array_carritoAlmacen_7_MN[0]['remision'],
      );
    }

    ////////////////////////////////////////////////////////
    // ALMACÉN 56 - PUEBLA

    // MONEDA USD
    if (count($array_carritoAlmacen_56_USD) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Puebla',
        'moneda' => 'USD',
        'remision' => $array_carritoAlmacen_56_USD[0]['remision'],
      );
    }

    // MONEDA MN
    if (count($array_carritoAlmacen_56_MN) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'Puebla',
        'moneda' => 'MN',
        'remision' => $array_carritoAlmacen_56_MN[0]['remision'],
      );
    }

    ////////////////////////////////////////////////////////
    // ALMACÉN 74 - LEÓN

    // MONEDA USD
    if (count($array_carritoAlmacen_74_USD) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'León',
        'moneda' => 'USD',
        'remision' => $array_carritoAlmacen_74_USD[0]['remision'],
      );
    }

    // MONEDA MN
    if (count($array_carritoAlmacen_74_MN) > 0) {
      $posicion = count($array_almacenRemision);

      $array_almacenRemision[$posicion] = array(
        'almacen' => 'León',
        'moneda' => 'MN',
        'remision' => $array_carritoAlmacen_74_MN[0]['remision'],
      );
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO CLIENTE DE MXCOMP
    require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/ticket_compra.php';

    $link_detallesCompra = HOST_LINK . "compra-detalles/" . $id_ordenCompra . "/" . $ordenCompra;

    $correo_ticketCompra = new Correo_ticketCompra($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesCompra, $correo_cliente);

    if ($correo_ticketCompra->enviarCorreo()) {
      $proceso_correcto = true;
    } else {
      $proceso_correcto = true;

      $correos_no_enviados = "Cliente";
      $no_correos_no_enviados++;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO PARA USUARIOS INTERNOS DE MXCOMP - MESA DE CONTROL, FINANZAS Y COMPRAS
    if ($proceso_correcto) {
      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/remisiones_mxcomp.php';

      $link_detallesVenta = HOST_LINK_ADMIN . "ordenes-compra/detalles/" . $id_ordenCompra . "/" . $ordenCompra . "/" . $codigoUsuario;

      $correo_remisionesMxcomp = new Correo_remisionesMXcomp($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesVenta, $array_almacenRemision);

      if ($correo_remisionesMxcomp->enviarCorreo()) {
        $proceso_correcto = true;
      } else {
        $proceso_correcto = true;

        if ($no_correos_no_enviados === 0) {
          $correos_no_enviados .= "Internos MXcomp";
        } else {
          $correos_no_enviados .= ", Internos MXcomp";
        }
      }
    }

    // SI SE MANDARON TODOS LOS CORREOS DE FORMA EXITOSA
    if ($no_correos_no_enviados === 0) {
      $errorOrdenCompra_existe = false;
      $errorOrdenCompra_mensaje_error = "";
      $errorOrdenCompra_seccion = "";
      $errorOrdenCompra_proveedor = "";
      $errorOrdenCompra_numero_almacen = "";
      $errorOrdenCompra_nombre_almacen = "";
      $errorOrdenCompra_moneda_remision = "";
      $errorOrdenCompra_cliente_id = "";
      $errorOrdenCompra_codigo_cliente = "";
      $errorOrdenCompra_remisiones_eliminar = "";
    } else {
      $errorOrdenCompra_existe = true;
      $errorOrdenCompra_mensaje_error = "Correos: Uno o más correos no se enviaron en la orden de compra #" . $ordenCompra . " - Correo de " . $correos_no_enviados;
      $errorOrdenCompra_seccion = "Correos (Carrito - Depósito/Transferencia)";
      $errorOrdenCompra_proveedor = "----";
      $errorOrdenCompra_numero_almacen = "----";
      $errorOrdenCompra_nombre_almacen = "----";
      $errorOrdenCompra_moneda_remision = "----";
      $errorOrdenCompra_cliente_id = $idUser;
      $errorOrdenCompra_codigo_cliente = $codigoUsuario;
      $errorOrdenCompra_remisiones_eliminar = "----";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE REGRESA LA URL PORQUE TODO SALIÓ BIEN
  if ($proceso_correcto) {
    $respuesta = "3"; // EXITO
    $link = "estado-compra/1/" . $ordenCompra;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL GENERAR ALGUNA REMISION O AL HACER UNA CONSULTA, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if ($errorOrdenCompra_existe) {
    try {
      $conexion_admin->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, proveedor, numeroAlmacen, nombreAlmacen, monedaRemision, clienteID, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :proveedor, :numeroAlmacen, :nombreAlmacen, :monedaRemision, :clienteID, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorOrdenCompra_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorOrdenCompra_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':proveedor', $errorOrdenCompra_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $errorOrdenCompra_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorOrdenCompra_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorOrdenCompra_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $errorOrdenCompra_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorOrdenCompra_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorOrdenCompra_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $conexion_admin->commit();
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de las remisiones.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL MODIFICAR LAS EXISTENCIAS, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if ($errorModificarExistencias_existe) {
    try {
      $conexion_admin->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, proveedor, numeroAlmacen, nombreAlmacen, monedaRemision, clienteID, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :proveedor, :numeroAlmacen, :nombreAlmacen, :monedaRemision, :clienteID, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorModificarExistencias_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorModificarExistencias_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':proveedor', $errorModificarExistencias_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $errorModificarExistencias_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorModificarExistencias_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorModificarExistencias_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $errorModificarExistencias_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorModificarExistencias_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorModificarExistencias_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $conexion_admin->commit();
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de modificar las existencias.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL ELIMINAR LOS PRODUCTOS DEL CARRITO, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if ($errorEliminarProductosCarrito_existe) {
    try {
      $conexion_admin->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, proveedor, numeroAlmacen, nombreAlmacen, monedaRemision, clienteID, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :proveedor, :numeroAlmacen, :nombreAlmacen, :monedaRemision, :clienteID, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorEliminarProductosCarrito_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorEliminarProductosCarrito_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':proveedor', $errorEliminarProductosCarrito_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $errorEliminarProductosCarrito_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorEliminarProductosCarrito_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorEliminarProductosCarrito_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $errorEliminarProductosCarrito_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorEliminarProductosCarrito_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorEliminarProductosCarrito_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $conexion_admin->commit();
    } catch (PDOException $error) {
      $conexion_admin->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de eliminar los productos del carrito.";
    }
  }

  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'link' => $link,
  ];
  echo json_encode($json);
}
?>