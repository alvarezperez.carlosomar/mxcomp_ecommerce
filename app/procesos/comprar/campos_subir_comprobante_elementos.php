<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'mostrar'){
  $respuesta = '1';
  $mensaje = '
    <div class="p-cuenta-contenedor_botones">
      <button type="submit" class="p-button p-button_info" style="margin-bottom: 0;" title="Seleccionar comprobante de pago">
        <span>
          <i class="fas fa-file-upload"></i>
        </span>
        <span><b>Subir comprobante</b></span>
      </button>
    </div>

    <div id="id-comprobante-subir_contenedor_loading" style="display: none; margin-top: 1rem;">
      <div class="p-loading-general p-comprobante-loading" id="id-comprobante-subiendo_loading">
        <div></div>
        <p><b>Subiendo, no recargues la página</b></p>
      </div>
    </div>

    <div id="id-comprobante-subir_contenedor_span_ok" style="display: none;">
      <label class="p-text p-text-span_compra_detalles p-text-span_compra_detalles_ok" style="color: #188B69;">
        <span>El archivo se subió correctamente.</span>
        <span><i class="fas fa-check-circle"></i></span>
      </label>
    </div>

    <div id="id-comprobante-subir_contenedor_span_error" style="display: none;"></div>';

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>