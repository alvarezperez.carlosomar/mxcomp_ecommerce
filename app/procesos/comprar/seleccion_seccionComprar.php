<?php
if(isset($_POST['seccion']) && $_POST['seccion'] === "producto"){
  session_start();
  $_SESSION['seccion'] = "producto";

  $_SESSION['__seccion_comprar_producto__'][0] = array(
    'sku' => trim($_POST['sku']),
    'unidades_almacen_1' => trim($_POST['unidades_Almacen_1']),
    'unidades_almacen_7' => trim($_POST['unidades_Almacen_7']),
    'unidades_almacen_56' => trim($_POST['unidades_Almacen_56']),
    'unidades_almacen_74' => trim($_POST['unidades_Almacen_74'])
  );
  
  $json = [
    'respuesta' => "1",
    'mensaje' => "1"   // OPCION PARA URL AMIGABLE DE COMPRAR PRODUCTO
  ];
  echo json_encode($json);
}

if(isset($_POST['seccion']) && $_POST['seccion'] === "carrito"){
  session_start();
  $_SESSION['seccion'] = "carrito";
  
  $json = [
    'respuesta' => "1",
    'mensaje' => "2"   // OPCION PARA URL AMIGABLE DE COMPRAR CARRITO
  ];
  echo json_encode($json);
}
?>