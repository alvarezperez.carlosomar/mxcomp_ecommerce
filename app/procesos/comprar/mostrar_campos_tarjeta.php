<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  $respuesta = "1";
  
  $html = '
    <h4>TARJETA DE CRÉDITO / DÉBITO</h4>
    <div class="p-comprar-modal_cont_campos">
      <p class="p-text p-text_help p-text_help_margin">
        <span>Ingresa los datos de la tarjeta, no se almacena esta información, sólo se procesará el pago.</span>
      </p>
      
      <div class="p-pasarela-pagos_contenedor">
        <div class="p-pasarela-pagos_elementos" style="margin-bottom: 1rem;">
          <label class="p-label p-text_p">
            <span>Tarjetas aceptadas:</span>
          </label>

          <div class="p-pasarela-pagos_contenedor_tarjetas">
            <div class="p-pasarela-pagos_tarjeta_imagen" title="Visa">
              <i class="p-pasarela-pagos_tarjeta_icon fab fa-cc-visa" style="color: #1A1F71;"></i>
            </div>

            <div class="p-pasarela-pagos_tarjeta_imagen" title="Mastercard">
              <i class="p-pasarela-pagos_tarjeta_icon fab fa-cc-mastercard" style="color: #F2F2F2;"></i>
              <span class="p-pasarela-pagos_tarjeta_span_mastercard"></span>
            </div>

            <div class="p-pasarela-pagos_tarjeta_imagen" title="American Express">
              <i class="p-pasarela-pagos_tarjeta_icon fab fa-cc-amex" style="color: #016FD0;"></i>
            </div>

            <div class="p-pasarela-pagos_tarjeta_imagen" title="Carnet">
              <img src="images/tarjetas/logo_carnet.svg" style="position: relative; top: -1px; width: 39px;" alt="Logo Carnet">
            </div>
          </div>
        </div>

        <div class="p-loading-general p-producto-loading" id="id-pasarela-pagos_loading" style="padding: 0;">
          <div></div>
        </div>
        
        <div class="p-notification_contenedor p-margin-top_1rem" id="id-pasarela-pagos_alert_success">
          <div class="p-notification p-notification_letter_success">
            <span>
              <i class="fas fa-check-circle"></i>
            </span>
            <span class="p-notification_p">
              <b>Los datos de la tarjeta son válidos.</b>
            </span>
          </div>
        </div>

        <div class="p-notification_contenedor p-margin-top_1rem" id="id-pasarela-pagos_alert_info">
          <div class="p-notification p-notification_letter_info">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="p-notification_p">
              <b>Sólo se aceptan tarjetas Visa, Visa Electron, Mastercard, American Express y Carnet.</b>
            </span>
          </div>
        </div>

        <div class="p-notification_contenedor p-margin-top_1rem" id="id-pasarela-pagos_alert_error">
          <div class="p-notification p-notification_letter_error">
            <span>
              <i class="fas fa-times-circle"></i>
            </span>
            <span class="p-notification_p" id="id-pasarela-pagos_alert_error_span"></span>
          </div>
        </div>

        <div class="p-columnas p-field_marginBottom">
          <div class="p-columna">
            <div class="p-field">
              <label class="p-label p-text_p">
                <span>Nombre del tarjetahabiente:</span>
                <span class="p-text p-text_help">*</span>
              </label>

              <p class="p-text p-text_help">
                <span>Máximo 80 caracteres.</span>
              </p>

              <div class="p-control">
                <input type="text" class="p-input g-tarjeta-nombre_input" placeholder="Nombre del tarjetahabiente" autocomplete="off" maxlength="80" style="text-transform: uppercase;">
                <label class="p-label p-label_tipo_tarjeta" style="margin-left: .5rem;">
                  <span class="g-tarjeta-nombre_contador_span">0</span>
                </label>
              </div>

              <p class="p-text p-text_info g-tarjeta-nombre_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo letras y espacios son permitidos.</span>
              </p>
              <p class="p-text p-text_error g-tarjeta-nombre_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>El campo se encuentra vacío.</span>
              </p>
            </div>
          </div>
        </div>

        <div class="p-columnas p-field_marginBottom">
          <div class="p-columna">
            <div class="p-field">
              <label class="p-label p-text_p">
                <span>Número de la tarjeta:</span>
                <span class="p-text p-text_help">*</span>
              </label>

              <div class="p-control">
                <input type="text" class="p-input g-input-click g-tarjeta-numero_input" placeholder="Número de la tarjeta" autocomplete="off" maxlength="19">
              </div>

              <p class="p-text p-text_info g-tarjeta-numero_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>El número de la tarjeta sólo debe contener números, está incompleto o es incorrecto.</span>
              </p>
              <p class="p-text p-text_error g-tarjeta-numero_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>El campo se encuentra vacío.</span>
              </p>
            </div>
          </div>
        </div>

        <div class="p-columnas p-field_marginBottom">
          <div class="p-columna">
            <div class="p-field">
              <label class="p-label p-text_p">
                <span>Fecha de expiración:</span>
                <span class="p-text p-text_help">*</span>
              </label>

              <div class="p-control">
                <input type="text" class="p-input g-input-click g-tarjeta-mes_exp_input" placeholder="MM" style="max-width: 80px; margin-right: 5px;" autocomplete="off" maxlength="2">
                <label class="p-label p-label_tipo_tarjeta">/</label>
                <input type="text" class="p-input g-input-click g-tarjeta-anio_exp_input" placeholder="AA o AAAA" style="max-width: 120px; margin-left: 5px;" autocomplete="off" maxlength="4">
              </div>

              <p class="p-text p-text_info g-tarjeta-mes_exp_alert_info_1">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo números en el mes son permitidos.</span>
              </p>
              <p class="p-text p-text_info g-tarjeta-mes_exp_alert_info_2">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>El mes debe de ser de 2 dígitos.</span>
              </p>
              <p class="p-text p-text_info g-tarjeta-anio_exp_alert_info_1">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo números en el año son permitidos.</span>
              </p>
              <p class="p-text p-text_info g-tarjeta-anio_exp_alert_info_2">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>El año debe de ser de 2 o 4 dígitos.</span>
              </p>
              <p class="p-text p-text_info g-tarjeta-fecha_exp_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>La fecha de expiración no es válida, revisa el mes y/o el año.</span>
              </p>
              <p class="p-text p-text_error g-tarjeta-mes_exp_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>El campo del mes se encuentra vacío.</span>
              </p>
              <p class="p-text p-text_error g-tarjeta-anio_exp_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>El campo del año se encuentra vacío.</span>
              </p>
            </div>
          </div>
        </div>

        <div class="p-columnas p-field_marginBottom">
          <div class="p-columna">
            <div class="p-field">
              <label class="p-label p-text_p">
                <span>Código de seguridad (CVC):</span>
                <span class="p-text p-text_help">*</span>
              </label>

              <div class="p-control">
                <input type="text" class="p-input g-input-click g-tarjeta-cvc_input" placeholder="CVC" style="max-width: 140px; margin-right: 1rem;" autocomplete="off" maxlength="4">
                <img src="images/tarjetas/cvc_1.png" alt="CVC 1" style="margin-right: .5rem;">
                <img src="images/tarjetas/cvc_2.png" alt="CVC 2">
              </div>

              <p class="p-text p-text_info g-tarjeta-cvc_alert_info_1">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>El código de seguridad no es válido, revísalo por favor.</span>
              </p>
              <p class="p-text p-text_info g-tarjeta-cvc_alert_info_2">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>El código de seguridad sólo puede ser de 3 o 4 dígitos.</span>
              </p>
              <p class="p-text p-text_error g-tarjeta-cvc_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>Este campo se encuentra vacío.</span>
              </p>
            </div>
          </div>
        </div>

        <div class="p-field_marginTop p-buttons p-buttons_right p-comprar-modal_derecho_buttons">
          <button class="p-button p-button_success" id="id-confirmar-pago">
            <span>
              <i class="fas fa-dollar-sign"></i>
            </span>
            <span><b>Pagar compra</b></span>
          </button>
          <a class="p-button p-button_delete" id="id-comprar-modal_button_cancelar">
            <span>
              <i class="fas fa-times"></i>
            </span>
            <span><b>Cancelar</b></span>
          </a>
        </div>
      </div>
    </div>';
  
  $mensaje = $html;
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>