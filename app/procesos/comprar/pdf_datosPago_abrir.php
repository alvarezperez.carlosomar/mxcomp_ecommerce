<?php 
$dirname = dirname(__DIR__, 3);

require $dirname . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable($dirname);
$dotenv->load();

// Plantilla de datos
require('pdf_datosPago_crear.php');

// CSS de plantilla
$css = file_get_contents(dirname(__DIR__, 2) . '/styles/pdf_datos.min.css');

$mpdf = new \Mpdf\Mpdf([
  'utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0
]);

$html = gethtml();

$mpdf->writeHtml($css, \Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->writeHtml($html, \Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->Output("mxcomp_datos_pago.pdf", "I");
?>