<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/clases/usuario/metodos_usuario.php';
  require_once dirname(__DIR__, 2) . '/clases/direcciones/metodos_direcciones.php';

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $proceso_exitoso = false;
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario = new Usuario($idUsuario, $codigoUsuario);
    $direccionUsuario = new Direccion($idUsuario, $codigoUsuario);
    $proceso_exitoso = true;
  }else{
    $mensaje = "No es numérico";
    $proceso_exitoso = false;
  }
  
  if($proceso_exitoso){
    if($usuario->buscarUsuario()){
      $proceso_exitoso = true;
    }else{
      $mensaje = $usuario->buscarUsuario_mensaje;
      $proceso_exitoso = false;
    }
  }

  if($proceso_exitoso){
    if($direccionUsuario->ver_domicilioEnvio()){
      $domiEnvio_nombreDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_desencriptado;
      $domiEnvio_noTelefonico = $direccionUsuario->ver_domEnv_noTelefonico_desencriptado;
      $domiEnvio_direccionCompleta = $direccionUsuario->ver_domEnv_direccionCompleta_desencriptado;

      $html = '
  <p class="p-comprar-texto-domicilio p-margin-bottom_1rem">
    <span class="p-margin-bottom_1rem">' . $domiEnvio_direccionCompleta . '</span>

    <span><b>Recibe:</b> ' . $domiEnvio_nombreDestinatario . '</span>';

      if(!is_null($direccionUsuario->ver_domEnv_entreCalle1) && !is_null($direccionUsuario->ver_domEnv_entreCalle2)){
        $html .= '
    <span><b>Entre calles:</b> ' . $direccionUsuario->ver_domEnv_entreCalle1 . ' y ' . $direccionUsuario->ver_domEnv_entreCalle2 . '</span>';
      }

      if(!is_null($direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado)){
        $html .= '
    <span><b>Referencias adicionales:</b> ' . $direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado . '</span>';
      }

      $html .= '
    <span><b>Tel.:</b> ' . $domiEnvio_noTelefonico . '</span>';

      if(!is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio1) && !is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio2)){
        $html .= '
    <span><b>Horario de entrega:</b> De ' . hora($direccionUsuario->ver_domEnv_horaEntregaEnvio1) . ' a ' . hora($direccionUsuario->ver_domEnv_horaEntregaEnvio2) . '</span>';
      }else{
        $html .= '
    <span><b>Horario de entrega:</b> No está establecido</span>';
      }

      $html .= '
    </p>';

      $mensaje = $html;
    }else{
      $mensaje = $direccionUsuario->ver_domEnv_mensaje;
    }
  }

  $respuesta = "1";

  unset($usuarioClases);
  unset($direccionUsuario);
  unset($_POST['accion']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>