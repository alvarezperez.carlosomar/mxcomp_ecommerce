<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'enviar'){
  session_start();
  $_SESSION['pdf_datos'] = [
    'orden_compra' => (string) trim($_POST['orden_compra']),
    'pago_compra' => (float) trim($_POST['pago_compra'])
  ];

  $respuesta = '1';

  $json = [ 'respuesta' => $respuesta ];
  echo json_encode($json);
}
?>