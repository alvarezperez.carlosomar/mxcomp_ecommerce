<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'mostrar'){
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  $ordenCompra = desencriptar(trim($_POST['orden_compra']));
  $codigoUsuario = desencriptar(trim($_POST['codigo_usuario']));
  $mensaje = '';

  $Conn_Admin = new Conexion_admin();

  try{
    $sql = "SELECT metodoPago, numeroEstadoPago, datosPago, comprobantePago, ubicacionComprobantePago, nombreComprobante, esImagen, oportunidadesComprobante, comprobanteRechazado, fechaProceso FROM __orden_compra_datos_pago WHERE ordenCompra = :ordenCompra AND codigoCliente = :codigoCliente";
    $stmt = $Conn_Admin->pdo->prepare($sql);
    $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
    $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
    $stmt->execute();
    $datos_ordComp_pago = $stmt->fetch(PDO::FETCH_ASSOC);

    $metodoPago = (string) trim($datos_ordComp_pago['metodoPago']);
    $numeroEstadoPago = (int) trim($datos_ordComp_pago['numeroEstadoPago']);
    $datosPago = (string) trim($datos_ordComp_pago['datosPago']);
    $comprobantePago = (int) trim($datos_ordComp_pago['comprobantePago']);
    $ubicacionComprobantePago = (string) trim($datos_ordComp_pago['ubicacionComprobantePago']);
    $nombreComprobante = (string) trim($datos_ordComp_pago['nombreComprobante']);
    $esImagen = (string) trim($datos_ordComp_pago['esImagen']);
    $oportunidadesComprobante = (int) trim($datos_ordComp_pago['oportunidadesComprobante']);
    $comprobanteRechazado = (int) trim($datos_ordComp_pago['comprobanteRechazado']);
    $fechaProceso = (string) trim($datos_ordComp_pago['fechaProceso']);

    $mensaje .= '
        <p class="p-text_p">
          <span>El método elegido fue <b>' . $metodoPago . '</b>.</span>
        </p>';

    if($metodoPago === 'Tarjeta'){
      $mensaje .= '
        <p class="p-text_p">
          <span>Se pagó por medio de una <b>' . $datosPago . '</b>.</span>
        </p>';
    }

    $dirname = '../../';
    switch($numeroEstadoPago){
      case 1: // PENDIENTE
        if($comprobantePago === 0){
          if($comprobanteRechazado === 1){
            $url_archivo = $ubicacionComprobantePago.$nombreComprobante;

            // Revisa si existe el comprobante de pago
            if(file_exists($dirname . $url_archivo)){
              // Si existe, se elimina
              unlink($dirname . $url_archivo);
            }

            $mensaje .= '
        <div class="p-notification p-notification_letter_info">
          <span>
            <i class="fas fa-info-circle"></i>
          </span>
          <span class="p-notification_p">Tu comprobante de pago fue rechazado.</span>
        </div>';
          }

          $mensaje .= '
        <input type="hidden" value="' . encriptar($oportunidadesComprobante) . '" id="id-comprar-oportunidades_comprobante">';

          if($oportunidadesComprobante < 3){
            $mensaje .= '
        <div class="p-notification p-notification_letter_info">
          <span>
            <b>Por favor súbelo de nuevo. Te ' . ( $oportunidadesComprobante === 1 ? 'queda ' . $oportunidadesComprobante . ' oportunidad' : 'quedan ' . $oportunidadesComprobante . ' oportunidades' ) . ', después la compra será cancelada.</b>
          </span>
        </div>';
          }

          $mensaje .= '
        <form id="id-comprobante-subir_archivo_form" method="POST" enctype="multipart/form-data">
          <div class="p-field_margin_comprobante" style="margin-bottom: .5rem;">
            <label class="p-label">Comprobante de pago:</label>
            <div class="p-control" style="display: block;">
              <input type="file" id="id-comprobante-subir_archivo_input_file" accept="image/png, image/jpeg, application/pdf" name="archivo" hidden>
              <button type="button" class="p-button p-button_success" id="id-comprobante-subir_archivo_seleccionar_button" title="Seleccionar comprobante de pago" style="margin-bottom: 0;">
                <span>
                  <i class="fas fa-file"></i>
                </span>
                <span><b>Seleccionar archivo</b></span>
              </button>
              <label class="p-text p-text-span_compra_detalles" id="id-comprobante-subir_archivo_span">
                <span>No se eligió el archivo</span>
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
              </label>
            </div>
          </div>

          <div class="p-field_margin_comprobante p-cuenta-contenedor_botones" id="id-comprobante-subir_archivo_contenedor_subir" style="display: none; margin-top: .5rem;"></div>
        </form>

        <p class="mx-etiqueta mx-etiqueta-info">
          <span class="mx-etiqueta-icon">
            <i class="fas fa-spinner"></i>
          </span>
          <span><b>Esperando el comprobante de pago</b></span>
        </p>';
        }else{
          $mensaje .= '
        <div class="p-notification p-notification_letter_info">
          <span><b>Acabas de subir tu comprobante de pago, se va a revisar y te notificaremos si fue rechazado o aceptado.</b></span>
        </div>

        <div class="p-cuenta-contenedor_botones">
          <button class="p-button p-button_info g-modal-archivo" title="Ver comprobante de pago">
            <span>
              <i class="fas fa-eye"></i>
            </span>
            <span><b>Ver comprobante de pago</b></span>
          </button>
        </div>

        <p class="mx-etiqueta mx-etiqueta-warning">
          <span class="mx-etiqueta-icon">
            <i class="fas fa-spinner"></i>
          </span>
          <span><b>Esperando que se acredite el pago</b></span>
        </p>

        <div class="mx-modal" id="id-modal-archivo_contenedor">
          <div class="mx-modal-fondo g-modal-archivo_cerrar"></div>
          <div class="mx-modal-contenido" id="id-modal-archivo_contenido">
            <div class="mx-modal-contenido_archivo">
              <button class="p-button p-button_square p-button_delete mx-modal-button_close g-modal-archivo_cerrar" title="Cerrar modal">
                <span>
                  <i class="fas fa-lg fa-times"></i>
                </span>
              </button>';

          $url_archivo = HOST_LINK . $ubicacionComprobantePago . $nombreComprobante;

          if($esImagen === '1'){
            $mensaje .= '
              <img src="' . $url_archivo . '" alt="Comprobante de pago">';
          }else{
            $mensaje .= '
              <object data="' . $url_archivo . '" type="application/pdf"></object>';
          }

          $mensaje .= '
            </div>
          </div>
        </div>';
        }
        break;

      case 2: // CANCELADO
        $url_archivo = $ubicacionComprobantePago.$nombreComprobante;

        // Revisa si existe el comprobante de pago
        if(file_exists($dirname . $url_archivo)){
          // Si existe, se elimina
          unlink($dirname. $url_archivo);
        }

        $mensaje .= '
        <p class="p-text_p">
          <span>Se canceló el <b>' . fecha_con_hora($fechaProceso) . '</b>.</span>
        </p>';

        if($comprobanteRechazado === 1){
          $mensaje .= '
        <div class="p-notification p-notification_letter_info">
          <span><b>La compra fué cancelada, porque tu comprobante de pago fue rechazado.</b></span>
        </div>';
        }

        $mensaje .= '
        <div>
          <p class="mx-etiqueta mx-etiqueta-error">
            <span class="mx-etiqueta-icon">
              <i class="fas fa-times-circle"></i>
            </span>
            <span><b>Pago cancelado</b></span>
          </p>
        </div>';
        break;

      case 3: // ACREDITADO
        $mensaje .= '
        <p>
          <span>Se acreditó el <b>' . fecha_con_hora($fechaProceso) . '</b>.</span>
        </p>

        <p class="mx-etiqueta mx-etiqueta-turquesa">
          <span class="mx-etiqueta-icon">
            <i class="fas fa-check-circle"></i>
          </span>
          <span><b>Pago acreditado</b></span>
        </p>';
        break;
    }

    $stmt = null;
  }catch(PDOException $error){
    //$msj_error = 'Error: ' . $error->getMessage();
    $msj_error = 'No se encontró la información para esta compra.';

    $mensaje = '
      <div class="p-notification p-notification_letter_error">
        <span>
          <i class="fas fa-times-circle"></i>
        </span>
        <span class="p-notification_p">' . $msj_error . '</span>
      </div>';
  }

  $respuesta = '1';

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>