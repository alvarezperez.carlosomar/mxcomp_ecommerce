<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'comprar'){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  require_once dirname(__DIR__, 2) . '/clases/usuario/metodos_usuario.php';
  require_once dirname(__DIR__, 2) . '/clases/proveedor/metodos_PCH.php';
  require_once dirname(__DIR__, 2) . '/clases/producto/metodos_producto.php';
  require_once dirname(__DIR__, 2) . '/clases/carrito/metodos_carrito.php';
  require_once dirname(__DIR__, 2) . '/clases/direcciones/metodos_direcciones.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $seccion = $_POST['seccion'];
  $metodoEnvio = $_POST['metodo_envio'];
  $costoEnvio_total = $_POST['costo_envio_total'];
  $metodoPago = $_POST['metodo_pago'];
  $totalPrecio_productos = $_POST['total_precio_productos'];
  $totalPagar = $_POST['total_pagar'];
  $unidadesComprar = $_POST['unidades_comprar'];

  $Conn_MXcomp = new Conexion_mxcomp();
  $Conn_Admin = new Conexion_admin();

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $proceso_correcto = false;
  $mensaje = '';
  $link = '';
  $correo_cliente = '';

  // REVISA SI EL ID DE USUARIO ES NUMERICO Y QUE EXISTA EL USUARIO
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario_Objeto = new Usuario($idUsuario, $codigoUsuario);

    // SE BUSCA EL USUARIO
    if($usuario_Objeto->buscarUsuario()){
      // ENCRIPTAMOS EL CODIGO DE USUARIO
      $codigoUsuario_encriptado = encriptar($codigoUsuario);

      $correo_cliente = desencriptar($usuario_Objeto->buscarUsuario_correo);
      $proceso_correcto = true;
    }else{
      $respuesta = '0';
      $mensaje = (string) $usuario_Objeto->buscarUsuario_mensaje;
      $proceso_correcto = false;
    }
  }else{
    $respuesta = '0';
    $mensaje = 'EL id del usuario no es numérico';
    $proceso_correcto = false;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // REVISA TODOS LOS CAMPOS
  if($proceso_correcto){
    // REVISA LA VARIABLE SECCION
    if($seccion !== "" && ($seccion === 'producto' || $seccion === 'carrito')){
      $seccion = (string) $seccion;
      $proceso_correcto = true;
    }else{
      $respuesta = '1'; // MENSAJE DE INFO
      $mensaje = 'Sólo se permite comprar desde el <b>producto</b> o <b>carrito</b>. Recarga la página y vuelve a realizar el proceso de compra.';
      $proceso_correcto = false;
    }

    // REVISA EL METODO DE ENVIO
    if($proceso_correcto){
      if($metodoEnvio !== "" && ($metodoEnvio === 'Sucursal' || $metodoEnvio === 'Paqueteria')){
        $metodoEnvio = (string) $metodoEnvio;
        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'El método de envío <b>' . $metodoEnvio . '</b> no existe. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }

    // REVISA EL COSTO DE ENVIO TOTAL
    if($proceso_correcto){
      if($costoEnvio_total !== "" && validar_precio_caracteres($costoEnvio_total)){
        if($costoEnvio_total === "0.00" || $costoEnvio_total === "0"){
          if($metodoEnvio === 'Paqueteria'){
            $costoEnvio_total = 'Envío gratis';
          }else{
            $costoEnvio_total = '0.00';
          }
        }else{
          $costoEnvio_total = (float) $costoEnvio_total;
        }

        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'El <b>costo de envío total</b> no tiene formato de precio. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }

    // REVISA EL METODO DE PAGO
    if($proceso_correcto){
      if($metodoPago !== "" && ($metodoPago === 'Tarjeta' || $metodoPago === 'Depósito/Transferencia')){
        $metodoPago = (string) $metodoPago;
        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'El método de pago <b>' . $metodoPago . '</b> no existe. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }

    // REVISA EL PRECIO TOTAL DE LOS PRODUCTOS A COMPRAR
    if($proceso_correcto){
      if($totalPrecio_productos !== "" && validar_precio_caracteres($totalPrecio_productos)){
        $totalPrecio_productos = (float) $totalPrecio_productos;
        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'El <b>precio total</b> de los productos no tiene formato de precio. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }

    // REVISA EL TOTAL A PAGAR
    if($proceso_correcto){
      if($totalPagar !== "" && validar_precio_caracteres($totalPagar)){
        $totalPagar = (float) $totalPagar;
        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'El <b>total a pagar</b> no tiene formato de precio. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }

    // REVISA LAS UNIDADES A COMPRAR
    if($proceso_correcto){
      if($unidadesComprar !== "" && validar_campo_numerico($unidadesComprar)){
        $unidadesComprar = (int) $unidadesComprar;
        $proceso_correcto = true;
      }else{
        $respuesta = '1'; // MENSAJE DE INFO
        $mensaje = 'No se obtuvieron las unidades a comprar. Recarga la página y vuelve a realizar el proceso de compra.';
        $proceso_correcto = false;
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // SE BUSCAN LOS DATOS DEL DOMICILIO DE ENVIO

  if($proceso_correcto){
    if($metodoEnvio === 'Paqueteria'){
      $direccion_Objeto = new Direccion($idUsuario, $codigoUsuario);

      if($direccion_Objeto->ver_domicilioEnvio()){
        // PARA LAS REMISIONES PROVEEDOR
        $nombreDestinatario_desencriptado = $direccion_Objeto->ver_domEnv_nombreDestinatario_desencriptado;
        $calleNumero_desencriptado = $direccion_Objeto->ver_domEnv_tipoVialidad . " " . $direccion_Objeto->ver_domEnv_nombreVialidad . " No. Ext. " . $direccion_Objeto->ver_domEnv_noExterior . ", No. Int. " . $direccion_Objeto->ver_domEnv_noInterior;
        $colonia_desencriptado = $direccion_Objeto->ver_domEnv_colonia;
        $ciudadMunicipio_desencriptado = $direccion_Objeto->ver_domEnv_ciudadMunicipio;
        $nombreEstado_desencriptado = $direccion_Objeto->ver_domEnv_nombreEstado;
        $codigoPostal_desencriptado = $direccion_Objeto->ver_domEnv_codigoPostal;
        $telefono_desencriptado = $direccion_Objeto->ver_domEnv_noTelefonico_desencriptado;


        // REGISTRO EN ORDEN DE COMPRA Y CORREO REMISIONES PCH
        $direccionCompleta_encriptado = $direccion_Objeto->ver_domEnv_direccionCompleta_encriptado; // ENCRIPTADA
        $nombreDestinatario_encriptado = $direccion_Objeto->ver_domEnv_nombreDestinatario_encriptado; // ENCRIPTADO
        $telefono_encriptado = $direccion_Objeto->ver_domEnv_noTelefonico_encriptado; // ENCRIPTADO
        $entreCalles_encriptado = $direccion_Objeto->ver_domEnv_entreCalles_encriptado; // ENCRIPTADO
        $referenciasAdicionales_encriptado = $direccion_Objeto->ver_domEnv_referenciasAdicionales_encriptado; // ENCRIPTADO
        $horarioEntrega_encriptado = $direccion_Objeto->ver_domEnv_horarioEntrega; // ENCRIPTADO


        // REGISTRO EN ORDEN DE COMPRA y CORREO DE PCH DEL DOMICILIO DE ENVIO
        $calleNumero_encriptado = encriptar_con_clave($calleNumero_desencriptado, $codigoUsuario_encriptado);
        $codigoPostal_encriptado = encriptar_con_clave($codigoPostal_desencriptado, $codigoUsuario_encriptado);
        $colonia_encriptado = encriptar_con_clave($colonia_desencriptado, $codigoUsuario_encriptado);
        $ciudadMunicipio_encriptado = encriptar_con_clave($ciudadMunicipio_desencriptado, $codigoUsuario_encriptado);
        $nombreEstado_encriptado = encriptar_con_clave($nombreEstado_desencriptado, $codigoUsuario_encriptado);

        $entreCalle1_encriptado = is_null($direccion_Objeto->ver_domEnv_entreCalle1) ? NULL : encriptar_con_clave($direccion_Objeto->ver_domEnv_entreCalle1, $codigoUsuario_encriptado);

        $entreCalle2_encriptado = is_null($direccion_Objeto->ver_domEnv_entreCalle2) ? NULL : encriptar_con_clave($direccion_Objeto->ver_domEnv_entreCalle2, $codigoUsuario_encriptado);

        $horaEntregaEnvio1_encriptado = is_null($direccion_Objeto->ver_domEnv_horaEntregaEnvio1) ? NULL : encriptar_con_clave($direccion_Objeto->ver_domEnv_horaEntregaEnvio1, $codigoUsuario_encriptado);

        $horaEntregaEnvio2_encriptado = is_null($direccion_Objeto->ver_domEnv_horaEntregaEnvio2) ? NULL : encriptar_con_clave($direccion_Objeto->ver_domEnv_horaEntregaEnvio2, $codigoUsuario_encriptado);

        $proceso_correcto = true;
      }else{
        $respuesta = $direccion_Objeto->ver_domEnv_respuesta; // MENSAJE DE INFO, TRAE RESPUESTA 1
        $mensaje = $direccion_Objeto->ver_domEnv_mensaje;
        $proceso_correcto = false;
      }
    }else{
      $direccionCompleta_encriptado = NULL;
      $nombreDestinatario_encriptado = NULL;
      $telefono_encriptado = NULL;
      $entreCalles_encriptado = NULL;
      $referenciasAdicionales_encriptado = NULL;
      $horarioEntrega_encriptado = NULL;
      $proceso_correcto = true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // BUSCAR DATOS DEL PRODUCTO
  $array_productos_remision = [];
  $array_productos = [];
  if($proceso_correcto){
    $producto_Objeto = new Producto($idUsuario, $codigoUsuario);
    $carrito_Objeto = new Carrito($idUsuario, $codigoUsuario);

    if($seccion === 'producto'){
      $prod_codigoProducto = $_SESSION['__producto_unidades__']['codigoProducto'];

      if($producto_Objeto->buscarProducto($prod_codigoProducto)){
        $prod_skuProveedor = (string) $producto_Objeto->buscarProducto_skuProveedor;
        $prod_descripcion = (string) $producto_Objeto->buscarProducto_descripcion;
        $prod_descripcionURL = (string) $producto_Objeto->buscarProducto_descripcionURL;
        $prod_nombreMarca = (string) $producto_Objeto->buscarProducto_nombreMarca;
        $prod_precioProveedor = (float) $producto_Objeto->buscarProducto_precioProveedor;
        $prod_monedaProveedor = (string) $producto_Objeto->buscarProducto_monedaProveedor;
        $prod_precioMXcomp = (float) $producto_Objeto->buscarProducto_precioMXcomp;
        $prod_monedaMXcomp = (string) $producto_Objeto->buscarProducto_monedaMXcomp;
        $prod_almacenes = (array) $producto_Objeto->buscarProducto_almacenes;
        $prod_tieneImagen = (string) $producto_Objeto->buscarProducto_tieneImagen;
        $prod_numeroUbicacionImagen = (string) $producto_Objeto->buscarProducto_numeroUbicacionImagen;
        $prod_nombreImagen = (string) $producto_Objeto->buscarProducto_nombreImagen;
        $prod_versionImagen = (string) $producto_Objeto->buscarProducto_versionImagen;

        $almacen_cantidad = [];
        foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $prod_nombreAlmacen=>$prod_datos_almacen){
          $prod_unidadesAlmacenComprar = (int) $prod_datos_almacen['unidades'];

          if($prod_unidadesAlmacenComprar > 0){
            foreach($prod_almacenes as $datos_almacen){
              if($prod_nombreAlmacen === $datos_almacen['nombre_almacen']){
                $prod_idAlmacen = (int) $datos_almacen['id_almacen'];
                break;
              }
            }

            $array[0] = [
              'codigoProducto' => $prod_codigoProducto,
              'skuProveedor' => $prod_skuProveedor,
              'descripcion' => $prod_descripcion,
              'descripcionURL' => $prod_descripcionURL,
              'nombreMarca' => $prod_nombreMarca,
              'precioProveedor' => $prod_precioProveedor,
              'precioMXcomp' => $prod_precioMXcomp,
              'monedaMXcomp' => $prod_monedaMXcomp,
              'unidadesAlmacenComprar' => $prod_unidadesAlmacenComprar,
              'tieneImagen' => $prod_tieneImagen,
              'numeroUbicacionImagen' => $prod_numeroUbicacionImagen,
              'nombreImagen' => $prod_nombreImagen,
              'versionImagen' => $prod_versionImagen
            ];

            $array_productos_remision[count($array_productos_remision)] = [
              'idAlmacen' => $prod_idAlmacen,
              'nombreAlmacen' => $prod_nombreAlmacen,
              'monedaRemision' => $prod_monedaProveedor,
              'productosComprarRemision' => $prod_unidadesAlmacenComprar,
              'remision' => '',
              'productos' => $array
            ];

            $almacen_cantidad[$prod_nombreAlmacen] = (int) $prod_unidadesAlmacenComprar;
          }
        }

        $array_productos[0] = [
          'codigoProducto' => (string) $prod_codigoProducto,
	        'skuProveedor' => (string) $prod_skuProveedor,
	        'almacen_cantidad' => $almacen_cantidad
        ];

        $proceso_correcto = true;
      }else{
        $opcion = (int) $producto_Objeto->buscarProducto_opcion;

        if($opcion === 1){
          $respuesta = '1';
          $mensaje = 'Lo sentimos, este producto ya no se encuentra en nuestros registros, revisa tu pedido y vuelve a realizar tu compra.';
        }else{
          $respuesta = '2';
          $mensaje = $producto_Objeto->buscarProducto_mensaje;
        }

        $proceso_correcto = false;
      }
    }else{
      if($carrito_Objeto->buscarProductos_comprar()){
        $array_carrito = $carrito_Objeto->buscarProductos_comprar_array;

        foreach($array_carrito as $d_prod_carrito){
          $cart_codigoProducto = (string) $d_prod_carrito['codigoProducto'];
          $cart_skuProveedor = (string) $d_prod_carrito['skuProveedor'];
          $cart_descripcion = (string) $d_prod_carrito['descripcion'];
          $cart_descripcionURL = (string) $d_prod_carrito['descripcionURL'];
          $cart_nombreMarca = (string) $d_prod_carrito['nombreMarca'];
          $cart_precioProveedor = (float) $d_prod_carrito['precioProveedor'];
          $cart_monedaProveedor = (string) $d_prod_carrito['monedaProveedor'];
          $cart_precioMXcomp = (float) $d_prod_carrito['precioMXcomp'];
          $cart_monedaMXcomp = (string) $d_prod_carrito['monedaMXcomp'];
          $cart_unidadesAlmacenComprar = (int) $d_prod_carrito['unidades'];
          $cart_idAlmacen = (int) $d_prod_carrito['numeroAlmacen'];
          $cart_nombreAlmacen = (string) $d_prod_carrito['nombreAlmacen'];
          $cart_tieneImagen = (string) $d_prod_carrito['tieneImagen'];
          $cart_numeroUbicacionImagen = $d_prod_carrito['numeroUbicacionImagen'];
          $cart_nombreImagen = (string) $d_prod_carrito['nombreImagen'];
          $cart_versionImagen = $d_prod_carrito['versionImagen'];

          // CREAR ARRAY $array_productos_remision

          $array[0] = [
            'codigoProducto' => $cart_codigoProducto,
            'skuProveedor' => $cart_skuProveedor,
            'descripcion' => $cart_descripcion,
            'descripcionURL' => $cart_descripcionURL,
            'nombreMarca' => $cart_nombreMarca,
            'precioProveedor' => $cart_precioProveedor,
            'precioMXcomp' => $cart_precioMXcomp,
            'monedaMXcomp' => $cart_monedaMXcomp,
            'unidadesAlmacenComprar' => $cart_unidadesAlmacenComprar,
            'tieneImagen' => $cart_tieneImagen,
            'numeroUbicacionImagen' => $cart_numeroUbicacionImagen,
            'nombreImagen' => $cart_nombreImagen,
            'versionImagen' => $cart_versionImagen
          ];

          if(count($array_productos_remision) === 0){
            $array_productos_remision[count($array_productos_remision)] = [
              'idAlmacen' => $cart_idAlmacen,
              'nombreAlmacen' => $cart_nombreAlmacen,
              'monedaRemision' => $cart_monedaProveedor,
              'productosComprarRemision' => $cart_unidadesAlmacenComprar,
              'remision' => '',
              'productos' => $array
            ];
          }else{
            $almacen_moneda_existe = false;

            foreach($array_productos_remision as $ind=>$data){
              if($cart_nombreAlmacen === $data['nombreAlmacen'] && $cart_monedaProveedor === $data['monedaRemision']){
                $array_productos_remision[$ind]['productosComprarRemision'] += $cart_unidadesAlmacenComprar;
                $array_productos_remision[$ind]['productos'] = array_merge($array_productos_remision[$ind]['productos'], $array);
                $almacen_moneda_existe = true;
                break;
              }
            }

            if($almacen_moneda_existe === false){
              $array_productos_remision[count($array_productos_remision)] = [
                'idAlmacen' => $cart_idAlmacen,
                'nombreAlmacen' => $cart_nombreAlmacen,
                'monedaRemision' => $cart_monedaProveedor,
                'productosComprarRemision' => $cart_unidadesAlmacenComprar,
                'remision' => '',
                'productos' => $array
              ];
            }
          }

          //////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // CREAR ARRAY $array_productos

          $almacen_cantidad = [];
          $almacen_cantidad[$cart_nombreAlmacen] = (int) $cart_unidadesAlmacenComprar;

          if(count($array_productos) > 0){
            $array_codigosProducto = array_column($array_productos, 'codigoProducto');
          }

          if(count($array_productos) === 0 || !in_array($cart_codigoProducto, $array_codigosProducto)){
            $array_productos[count($array_productos)] = [
              'codigoProducto' => (string) $cart_codigoProducto,
              'skuProveedor' => (string) $cart_skuProveedor,
              'almacen_cantidad' => $almacen_cantidad
            ];
          }else{
            foreach($array_productos as $ind=>$data){
              if($cart_codigoProducto === $data['codigoProducto']){
                $array_productos[$ind]['almacen_cantidad'] = array_merge($array_productos[$ind]['almacen_cantidad'], $almacen_cantidad);
                break;
              }
            }
          }
        }

        $proceso_correcto = true;
      }else{
        $respuesta = '2'; // MENSAJE DE ERROR
        $mensaje = $carrito_Objeto->buscarProductos_comprar_mensaje;
        $proceso_correcto = false;
      }
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE REMISIONES Y/O ORDEN DE COMPRA
  $errorOrdenCompra_existe = false;
  $errorOrdenCompra_mensaje_error = "";
  $errorOrdenCompra_seccion = "";
  $errorOrdenCompra_proveedor = "";
  $errorOrdenCompra_numero_almacen = "";
  $errorOrdenCompra_nombre_almacen = "";
  $errorOrdenCompra_moneda_remision = "";
  $errorOrdenCompra_cliente_id = "";
  $errorOrdenCompra_codigo_cliente = "";
  $errorOrdenCompra_remisiones_eliminar = "";

  $acumulacion_remisiones = "";
  $id_ordenCompra = "";

  // SE REGISTRA LA ORDEN DE COMPRA EN SUS RESPECTIVAS TABLAS, SE MANEJA COMO UNA TRANSACCION
  if($proceso_correcto){
    $tipo_error = "";
    $proceso_correcto = false; // Para los siguientes pasos se vuelve a resetear a false

    try{
      $Conn_Admin->pdo->beginTransaction();
      $fechaCompra = date("Y-m-d H:i:s");

      // SE GENERA LA ORDEN DE COMPRA
      $tipo_error = "No se pudo consultar la existencia de las ordenes de compras.";

      $sql = "SELECT COUNT(id) FROM __ordenes_compra";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->execute();
      $ordenesCompras_existen = (int) $stmt->fetchColumn();

      if($ordenesCompras_existen === 0){
        $ordenCompra = 1;
      }else{
        $tipo_error = "No se pudo generar la orden de compra.";

        $sql = "SELECT ordenCompra FROM __ordenes_compra ORDER BY id DESC LIMIT 1";
        $stmt = $Conn_Admin->pdo->prepare($sql);
        $stmt->execute();
        $numeracion = (int) $stmt->fetchColumn();
        $ordenCompra = $numeracion + 1;
      }

      $tipo_error = "";

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // SE GENERAN LAS REMISIONES PARA LA ORDEN DE COMPRA

      require_once dirname(__DIR__, 2) . '/clases/proveedor/metodos_PCH.php';
      $proveedorPCH = new PCH_Mayoreo();
      $seguro_envio = true;

      foreach($array_productos_remision as $indice=>$datos_producto_comprar){
        $idAlmacen = (int) $datos_producto_comprar['idAlmacen'];
        $nombreAlmacen = (string) $datos_producto_comprar['nombreAlmacen'];
        $monedaRemision = (string) $datos_producto_comprar['monedaRemision'];
        $productos = $datos_producto_comprar['productos'];

        $productos_pedido = [];
        foreach($productos as $datos){
          $skuProveedor = (string) $datos['skuProveedor'];
          $unidadesAlmacenComprar = (int) $datos['unidadesAlmacenComprar'];
  
          $posicion = count($productos_pedido);
          $productos_pedido[$posicion] = [
            "sku" => (string) $skuProveedor,
            "qty" => (int) $unidadesAlmacenComprar
          ];
        }

        if($proveedorPCH->realizarPedido($idAlmacen, $seguro_envio, $ordenCompra, $nombreDestinatario_desencriptado, $calleNumero_desencriptado, $colonia_desencriptado, $ciudadMunicipio_desencriptado, $nombreEstado_desencriptado, $codigoPostal_desencriptado, $telefono_desencriptado, $productos_pedido)){
          $remision = (string) $proveedorPCH->rPedido_pedidos[0]->so;
          $array_productos_remision[$indice]['remision'] = $remision;

          $acumulacion_remisiones = $acumulacion_remisiones === "" ? $remision : $acumulacion_remisiones . ', ' . $remision;

          $errorOrdenCompra_existe = false;
        }else{
          // 1: WEB SERVICES | 2: PROVEEDOR
          $errorOrdenCompra_mensaje_error = $proveedorPCH->opcion = '1' ? $proveedorPCH->mensajeError_webServices : $proveedorPCH->mensajeError_Proveedor;

          $errorOrdenCompra_existe = true;
        }
        
        if($errorOrdenCompra_existe){
          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, no fue posible establecer la conexión con el proveedor, intenta realizar la compra en unos minutos.";

          $errorOrdenCompra_seccion = 'Remisiones (' . $seccion . ' - ' . $metodoPago . ')';
          $errorOrdenCompra_proveedor = 'PCH MAYOREO';
          $errorOrdenCompra_numero_almacen = $idAlmacen;
          $errorOrdenCompra_nombre_almacen = $nombreAlmacen;
          $errorOrdenCompra_moneda_remision = $monedaRemision;
          $errorOrdenCompra_cliente_id = $idUsuario;
          $errorOrdenCompra_codigo_cliente = $codigoUsuario;
          $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones === '' ? '----' : $acumulacion_remisiones;

          $proceso_correcto = false;
          break;
        }else{
          $proceso_correcto = true;
        }
      }

      /////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // SE GUARDAN LOS REGISTROS EN LA TABLA QUE LE CORRESPONDE, POR CADA ALMACEN
      if($proceso_correcto){
        $tipo_error = "Se canceló la compra porque surgió un problema durante el proceso, vuelve a realizar la compra. Si sigue apareciendo este mensaje, ponte en contacto con atención a clientes.";

        // EN CASO DE HABER UN ERROR EN UNA CONSULTA
        $errorOrdenCompra_existe = true;
        $errorOrdenCompra_mensaje_error = 'Orden de compra: Surgió un problema al tratar de ingresar un registro en una tabla. - ';
        $errorOrdenCompra_seccion = 'Orden de compra (' . $seccion . ' - ' . $metodoPago . ')';
        $errorOrdenCompra_proveedor = '----';
        $errorOrdenCompra_numero_almacen = '----';
        $errorOrdenCompra_nombre_almacen = '----';
        $errorOrdenCompra_moneda_remision = '----';
        $errorOrdenCompra_cliente_id = $idUsuario;
        $errorOrdenCompra_codigo_cliente = $codigoUsuario;
        $errorOrdenCompra_remisiones_eliminar = $acumulacion_remisiones === '' ? '----' : $acumulacion_remisiones;

        $proceso_correcto = false;

        // SE INSERTA LA ORDEN DE COMPRA EN LA TABLA __ORDENES_COMPRA
        $numeroEstadoCompra = "1";
        $estadoCompra = "Pendiente";
        $productosComprados = (int) $unidadesComprar;
        $monedaPedido = "MXN";
        $numeroEstadoPago = "1";
        $estadoPago = "Pendiente";

        if($productosComprados > 0){
          $sql = "INSERT INTO __ordenes_compra(ordenCompra, idCliente, codigoCliente, numeroEstadoCompra, estadoCompra, productosComprados, totalPagado, precioPedido, monedaPedido, numeroEstadoPago, estadoPago, metodoEnvio, costoEnvio, direccionCompleta, calleNumero, codigoPostal, colonia, ciudadMunicipio, nombreEstado, nombreDestinatario, telefono, entreCalles, entreCalle1, entreCalle2, referenciasAdicionales, horarioEntrega, horaEntregaEnvio1, horaEntregaEnvio2, seccion, fechaCompra) VALUES(:ordenCompra, :idCliente, :codigoCliente, :numeroEstadoCompra, :estadoCompra, :productosComprados, :totalPagado, :precioPedido, :monedaPedido, :numeroEstadoPago, :estadoPago, :metodoEnvio, :costoEnvio, :direccionCompleta, :calleNumero, :codigoPostal, :colonia, :ciudadMunicipio, :nombreEstado, :nombreDestinatario, :telefono, :entreCalles, :entreCalle1, :entreCalle2, :referenciasAdicionales, :horarioEntrega, :horaEntregaEnvio1, :horaEntregaEnvio2, :seccion, :fechaCompra)";
          $stmt = $Conn_Admin->pdo->prepare($sql);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':numeroEstadoCompra', $numeroEstadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':estadoCompra', $estadoCompra, PDO::PARAM_STR);
          $stmt->bindParam(':productosComprados', $productosComprados, PDO::PARAM_STR);
          $stmt->bindParam(':totalPagado', $totalPagar, PDO::PARAM_STR);
          $stmt->bindParam(':precioPedido', $totalPrecio_productos, PDO::PARAM_STR);
          $stmt->bindParam(':monedaPedido', $monedaPedido, PDO::PARAM_STR);
          $stmt->bindParam(':numeroEstadoPago', $numeroEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':metodoEnvio', $metodoEnvio, PDO::PARAM_STR);
          $stmt->bindParam(':costoEnvio', $costoEnvio_total, PDO::PARAM_STR);
          $stmt->bindParam(':direccionCompleta', $direccionCompleta_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':calleNumero', $calleNumero_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':codigoPostal', $codigoPostal_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':colonia', $colonia_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':nombreEstado', $nombreEstado_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':nombreDestinatario', $nombreDestinatario_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':telefono', $telefono_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalles', $entreCalles_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalle1', $entreCalle1_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':entreCalle2', $entreCalle2_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':horarioEntrega', $horarioEntrega_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':horaEntregaEnvio1', $horaEntregaEnvio1_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':horaEntregaEnvio2', $horaEntregaEnvio2_encriptado, PDO::PARAM_STR);
          $stmt->bindParam(':seccion', $seccion, PDO::PARAM_STR);
          $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          $id_ordenCompra = (int) $Conn_Admin->pdo->lastInsertId();

          // SE INSERTAN LOS DATOS DE PAGO PARA LA ORDEN DE COMPRA EN LA TABLA __ORDEN_COMPRA_DATOS_PAGO
          $numeroEstadoPago = "1";
          $estadoPago = "Pago pendiente";
          $comprobantePago = 0;
          $oportunidadesComprobante = 3;

          $sql = "INSERT INTO __orden_compra_datos_pago(ordenCompra, idCliente, codigoCliente, metodoPago, numeroEstadoPago, estadoPago, comprobantePago, oportunidadesComprobante, fechaRegistro) VALUES(:ordenCompra, :idCliente, :codigoCliente, :metodoPago, :numeroEstadoPago, :estadoPago, :comprobantePago, :oportunidadesComprobante, :fechaRegistro)";
          $stmt = $Conn_Admin->pdo->prepare($sql);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':metodoPago', $metodoPago, PDO::PARAM_STR);
          $stmt->bindParam(':numeroEstadoPago', $numeroEstadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':estadoPago', $estadoPago, PDO::PARAM_STR);
          $stmt->bindParam(':comprobantePago', $comprobantePago, PDO::PARAM_INT);
          $stmt->bindParam(':oportunidadesComprobante', $oportunidadesComprobante, PDO::PARAM_INT);
          $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
          $stmt->execute();

          // SE INSERTAN REGISTROS DE LA COMPRA PARA CADA ALMACEN
          foreach($array_productos_remision as $indice=>$datos_producto_comprar){
            $idAlmacen = (string) $datos_producto_comprar['idAlmacen'];
            $nombreAlmacen = (string) $datos_producto_comprar['nombreAlmacen'];
            $monedaRemision = (string) $datos_producto_comprar['monedaRemision'];
            $productosComprarRemision = (int) $datos_producto_comprar['productosComprarRemision'];
            $remision_almacen = (string) $datos_producto_comprar['remision'];
            $productos = $datos_producto_comprar['productos'];

            // TABLA DE __remisiones
            $numeroEstadoRemision = "1";
            $estadoRemision = "Pendiente";
            $nombreProveedor = "PCH MAYOREO";

            $importeRemision = 0.00;
            foreach($productos as $datos){
              // ANTERIOR
              //$importeRemision += (float) $datos['precioProveedor'] * $datos['unidadesAlmacenComprar'];

              $precio_remision = (float) round($datos['precioMXcomp'] / 1.32, 2);
              $importeRemision += (float) $precio_remision * $datos['unidadesAlmacenComprar'];
            }

            $sql = "INSERT INTO __remisiones(ordenCompra, idCliente, codigoCliente, numeroEstadoRemision, estadoRemision, nombreProveedor, idAlmacen, nombreAlmacen, numeroRemision, importeRemision, monedaRemision, productosComprados, fechaRegistro) VALUES(:ordenCompra, :idCliente, :codigoCliente, :numeroEstadoRemision, :estadoRemision, :nombreProveedor, :idAlmacen, :nombreAlmacen, :numeroRemision, :importeRemision, :monedaRemision, :productosComprados, :fechaRegistro)";
            $stmt = $Conn_Admin->pdo->prepare($sql);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
            $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':idAlmacen', $idAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroRemision', $remision_almacen, PDO::PARAM_STR);
            $stmt->bindParam(':importeRemision', $importeRemision, PDO::PARAM_STR);
            $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
            $stmt->bindParam(':productosComprados', $productosComprarRemision, PDO::PARAM_STR);
            $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
            $stmt->execute();

            $idRemision = (int) $Conn_Admin->pdo->lastInsertId();

            // Tabla de __datos_factura_envio
            $sql = "SELECT COUNT(id) AS conteo FROM __datos_factura_envio WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente AND nombreProveedor = :nombreProveedor AND idAlmacen = :idAlmacen";
            $stmt = $Conn_Admin->pdo->prepare($sql);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':idAlmacen', $idAlmacen, PDO::PARAM_STR);
            $stmt->execute();
            $registro_existe = (int) $stmt->fetchColumn();

            if($registro_existe === 0){
              // REGISTROS EN __datos_factura_envio
              $datosFactura = '0';
              $datosEnvioEntrega = '0';

              $sql = "INSERT INTO __datos_factura_envio(ordenCompra, idCliente, codigoCliente, nombreProveedor, idAlmacen, nombreAlmacen, datosFactura, datosEnvioEntrega) VALUES (:ordenCompra, :idCliente, :codigoCliente, :nombreProveedor, :idAlmacen, :nombreAlmacen, :datosFactura, :datosEnvioEntrega)";
              $stmt = $Conn_Admin->pdo->prepare($sql);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':idAlmacen', $idAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':datosFactura', $datosFactura, PDO::PARAM_STR);
              $stmt->bindParam(':datosEnvioEntrega', $datosEnvioEntrega, PDO::PARAM_STR);
              $stmt->execute();

              $idDatosFacturaEnvio = (int) $Conn_Admin->pdo->lastInsertId();

              // TABLA DE __estados_factura_envio
              $mensaje = "El cliente solicitó la remisión. En espera de ingresar los datos de la factura.";
  
              $sql = "INSERT INTO __estados_factura_envio(ordenCompra, idCliente, codigoCliente, idDatosFacturaEnvio, numeroEstadoRemision, estadoRemision, mensaje, fechaRegistro) VALUES(:ordenCompra, :idCliente, :codigoCliente, :idDatosFacturaEnvio, :numeroEstadoRemision, :estadoRemision, :mensaje, :fechaRegistro)";
              $stmt = $Conn_Admin->pdo->prepare($sql);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':idDatosFacturaEnvio', $idDatosFacturaEnvio, PDO::PARAM_INT);
              $stmt->bindParam(':numeroEstadoRemision', $numeroEstadoRemision, PDO::PARAM_STR);
              $stmt->bindParam(':estadoRemision', $estadoRemision, PDO::PARAM_STR);
              $stmt->bindParam(':mensaje', $mensaje, PDO::PARAM_STR);
              $stmt->bindParam(':fechaRegistro', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }

            // TABLA DE __orden_compra_productos
            $monedaRemision = 'MXN'; // Sólo en el nuevo cambio

            foreach($productos as $datos){
              $codigoProducto = (string) $datos['codigoProducto'];
              $skuProveedor = (string) $datos['skuProveedor'];
              $descripcion = (string) $datos['descripcion'];
              $descripcionURL = (string) $datos['descripcionURL'];
              $nombreMarca = (string) $datos['nombreMarca'];
              $precioProveedor = (float) $datos['precioProveedor'];
              $precioMXcomp = (float) $datos['precioMXcomp'];
              $monedaMXcomp = (string) $datos['monedaMXcomp'];
              $unidadesAlmacenComprar = (int) $datos['unidadesAlmacenComprar'];
              $tieneImagen = (string) $datos['tieneImagen'];
              $numeroUbicacionImagen = is_null($datos['numeroUbicacionImagen']) || $datos['numeroUbicacionImagen'] === '' ? NULL : (string) $datos['numeroUbicacionImagen'];
              $nombreImagen = (string) $datos['nombreImagen'];
              $versionImagen = is_null($datos['versionImagen']) || $datos['versionImagen'] === '' ? NULL : (string) $datos['versionImagen'];

              // ANTERIOR
              //$precioUnitarioRemision = (float) $precioProveedor;

              $precioUnitarioRemision = (float) round($precioMXcomp / 1.32, 2);
              $precioTotalMXcomp = (float) $precioMXcomp * $unidadesAlmacenComprar;
              $costoRemison = (float) $precioUnitarioRemision * $unidadesAlmacenComprar;
  
              $sql = "INSERT INTO __orden_compra_productos(ordenCompra, idCliente, codigoCliente, idRemision, nombreProveedor, idAlmacen, nombreAlmacen, codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioUnitarioMXcomp, monedaMXcomp, precioUnitarioRemision, monedaRemision, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, costoRemison, fechaCompra) VALUES(:ordenCompra, :idCliente, :codigoCliente, :idRemision, :nombreProveedor, :idAlmacen, :nombreAlmacen, :codigoProducto, :skuProveedor, :descripcion, :descripcionURL, :nombreMarca, :precioUnitarioMXcomp, :monedaMXcomp, :precioUnitarioRemision, :monedaRemision, :cantidadComprada, :precioTotalMXcomp, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :costoRemison, :fechaCompra)";
              $stmt = $Conn_Admin->pdo->prepare($sql);
              $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
              $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':idRemision', $idRemision, PDO::PARAM_INT);
              $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':idAlmacen', $idAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
              $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioMXcomp', $precioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':precioUnitarioRemision', $precioUnitarioRemision, PDO::PARAM_STR);
              $stmt->bindParam(':monedaRemision', $monedaRemision, PDO::PARAM_STR);
              $stmt->bindParam(':cantidadComprada', $unidadesAlmacenComprar, PDO::PARAM_STR);
              $stmt->bindParam(':precioTotalMXcomp', $precioTotalMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':costoRemison', $costoRemison, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCompra', $fechaCompra, PDO::PARAM_STR);
              $stmt->execute();
            }
          }

          //////////////////////////////////////////////////////////
          // SE RESETEAN LAS VARIABLES DE ERROR DE ORDEN DE COMPRA

          $errorOrdenCompra_existe = false;
          $errorOrdenCompra_mensaje_error = "";
          $errorOrdenCompra_seccion = "";
          $errorOrdenCompra_proveedor = "";
          $errorOrdenCompra_numero_almacen = "";
          $errorOrdenCompra_nombre_almacen = "";
          $errorOrdenCompra_moneda_remision = "";
          $errorOrdenCompra_cliente_id = "";
          $errorOrdenCompra_codigo_cliente = "";
          $errorOrdenCompra_remisiones_eliminar = "";

          $proceso_correcto = true;
        }else{
          $errorOrdenCompra_existe = true;
          $errorOrdenCompra_mensaje_error = 'Orden de compra: Se detectó que no hay productos en el proveedor PCH, se cancela la compra.';
          $errorOrdenCompra_seccion = 'Orden de compra (' . $seccion . ' - ' . $metodoPago . ')';
          $errorOrdenCompra_proveedor = '----';
          $errorOrdenCompra_numero_almacen = '----';
          $errorOrdenCompra_nombre_almacen = '----';
          $errorOrdenCompra_moneda_remision = '----';
          $errorOrdenCompra_cliente_id = $idUsuario;
          $errorOrdenCompra_codigo_cliente = $codigoUsuario;

          $respuesta = "1"; // MENSAJE DE INFO
          $mensaje = "Lo sentimos, no fue posible concretar la compra, el proveedor ya no cuenta con existencias para los producto(s).";
          $proceso_correcto = false;
        }
      }

      $Conn_Admin->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_Admin->pdo->rollBack();
      $respuesta = "2"; // MENSAJE DE ERROR
      $errorOrdenCompra_mensaje_error .= $error->getMessage();
      $mensaje = $tipo_error;
      $proceso_correcto = false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // VARIABLES PARA ERRORES DE MODIFICAR EXISTENCIAS
  $errorModificarExistencias_existe = false;
  $errorModificarExistencias_mensaje_error = "";
  $errorModificarExistencias_seccion = "";
  $errorModificarExistencias_proveedor = "";
  $errorModificarExistencias_numero_almacen = "";
  $errorModificarExistencias_nombre_almacen = "";
  $errorModificarExistencias_moneda_remision = "";
  $errorModificarExistencias_cliente_id = "";
  $errorModificarExistencias_codigo_cliente = "";
  $errorModificarExistencias_remisiones_eliminar = "";

  // SE MODIFICAN LAS EXISTENCIAS DE CADA ALMACEN PARA EL PRODUCTO COMPRADO
  if($proceso_correcto){
    $brand_error = false;
    $cantidad_productos_modificados = 0;
    foreach($array_productos as $datos){
      $codigoProducto = (string) $datos['codigoProducto'];
      $skuProveedor = (string) $datos['skuProveedor'];
      $almacen_cantidad = $datos['almacen_cantidad'];

      if($producto_Objeto->buscarProducto($codigoProducto)){
        $almacenes = (array) $producto_Objeto->buscarProducto_almacenes;
      }else{
        $brand_mensaje = $producto_Objeto->buscarProducto_mensaje;
        $brand_error = true;
        break;
      }

      if(!$brand_error){
        $existenciaTotal = 0;
        foreach($almacenes as $indice=>$datos_almacen){
          $nombreAlmacen = $datos_almacen['nombre_almacen'];
          if(array_key_exists($nombreAlmacen, $almacen_cantidad)){
            $existencia = (int) $datos_almacen['existencia'];
            $existencia = (int) $existencia - $almacen_cantidad[$nombreAlmacen];
            $almacenes[$indice]['existencia'] = $existencia < 0 ? 0 : $existencia;
          }

          $existenciaTotal += $almacenes[$indice]['existencia'];
        }

        $fechaActualizacion = date("Y-m-d H:i:s");
        $almacenes = json_encode($almacenes);
        if( $producto_Objeto->actualizarExistencia($existenciaTotal, $almacenes, $fechaActualizacion, $codigoProducto, $skuProveedor) ){
          $cantidad_productos_modificados++;
        }else{
          $brand_mensaje = $producto_Objeto->actualizarExistencia_mensaje;
          $brand_error = true;
          break;
        }
      }
    }

    if($seccion === 'producto' && $cantidad_productos_modificados > 0){
      unset($_SESSION['__producto_unidades__']);
    }

    if($brand_error){
      // EN CASO DE HABER UN ERROR EN LA CONSULTA
      $errorModificarExistencias_existe = true;
      $errorModificarExistencias_mensaje_error = 'Existencias producto: Se generó la orden de compra, pero las existencias del producto no se modificaron. - ' . $brand_mensaje;
      $errorModificarExistencias_seccion = 'Existencias producto (' . $seccion . ' - ' . $metodoPago . ')';
      $errorModificarExistencias_proveedor = '----';
      $errorModificarExistencias_numero_almacen = '----';
      $errorModificarExistencias_nombre_almacen = '----';
      $errorModificarExistencias_moneda_remision = '----';
      $errorModificarExistencias_cliente_id = $idUsuario;
      $errorModificarExistencias_codigo_cliente = $codigoUsuario;
      $errorModificarExistencias_remisiones_eliminar = '----';
    }

    $proceso_correcto = true;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if($seccion === 'carrito'){
    // VARIABLES PARA ERRORES DE ELIMINAR PRODUCTOS DEL CARRITO
    $errorEliminarProductosCarrito_existe = false;
    $errorEliminarProductosCarrito_mensaje_error = "";
    $errorEliminarProductosCarrito_seccion = "";
    $errorEliminarProductosCarrito_proveedor = "";
    $errorEliminarProductosCarrito_numero_almacen = "";
    $errorEliminarProductosCarrito_nombre_almacen = "";
    $errorEliminarProductosCarrito_moneda_remision = "";
    $errorEliminarProductosCarrito_cliente_id = "";
    $errorEliminarProductosCarrito_codigo_cliente = "";
    $errorEliminarProductosCarrito_remisiones_eliminar = "";
  
    // SE ELIMINAN LOS PRODUCTOS COMPRADOS DEL CARRITO DEL USUARIO
    if($proceso_correcto){
      try{
        $Conn_MXcomp->pdo->beginTransaction();
  
        $tieneExistencias = '1';
        $guardado = '0';
        $registroExiste = '1';
  
        $sql = "DELETE FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste";
        $stmt = $Conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
        $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
        $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
        $stmt->execute();
  
        $Conn_MXcomp->pdo->commit();
        $stmt = null;
      }catch(PDOException $error){
        $Conn_MXcomp->pdo->rollBack();
  
        // EN CASO DE HABER UN ERROR EN LA CONSULTA
        $errorEliminarProductosCarrito_existe = true;
        $errorEliminarProductosCarrito_mensaje_error = 'Eliminación productos: Se generó la orden de compra, pero no se eliminaron los productos del carrito. - ' . $error->getMessage();
        $errorEliminarProductosCarrito_seccion = 'Eliminación productos (' . $seccion . ' - ' . $metodoPago . ')';
        $errorEliminarProductosCarrito_proveedor = '----';
        $errorEliminarProductosCarrito_numero_almacen = '----';
        $errorEliminarProductosCarrito_nombre_almacen = '----';
        $errorEliminarProductosCarrito_moneda_remision = '----';
        $errorEliminarProductosCarrito_cliente_id = $idUsuario;
        $errorEliminarProductosCarrito_codigo_cliente = $codigoUsuario;
        $errorEliminarProductosCarrito_remisiones_eliminar = '----';
      }
  
      $proceso_correcto = true;
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // SE MANDAN LOS CORREOS AL USUARIO Y A LAS CUENTAS DE MXCOMP
  if($proceso_correcto){
    $no_correos_no_enviados = 0;
    $correos_no_enviados = "";

    $fechaCompra_correo = fecha_con_hora($fechaCompra);

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO CLIENTE DE MXCOMP
    require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/ticket_compra.php';

    $link_detallesCompra = HOST_LINK . 'compra-detalles/' . $id_ordenCompra . '/' . $ordenCompra;

    $correo_ticketCompra = new Correo_ticketCompra($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesCompra, $correo_cliente);

    if( !$correo_ticketCompra->enviarCorreo() ){
      $correos_no_enviados = 'Cliente';
      $no_correos_no_enviados++;
    }

    $proceso_correcto = true;

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO PARA USUARIOS INTERNOS DE MXCOMP - MESA DE CONTROL, FINANZAS Y COMPRAS
    if($proceso_correcto){
      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/remisiones_mxcomp.php';

      $link_detallesVenta = HOST_LINK_ADMIN . 'ordenes-compra/detalles/' . $id_ordenCompra . '/' . $ordenCompra . '/' . $codigoUsuario;

      $correo_remisionesMxcomp = new Correo_remisionesMXcomp($ordenCompra, $fechaCompra_correo, $metodoPago, $totalPagar, $link_detallesVenta, $array_productos_remision);

      if( !$correo_remisionesMxcomp->enviarCorreo() ){
        $correos_no_enviados .= $no_correos_no_enviados === 0 ? 'Internos MXcomp' : ', Internos MXcomp';
        //$no_correos_no_enviados++;
      }

      $proceso_correcto = true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // CORREO PARA DOMICILIO DE ENVIO A PCH
    /* if($proceso_correcto){
      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/domicilioEnvio_PCH.php';

      $correo_domicilioEnvio_PCH = new Correo_domicilioEnvio_PCH(
        $ordenCompra,
        desencriptar_con_clave($calleNumero_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($codigoPostal_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($colonia_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($ciudadMunicipio_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($nombreEstado_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($nombreDestinatario_encriptado, $codigoUsuario_encriptado),
        desencriptar_con_clave($telefono_encriptado, $codigoUsuario_encriptado),
        is_null($entreCalle1_encriptado) ? NULL : desencriptar_con_clave($entreCalle1_encriptado, $codigoUsuario_encriptado),
        is_null($entreCalle2_encriptado) ? NULL : desencriptar_con_clave($entreCalle2_encriptado, $codigoUsuario_encriptado),
        is_null($horaEntregaEnvio1_encriptado) ? NULL : desencriptar_con_clave($horaEntregaEnvio1_encriptado, $codigoUsuario_encriptado),
        is_null($horaEntregaEnvio2_encriptado) ? NULL : desencriptar_con_clave($horaEntregaEnvio2_encriptado, $codigoUsuario_encriptado)
      );

      if( !$correo_domicilioEnvio_PCH->enviarCorreo() ){
        $correos_no_enviados .= $no_correos_no_enviados === 0 ? 'Domicilio Envio PCH' : ', Domicilio Envio PCH';
      }

      $proceso_correcto = true;
    } */

    // SI SE MANDARON TODOS LOS CORREOS DE FORMA EXITOSA
    if($no_correos_no_enviados === 0){
      $errorOrdenCompra_existe = false;
      $errorOrdenCompra_mensaje_error = '';
      $errorOrdenCompra_seccion = '';
      $errorOrdenCompra_proveedor = '';
      $errorOrdenCompra_numero_almacen = '';
      $errorOrdenCompra_nombre_almacen = '';
      $errorOrdenCompra_moneda_remision = '';
      $errorOrdenCompra_cliente_id = '';
      $errorOrdenCompra_codigo_cliente = '';
      $errorOrdenCompra_remisiones_eliminar = '';
    }else{
      $errorOrdenCompra_existe = true;
      $errorOrdenCompra_mensaje_error = 'Correos: Uno o más correos no se enviaron en la orden de compra #' . $ordenCompra . ' - Correo de ' . $correos_no_enviados;
      $errorOrdenCompra_seccion = 'Correos (' . $seccion . ' - ' . $metodoPago . ')';
      $errorOrdenCompra_proveedor = '----';
      $errorOrdenCompra_numero_almacen = '----';
      $errorOrdenCompra_nombre_almacen = '----';
      $errorOrdenCompra_moneda_remision = '----';
      $errorOrdenCompra_cliente_id = $idUsuario;
      $errorOrdenCompra_codigo_cliente = $codigoUsuario;
      $errorOrdenCompra_remisiones_eliminar = '----';
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // PROCESOS
  if($proceso_correcto){
    $respuesta = '3'; // EXITO

    $op = $metodoPago === 'Depósito/Transferencia' ? '1' : '2';
    $link = 'estado-compra/' . $op . '/' . $ordenCompra;
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL GENERAR ALGUNA REMISION O AL HACER UNA CONSULTA, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if($errorOrdenCompra_existe){
    try{
      $Conn_Admin->pdo->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, nombreProveedor, idAlmacen, nombreAlmacen, monedaRemision, idCliente, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :nombreProveedor, :idAlmacen, :nombreAlmacen, :monedaRemision, :idCliente, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorOrdenCompra_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorOrdenCompra_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':nombreProveedor', $errorOrdenCompra_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':idAlmacen', $errorOrdenCompra_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorOrdenCompra_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorOrdenCompra_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $errorOrdenCompra_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorOrdenCompra_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorOrdenCompra_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $Conn_Admin->pdo->commit();
    }catch(PDOException $error){
      $Conn_Admin->pdo->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de las remisiones.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL MODIFICAR LAS EXISTENCIAS, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if($errorModificarExistencias_existe){
    try{
      $Conn_Admin->pdo->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, nombreProveedor, idAlmacen, nombreAlmacen, monedaRemision, idCliente, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :nombreProveedor, :idAlmacen, :nombreAlmacen, :monedaRemision, :idCliente, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorModificarExistencias_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorModificarExistencias_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':nombreProveedor', $errorModificarExistencias_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':idAlmacen', $errorModificarExistencias_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorModificarExistencias_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorModificarExistencias_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $errorModificarExistencias_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorModificarExistencias_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorModificarExistencias_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $Conn_Admin->pdo->commit();
    }catch (PDOException $error){
      $Conn_Admin->pdo->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de modificar las existencias.";
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // EN CASO DE HABER ERRORES AL ELIMINAR LOS PRODUCTOS DEL CARRITO, SE REGISTRA EN LA BITACORA CORRESPONDIENTE LO SIGUIENTE
  if($seccion === 'carrito' && $errorEliminarProductosCarrito_existe){
    try{
      $Conn_Admin->pdo->beginTransaction();
      $fechaRegistro = date("Y-m-d H:i:s");

      $sql = "INSERT INTO __bitacora_errores_orden_compra(estadoProceso, mensajeError, seccion, nombreProveedor, idAlmacen, nombreAlmacen, monedaRemision, idCliente, codigoCliente, remisionesEliminar, fechaRegistro) VALUES('Error', :mensajeError, :seccion, :nombreProveedor, :idAlmacen, :nombreAlmacen, :monedaRemision, :idCliente, :codigoCliente, :remisionesEliminar, :fechaRegistro)";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':mensajeError', $errorEliminarProductosCarrito_mensaje_error, PDO::PARAM_STR);
      $stmt->bindParam(':seccion', $errorEliminarProductosCarrito_seccion, PDO::PARAM_STR);
      $stmt->bindParam(':nombreProveedor', $errorEliminarProductosCarrito_proveedor, PDO::PARAM_STR);
      $stmt->bindParam(':idAlmacen', $errorEliminarProductosCarrito_numero_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $errorEliminarProductosCarrito_nombre_almacen, PDO::PARAM_STR);
      $stmt->bindParam(':monedaRemision', $errorEliminarProductosCarrito_moneda_remision, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $errorEliminarProductosCarrito_cliente_id, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $errorEliminarProductosCarrito_codigo_cliente, PDO::PARAM_STR);
      $stmt->bindParam(':remisionesEliminar', $errorEliminarProductosCarrito_remisiones_eliminar, PDO::PARAM_STR);
      $stmt->bindParam(':fechaRegistro', $fechaRegistro, PDO::PARAM_STR);
      $stmt->execute();

      $Conn_Admin->pdo->commit();
    }catch(PDOException $error){
      $Conn_Admin->pdo->rollBack();
      //$respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      //$mensaje = "No se guardó la información correspondiente al error de eliminar los productos del carrito.";
    }
  }

  unset($_POST['accion']);
  unset($_POST['seccion']);
  unset($_POST['metodo_envio']);
  unset($_POST['costo_envio_total']);
  unset($_POST['metodo_pago']);
  unset($_POST['total_precio_productos']);
  unset($_POST['total_pagar']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje, 'link' => $link ];
  echo json_encode($json);
}
?>