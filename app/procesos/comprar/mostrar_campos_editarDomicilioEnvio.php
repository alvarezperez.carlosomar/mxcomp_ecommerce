<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  require_once dirname(__DIR__, 2) . '/clases/usuario/metodos_usuario.php';
  require_once dirname(__DIR__, 2) . '/clases/direcciones/metodos_direcciones.php';

  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_exitoso = false;
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario = new Usuario($idUsuario, $codigoUsuario);
    $direccionUsuario = new Direccion($idUsuario, $codigoUsuario);
    $proceso_exitoso = true;
  }else{
    $mensaje = "No es numérico";
    $proceso_exitoso = false;
  }

  if($proceso_exitoso){
    if($usuario->buscarUsuario()){
      $proceso_exitoso = true;
    }else{
      $mensaje = $usuario->buscarUsuario_mensaje;
      $proceso_exitoso = false;
    }
  }

  if($proceso_exitoso){
    if($direccionUsuario->ver_domicilioEnvio()){
      $nombreDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_desencriptado;
      $tipoVialidad = $direccionUsuario->ver_domEnv_tipoVialidad;
      $nombreVialidad = $direccionUsuario->ver_domEnv_nombreVialidad;
      $noExterior = $direccionUsuario->ver_domEnv_noExterior === 'S/N' ? NULL : $direccionUsuario->ver_domEnv_noExterior;
      $noInterior = $direccionUsuario->ver_domEnv_noInterior === 'S/N' ? NULL : $direccionUsuario->ver_domEnv_noInterior;
      $codigoPostal = $direccionUsuario->ver_domEnv_codigoPostal;
      $colonia = $direccionUsuario->ver_domEnv_colonia;
      $ciudadMunicipio = $direccionUsuario->ver_domEnv_ciudadMunicipio;
      $nombreEstado = $direccionUsuario->ver_domEnv_nombreEstado;
      $entreCalle1 = $direccionUsuario->ver_domEnv_entreCalle1;
      $entreCalle2 = $direccionUsuario->ver_domEnv_entreCalle2;
      $referenciasAdicionales = $direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado;
      $noTelefonico = $direccionUsuario->ver_domEnv_noTelefonico_desencriptado;
      $horaEntregaEnvio_1 = $direccionUsuario->ver_domEnv_horaEntregaEnvio1;
      $horaEntregaEnvio_2 = $direccionUsuario->ver_domEnv_horaEntregaEnvio2;
      $proceso_exitoso = true;
    }else{
      $mensaje = $direccionUsuario->ver_domEnv_mensaje;
      $proceso_exitoso = false;
    }
  }

  if($proceso_exitoso){
    $html = '
  <h4>EDITAR DATOS DE ENVÍO</h4>
  <div class="p-comprar-modal_cont_campos">
    <p class="p-text p-text_help p-text_help_margin">
      <span>Edite los datos en caso de ser necesario, ya que este domicilio será usada para enviar su pedido.</span>
    </p>
    <p class="p-text p-text_help p-text_help_margin p-text-align_right">
      <span>* Obligatorio</span>
    </p>

    <form id="g-domiEnvio-form_editar">
      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Nombre del destinatario (con apellidos):</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-nombre_destinatario_input" placeholder="Nombre del destinatario (con apellidos)" value="' . $nombreDestinatario . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-nombre_destinatario_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo letras y espacios, por favor.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-nombre_destinatario_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Tipo de vialidad:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-domiEnvio-tipo_vialidad_div">
                <select class="p-input g-form_select g-domiEnvio-tipo_vialidad_select">
                  <option value="">Selecciona el tipo</option>';

    try{
      $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
        $selected = $tipoVialidad === trim($datos_tipoVialidad['nombreTipoVialidad']) ? 'selected="selected"' : '';

        $html .= '
                  <option ' . $selected . ' value="' . trim($datos_tipoVialidad['id']) . '">' . trim($datos_tipoVialidad['nombreTipoVialidad']) . '</option>';
      }
    }catch(PDOException $error){
      //$mensaje_error = 'Error: ' . $error->getMessage();
      $mensaje_error = 'Problema al buscar los tipos de vialidad';
      $html .= '
                  <option value="">' . $mensaje_error . '</option>';
    }

    $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-domiEnvio-tipo_vialidad_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Este tipo de vialidad no existe.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-tipo_vialidad_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado un tipo de vialidad.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Nombre de vialidad:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-nombre_vialidad_input" placeholder="Nombre de vialidad" value="' . $nombreVialidad . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-nombre_vialidad_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-nombre_vialidad_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. exterior:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">';

    if(is_null($noExterior)){
      $html .= '
              <input type="text" class="p-input g-form-noExterior_input g-domiEnvio-no_exterior_input" placeholder="S/N" autocomplete="off" disabled>';
    }else{
      $html .= '
              <input type="text" class="p-input g-form-noExterior_input g-domiEnvio-no_exterior_input" placeholder="No. exterior" value="' . $noExterior . '" autocomplete="off">';
    }

    $html .= '
            </div>
            <p class="p-text p-text_info g-form-noExterior_alert_info g-domiEnvio-no_exterior_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
            </p>
            <p class="p-text p-text_error g-form-noExterior_alert_error g-domiEnvio-no_exterior_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>';

    if(is_null($noExterior)){
      $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-domiEnvio-no_exterior_checkbox" checked>
            <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Agregar no. exterior</label>';
    }else{
      $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-domiEnvio-no_exterior_checkbox">
            <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>';
    }

    $html .= '
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. interior:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">';

    if(is_null($noInterior)){
      $html .= '
              <input type="text" class="p-input g-form-noInterior_input g-domiEnvio-no_interior_input" placeholder="S/N" autocomplete="off" disabled>';
    }else{
      $html .= '
              <input type="text" class="p-input g-form-noInterior_input g-domiEnvio-no_interior_input" placeholder="No. interior" value="' . $noInterior . '" autocomplete="off">';
    }

    $html .= '
            </div>
            <p class="p-text p-text_info g-form-noInterior_alert_info g-domiEnvio-no_interior_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
            </p>
            <p class="p-text p-text_error g-form-noInterior_alert_error g-domiEnvio-no_interior_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>';

    if(is_null($noInterior)){
      $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-domiEnvio-no_interior_checkbox" checked>
            <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Agregar no. interior</label>';
    }else{
      $html .= '
            <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-domiEnvio-no_interior_checkbox">
            <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>';
    }

    $html .= '
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Código postal:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-codigo_postal_input" placeholder="Código postal" value="' . $codigoPostal . '" autocomplete="off" maxlength="5">
            </div>
            <p class="p-text p-text_info g-domiEnvio-codigo_postal_alert_info_1">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo números, por favor.</span>
            </p>
            <p class="p-text p-text_info g-domiEnvio-codigo_postal_alert_info_2">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>El código postal debe de contener 5 números.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-codigo_postal_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Colonia:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-colonia_input" placeholder="Colonia" value="' . $colonia . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-colonia_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-colonia_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Ciudad o municipio:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-ciudad_municipio_input" placeholder="Ciudad o municipio" value="' . $ciudadMunicipio . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-ciudad_municipio_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo letras y espacios, por favor.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-ciudad_municipio_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Estado:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <div class="p-control">
              <div class="p-select g-domiEnvio-estado_div">
                <select class="p-input g-form_select g-domiEnvio-estado_select">
                  <option value="">Selecciona tu estado</option>';

    try{
      $sql = "SELECT id, nombreEstado FROM __estados_codigos";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
        $selected = $nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';

        $html .= '
                  <option ' . $selected . ' value="' . trim($datos_estado['id']) . '">' . trim($datos_estado['nombreEstado']) . '</option>';
      }
    }catch(PDOException $error){
      //$mensaje_error = 'Error: ' . $error->getMessage();
      $mensaje_error = 'Problema al buscar los estados';
      $html .= '
                  <option value="">' . $mensaje_error . '</option>';
    }

    $html .= '
                </select>
              </div>
            </div>
            <p class="p-text p-text_info g-domiEnvio-estado_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Este estado no existe.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-estado_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>No has seleccionado un estado.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Entre calles:</span>
              <span class="p-text p-text_help">(Opcional)</span>
            </label>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-entre_calles_1_input" placeholder="Calle #1" value="' . $entreCalle1 . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-entre_calles_1_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-entre_calles_1_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>La calle #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
            </p>
            <div class="p-control p-control_marginTop">
              <input type="text" class="p-input g-domiEnvio-entre_calles_2_input" placeholder="Calle #2" value="' . $entreCalle2 . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-entre_calles_2_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-entre_calles_2_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>La calle #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Referencias adicionales:</span>
              <span class="p-text p-text_help">(Opcional)</span>
            </label>
            <div class="p-control">
              <textarea class="p-input p-textarea g-domiEnvio-referencias_adicionales_textarea" placeholder="Ejemplo: frente a tienda...">' . $referenciasAdicionales . '</textarea>
            </div>
            <p class="p-text p-text_info g-domiEnvio-referencias_adicionales_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>No. telefónico:</span>
              <span class="p-text p-text_help">*</span>
            </label>
            <p class="p-text p-text_help">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
            <div class="p-control">
              <input type="text" class="p-input g-domiEnvio-no_telefonico_input" placeholder="No. telefónico" value="' . $noTelefonico . '" autocomplete="off" maxlength="10">
            </div>
            <p class="p-text p-text_info g-domiEnvio-no_telefonico_alert_info_1">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>Sólo números, por favor.</span>
            </p>
            <p class="p-text p-text_info g-domiEnvio-no_telefonico_alert_info_2">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>El número telefónico debe de contener 10 dígitos.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-no_telefonico_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>Este campo se encuentra vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-columnas p-field_marginBottom">
        <div class="p-columna">
          <div class="p-field">
            <label class="p-label p-text_p">
              <span>Rango horario de entrega:</span>
              <span class="p-text p-text_help">(Opcional)</span>
            </label>
            <div class="p-control">
              <input type="time" class="p-input p-date g-domiEnvio-rango_horario_entrega_1_time" value="' . $horaEntregaEnvio_1 . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-rango_horario_entrega_1_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>La hora no tiene el formato permitido.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-rango_horario_entrega_1_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>La hora de entrega #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
            </p>
            <div class="p-control p-control_marginTop">
              <input type="time" class="p-input p-date g-domiEnvio-rango_horario_entrega_2_time" value="' . $horaEntregaEnvio_2 . '" autocomplete="off">
            </div>
            <p class="p-text p-text_info g-domiEnvio-rango_horario_entrega_2_alert_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span>La hora no tiene el formato permitido.</span>
            </p>
            <p class="p-text p-text_error g-domiEnvio-rango_horario_entrega_2_alert_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span>La hora de entrega #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
            </p>
          </div>
        </div>
      </div>

      <div class="p-field_marginTop p-buttons p-buttons_right p-comprar-modal_derecho_buttons">
        <button type="submit" class="p-button p-button_info g-domiEnvio-guardar_cambios_button">
          <span>
            <i class="fas fa-save"></i>
          </span>
          <span><b>Guardar cambios</b></span>
        </button>
        <a class="p-button p-button_delete" id="id-comprar-modal_button_cancelar">
          <span>
            <i class="fas fa-times"></i>
          </span>
          <span><b>Cancelar</b></span>
        </a>
      </div>
    </form>
  </div>';

    $mensaje = $html;
  }

  $respuesta = "1";

  unset($usuario);
  unset($direccionUsuario);
  unset($Conn_mxcomp);
  unset($_POST['accion']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>