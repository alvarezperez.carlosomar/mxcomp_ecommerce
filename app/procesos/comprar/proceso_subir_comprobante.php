<?php
if(isset($_POST['accion']) && $_POST['accion'] === 'subir'){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $codigoUsuario = desencriptar($_POST['codigo_usuario']);
  $id_ordenCompra = desencriptar($_POST['ID_orden_compra']);
  $ordenCompra = desencriptar($_POST['orden_compra']);
  $oportunidades_comprobante = desencriptar($_POST['oportunidades_comprobante']);

  $Conn_Admin = new Conexion_admin();

  $array_formato_archivos = ['jpg', 'jpeg', 'png', 'pdf'];
  $proceso_correcto = false;
  $mensaje = '';

  if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
    $archivo_size = $_FILES['archivo']['size'];
    $archivo_extension = pathinfo($_FILES['archivo']['name'])['extension'];

    if(in_array($archivo_extension, $array_formato_archivos)){
      if($archivo_size < 5242880){
        $proceso_correcto = true;
      }else{
        $respuesta = '0';
        $mensaje_texto = 'La extensíon del archivo es .' . $archivo_extension . ', pero supera los 5 MB.';
        $proceso_correcto = false;
      }
    }else{
      $respuesta = '0';
      $mensaje_texto = 'La extensión no es válida, sólo se permite jpg/jpeg, png o pdf.';
      $proceso_correcto = false;
    }
  }else{
    $respuesta = '0';
    $mensaje_texto = 'No se cargó el comprobante, vuelve a hacerlo.';
    $proceso_correcto = false;
  }

  if($proceso_correcto){
    $carpeta_usuario = 'clientes/' . $codigoUsuario;
    $carpeta_usuario_comprobante = $carpeta_usuario . '/comprobante_pago';
    $ubicacionComprobantePago = $carpeta_usuario_comprobante . '/';
    $dirname = '../../';

    if(!file_exists($dirname . $carpeta_usuario)){
      mkdir($dirname . $carpeta_usuario, 0777, true);
    }

    if(!file_exists($dirname . $carpeta_usuario_comprobante)){
      mkdir($dirname . $carpeta_usuario_comprobante, 0777, true);
    }

    $nombre_comprobante = 'comprobante_OC' . $ordenCompra . '.' . $archivo_extension;
    $ubicacion_comprobante = $dirname . $ubicacionComprobantePago . $nombre_comprobante;

    $esImagen = $archivo_extension === 'pdf' ? '0' : '1';

    if(move_uploaded_file($_FILES['archivo']['tmp_name'], $ubicacion_comprobante)){
      $proceso_correcto = true;
    }else{
      $respuesta = '0';
      $mensaje_texto = 'El archivo no se subió, vuelve a realizar el proceso.';
      $proceso_correcto = false;
    }
  }

  if($proceso_correcto){
    $comprobantePago = 1;
    $oportunidades_comprobante = (int) $oportunidades_comprobante - 1;

    try{
      $Conn_Admin->pdo->beginTransaction();

      $sql = "UPDATE __orden_compra_datos_pago SET codigoUsuario_tipoGestorPagos = NULL, nombreUsuario_nombreGestorPagos = NULL, comprobantePago = :comprobantePago, ubicacionComprobantePago = :ubicacionComprobantePago, nombreComprobante = :nombreComprobante, esImagen = :esImagen, oportunidadesComprobante = :oportunidadesComprobante, comprobanteRechazado = NULL, fechaProceso = NULL WHERE ordenCompra = :ordenCompra AND codigoCliente = :codigoCliente";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':comprobantePago', $comprobantePago, PDO::PARAM_INT);
      $stmt->bindParam(':ubicacionComprobantePago', $ubicacionComprobantePago, PDO::PARAM_STR);
      $stmt->bindParam(':nombreComprobante', $nombre_comprobante, PDO::PARAM_STR);
      $stmt->bindParam(':esImagen', $esImagen, PDO::PARAM_STR);
      $stmt->bindParam(':oportunidadesComprobante', $oportunidades_comprobante, PDO::PARAM_INT);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/enviar_comprobante.php';

      $fechaProceso = date("Y-m-d H:i:s");
      $fechaProceso = fecha_con_hora($fechaProceso);

      $codigoUsuario_encriptado = encriptar($codigoUsuario);

      $nombreUsuario = desencriptar_con_clave($_SESSION['__nombre_usu__'], $codigoUsuario_encriptado);
      $link_detallesVenta = HOST_LINK_ADMIN . 'ordenes-compra/detalles/' . $id_ordenCompra . '/' . $ordenCompra . '/' . $codigoUsuario;
      $archivo_comprobante = $ubicacionComprobantePago . $nombre_comprobante;

      $enviar_comprobante = new Correo_enviarComprobante($ordenCompra, $fechaProceso, $nombreUsuario, $codigoUsuario, $link_detallesVenta, $archivo_comprobante);

      $enviar_comprobante->enviarCorreo();

      $respuesta = '1';
      $proceso_correcto = true;

      $Conn_Admin->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_Admin->pdo->rollBack();
      $respuesta = '0';
      //$mensaje_texto = 'Error: ' . $error->getMessage();
      $mensaje_texto = 'No se pudo completar la subida del comprobante de pago, vuelve a intentarlo. Si sigue apareciendo este mensaje, contacta a atención a clientes.';
      $proceso_correcto = false;

      unlink($ubicacion_comprobante);
    }
  }

  if($respuesta === '0'){
    $mensaje = '
      <label class="p-text p-text-span_compra_detalles">
        <span>' . $mensaje_texto . '</span>
        <span>
          <i class="fas fa-times-circle"></i>
        </span>
      </label>';
  }

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>