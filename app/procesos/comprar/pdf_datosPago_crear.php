<?php
function gethtml() {
  session_start();
  $dirname = dirname(__DIR__, 2);

  require_once $dirname . '/funciones/fecha_hora_formatos.php';
  require_once $dirname . '/funciones/encriptacion.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

  $fechaCompra = (string) date("Y-m-d H:i:s");

  $fecha_inicio_pagina = '2019';
  $fecha_footer = date('Y') > $fecha_inicio_pagina ? $fecha_inicio_pagina . '-' . date('Y') : $fecha_inicio_pagina;

  $ordenCompra = (string) $_SESSION['pdf_datos']['orden_compra'];
  $pagoCompra = (float) $_SESSION['pdf_datos']['pago_compra'];

  $texto_compra = '$ ' . number_format($pagoCompra, 2) . ' MXN';

  unset($_SESSION['pdf_datos']);

  $html = '
    <html>
      <body>
        <header class="cabecera">
          <div class="logo" width="90">
            <img src="' . $dirname . '/images/logos_pdf/logo_mxcomp.jpg" alt="">
          </div>
          <div class="compania_datos">
            <div>MXcomp; la tecnología en tus manos S.A. de C.V.</div>
            <div><b>RFC:</b> MTT200210NP9</div>
            <div><b>' . fecha($fechaCompra) . ', ' . hora($fechaCompra) . '</b></div>
            <div><b>Compra #' . desencriptar($ordenCompra) . '</b></div>
          </div>
        </header>

        <main>
          <h3 class="titulo"><b>A depositar/transferir</b></h3>
          <p class="parrafo" id="marginBottom_40px"><b>' . $texto_compra . '</b></p>

          <h3 class="titulo"><b>Datos para pago</b></h3>

          <p class="parrafo" id="marginBottom_5px"><b>Titular:</b> MXCOMP; LA TECNOLOGÍA EN TUS MANOS S.A. DE C.V.</p>
          <p class="parrafo"><b>Domicilio fiscal:</b> Calle Río Galindo No. 4-A, Col. San Cayetano, C.P. 76806, San Juan del Río, Qro.</p>

          <p class="parrafo" id="marginTop_20px"><b>Institución bancaria:</b> Santander</p>
          <p class="parrafo" id="marginBottom_5px"><b>No. de tarjeta:</b> 5579 0890 0171 8835</p>
          <p class="parrafo" id="marginBottom_5px"><b>No. de cuenta:</b> 65508000346</p>
          <p class="parrafo" id="marginBottom_5px"><b>CLABE:</b> 014685655080003469</p>
          <p class="parrafo"><b>Sucursal:</b> 7896</p>

          <p class="parrafo" id="marginTop_20px"><b>Institución bancaria:</b> BBVA</p>
          <p class="parrafo" id="marginBottom_5px"><b>No. de tarjeta:</b> 4555 1130 0745 9516</p>
          <p class="parrafo" id="marginBottom_5px"><b>No. de cuenta:</b> 0115706890</p>
          <p class="parrafo" id="marginBottom_5px"><b>CLABE:</b> 012685001157068909</p>
          <p class="parrafo" id="marginBottom_40px"><b>Sucursal:</b> 0485</p>

          <h3 class="titulo"><b>Puedes hacerlo en estos establecimientos</b></h3>
          <div id="marginBottom_40px">
            <img src="' . $dirname . '/images/logos_pdf/logo_SPEI.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_Telecomm-Mexico.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_OXXO.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_7-Eleven.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_Circle-K.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_Extra.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_Farmacias-Guadalajara.jpg" alt="" style="margin: .5rem; height: 25px;">
            <img src="' . $dirname . '/images/logos_pdf/logo_Farmacias-del-ahorro.jpg" alt="" style="margin: .5rem; height: 25px;">
          </div>

          <p class="parrafo">Después de hacer tu pago, entra a <b>Mi cuenta>Mis compras</b>, selecciona tu compra y sube el comprobante que te entregaron en el establecimiento.</p>

          <p class="parrafo" id="marginTop_20px">Cualquier duda o aclaración, contáctanos en <b><u>atencionaclientes@mxcomp.com.mx</u></b> o en los siguientes teléfonos: <b>55 8796 5174 / 427 148 0756</b></p>

          <p class="parrafo" id="marginTop_20px"><b>Gracias por tu compra.</b></p>

          <p class="parrafo" id="marginBottom_40px"><b>Atte. Equipo de MXcomp</b></p>
        </main>
        <footer>
          <small><b>Copyright © ' . $fecha_footer . ' MXcomp. Todos los derechos reservados.</b></small>
        </footer>
      </body>
    </html>';

  return $html;
}
?>