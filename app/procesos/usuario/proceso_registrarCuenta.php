<?php
if(isset($_POST['accion']) && $_POST['accion'] === "registrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_password.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  require_once dirname(__DIR__, 2) . '/funciones/creacion_token.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $nombreS = trim($_POST['nombres']);
  $apellidoPaterno = trim($_POST['apellido_paterno']);
  $apellidoMaterno = trim($_POST['apellido_materno']);
  $correo = trim($_POST['correo']);
  $password = trim($_POST['password']);
  $confirmarPassword = trim($_POST['confirmar_password']);
  $sexo = trim($_POST['sexo']);
  $fechaNacimiento = trim($_POST['fecha_nacimiento']);
  $noTelefonico1 = trim($_POST['no_telefonico_1']);
  $noTelefonico2 = trim($_POST['no_telefonico_2']);
  $noTelefonico3 = trim($_POST['no_telefonico_3']);
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $tipoVialidad = "";
  $nombreVialidad = trim($_POST['nombre_vialidad']);
  $noExterior = trim($_POST['no_exterior']);
  $noExteriorEstadoCheck = trim($_POST['no_exterior_estadoCheck']);
  $noInterior = trim($_POST['no_interior']);
  $noInteriorEstadoCheck = trim($_POST['no_interior_estadoCheck']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $idEstado = trim($_POST['estado']);
  $nombreEstado = "";
  $entreCalle1 = trim($_POST['entre_calle_1']);
  $entreCalle2 = trim($_POST['entre_calle_2']);
  $referenciasAdicionales = trim($_POST['referencias_adicionales']);
  $recaptcha = trim($_POST['captcha']);
  $termCond_aviPriv = trim($_POST['termCond_aviPriv']);

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';

  // REVISA EL NOMBRE
  if($nombreS !== "" && validar_campo_letras_espacios($nombreS)){
    $nombreS = (string) $nombreS;
    $proceso_correcto = true;
  }else if($nombreS === ""){
    $respuesta = "1"; // El campo "Nombre(s)" se encuentra vacío
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // El Nombre(s) no cumple con la expresion regular
    $proceso_correcto = false;
  }

  // REVISA EL APELLIDO PATERNO
  if($proceso_correcto){
    if($apellidoPaterno !== "" && validar_campo_letras_espacios($apellidoPaterno)){
      $apellidoPaterno = (string) $apellidoPaterno;
      $proceso_correcto = true;
    }else if($apellidoPaterno === ""){
      $respuesta = "3"; // El campo "Apellido paterno" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // El Apellido paterno no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL APELLIDO MATERNO
  if($proceso_correcto){
    if($apellidoMaterno !== "" && validar_campo_letras_espacios($apellidoMaterno)){
      $apellidoMaterno = (string) $apellidoMaterno;
      $proceso_correcto = true;
    }else if($apellidoMaterno === ""){
      $respuesta = "5"; // El campo "Apellido materno" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "6"; // El Apellido materno no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL CORREO ELECTRONICO
  if($proceso_correcto){
    if($correo !== "" && validar_correo($correo)){
      $correo = (string) $correo;

      try{
        $sql = "SELECT COUNT(id) FROM __usuarios WHERE correo = :correo";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':correo', $correo, PDO::PARAM_STR);
        $stmt->execute();
        $correo_registrado = (int) $stmt->fetchColumn();

        if($correo_registrado === 0){
          $proceso_correcto = true;
        }else{
          $respuesta = "9"; // El Correo electronico ya se encuentra registrado
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0"; // ESTA RESPUESTA SERÁ MANEJADA PARA ERRORES QUE SE GENEREN
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al revisar si el correo existe, recarga la página o ponte en contacto con atención a clientes.";
        $proceso_correcto = false;
      }
    }else if($correo === ""){
      $respuesta = "7"; // El campo "Correo electronico" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "8"; // El Correo electronico no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LA CONTRASEÑA
  if($proceso_correcto){
    if($password !== "" && validar_password($password)){
      $password = (string) $password;
      $proceso_correcto = true;
    }else if($password === ""){
      $respuesta = "10"; // El campo "Contraseña" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "11"; // La Contraseña no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA CONFIRMAR CONTRASEÑA Y ADEMAS DE QUE SI SON IDENTICAS
  if($proceso_correcto){
    if($confirmarPassword !== "" && validar_password($confirmarPassword)){
      $confirmarPassword = (string) $confirmarPassword;

      if($password === $confirmarPassword){
        $proceso_correcto = true;
      }else{
        $respuesta = "14"; // Las contraseñas no son identicas
        $proceso_correcto = false;
      }
    }else if($confirmarPassword === ""){
      $respuesta = "12"; // El campo "Confirmar contraseña" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "13"; // La Contraseña a confirmar no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL SEXO
  if($proceso_correcto){
    if($sexo !== "" && ($sexo === "Hombre" || $sexo === "Mujer")){
      $sexo = (string) $sexo;
      $proceso_correcto = true;
    }else if($sexo === ""){
      $respuesta = "15"; // No se selecciono el Sexo
      $proceso_correcto = false;
    }else{
      $respuesta = "16"; // Este tipo de Sexo no existe
      $proceso_correcto = false;
    }
  }

  // REVISA LA FECHA DE NACIMIENTO
  if($proceso_correcto){
    if($fechaNacimiento !== "" && validar_campo_fecha($fechaNacimiento)){
      $fechaNacimiento = (string) $fechaNacimiento;
      $proceso_correcto = true;
    }else{
      $respuesta = "17"; // No se ha escrito/seleccionado una Fecha de nacimiento o no respeta la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LOS NUMEROS TELEFONICOS
  if($proceso_correcto){
    if($noTelefonico1 !== "" && validar_campo_numerico($noTelefonico1)){
      $noTelefonico1 = (string) $noTelefonico1;
      
      if(mb_strlen($noTelefonico1) === 10){
        $noTelefonico2_correcto = false;
        $noTelefonico3_correcto = false;

        if($noTelefonico2 !== ""){
          if(validar_campo_numerico($noTelefonico2)){
            $noTelefonico2 = (string) $noTelefonico2;
            
            if(mb_strlen($noTelefonico2) === 10){
              $noTelefonico2_correcto = true;
            }else{
              $respuesta = "22"; // El campo No. Telefonico #2 no tiene 10 digitos
              $proceso_correcto = false;
            }
          }else{
            $respuesta = "21"; // El No. telefonico #2 no cumple con la expresion regular
            $proceso_correcto = false;
          }
        }else{
          $noTelefonico2_correcto = true;
        }

        if($noTelefonico2_correcto){
          if($noTelefonico3 !== ""){
            if(validar_campo_numerico($noTelefonico3)){
              $noTelefonico3 = (string) $noTelefonico3;
              
              if(mb_strlen($noTelefonico2) === 10){
                $noTelefonico3_correcto = true;
              }else{
                $respuesta = "24"; // El campo No. Telefonico #3 no tiene 10 digitos
                $proceso_correcto = false;
              }
            }else{
              $respuesta = "23"; // El No. telefonico #3 no cumple con la expresion regular
              $proceso_correcto = false;
            }
          }else{
            $noTelefonico3_correcto = true;
          }
        }

        if($noTelefonico2_correcto && $noTelefonico3_correcto){
          if($noTelefonico2 === "" && $noTelefonico3 !== ""){
            $noTelefonico2 = $noTelefonico3;
            $noTelefonico3 = "";
          }
          if($noTelefonico2 === ""){
            $noTelefonico2 = NULL;
          }
          if($noTelefonico3 === ""){
            $noTelefonico3 = NULL;
          }

          $proceso_correcto = true;
        }
      }else{
        $respuesta = "20"; // El campo "No. Telefonico #1" no tiene 10 digitos
        $proceso_correcto = false;
      }
    }else if($noTelefonico1 === ""){
      $respuesta = "18"; // El campo "No. Telefonico #1" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "19"; // El No. Telefonico #1 no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL TIPO DE VIALIDAD
  if($proceso_correcto){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreTipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];

        if($tipoVialidad_existe === 1){
          $tipoVialidad = (string) $datos_tipoVialidad['nombreTipoVialidad'];
          $proceso_correcto = true;
        }else{
          $respuesta = "26"; // El Tipo de vialidad no existe
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0"; // ESTA RESPUESTA SERÁ MANEJADA PARA ERRORES QUE SE GENEREN
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar los tipos de vialidad.";
        $proceso_correcto = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = "25"; // No se ha seleccionado un Tipo de vialidad
      $proceso_correcto = false;
    }else{
      $respuesta = "26"; // El Tipo de vialidad no existe
      $proceso_correcto = false;
    }
  }

  // REVISA EL NOMBRE DE VIALIDAD
  if($proceso_correcto){
    if($nombreVialidad !== "" && validar_campo_letras_espacios_simbolos($nombreVialidad)){
      $nombreVialidad = (string) $nombreVialidad;
      $proceso_correcto = true;
    }else if($nombreVialidad === ""){
      $respuesta = "27"; // El campo "Nombre de vialidad" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "28"; // El Nombre de vialidad no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL NUMERO EXTERIOR
  if($proceso_correcto){
    if(($noExterior !== "" && $noExteriorEstadoCheck === "false" && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === "true")){
      $noExterior = (string) $noExterior;
      $proceso_correcto = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === "false"){
      $respuesta = "29"; // El campo "No. exterior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "30"; // El No. exterior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_correcto = false;
    }
  }

  // REVISA EL NUMERO INTERIOR
  if($proceso_correcto){
    if(($noInterior !== "" && $noInteriorEstadoCheck === "false" && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === "true")){
      $noInterior = (string) $noInterior;
      $proceso_correcto = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === "false"){
      $respuesta = "31"; // El campo "No. interior" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "32"; // El No. interior no cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_correcto = false;
    }
  }

  // REVISA EL CÓDIGO POSTAL
  if($proceso_correcto){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(mb_strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_correcto = true;
      }else{
        $respuesta = "35"; // El Codigo Postal no tiene 5 numeros
        $proceso_correcto = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = "33"; // El campo "Codigo Postal" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "34"; // El Codigo Postal no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LA COLONIA
  if($proceso_correcto){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_correcto = true;
    }else if($colonia === ""){
      $respuesta = "36"; // El campo "Colonia" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "37"; // La Colonia no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_correcto){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_correcto = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = "38"; // El campo "Ciudad o Municipio" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "39"; // La Ciudad o Municipio no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }

  // REVISA EL ESTADO
  if($proceso_correcto){
    if($idEstado !== "" && validar_campo_numerico($idEstado)){
      $idEstado = (int) $idEstado;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nombreEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $idEstado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];

        if($estado_existe === 1){
          $nombreEstado = (string) $datos_estado['nombreEstado'];
          $proceso_correcto = true;
        }else{
          $respuesta = "41"; // Este Estado no existe
          $proceso_correcto = false;
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0"; // ESTA RESPUESTA SERÁ MANEJADA PARA ERRORES QUE SE GENEREN
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Existe un problema al buscar el Estado.";
        $proceso_correcto = false;
      }
    }else if($idEstado === ""){
      $respuesta = "40"; // No se ha seleccionado un Estado
      $proceso_correcto = false;
    }else{
      $respuesta = "41"; // Este Estado no existe
      $proceso_correcto = false;
    }
  }

  // REVISA LOS CAMPOS ENTRE CALLES #1 Y #2
  if($proceso_correcto){
    $entreCalle1_correcto = false;
    $entreCalle2_correcto = false;

    if($entreCalle1 === "" && $entreCalle2 === ""){
      $entreCalle1 = NULL;
      $entreCalle2 = NULL;
      $proceso_correcto = true;
    }else{
      if($entreCalle1 !== ""){
        if(validar_campo_letras_espacios_simbolos($entreCalle1)){
          $entreCalle1 = (string) $entreCalle1;
          $entreCalle1_correcto = true;
        }else{
          $respuesta = "43"; // La Calle #1 no cumple con la expresion regular
          $entreCalle1_correcto = false;
        }
      }else{
        $entreCalle1 = NULL;
        $respuesta = "42"; // La Calle #1 esta vacía
        $entreCalle1_correcto = false;
      }

      if($entreCalle1_correcto){
        if($entreCalle2 !== ""){
          if(validar_campo_letras_espacios_simbolos($entreCalle2)){
            $entreCalle2 = (string) $entreCalle2;
            $entreCalle2_correcto = true;
          }else{
            $respuesta = "45"; // La Calle #2 no cumple con la expresion regular
            $entreCalle2_correcto = false;
          }
        }else{
          $entreCalle2 = NULL;
          $respuesta = "44"; // La Calle #2 esta vacía
          $entreCalle2_correcto = false;
        }
      }

      if($entreCalle1_correcto && $entreCalle2_correcto){
        $proceso_correcto = true;
      }else{
        $proceso_correcto = false;
      }
    }
  }

  // REVISA LAS REFERENCIAS ADICIONALES
  if($proceso_correcto){
    if($referenciasAdicionales !== ""){
      if(validar_campo_letras_espacios_simbolos($referenciasAdicionales)){
        $referenciasAdicionales = (string) $referenciasAdicionales;
        $proceso_correcto = true;
      }else{
        $respuesta = "46"; // Las Referencias adicionales no cumplen con la expresion regular
        $proceso_correcto = false;
      }
    }else{
      $referenciasAdicionales = NULL;
      $proceso_correcto = true;
    }
  }

  // REVISA QUE LOS TÉRMINOS Y CONDICIONES, ASI COMO EL AVISO DE PRIVACIDAD SEAN ACEPTADOS
  if($proceso_correcto){
    if($termCond_aviPriv === "true"){
      $proceso_correcto = true;
    }else{
      $respuesta = "47"; // No has aceptado el Aviso de privacidad.
      $proceso_correcto = false;
    }
  }

  // REVISA QUE EL RECAPTCHA DEVUELVA QUE ERES HUMANO
  if($proceso_correcto){
    if(isset($recaptcha) && $recaptcha){
      $secret_key = SECRET_KEY;
      $ip = $_SERVER['REMOTE_ADDR'];
      $validation_server = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response=' . $recaptcha . '&remoteip=' . $ip);
      $valido = json_decode($validation_server);

      if($valido->success === true && $valido->score >= 0.5 && $valido->action === "user_register"){
        $proceso_correcto = true;
      }else{
        $respuesta = "49"; // No se puede hacer el registro. Se detectó que no eres humano.
        $proceso_correcto = false;
      }
    }else{
      $respuesta = "48"; // No se recibió información del recaptcha de Google, revisa tu conexión a Internet y vuelve a realizar tu registro.
      $proceso_correcto = false;
    }
  }
  
  // REALIZA TODO EL PROCESO REGISTRAR LA INFORMACION DE LA CUENTA
  if($proceso_correcto){
    ////////// GENERAR TOKEN PARA ACTIVACION DE CUENTA //////////
    $token = token_30();

    $array_LetrasAcentos = array('á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú');
    $array_LetrasSinAcentos = array('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

    $array_vocales = array('A', 'E', 'I', 'O', 'U');
    $array_numerosVocales = array('4', '3', '1', '0', '7');

    // REEMPLAZA LAS LETRAS ACENTUADAS
    $nomSinAcento = str_replace($array_LetrasAcentos, $array_LetrasSinAcentos, $nombreS);
    $apePaternoSinAcento = str_replace($array_LetrasAcentos, $array_LetrasSinAcentos, $apellidoPaterno);
    $apeMaternoSinAcento = str_replace($array_LetrasAcentos, $array_LetrasSinAcentos, $apellidoMaterno);

    // DIVIDIR NOMBRE EN ARRAY
    $arrayNombre = explode(" ", $nomSinAcento);

    if(array_key_exists(1, $arrayNombre)){
      $letra1 = mb_strtoupper(mb_substr($arrayNombre[0], 0, 1)); // UNA LETRA
      $letra2 = mb_strtoupper(mb_substr($arrayNombre[1], 0, 1)); // UNA LETRA
      $nombre_Siglas = $letra1 . $letra2; // SE UNEN LAS DOS LETRAS
    }else{
      $nombre_Siglas = mb_strtoupper(mb_substr($arrayNombre[0], 0, 2)); // DOS PRIMERAS LETRAS
    }

    $apePat_Siglas = mb_strtoupper(mb_substr($apePaternoSinAcento, 0, 2)); // DOS PRIMERAS LETRAS
    $apeMat_Siglas = mb_strtoupper(mb_substr($apeMaternoSinAcento, 0, 1)); // UNA LETRA

    // REMPLAZAR VOCALES POR NUMEROS
    $apePat_Siglas = str_replace($array_vocales, $array_numerosVocales, $apePat_Siglas);
    $apeMat_Siglas = str_replace($array_vocales, $array_numerosVocales, $apeMat_Siglas);
    $nombre_Siglas = str_replace($array_vocales, $array_numerosVocales, $nombre_Siglas);

    $codigoUsuario_Siglas = $apePat_Siglas . $apeMat_Siglas . $nombre_Siglas;

    $codigoUsuario_nuevo = "";
    
    try{
      $Conn_mxcomp->pdo->beginTransaction();

      $datoArmadoCodigo = (string) $codigoUsuario_Siglas . '%';

      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY codigoUsuario LIKE :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoUsuario', $datoArmadoCodigo, PDO::PARAM_STR);
      $stmt->execute();
      $cantidad_codigosUsuario = (int) $stmt->fetchColumn();

      if($cantidad_codigosUsuario === 0){
        $codigoUsuario_nuevo = (string) $codigoUsuario_Siglas . '-1';
      }else{
        $sql = "SELECT codigoUsuario FROM __usuarios WHERE BINARY codigoUsuario LIKE :codigoUsuario ORDER BY codigoUsuario DESC LIMIT 1";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':codigoUsuario', $datoArmadoCodigo, PDO::PARAM_STR);
        $stmt->execute();
        $codigoUsuario = (string) $stmt->fetchColumn();

        $numeracion = explode('-', $codigoUsuario);
        $numeracion = (int) $numeracion[1];
        $nueva_numeracion = $numeracion + 1;
        $codigoUsuario_nuevo = (string) $codigoUsuario_Siglas . '-' . $nueva_numeracion;
      }

      ////////// GENERAR CONTRASEÑA ENCRIPTADA //////////
      $token_Password = token_4();

      //ENCRIPTAMOS EL CODIGO DE USUARIO
      $codigoUsuario_encriptado = encriptar($codigoUsuario_nuevo);

      $passwordEncriptada = encriptar_con_clave($password, $codigoUsuario_encriptado);
      $unionTokenPass = $token_Password . $passwordEncriptada;

      $pass = (string) mb_substr($unionTokenPass, 0, mb_strlen($unionTokenPass) / 2);
      $salt = (string) mb_substr($unionTokenPass, mb_strlen($unionTokenPass) / 2, mb_strlen($unionTokenPass));

      $fechaCreacionCuenta = date("Y-m-d H:i:s");
      $fechaEspera = date("Y-m-d H:i:s", strtotime('+24 hours'));
      $fechaEliminacionInfo = date("Y-m-d H:i:s", strtotime('+7 days'));

      $noExterior = $noExterior === '' ? 'S/N' : $noExterior;
      $noInterior = $noInterior === '' ? 'S/N' : $noInterior;

      $nombreDestinatario = (string) $nombreS . ' ' . $apellidoPaterno . ' ' . $apellidoMaterno;

      //ENCRIPTAMOS LOS CAMPOS DEL USUARIO
      $nombreS_encriptado = encriptar_con_clave($nombreS, $codigoUsuario_encriptado);
      $apellidoPaterno = encriptar_con_clave($apellidoPaterno, $codigoUsuario_encriptado);
      $apellidoMaterno = encriptar_con_clave($apellidoMaterno, $codigoUsuario_encriptado);
      $correo_encriptado = encriptar($correo);
      $fechaNacimiento = encriptar_con_clave($fechaNacimiento, $codigoUsuario_encriptado);
      $noTelefonico1 = encriptar_con_clave($noTelefonico1, $codigoUsuario_encriptado);

      if(!is_null($noTelefonico2)){
        $noTelefonico2 = encriptar_con_clave($noTelefonico2, $codigoUsuario_encriptado);
      }

      if(!is_null($noTelefonico3)){
        $noTelefonico3 = encriptar_con_clave($noTelefonico3, $codigoUsuario_encriptado);
      }

      $sql = "INSERT INTO __usuarios(codigoUsuario, nombreS, apellidoPaterno, apellidoMaterno, correo, pass_word, ssap_revision, sexo, fechaNacimiento, noTelefonico1, noTelefonico2, noTelefonico3, cuentaActiva, token, fechaEsperaProceso, fechaEliminacionInfo, fechaCreacion) VALUES (:codigoUsuario, :nombreS, :apellidoPaterno, :apellidoMaterno, :correo, :pass_word, :ssap_revision, :sexo, :fechaNacimiento, :noTelefonico1, :noTelefonico2, :noTelefonico3, 0, :token, :fechaEsperaProceso, :fechaEliminacionInfo, :fechaCreacion)";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario_nuevo, PDO::PARAM_STR);
      $stmt->bindParam(':nombreS', $nombreS_encriptado, PDO::PARAM_STR);
      $stmt->bindParam(':apellidoPaterno', $apellidoPaterno, PDO::PARAM_STR);
      $stmt->bindParam(':apellidoMaterno', $apellidoMaterno, PDO::PARAM_STR);
      $stmt->bindParam(':correo', $correo_encriptado, PDO::PARAM_STR);
      $stmt->bindParam(':pass_word', $pass, PDO::PARAM_STR);
      $stmt->bindParam(':ssap_revision', $salt, PDO::PARAM_STR);
      $stmt->bindParam(':sexo', $sexo, PDO::PARAM_STR);
      $stmt->bindParam(':fechaNacimiento', $fechaNacimiento, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico1', $noTelefonico1, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico2', $noTelefonico2, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico3', $noTelefonico3, PDO::PARAM_STR);
      $stmt->bindParam(':token', $token, PDO::PARAM_STR);
      $stmt->bindParam(':fechaEsperaProceso', $fechaEspera, PDO::PARAM_STR);
      $stmt->bindParam(':fechaEliminacionInfo', $fechaEliminacionInfo, PDO::PARAM_STR);
      $stmt->bindParam(':fechaCreacion', $fechaCreacionCuenta, PDO::PARAM_STR);
      $stmt->execute();

      $ID_insertado = (int) $Conn_mxcomp->pdo->lastInsertId();
      $fechaCreacionCuenta = date("Y-m-d H:i:s");

      //ENCRIPTAMOS LOS CAMPOS DE LAS DIRECCIONES
      $nombreVialidad = encriptar_con_clave($nombreVialidad, $codigoUsuario_encriptado);
      $noExterior = encriptar_con_clave($noExterior, $codigoUsuario_encriptado);
      $noInterior = encriptar_con_clave($noInterior, $codigoUsuario_encriptado);
      $codigoPostal = encriptar_con_clave($codigoPostal, $codigoUsuario_encriptado);
      $colonia = encriptar_con_clave($colonia, $codigoUsuario_encriptado);
      $ciudadMunicipio = encriptar_con_clave($ciudadMunicipio, $codigoUsuario_encriptado);
      $nombreEstado = encriptar_con_clave($nombreEstado, $codigoUsuario_encriptado);

      if(!is_null($entreCalle1)){
        $entreCalle1 = encriptar_con_clave($entreCalle1, $codigoUsuario_encriptado);
      }

      if(!is_null($entreCalle2)){
        $entreCalle2 = encriptar_con_clave($entreCalle2, $codigoUsuario_encriptado);
      }

      if(!is_null($referenciasAdicionales)){
        $referenciasAdicionales = encriptar_con_clave($referenciasAdicionales, $codigoUsuario_encriptado);
      }

      $tipoDireccion = 'particular';

      $sql = "INSERT INTO __direcciones (idUsuario, codigoUsuario, tipoDireccion, idTipoVialidad, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, idEstado, nombreEstado, entreCalle1, entreCalle2, referenciasAdicionales, fechaCreacion) VALUES (:idUsuario, :codigoUsuario, :tipoDireccion, :idTipoVialidad, :tipoVialidad, :nombreVialidad, :noExterior, :noInterior, :codigoPostal, :colonia, :ciudadMunicipio, :idEstado, :nombreEstado, :entreCalle1, :entreCalle2, :referenciasAdicionales, :fechaCreacion)";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $ID_insertado, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario_nuevo, PDO::PARAM_STR);
      $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
      $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
      $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
      $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
      $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
      $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
      $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
      $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
      $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle1', $entreCalle1, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle2', $entreCalle2, PDO::PARAM_STR);
      $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
      $stmt->bindParam(':fechaCreacion', $fechaCreacionCuenta, PDO::PARAM_STR);
      $stmt->execute();

      $fechaCreacionCuenta = date("Y-m-d H:i:s");

      $tipoDireccion = 'envio';
      $nombreDestinatario = encriptar_con_clave($nombreDestinatario, $codigoUsuario_encriptado);

      $sql = "INSERT INTO __direcciones (idUsuario, codigoUsuario, tipoDireccion, idTipoVialidad, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, idEstado, nombreEstado, entreCalle1, entreCalle2, referenciasAdicionales, noTelefonico, nombreDestinatario, fechaCreacion) VALUES (:idUsuario, :codigoUsuario, :tipoDireccion, :idTipoVialidad, :tipoVialidad, :nombreVialidad, :noExterior, :noInterior, :codigoPostal, :colonia, :ciudadMunicipio, :idEstado, :nombreEstado, :entreCalle1, :entreCalle2, :referenciasAdicionales, :noTelefonico, :nombreDestinatario, :fechaCreacion)";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $ID_insertado, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario_nuevo, PDO::PARAM_STR);
      $stmt->bindParam(':tipoDireccion', $tipoDireccion, PDO::PARAM_STR);
      $stmt->bindParam(':idTipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
      $stmt->bindParam(':tipoVialidad', $tipoVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':nombreVialidad', $nombreVialidad, PDO::PARAM_STR);
      $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
      $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
      $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
      $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
      $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
      $stmt->bindParam(':idEstado', $idEstado, PDO::PARAM_INT);
      $stmt->bindParam(':nombreEstado', $nombreEstado, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle1', $entreCalle1, PDO::PARAM_STR);
      $stmt->bindParam(':entreCalle2', $entreCalle2, PDO::PARAM_STR);
      $stmt->bindParam(':referenciasAdicionales', $referenciasAdicionales, PDO::PARAM_STR);
      $stmt->bindParam(':noTelefonico', $noTelefonico1, PDO::PARAM_STR);
      $stmt->bindParam(':nombreDestinatario', $nombreDestinatario, PDO::PARAM_STR);
      $stmt->bindParam(':fechaCreacion', $fechaCreacionCuenta, PDO::PARAM_STR);
      $stmt->execute();

      $proceso_correcto = true;
      $Conn_mxcomp->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "0"; // ESTA RESPUESTA SERÁ MANEJADA PARA ERRORES QUE SE GENEREN
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Existe un problema al registrar la cuenta, recarga la página para volver a hacer el proceso o ponte en contacto con atención a clientes.";
      $proceso_correcto = false;
    }
  }
  
  // ENVÍA EL CORREO AL CLIENTE
  if($proceso_correcto){
    require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/activar_cuenta.php';

    $linkActivacion = HOST_LINK . 'activar-cuenta/' . $correo . '/' . $token;
    $fechaEsperaProceso = fecha_con_hora($fechaEspera);

    $correo_activarCuenta = new Correo_activarCuenta($nombreS, $fechaEsperaProceso, $linkActivacion, $correo);

    $correo_activarCuenta->enviarCorreo();
    $respuesta = "50"; //TODO BIEN
  }

  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['nombres']);
  unset($_POST['apellido_paterno']);
  unset($_POST['apellido_materno']);
  unset($_POST['correo']);
  unset($_POST['password']);
  unset($_POST['confirmar_password']);
  unset($_POST['sexo']);
  unset($_POST['fecha_nacimiento']);
  unset($_POST['no_telefonico_1']);
  unset($_POST['no_telefonico_2']);
  unset($_POST['no_telefonico_3']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nombre_vialidad']);
  unset($_POST['no_exterior']);
  unset($_POST['no_exterior_estadoCheck']);
  unset($_POST['no_interior']);
  unset($_POST['no_interior_estadoCheck']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  unset($_POST['entre_calle_1']);
  unset($_POST['entre_calle_2']);
  unset($_POST['referencias_adicionales']);
  unset($_POST['captcha']);
  unset($_POST['termCond_aviPriv']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>