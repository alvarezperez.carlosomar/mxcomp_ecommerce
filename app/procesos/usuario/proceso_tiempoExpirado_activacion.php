<?php
if(isset($_POST['accion']) && $_POST['accion'] === "enviar"){
  include dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  include dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  include dirname(__DIR__, 2) . '/funciones/creacion_token.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $correo = trim($_POST['correo']);
  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA EL CORREO
  if($correo !== "" && validar_correo($correo)){
    $proceso_correcto = true;
  }else if($correo === ""){
    $respuesta = "1"; // SE ENCUENTRA VACÍO EL CAMPO
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // NO ES UN CORREO VÁLIDO
    $proceso_correcto = false;
  }
  
  // ENVIA EL CORREO DE ACTIVACION, VUELVE A GENERAR EL TOKEN Y LOS REGISTROS
  if($proceso_correcto){
    try{
      // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
      $correo_consulta = encriptar($correo);

      $Conn_mxcomp->pdo->beginTransaction();
      
      $sql = "SELECT COUNT(id) AS conteo, id, codigoUsuario, nombreS FROM __usuarios WHERE correo = :correo";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $registro_existe = (int) $datos_usuario['conteo'];
      
      if($registro_existe === 1){
        $idUsuario = (int) $datos_usuario['id'];
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];
        
        $fechaEsperaProceso = date("Y-m-d H:i:s", strtotime('+24 hours'));
        $fechaEliminacionInfo = date("Y-m-d H:i:s", strtotime('+7 days'));
        $fechaActualizacion = date("Y-m-d H:i:s");
        
        //ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar(trim($codigoUsuario));
        $nombreS = desencriptar_con_clave(trim($datos_usuario['nombreS']), $codigoUsuario_encriptado);
        
        $token = token_30();
        
        require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/activar_cuenta.php';

        $linkActivacion = HOST_LINK . 'activar-cuenta/' . $correo . '/' . $token;
        $fechaEspera = fecha_con_hora($fechaEsperaProceso);

        $correo_activarCuenta = new Correo_activarCuenta($nombreS, $fechaEspera, $linkActivacion, $correo);

        if($correo_activarCuenta->enviarCorreo()){
          $sql = "UPDATE __usuarios SET token = :token, fechaEsperaProceso = :fechaEsperaProceso, fechaEliminacionInfo = :fechaEliminacionInfo, fechaActualizacion = :fechaActualizacion WHERE id = :id AND codigoUsuario = :codigoUsuario AND correo = :correo";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':token', $token, PDO::PARAM_STR);
          $stmt->bindParam(':fechaEsperaProceso', $fechaEsperaProceso, PDO::PARAM_STR);
          $stmt->bindParam(':fechaEliminacionInfo', $fechaEliminacionInfo, PDO::PARAM_STR);
          $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
          $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
          $stmt->execute();
          
          $respuesta = "5"; // SE ENVIO EL CORREO ELECTRONICO
        }else{
          $respuesta = "4"; // NO SE PUDO ENVIAR EL CORREO
        }
      }else{
        $respuesta = "3"; // EL CORREO NO SE ENCUENTRA REGISTRADO
      }
      
      $Conn_mxcomp->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "0"; // ERROR EN ALGUNA CONSULTA
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar al usuario";
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['correo']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>