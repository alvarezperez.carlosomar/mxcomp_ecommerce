<?php
if(isset($_POST['accion']) && $_POST['accion'] === "iniciar"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_password.php';
  include dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  if(!isset($_SESSION['__intentos_inicio_sesion__'])){
    $_SESSION['__intentos_inicio_sesion__'] = 0;
  }

  $correo = trim($_POST['correo']);
  $password = trim($_POST['password']);
  
  $Conn_mxcomp = new Conexion_mxcomp();
  $guardar_fecha_bloqueo = false;
  $proceso_correcto = false;
  $mensaje_intentos = '';
  $mensaje = '';
  
  // REVISA EL CORREO ELECTRONICO
  if($correo !== "" && validar_correo($correo)){
    $correo = (string) $correo;
    $correo = encriptar($correo); // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
    $proceso_correcto = true;
  }else if($correo === ""){
    $respuesta = "1"; // El campo "Correo electronico" se encuentra vacio
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // El Correo electrónico no cumple con la expresion regular
    $proceso_correcto = false;
  }
  
  // REVISA LA PASSWORD
  if($proceso_correcto){
    if($password !== "" && validar_password($password)){
      $password = (string) $password;
      $proceso_correcto = true;
    }else if($password === ""){
      $respuesta = "3"; // El campo "Contraseña" se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // La Contraseña no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LOS TIEMPOS DE BLOQUEO PARA INICIO DE SESION
  if($proceso_correcto){
    // VERIFICAMOS QUE LOS INTENTOS YA SON MAS DE 5, LO CUAL SIGNIFICA QUE ESTA BLOQUEADO EL INICIO DE SESION
    if($_SESSION['__intentos_inicio_sesion__'] === 6){
      $fechaActual = date("Y-m-d H:i:s");

      // SI LA FECHA ACTUAL ES MAYOR A LA FECHA DE BLOQUEO, QUIERE DECIR QUE LA SESSION SE DEBE DE DESBLOQUEAR
      // SE REINICIAN LOS INTENTOS DE INICIO DE SESION Y SE ELIMINA LA VARIABLE QUE CONTIENE LA FECHA DE BLOQUEO
      if($fechaActual > $_SESSION['__fechaEsperaInicioSesion']){
        $_SESSION['__intentos_inicio_sesion__'] = 0;
        unset($_SESSION['__fechaEsperaInicioSesion']);
      }
    }

    // SI LOS INTENTOS DE INICIO DE SESION SON MENOR DE 5, SE HACE LO SIGUIENTE
    if($_SESSION['__intentos_inicio_sesion__'] < 5){
      $proceso_correcto = true;
    }else{
      $respuesta = "8";
      $mensaje_intentos = '
        <div class="p-notification_contenedor g-iniciarSesion-notification_error">
          <div class="p-notification p-notification_letter_error">
            <span>
              <b>Has terminado con tus intentos de inicio de sesión permitidos, espera 2 minutos e inténtalo de nuevo.</b>
            </span>
          </div>
        </div>';
      $proceso_correcto = false;
    }
  }

  // REVISA QUE LA CUENTA EXISTA Y SE ENCUENTRE ACTIVA
  if($proceso_correcto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, id, codigoUsuario, nombreS, pass_word, ssap_revision, cuentaActiva FROM __usuarios WHERE BINARY correo = :correo";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':correo', $correo, PDO::PARAM_STR);
      $stmt->execute();
      $datos_cuenta = $stmt->fetch(PDO::FETCH_ASSOC);

      $cuentaExiste = (int) trim($datos_cuenta['conteo']);
      $idUsuario = (int) trim($datos_cuenta['id']);
      $codigoUsuario = (string) trim($datos_cuenta['codigoUsuario']);
      $nombreS = (string) trim($datos_cuenta['nombreS']);
      $cuentaActiva = (int) trim($datos_cuenta['cuentaActiva']);

      if($cuentaExiste === 1){
        if($cuentaActiva === 1){
          ///////////// OBTENER LA PASSWORD ENCRIPTADA /////////////

          // UNIMOS LAS PARTES DE LAS CONTRASEÑAS DE LA BASE DE DATOS
          $passwordBD_token = (string) trim($datos_cuenta['pass_word']) . trim($datos_cuenta['ssap_revision']);
          // QUITAMOS LOS PRIMEROS 4 CARACTERES
          $passwordBD_original = mb_substr($passwordBD_token, 4);
          // ENCRIPTAMOS EL CODIGO DE USUARIO
          $codigoUsuario_encriptado = encriptar($codigoUsuario);
          // ENCRIPTAMOS LA PASSWORD DEL FORMULARIO CON EL CODIGO DE USUARIO ENCRIPTADO
          $passwordForm_encriptada = encriptar_con_clave($password, $codigoUsuario_encriptado);

          ///////////// FINALIZAMOS OBTENER LA PASSWORD ENCRIPTADA /////////////

          // SI LAS CONTRASEÑAS SON IDÉNTICAS
          if($passwordForm_encriptada === $passwordBD_original){
            $proceso_correcto = true;
          }else{
            $intentos = 5 - ((int) $_SESSION['__intentos_inicio_sesion__'] + 1);

            if($intentos === 0){
              $respuesta = "8";
              $mensaje_intentos = '
                <div class="p-notification_contenedor g-iniciarSesion-notification_error">
                  <div class="p-notification p-notification_letter_error">
                    <span>
                      <b>Has terminado con tus intentos de inicio de sesión permitidos, espera 2 minutos e inténtalo de nuevo.</b>
                    </span>
                  </div>
                </div>';

              $guardar_fecha_bloqueo = true;
            }else{
              $respuesta = "5"; // MENSAJE DE CORREO O CONTRASEÑA NO REGISTRADO
              $mensaje = '
                <div class="p-notification_contenedor g-iniciarSesion-alert_error">
                  <div class="p-notification p-notification_error">
                    <span>
                      <b>No puedes iniciar sesión, revisa tu correo electrónico o contraseña, por favor.</b>
                    </span>
                  </div>
                </div>';

              $mensaje_intentos = '
                <div class="p-notification_contenedor g-iniciarSesion-notification_error">
                  <div class="p-notification p-notification_letter_error">
                    <span>
                      <b>Te quedan ' . $intentos . ' de 5 intentos, después se bloqueará el inicio de sesión por 2 minutos.</b>
                    </span>
                  </div>
                </div>';
            }
            
            $_SESSION['__intentos_inicio_sesion__'] += 1;
            $proceso_correcto = false;
          }
        }else{
          $respuesta = "6"; // MENSAJE DE QUE NO SE HA ACTIVADO LA CUENTA
          $mensaje = '
            <div class="p-notification_contenedor g-iniciarSesion-alert_info">
              <div class="p-notification p-notification_info">
                <span>
                  <b>No puedes iniciar sesión porque no has activado tu cuenta de MXcomp. Ve al link que se envio por correo para activarla.</b>
                </span>
              </div>
            </div>';
          $proceso_correcto = false;
        }
      }else{
        $intentos = 5 - ((int) $_SESSION['__intentos_inicio_sesion__'] + 1);
        
        if($intentos === 0){
          $respuesta = "8";
          $mensaje_intentos = '
            <div class="p-notification_contenedor g-iniciarSesion-notification_error">
              <div class="p-notification p-notification_letter_error">
                <span>
                  <b>Has terminado con tus intentos de inicio de sesión permitidos, espera 2 minutos e inténtalo de nuevo.</b>
                </span>
              </div>
            </div>';

          $guardar_fecha_bloqueo = true;
        }else{
          $respuesta = "5"; // MENSAJE DE CORREO O CONTRASEÑA NO REGISTRADO
          $mensaje = '
            <div class="p-notification_contenedor g-iniciarSesion-alert_error">
              <div class="p-notification p-notification_error">
                <span>
                  <b>La cuenta no existe, revisa tu correo electrónico o contraseña, por favor.</b>
                </span>
              </div>
            </div>';

          $mensaje_intentos = '
            <div class="p-notification_contenedor g-iniciarSesion-notification_error">
              <div class="p-notification p-notification_letter_error">
                <span>
                  <b>Te quedan ' . $intentos . ' de 5 intentos, después se bloqueará el inicio de sesión por 2 minutos.</b>
                </span>
              </div>
            </div>';
        }
        
        $_SESSION['__intentos_inicio_sesion__'] += 1;
        $proceso_correcto = false;
      }

      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "9"; // MENSAJE PARA CUANDO HAYA UN ERROR EN ALGUNA CONSULTA
      //$error_texto = "Error: " . $error->getMessage();
      $error_texto = 'Problema al buscar el usuario, revisa tu información, si sigue saliendo este mensaje ponte en contacto con atención a clientes';

      $mensaje = '
        <div class="p-notification_contenedor g-iniciarSesion-alert_error">
          <div class="p-notification p-notification_error">
            <span>
              <b>' . $error_texto . '</b>
            </span>
          </div>
        </div>';

      $proceso_correcto = false;
    }
  }
  
  // SE REGISTRA EL ACCESO Y SE REVISA SI HAY PRODUCTOS EN EL CARRITO TEMPORAL
  if($proceso_correcto){
    try{
      $fechaActual = date("Y-m-d H:i:s");
      $Conn_mxcomp->pdo->beginTransaction();

      $sql = "UPDATE __usuarios SET fechaUltimoIngreso = :fechaUltimoIngreso, fechaActualizacion = :fechaActualizacion WHERE id = :id AND codigoUsuario = :codigoUsuario AND correo = :correo";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':fechaUltimoIngreso', $fechaActual, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
      $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':correo', $correo, PDO::PARAM_STR);
      $stmt->execute();

      //////////////////////////////////////////////////////////////////////////////////////////////////////
      // SE REVISA SI TIENE PRODUCTOS EN EL CARRITO

      if(isset($_SESSION['__carrito__'])){
        foreach($_SESSION['__carrito__'] as $nombreAlmacen=>$productos_carritoAlmacen){
          foreach($productos_carritoAlmacen as $informacion_producto){
            $Producto_codigoProducto = (string) $informacion_producto['codigoProducto'];
            $Producto_skuProveedor = (string) $informacion_producto['skuProveedor'];
            $Producto_descripcion = (string) $informacion_producto['descripcion'];
            $Producto_descripcionURL = (string) $informacion_producto['descripcionURL'];
            $Producto_nombreMarca = (string) $informacion_producto['nombreMarca'];
            $Producto_precioProveedor = (string) $informacion_producto['precioProveedor'];
            $Producto_monedaProveedor = (string) $informacion_producto['monedaProveedor'];
            $Producto_precioMXcomp = (string) $informacion_producto['precioMXcomp'];
            $Producto_monedaMXcomp = (string) $informacion_producto['monedaMXcomp'];
            $Producto_unidades = (int) $informacion_producto['unidades'];
            $Producto_numeroAlmacen = (string) $informacion_producto['numeroAlmacen'];
            $Producto_existenciaAlmacen = (int) $informacion_producto['existenciaAlmacen'];
            $Producto_existenciaTotalProducto = (int) $informacion_producto['existenciaTotalProducto'];
            $Producto_nombreProveedor = (string) $informacion_producto['nombreProveedor'];
            $Producto_tieneImagen = (string) $informacion_producto['tieneImagen'];
            $Producto_numeroUbicacionImagen = $informacion_producto['numeroUbicacionImagen'];
            $Producto_nombreImagen = (string) $informacion_producto['nombreImagen'];
            $Producto_versionImagen = $informacion_producto['versionImagen'];
            $Producto_envioGratisPermitido = (string) $informacion_producto['envioGratisPermitido'];

            $Producto_numeroUbicacionImagen = is_null($Producto_numeroUbicacionImagen) ? NULL : trim($Producto_numeroUbicacionImagen);
            $Producto_versionImagen = is_null($Producto_versionImagen) ? NULL : trim($Producto_versionImagen);

            $Producto_nombreAlmacen = (string) $nombreAlmacen;
            $Producto_guardado = "0";

            $sql = "SELECT COUNT(id) AS conteo, id, unidades FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':codigoProducto', $Producto_codigoProducto, PDO::PARAM_STR);
            $stmt->bindParam(':numeroAlmacen', $Producto_numeroAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $Producto_nombreAlmacen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
            $stmt->execute();
            $datos_carrito = $stmt->fetch(PDO::FETCH_ASSOC);
            $productoCarrito_existe = (int) $datos_carrito['conteo'];
            $id_producto = (int) $datos_carrito['id'];

            if($productoCarrito_existe === 1){
              $cantidad_nueva = (int) $datos_carrito['unidades'] + $Producto_unidades;

              $sql = "UPDATE __carrito SET descripcion = :descripcion, descripcionURL = :descripcionURL, nombreMarca = :nombreMarca, precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, unidades = :unidades, tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, nombreImagen = :nombreImagen, versionImagen = :versionImagen, envioGratisPermitido = :envioGratisPermitido, guardado = :guardado, fechaActualizacion = :fechaActualizacion WHERE id = :id AND idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioProveedor', $Producto_precioProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':monedaProveedor', $Producto_monedaProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':precioMXcomp', $Producto_precioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':unidades', $cantidad_nueva, PDO::PARAM_INT);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':envioGratisPermitido', $Producto_envioGratisPermitido, PDO::PARAM_STR);
              $stmt->bindParam(':guardado', $Producto_guardado, PDO::PARAM_STR);
              $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
              $stmt->bindParam(':id', $id_producto, PDO::PARAM_INT);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':codigoProducto', $Producto_codigoProducto, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $Producto_numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $Producto_nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
              $stmt->execute();
            }else{
              $tieneExistencias = "1";
              $registroExiste = "1";

              $sql = "INSERT INTO __carrito(idUsuario, codigoUsuario, codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, unidades, numeroAlmacen, nombreAlmacen, existenciaAlmacen, existenciaTotalProducto, nombreProveedor, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, envioGratisPermitido, tieneExistencias, guardado, registroExiste, fechaCreacion) 
              VALUES (:idUsuario, :codigoUsuario, :codigoProducto, :skuProveedor, :descripcion, :descripcionURL, :nombreMarca, :precioProveedor, :monedaProveedor, :precioMXcomp, :monedaMXcomp, :unidades, :numeroAlmacen, :nombreAlmacen, :existenciaAlmacen, :existenciaTotalProducto, :nombreProveedor, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :envioGratisPermitido, :tieneExistencias, :guardado, :registroExiste, :fechaCreacion)";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':codigoProducto', $Producto_codigoProducto, PDO::PARAM_STR);
              $stmt->bindParam(':skuProveedor', $Producto_skuProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
              $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
              $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
              $stmt->bindParam(':precioProveedor', $Producto_precioProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':monedaProveedor', $Producto_monedaProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':precioMXcomp', $Producto_precioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':unidades', $Producto_unidades, PDO::PARAM_INT);
              $stmt->bindParam(':numeroAlmacen', $Producto_numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $Producto_nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':existenciaAlmacen', $Producto_existenciaAlmacen, PDO::PARAM_INT);
              $stmt->bindParam(':existenciaTotalProducto', $Producto_existenciaTotalProducto, PDO::PARAM_INT);
              $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
              $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
              $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
              $stmt->bindParam(':envioGratisPermitido', $Producto_envioGratisPermitido, PDO::PARAM_STR);
              $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
              $stmt->bindParam(':guardado', $Producto_guardado, PDO::PARAM_STR);
              $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
              $stmt->bindParam(':fechaCreacion', $fechaActual, PDO::PARAM_STR);
              $stmt->execute();
            }
          }
        }
      }

      $_SESSION['__id__'] = encriptar($idUsuario);
      $_SESSION['__nombre_usu__'] = $nombreS;
      $_SESSION['__codigo_usu__'] = encriptar($codigoUsuario);

      $respuesta = "7"; //TODO BIEN
      $mensaje = '
        <div class="p-notification_contenedor g-iniciarSesion-alert_success">
          <div class="p-notification p-notification_success p-notification_loading">
            <span class="p-notification_span_load">
              <b>Iniciando, espere por favor ...</b>
            </span>
          </div>
        </div>';

      unset($_SESSION['__intentos_inicio_sesion__']);
      unset($_SESSION['__carrito__']);

      $Conn_mxcomp->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conn_mxcomp->pdo->rollBack();
      $respuesta = "9"; // MENSAJE PARA CUANDO HAYA UN ERROR EN ALGUNA CONSULTA
      //$error_texto = "Error: " . $error->getMessage();
      $error_texto = 'Problema al iniciar sesion, vuelve a dar clic en enviar. Si sigue saliendo este mensaje ponte en contacto con atención a clientes';

      $mensaje = '
        <div class="p-notification_contenedor g-iniciarSesion-alert_error">
          <div class="p-notification p-notification_error">
            <span>
              <b>' . $error_texto . '</b>
            </span>
          </div>
        </div>';
    }
  }
  
  // SI ES TRUE SE GUARDA LA FECHA Y HORA DEL MOMENTO QUE SE BLOQUEÓ EL ACCESO
  if($guardar_fecha_bloqueo){
    $fechaEsperaInicioSesion = date("Y-m-d H:i:s", strtotime('+2 minute'));
    $_SESSION['__fechaEsperaInicioSesion'] = $fechaEsperaInicioSesion;
    $_SESSION['__intentos_inicio_sesion__'] += 1;
    $guardar_fecha_bloqueo = false;
  }
  
  unset($_POST['accion']);
  unset($_POST['correo']);
  unset($_POST['password']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje, 'mensaje_intentos' => $mensaje_intentos ];
  echo json_encode($json);
}
?>