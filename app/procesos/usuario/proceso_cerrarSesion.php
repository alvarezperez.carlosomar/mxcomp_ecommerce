<?php
if(isset($_POST['accion']) && $_POST['accion'] === "cerrar"){
  session_start();
  session_destroy();
  include dirname(__DIR__, 2) . '/global/config.php';
  
  unset($_POST);
  $_POST = array();
  unset($_GET);
  $_GET = array();
  
  $json = [ 'respuesta' => '1', 'url' => HOST_LINK ];
  echo json_encode($json);
}
?>