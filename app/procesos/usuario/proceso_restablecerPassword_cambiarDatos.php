<?php
if(isset($_POST['accion']) && $_POST['accion'] === "restablecer"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_password.php';
  include dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  include dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  include dirname(__DIR__, 2) . '/funciones/creacion_token.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $nuevaPassword = trim($_POST['nueva_password']);
  $confirmarPassword = trim($_POST['confirmar_password']);
  $correo = trim($_POST['correo']);
  $token = trim($_POST['token']);
  
  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA LA NUEVA CONTRASEÑA
  if($nuevaPassword !== "" && validar_password($nuevaPassword)){
    $nuevaPassword = (string) $nuevaPassword;
    $proceso_correcto = true;
  }else if($nuevaPassword === ""){
    $respuesta = "1"; // El campo "Nueva contraseña" se encuentra vacío
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // La nueva contraseña no cumple con la expresion regular
    $proceso_correcto = false;
  }
  
  // REVISA LA CONFIRMACIÓN DE LA NUEVA CONTRASEÑA
  if($proceso_correcto){
    if($confirmarPassword !== "" && validar_password($confirmarPassword)){
      $confirmarPassword = (string) $confirmarPassword;
      $proceso_correcto = true;
    }else if($confirmarPassword === ""){
      $respuesta = "3"; // El campo "Confirmar contraseña" se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // El campo "Confirmar contraseña" no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA QUE LAS CONTRASEÑAS COINCIDAN
  if($proceso_correcto){
    if($nuevaPassword === $confirmarPassword){
      $proceso_correcto = true;
    }else{
      $respuesta = "5"; // Las contraseñas no son idénticas
      $proceso_correcto = false;
      $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_info">
      <span>
        <b>Las contraseñas no coinciden, revísalas por favor.</b>
      </span>
    </div>
  </div>';
    }
  }
  
  // REVISA QUE EL CORREO Y EL TOKEN SEAN VÁLIDOS
  if($proceso_correcto){
    if( ($correo !== "" && validar_correo($correo)) && ($token !== "" && validar_token($token)) ){
      $proceso_correcto = true;
    }else{
      $respuesta = "5";
      $proceso_correcto = false;
      $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_error">
      <span>
        <b>El correo y el token, no son válidos.</b>
      </span>
    </div>
  </div>';
    }
  }
  
  if($proceso_correcto){
    // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
    $correo_consulta = encriptar($correo);

    try{
      $sql = "SELECT COUNT(id) AS conteo, id, codigoUsuario, nombreS, cuentaActiva, fechaEsperaProceso FROM __usuarios WHERE correo = :correo AND token = :token";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
      $stmt->bindParam(':token', $token, PDO::PARAM_STR);
      $stmt->execute();
      $datos_cuenta = $stmt->fetch(PDO::FETCH_ASSOC);
      $cuenta_existe = (int) $datos_cuenta['conteo'];

      if($cuenta_existe === 1){
        $cuenta_activa = (int) $datos_cuenta['cuentaActiva'];

        if($cuenta_activa === 1){
          $fechaEsperaProceso = (string) $datos_cuenta['fechaEsperaProceso'];
          $fechaActual = date("Y-m-d H:i:s");

          if($fechaActual <= $fechaEsperaProceso){
            $idUsuario = (int) $datos_cuenta['id'];
            $codigoUsuario = (string) $datos_cuenta['codigoUsuario'];

            $codigoUsuario_encriptado = encriptar($codigoUsuario);
            
            $nombreS = desencriptar_con_clave(trim($datos_cuenta['nombreS']), $codigoUsuario_encriptado);
            
            $password_encriptada = encriptar_con_clave($nuevaPassword, $codigoUsuario_encriptado);
            $token_password = token_4();
            $password_nueva = $token_password . $password_encriptada;

            $pass = (string) mb_substr($password_nueva, 0, mb_strlen($password_nueva) / 2);
            $salt = (string) mb_substr($password_nueva, mb_strlen($password_nueva) / 2, mb_strlen($password_nueva));
            $fechaActual = date("Y-m-d H:i:s");

            $sql = "UPDATE __usuarios SET pass_word = :pass_word, ssap_revision = :ssap_revision, token = NULL, fechaEsperaProceso = NULL, fechaActualizacion = :fechaActualizacion WHERE id = :idUsuario AND correo = :correo";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':pass_word', $pass, PDO::PARAM_STR);
            $stmt->bindParam(':ssap_revision', $salt, PDO::PARAM_STR);
            $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
            $stmt->execute();
            
            require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/restablecer_password_confirmacion.php';

            $linkAtencionClientes = HOST_LINK . 'atencion-a-clientes';

            $correo_restablecerPassword_confirmacion = new Correo_restablecerPassword_confirmacion($nombreS, $linkAtencionClientes, $correo);
            $correo_restablecerPassword_confirmacion->enviarCorreo();

            $respuesta = "6";
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_success">
      <span>
        <b>Se restableció la password. Se redireccionará para que inicies sesión en: <span class="g-contenedor-tiempo_inicio">5 segundos</span></b>
      </span>
    </div>
  </div>';
          }else{
            $respuesta = "5";
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_info">
      <span>
        <b>Expiró el tiempo para restablecer tu password, vuelve a realizar el proceso.</b>
      </span>
    </div>
  </div>';
          }
        }else{
          $respuesta = "5";
          $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_info">
      <span>
        <b>No se encuentra activada tu cuenta de MXcomp.</b>
      </span>
    </div>
  </div>';
        }
      }else{
        $respuesta = "5";
        $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_error">
      <span>
        <b>El correo y el token, no se encuentran registrados.</b>
      </span>
    </div>
  </div>';
      }
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al realizar una consulta.";
    }
  }

  unset($_POST['accion']);
  unset($_POST['nueva_password']);
  unset($_POST['confirmar_password']);
  unset($_POST['correo']);
  unset($_POST['token']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>