<?php
if(isset($_POST['accion']) && $_POST['accion'] === "recuperar"){
  include dirname(__DIR__, 2) . '/funciones/validaciones_correo.php';
  include dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
  include dirname(__DIR__, 2) . '/funciones/creacion_token.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $correo = trim($_POST['correo']);

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';

  // REVISA EL CORREO ELECTRONICO
  if($correo !== "" && validar_correo($correo)){
    $correo = (string) $correo;
    $proceso_correcto = true;
  }else if($correo === ""){
    $respuesta = "1"; // El campo se encuentra vacío
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // El correo electrónico no es válido
    $proceso_correcto = false;
  }
  
  // REALIZA EL PROCESO PARA MANDAR EL CORREO CON LAS INSTRUCCIONES
  if($proceso_correcto){
    try{
      // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
      $correo_consulta = encriptar($correo);
      
      $sql = "SELECT COUNT(id) AS conteo, id, codigoUsuario, nombreS, cuentaActiva, token, fechaEsperaProceso FROM __usuarios WHERE correo = :correo";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $cuenta_existe = (int) $datos_usuario['conteo'];
      
      if($cuenta_existe === 1){
        $idUsuario = (int) $datos_usuario['id'];
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];
        $cuentaActiva = (int) $datos_usuario['cuentaActiva'];
        $token_BD = (string) $datos_usuario['token'];
        $fechaEsperaProceso_BD = (string) $datos_usuario['fechaEsperaProceso'];

        //ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);
        $nombreS = desencriptar_con_clave(trim($datos_usuario['nombreS']), $codigoUsuario_encriptado);
        
        if($cuentaActiva === 1){
          $token = token_30();
          
          require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/restablecer_password_proceso.php';

          $linkActivacion = HOST_LINK . 'restablecer-password/' . $correo . '/' . $token;
          $linkAtencionClientes = HOST_LINK . 'atencion-a-clientes';
          
          $correo_restablecerPassword_proceso = new Correo_restablecerPassword_proceso($nombreS, $linkActivacion, $linkAtencionClientes, $correo);
          
          if($correo_restablecerPassword_proceso->enviarCorreo()){
            $fechaEsperaProceso = date("Y-m-d H:i:s", strtotime('+24 hours'));
            $fechaActualizacion = date("Y-m-d H:i:s");

            $sql = "UPDATE __usuarios SET token = :token, fechaEsperaProceso = :fechaEsperaProceso, fechaActualizacion = :fechaActualizacion WHERE id = :idUsuario AND correo = :correo";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':token', $token, PDO::PARAM_STR);
            $stmt->bindParam(':fechaEsperaProceso', $fechaEsperaProceso, PDO::PARAM_STR);
            $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
            $stmt->execute();
            
            $respuesta = "5"; // El correo fue enviado
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_success">
      <span>
        <b>Te enviamos un correo para que sigas los pasos para el proceso, revisa tu bandeja de entrada o tu carpeta de Spam. Si no ha llegado espera unos minutos, si ya pasaron horas y no ha llegado, vuelve a realizar el proceso.</b>
      </span>
    </div>
  </div>';
          }else{
            $respuesta = "4"; // El correo no se envió
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_error">
      <span>
        <b>No se envió el correo, revisa tu conexión a internet o vuelve a realizar el proceso.</b>
      </span>
    </div>
  </div>';
          }
        }else{
          require_once dirname(__DIR__, 2) . '/clases/plantillas_correos/activar_cuenta.php';

          $linkActivacion = HOST_LINK . 'activar-cuenta/' . $correo . '/' . $token_BD;
          $fechaEsperaProceso_BD = fecha_con_hora($fechaEsperaProceso_BD);

          $correo_activarCuenta = new Correo_activarCuenta($nombreS, $fechaEsperaProceso_BD, $linkActivacion, $correo);

          if($correo_activarCuenta->enviarCorreo()){
            $respuesta = "5"; // El correo fue enviado
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_info">
      <span>
        <b>No puedes recuperar tu contraseña hasta que actives tu cuenta, te enviamos un correo para que hagas el proceso de activación, revisa tu bandeja de entrada o tu carpeta de Spam. Si no ha llegado pasado unos minutos, vuelve a realizar el proceso.</b>
      </span>
    </div>
  </div>';
          }else{
            $respuesta = "4"; // El correo no se envió
            $mensaje = '
  <div class="p-notification_contenedor g-restablecerPassword-notification_contenedor">
    <div class="p-notification p-notification_letter_error">
      <span>
        <b>No se envió el correo, revisa tu conexión a internet o vuelve a realizar el proceso.</b>
      </span>
    </div>
  </div>';
          }
        }
      }else{
        $respuesta = "3";
        $mensaje = '';
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Hubo un problema en la consulta";
    }
  }
  
  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['correo']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>