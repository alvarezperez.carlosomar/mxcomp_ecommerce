<?php
if(isset($_POST['accion']) && $_POST['accion'] === "generar"){
  require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 1) . '/conn.php';
  
  $Conn_mxcomp = new Conexion_mxcomp();
  
  $codigo_postal = trim($_POST['codigo_postal']);
  $mensaje = '';
  
  if(validar_campo_numerico($codigo_postal)){
    if(mb_strlen($codigo_postal) >= 2){

      $digitosEstado = (string) mb_substr($codigo_postal, 0, 2);

      try{
        $sql = "SELECT COUNT(id) AS conteo, nombreEstado FROM __estados_codigos WHERE digitosEstado1 = :digitosEstado";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':digitosEstado', $digitosEstado, PDO::PARAM_STR);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);

        $nombreEstado = "";
        if((int) $datos_estado['conteo'] === 1){
          $respuesta = "2";
          $nombreEstado = (string) trim($datos_estado['nombreEstado']);
          
          $sql = "SELECT id, nombreEstado FROM __estados_codigos";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->execute();
          
          $mensaje = '<option value="">Selecciona tu estado</option>';
          
          while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
            $selected = $nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';

            $mensaje .= '<option value="' . trim($datos_estado['id']) . '" ' . $selected . '>' . trim($datos_estado['nombreEstado']) . '</option>';
          }
        }else{
          $sql = "SELECT nombreEstado, digitosEstado1, digitosEstado2 FROM __estados_codigos WHERE digitosEstado2 IS NOT NULL";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->execute();

          while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
            if($digitosEstado >= (string) $datos_estado['digitosEstado1'] && $digitosEstado <= (string) $datos_estado['digitosEstado2']){
              $nombreEstado = (string) $datos_estado['nombreEstado'];
              break;
            }
          }
          if($nombreEstado !== ""){
            $respuesta = "2";
            
            $sql = "SELECT id, nombreEstado FROM __estados_codigos";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->execute();

            $mensaje = '<option value="">Selecciona tu estado</option>';

            while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
              $selected = $nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';

              $mensaje .= '<option value="' . trim($datos_estado['id']) . '" ' . $selected . '>' . trim($datos_estado['nombreEstado']) . '</option>';
            }
          }else{
            $respuesta = "3";
            
            $sql = "SELECT id, nombreEstado FROM __estados_codigos";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->execute();

            $mensaje = '<option value="" selected="selected">Selecciona tu estado</option>';

            while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
              $mensaje .= '<option value="' . trim($datos_estado['id']) . '">' . trim($datos_estado['nombreEstado']) . '</option>';
            }
          }
        }

        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al buscar la informacion de los estados";
      }
    }else{
      $respuesta = "4"; // ES PARA REGRESAR EL SELECT A SU POSICION POR DEFAULT
    }
  }else{
    $respuesta = "1"; // SI EL CODIGO POSTAL NO ES NUMERICO
  }

  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['codigo_postal']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>