<?php
if(isset($_POST['accion']) && $_POST['accion'] === "paginacion"){
  session_start();
  
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  
  $pagina = trim($_POST['pagina']);
  $mensaje = '';
  
  if($pagina !== "" && validar_campo_numerico($pagina)){
    $_SESSION['__pagina_actual'] = (int) $pagina;
    
    $totalProductos = (int) trim($_SESSION['__total_productos__']);
    $productosPorPagina = (int) trim($_SESSION['__productos_por_pagina__']);
    $paginas = (float) ceil($totalProductos / $productosPorPagina);
    
    if($_SESSION['__pagina_actual'] !== 0 && $_SESSION['__pagina_actual'] <= $paginas){
      $respuesta = "1"; // PAGINACION CORRECTA
    }else{
      $_SESSION['__pagina_actual'] = 1;
      $respuesta = "2"; // HUBO UN PROBLEMA EN LA PAGINACIÓN
      $mensaje = "Hubo un problema en la paginación.";
    }
  }else{
    $respuesta = "2"; // HUBO UN PROBLEMA EN LA PAGINACIÓN
    $mensaje = "Hubo un problema en la paginación.";
  }
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>