<?php
if(isset($_POST['accion']) && $_POST['accion'] === "ordenar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';

  $activado = trim($_POST['activado']);
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA EL VALOR DE ACTIVO, QUE SEA BINARIO
  if($activado !== "" && validar_campo_binario($activado)){
    $activado = (int) $activado;
    $proceso_correcto = true;
  }else{
    $respuesta = "1";
    $proceso_correcto = false;
  }

  // PROCESOS PARA ORDENAR POR PRECIO MENOR
  if($proceso_correcto){
    /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA CATEGORIA
    if(isset($_SESSION['__categoria_seleccionada'])){
      /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA DE TIPO, SI ES ASI SE HACE LO SIGUIENTE
      if(isset($_SESSION['__subcat_tipo_seleccionadas'])){
        /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA ESPECIFICAS, SI ES ASI SE HACE LO SIGUIENTE
        if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
          /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA MARCA
          if(isset($_SESSION['__marca_seleccionada'])){
            /////// FILTRO PARA MARCA, CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
            // SI EXISTE FILTRO DE RANGO DE PRECIOS
            if(isset($_SESSION['__rango_precios__'])){
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          
                          if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                            $mostrar = true;
                            break 2;
                          }
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          $mostrar = true;
                          break 2;
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }else{
            /////// FILTRO PARA CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
            // SI EXISTE FILTRO DE RANGO DE PRECIOS
            if(isset($_SESSION['__rango_precios__'])){
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          
                          if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                            $mostrar = true;
                            break 2;
                          }
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          $mostrar = true;
                          break 2;
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }
        }else{
          if(isset($_SESSION['__marca_seleccionada'])){
            /////// FILTRO PARA MARCA, CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
            // SI EXISTE FILTRO DE RANGO DE PRECIOS
            if(isset($_SESSION['__rango_precios__'])){
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      
                      if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                        $mostrar = true;
                        break;
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      $mostrar = true;
                      break;
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }else{
            /////// FILTRO PARA CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
            // SI EXISTE FILTRO DE RANGO DE PRECIOS
            if(isset($_SESSION['__rango_precios__'])){
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      
                      if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                        $mostrar = true;
                        break;
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      $mostrar = true;
                      break;
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }
        }
      }else{
        if(isset($_SESSION['__marca_seleccionada'])){
          /////// FILTRO PARA MARCA Y CATEGORIA SELECCIONADAS
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ) ){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ) ){
                $mostrar = true;
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }else{
          /////// FILTRO PARA CATEGORIA SELECCIONADA
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){
                $mostrar = true;
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }
      }
    }else if(isset($_SESSION['__marca_seleccionada'])){
      /////// SINO ESTA SELECCIONADA LA CATEGORIA
      // SI EXISTE FILTRO DE RANGO DE PRECIOS
      if(isset($_SESSION['__rango_precios__'])){
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $mostrar = false;

          if( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ){
            
            if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
              $mostrar = true;
            }
          }

          if($mostrar){
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
          }else{
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
          }
        }
      }else{
        // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $mostrar = false;

          if( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ){
            $mostrar = true;
          }

          if($mostrar){
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
          }else{
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
          }
        }
      }
    }else{
      // SI EXISTE FILTRO DE RANGO DE PRECIOS
      if(isset($_SESSION['__rango_precios__'])){
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $mostrar = false;
          
          if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
            $mostrar = true;
          }
          
          if($mostrar){
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
          }else{
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
          }
        }
      }else{
        // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
        }
      }
    }

    unset($_SESSION['__productosImprimir_Busqueda__']);
    $contador = 0;
    foreach($_SESSION['__productosBusqueda__'] as $datos_pd){
      if($datos_pd['mostrarBusqueda'] === "1"){
        $_SESSION['__productosImprimir_Busqueda__'][$contador] = [
          'codigoProducto' => $datos_pd['codigoProducto'],
          'skuProveedor' => $datos_pd['skuProveedor'],
          'descripcion' => $datos_pd['descripcion'],
          'descripcionURL' => $datos_pd['descripcionURL'],
          'categoriaID' => $datos_pd['categoriaID'],
          'marcaID' => $datos_pd['marcaID'],
          'nombreMarca' => $datos_pd['nombreMarca'],
          'subcat_tipoID' => $datos_pd['subcat_tipoID'],
          'subcat_especificaID' => $datos_pd['subcat_especificaID'],
          'precioMXcomp' => $datos_pd['precioMXcomp'],
          'monedaMXcomp' => $datos_pd['monedaMXcomp'],
          'existenciaTotal' => $datos_pd['existenciaTotal'],
          'tieneImagen' => $datos_pd['tieneImagen'],
          'numeroUbicacionImagen' => $datos_pd['numeroUbicacionImagen'],
          'nombreImagen' => $datos_pd['nombreImagen'],
          'versionImagen' => $datos_pd['versionImagen'],
          'activo' => $datos_pd['activo'],
          'mostrarBusqueda' => $datos_pd['mostrarBusqueda']
        ];
        $contador++;
      }
    }
    
    if($activado === 0){
      $_SESSION['__activacion_precio_menor_mayor'] = "1";
      if(isset($_SESSION['__activacion_precio_mayor_menor'])){
        unset($_SESSION['__activacion_precio_mayor_menor']);
      }
      
      if(isset($_SESSION['__productosImprimir_Busqueda__'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_ASC, $_SESSION['__productosImprimir_Busqueda__']);
      }
    }else{
      unset($_SESSION['__activacion_precio_menor_mayor']);
    }
    
    $respuesta = "2";
    $mensaje = $activado;
  }

  unset($_POST['accion']);
  unset($_POST['activado']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>