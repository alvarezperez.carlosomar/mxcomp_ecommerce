<?php
if(isset($_POST['accion']) && $_POST['accion'] === "filtrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  
  $opcion = trim($_POST['opcion']);

  switch($opcion){
    case 'agregar':
      $precioInicial = trim($_POST['precio_inicial']);
      $precioFinal = trim($_POST['precio_final']);
      $proceso_correcto = false;
      
      // REVISA EL PRECIO INICIAL
      if($precioInicial !== "" && validar_campo_numerico($precioInicial)){
        $precioInicial = (float) $precioInicial . '.00';
        $proceso_correcto = true;
      }else if($precioInicial !== "" && validar_precio_formato($precioInicial)){
        $precioInicial = (float) $precioInicial;
        $proceso_correcto = true;
      }else if($precioInicial === ""){
        $respuesta = "1"; // El campo "Precio inicial" se encuentra vacío
        $proceso_correcto = false;
      }else{
        $respuesta = "2"; // El precio inicial no respeta el formato
        $proceso_correcto = false;
      }

      // REVISA EL PRECIO FINAL
      if($proceso_correcto){
        if($precioFinal !== "" && validar_campo_numerico($precioFinal)){
          $precioFinal = (float) $precioFinal . '.00';
          $proceso_correcto = true;
        }else if($precioFinal !== "" && validar_precio_formato($precioFinal)){
          $precioFinal = (float) $precioFinal;
          $proceso_correcto = true;
        }else if($precioFinal === ""){
          $respuesta = "3"; // El campo "Precio final" se encuentra vacío
          $proceso_correcto = false;
        }else{
          $respuesta = "4"; // El precio final no respeta el formato
          $proceso_correcto = false;
        }
      }

      // PROCESOS PARA FILTRAR POR RANGO DE PRECIO
      if($proceso_correcto){
        if($precioInicial > $precioFinal){
          $precioTemporal = $precioInicial;
          $precioInicial = $precioFinal;
          $precioFinal = $precioTemporal;
          unset($precioTemporal);
        }

        $respuesta = "5";

        $_SESSION['__rango_precios__'][0] = [
          'precio_inicial' => (float) $precioInicial,
          'precio_final' => (float) $precioFinal
        ];

        if(isset($_SESSION['__categoria_seleccionada'])){

          /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA DE TIPO, SI ES ASI SE HACE LO SIGUIENTE
          if(isset($_SESSION['__subcat_tipo_seleccionadas'])){

            /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA ESPECIFICAS, SI ES ASI SE HACE LO SIGUIENTE
            if(isset($_SESSION['__subcat_especificas_seleccionadas'])){

              if(isset($_SESSION['__marca_seleccionada'])){
                /////// FILTRO PARA MARCA, CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
                foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                  $mostrar = false;

                  // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                  if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                    // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                      // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                      if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                        // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                        foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                          // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                          if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){

                            if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
                              $mostrar = true;
                              break 2;
                            }
                          }
                        }
                      }
                    }
                  }

                  if($mostrar){
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                  }else{
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                  }
                }
              }else{
                /////// FILTRO PARA CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
                foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                  $mostrar = false;

                  // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                  if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                    // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                      // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                      if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                        // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                        foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                          // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                          if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){

                            if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
                              $mostrar = true;
                              break 2;
                            }
                          }
                        }
                      }
                    }
                  }

                  if($mostrar){
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                  }else{
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                  }
                }
              }
            }else{
              if(isset($_SESSION['__marca_seleccionada'])){
                /////// FILTRO PARA MARCA, CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
                foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                  $mostrar = false;

                  // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                  if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                    // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                      // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                      if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                        if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
                          $mostrar = true;
                          break;
                        }
                      }
                    }
                  }

                  if($mostrar){
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                  }else{
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                  }
                }
              }else{
                /////// FILTRO PARA CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
                foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                  $mostrar = false;

                  // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                  if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                    // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                      // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                      if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                        if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
                          $mostrar = true;
                          break;
                        }
                      }
                    }
                  }

                  if($mostrar){
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                  }else{
                    $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                  }
                }
              }
            }
          }else{
            if(isset($_SESSION['__marca_seleccionada'])){
              /////// FILTRO PARA MARCA Y CATEGORIA SELECCIONADAS
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ) ){
                  if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
                    $mostrar = true;
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              /////// FILTRO PARA CATEGORIA SELECCIONADA
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ) ){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }
        }else if(isset($_SESSION['__marca_seleccionada'])){ /////// SINO ESTA SELECCIONADA LA CATEGORIA
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            if( ( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ) && ( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ) ){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }else{
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            if( ((float) $datos['precioMXcomp'] >= (float) $precioInicial) && ((float) $datos['precioMXcomp'] <= (float) $precioFinal) ){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }

        unset($_SESSION['__productosImprimir_Busqueda__']);
        $contador = 0;
        foreach($_SESSION['__productosBusqueda__'] as $datos_pd){
          if($datos_pd['mostrarBusqueda'] === "1"){
            $_SESSION['__productosImprimir_Busqueda__'][$contador] = [
              'codigoProducto' => $datos_pd['codigoProducto'],
              'skuProveedor' => $datos_pd['skuProveedor'],
              'descripcion' => $datos_pd['descripcion'],
              'descripcionURL' => $datos_pd['descripcionURL'],
              'categoriaID' => $datos_pd['categoriaID'],
              'marcaID' => $datos_pd['marcaID'],
              'nombreMarca' => $datos_pd['nombreMarca'],
              'subcat_tipoID' => $datos_pd['subcat_tipoID'],
              'subcat_especificaID' => $datos_pd['subcat_especificaID'],
              'precioMXcomp' => $datos_pd['precioMXcomp'],
              'monedaMXcomp' => $datos_pd['monedaMXcomp'],
              'existenciaTotal' => $datos_pd['existenciaTotal'],
              'tieneImagen' => $datos_pd['tieneImagen'],
              'numeroUbicacionImagen' => $datos_pd['numeroUbicacionImagen'],
              'nombreImagen' => $datos_pd['nombreImagen'],
              'versionImagen' => $datos_pd['versionImagen'],
              'activo' => $datos_pd['activo'],
              'mostrarBusqueda' => $datos_pd['mostrarBusqueda']
            ];
            $contador++;
          }
        }

        if(isset($_SESSION['__productosImprimir_Busqueda__'])){
          if(isset($_SESSION['__activacion_precio_menor_mayor'])){
            foreach($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
              $array_auxiliar[$indice] = $registro['precioMXcomp'];
            }

            array_multisort($array_auxiliar, SORT_ASC, $_SESSION['__productosImprimir_Busqueda__']);
          }

          if(isset($_SESSION['__activacion_precio_mayor_menor'])){
            foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
              $array_auxiliar[$indice] = $registro['precioMXcomp'];
            }

            array_multisort($array_auxiliar, SORT_DESC, $_SESSION['__productosImprimir_Busqueda__']);
          }
        }
      }
      break;

    case 'eliminar':
      if(isset($_SESSION['__rango_precios__'])){
        unset($_SESSION['__rango_precios__']);
      }

      if(isset($_SESSION['__categoria_seleccionada'])){

        /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA DE TIPO, SI ES ASI SE HACE LO SIGUIENTE
        if(isset($_SESSION['__subcat_tipo_seleccionadas'])){

          /////// FILTRO PARA CUANDO ESTA SELECCIONADA UNA SUBCATEGORIA ESPECIFICAS, SI ES ASI SE HACE LO SIGUIENTE
          if(isset($_SESSION['__subcat_especificas_seleccionadas'])){

            if(isset($_SESSION['__marca_seleccionada'])){
              /////// FILTRO PARA MARCA, CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          $mostrar = true;
                          break 2;
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              /////// FILTRO PARA CATEGORIA, SUBCATEGORIAS DE TIPO Y SUBCATEGORIAS ESPECIFICAS SELECCIONADAS
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                    // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){

                      // SE REALIZA UN CICLO A LAS SUBCATEGORIAS ESPECIFICAS
                      foreach($_SESSION['__subcat_especificas_seleccionadas'] as $ind=>$subcat_especificas_datos){
                        // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS ESPECIFICAS CON LA SELECCIONADA
                        if( (int) $datos['subcat_especificaID'] === (int) $subcat_especificas_datos['id'] ){
                          $mostrar = true;
                          break 2;
                        }
                      }
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }else{
            if(isset($_SESSION['__marca_seleccionada'])){
              /////// FILTRO PARA MARCA, CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      $mostrar = true;
                      break;
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }else{
              /////// FILTRO PARA CATEGORIA Y SUBCATEGORIAS DE TIPO SELECCIONADAS
              foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
                $mostrar = false;

                // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y DE MARCA CON LA SELECCIONADA
                if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

                  // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
                  foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){

                    // SE REVISA SI COINCIDEN LAS IDS DE LA SUBCATEGORIA DE TIPO CON LA SELECCIONADA
                    if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                      $mostrar = true;
                      break;
                    }
                  }
                }

                if($mostrar){
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
                }else{
                  $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
                }
              }
            }
          }
        }else{
          if(isset($_SESSION['__marca_seleccionada'])){
            /////// FILTRO PARA MARCA Y CATEGORIA SELECCIONADAS
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ) ){
                $mostrar = true;
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            /////// FILTRO PARA CATEGORIA SELECCIONADA
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;

              if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){
                $mostrar = true;
              }

              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }
      }else if(isset($_SESSION['__marca_seleccionada'])){ /////// SINO ESTA SELECCIONADA LA CATEGORIA
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $mostrar = false;

          if( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ){
            $mostrar = true;
          }

          if($mostrar){
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
          }else{
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
          }
        }
      }else{
        foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
          $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
        }
      }

      unset($_SESSION['__productosImprimir_Busqueda__']);
      $contador = 0;
      foreach($_SESSION['__productosBusqueda__'] as $datos_pd){
        if($datos_pd['mostrarBusqueda'] === "1"){
          $_SESSION['__productosImprimir_Busqueda__'][$contador] = [
            'codigoProducto' => $datos_pd['codigoProducto'],
            'skuProveedor' => $datos_pd['skuProveedor'],
            'descripcion' => $datos_pd['descripcion'],
            'descripcionURL' => $datos_pd['descripcionURL'],
            'categoriaID' => $datos_pd['categoriaID'],
            'marcaID' => $datos_pd['marcaID'],
            'nombreMarca' => $datos_pd['nombreMarca'],
            'subcat_tipoID' => $datos_pd['subcat_tipoID'],
            'subcat_especificaID' => $datos_pd['subcat_especificaID'],
            'precioMXcomp' => $datos_pd['precioMXcomp'],
            'monedaMXcomp' => $datos_pd['monedaMXcomp'],
            'existenciaTotal' => $datos_pd['existenciaTotal'],
            'tieneImagen' => $datos_pd['tieneImagen'],
            'numeroUbicacionImagen' => $datos_pd['numeroUbicacionImagen'],
            'nombreImagen' => $datos_pd['nombreImagen'],
            'versionImagen' => $datos_pd['versionImagen'],
            'activo' => $datos_pd['activo'],
            'mostrarBusqueda' => $datos_pd['mostrarBusqueda']
          ];
          $contador++;
        }
      }

      if(isset($_SESSION['__activacion_precio_menor_mayor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_ASC, $_SESSION['__productosImprimir_Busqueda__']);
      }

      if(isset($_SESSION['__activacion_precio_mayor_menor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_DESC, $_SESSION['__productosImprimir_Busqueda__']);
      }

      $respuesta = "1";
      break;

    default:
      $respuesta = "0";
  }

  unset($_POST['accion']);
  unset($_POST['opcion']);
  unset($_POST['precio_inicial']);
  unset($_POST['precio_final']);

  $json = [ 'respuesta' => $respuesta ];
  echo json_encode($json);
}
?>