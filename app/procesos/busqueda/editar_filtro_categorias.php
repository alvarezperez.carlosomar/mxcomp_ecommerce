<?php
if(isset($_POST['accion']) && $_POST['accion'] === "filtrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  
  $idCategoria = desencriptar(trim($_POST['id']));

  $proceso_correcto = false;
  $mensaje = '';

  // REVISA EL ID DE LA CATEGORIA
  if($idCategoria !== "" && validar_campo_numerico($idCategoria)){
    $idCategoria = (int) $idCategoria;
    $proceso_correcto = true;
  }else{
    $respuesta = "1"; // NO ES NUMÉRICO
    $proceso_correcto = false;
  }

  // PROCESOS PARA EDITAR EL FILTRO DE CATEGORÍAS
  if($proceso_correcto){
    if(isset($_SESSION['__categoria_seleccionada'])){
      unset($_SESSION['__categoria_seleccionada']);

      if(isset($_SESSION['__subcat_tipo_seleccionadas']) || isset($_SESSION['__subcat_especificas_seleccionadas'])){
        unset($_SESSION['__subcat_tipo_seleccionadas']);
        unset($_SESSION['__subcat_especificas_seleccionadas']);
      }
      
      /////// FILTROS PARA CUANDO SE ELIMINO LA CATEGORIA, SI ESTA SELECCIONADA LA MARCA SE APLICA LO SIGUIENTE
      if(isset($_SESSION['__marca_seleccionada'])){
        
        // SI EXISTE FILTRO DE RANGO DE PRECIOS
        if(isset($_SESSION['__rango_precios__'])){
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;
            
            if( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ){
              
              if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                $mostrar = true;
              }
            }
            
            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }else{
          // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;
            
            if( (int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'] ){
              $mostrar = true;
            }
            
            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }
      }else{ /////// SINO ESTA SELECCIONADA LA MARCA
        // SI EXISTE FILTRO DE RANGO DE PRECIOS
        if(isset($_SESSION['__rango_precios__'])){
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;
            
            if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
              $mostrar = true;
            }
            
            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }else{
          // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
          }
        }
      }
      
      unset($_SESSION['__pagina_actual']);
      $respuesta = "3";
    }else{
      $idsCategoria_array = array_column($_SESSION['__categorias_Busqueda__'], 'id');
      
      if(in_array($idCategoria, $idsCategoria_array)){
        foreach($_SESSION['__categorias_Busqueda__'] as $indice=>$datos){
          if((int) $datos['id'] === $idCategoria){
            $datosCat = array(
              'id' => $datos['id'],
              'nombre' => $datos['nombre']
            );
            $_SESSION['__categoria_seleccionada'][0] = $datosCat;
            break;
          }
        }
        
        /////// FILTROS PARA CUANDO SE ELIMINO LA CATEGORIA, SI ESTA SELECCIONADA LA MARCA SE APLICA LO SIGUIENTE
        if(isset($_SESSION['__marca_seleccionada'])){
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              if( ((int) $datos['categoriaID'] === (int) $idCategoria) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              if( ((int) $datos['categoriaID'] === (int) $idCategoria) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id']) ){
                $mostrar = true;
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }else{ /////// SINO ESTA SELECCIONADA LA MARCA
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              if((int) $datos['categoriaID'] === (int) $idCategoria){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              if((int) $datos['categoriaID'] === (int) $idCategoria){
                $mostrar = true;
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }
        
        unset($_SESSION['__pagina_actual']);
        $respuesta = "3";
      }else{
        $respuesta = "1";
      }
    }
    
    if($respuesta === "3" && count($_SESSION['__productosBusqueda__']) !== 0){
      unset($_SESSION['__productosImprimir_Busqueda__']);
      
      $contador = 0;
      foreach($_SESSION['__productosBusqueda__'] as $datos_pd){
        if($datos_pd['mostrarBusqueda'] === "1"){
          $_SESSION['__productosImprimir_Busqueda__'][$contador] = [
            'codigoProducto' => $datos_pd['codigoProducto'],
            'skuProveedor' => $datos_pd['skuProveedor'],
            'descripcion' => $datos_pd['descripcion'],
            'descripcionURL' => $datos_pd['descripcionURL'],
            'categoriaID' => $datos_pd['categoriaID'],
            'marcaID' => $datos_pd['marcaID'],
            'nombreMarca' => $datos_pd['nombreMarca'],
            'subcat_tipoID' => $datos_pd['subcat_tipoID'],
            'subcat_especificaID' => $datos_pd['subcat_especificaID'],
            'precioMXcomp' => $datos_pd['precioMXcomp'],
            'monedaMXcomp' => $datos_pd['monedaMXcomp'],
            'existenciaTotal' => $datos_pd['existenciaTotal'],
            'tieneImagen' => $datos_pd['tieneImagen'],
            'numeroUbicacionImagen' => $datos_pd['numeroUbicacionImagen'],
            'nombreImagen' => $datos_pd['nombreImagen'],
            'versionImagen' => $datos_pd['versionImagen'],
            'activo' => $datos_pd['activo'],
            'mostrarBusqueda' => $datos_pd['mostrarBusqueda']
          ];
          $contador++;
        }
      }
      
      if(isset($_SESSION['__activacion_precio_menor_mayor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_ASC, $_SESSION['__productosImprimir_Busqueda__']);
      }

      if(isset($_SESSION['__activacion_precio_mayor_menor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_DESC, $_SESSION['__productosImprimir_Busqueda__']);
      }
    }
  }

  unset($_POST['accion']);
  unset($_POST['id']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>