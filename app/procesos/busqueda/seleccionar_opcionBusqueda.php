<?php
if(isset($_POST['accion']) && $_POST['accion'] === "buscar"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  $Conn_mxcomp = new Conexion_mxcomp();
  
  if(isset($_POST['opcion']) && ($_POST['opcion'] === "catalogo" || $_POST['opcion'] === "categorias" || $_POST['opcion'] === "marcas")){
    unset($_SESSION['__opcion_busqueda__']);
    try{
      $sql = "SELECT COUNT(id) FROM __productos";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();
      $productos_existen = (int) $stmt->fetchColumn();

      if($productos_existen >= 1){
        $respuesta = "1";
        
        if($_POST['opcion'] === "catalogo"){
          $mensaje = "catalogo";
          $_SESSION['__opcion_busqueda__'] = "catalogo";
          
          unset($_SESSION['__palabra_clave_opcion__']);
          unset($_SESSION['__id_categoria__']);
          unset($_SESSION['__id_marca__']);
          unset($_SESSION['__productosBusqueda__']);
          unset($_SESSION['__productosImprimir_Busqueda__']);
          unset($_SESSION['__categorias_Busqueda__']);
          unset($_SESSION['__subcat_tipos_Busqueda__']);
          unset($_SESSION['__subcat_especificas_Busqueda__']);
          unset($_SESSION['__marcas_Busqueda__']);
          unset($_SESSION['__productos_por_pagina__']);
          unset($_SESSION['__total_productos__']);
          unset($_SESSION['__pagina_actual']);
          unset($_SESSION['__categoria_seleccionada']);
          unset($_SESSION['__subcat_tipo_seleccionadas']);
          unset($_SESSION['__subcat_especificas_seleccionadas']);
          unset($_SESSION['__marca_seleccionada']);
          unset($_SESSION['__rango_precios__']);
          unset($_SESSION['__activacion_precio_menor_mayor']);
          unset($_SESSION['__activacion_precio_mayor_menor']);
        }
        
        if($_POST['opcion'] === "categorias"){
          $id = trim($_POST['categoria']);
          
          if($id !== "" && validar_campo_numerico($id)){
            $id = (int) $id;
            
            try{
              $sql = "SELECT COUNT(id) AS conteo, nombre FROM __categorias WHERE id = :id";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':id', $id, PDO::PARAM_INT);
              $stmt->execute();
              $datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC);
              $categoria_existe = (int) $datos_categoria['conteo'];
              
              if($categoria_existe === 1){
                $nombre_categoria = (string) $datos_categoria['nombre'];
                $nombre_categoria = str_replace(" ", "_", $nombre_categoria);
                
                $mensaje = 'categoria/' . $id . '/' . $nombre_categoria;
                $_SESSION['__opcion_busqueda__'] = "categorias";
                $_SESSION['__id_categoria__'] = $id;
                
                unset($_SESSION['__palabra_clave_opcion__']);
                unset($_SESSION['__id_marca__']);
                unset($_SESSION['__productosBusqueda__']);
                unset($_SESSION['__productosImprimir_Busqueda__']);
                unset($_SESSION['__categorias_Busqueda__']);
                unset($_SESSION['__subcat_tipos_Busqueda__']);
                unset($_SESSION['__subcat_especificas_Busqueda__']);
                unset($_SESSION['__marcas_Busqueda__']);
                unset($_SESSION['__productos_por_pagina__']);
                unset($_SESSION['__total_productos__']);
                unset($_SESSION['__pagina_actual']);
                unset($_SESSION['__categoria_seleccionada']);
                unset($_SESSION['__subcat_tipo_seleccionadas']);
                unset($_SESSION['__subcat_especificas_seleccionadas']);
                unset($_SESSION['__marca_seleccionada']);
                unset($_SESSION['__rango_precios__']);
                unset($_SESSION['__activacion_precio_menor_mayor']);
                unset($_SESSION['__activacion_precio_mayor_menor']);
              }else{
                $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
                $mensaje = "No es numerico";
              }
              
              $stmt = null;
            }catch(PDOException $e){
              $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
              //$mensaje = $e->getMessage();
              $mensaje = "No es numerico";
            }
          }else{
            $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
            $mensaje = "No es numerico";
          }
        }
        
        if($_POST['opcion'] === "marcas"){
          $id = trim($_POST['marca']);
          
          if($id !== "" && validar_campo_numerico($id)){
            $id = (int) $id;
            
            try{
              $sql = "SELECT COUNT(id) AS conteo, nombre FROM __marcas WHERE id = :id";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':id', $id, PDO::PARAM_INT);
              $stmt->execute();
              $datos_marca = $stmt->fetch(PDO::FETCH_ASSOC);
              $marca_existe = (int) $datos_marca['conteo'];
              
              if($marca_existe === 1){
                $nombre_marca = (string) $datos_marca['nombre'];
                $nombre_marca = str_replace(" ", "_", $nombre_marca);
                
                $mensaje = 'marca/' . $id . '/' . $nombre_marca;
                $_SESSION['__opcion_busqueda__'] = "marcas";
                $_SESSION['__id_marca__'] = $id;
                
                unset($_SESSION['__palabra_clave_opcion__']);
                unset($_SESSION['__id_categoria__']);
                unset($_SESSION['__productosBusqueda__']);
                unset($_SESSION['__productosImprimir_Busqueda__']);
                unset($_SESSION['__categorias_Busqueda__']);
                unset($_SESSION['__subcat_tipos_Busqueda__']);
                unset($_SESSION['__subcat_especificas_Busqueda__']);
                unset($_SESSION['__marcas_Busqueda__']);
                unset($_SESSION['__productos_por_pagina__']);
                unset($_SESSION['__total_productos__']);
                unset($_SESSION['__pagina_actual']);
                unset($_SESSION['__categoria_seleccionada']);
                unset($_SESSION['__subcat_tipo_seleccionadas']);
                unset($_SESSION['__subcat_especificas_seleccionadas']);
                unset($_SESSION['__marca_seleccionada']);
                unset($_SESSION['__rango_precios__']);
                unset($_SESSION['__activacion_precio_menor_mayor']);
                unset($_SESSION['__activacion_precio_mayor_menor']);
              }else{
                $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
                $mensaje = "No es numerico";
              }
              
              $stmt = null;
            }catch(PDOException $e){
              $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
              //$mensaje = $e->getMessage();
              $mensaje = "No es numerico";
            }
          }else{
            $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
            $mensaje = "No es numerico";
          }
        }
      }else{
        $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
        $mensaje = "No existen los productos";
      }
      
      $stmt = null;
    }catch(PDOException $e){
      $respuesta = "2";
      //$mensaje = $e->getMessage();
      $mensaje = "Error en consulta al buscar todos los productos";
    }
  }else{
    $busqueda = trim($_POST['busqueda'], " \t\xC2\xA0");

    if($busqueda !== ""){
      if(validar_codigoProducto_formato($busqueda)){
        $codigoProducto = (string) strtoupper($busqueda);

        try{
          $sql = "SELECT COUNT(id) AS conteo, descripcionURL FROM __productos WHERE codigoProducto = :codigoProducto";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
          $stmt->execute();
          $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
          $producto_existe = (int) $datos_producto['conteo'];

          if($producto_existe === 1){
            $descripcionURL = $datos_producto['descripcionURL'];
            
            $respuesta = "2"; // SE ENCONTRO EL CÓDIGO DEL PRODUCTO
            $mensaje = $codigoProducto . "/" . $descripcionURL . "/";
          }else{
            $respuesta = "4"; // NO EXISTE EL CÓDIGO DEL PRODUCTO
            $mensaje = '<span class="p-navbar-busqueda_form_div_span"><b>El producto buscado no está disponible.</b></span>';
          }

          $stmt = null;
        }catch(PDOException $error){
          $respuesta = "3";
          //echo $error->getMessage();
          $mensaje = "Error en consulta del producto.";
        }
      }else if(validar_sku_PCH($busqueda)){
        $skuProveedor = (string) strtoupper($busqueda);

        try{
          $sql = "SELECT COUNT(id) AS conteo, codigoProducto, descripcionURL FROM __productos WHERE skuProveedor = :skuProveedor";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
          $stmt->execute();
          $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
          $producto_existe = (int) $datos_producto['conteo'];

          if($producto_existe === 1){
            $codigoProducto = $datos_producto['codigoProducto'];
            $descripcionURL = $datos_producto['descripcionURL'];
            
            $respuesta = "2"; // SE ENCONTRO LA SKU
            $mensaje = $codigoProducto . "/" . $descripcionURL . "/";
          }else{
            $respuesta = "4"; // NO EXISTE LA SKU BUSCADA
            $mensaje = '<span class="p-navbar-busqueda_form_div_span"><b>El producto buscado no está disponible.</b></span>';
          }

          $stmt = null;
        }catch(PDOException $error){
          $respuesta = "3";
          //echo $error->getMessage();
          $mensaje = "Error en consulta del producto.";
        }
      }else{
        $palabras = explode(" ", $busqueda);

        $palabras_Busqueda_texto = "";
        $contador = 0;
        foreach($palabras as $indice=>$palabra){
          if($palabra !== ""){
            //TEXTO DE BUSQUEDA
            $palabras_Busqueda_texto .= $palabra;

            if((int) count($palabras) !== (int) ($indice + 1)){
              $palabras_Busqueda_texto .= " ";
            }

            //ARRAY DE LAS PALABRAS DE BUSQUEDA
            $palabras_Busqueda[$contador] = $palabra;
            $contador++;
          }
        }

        try{
          $sql = "SELECT COUNT(Productos.id) AS conteo, Productos.codigoProducto, Productos.descripcionURL FROM __productos Productos WHERE ";

          foreach($palabras_Busqueda as $indice=>$palabra){
            $sql .= "Productos.descripcion LIKE ?";

            if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
              $sql .= " AND ";
            }
          }

          //$sql .= " AND Productos.tieneImagen = '1' AND Productos.activo = 1";
          $sql .= " AND Productos.activo = 1";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);

          $r = 1;
          foreach($palabras_Busqueda as $palabra){
            $termino_desc = "%" . trim($palabra, " \t\xC2\xA0") . "%";
            $stmt->bindValue($r, $termino_desc, PDO::PARAM_STR);
            $r++;
          }

          $stmt->execute();
          $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);

          if((int) $datos_producto['conteo'] === 1){
            $codigoProducto = (string) $datos_producto['codigoProducto'];
            $descripcionURL = (string) $datos_producto['descripcionURL'];
            
            $mensaje = $codigoProducto . "/" . $descripcionURL . "/";
          }else{
            if((int) $datos_producto['conteo'] === 0){
              $sql = "SELECT COUNT(Productos.id) AS conteo, Productos.codigoProducto FROM __productos Productos WHERE ";

              foreach($palabras_Busqueda as $indice=>$palabra){
                $sql .= "Productos.descripcion LIKE ?";

                if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
                  $sql .= " OR ";
                }
              }

              //$sql .= " AND Productos.tieneImagen = '1' AND Productos.activo = 1";
              $sql .= " AND Productos.activo = 1";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($palabras_Busqueda as $palabra){
                $termino_desc = "%" . trim($palabra, " \t\xC2\xA0") . "%";
                $stmt->bindValue($r, $termino_desc, PDO::PARAM_STR);
                $r++;
              }

              $stmt->execute();
            }
            
            $mensaje = "buscar[" . trim($palabras_Busqueda_texto) . "]";
          }

          $respuesta = "2";
          $stmt = null;
        }catch(PDOException $error){
          $respuesta = "3";
          //$mensaje = "Error: " . $error->getMessage();
          $mensaje = "Error en consulta de productos.";
        }
      }
    }else{
      $respuesta = "1"; // ESTA VACÍO EL CAMPO
      $mensaje = '<span class="p-navbar-busqueda_form_div_span"><b>Por favor escriba la sku o lo que desea buscar.</b></span>';
    }
  }

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>