<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  
  $respuesta = "1";
  $contenedor_resultados = '';
  $contenedor_filtros = '';
  $boton_filtros = '';
  $contenedor_paginacion = '';
  $contenedor_productos = '';
  $totalProductos = 0;
  $filtroRangoPrecio = 0;
  
  /////////////////////////////////////////////////////////////////////////
  /////////////////////// CONTENEDOR DE RESULTADOS ////////////////////////
  if(isset($_SESSION['__opcion_busqueda__'])){
    if(isset($_SESSION['__productosImprimir_Busqueda__'])){
      foreach($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$dato){
        if($dato['mostrarBusqueda'] === '1'){
          $totalProductos++;
        }
      }

      $_SESSION['__total_productos__'] = $totalProductos;
    }else{
      $_SESSION['__total_productos__'] = 0;
    }
  }else if(isset($_SESSION['__terminos_buscados'])){
    $palabras = explode(" ", $_SESSION['__terminos_buscados']);

    $contador = 0;
    foreach($palabras as $indice=>$palabra){
      if($palabra !== ""){
        $palabras_Busqueda[$contador] = $palabra;
        $contador++;
      }
    }

    $terminosBuscados = "";
    foreach($palabras_Busqueda as $indice=>$palabra){
      $terminosBuscados .= $palabra;

      if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
        $terminosBuscados .= " ";
      }
    }

    if(isset($_SESSION['__productosImprimir_Busqueda__'])){
      foreach($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$dato){
        if($dato['mostrarBusqueda'] === '1'){
          $totalProductos++;
        }
      }

      $_SESSION['__total_productos__'] = $totalProductos;
    }else{
      $_SESSION['__total_productos__'] = 0;
    }
  }

  if((int) $_SESSION['__total_productos__'] === 1){
    $texto_resultados = $_SESSION['__total_productos__'] . ' resultado';
  }else{
    $texto_resultados = $_SESSION['__total_productos__'] . ' resultados';
  }

  $contenedor_resultados = '
    <h2 class="p-subtitulo p-text_p">' . $texto_resultados . '</h2>';
  
  if(isset($_SESSION['__opcion_busqueda__']) || isset($_SESSION['__terminos_buscados'])){

    /////////////////////////////////////////////////////////////////////////
    /////////////////////// CONTENEDOR DE FILTROS ////////////////////////
    $contenedor_filtros = '
    <div class="p-busqueda-filtros_movil_fondo" id="id-busqueda-filtros_button_close"></div>
    
    <div class="p-busqueda-filtros_movil_contenedor">
    
      <div class="p-busqueda-filtros_movil_loading_disabled" id="id-busqueda_loading_filros_mensaje_movil">
        <div class="p-busqueda-filtros_movil_loading_contenedor">
          <div class="p-busqueda-filtros_movil_loading_div">
            <div></div>
            <span>Aplicando filtro</span>
          </div>
        </div>
      </div>

      <div class="p-busqueda-filtros_movil_contenedor_aside">
        <aside class="p-busqueda-menu_options p-busqueda-filtros_movil_contenedor_filtros" id="id-busqueda-menu_options">
          <div class="p-busqueda-filtros_desktop_loading_disabled" id="id-busqueda_loading_filros_mensaje_desktop">
            <div class="p-busqueda-filtros_desktop_loading_div">
              <div></div>
              <span>Aplicando filtro</span>
            </div>
          </div>';

    //SI NO HAY PRODUCTOS
    if($totalProductos === 0 && !isset($_SESSION['__rango_precios__'])){
      $contenedor_filtros .= '
          <a class="p-busqueda-p_option g-busqueda-filtros p-busqueda-p_option_active" data-filtro="1">
            <span>Categorías</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op g-busqueda-menu_opciones" data-filtro="1" style="display:block">
            <li>
              <a class="p-busqueda-no_cat-marc">
                <span>Ninguna categoría</span>
              </a>
            </li>
          </ul>
          <a class="p-busqueda-p_option p-busqueda-filtros_separador g-busqueda-filtros p-busqueda-p_option_active" data-filtro="4">
            <span>Marcas</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op g-busqueda-menu_opciones" data-filtro="4" style="display:block">
            <li>
              <a class="p-busqueda-no_cat-marc">
                <span>Ninguna marca</span>
              </a>
            </li>
          </ul>';

    //SI NO HAY PRODUCTOS Y EXISTE EL FILTRO DE RANGO DE PRECIOS
    }else if($totalProductos === 0 && isset($_SESSION['__rango_precios__'])){
      $filtroRangoPrecio = 1;
      
      if(isset($_SESSION['__rango_precios__'])){
        $precioInicial = number_format($_SESSION['__rango_precios__'][0]['precio_inicial'], 2, '.', '');
        $precioFinal = number_format($_SESSION['__rango_precios__'][0]['precio_final'], 2, '.', '');
      }else{
        $precioInicial = '';
        $precioFinal = '';
      }

      $contenedor_filtros .= '
          <a class="p-busqueda-p_option p-busqueda-filtros_separador g-busqueda-filtros';

      if(isset($_SESSION['__rango_precios__']) || isset($_SESSION['__activacion_precio_menor_mayor']) || isset($_SESSION['__activacion_precio_mayor_menor'])){
        $contenedor_filtros .= ' p-busqueda-p_option_active';
      }

      $contenedor_filtros .= '" data-filtro="5">
            <span>Filtros avanzados</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <div class="p-busqueda-filtros_avanzados_div g-busqueda-menu_opciones" data-filtro="5"';
    
      if(isset($_SESSION['__rango_precios__']) || isset($_SESSION['__activacion_precio_menor_mayor']) || isset($_SESSION['__activacion_precio_mayor_menor'])){
        $contenedor_filtros .= ' style="display:block"';
      }

      $contenedor_filtros .= '>
            <p>
              <span><b>Rango de precio:</b></span>
            </p>
            <p class="p-text p-text_help">Formato moneda, ejemplo: 0.00</p>
            <div class="p-field">
              <div class="p-control">
                <span style="width: 16px; display: flex; align-items: center; justify-content: center; color: #16a085; font-size: .9rem !important; padding: 0px 5px 3px 5px;">
                  <i class="fas fa-dollar-sign"></i>
                </span>
                <input type="text" class="p-input g-busqueda-precio_inicial_input" placeholder="Precio inicial" value="' . $precioInicial . '" autocomplete="off">
                <span style="display: flex; justify-content: center; color: #16a085; font-size: 0.7rem !important; padding: 0px 5px;"><b>MXN</b></span>
              </div>
              <p class="p-text p-text_info g-busqueda-precio_inicial_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo formato moneda con 2 decimales, si es necesario.</span>
              </p>
              <p class="p-text p-text_error g-busqueda-precio_inicial_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>Este campo se encuentra vacío.</span>
              </p>
            </div>
            <div class="p-field">
              <div class="p-control">
                <span style="width: 16px; display: flex; align-items: center; justify-content: center; color: #16a085; font-size: .9rem !important; padding: 0px 5px 3px 5px;">
                  <i class="fas fa-dollar-sign"></i>
                </span>
                <input type="text" class="p-input g-busqueda-precio_final_input" placeholder="Precio final" value="' . $precioFinal . '" autocomplete="off">
                <span style="display: flex; justify-content: center; color: #16a085; font-size: 0.7rem !important; padding: 0px 5px;"><b>MXN</b></span>
              </div>
              <p class="p-text p-text_info g-busqueda-precio_final_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo formato moneda con 2 decimales, si es necesario.</span>
              </p>
              <p class="p-text p-text_error g-busqueda-precio_final_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>Este campo se encuentra vacío.</span>
              </p>
            </div>
            <div>
              <a class="p-button p-button_success p-busqueda-button_filtros g-busqueda-rangoPrecio_filtrar">
                <span>
                  <i class="fas fa-funnel-dollar"></i>
                </span>
                <span><b>Filtrar</b></span>
              </a>';

      if(isset($_SESSION['__rango_precios__'])){
        $contenedor_filtros .= '
              <a class="p-button p-button_delete p-busqueda-button_filtros g-busqueda-rangoPrecio_eliminar">
                <span>
                  <i class="fas fa-trash-alt"></i>
                </span>
                <span><b>Eliminar filtro</b></span>
              </a>';
      }

      $contenedor_filtros .= '
            </div>
          </div>';

    //SI HAY PRODUCTOS
    }else{
      ////////////////  FILTRO DE CATEGORIAS  ////////////////
      // SI NO EXISTE UNA OPCION DE BUSQUEDA O EXISTE PERO NO ES CATEGORIAS, SE HACER LO QUE SE ENCUENTRA DENTRO DEL IF

      if(!isset($_SESSION['__opcion_busqueda__']) || $_SESSION['__opcion_busqueda__'] !== "categorias"){
        $contenedor_filtros .= '
          <a class="p-busqueda-p_option g-busqueda-filtros' . ( isset($_SESSION['__categoria_seleccionada']) ? ' p-busqueda-p_option_active' : '' ) . '" data-filtro="1">
            <span>Categorías</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op g-busqueda-menu_opciones" data-filtro="1"' . ( isset($_SESSION['__categoria_seleccionada']) ? ' style="display:block"' : '' ) . '>';

        if(isset($_SESSION['__categoria_seleccionada'])){
          $categoria = mb_strtolower(trim($_SESSION['__categoria_seleccionada'][0]['nombre']), 'UTF-8');
          $contenedor_filtros .= '
            <li>
              <a class="p-busqueda-menu_pag_activa g-busqueda-filtro_opcion" data-seccion="categoria" data-id="' . encriptar(trim($_SESSION['__categoria_seleccionada'][0]['id'])) . '">
                <span>' . ucfirst($categoria) . '</span>
                <span>
                  <i class="fas fa-times"></i>
                </span>
              </a>
            </li>';
        }else{
          if(isset($_SESSION['__marca_seleccionada'])){
            foreach($_SESSION['__categorias_Busqueda__'] as $dato_catBusqueda){
              $categoria = mb_strtolower(trim($dato_catBusqueda['nombre']), 'UTF-8');

              $cantidad_cat = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $dato_catBusqueda['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){

                    if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                      $cantidad_cat++;
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $dato_catBusqueda['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){
                    $cantidad_cat++;
                  }
                }
              }

              if($cantidad_cat !== 0){
                $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="categoria" data-id="' . encriptar(trim($dato_catBusqueda['id'])) . '">
                <span>' . ucfirst($categoria) . ' (' . $cantidad_cat . ')</span>
              </a>
            </li>';
              }
            }
          }else{
            foreach($_SESSION['__categorias_Busqueda__'] as $dato_catBusqueda){
              $categoria = mb_strtolower(trim($dato_catBusqueda['nombre']), 'UTF-8');

              $cantidad_cat = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $dato_catBusqueda['id'] === (int) $dato_prod['categoriaID'] ){

                    if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                      $cantidad_cat++;
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $dato_catBusqueda['id'] === (int) $dato_prod['categoriaID'] ){
                    $cantidad_cat++;
                  }
                }
              }

              if($cantidad_cat !== 0){
                $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="categoria" data-id="' . encriptar(trim($dato_catBusqueda['id'])) . '">
                <span>' . ucfirst($categoria) . ' (' . $cantidad_cat . ')</span>
              </a>
            </li>';
              }
            }
          }
        }

        $contenedor_filtros .= '
          </ul>';
      }

      if( !($totalProductos === 0 && isset($_SESSION['__rango_precios__'])) ){

        ////////// SUBCATEGORIA DE TIPOS //////////
        if(isset($_SESSION['__categoria_seleccionada'])){

          $sub_opcion = isset($_SESSION['__opcion_busqueda__']) && $_SESSION['__opcion_busqueda__'] === "categorias" ? '' : ' p-busqueda-p_option_sub';

          $contenedor_filtros .= '
          <a class="p-busqueda-p_option' . $sub_opcion . ' p-busqueda-filtros_separador g-busqueda-filtros' . ( isset($_SESSION['__subcat_tipo_seleccionadas']) ? ' p-busqueda-p_option_active' : '' ) . '" data-filtro="2">
            <span>Selección por tipo</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op p-busqueda-menu_op_sub g-busqueda-menu_opciones" data-filtro="2"' . ( isset($_SESSION['__subcat_tipo_seleccionadas']) ? ' style="display:block"' : '' ) . '>';

          if(isset($_SESSION['__marca_seleccionada'])){
            $id_elemento_consecutivo = 1;
            foreach($_SESSION['__subcat_tipos_Busqueda__'] as $dato_subcat_tipoBusqueda){
              $subcat_tipo = mb_strtolower(trim($dato_subcat_tipoBusqueda['nombre']), 'UTF-8');

              $cantidad_subcat_tipo = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){

                    if( (int) $dato_subcat_tipoBusqueda['id'] === (int) $dato_prod['subcat_tipoID'] ){

                      if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                        $cantidad_subcat_tipo++;
                      }
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){

                    if( (int) $dato_subcat_tipoBusqueda['id'] === (int) $dato_prod['subcat_tipoID'] ){
                      $cantidad_subcat_tipo++;
                    }
                  }
                }
              }

              if($cantidad_subcat_tipo !== 0){
                if(isset($_SESSION['__subcat_tipo_seleccionadas'])){
                  $ids_subcatTipos = array_column($_SESSION['__subcat_tipo_seleccionadas'], 'id');
                  if(in_array($dato_subcat_tipoBusqueda['id'], $ids_subcatTipos)){
                    $checked_estado = ' checked';
                    $cantidad_prodSubcat = isset($_SESSION['__subcat_especificas_seleccionadas']) ? '' : ' (' . $cantidad_subcat_tipo . ')';
                  }else{
                    $checked_estado = '';
                    $cantidad_prodSubcat = '';
                  }
                }else{
                  $checked_estado = '';
                  $cantidad_prodSubcat = ' (' . $cantidad_subcat_tipo . ')';
                }

                $contenedor_filtros .= '
            <li>
              <input type="checkbox" class="p-checkbox_input_check" id="id-busqueda-subcat_tipo_' . $id_elemento_consecutivo . '"' . $checked_estado . '>
              <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-filtro_opcion" for="id-busqueda-subcat_tipo_' . $id_elemento_consecutivo . '" data-seccion="subcat_tipo" data-id="' . encriptar(trim($dato_subcat_tipoBusqueda['id'])) . '">
                <span>' . ucfirst($subcat_tipo) . $cantidad_prodSubcat . '</span>
              </label>
            </li>';

                $id_elemento_consecutivo++;
              }
            }
          }else{
            $id_elemento_consecutivo = 1;
            foreach($_SESSION['__subcat_tipos_Busqueda__'] as $dato_subcat_tipoBusqueda){
              $subcat_tipo = mb_strtolower(trim($dato_subcat_tipoBusqueda['nombre']), 'UTF-8');

              $cantidad_subcat_tipo = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID'] ){

                    if( (int) $dato_subcat_tipoBusqueda['id'] === (int) $dato_prod['subcat_tipoID'] ){

                      if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                        $cantidad_subcat_tipo++;
                      }
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID'] ){

                    if( (int) $dato_subcat_tipoBusqueda['id'] === (int) $dato_prod['subcat_tipoID'] ){
                      $cantidad_subcat_tipo++;
                    }
                  }
                }
              }

              if($cantidad_subcat_tipo !== 0){
                if(isset($_SESSION['__subcat_tipo_seleccionadas'])){
                  $ids_subcatTipos = array_column($_SESSION['__subcat_tipo_seleccionadas'], 'id');
                  if(in_array($dato_subcat_tipoBusqueda['id'], $ids_subcatTipos)){
                    $checked_estado = ' checked';
                    $cantidad_prodSubcat = isset($_SESSION['__subcat_especificas_seleccionadas']) ? '' : ' (' . $cantidad_subcat_tipo . ')';
                  }else{
                    $checked_estado = '';
                    $cantidad_prodSubcat = '';
                  }
                }else{
                  $checked_estado = '';
                  $cantidad_prodSubcat = ' (' . $cantidad_subcat_tipo . ')';
                }

                $contenedor_filtros .= '
            <li>
              <input type="checkbox" class="p-checkbox_input_check" id="id-busqueda-subcat_tipo_' . $id_elemento_consecutivo . '"' . $checked_estado . '>
              <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-filtro_opcion" for="id-busqueda-subcat_tipo_' . $id_elemento_consecutivo . '" data-seccion="subcat_tipo" data-id="' . encriptar(trim($dato_subcat_tipoBusqueda['id'])) . '">
                <span>' . ucfirst($subcat_tipo) . $cantidad_prodSubcat . '</span>
              </label>
            </li>';

                $id_elemento_consecutivo++;
              }
            }
          }

          $contenedor_filtros .= '
          </ul>';
        }

        ////////// SUBCATEGORIA ESPECIFICA //////////
        if(isset($_SESSION['__categoria_seleccionada']) && isset($_SESSION['__subcat_tipo_seleccionadas'])){
          $contenedor_filtros .= '
          <a class="p-busqueda-p_option p-busqueda-p_option_sub p-busqueda-filtros_separador g-busqueda-filtros' . ( isset($_SESSION['__subcat_especificas_seleccionadas']) ? ' p-busqueda-p_option_active' : '' ) . '" data-filtro="3">
            <span>Selección específica</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op p-busqueda-menu_op_sub g-busqueda-menu_opciones" data-filtro="3"' . ( isset($_SESSION['__subcat_especificas_seleccionadas']) ? ' style="display:block"' : '' ) . '>';

          if(isset($_SESSION['__marca_seleccionada'])){
            $id_elemento_consecutivo = 1;
            foreach($_SESSION['__subcat_especificas_Busqueda__'] as $dato_subcat_especificaBusqueda){
              $subcat_especifica = mb_strtolower(trim($dato_subcat_especificaBusqueda['nombre']), 'UTF-8');

              $cantidad_subcat_especifica = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){

                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                      if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                        if( (int) $dato_subcat_especificaBusqueda['id'] === (int) $dato_prod['subcat_especificaID'] ){

                          if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                            $cantidad_subcat_especifica++;
                          }
                        }
                      }
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) && ((int) $_SESSION['__marca_seleccionada'][0]['id'] === (int) $dato_prod['marcaID']) ){

                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                      if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                        if( (int) $dato_subcat_especificaBusqueda['id'] === (int) $dato_prod['subcat_especificaID'] ){
                          $cantidad_subcat_especifica++;
                        }
                      }
                    }
                  }
                }
              }

              if($cantidad_subcat_especifica !== 0){
                if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
                  $ids_subcatEspecificas = array_column($_SESSION['__subcat_especificas_seleccionadas'], 'id');
                  if(in_array($dato_subcat_especificaBusqueda['id'], $ids_subcatEspecificas)){
                    $checked_estado = ' checked';
                    $cantidad_prodSubcat = ' (' . $cantidad_subcat_especifica . ')';
                  }else{
                    $checked_estado = '';
                    $cantidad_prodSubcat = '';
                  }
                }else{
                  $checked_estado = '';
                  $cantidad_prodSubcat = ' (' . $cantidad_subcat_especifica . ')';
                }

                $contenedor_filtros .= '
            <li>
              <input type="checkbox" class="p-checkbox_input_check" id="id-busqueda-subcat_especifica_' . $id_elemento_consecutivo . '"' . $checked_estado . '>
              <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-filtro_opcion" for="id-busqueda-subcat_especifica_' . $id_elemento_consecutivo . '" data-seccion="subcat_especifica" data-id="' . encriptar(trim($dato_subcat_especificaBusqueda['id'])) . '">
                <span>' . ucfirst($subcat_especifica) . $cantidad_prodSubcat . '</span>
              </label>
            </li>';

                $id_elemento_consecutivo++;
              }
            }
          }else{
            $id_elemento_consecutivo = 1;
            foreach($_SESSION['__subcat_especificas_Busqueda__'] as $dato_subcat_especificaBusqueda){
              $subcat_especifica = mb_strtolower(trim($dato_subcat_especificaBusqueda['nombre']), 'UTF-8');

              $cantidad_subcat_especifica = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID'] ){

                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                      if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                        if( (int) $dato_subcat_especificaBusqueda['id'] === (int) $dato_prod['subcat_especificaID'] ){

                          if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                            $cantidad_subcat_especifica++;
                          }
                        }
                      }
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID'] ){

                    foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                      if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                        if( (int) $dato_subcat_especificaBusqueda['id'] === (int) $dato_prod['subcat_especificaID'] ){
                          $cantidad_subcat_especifica++;
                        }
                      }
                    }
                  }
                }
              }

              if($cantidad_subcat_especifica !== 0){
                if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
                  $ids_subcatEspecificas = array_column($_SESSION['__subcat_especificas_seleccionadas'], 'id');
                  if(in_array($dato_subcat_especificaBusqueda['id'], $ids_subcatEspecificas)){
                    $checked_estado = ' checked';
                    $cantidad_prodSubcat = ' (' . $cantidad_subcat_especifica . ')';
                  }else{
                    $checked_estado = '';
                    $cantidad_prodSubcat = '';
                  }
                }else{
                  $checked_estado = '';
                  $cantidad_prodSubcat = ' (' . $cantidad_subcat_especifica . ')';
                }

                $contenedor_filtros .= '
            <li>
              <input type="checkbox" class="p-checkbox_input_check" id="id-busqueda-subcat_especifica_' . $id_elemento_consecutivo . '"' . $checked_estado . '>
              <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-filtro_opcion" for="id-busqueda-subcat_especifica_' . $id_elemento_consecutivo . '" data-seccion="subcat_especifica" data-id="' . encriptar(trim($dato_subcat_especificaBusqueda['id'])) . '">
                <span>' . ucfirst($subcat_especifica) . $cantidad_prodSubcat . '</span>
              </label>
            </li>';

                $id_elemento_consecutivo++;
              }
            }
          }

          $contenedor_filtros .= '
          </ul>';
        }
      }
      ////////////////  FILTRO DE MARCAS  ////////////////
      // SI NO EXISTE UNA OPCION DE BUSQUEDA O EXISTE PERO NO ES MARCAS, SE HACE LO QUE SE ENCUENTRA DENTRO DEL IF

      if(!isset($_SESSION['__opcion_busqueda__']) || $_SESSION['__opcion_busqueda__'] !== "marcas"){
        $contenedor_filtros .= '
          <a class="p-busqueda-p_option p-busqueda-filtros_separador g-busqueda-filtros' . ( isset($_SESSION['__marca_seleccionada']) ? ' p-busqueda-p_option_active' : '' ) . '" data-filtro="4">
            <span>Marcas</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <ul class="p-busqueda-menu_op g-busqueda-menu_opciones" data-filtro="4"' . ( isset($_SESSION['__marca_seleccionada']) ? ' style="display:block"' : '' ) . '>';

        if(isset($_SESSION['__marca_seleccionada'])){
          $marca = mb_strtolower(trim($_SESSION['__marca_seleccionada'][0]['nombre']), 'UTF-8');
          $contenedor_filtros .= '
            <li>
              <a class="p-busqueda-menu_pag_activa g-busqueda-filtro_opcion" data-seccion="marca" data-id="' . encriptar(trim($_SESSION['__marca_seleccionada'][0]['id'])) . '">
                <span>' . ucfirst($marca) . '</span>
                <span>
                  <i class="fas fa-times"></i>
                </span>
              </a>
            </li>';
        }else{
          // SI EXISTE LA VARIABLE SESSION DE LA CATEGORIA SELECCIONADA, SE HACE LO SIGUIENTE
          if(isset($_SESSION['__categoria_seleccionada'])){

            // SI EXISTE LA VARIABLE SESSION DE LAS SUBCATEGORIAS DE TIPO SELECCIONADAS, SE HACE LO SIGUIENTE
            if(isset($_SESSION['__subcat_tipo_seleccionadas'])){

              // SI EXISTE LA VARIABLE SESSION DE LAS SUBCATEGORIAS ESPECIFICAS SELECCIONADAS, SE HACE LO SIGUIENTE
              if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
                foreach($_SESSION['__marcas_Busqueda__'] as $dato_marcaBusqueda){
                  $marca = mb_strtolower(trim($dato_marcaBusqueda['nombre']), 'UTF-8');

                  $cantidad_marca = 0;
                  if(isset($_SESSION['__rango_precios__'])){
                    foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                      if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){

                        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                          if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                            foreach($_SESSION['__subcat_especificas_seleccionadas'] as $dato_subcat_especifica){
                              if( (int) $dato_subcat_especifica['id'] === (int) $dato_prod['subcat_especificaID'] ){

                                if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                                  $cantidad_marca++;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }else{
                    foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                      if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){

                        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                          if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                            foreach($_SESSION['__subcat_especificas_seleccionadas'] as $dato_subcat_especifica){
                              if( (int) $dato_subcat_especifica['id'] === (int) $dato_prod['subcat_especificaID'] ){
                                $cantidad_marca++;
                              }
                            }
                          }
                        }
                      }
                    }
                  }

                  if($cantidad_marca !== 0){
                    $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="marca" data-id="' . encriptar(trim($dato_marcaBusqueda['id'])) . '">
                <span>' . ucfirst($marca) . ' (' . $cantidad_marca . ')</span>
              </a>
            </li>';
                  }
                }
              }else{
                // SI NO EXISTE LA VARIABLE SESSION DE LAS SUBCATEGORIAS ESPECIFICAS SELECCIONADAS, SE HACE LO SIGUIENTE
                foreach($_SESSION['__marcas_Busqueda__'] as $dato_marcaBusqueda){
                  $marca = mb_strtolower(trim($dato_marcaBusqueda['nombre']), 'UTF-8');

                  $cantidad_marca = 0;
                  if(isset($_SESSION['__rango_precios__'])){
                    foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                      if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){

                        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                          if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){

                            if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                              $cantidad_marca++;
                            }
                          }
                        }
                      }
                    }
                  }else{
                    foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                      if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){

                        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $dato_subcat_tipo){
                          if( (int) $dato_subcat_tipo['id'] === (int) $dato_prod['subcat_tipoID'] ){
                            $cantidad_marca++;
                          }
                        }
                      }
                    }
                  }

                  if($cantidad_marca !== 0){
                    $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="marca" data-id="' . encriptar(trim($dato_marcaBusqueda['id'])) . '">
                <span>' . ucfirst($marca) . ' (' . $cantidad_marca . ')</span>
              </a>
            </li>';
                  }
                }
              }
            }else{
              // SI NO EXISTE LA VARIABLE SESSION DE LAS SUBCATEGORIAS DE TIPO SELECCIONADAS, SE HACE LO SIGUIENTE
              foreach($_SESSION['__marcas_Busqueda__'] as $dato_marcaBusqueda){
                $marca = mb_strtolower(trim($dato_marcaBusqueda['nombre']), 'UTF-8');

                $cantidad_marca = 0;
                if(isset($_SESSION['__rango_precios__'])){
                  foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                    if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){

                      if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                        $cantidad_marca++;
                      }
                    }
                  }
                }else{
                  foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                    if( ((int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID']) && ((int) $_SESSION['__categoria_seleccionada'][0]['id'] === (int) $dato_prod['categoriaID']) ){
                      $cantidad_marca++;
                    }
                  }
                }

                if($cantidad_marca !== 0){
                  $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="marca" data-id="' . encriptar(trim($dato_marcaBusqueda['id'])) . '">
                <span>' . ucfirst($marca) . ' (' . $cantidad_marca . ')</span>
              </a>
            </li>';
                }
              }
            }
          }else{
            // SI NO EXISTE LA VARIABLE SESSION DE LA CATEGORIA SELECCIONADA, SE HACE LO SIGUIENTE
            foreach($_SESSION['__marcas_Busqueda__'] as $dato_marcaBusqueda){
              $marca = mb_strtolower(trim($dato_marcaBusqueda['nombre']), 'UTF-8');

              $cantidad_marca = 0;
              if(isset($_SESSION['__rango_precios__'])){
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID'] ){

                    if( ((float) $dato_prod['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $dato_prod['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                      $cantidad_marca++;
                    }
                  }
                }
              }else{
                foreach($_SESSION['__productosBusqueda__'] as $dato_prod){
                  if( (int) $dato_marcaBusqueda['id'] === (int) $dato_prod['marcaID'] ){
                    $cantidad_marca++;
                  }
                }
              }

              if($cantidad_marca !== 0){
                $contenedor_filtros .= '
            <li>
              <a class="g-busqueda-filtro_opcion" data-seccion="marca" data-id="' . encriptar(trim($dato_marcaBusqueda['id'])) . '">
                <span>' . ucfirst($marca) . ' (' . $cantidad_marca . ')</span>
              </a>
            </li>';
              }
            }
          }
        }

        $contenedor_filtros .= '
          </ul>';
      }

      if(isset($_SESSION['__rango_precios__'])){
        $precioInicial = number_format($_SESSION['__rango_precios__'][0]['precio_inicial'], 2, '.', '');
        $precioFinal = number_format($_SESSION['__rango_precios__'][0]['precio_final'], 2, '.', '');
      }else{
        $precioInicial = '';
        $precioFinal = '';
      }

      $contenedor_filtros .= '
          <a class="p-busqueda-p_option p-busqueda-filtros_separador g-busqueda-filtros';

      if(isset($_SESSION['__rango_precios__']) || isset($_SESSION['__activacion_precio_menor_mayor']) || isset($_SESSION['__activacion_precio_mayor_menor'])){
        $contenedor_filtros .= ' p-busqueda-p_option_active';
      }

      $contenedor_filtros .= '" data-filtro="5">
            <span>Filtros avanzados</span>
            <span>
              <i class="fas fa-chevron-down"></i>
            </span>
          </a>
          <div class="p-busqueda-filtros_avanzados_div g-busqueda-menu_opciones" data-filtro="5"';
    
      if(isset($_SESSION['__rango_precios__']) || isset($_SESSION['__activacion_precio_menor_mayor']) || isset($_SESSION['__activacion_precio_mayor_menor'])){
        $contenedor_filtros .= ' style="display:block"';
      }

      $contenedor_filtros .= '>
            <p>
              <span><b>Rango de precio:</b></span>
            </p>
            <p class="p-text p-text_help">Formato moneda, ejemplo: 0.00</p>
            <div class="p-field">
              <div class="p-control">
                <span style="width: 16px; display: flex; align-items: center; justify-content: center; color: #16a085; font-size: .9rem !important; padding: 0px 5px 3px 5px;">
                  <i class="fas fa-dollar-sign"></i>
                </span>
                <input type="text" class="p-input g-busqueda-precio_inicial_input" placeholder="Precio inicial" value="' . $precioInicial . '" autocomplete="off">
                <span style="display: flex; justify-content: center; color: #16a085; font-size: 0.7rem !important; padding: 0px 5px;"><b>MXN</b></span>
              </div>
              <p class="p-text p-text_info g-busqueda-precio_inicial_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo formato moneda con 2 decimales, si es necesario.</span>
              </p>
              <p class="p-text p-text_error g-busqueda-precio_inicial_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>Este campo se encuentra vacío.</span>
              </p>
            </div>
            <div class="p-field">
              <div class="p-control">
                <span style="width: 16px; display: flex; align-items: center; justify-content: center; color: #16a085; font-size: .9rem !important; padding: 0px 5px 3px 5px;">
                  <i class="fas fa-dollar-sign"></i>
                </span>
                <input type="text" class="p-input g-busqueda-precio_final_input" placeholder="Precio final" value="' . $precioFinal . '" autocomplete="off">
                <span style="display: flex; justify-content: center; color: #16a085; font-size: 0.7rem !important; padding: 0px 5px;"><b>MXN</b></span>
              </div>
              <p class="p-text p-text_info g-busqueda-precio_final_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span>Sólo formato moneda con 2 decimales, si es necesario.</span>
              </p>
              <p class="p-text p-text_error g-busqueda-precio_final_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span>Este campo se encuentra vacío.</span>
              </p>
            </div>
            <div>
              <a class="p-button p-button_success p-busqueda-button_filtros g-busqueda-rangoPrecio_filtrar">
                <span>
                  <i class="fas fa-funnel-dollar"></i>
                </span>
                <span><b>Filtrar</b></span>
              </a>';

      if(isset($_SESSION['__rango_precios__'])){
        $contenedor_filtros .= '
              <a class="p-button p-button_delete p-busqueda-button_filtros g-busqueda-rangoPrecio_eliminar">
                <span>
                  <i class="fas fa-trash-alt"></i>
                </span>
                <span><b>Eliminar filtro</b></span>
              </a>';
      }

      $contenedor_filtros .= '
            </div>
            <hr class="p-busqueda-filtros_hr">
            <p>
              <span><b>Ordenar por:</b></span>
            </p>';

      if(isset($_SESSION['__activacion_precio_menor_mayor'])){
        $value_input = 1;
        $checked_label = ' checked';
      }else{
        $value_input = 0;
        $checked_label = '';
      }

      $contenedor_filtros .= '
            <input type="checkbox" class="p-checkbox_input_check" id="id-ordenar-precio_menor"' . $checked_label . '>
            <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-ordenar_precio" for="id-ordenar-precio_menor" data-seccion="precio_menor" data-activado="' . $value_input . '">
              <span>Precio menor</span>
            </label>';

      if(isset($_SESSION['__activacion_precio_mayor_menor'])){
        $value_input = 1;
        $checked_label = ' checked';
      }else{
        $value_input = 0;
        $checked_label = '';
      }

      $contenedor_filtros .= '
            <input type="checkbox" class="p-checkbox_input_check" id="id-ordenar-precio_mayor"' . $checked_label . '>
            <label class="p-checkbox_label_check p-busqueda-checkbox_label g-busqueda-ordenar_precio" for="id-ordenar-precio_mayor" data-seccion="precio_mayor" data-activado="' . $value_input . '">
              <span>Precio mayor</span>
            </label>
          </div>';
    }

    $contenedor_filtros .= '
        </aside>
      </div>
    </div>';

    /////////////////////////////////////////////////////////////////////////
    /////////////////////// BOTON DE FILTROS MOVIL ////////////////////////


    if( $totalProductos !== 0 || ( $totalProductos === 0 && isset($_SESSION['__rango_precios__']) ) ){
      $boton_filtros = '
    <div id="id-busqueda-filtros_contenedor_boton">
      <a class="p-button p-button_file_pdf p-button_largo p-busqueda-filtros_movil_button p-busqueda-filtros_movil_button_fixed" id="id-busqueda-filtros_button_open">
        <span>' . $_SESSION['__total_productos__'] . ' resultados</span>
        <div>
          <span>
            <b>Filtros</b>
          </span>
          <span>
            <i class="fas fa-sliders-h"></i>
          </span>
        </div>
      </a>
    </div>';
    }else{
      $boton_filtros = '';
    }

    /////////////////////////////////////////////////////////////////////////
    /////////////////////// CONTENEDOR DE PRODUCTOS ////////////////////////

    $productosPorPagina = (int) trim($_SESSION['__productos_por_pagina__']);

    if(isset($_SESSION['__pagina_actual']) && validar_campo_numerico(trim($_SESSION['__pagina_actual']))){
      $pagina_actual = (int) trim($_SESSION['__pagina_actual']);
    }else{
      $pagina_actual = 1;
    }

    if($totalProductos === 0){

      if(isset($_SESSION['__rango_precios__'])){
        $contenedor_productos = '
      <div class="p-notification p-notification_letter_info">
        <span>
          <b>El filtro de rango de precio no arrojó ningún resultado. Para generar o regresar a los resutados anteriores, cambia o elimina el filtro.</b>
        </span>
      </div>';
      }else{
        $contenedor_productos = '';
      }

      $contenedor_productos .= '
      <div class="p-busqueda-contenedor_no_productos">
        <img src="images/busqueda_no_productos.png" alt="No se encontraron productos.">
      </div>';
    }else{
      /////////////////////////////////////////////////////////////////////////
      /////////////////////// CONTENEDOR DE PAGINACION ////////////////////////

      //PAGINAS QUE SE VERÁN EN LA PAGINACIÓN, SE REDONDEA HACIA ARRIBA
      $paginas_totales = (int) ceil($totalProductos / $productosPorPagina);

      ////////////////  BOTONES PARA NAVEGACION DE LA PAGINACION  ////////////////
      $contenedor_paginacion .= '
        <nav class="p-busqueda-paginacion_nav">
          <div class="p-busqueda-paginacion_botones">
            <a class="p-button_inverso' . ( $pagina_actual <= 1 ? '" disabled' : ' g-busqueda-paginacion" data-pagina="' . ($pagina_actual - 1) . '"' ) . '>
              <span>
                <i class="fas fa-chevron-left"></i>
              </span>
              <span><b>Anterior</b></span>
            </a>
            
            <ul class="p-busqueda-paginacion_lista_paginas">
              <li><a class="p-button p-button_inverso p-button_square' . ( $pagina_actual === 1 ? ' p-busqueda-paginacion_pagina_activa"' : ' g-busqueda-paginacion" data-pagina="1"' ) . '><b>1</b></a></li>';

      //SI EXISTEN MENOS DE 11 PAGINAS A MOSTRAR EN LA PAGINACION, ENTONCES SE HARA LO SIGUIENTE
      if($paginas_totales <= 10){
        for($pagina_generada = 2; $pagina_generada < $paginas_totales; $pagina_generada++){
          $contenedor_paginacion .= '
              <li><a class="p-button p-button_inverso p-button_square' . ( $pagina_actual === $pagina_generada ? ' p-busqueda-paginacion_pagina_activa"' : ' g-busqueda-paginacion" data-pagina="' . $pagina_generada . '"' ) . '><b>' . $pagina_generada . '</b></a></li>';
        }
      }else{
        if($pagina_actual >= 5){
          $contenedor_paginacion .= '
              <li>
                <span><i class="fas fa-ellipsis-h"></i></span>
              </li>';
        }

        if($pagina_actual < 5){
          $inicio = 2;
          $final = 5;
        }else if($pagina_actual > ($paginas_totales - 4)){
          $inicio =  ($paginas_totales - 4);
          $final = $paginas_totales - 1;
        }else{
          $inicio = $pagina_actual - 2;
          $final = $pagina_actual + 2;
        }

        for($pagina_generada = $inicio; $pagina_generada <= $final; $pagina_generada++){
          $contenedor_paginacion .= '
              <li><a class="p-button p-button_inverso p-button_square' . ( $pagina_actual === $pagina_generada ? ' p-busqueda-paginacion_pagina_activa"' : ' g-busqueda-paginacion" data-pagina="' . $pagina_generada . '"' ) . '><b>' . $pagina_generada . '</b></a></li>';
        }

        if($pagina_actual <= ($paginas_totales - 4)){
          $contenedor_paginacion .= '
              <li>
                <span><i class="fas fa-ellipsis-h"></i></span>
              </li>';
        }
      }

      if($paginas_totales !== 1){
        $contenedor_paginacion .= '
              <li><a class="p-button p-button_inverso p-button_square' . ( $pagina_actual === $paginas_totales ? ' p-busqueda-paginacion_pagina_activa"' : ' g-busqueda-paginacion" data-pagina="' . $paginas_totales . '"' ) . '><b>' . $paginas_totales . '</b></a></li>';
      }
      
      $contenedor_paginacion .= '
            </ul>
            
            <div class="p-select p-busqueda-paginacion_contenedor_select">
              <select class="p-input p-busqueda-paginacion_select" id="id-busqueda-paginacion_select">';
      
      for($pagina_opcion = 1; $pagina_opcion <= $paginas_totales; $pagina_opcion++){
        $selected = '';
        $value = ' value="' . $pagina_opcion . '"';
        
        if($pagina_actual === $pagina_opcion){
          $selected = ' selected="selected"';
          $value = '';
        }
        
        $contenedor_paginacion .= '
                <option' . $selected . $value . '>' . $pagina_opcion . '</option>';
      }
      
      $contenedor_paginacion .= '
              </select>
            </div>
            
            <a class="p-button_inverso' . ( $pagina_actual >= $paginas_totales ? '" disabled' : ' g-busqueda-paginacion" data-pagina="' . ($pagina_actual + 1) . '"' ) . '>
              <span><b>Siguiente</b></span>
              <span>
                <i class="fas fa-chevron-right"></i>
              </span>
            </a>
          </div>
        </nav>';

      $contenedor_productos = '
      <div>' . $contenedor_paginacion . '</div>';

      $inicio_articulo = ($pagina_actual - 1) * $productosPorPagina;
      $final_producto = $pagina_actual * $productosPorPagina;

      if($paginas_totales === 1 && $final_producto > $totalProductos){
        $final_producto = $totalProductos;
      }

      //SE OBTIENEN LOS PRODUCTOS TOTALES DE LA BUSQUEDA
      $contador = 1;
      $totalRegistros = $productosPorPagina;
    
      $contenedor_productos .= '
      <div class="p-columns-products_container">';
    
      for($i = $inicio_articulo; $i < $final_producto; $i++){
        if(!isset($_SESSION['__productosImprimir_Busqueda__'][$i]['codigoProducto'])){
          break;
        }

        $descripcion = (string) trim($_SESSION['__productosImprimir_Busqueda__'][$i]['descripcion'], " \xC2\xA0");
        $descripcion_editada = str_replace('"', '', $descripcion);
        $descripcionURL = (string) trim($_SESSION['__productosImprimir_Busqueda__'][$i]['descripcionURL']);
        
        $tieneImagen = (int) trim($_SESSION['__productosImprimir_Busqueda__'][$i]['tieneImagen']);
        $numeroUbicacionImagen = $_SESSION['__productosImprimir_Busqueda__'][$i]['numeroUbicacionImagen']; // PARA REVISAR SI ES NULL
        $nombreMarca = trim($_SESSION['__productosImprimir_Busqueda__'][$i]['nombreMarca']);
        $nombreImagen = trim($_SESSION['__productosImprimir_Busqueda__'][$i]['nombreImagen']);
        $versionImagen = trim($_SESSION['__productosImprimir_Busqueda__'][$i]['versionImagen']);
        
        // SI ES NULL NO TIENE UBICACION LA IMAGEM Y NO EXISTE
        if(is_null($numeroUbicacionImagen) && $tieneImagen === 0){
          $imagen = "images/no_imagen_disponible.png";
          $alt = "alt='Imagen no disponible'";
        }else{
          $numeroUbicacionImagen = (int) $numeroUbicacionImagen;

          $alt_editado = ucwords(strtolower( $descripcion_editada . ', ' . $nombreMarca));
          $alt = "alt='" . $alt_editado . "'";
          
          switch($numeroUbicacionImagen){
            case 1: // UBICACION NUEVA
              $imagen = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '.jpg';
              break;
          }
        }

        $title = "title='" . $descripcion_editada . "'";

        $contenedor_productos .= '
        <div class="p-column-product_busqueda">
          <div class="p-column-product_card">';
        
        if(isset($_SESSION['__id__'])){
          /*$contenedor_productos .= '
            <div class="p-column-product_card_contenedor_fav">
              <a class="p-column-product_card_fav">
                <span>
                  <i class="far fa-heart"></i>
                </span>
                <span>
                  <i class="fas fa-heart"></i>
                </span>
              </a>
            </div>';*/
        }

        $contenedor_productos .= '
            <a href="' . $_SESSION['__productosImprimir_Busqueda__'][$i]['codigoProducto'] . '/' . $descripcionURL . '/" ' . $title . '>
              <figure class="p-text-align_center">
                <picture>
                  <!--<source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                  <img src="' . $imagen . "?v=" . $versionImagen . '" ' . $alt . ' width="120">
                </picture>
              </figure>
              <div class="p-column-product_card_content">
                <div class="p-column-product_card_content_info">';

        $lim = 50;
        $tam = (int) strlen($descripcion);
        if((int) $tam > (int) $lim){
          $n_txt =  mb_substr($descripcion, 0, $lim, 'UTF-8');
          $descripcion = $n_txt . '...';
        }

        $contenedor_productos .= '
                  <p class="p-column-product_title_prod">' . $descripcion . '</p>
                  <p class="p-column-product_precio_color">
                    <span>$ ' . number_format($_SESSION['__productosImprimir_Busqueda__'][$i]['precioMXcomp'], 2, '.', ',') . '</span>
                    <span>' . $_SESSION['__productosImprimir_Busqueda__'][$i]['monedaMXcomp'] . '</span>
                  </p>
                </div>';

        if((int) $_SESSION['__productosImprimir_Busqueda__'][$i]['existenciaTotal'] === 0){
          $contenedor_productos .= '
                <div class="p-column-product_card_content_existencias_aviso">
                  <span>
                    <b>Sin existencias</b>
                  </span>
                </div>';
        }

        $contenedor_productos .= '
              </div>
            </a>
          </div>
        </div>';
      }
    
      $contenedor_productos .= '
      </div>
      
      <div style="margin: 2rem 0 0 0;">' . $contenedor_paginacion . '</div>';
    }
  }else{
    $respuesta = "2";
  }
  
  $json = [
    'respuesta' => $respuesta,
    'html1' => $contenedor_resultados,
    'html2' => $contenedor_filtros,
    'html3' => $boton_filtros,
    'html4' => $contenedor_productos,
    'total_productos' => $totalProductos,
    'filtro_rango_precio' => $filtroRangoPrecio
  ];
  echo json_encode($json);
}
?>