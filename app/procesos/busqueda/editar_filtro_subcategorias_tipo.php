<?php
if(isset($_POST['accion']) && $_POST['accion'] === "filtrar"){
  session_start();

  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  
  $idSubcat_tipo = desencriptar(trim($_POST['id']));

  $proceso_correcto = false;
  $mensaje = '';

  // REVISA EL ID DE LA SUBCATEGORÍA DE TIPOS
  if($idSubcat_tipo !== "" && validar_campo_numerico($idSubcat_tipo)){
    $idSubcat_tipo = (int) $idSubcat_tipo;
    $proceso_correcto = true;
  }else{
    $respuesta = "1"; // NO ES NUMÉRICO
    $proceso_correcto = false;
  }

  // PROCESOS PARA EDITAR EL FILTRO DE SUBCATEGORÍA DE TIPOS
  if($proceso_correcto){
    $bandera_ordernar_array = false;
    $bandera_ejecutar_consulta = false;
    $bandera_existe_variable_session = false;
    
    if(isset($_SESSION['__subcat_tipo_seleccionadas'])){
      $IDsSubcat_tipo_array = array_column($_SESSION['__subcat_tipo_seleccionadas'], 'id');
      if(in_array($idSubcat_tipo, $IDsSubcat_tipo_array)){
        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $indice=>$datos){
          if((int) $datos['id'] === $idSubcat_tipo){
            unset($_SESSION['__subcat_tipo_seleccionadas'][$indice]);

            if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
              unset($_SESSION['__subcat_especificas_seleccionadas']);
            }
            
            $bandera_ordernar_array = true;
            break;
          }
        }
      }else{
        $bandera_ejecutar_consulta = true;
        $bandera_existe_variable_session = true;
      }
    }else{
      $bandera_ejecutar_consulta = true;
    }
    
    $bandera_existen_filtros = false;
    
    if($bandera_ejecutar_consulta){
      $idsSubcat_tipos_array = array_column($_SESSION['__subcat_tipos_Busqueda__'], 'id');
      
      if(in_array($idSubcat_tipo, $idsSubcat_tipos_array)){
        foreach($_SESSION['__subcat_tipos_Busqueda__'] as $indice=>$datos){
          if((int) $datos['id'] === $idSubcat_tipo){
            if($bandera_existe_variable_session){
              $posicion = count($_SESSION['__subcat_tipo_seleccionadas']);
              $datosSubcatTipo = array(
                'id' => $datos['id'],
                'nombre' => $datos['nombre']
              );

              $_SESSION['__subcat_tipo_seleccionadas'][$posicion] = $datosSubcatTipo;
              
              if(isset($_SESSION['__subcat_especificas_seleccionadas'])){
                unset($_SESSION['__subcat_especificas_seleccionadas']);
              }
            }else{
              $datosSubcatTipo = array(
                'id' => $datos['id'],
                'nombre' => $datos['nombre']
              );

              $_SESSION['__subcat_tipo_seleccionadas'][0] = $datosSubcatTipo;
            }
            break;
          }
        }
        
        $bandera_existen_filtros = true;
        
        unset($_SESSION['__pagina_actual']);
        $respuesta = "3";
      }else{
        $respuesta = "1";
      }
    }
    
    if($bandera_ordernar_array){
      if(count($_SESSION['__subcat_tipo_seleccionadas']) !== 0){
        $posicionNuevoArray = 0;
        $arrayTemporal = [];
        foreach($_SESSION['__subcat_tipo_seleccionadas'] as $datos){
          $T = array(
            'id' => $datos['id'],
            'nombre' => $datos['nombre']
          );
          $arrayTemporal[$posicionNuevoArray] = $T;
          $posicionNuevoArray++;
        }
        $_SESSION['__subcat_tipo_seleccionadas'] = $arrayTemporal;
        $arrayTemporal = null;
        
        $bandera_existen_filtros = true;
      }else{
        unset($_SESSION['__subcat_tipo_seleccionadas']);
        
        ////// ESTABLECIENDO LOS PRODUCTOS VISIBLES EN LA BUSQUEDA
        if(isset($_SESSION['__marca_seleccionada'])){
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y MARCA CON LA SELECCIONADA
              if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'])){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y MARCA CON LA SELECCIONADA
              if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'])){
                $mostrar = true;
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }else{
          // SI EXISTE FILTRO DE RANGO DE PRECIOS
          if(isset($_SESSION['__rango_precios__'])){
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA CON LA SELECCIONADA
              if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){
                
                if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                  $mostrar = true;
                }
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }else{
            // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
            foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
              $mostrar = false;
              
              // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA CON LA SELECCIONADA
              if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){
                $mostrar = true;
              }
              
              if($mostrar){
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
              }else{
                $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
              }
            }
          }
        }
      }
      
      unset($_SESSION['__pagina_actual']);
      $respuesta = "3";
    }
    
    if($bandera_existen_filtros){
      ////// ESTABLECIENDO LOS PRODUCTOS VISIBLES EN LA BUSQUEDA
      if(isset($_SESSION['__marca_seleccionada'])){
        // SI EXISTE FILTRO DE RANGO DE PRECIOS
        if(isset($_SESSION['__rango_precios__'])){
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;

            // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y MARCA CON LA SELECCIONADA
            if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'])){

              // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
              foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO DE LA VARIABLE SESSION CON LA DEL PRODUCTO
                if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                  
                  if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                    $mostrar = true;
                    break;
                  }
                }
              }
            }

            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }else{
          // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;

            // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA Y MARCA CON LA SELECCIONADA
            if( ((int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id']) && ((int) $datos['marcaID'] === (int) $_SESSION['__marca_seleccionada'][0]['id'])){

              // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
              foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO DE LA VARIABLE SESSION CON LA DEL PRODUCTO
                if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                  $mostrar = true;
                  break;
                }
              }
            }

            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }
      }else{
        // SI EXISTE FILTRO DE RANGO DE PRECIOS
        if(isset($_SESSION['__rango_precios__'])){
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;

            // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA CON LA SELECCIONADA
            if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

              // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
              foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO DE LA VARIABLE SESSION CON LA DEL PRODUCTO
                if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                  
                  if( ((float) $datos['precioMXcomp'] >= (float) $_SESSION['__rango_precios__'][0]['precio_inicial']) && ((float) $datos['precioMXcomp'] <= (float) $_SESSION['__rango_precios__'][0]['precio_final']) ){
                    $mostrar = true;
                    break;
                  }
                }
              }
            }

            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }else{
          // SI NO EXISTE FILTRO DE RANGO DE PRECIOS
          foreach($_SESSION['__productosBusqueda__'] as $indice=>$datos){
            $mostrar = false;

            // SE REVISA SI COINCIDEN LAS IDS DE CATEGORIA CON LA SELECCIONADA
            if( (int) $datos['categoriaID'] === (int) $_SESSION['__categoria_seleccionada'][0]['id'] ){

              // SE REALIZA UN CICLO A LAS SUBCATEGORIAS DE TIPO
              foreach($_SESSION['__subcat_tipo_seleccionadas'] as $ind=>$subcat_tipo_datos){
                // SE REVISA SI COINCIDEN LAS IDS DE LAS SUBCATEGORIAS DE TIPO DE LA VARIABLE SESSION CON LA DEL PRODUCTO
                if( (int) $datos['subcat_tipoID'] === (int) $subcat_tipo_datos['id'] ){
                  $mostrar = true;
                  break;
                }
              }
            }

            if($mostrar){
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '1';
            }else{
              $_SESSION['__productosBusqueda__'][$indice]['mostrarBusqueda'] = '0';
            }
          }
        }
      }
    }
    
    if($respuesta === "3" && count($_SESSION['__productosBusqueda__']) !== 0){
      unset($_SESSION['__productosImprimir_Busqueda__']);
      
      $contador = 0;
      foreach($_SESSION['__productosBusqueda__'] as $datos_pd){
        if($datos_pd['mostrarBusqueda'] === "1"){
          $_SESSION['__productosImprimir_Busqueda__'][$contador] = [
            'codigoProducto' => $datos_pd['codigoProducto'],
            'skuProveedor' => $datos_pd['skuProveedor'],
            'descripcion' => $datos_pd['descripcion'],
            'descripcionURL' => $datos_pd['descripcionURL'],
            'categoriaID' => $datos_pd['categoriaID'],
            'marcaID' => $datos_pd['marcaID'],
            'nombreMarca' => $datos_pd['nombreMarca'],
            'subcat_tipoID' => $datos_pd['subcat_tipoID'],
            'subcat_especificaID' => $datos_pd['subcat_especificaID'],
            'precioMXcomp' => $datos_pd['precioMXcomp'],
            'monedaMXcomp' => $datos_pd['monedaMXcomp'],
            'existenciaTotal' => $datos_pd['existenciaTotal'],
            'tieneImagen' => $datos_pd['tieneImagen'],
            'numeroUbicacionImagen' => $datos_pd['numeroUbicacionImagen'],
            'nombreImagen' => $datos_pd['nombreImagen'],
            'versionImagen' => $datos_pd['versionImagen'],
            'activo' => $datos_pd['activo'],
            'mostrarBusqueda' => $datos_pd['mostrarBusqueda']
          ];
          $contador++;
        }
      }
      
      if(isset($_SESSION['__activacion_precio_menor_mayor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_ASC, $_SESSION['__productosImprimir_Busqueda__']);
      }

      if(isset($_SESSION['__activacion_precio_mayor_menor'])){
        foreach ($_SESSION['__productosImprimir_Busqueda__'] as $indice=>$registro) {
          $array_auxiliar[$indice] = $registro['precioMXcomp'];
        }

        array_multisort($array_auxiliar, SORT_DESC, $_SESSION['__productosImprimir_Busqueda__']);
      }
    }
  }

  unset($_POST['accion']);
  unset($_POST['id']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>