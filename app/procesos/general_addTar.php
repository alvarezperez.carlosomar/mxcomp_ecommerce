<?php
if(isset($_POST['accion']) && $_POST['accion'] === "agregar"){
  session_start();
  include '../funciones/validaciones_campos.php';
  include '../funciones/encriptacion.php';
  include '../global/config.php';
  include '../conn.php';
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  include '../Openpay/Openpay.php';

  $openpay = Openpay::getInstance(ID_Openpay, Llave_Privada_Openpay);
  Openpay::setProductionMode(false);
  
  $nomTitular = trim($_POST['nom_titu']);
  $numTarjeta = trim($_POST['num_tarjeta']);
  $tipoTarjeta = trim($_POST['tipo_tarjeta']);
  $mesVencimiento = trim($_POST['mes_vencimiento']);
  $anioVencimiento = trim($_POST['anio_vencimiento']);
  $cvc_tarjeta = trim($_POST['cvc']);
  $id_tipoVialidad = trim($_POST['id_tipo_vialidad']);
  $nom_tipo_vialidad = trim($_POST['nom_tipo_vialidad']);
  $nomVialidad = trim($_POST['nom_vialidad']);
  $noExterior = trim($_POST['no_ext']);
  $noInterior = trim($_POST['no_int']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $id_estado = trim($_POST['id_estado']);
  $nom_estado = trim($_POST['nom_estado']);
  $linea1 = trim($_POST['linea1']);
  $token_id = trim($_POST['token_id']);
  $device_session_id = trim($_POST['DSID']);
  
  $proceso_correcto = false;
  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $mensaje = "";
  
  if(validar_campo_numerico($idUser)){
    $idUser = (int) $idUser;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario, nombreS, apellidoPaterno, apellidoMaterno, correo, idOpenpayCliente FROM __usuarios WHERE id = :idUser AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      
      if((int) $datos_usuario['conteo'] === 1){
        $proceso_correcto = true;
      }else{
        $respuesta = "1"; // MENSAJE DE INFO
        $mensaje = "Se detectó que el usuario no existe, recarga la página o cierra sesión e ingresa de nuevo para hacer el proceso.";
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "1"; // MENSAJE DE INFO
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Hubo un problema al buscar el usuario.";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }
  
  $idOpenpayCliente = "";
  $idOpenpayTarjeta = "";
  $error_cliente = false;
  $error_tarjeta_mensaje = false;
  $error_tarjeta_consulta = false;
  
  if($proceso_correcto){
    $codigoUsuario = (string) trim($datos_usuario['codigoUsuario']);
    $nombreS = (string) trim($datos_usuario['nombreS']);
    $apellidoPaterno = trim($datos_usuario['apellidoPaterno']);
    $apellidoMaterno = trim($datos_usuario['apellidoMaterno']);
    $apellidos = (string) $apellidoPaterno." ".$apellidoMaterno;
    $correo = (string) trim($datos_usuario['correo']);
    $idOpenpayCliente = $datos_usuario['idOpenpayCliente'];
    
    // SI EL USUARIO NO ESTA REGISTRADO COMO CLIENTE EN OPENPAY, SE CREA EL CLIENTE Y LUEGO SE ALMACENA EL ID QUE GENERA OPENPAY
    if(is_null($idOpenpayCliente)){
      try{
        $customerData = array(
          'external_id' => $codigoUsuario,
          'name' => $nombreS,
          'last_name' => $apellidos,
          'email' => $correo,
          'requires_account' => false
        );

        $customer = $openpay->customers->add($customerData);
        $idOpenpayCliente = $customer->id;
        
        $fechaActual = date("Y-m-d H:i:s");

        try{
          $conexion->beginTransaction();
          $sql = "UPDATE __usuarios SET idOpenpayCliente = :idOpenpayCliente, fechaActualizacion = :fechaActualizacion WHERE id = :id AND codigoUsuario = :codigoUsuario";
          $stmt = $conexion->prepare($sql);
          $stmt->bindParam(':idOpenpayCliente', $idOpenpayCliente, PDO::PARAM_STR);
          $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
          $stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();

          $stmt = null;
          $conexion->commit();
          $proceso_correcto = true;
          $error_cliente = false;
        }catch(PDOException $error){
          $respuesta = "1"; // MENSAJE DE INFO
          //$mensaje = "Error: ".$error->getMessage();
          $mensaje = "Hubo un problema al actualizar la informacion del usuario.";
          $proceso_correcto = false;
          $error_cliente = true;
          $conexion->rollBack();
        }
      }catch(OpenpayApiTransactionError $e){
        error_log('ERROR on the transaction: ' . $e->getMessage() . 
              ' [error code: ' . $e->getErrorCode() . 
              ', error category: ' . $e->getCategory() . 
              ', HTTP code: '. $e->getHttpCode() . 
              ', request ID: ' . $e->getRequestId() . ']', 0);

        $proceso_correcto = false;
        $codigo_error = (int) $e->getErrorCode();
        
        //$mensaje_error = ' ['.$e->getDescription().']';
        $mensaje_error = '';
        
        // ERRORES: GENERALES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 1000){
          $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
        }else if($codigo_error === 1001){
          $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
        }else if($codigo_error === 1002){
          $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
        }else if($codigo_error === 1003){
          $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
        }else if($codigo_error === 1004){
          $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
        }else if($codigo_error === 1005){
          $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
        }else if($codigo_error === 1006){
          $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
        }else if($codigo_error === 1007){
          $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
        }else if($codigo_error === 1008){
          $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
        }else if($codigo_error === 1009){
          $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
        }else if($codigo_error === 1010){
          $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
        }else if($codigo_error === 1011){
          $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
        }else if($codigo_error === 1012){
          $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
        }else if($codigo_error === 1013){
          $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
        }else if($codigo_error === 1014){
          $mensaje = "La cuenta esta inactiva.".$mensaje_error;
        }else if($codigo_error === 1015){
          $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
        }else if($codigo_error === 1016){
          $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
        }else if($codigo_error === 1017){
          $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
        }else if($codigo_error === 1018){
          $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
        }else if($codigo_error === 1020){
          $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
        }
        
        // ERRORES: ALMACENAMIENTO
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 2001){
          $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2002){
          $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2003){
          $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
        }else if($codigo_error === 2004){
          $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
        }else if($codigo_error === 2005){
          $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
        }else if($codigo_error === 2006){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
        }else if($codigo_error === 2007){
          $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
        }else if($codigo_error === 2008){
          $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
        }else if($codigo_error === 2009){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
        }else if($codigo_error === 2010){
          $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
        }else if($codigo_error === 2011){
          $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
        }
        
        // ERRORES: TARJETAS
        $respuesta = "1"; // MENSAJE DE INFO
        
        if($codigo_error === 3001){
          $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3002){
          $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3003){
          $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 3004){
          $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3005){
          $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
        }else if($codigo_error === 3006){
          $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3007){
          $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3008){
          $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3009){
          $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3010){
          $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3011){
          $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
        }else if($codigo_error === 3012){
          $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
        }
        
        // ERRORES: CUENTAS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 4001){
          $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 4002){
          $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
        }
        
        // ERRORES: ORDENES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 5001){
          $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
        }
        
        // ERRORES: WEBHOOKS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 6001){
          $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
        }else if($codigo_error === 6002){
          $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
        }else if($codigo_error === 6003){
          $mensaje = "El servicio respondio con errores.".$mensaje_error;
        }
      }catch(OpenpayApiRequestError $e){
        error_log('ERROR on the request: ' . $e->getMessage(), 0);

        $proceso_correcto = false;
        $codigo_error = (int) $e->getErrorCode();
        
        //$mensaje_error = ' ['.$e->getDescription().']';
        $mensaje_error = '';
        
        // ERRORES: GENERALES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 1000){
          $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
        }else if($codigo_error === 1001){
          $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
        }else if($codigo_error === 1002){
          $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
        }else if($codigo_error === 1003){
          $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
        }else if($codigo_error === 1004){
          $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
        }else if($codigo_error === 1005){
          $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
        }else if($codigo_error === 1006){
          $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
        }else if($codigo_error === 1007){
          $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
        }else if($codigo_error === 1008){
          $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
        }else if($codigo_error === 1009){
          $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
        }else if($codigo_error === 1010){
          $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
        }else if($codigo_error === 1011){
          $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
        }else if($codigo_error === 1012){
          $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
        }else if($codigo_error === 1013){
          $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
        }else if($codigo_error === 1014){
          $mensaje = "La cuenta esta inactiva.".$mensaje_error;
        }else if($codigo_error === 1015){
          $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
        }else if($codigo_error === 1016){
          $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
        }else if($codigo_error === 1017){
          $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
        }else if($codigo_error === 1018){
          $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
        }else if($codigo_error === 1020){
          $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
        }
        
        // ERRORES: ALMACENAMIENTO
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 2001){
          $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2002){
          $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2003){
          $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
        }else if($codigo_error === 2004){
          $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
        }else if($codigo_error === 2005){
          $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
        }else if($codigo_error === 2006){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
        }else if($codigo_error === 2007){
          $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
        }else if($codigo_error === 2008){
          $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
        }else if($codigo_error === 2009){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
        }else if($codigo_error === 2010){
          $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
        }else if($codigo_error === 2011){
          $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
        }
        
        // ERRORES: TARJETAS
        $respuesta = "1"; // MENSAJE DE INFO
        
        if($codigo_error === 3001){
          $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3002){
          $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3003){
          $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 3004){
          $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3005){
          $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
        }else if($codigo_error === 3006){
          $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3007){
          $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3008){
          $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3009){
          $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3010){
          $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3011){
          $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
        }else if($codigo_error === 3012){
          $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
        }
        
        // ERRORES: CUENTAS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 4001){
          $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 4002){
          $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
        }
        
        // ERRORES: ORDENES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 5001){
          $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
        }
        
        // ERRORES: WEBHOOKS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 6001){
          $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
        }else if($codigo_error === 6002){
          $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
        }else if($codigo_error === 6003){
          $mensaje = "El servicio respondio con errores.".$mensaje_error;
        }
      }catch(OpenpayApiConnectionError $e){
        error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR while connecting to the API: '.$e->getMessage();
        $proceso_correcto = false;
      }catch(OpenpayApiAuthError $e){
        error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR on the authentication: '.$e->getMessage();
        $proceso_correcto = false;
      }catch(OpenpayApiError $e){
        error_log('ERROR on the API: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR on the API: '.$e->getMessage();
        $proceso_correcto = false;
      }catch(Exception $e){
        error_log('Error on the script: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'Error on the script: '.$e->getMessage();
        $proceso_correcto = false;
      }
    }
    
    // AHORA SE CREA LA TARJETA EN OPENPAY
    if($proceso_correcto){
      try{
        $cardDataRequest = array(
          'holder_name' => $nomTitular,
          'card_number' => $numTarjeta,
          'cvv2' => $cvc_tarjeta,
          'expiration_month' => $mesVencimiento,
          'expiration_year' => $anioVencimiento,
          'device_session_id' => $device_session_id,
          'address' => array(
            'line1' => $linea1,
            'line2' => '',
            'line3' => $colonia,
            'postal_code' => $codigoPostal,
            'state' => $nom_estado,
            'city' => $ciudadMunicipio,
            'country_code' => 'MX'
          )
        );

        $customer = $openpay->customers->get($idOpenpayCliente);
        $card = $customer->cards->add($cardDataRequest);

        $idOpenpayTarjeta = $card->id;

        if($tipoTarjeta === "American Express"){
          $ultimosDigitos_numTar = substr($numTarjeta, -5);
        }else{
          $ultimosDigitos_numTar = substr($numTarjeta, -4);
        }
        
        if($tipoTarjeta === ""){
          $tipoTarjeta = ucfirst($card->brand);
        }

        try{
          $conexion->beginTransaction();
          $fechaCreacion = date("Y-m-d H:i:s");

          $sql = "INSERT INTO __tarjetas_u(userID, codigoUsuario, nomTitu, tipoTar, ultimosDigitos_numTar, mesExpiracion, anioExpiracion, id_tipoVialidad, tipoVialidad, nomVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, id_estado, estado, idOpenpayCliente, idOpenpayTarjeta, tokenID_openpay, fechaCreacion) VALUES(:userID, :codigoUsuario, :nomTitular, :tipoTarjeta, :ultimosDigitos_numTar, :mesExpiracion, :anioExpiracion, :id_tipoVialidad, :tipoVialidad, :nomVialidad, :noExterior, :noInterior, :codigoPostal, :colonia, :ciudadMunicipio, :id_estado, :estado, :idOpenpayCliente, :idOpenpayTarjeta, :tokenID_openpay, :fechaCreacion)";
          $stmt = $conexion->prepare($sql);
          $stmt->bindParam(':userID', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':nomTitular', $nomTitular, PDO::PARAM_STR);
          $stmt->bindParam(':tipoTarjeta', $tipoTarjeta, PDO::PARAM_STR);
          $stmt->bindParam(':ultimosDigitos_numTar', $ultimosDigitos_numTar, PDO::PARAM_STR);
          $stmt->bindParam(':mesExpiracion', $mesVencimiento, PDO::PARAM_STR);
          $stmt->bindParam(':anioExpiracion', $anioVencimiento, PDO::PARAM_STR);
          $stmt->bindParam(':id_tipoVialidad', $id_tipoVialidad, PDO::PARAM_INT);
          $stmt->bindParam(':tipoVialidad', $nom_tipo_vialidad, PDO::PARAM_STR);
          $stmt->bindParam(':nomVialidad', $nomVialidad, PDO::PARAM_STR);
          $stmt->bindParam(':noExterior', $noExterior, PDO::PARAM_STR);
          $stmt->bindParam(':noInterior', $noInterior, PDO::PARAM_STR);
          $stmt->bindParam(':codigoPostal', $codigoPostal, PDO::PARAM_STR);
          $stmt->bindParam(':colonia', $colonia, PDO::PARAM_STR);
          $stmt->bindParam(':ciudadMunicipio', $ciudadMunicipio, PDO::PARAM_STR);
          $stmt->bindParam(':id_estado', $id_estado, PDO::PARAM_INT);
          $stmt->bindParam(':estado', $nom_estado, PDO::PARAM_STR);
          $stmt->bindParam(':idOpenpayCliente', $idOpenpayCliente, PDO::PARAM_STR);
          $stmt->bindParam(':idOpenpayTarjeta', $idOpenpayTarjeta, PDO::PARAM_STR);
          $stmt->bindParam(':tokenID_openpay', $token_id, PDO::PARAM_STR);
          $stmt->bindParam(':fechaCreacion', $fechaCreacion, PDO::PARAM_STR);
          $stmt->execute();

          $conexion->commit();

          $stmt = null;
          $respuesta = "3";
          $error_tarjeta_consulta = false;
        }catch(PDOException $error){
          $respuesta = "1"; // MENSAJE DE INFO
          //$mensaje = "Error: ".$error->getMessage();
          $mensaje = "Hubo un problema al agregar los datos de la tarjeta.";
          $error_tarjeta_consulta = true;
          $conexion->rollBack();
        }
        
        $error_cliente = false;
      }catch(OpenpayApiTransactionError $e){
        error_log('ERROR on the transaction: ' . $e->getMessage() . 
              ' [error code: ' . $e->getErrorCode() . 
              ', error category: ' . $e->getCategory() . 
              ', HTTP code: '. $e->getHttpCode() . 
              ', request ID: ' . $e->getRequestId() . ']', 0);

        $error_tarjeta_mensaje = true;
        $codigo_error = (int) $e->getErrorCode();

        //$mensaje_error = ' ['.$e->getDescription().']';
        $mensaje_error = '';
        
        // ERRORES: GENERALES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 1000){
          $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
        }else if($codigo_error === 1001){
          $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
        }else if($codigo_error === 1002){
          $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
        }else if($codigo_error === 1003){
          $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
        }else if($codigo_error === 1004){
          $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
        }else if($codigo_error === 1005){
          $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
        }else if($codigo_error === 1006){
          $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
        }else if($codigo_error === 1007){
          $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
        }else if($codigo_error === 1008){
          $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
        }else if($codigo_error === 1009){
          $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
        }else if($codigo_error === 1010){
          $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
        }else if($codigo_error === 1011){
          $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
        }else if($codigo_error === 1012){
          $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
        }else if($codigo_error === 1013){
          $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
        }else if($codigo_error === 1014){
          $mensaje = "La cuenta esta inactiva.".$mensaje_error;
        }else if($codigo_error === 1015){
          $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
        }else if($codigo_error === 1016){
          $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
        }else if($codigo_error === 1017){
          $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
        }else if($codigo_error === 1018){
          $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
        }else if($codigo_error === 1020){
          $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
        }
        
        // ERRORES: ALMACENAMIENTO
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 2001){
          $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2002){
          $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2003){
          $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
        }else if($codigo_error === 2004){
          $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
        }else if($codigo_error === 2005){
          $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
        }else if($codigo_error === 2006){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
        }else if($codigo_error === 2007){
          $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
        }else if($codigo_error === 2008){
          $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
        }else if($codigo_error === 2009){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
        }else if($codigo_error === 2010){
          $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
        }else if($codigo_error === 2011){
          $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
        }
        
        // ERRORES: TARJETAS
        $respuesta = "1"; // MENSAJE DE INFO
        
        if($codigo_error === 3001){
          $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3002){
          $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3003){
          $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 3004){
          $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3005){
          $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
        }else if($codigo_error === 3006){
          $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3007){
          $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3008){
          $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3009){
          $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3010){
          $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3011){
          $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
        }else if($codigo_error === 3012){
          $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
        }
        
        // ERRORES: CUENTAS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 4001){
          $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 4002){
          $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
        }
        
        // ERRORES: ORDENES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 5001){
          $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
        }
        
        // ERRORES: WEBHOOKS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 6001){
          $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
        }else if($codigo_error === 6002){
          $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
        }else if($codigo_error === 6003){
          $mensaje = "El servicio respondio con errores.".$mensaje_error;
        }
      }catch(OpenpayApiRequestError $e){
        error_log('ERROR on the request: ' . $e->getMessage(), 0);

        $error_tarjeta_mensaje = true;
        $codigo_error = (int) $e->getErrorCode();
        
        //$mensaje_error = ' ['.$e->getDescription().']';
        $mensaje_error = '';
        
        // ERRORES: GENERALES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 1000){
          $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
        }else if($codigo_error === 1001){
          $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
        }else if($codigo_error === 1002){
          $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
        }else if($codigo_error === 1003){
          $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
        }else if($codigo_error === 1004){
          $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
        }else if($codigo_error === 1005){
          $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
        }else if($codigo_error === 1006){
          $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
        }else if($codigo_error === 1007){
          $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
        }else if($codigo_error === 1008){
          $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
        }else if($codigo_error === 1009){
          $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
        }else if($codigo_error === 1010){
          $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
        }else if($codigo_error === 1011){
          $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
        }else if($codigo_error === 1012){
          $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
        }else if($codigo_error === 1013){
          $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
        }else if($codigo_error === 1014){
          $mensaje = "La cuenta esta inactiva.".$mensaje_error;
        }else if($codigo_error === 1015){
          $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
        }else if($codigo_error === 1016){
          $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
        }else if($codigo_error === 1017){
          $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
        }else if($codigo_error === 1018){
          $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
        }else if($codigo_error === 1020){
          $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
        }
        
        // ERRORES: ALMACENAMIENTO
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 2001){
          $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2002){
          $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
        }else if($codigo_error === 2003){
          $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
        }else if($codigo_error === 2004){
          $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
        }else if($codigo_error === 2005){
          $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
        }else if($codigo_error === 2006){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
        }else if($codigo_error === 2007){
          $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
        }else if($codigo_error === 2008){
          $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
        }else if($codigo_error === 2009){
          $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
        }else if($codigo_error === 2010){
          $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
        }else if($codigo_error === 2011){
          $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
        }
        
        // ERRORES: TARJETAS
        $respuesta = "1"; // MENSAJE DE INFO
        
        if($codigo_error === 3001){
          $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3002){
          $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3003){
          $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 3004){
          $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3005){
          $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
        }else if($codigo_error === 3006){
          $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3007){
          $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3008){
          $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3009){
          $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3010){
          $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
        }else if($codigo_error === 3011){
          $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
        }else if($codigo_error === 3012){
          $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
        }
        
        // ERRORES: CUENTAS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 4001){
          $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
        }else if($codigo_error === 4002){
          $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
        }
        
        // ERRORES: ORDENES
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 5001){
          $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
        }
        
        // ERRORES: WEBHOOKS
        $respuesta = "2"; // MENSAJE DE ERROR
        
        if($codigo_error === 6001){
          $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
        }else if($codigo_error === 6002){
          $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
        }else if($codigo_error === 6003){
          $mensaje = "El servicio respondio con errores.".$mensaje_error;
        }
      }catch(OpenpayApiConnectionError $e){
        error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR while connecting to the API: '.$e->getMessage();
        $error_tarjeta_mensaje = true;
      }catch(OpenpayApiAuthError $e){
        error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR on the authentication: '.$e->getMessage();
        $error_tarjeta_mensaje = true;
      }catch(OpenpayApiError $e){
        error_log('ERROR on the API: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'ERROR on the API: '.$e->getMessage();
        $error_tarjeta_mensaje = true;
      }catch(Exception $e){
        error_log('Error on the script: ' . $e->getMessage(), 0);

        $respuesta = "0";
        $mensaje = 'Error on the script: '.$e->getMessage();
        $error_tarjeta_mensaje = true;
      }
    }
  }
  
  // SE ELIMINA EL CLIENTE DE OPENPAY SI HAY UN ERROR EN LA CONSULTA
  if($error_tarjeta_consulta){
    try{
      $customer = $openpay->customers->get($idOpenpayCliente);
      $card = $customer->cards->get($idOpenpayTarjeta);
      $card->delete();

      $error_tarjeta_mensaje = true;
    }catch(OpenpayApiTransactionError $e){
      error_log('ERROR on the transaction: ' . $e->getMessage() . 
            ' [error code: ' . $e->getErrorCode() . 
            ', error category: ' . $e->getCategory() . 
            ', HTTP code: '. $e->getHttpCode() . 
            ', request ID: ' . $e->getRequestId() . ']', 0);

      $error_tarjeta_mensaje = false;
      $codigo_error = (int) $e->getErrorCode();

      //$mensaje_error = ' ['.$e->getDescription().']';
      $mensaje_error = '';

      // ERRORES: GENERALES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 1000){
        $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
      }else if($codigo_error === 1001){
        $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
      }else if($codigo_error === 1002){
        $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
      }else if($codigo_error === 1003){
        $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
      }else if($codigo_error === 1004){
        $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
      }else if($codigo_error === 1005){
        $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
      }else if($codigo_error === 1006){
        $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
      }else if($codigo_error === 1007){
        $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
      }else if($codigo_error === 1008){
        $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
      }else if($codigo_error === 1009){
        $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
      }else if($codigo_error === 1010){
        $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
      }else if($codigo_error === 1011){
        $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
      }else if($codigo_error === 1012){
        $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
      }else if($codigo_error === 1013){
        $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
      }else if($codigo_error === 1014){
        $mensaje = "La cuenta esta inactiva.".$mensaje_error;
      }else if($codigo_error === 1015){
        $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
      }else if($codigo_error === 1016){
        $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
      }else if($codigo_error === 1017){
        $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
      }else if($codigo_error === 1018){
        $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
      }else if($codigo_error === 1020){
        $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
      }

      // ERRORES: ALMACENAMIENTO
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 2001){
        $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2002){
        $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2003){
        $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
      }else if($codigo_error === 2004){
        $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
      }else if($codigo_error === 2005){
        $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
      }else if($codigo_error === 2006){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
      }else if($codigo_error === 2007){
        $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
      }else if($codigo_error === 2008){
        $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
      }else if($codigo_error === 2009){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
      }else if($codigo_error === 2010){
        $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
      }else if($codigo_error === 2011){
        $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
      }

      // ERRORES: TARJETAS
      $respuesta = "1"; // MENSAJE DE INFO

      if($codigo_error === 3001){
        $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3002){
        $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3003){
        $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 3004){
        $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3005){
        $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
      }else if($codigo_error === 3006){
        $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3007){
        $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3008){
        $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3009){
        $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3010){
        $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3011){
        $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
      }else if($codigo_error === 3012){
        $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
      }

      // ERRORES: CUENTAS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 4001){
        $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 4002){
        $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
      }

      // ERRORES: ORDENES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 5001){
        $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
      }

      // ERRORES: WEBHOOKS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 6001){
        $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
      }else if($codigo_error === 6002){
        $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
      }else if($codigo_error === 6003){
        $mensaje = "El servicio respondio con errores.".$mensaje_error;
      }
    }catch(OpenpayApiRequestError $e){
      error_log('ERROR on the request: ' . $e->getMessage(), 0);

      $error_tarjeta_mensaje = false;
      $codigo_error = (int) $e->getErrorCode();

      //$mensaje_error = ' ['.$e->getDescription().']';
      $mensaje_error = '';

      // ERRORES: GENERALES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 1000){
        $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
      }else if($codigo_error === 1001){
        $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
      }else if($codigo_error === 1002){
        $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
      }else if($codigo_error === 1003){
        $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
      }else if($codigo_error === 1004){
        $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
      }else if($codigo_error === 1005){
        $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
      }else if($codigo_error === 1006){
        $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
      }else if($codigo_error === 1007){
        $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
      }else if($codigo_error === 1008){
        $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
      }else if($codigo_error === 1009){
        $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
      }else if($codigo_error === 1010){
        $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
      }else if($codigo_error === 1011){
        $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
      }else if($codigo_error === 1012){
        $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
      }else if($codigo_error === 1013){
        $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
      }else if($codigo_error === 1014){
        $mensaje = "La cuenta esta inactiva.".$mensaje_error;
      }else if($codigo_error === 1015){
        $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
      }else if($codigo_error === 1016){
        $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
      }else if($codigo_error === 1017){
        $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
      }else if($codigo_error === 1018){
        $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
      }else if($codigo_error === 1020){
        $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
      }

      // ERRORES: ALMACENAMIENTO
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 2001){
        $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2002){
        $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2003){
        $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
      }else if($codigo_error === 2004){
        $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
      }else if($codigo_error === 2005){
        $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
      }else if($codigo_error === 2006){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
      }else if($codigo_error === 2007){
        $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
      }else if($codigo_error === 2008){
        $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
      }else if($codigo_error === 2009){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
      }else if($codigo_error === 2010){
        $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
      }else if($codigo_error === 2011){
        $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
      }

      // ERRORES: TARJETAS
      $respuesta = "1"; // MENSAJE DE INFO

      if($codigo_error === 3001){
        $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3002){
        $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3003){
        $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 3004){
        $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3005){
        $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
      }else if($codigo_error === 3006){
        $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3007){
        $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3008){
        $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3009){
        $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3010){
        $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3011){
        $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
      }else if($codigo_error === 3012){
        $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
      }

      // ERRORES: CUENTAS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 4001){
        $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 4002){
        $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
      }

      // ERRORES: ORDENES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 5001){
        $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
      }

      // ERRORES: WEBHOOKS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 6001){
        $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
      }else if($codigo_error === 6002){
        $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
      }else if($codigo_error === 6003){
        $mensaje = "El servicio respondio con errores.".$mensaje_error;
      }
    }catch(OpenpayApiConnectionError $e){
      error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR while connecting to the API: '.$e->getMessage();
      $error_tarjeta_mensaje = false;
    }catch(OpenpayApiAuthError $e){
      error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR on the authentication: '.$e->getMessage();
      $error_tarjeta_mensaje = false;
    }catch(OpenpayApiError $e){
      error_log('ERROR on the API: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR on the API: '.$e->getMessage();
      $error_tarjeta_mensaje = false;
    }catch(Exception $e){
      error_log('Error on the script: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'Error on the script: '.$e->getMessage();
      $error_tarjeta_mensaje = false;
    }
  }
  
  // SE ELIMINA EL CLIENTE DE OPENPAY SI HAY UN ERROR EN LA CONSULTA
  if($error_tarjeta_mensaje){
    try{
      $sql = "SELECT COUNT(id) FROM __tarjetas_u WHERE userID = :userID AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':userID', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $tarjetas_existen = (int) $stmt->fetchColumn();
      
      if($tarjetas_existen === 0){
        $error_cliente = true;
      }else{
        $error_cliente = false;
      }

      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "1"; // MENSAJE DE INFO
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Hubo un problema al buscar la información de las tarjetas.";
      $error_cliente = false;
    }
  }
  
  // SE ELIMINA EL CLIENTE DE OPENPAY SI HAY UN ERROR EN LA CONSULTA
  if($error_cliente){
    try{
      $customer = $openpay->customers->get($idOpenpayCliente);
      $customer->delete();
      
      try{
        $conexion->beginTransaction();

        $sql = "UPDATE __usuarios SET idOpenpayCliente = NULL, fechaActualizacion = :fechaActualizacion WHERE id = :id AND codigoUsuario = :codigoUsuario";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
        $stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();

        $stmt = null;
        $conexion->commit();
      }catch(PDOException $error){
        $respuesta = "1"; // MENSAJE DE INFO
        //$mensaje = "Error: ".$error->getMessage();
        $mensaje = "Hubo un problema al actualizar la informacion del usuario.";
        $conexion->rollBack();
      }
    }catch(OpenpayApiTransactionError $e){
      error_log('ERROR on the transaction: ' . $e->getMessage() . 
            ' [error code: ' . $e->getErrorCode() . 
            ', error category: ' . $e->getCategory() . 
            ', HTTP code: '. $e->getHttpCode() . 
            ', request ID: ' . $e->getRequestId() . ']', 0);

      $codigo_error = (int) $e->getErrorCode();
      
      //$mensaje_error = ' ['.$e->getDescription().']';
      $mensaje_error = '';

      // ERRORES: GENERALES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 1000){
        $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
      }else if($codigo_error === 1001){
        $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
      }else if($codigo_error === 1002){
        $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
      }else if($codigo_error === 1003){
        $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
      }else if($codigo_error === 1004){
        $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
      }else if($codigo_error === 1005){
        $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
      }else if($codigo_error === 1006){
        $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
      }else if($codigo_error === 1007){
        $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
      }else if($codigo_error === 1008){
        $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
      }else if($codigo_error === 1009){
        $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
      }else if($codigo_error === 1010){
        $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
      }else if($codigo_error === 1011){
        $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
      }else if($codigo_error === 1012){
        $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
      }else if($codigo_error === 1013){
        $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
      }else if($codigo_error === 1014){
        $mensaje = "La cuenta esta inactiva.".$mensaje_error;
      }else if($codigo_error === 1015){
        $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
      }else if($codigo_error === 1016){
        $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
      }else if($codigo_error === 1017){
        $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
      }else if($codigo_error === 1018){
        $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
      }else if($codigo_error === 1020){
        $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
      }

      // ERRORES: ALMACENAMIENTO
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 2001){
        $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2002){
        $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2003){
        $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
      }else if($codigo_error === 2004){
        $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
      }else if($codigo_error === 2005){
        $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
      }else if($codigo_error === 2006){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
      }else if($codigo_error === 2007){
        $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
      }else if($codigo_error === 2008){
        $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
      }else if($codigo_error === 2009){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
      }else if($codigo_error === 2010){
        $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
      }else if($codigo_error === 2011){
        $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
      }

      // ERRORES: TARJETAS
      $respuesta = "1"; // MENSAJE DE INFO

      if($codigo_error === 3001){
        $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3002){
        $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3003){
        $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 3004){
        $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3005){
        $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
      }else if($codigo_error === 3006){
        $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3007){
        $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3008){
        $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3009){
        $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3010){
        $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3011){
        $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
      }else if($codigo_error === 3012){
        $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
      }

      // ERRORES: CUENTAS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 4001){
        $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 4002){
        $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
      }

      // ERRORES: ORDENES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 5001){
        $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
      }

      // ERRORES: WEBHOOKS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 6001){
        $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
      }else if($codigo_error === 6002){
        $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
      }else if($codigo_error === 6003){
        $mensaje = "El servicio respondio con errores.".$mensaje_error;
      }
    }catch(OpenpayApiRequestError $e){
      error_log('ERROR on the request: ' . $e->getMessage(), 0);

      $codigo_error = (int) $e->getErrorCode();
      
      //$mensaje_error = ' ['.$e->getDescription().']';
      $mensaje_error = '';

      // ERRORES: GENERALES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 1000){
        $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
      }else if($codigo_error === 1001){
        $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
      }else if($codigo_error === 1002){
        $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
      }else if($codigo_error === 1003){
        $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
      }else if($codigo_error === 1004){
        $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
      }else if($codigo_error === 1005){
        $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
      }else if($codigo_error === 1006){
        $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
      }else if($codigo_error === 1007){
        $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
      }else if($codigo_error === 1008){
        $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
      }else if($codigo_error === 1009){
        $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
      }else if($codigo_error === 1010){
        $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
      }else if($codigo_error === 1011){
        $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
      }else if($codigo_error === 1012){
        $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
      }else if($codigo_error === 1013){
        $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
      }else if($codigo_error === 1014){
        $mensaje = "La cuenta esta inactiva.".$mensaje_error;
      }else if($codigo_error === 1015){
        $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
      }else if($codigo_error === 1016){
        $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
      }else if($codigo_error === 1017){
        $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
      }else if($codigo_error === 1018){
        $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
      }else if($codigo_error === 1020){
        $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
      }

      // ERRORES: ALMACENAMIENTO
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 2001){
        $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2002){
        $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
      }else if($codigo_error === 2003){
        $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
      }else if($codigo_error === 2004){
        $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
      }else if($codigo_error === 2005){
        $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
      }else if($codigo_error === 2006){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
      }else if($codigo_error === 2007){
        $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
      }else if($codigo_error === 2008){
        $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
      }else if($codigo_error === 2009){
        $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
      }else if($codigo_error === 2010){
        $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
      }else if($codigo_error === 2011){
        $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
      }

      // ERRORES: TARJETAS
      $respuesta = "1"; // MENSAJE DE INFO

      if($codigo_error === 3001){
        $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3002){
        $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3003){
        $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 3004){
        $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3005){
        $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
      }else if($codigo_error === 3006){
        $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3007){
        $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3008){
        $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3009){
        $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3010){
        $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
      }else if($codigo_error === 3011){
        $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
      }else if($codigo_error === 3012){
        $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
      }

      // ERRORES: CUENTAS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 4001){
        $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
      }else if($codigo_error === 4002){
        $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
      }

      // ERRORES: ORDENES
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 5001){
        $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
      }

      // ERRORES: WEBHOOKS
      $respuesta = "2"; // MENSAJE DE ERROR

      if($codigo_error === 6001){
        $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
      }else if($codigo_error === 6002){
        $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
      }else if($codigo_error === 6003){
        $mensaje = "El servicio respondio con errores.".$mensaje_error;
      }
    }catch(OpenpayApiConnectionError $e){
      error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR while connecting to the API: '.$e->getMessage();
    }catch(OpenpayApiAuthError $e){
      error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR on the authentication: '.$e->getMessage();
    }catch(OpenpayApiError $e){
      error_log('ERROR on the API: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'ERROR on the API: '.$e->getMessage();
    }catch(Exception $e){
      error_log('Error on the script: ' . $e->getMessage(), 0);

      $respuesta = "0";
      $mensaje = 'Error on the script: '.$e->getMessage();
    }
  }
  
  unset($_POST['accion']);
  unset($_POST['nom_titu']);
  unset($_POST['num_tarjeta']);
  unset($_POST['tipo_tarjeta']);
  unset($_POST['mes_vencimiento']);
  unset($_POST['anio_vencimiento']);
  unset($_POST['cvc']);
  unset($_POST['id_tipo_vialidad']);
  unset($_POST['nom_tipo_vialidad']);
  unset($_POST['nom_vialidad']);
  unset($_POST['no_ext']);
  unset($_POST['no_int']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['id_estado']);
  unset($_POST['nom_estado']);
  unset($_POST['linea1']);
  unset($_POST['token_id']);
  unset($_POST['DSID']);
  
  $json[] = array(
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  );
  $json_string = json_encode($json);
  echo $json_string;
}
?>