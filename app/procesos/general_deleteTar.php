<?php
if(isset($_POST['accion']) && $_POST['accion'] === "eliminar"){
  session_start();
  include '../funciones/validaciones_password.php';
  include '../funciones/validaciones_campos.php';
  include '../funciones/encriptacion.php';
  include '../global/config.php';
  include '../conn.php';
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  include '../Openpay/Openpay.php';

  $openpay = Openpay::getInstance(ID_Openpay, Llave_Privada_Openpay);
  Openpay::setProductionMode(false);
  
  $idTar = trim($_POST['valor']);
  $password = trim($_POST['pass']);
  
  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $idTar = desencriptar($idTar);
  
  $proceso_correcto = false;
  $mensaje = '';
  
  if($password !== "" && validar_password($password)){
    if($idUser !== "" && validar_campo_numerico($idUser)){
      $idUser = (int) $idUser;
      
      try{
        $sql = "SELECT COUNT(id) AS usuario_existe, pass_word, ssap_revision FROM __usuarios WHERE id = :idUser AND codigoUsuario = :codigoUsuario";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
        $datos_cuenta = $stmt->fetch(PDO::FETCH_ASSOC);

        $cuentaExiste = (int) trim($datos_cuenta['usuario_existe']);

        if($cuentaExiste === 1){
          /////////////// OBTENEMOS LA PASSWORD ENCRIPTADA
          // UNIMOS LAS PARTES DE LAS CONTRASEÑAS DE LA BASE DE DATOS
          $passwordBD_token = (string) trim($datos_cuenta['pass_word']).trim($datos_cuenta['ssap_revision']);
          // QUITAMOS LOS PRIMEROS 4 CARACTERES
          $passwordBD_original = substr($passwordBD_token, 4);
          // ENCRIPTAMOS EL CODIGO DE USUARIO
          $codigoUsuario_encriptado = encriptar($codigoUsuario);
          // ENCRIPTAMOS LA PASSWORD DEL FORMULARIO CON EL CODIGO DE USUARIO ENCRIPTADO
          $passwordForm_encriptada = encriptar_con_clave($password, $codigoUsuario_encriptado);
          
          if($passwordForm_encriptada === $passwordBD_original){
            if($idTar !== "" && validar_campo_numerico($idTar)){
              $idTar = (int) $idTar;
              
              try{
                $sql = "SELECT COUNT(id) AS conteo, idOpenpayCliente, idOpenpayTarjeta FROM __tarjetas_u WHERE id = :idTar AND userID = :idUser AND codigoUsuario = :codigoUsuario";
                $stmt = $conexion->prepare($sql);
                $stmt->bindParam(':idTar', $idTar, PDO::PARAM_INT);
                $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
                $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
                $stmt->execute();
                $datos_tarjeta = $stmt->fetch(PDO::FETCH_ASSOC);
                $tarjeta_existe = (int) $datos_tarjeta['conteo'];

                if($tarjeta_existe === 1){
                  $idOpenpayCliente = (string) $datos_tarjeta['idOpenpayCliente'];
                  $idOpenpayTarjeta = (string) $datos_tarjeta['idOpenpayTarjeta'];
                  
                  try{
                    $customer = $openpay->customers->get($idOpenpayCliente);
                    $card = $customer->cards->get($idOpenpayTarjeta);
                    $card->delete();
                    $proceso_correcto = true;
                  }catch(OpenpayApiTransactionError $e){
                    error_log('ERROR on the transaction: ' . $e->getMessage() . 
                          ' [error code: ' . $e->getErrorCode() . 
                          ', error category: ' . $e->getCategory() . 
                          ', HTTP code: '. $e->getHttpCode() . 
                          ', request ID: ' . $e->getRequestId() . ']', 0);
                    
                    $proceso_correcto = false;
                    $codigo_error = (int) $e->getErrorCode();
                    
                    //$mensaje_error = ' ['.$e->getDescription().']';
                    $mensaje_error = '';
                    
                    // ERRORES: GENERALES
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 1000){
                      $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
                    }else if($codigo_error === 1001){
                      $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
                    }else if($codigo_error === 1002){
                      $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
                    }else if($codigo_error === 1003){
                      $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
                    }else if($codigo_error === 1004){
                      $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
                    }else if($codigo_error === 1005){
                      $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
                    }else if($codigo_error === 1006){
                      $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
                    }else if($codigo_error === 1007){
                      $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
                    }else if($codigo_error === 1008){
                      $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
                    }else if($codigo_error === 1009){
                      $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
                    }else if($codigo_error === 1010){
                      $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
                    }else if($codigo_error === 1011){
                      $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
                    }else if($codigo_error === 1012){
                      $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
                    }else if($codigo_error === 1013){
                      $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
                    }else if($codigo_error === 1014){
                      $mensaje = "La cuenta esta inactiva.".$mensaje_error;
                    }else if($codigo_error === 1015){
                      $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
                    }else if($codigo_error === 1016){
                      $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
                    }else if($codigo_error === 1017){
                      $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
                    }else if($codigo_error === 1018){
                      $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
                    }else if($codigo_error === 1020){
                      $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
                    }

                    // ERRORES: ALMACENAMIENTO
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 2001){
                      $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
                    }else if($codigo_error === 2002){
                      $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
                    }else if($codigo_error === 2003){
                      $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
                    }else if($codigo_error === 2004){
                      $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
                    }else if($codigo_error === 2005){
                      $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
                    }else if($codigo_error === 2006){
                      $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
                    }else if($codigo_error === 2007){
                      $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
                    }else if($codigo_error === 2008){
                      $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
                    }else if($codigo_error === 2009){
                      $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
                    }else if($codigo_error === 2010){
                      $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
                    }else if($codigo_error === 2011){
                      $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
                    }

                    // ERRORES: TARJETAS
                    $respuesta = "5"; // MENSAJE DE INFO

                    if($codigo_error === 3001){
                      $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3002){
                      $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3003){
                      $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
                    }else if($codigo_error === 3004){
                      $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3005){
                      $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
                    }else if($codigo_error === 3006){
                      $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3007){
                      $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3008){
                      $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3009){
                      $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3010){
                      $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3011){
                      $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
                    }else if($codigo_error === 3012){
                      $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
                    }

                    // ERRORES: CUENTAS
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 4001){
                      $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
                    }else if($codigo_error === 4002){
                      $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
                    }

                    // ERRORES: ORDENES
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 5001){
                      $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
                    }

                    // ERRORES: WEBHOOKS
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 6001){
                      $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
                    }else if($codigo_error === 6002){
                      $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
                    }else if($codigo_error === 6003){
                      $mensaje = "El servicio respondio con errores.".$mensaje_error;
                    }
                  }catch(OpenpayApiRequestError $e){
                    error_log('ERROR on the request: ' . $e->getMessage(), 0);

                    $proceso_correcto = false;
                    $codigo_error = (int) $e->getErrorCode();
                    
                    //$mensaje_error = ' ['.$e->getDescription().']';
                    $mensaje_error = '';
                    
                    // ERRORES: GENERALES
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 1000){
                      $mensaje = "Ocurrió un error interno en el servidor de Openpay.".$mensaje_error;
                    }else if($codigo_error === 1001){
                      $mensaje = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.".$mensaje_error;
                    }else if($codigo_error === 1002){
                      $mensaje = "La llamada no esta autenticada o la autenticación es incorrecta.".$mensaje_error;
                    }else if($codigo_error === 1003){
                      $mensaje = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.".$mensaje_error;
                    }else if($codigo_error === 1004){
                      $mensaje = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.".$mensaje_error;
                    }else if($codigo_error === 1005){
                      $mensaje = "Uno de los recursos requeridos no existe.".$mensaje_error;
                    }else if($codigo_error === 1006){
                      $mensaje = "Ya existe una transacción con el mismo ID de orden.".$mensaje_error;
                    }else if($codigo_error === 1007){
                      $mensaje = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta de Openpay no fue aceptada.".$mensaje_error;
                    }else if($codigo_error === 1008){
                      $mensaje = "Una de las cuentas requeridas en la petición se encuentra desactivada.".$mensaje_error;
                    }else if($codigo_error === 1009){
                      $mensaje = "El cuerpo de la petición es demasiado grande.".$mensaje_error;
                    }else if($codigo_error === 1010){
                      $mensaje = "Se esta utilizando la llave pública para hacer una llamada que requiere la llave privada, o bien, se esta usando la llave privada desde JavaScript.".$mensaje_error;
                    }else if($codigo_error === 1011){
                      $mensaje = "Se solicita un recurso que esta marcado como eliminado.".$mensaje_error;
                    }else if($codigo_error === 1012){
                      $mensaje = "El monto transacción esta fuera de los limites permitidos.".$mensaje_error;
                    }else if($codigo_error === 1013){
                      $mensaje = "La operación no esta permitida para el recurso.".$mensaje_error;
                    }else if($codigo_error === 1014){
                      $mensaje = "La cuenta esta inactiva.".$mensaje_error;
                    }else if($codigo_error === 1015){
                      $mensaje = "No se ha obtenido respuesta de la solicitud realizada al servicio.".$mensaje_error;
                    }else if($codigo_error === 1016){
                      $mensaje = "El mail del comercio ya ha sido procesada.".$mensaje_error;
                    }else if($codigo_error === 1017){
                      $mensaje = "El gateway no se encuentra disponible en ese momento.".$mensaje_error;
                    }else if($codigo_error === 1018){
                      $mensaje = "El número de intentos de cargo es mayor al permitido.".$mensaje_error;
                    }else if($codigo_error === 1020){
                      $mensaje = "El número de dígitos decimales es inválido para esta moneda.".$mensaje_error;
                    }

                    // ERRORES: ALMACENAMIENTO
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 2001){
                      $mensaje = "La cuenta de banco con esta CLABE ya se encuentra registrada en el cliente.".$mensaje_error;
                    }else if($codigo_error === 2002){
                      $mensaje = "La tarjeta con este número ya se encuentra registrada en el cliente.".$mensaje_error;
                    }else if($codigo_error === 2003){
                      $mensaje = "El cliente con este identificador externo (External ID) ya existe.".$mensaje_error;
                    }else if($codigo_error === 2004){
                      $mensaje = "El dígito verificador del número de tarjeta es inválido de acuerdo al algoritmo Luhn.".$mensaje_error;
                    }else if($codigo_error === 2005){
                      $mensaje = "La fecha de expiración de la tarjeta es anterior a la fecha actual.".$mensaje_error;
                    }else if($codigo_error === 2006){
                      $mensaje = "El código de seguridad de la tarjeta (CVV2) no fue proporcionado.".$mensaje_error;
                    }else if($codigo_error === 2007){
                      $mensaje = "El número de tarjeta es de prueba, solamente puede usarse en Sandbox.".$mensaje_error;
                    }else if($codigo_error === 2008){
                      $mensaje = "La tarjeta consultada no es valida para puntos.".$mensaje_error;
                    }else if($codigo_error === 2009){
                      $mensaje = "El código de seguridad de la tarjeta (CVV2) no es válido.".$mensaje_error;
                    }else if($codigo_error === 2010){
                      $mensaje = "Autenticación 3D Secure fallida.".$mensaje_error;
                    }else if($codigo_error === 2011){
                      $mensaje = "Tipo de tarjeta no soportada.".$mensaje_error;
                    }

                    // ERRORES: TARJETAS
                    $respuesta = "5"; // MENSAJE DE INFO

                    if($codigo_error === 3001){
                      $mensaje = "La tarjeta fue rechazada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3002){
                      $mensaje = "La tarjeta ha expirado. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3003){
                      $mensaje = "La tarjeta no tiene fondos suficientes.".$mensaje_error;
                    }else if($codigo_error === 3004){
                      $mensaje = "La tarjeta ha sido identificada como una tarjeta robada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3005){
                      $mensaje = "La tarjeta ha sido rechazada por el sistema antifraudes.".$mensaje_error;
                    }else if($codigo_error === 3006){
                      $mensaje = "La operación no esta permitida para este cliente o esta transacción. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3007){
                      $mensaje = "Deprecado. La tarjeta fue declinada. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3008){
                      $mensaje = "La tarjeta no es soportada en transacciones en línea. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3009){
                      $mensaje = "La tarjeta fue reportada como perdida. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3010){
                      $mensaje = "El banco ha restringido la tarjeta. Ponte en contacto con tu banco.".$mensaje_error;
                    }else if($codigo_error === 3011){
                      $mensaje = "El banco ha solicitado que la tarjeta sea retenida. Contacta al banco.".$mensaje_error;
                    }else if($codigo_error === 3012){
                      $mensaje = "Se requiere solicitar al banco autorización para realizar este pago.".$mensaje_error;
                    }

                    // ERRORES: CUENTAS
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 4001){
                      $mensaje = "La cuenta de Openpay no tiene fondos suficientes.".$mensaje_error;
                    }else if($codigo_error === 4002){
                      $mensaje = "La operación no puede ser completada hasta que sean pagadas las comisiones pendientes.".$mensaje_error;
                    }

                    // ERRORES: ORDENES
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 5001){
                      $mensaje = "La orden con este identificador externo (external_order_id) ya existe.".$mensaje_error;
                    }

                    // ERRORES: WEBHOOKS
                    $respuesta = "6"; // MENSAJE DE ERROR

                    if($codigo_error === 6001){
                      $mensaje = "El webhook ya ha sido procesado.".$mensaje_error;
                    }else if($codigo_error === 6002){
                      $mensaje = "No se ha podido conectar con el servicio de webhook.".$mensaje_error;
                    }else if($codigo_error === 6003){
                      $mensaje = "El servicio respondio con errores.".$mensaje_error;
                    }
                  }catch(OpenpayApiConnectionError $e){
                    error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

                    $respuesta = "0";
                    $mensaje = 'ERROR while connecting to the API: '.$e->getMessage();
                    $proceso_correcto = false;
                  }catch(OpenpayApiAuthError $e){
                    error_log('ERROR on the authentication: ' . $e->getMessage(), 0);

                    $respuesta = "0";
                    $mensaje = 'ERROR on the authentication: '.$e->getMessage();
                    $proceso_correcto = false;
                  }catch(OpenpayApiError $e){
                    error_log('ERROR on the API: ' . $e->getMessage(), 0);

                    $respuesta = "0";
                    $mensaje = 'ERROR on the API: '.$e->getMessage();
                    $proceso_correcto = false;
                  }catch(Exception $e){
                    error_log('Error on the script: ' . $e->getMessage(), 0);
                    
                    $respuesta = "0";
                    $mensaje = 'Error on the script: '.$e->getMessage();
                    $proceso_correcto = false;
                  }
                  
                  if($proceso_correcto){
                    $sql = "DELETE FROM __tarjetas_u WHERE id = :id AND userID = :userID AND codigoUsuario = :codigoUsuario AND idOpenpayCliente = :idOpenpayCliente AND idOpenpayTarjeta = :idOpenpayTarjeta";
                    $stmt = $conexion->prepare($sql);
                    $stmt->bindParam(':id', $idTar, PDO::PARAM_INT);
                    $stmt->bindParam(':userID', $idUser, PDO::PARAM_INT);
                    $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
                    $stmt->bindParam(':idOpenpayCliente', $idOpenpayCliente, PDO::PARAM_STR);
                    $stmt->bindParam(':idOpenpayTarjeta', $idOpenpayTarjeta, PDO::PARAM_STR);
                    $stmt->execute();
                    
                    $respuesta = "7";
                  }
                }else{
                  $respuesta = "4"; //La tarjeta no existe
                }
              }catch(PDOException $error){
                $respuesta = "5"; // MENSAJE DE INFO
                //$mensaje = "Error: ".$error->getMessage();
                $mensaje = 'Hubo un problema al buscar la tarjeta.';
              }
            }else if($idTar === ""){
              $respuesta = "0"; // La id de la tarjeta esta vacia
              $mensaje = 'La id de la tarjeta esta vacia';
            }else{
              $respuesta = "0"; // La id de la tarjeta no es numerica
              $mensaje = 'La id de la tarjeta no es numerica';
            }
          }else{
            $respuesta = "3"; // La contraseña es incorrecta
          }
        }else{
          $respuesta = "0"; // El usuario no existe
          $mensaje = 'El usuario no existe';
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "5"; // MENSAJE DE INFO
        //$mensaje = "Error: ".$error->getMessage();
        $mensaje = 'Hubo un problema al buscar el usuario';
      }
    }else if($idUser === ""){
      $respuesta = "0"; // EL Id de usuario esta vacio
      $mensaje = 'EL Id de usuario esta vacio';
    }else{
      $respuesta = "0"; // El id de usuario no es numerico
      $mensaje = 'El Id de usuario no es numerico';
    }
  }else if($password === ""){
    $respuesta = "1"; // Este campo se encuentra vacio
  }else{
    $respuesta = "2"; // La contraseña contiene un simbolo no permitido.
  }
  
  $json[] = array(
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  );
  $json_string = json_encode($json);
  echo $json_string;
}
?>