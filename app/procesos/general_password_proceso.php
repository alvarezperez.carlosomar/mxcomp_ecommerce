<?php
if(isset($_POST['accion']) && $_POST['accion'] === "password"){
  include dirname(__DIR__, 1) . '/funciones/validaciones_password.php';

  $contrasenia = $_POST['password'];
  
  if($contrasenia !== "" && validar_password($contrasenia)){
    $seguridad = (int) seguridad_password($contrasenia);
    
    $respuesta = "3";
    $width = $seguridad . '%';
    $color = $seguridad <= 25 ? '#D9534F' : ( $seguridad <= 50 ? '#F0AD4E' : ( $seguridad <= 75 ? '#E2901B' : ( $seguridad <= 100 ? '#5CB85C' : '#424242' ) ) );
  }else if($contrasenia === ""){ // EL PASSWORD SE ENCUENTRA VACÍO
    $respuesta = "1";
    $width = '0%';
    $color = '#D9534F';
  }else{ // EL PASSWORD TIENE SIMBOLOS NO PERMITIDOS
    $respuesta = "2";
    $width = '0%';
    $color = '';
  }
  
  $json = [ 'respuesta' => $respuesta, 'width' => $width, 'color' => $color ];
  echo json_encode($json);
}
?>