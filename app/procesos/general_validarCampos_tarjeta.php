<?php
if(isset($_POST['accion']) && $_POST['accion'] === "validar"){
  session_start();
  include '../funciones/validaciones_campos.php';
  include '../funciones/encriptacion.php';
  include '../global/config.php';
  include '../conn.php';
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $nomTitular = trim($_POST['nom_titu']);
  $numTarjeta = trim($_POST['num_tarjeta']);
  $tipoTarjeta = trim($_POST['tipo_tarjeta']);
  $mesVencimiento = trim($_POST['mes_vencimiento']);
  $anioVencimiento = trim($_POST['anio_vencimiento']);
  $cvc_tarjeta = trim($_POST['cvc']);
  $id_tipoVialidad = trim($_POST['tipo_vialidad']);
  $nom_tipoVialidad = "";
  $nomVialidad = trim($_POST['nom_vialidad']);
  $noExterior = trim($_POST['no_ext']);
  $noExteriorEstadoCheck = trim($_POST['no_ext_estadoCheck']);
  $noInterior = trim($_POST['no_int']);
  $noInteriorEstadoCheck = trim($_POST['no_int_estadoCheck']);
  $codigoPostal = trim($_POST['codigo_postal']);
  $colonia = trim($_POST['colonia']);
  $ciudadMunicipio = trim($_POST['ciudad_municipio']);
  $id_estado = trim($_POST['estado']);
  $nom_estado = "";
  
  $proceso_correcto = false;
  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $mensaje = "";
  
  if(validar_campo_numerico($idUser)){
    $idUser = (int) $idUser;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario FROM __usuarios WHERE id = :idUser AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      
      if((int) $datos_usuario['conteo'] === 1){
        $proceso_correcto = true;
      }else{
        $respuesta = "0";
        $mensaje = "El usuario no existe";
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: ".$error->getMessage();
      $mensaje = "Problema al buscar el usuario";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "No es numérico";
    $proceso_correcto = false;
  }
  
  // REVISA LOS NOMBRES Y APELLIDOS DEL TITULAR
  if($proceso_correcto){
    if($nomTitular !== "" && validar_campo_letras_espacios($nomTitular)){
      $nomTitular = (string) $nomTitular;
      $proceso_correcto = true;
    }else if($nomTitular === ""){
      $respuesta = "1"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "2"; //El nombre del titular no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO DE LA TARJETA
  if($proceso_correcto){
    if($numTarjeta !== "" && validar_campo_numerico($numTarjeta)){
      $numTarjeta = (string) $numTarjeta;
      $proceso_correcto = true;
    }else if($numTarjeta === ""){
      $respuesta = "3"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; //El numero de tarjeta no es numerico
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL TIPO DE TARJETA
  if($proceso_correcto){
    if($tipoTarjeta === ""){
      $tipoTarjeta = (string) $tipoTarjeta;
      $proceso_correcto = true;
    }else{
      if($tipoTarjeta !== "" && validar_campo_letras_espacios($tipoTarjeta)){
        if($tipoTarjeta !== "Aún no se tiene el dato"){
          $tipoTarjeta = (string) $tipoTarjeta;
          $proceso_correcto = true;
        }
      }else if($tipoTarjeta === ""){
        $respuesta = "5"; //No se ha generado un tipo de tarjeta
        $proceso_correcto = false;
      }else{
        $respuesta = "6"; //Este tipo de tarjeta no entra en la expresion regular
        $proceso_correcto = false;
      }
    }
  }
  
  // REVISA EL MES DE LA FECHA DE VENCIMIENTO DE LA TARJETA
  if($proceso_correcto){
    if($mesVencimiento !== "" && validar_campo_numerico($mesVencimiento)){
      if(mb_strlen($mesVencimiento) === 2){
        $mesVencimiento = (string) $mesVencimiento;
        $proceso_correcto = true;
      }else{
        $respuesta = "9"; //El mes de la fecha de vencimiento no tiene 2 dígitos
        $proceso_correcto = false;
      }
    }else if($mesVencimiento === ""){
      $respuesta = "7"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "8"; //El mes de la fecha de vencimiento no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL AÑO DE LA FECHA DE VENCIMIENTO DE LA TARJETA
  if($proceso_correcto){
    if($anioVencimiento !== "" && validar_campo_numerico($anioVencimiento)){
      if(mb_strlen($anioVencimiento) === 2 || mb_strlen($anioVencimiento) === 4){
        $anioVencimiento = (string) $anioVencimiento;
        $proceso_correcto = true;
      }else{
        $respuesta = "12"; //El año de la fecha de vencimiento no tiene 2 o 4 digitos
        $proceso_correcto = false;
      }
    }else if($anioVencimiento === ""){
      $respuesta = "10"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "11"; //El año de la fecha de vencimiento no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CVC DE LA TARJETA (CODIGO DE SEGURIDAD)
  if($proceso_correcto){
    if($cvc_tarjeta !== "" && validar_campo_numerico($cvc_tarjeta)){
      if(mb_strlen($cvc_tarjeta) === 3 || mb_strlen($cvc_tarjeta) === 4){
        $cvc_tarjeta = (string) $cvc_tarjeta;
        $proceso_correcto = true;
      }else{
        $respuesta = "15"; // No es de 3 o 4 dígitos
        $proceso_correcto = false;
      }
    }else if($cvc_tarjeta === ""){
      $respuesta = "13"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "14"; //El CVC no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL TIPO DE VIALIDAD
  if($proceso_correcto){
    if($id_tipoVialidad !== "" && validar_campo_numerico($id_tipoVialidad)){
      $id_tipoVialidad = (int) $id_tipoVialidad;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nom_tipoVialidad FROM __tipos_vialidad WHERE id = :id";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':id', $id_tipoVialidad, PDO::PARAM_INT);
        $stmt->execute();
        $datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC);
        $tipoVialidad_existe = (int) $datos_tipoVialidad['conteo'];
        
        if($tipoVialidad_existe === 1){
          $nom_tipoVialidad = (string) $datos_tipoVialidad['nom_tipoVialidad'];
          $proceso_correcto = true;
        }else{
          $respuesta = "17"; //Este tipo de vialidad no existe
          $proceso_correcto = false;
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: ".$error->getMessage();
        $mensaje = "Existe un problema al buscar los tipos de vialidad";
        $proceso_correcto = false;
      }
    }else if($id_tipoVialidad === ""){
      $respuesta = "16"; //No se ha seleccionado un tipo de vialidad
      $proceso_correcto = false;
    }else{
      $respuesta = "17"; //Este tipo de vialidad no existe
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NOMBRE DE VIALIDAD
  if($proceso_correcto){
    if($nomVialidad !== "" && validar_campo_letras_espacios_simbolos($nomVialidad)){
      $nomVialidad = (string) $nomVialidad;
      $proceso_correcto = true;
    }else if($nomVialidad === ""){
      $respuesta = "18"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "19"; //El nombre de la vialidad no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO EXTERIOR
  if($proceso_correcto){
    if(($noExterior !== "" && $noExteriorEstadoCheck === "false" && validar_campo_noExt_Int($noExterior)) || ($noExterior === "" && $noExteriorEstadoCheck === "true")){
      $noExterior = (string) $noExterior;
      
      if($noExterior === ""){
        $noExterior = "S/N";
      }
      
      $proceso_correcto = true;
    }else if($noExterior === "" && $noExteriorEstadoCheck === "false"){
      $respuesta = "20"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "21"; //No cumple con la expresion regular y no esta habilitada la opcion "Sin no. exterior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL NUMERO INTERIOR
  if($proceso_correcto){
    if(($noInterior !== "" && $noInteriorEstadoCheck === "false" && validar_campo_noExt_Int($noInterior)) || ($noInterior === "" && $noInteriorEstadoCheck === "true")){
      $noInterior = (string) $noInterior;
      
      if($noInterior === ""){
        $noInterior = "S/N";
      }
      
      $proceso_correcto = true;
    }else if($noInterior === "" && $noInteriorEstadoCheck === "false"){
      $respuesta = "22"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "23"; //No cumple con la expresion regular y no esta habilitada la opcion "Sin no. interior"
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CODIGO POSTAL
  if($proceso_correcto){
    if($codigoPostal !== "" && validar_campo_numerico($codigoPostal)){
      if(strlen($codigoPostal) === 5){
        $codigoPostal = (string) $codigoPostal;
        $proceso_correcto = true;
      }else{
        $respuesta = "26"; //El codigo postal no tiene la longitud adecuada
        $proceso_correcto = false;
      }
    }else if($codigoPostal === ""){
      $respuesta = "24"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "25"; //El codigo postal no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA COLONIA
  if($proceso_correcto){
    if($colonia !== "" && validar_campo_letras_espacios_simbolos($colonia)){
      $colonia = (string) $colonia;
      $proceso_correcto = true;
    }else if($colonia === ""){
      $respuesta = "27"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "28"; //La colonia no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CIUDAD O MUNICIPIO
  if($proceso_correcto){
    if($ciudadMunicipio !== "" && validar_campo_letras_espacios($ciudadMunicipio)){
      $ciudadMunicipio = (string) $ciudadMunicipio;
      $proceso_correcto = true;
    }else if($ciudadMunicipio === ""){
      $respuesta = "29"; //Este campo se encuentra vacio
      $proceso_correcto = false;
    }else{
      $respuesta = "30"; //La ciudad o municipio no cumple con la expresion regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL ESTADO Y FINALIZA LA REVISIÓN DE LAS VALIDACIONES
  if($proceso_correcto){
    if($id_estado !== "" && validar_campo_numerico($id_estado)){
      $id_estado = (int) $id_estado;
      
      try{
        $sql = "SELECT COUNT(id) AS conteo, id, nomEstado FROM __estados_codigos WHERE id = :id";
        $stmt = $conexion->prepare($sql);
        $stmt->bindParam(':id', $id_estado, PDO::PARAM_INT);
        $stmt->execute();
        $datos_estado = $stmt->fetch(PDO::FETCH_ASSOC);
        $estado_existe = (int) $datos_estado['conteo'];
        
        if($estado_existe === 1){
          $nom_estado = (string) $datos_estado['nomEstado'];
          $respuesta = "33";
        }else{
          $respuesta = "32"; //Este estado no existe
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "0";
        //$mensaje = "Error: ".$error->getMessage();
        $mensaje = "Existe un problema al buscar el Estado";
      }
    }else if($id_estado === ""){
      $respuesta = "31"; //No se ha seleccionado un estado
    }else{
      $respuesta = "32"; //Este estado no existe
    }
  }
  
  unset($_POST['accion']);
  unset($_POST['nom_titu']);
  unset($_POST['num_tarjeta']);
  unset($_POST['tipo_tarjeta']);
  unset($_POST['mes_vencimiento']);
  unset($_POST['anio_vencimiento']);
  unset($_POST['cvc']);
  unset($_POST['tipo_vialidad']);
  unset($_POST['nom_vialidad']);
  unset($_POST['no_ext']);
  unset($_POST['no_ext_estadoCheck']);
  unset($_POST['no_int']);
  unset($_POST['no_int_estadoCheck']);
  unset($_POST['codigo_postal']);
  unset($_POST['colonia']);
  unset($_POST['ciudad_municipio']);
  unset($_POST['estado']);
  
  $json[] = array(
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'nom_tipoVialidad' => $nom_tipoVialidad,
    'noExterior' => $noExterior,
    'noInterior' => $noInterior,
    'nom_estado' => $nom_estado
  );
  $json_string = json_encode($json);
  echo $json_string;
}
?>