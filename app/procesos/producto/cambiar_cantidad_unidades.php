<?php
if(isset($_POST['accion']) && $_POST['accion'] === "cambiar"){
  session_start();
  
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';

  $seccion = trim($_POST['seccion']);
  $id = trim($_POST['id']);
  $nombreAlmacen = trim($_POST['almacen']);
  $codigoProducto = trim($_POST['codigo_producto']);
  $cantidad = trim($_POST['cantidad']);

  $Conn_mxcomp = new Conexion_mxcomp();

  $proceso_correcto = false;
  $cantidad_generada = 0;
  $mensaje = '';
  
  // REVISA EL ID
  if($id !== "" && validar_campo_numerico($id)){
    $id = (int) $id;
    $nombreAlmacen = desencriptar($nombreAlmacen);
    $proceso_correcto = true;
  }else{
    $respuesta = "0"; // SE REINICIA PORQUE NO ES NUMERICO EL ID
    $proceso_correcto = false;
  }
  
  // REVISA EL NOMBRE DEL ALMACÉN
  if($proceso_correcto){
    if($nombreAlmacen !== "" && validar_nombreAlmacen_carrito($nombreAlmacen)){
      $nombreAlmacen = (string) $nombreAlmacen;
      $proceso_correcto = true;
    }else{
      $respuesta = "0"; // SE REINICIA PORQUE NO ES NUMERICO EL ALMACEN
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CODIGO DEL PRODUCTO
  if($proceso_correcto){
    if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
      $codigoProducto = (string) $codigoProducto;
      $proceso_correcto = true;
    }else{
      $respuesta = "0"; // SE REINICIA PORQUE LA SKU NO ES VALIDA
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CANTIDAD
  if($proceso_correcto){
    if($cantidad !== "" && validar_campo_numerico($cantidad)){
      $cantidad = (int) $cantidad;
      $proceso_correcto = true;
    }else{
      $respuesta = "1"; // SE EJECUTA DE NUEVO LA FUNCION PARA MOSTRAR LOS ELEMENTOS
      $proceso_correcto = false;
    }
  }
  
  // SE REVISA QUE EL PRODUCTO SEA EL MISMO, SI ES DIFERENTE SE REINICIAN LAS CANTIDADES A 0 PARA EL NUEVO PRODUCTO
  if($proceso_correcto){
    if($codigoProducto !== (string) $_SESSION['__producto_unidades__']['codigoProducto']){
      try{
        $sql = "SELECT almacenes FROM __productos WHERE BINARY codigoProducto = :codigoProducto";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt->execute();
        $almacenes = (array) json_decode($stmt->fetchColumn(), true);

        // SE REVISA DE QUE ALMACENES SE DEBEN DE CREAR EN EL ARRAY
        $array_almacenes_producto = [];
        foreach($almacenes as $datos_almacen){
          $array_almacenes_producto[$datos_almacen['nombre_almacen']] = [
            'unidades' => 0,
            'activo' => 1
          ];
        }
      
        unset($_SESSION['__producto_unidades__']);

        $_SESSION['__producto_unidades__'] = array(
          'codigoProducto' => $codigoProducto,
          'arrayInputs' => $array_almacenes_producto
        );

        $proceso_correcto = true;
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "2";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "No se encontró el producto.";
        $proceso_correcto = false;
      }
    }else{
      $proceso_correcto = true;
    }
  }
  
  // SE REVISA QUE LA SECCIÓN EXISTA, PARA GENERAR LA CANTIDAD NUEVA
  if($proceso_correcto){
    $proceso_correcto = true;

    switch($seccion){
      case "escrita":
        $cantidad_generada = $cantidad;
        break;

      case "disminuir":
        $cantidad_generada = $cantidad - 1;
        break;

      case "aumentar":
        $cantidad_generada = $cantidad + 1;
        break;

      default:
        $respuesta = "0"; // SE REINICIA PORQUE NO EXISTE LA SECCION
        $proceso_correcto = false;
    }
  }

  // SE AGREGA LA NUEVA CANTIDAD AL ALMACÉN QUE LE CORRESPONDE
  if($proceso_correcto){
    foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $nombreAlmacen_indice=>$datos_producto){
      if($nombreAlmacen === $nombreAlmacen_indice){
        $_SESSION['__producto_unidades__']['arrayInputs'][$nombreAlmacen_indice]['unidades'] = $cantidad_generada;
        $respuesta = "3"; // SE CAMBIO LA CANTIDAD DE UNIDADES
        break;
      }
    }
  }

  unset($Conn_mxcomp);
  unset($_POST['accion']);
  unset($_POST['seccion']);
  unset($_POST['id']);
  unset($_POST['almacen']);
  unset($_POST['codigo_producto']);
  unset($_POST['cantidad']);
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>