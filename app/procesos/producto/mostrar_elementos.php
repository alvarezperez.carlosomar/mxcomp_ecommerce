<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();
  
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/global/config.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  $Producto_codigo = trim($_POST['codigo_producto']);
  $Conn_mxcomp = new Conexion_mxcomp();
  
  $html_campos_cantidad = '';
  $html_precio_total = '';
  $html_botones = '';
  
  if(validar_codigoProducto_caracteres($Producto_codigo) && validar_codigoProducto_formato($Producto_codigo)){
    $Producto_codigo = (string) $Producto_codigo;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, skuProveedor, precioMXcomp, monedaMXcomp, existenciaTotal, almacenes, codigoProveedor, nombreProveedor FROM __productos WHERE BINARY codigoProducto = :codigoProducto";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $Producto_codigo, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];
      
      if($producto_existe === 1){
        $respuesta = "1";

        // INFO BASE DE DATOS - PRODUCTO
        $Producto_skuProveedor = (string) $datos_producto['skuProveedor'];
        $Producto_precioMXcomp = (float) $datos_producto['precioMXcomp'];
        $Producto_monedaMXcomp = (string) $datos_producto['monedaMXcomp'];
        $Producto_existenciaTotal = (int) $datos_producto['existenciaTotal'];
        $Producto_almacenes = (array) json_decode($datos_producto['almacenes'], true);
        $Producto_codigoProveedor = (string) $datos_producto['codigoProveedor'];
        $Producto_nombreProveedor = (string) $datos_producto['nombreProveedor'];

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //CANTIDAD - HTML
        if($Producto_existenciaTotal === 0){
          $html_campos_cantidad = '
        <label class="p-label p-producto-p_avisoCarrito">No se cuenta con productos por el momento.</label>
        
        <div class="p-buttons p-producto-buttons_contenedor">
          <a href="atencion-a-clientes" class="p-button p-button_success">
            <span>
              <i class="fas fa-clipboard"></i>
            </span>
            <span><b>Solicitar existencia</b></span>
          </a>
        </div>';
        }else{
          $total_inputAlmacenes = 0;
          $id_almacen_consecutivo = 1;
          $array_almacenes_errores = [];

          $html_campos_cantidad = '
        <section class="p-producto-cantidadAlmacenes_contenedor">';
          
          foreach($Producto_almacenes as $datos_almacen){
            $Producto_nombreAlmacen = (string) $datos_almacen['nombre_almacen'];
            $Producto_existenciaAlmacen = (int) $datos_almacen['existencia'];
            $unidadesInput = (int) $_SESSION['__producto_unidades__']['arrayInputs'][$Producto_nombreAlmacen]['unidades'];

            $html_campos_cantidad .= '
          <div class="p-producto-cantidadAlmacenes_elementos_contenedor">
            <h3>Almacén ' . ucwords(mb_strtolower($Producto_nombreAlmacen)) . '</h3>';

            // REVISA CUANTAS UNIDADES EXISTEN EN EL CARRITO
            if(isset($_SESSION['__id__'])){
              $idUsuario = desencriptar(trim($_SESSION['__id__']));

              if(validar_campo_numerico($idUsuario)){
                try{
                  $sql = "SELECT COUNT(id) AS conteo, unidades, tieneExistencias FROM __carrito WHERE idUsuario = :idUsuario AND codigoProducto = :codigoProducto AND nombreAlmacen = :nombreAlmacen";
                  $stmt = $Conn_mxcomp->pdo->prepare($sql);
                  $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                  $stmt->bindParam(':codigoProducto', $Producto_codigo, PDO::PARAM_STR);
                  $stmt->bindParam(':nombreAlmacen', $Producto_nombreAlmacen, PDO::PARAM_STR);
                  $stmt->execute();
                  $datos_productoCar = $stmt->fetch(PDO::FETCH_ASSOC);
                  $producto_existe_carrito = (int) $datos_productoCar['conteo'];

                  if($producto_existe_carrito === 1){
                    $cantidadTotal_carrito = (int) $datos_productoCar['unidades'];

                    if((string) $datos_productoCar['tieneExistencias'] === '1'){
                      if($cantidadTotal_carrito > 0){
                        $html_campos_cantidad .= '
            <p class="p-producto-p_avisoCarrito">
              <b>Tienes ' . $cantidadTotal_carrito . ' ' . ( $cantidadTotal_carrito === 1 ? 'unidad' : 'unidades' ) . ' en tu carrito.</b>
            </p>';
                      }
                    }
                  }

                  $stmt = null;
                }catch(PDOException $e){
                  $html_campos_cantidad .= '
            <p class="p-producto-cantidad_cantidad_noDisp">
              <b>Error al revisar si el producto se encuentra en el carrito de este almacén.</b>
            </p>';
                }
              }
            }else{
              if(isset($_SESSION['__carrito__'])){
                foreach($_SESSION['__carrito__'] as $Carrito_nombreAlmacen=>$productos_carritoAlmacen){
                  if($Producto_nombreAlmacen === $Carrito_nombreAlmacen){
                    foreach($productos_carritoAlmacen as $informacion_producto){
                      if((string) $informacion_producto['codigoProducto'] === $Producto_codigo){
                        $cantidadTotal_carrito = (int) $informacion_producto['unidades'];
                        if($cantidadTotal_carrito > 0){
                          $html_campos_cantidad .= '
            <p class="p-producto-p_avisoCarrito">
              <b>Tienes ' . $cantidadTotal_carrito . ' ' . ( $cantidadTotal_carrito === 1 ? 'unidad' : 'unidades' ) . ' en tu carrito.</b>
            </p>';
                        }
                        break;
                      }
                    }
                  }
                }
              }
            }

            if( ($unidadesInput >= 1 && $unidadesInput <= $Producto_existenciaAlmacen) || ($unidadesInput === ($Producto_existenciaAlmacen + 1) ) ){
              $boton_disminuir = ' g-producto-cambiar_cantidad_boton" data-seccion="disminuir" data-id="' . $id_almacen_consecutivo . '" data-almacen="' . encriptar($Producto_nombreAlmacen) . '"';
            }else{
              $boton_disminuir = '" disabled';
            }
            
            if($unidadesInput < $Producto_existenciaAlmacen){
              $boton_aumentar = ' g-producto-cambiar_cantidad_boton" data-seccion="aumentar" data-id="' . $id_almacen_consecutivo . '" data-almacen="' . encriptar($Producto_nombreAlmacen) . '"';
            }else{
              $boton_aumentar = '" disabled';
            }

            $html_campos_cantidad .= '
            <div class="p-producto-cantidadAlmacenes_info_contenedor">
              <div class="p-producto-cantidadAlmacenes_campo_cantidad">
                <div>
                  <button class="p-button p-button_account p-button_square p-button_cantidad_square' . $boton_disminuir . '>
                    <span>
                      <i class="fas fa-minus"></i>
                    </span>
                  </button>

                  <div class="p-cantidadAlmacen_contenedor">
                    <input type="text" class="p-input p-producto-cantidadAlmacenes_input g-producto-cambiar_cantidad_input" data-seccion="escrita" data-id="' . $id_almacen_consecutivo . '" data-almacen="' . encriptar($Producto_nombreAlmacen) . '" value="' . $unidadesInput . '" autocomplete="off">
                    <div class="p-loading-general p-cantidadAlmacen_loading g-producto-cantidad_loading" data-id="' . $id_almacen_consecutivo . '">
                      <div></div>
                    </div>
                  </div>

                  <button class="p-button p-button_account p-button_square p-button_cantidad_square' . $boton_aumentar . '>
                    <span>
                      <i class="fas fa-plus"></i>
                    </span>
                  </button>
                </div>
                <label class="p-label p-producto-cantidadAlmacenes_label">' . ( $unidadesInput === 1 ? 'unidad' : 'unidades' ) . '</label>
              </div>
              <div>';
                      
            if($unidadesInput > $Producto_existenciaAlmacen){
              $html_campos_cantidad .= '
                <p class="p-producto-cantidad_cantidad_noDisp">
                  <b>Cantidad no disponible</b>
                </p>';
            }

            $html_campos_cantidad .= '
                <p class="p-producto-cantidad_cantidad_disp">
                  <b>' . ( $Producto_existenciaAlmacen === 1 ? $Producto_existenciaAlmacen . ' disponible' : $Producto_existenciaAlmacen . ' disponibles' ) . '</b>
                </p>
              </div>
            </div>
          </div>';

            //////////////////////////////////////////////////////////////////////////////////////////////
            // SE BUSCAN LAS CANTIDADES QUE SEAN MENOR A 0 O MAYORES A LA EXISTENCIA

            if( ($unidadesInput >= 0) && ($unidadesInput <= $Producto_existenciaAlmacen) ){
              $total_inputAlmacenes += $unidadesInput;
            }else{
              $posicion = count($array_almacenes_errores);
              $array_almacenes_errores[$posicion] = ucwords(mb_strtolower($Producto_nombreAlmacen));
            }

            $id_almacen_consecutivo++;
          }
          
          $html_campos_cantidad .= '
        </section>';

          ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // PRECIO TOTAL - HTML
          if(count($array_almacenes_errores) > 0){
            $texto = "En el almacén <b>";
            
            if(count($array_almacenes_errores) === 1){
              $texto .= $array_almacenes_errores[0];
            }else if(count($array_almacenes_errores) === 2){
              $texto .= $array_almacenes_errores[0] . " y " . $array_almacenes_errores[1];
            }else{
              $ultimoIndice = (int) count($array_almacenes_errores) - 1;
              
              foreach($array_almacenes_errores as $indice=>$nombreAlmacen){
                $texto .= $indice === 0 ? $nombreAlmacen : ($indice === $ultimoIndice ? " y " . $nombreAlmacen : ", " . $nombreAlmacen);
              }
            }

            $texto .= "</b> la cantidad es mayor a la disponible.";
            
            $html_precio_total = '
        <p class="p-producto-cantidad_cantidad_noDisp p-producto-aviso_precio_rojo">
          <span>' . $texto . '</span>
        </p>';
          }else{
            if($total_inputAlmacenes === 0){
              $html_precio_total = '';
            }else{
              $total_generado = number_format($Producto_precioMXcomp * $total_inputAlmacenes, 2, '.', ',');
  
              if($total_generado !== 0.00){  
                $html_precio_total = '
        <p class="p-producto-p_precio_total_color p-producto-p_precio_total_marginTop">
          <span>' . ( $total_inputAlmacenes === 1 ? "x" . $total_inputAlmacenes . " unidad:" : "x" . $total_inputAlmacenes . " unidades:" ) . '</span>
          <span>$ ' . $total_generado . '</span>
          <span>' . $Producto_monedaMXcomp . '</span>
        </p>';
              }
            }
          }
          
          ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // BOTONES COMPRAR Y AGREGAR AL CARRITO - HTML
          if($total_inputAlmacenes === 0 || count($array_almacenes_errores) > 0){
            $html_botones .= '
        <a class="p-button p-button_info" disabled title="Agrega al menos 1 unidad o una cantidad disponible.">
          <span>
            <i class="fas fa-dollar-sign"></i> 
          </span>
          <span><b>Comprar ahora</b></span>
        </a>';

        $html_botones .= '
        <a class="p-button_inverso p-button_info_inverso" disabled title="Agrega al menos 1 unidad o una cantidad disponible.">
          <span>
            <i class="fas fa-shopping-cart"></i> 
          </span>
          <span><b>Agregar al carrito</b></span>
        </a>';
          }else{
            $html_botones .= '
        <a href="comprar/1" class="p-button p-button_info g-producto-comprar">
          <span>
            <i class="fas fa-dollar-sign"></i> 
          </span>
          <span><b>Comprar ahora</b></span>
        </a>';

          $html_botones .= '
        <button class="p-button_inverso p-button_info_inverso" id="id-producto-agregar_carrito" data-codigo="' . encriptar($Producto_codigo) . '">
          <span>
            <i class="fas fa-shopping-cart"></i> 
          </span>
          <span><b>Agregar al carrito</b></span>
        </button>';
          }
        }
        
        $html_botones_ficha_tecnica = '';
        if($Producto_nombreProveedor === 'PCH MAYOREO'){
          $html_botones_ficha_tecnica = '
        <a class="p-button p-button_file_pdf p-producto-ficha_ver" id="id-producto-boton_abrir_modal_pdf">
          <span>
            <i class="far fa-file-pdf"></i>
          </span>
          <span><b>Ver ficha técnica</b></span>
        </a>
        <a href="https://fichastecnicas.pchmayoreo.com/' . $Producto_skuProveedor . '.pdf" target="_blank" class="p-button p-button_file_pdf p-producto-ficha_ocultar" id="id-producto-boton_descargar_pdf">
          <span>
            <i class="fas fa-file-download"></i>
          </span>
          <span><b>Descargar ficha técnica</b></span>
        </a>';
        }
      }else{
        $respuesta = "2";
        $html_campos_cantidad = '
        <p class="p-notification p-notification_letter_error" id="id-producto-mensaje_problema">
          <span>El producto no existe, por lo tanto no se encontraron elementos para interactuar.</span>
        </p>';
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "2";
      //$error_msg = "Error: " . $error->getMessage();
      $error_msg = "Ocurrió un problema al buscar las opciones del producto.";
      $html_campos_cantidad = '
        <p class="p-notification p-notification_letter_error" id="id-producto-mensaje_problema">
          <span>' . $error_msg . '</span>
        </p>';
    }
  }else{
    $respuesta = "2";
    $html_campos_cantidad = '
        <p class="p-notification p-notification_letter_error" id="id-producto-mensaje_problema">
          <span>Hay un problema con el código del producto.</span>
        </p>';
  }

  $json = [
    'respuesta' => $respuesta,
    'html_cCantidad' => $html_campos_cantidad,
    'html_pTotal' => $html_precio_total,
    'html_botones' => $html_botones,
    'html_botones_ficha_tecnica' => $html_botones_ficha_tecnica,
    'codigoProveedor' => $Producto_codigoProveedor,
    'skuProveedor' => $Producto_skuProveedor
  ];
  echo json_encode($json);
}
?>