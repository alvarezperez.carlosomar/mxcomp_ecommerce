<?php
if(isset($_POST['accion']) && $_POST['accion'] === "agregar"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $codigoProducto = desencriptar(trim($_POST['codigo_producto']));

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA EL CÓDIGO DEL PRODUCTO
  if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
    $codigoProducto = (string) $codigoProducto;
    $proceso_correcto = true;
  }else{
    $respuesta = "1";
    $mensaje = "<span>No se pudo concretar el proceso, vuelve a intentarlo.</span>";
    $proceso_correcto = false;
  }
  
  // BUSCA LA INFO DEL PRODUCTO
  if($proceso_correcto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, existenciaTotal, almacenes, nombreProveedor, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, envioGratisPermitido FROM __productos WHERE BINARY codigoProducto = :codigoProducto";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];

      if($producto_existe === 1){
        $Producto_skuProveedor = (string) trim($datos_producto['skuProveedor']);
        $Producto_descripcion = (string) trim($datos_producto['descripcion'], " \xC2\xA0");
        $Producto_descripcionURL = (string) trim($datos_producto['descripcionURL'], " \xC2\xA0");
        $Producto_nombreMarca = (string) trim($datos_producto['nombreMarca']);
        $Producto_precioProveedor = (float) trim($datos_producto['precioProveedor']);
        $Producto_monedaProveedor = (string) trim($datos_producto['monedaProveedor']);
        $Producto_precioMXcomp = (float) trim($datos_producto['precioMXcomp']);
        $Producto_monedaMXcomp = (string) trim($datos_producto['monedaMXcomp']);
        $Producto_existenciaTotal = (int) trim($datos_producto['existenciaTotal']);
        $Producto_almacenes = (array) json_decode(trim($datos_producto['almacenes']), true);
        $Producto_nombreProveedor = (string) trim($datos_producto['nombreProveedor']);
        $Producto_tieneImagen = (int) trim($datos_producto['tieneImagen']);
        $Producto_numeroUbicacionImagen = $datos_producto['numeroUbicacionImagen'];
        $Producto_nombreImagen = (string) trim($datos_producto['nombreImagen']);
        $Producto_versionImagen = $datos_producto['versionImagen'];
        $Producto_envioGratisPermitido = (string) trim($datos_producto['envioGratisPermitido']);

        $Producto_numeroUbicacionImagen = is_null($Producto_numeroUbicacionImagen) ? NULL : trim($Producto_numeroUbicacionImagen);
        $Producto_versionImagen = is_null($Producto_versionImagen) ? NULL : trim($Producto_versionImagen);
        
        $proceso_correcto = true;
      }else{
        $respuesta = "1";
        $mensaje = "<span>No se encontró el producto, recarga la página.</span>";
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "1";
      //$msj_error = "Error: " . $error->getMessage();
      $msj_error = "No se puso concretar el proceso, vuelve a intentarlo.";
      $mensaje = "<span>' . $msj_error . '</span>";
      $proceso_correcto = false;
    }
  }
  
  // REVISA QUE LAS CANTIDADES AL MOMENTO DE AGREGAR AL CARRITO SEAN MAYORES A 0
  if($proceso_correcto){
    $almacenes_totalUnidades = 0;
    foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $datos_almacen){
      $almacenes_totalUnidades = ( (int) $datos_almacen['unidades'] > 0 ) ? $almacenes_totalUnidades + 1 : $almacenes_totalUnidades;
    }

    if($almacenes_totalUnidades > 0){
      $proceso_correcto = true;
    }else{
      $respuesta = "1";
      $mensaje = "<span>Debes de tener al menos 1 unidad agregada en algún almacén, para añadir al carrito.</span>";
      $proceso_correcto = false;
    } 
  }
  
  // PROCESO PARA AGREGAR AL CARRITO: SESSION O BASE DE DATOS
  if($proceso_correcto){
    if(isset($_SESSION['__id__'])){
      $idUsuario = desencriptar(trim($_SESSION['__id__']));
      $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

      // REVISA EL ID DE USUARIO
      if(validar_campo_numerico($idUsuario)){
        $idUsuario = (int) $idUsuario;
        $proceso_correcto = true;
      }else{
        $respuesta = "1";
        $mensaje = "<span>Hay un problema con tu usuario, recarga la página.</span>";
        $proceso_correcto = false;
      }
      
      // SE AGREGA EL PRODUCTO EN EL CARRITO
      if($proceso_correcto){
        $proceso_correcto = false;
        $fechaActual = date("Y-m-d H:i:s");
        $guardado = '0';

        try{
          $Conn_mxcomp->pdo->beginTransaction();

          foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $nombreAlmacen=>$datos_almacen){
            $almacen_unidades = (int) $datos_almacen['unidades'];

            if($almacen_unidades > 0){
              $numeroAlmacen = '';
    
              foreach($Producto_almacenes as $datos_productoAlmacen){
                if($datos_productoAlmacen['nombre_almacen'] === $nombreAlmacen){
                  $numeroAlmacen = (string) $datos_productoAlmacen['id_almacen'];
                  $existenciaAlmacen = (int) $datos_productoAlmacen['existencia'];
                  break;
                }
              }

              $sql = "SELECT COUNT(id) AS conteo, id, unidades FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
              $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
              $stmt->execute();
              $datos_carrito = $stmt->fetch(PDO::FETCH_ASSOC);
              $productoCarrito_existe = (int) $datos_carrito['conteo'];
              $id_producto = (int) $datos_carrito['id'];

              if($productoCarrito_existe === 1){
                $cantidad_nueva = (int) $datos_carrito['unidades'] + $almacen_unidades;

                $sql = "UPDATE __carrito SET descripcion = :descripcion, descripcionURL = :descripcionURL, nombreMarca = :nombreMarca, precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, unidades = :unidades, tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, nombreImagen = :nombreImagen, versionImagen = :versionImagen, envioGratisPermitido = :envioGratisPermitido, guardado = :guardado, fechaActualizacion = :fechaActualizacion WHERE id = :id AND idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
                $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
                $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
                $stmt->bindParam(':precioProveedor', $Producto_precioProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':monedaProveedor', $Producto_monedaProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':precioMXcomp', $Producto_precioMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':unidades', $cantidad_nueva, PDO::PARAM_INT);
                $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
                $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
                $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':envioGratisPermitido', $Producto_envioGratisPermitido, PDO::PARAM_STR);
                $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
                $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
                $stmt->bindParam(':id', $id_producto, PDO::PARAM_INT);
                $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
                $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
                $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
                $stmt->execute();
              }else{
                $tieneExistencias = "1";
                $registroExiste = "1";

                $sql = "INSERT INTO __carrito(idUsuario, codigoUsuario, codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, unidades, numeroAlmacen, nombreAlmacen, existenciaAlmacen, existenciaTotalProducto, nombreProveedor, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, envioGratisPermitido, tieneExistencias, guardado, registroExiste, fechaCreacion)
                VALUES (:idUsuario, :codigoUsuario, :codigoProducto, :skuProveedor, :descripcion, :descripcionURL, :nombreMarca, :precioProveedor, :monedaProveedor, :precioMXcomp, :monedaMXcomp, :unidades, :numeroAlmacen, :nombreAlmacen, :existenciaAlmacen, :existenciaTotalProducto, :nombreProveedor, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :envioGratisPermitido, :tieneExistencias, :guardado, :registroExiste, :fechaCreacion)";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
                $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
                $stmt->bindParam(':skuProveedor', $Producto_skuProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':descripcion', $Producto_descripcion, PDO::PARAM_STR);
                $stmt->bindParam(':descripcionURL', $Producto_descripcionURL, PDO::PARAM_STR);
                $stmt->bindParam(':nombreMarca', $Producto_nombreMarca, PDO::PARAM_STR);
                $stmt->bindParam(':precioProveedor', $Producto_precioProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':monedaProveedor', $Producto_monedaProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':precioMXcomp', $Producto_precioMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':monedaMXcomp', $Producto_monedaMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':unidades', $almacen_unidades, PDO::PARAM_INT);
                $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
                $stmt->bindParam(':existenciaAlmacen', $existenciaAlmacen, PDO::PARAM_INT);
                $stmt->bindParam(':existenciaTotalProducto', $Producto_existenciaTotal, PDO::PARAM_INT);
                $stmt->bindParam(':nombreProveedor', $Producto_nombreProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':tieneImagen', $Producto_tieneImagen, PDO::PARAM_STR);
                $stmt->bindParam(':numeroUbicacionImagen', $Producto_numeroUbicacionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreImagen', $Producto_nombreImagen, PDO::PARAM_STR);
                $stmt->bindParam(':versionImagen', $Producto_versionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':envioGratisPermitido', $Producto_envioGratisPermitido, PDO::PARAM_STR);
                $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
                $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
                $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
                $stmt->bindParam(':fechaCreacion', $fechaActual, PDO::PARAM_STR);
                $stmt->execute();
              }
            }
          }

          $respuesta = "2";
          $mensaje = "Listo - Base de datos";

          unset($_SESSION['__producto_unidades__']);

          $Conn_mxcomp->pdo->commit();
          $stmt = null;
        }catch(PDOException $error){
          $Conn_mxcomp->pdo->rollBack();
          $respuesta = "1";
          //$error_msg = "Error: " . $error->getMessage();
          $error_msg = "Ocurrió un problema al agregar el producto al carrito, intenta de nuevo.";
          $mensaje = "<span>" . $error_msg . "</span>";
        }
      }
    }else{
      foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $nombreAlmacen=>$datos_almacen){
        $almacen_unidades = (int) $datos_almacen['unidades'];

        if($almacen_unidades > 0 ){
          $numeroAlmacen = 0;

          foreach($Producto_almacenes as $datos_productoAlmacen){
            if($datos_productoAlmacen['nombre_almacen'] === $nombreAlmacen){
              $numeroAlmacen = (int) $datos_productoAlmacen['id_almacen'];
              $existenciaAlmacen = (int) $datos_productoAlmacen['existencia'];
              break;
            }
          }

          // SI NO EXISTE EL ARRAY DEL CARRITO o NO EXISTE LA LLAVE DEL ALMACEN
          if( !isset($_SESSION['__carrito__']) || !array_key_exists($nombreAlmacen, $_SESSION['__carrito__']) ){
            $_SESSION['__carrito__'][$nombreAlmacen][0] = [
              'codigoProducto' => $codigoProducto,
              'skuProveedor' => $Producto_skuProveedor,
              'descripcion' => $Producto_descripcion,
              'descripcionURL' => $Producto_descripcionURL,
              'nombreMarca' => $Producto_nombreMarca,
              'precioProveedor' => $Producto_precioProveedor,
              'monedaProveedor' => $Producto_monedaProveedor,
              'precioMXcomp' => $Producto_precioMXcomp,
              'monedaMXcomp' => $Producto_monedaMXcomp,
              'unidades' => $almacen_unidades,
              'numeroAlmacen' => $numeroAlmacen,
              'existenciaAlmacen' => $existenciaAlmacen,
              'existenciaTotalProducto' => $Producto_existenciaTotal,
              'nombreProveedor' => $Producto_nombreProveedor,
              'tieneImagen' => $Producto_tieneImagen,
              'numeroUbicacionImagen' => $Producto_numeroUbicacionImagen,
              'nombreImagen' => $Producto_nombreImagen,
              'versionImagen' => $Producto_versionImagen,
              'envioGratisPermitido' => $Producto_envioGratisPermitido,
              'tieneExistencias' => '1',
              'guardado' => '0',
              'registroExiste' => '1'
            ];
          }else{
            // Se obtienen los codigos de producto
            $array_codigosProducto = array_column($_SESSION['__carrito__'][$nombreAlmacen], 'codigoProducto');

            // Se pregunta si el codigo del producto ya se encuentra agregado, si es así sólo se suman las unidades
            if(in_array($codigoProducto, $array_codigosProducto)){
              foreach($_SESSION['__carrito__'][$nombreAlmacen] as $indice_3=>$datos_producto){
                if($datos_producto['codigoProducto'] === $codigoProducto){
                  $_SESSION['__carrito__'][$nombreAlmacen][$indice_3]['unidades'] += $almacen_unidades;
                  break;
                }
              }

            // Si no se encuentra agregado, se realiza lo siguiente
            }else{
              // Se ordenan los productos de cada almacén en caso de que se hallan eliminado algunos anteriormente
              foreach($_SESSION['__carrito__'] as $indice_4=>$productos_almacen){
                $_SESSION['__carrito__'][$indice_4] = array_values($productos_almacen);
              }

              // Ya que se ordenaron, se obtiene la posicion del nuevo producto en el carrito y se agrega
              $posicion = count($_SESSION['__carrito__'][$nombreAlmacen]);
              $_SESSION['__carrito__'][$nombreAlmacen][$posicion] = [
                'codigoProducto' => $codigoProducto,
                'skuProveedor' => $Producto_skuProveedor,
                'descripcion' => $Producto_descripcion,
                'descripcionURL' => $Producto_descripcionURL,
                'nombreMarca' => $Producto_nombreMarca,
                'precioProveedor' => $Producto_precioProveedor,
                'monedaProveedor' => $Producto_monedaProveedor,
                'precioMXcomp' => $Producto_precioMXcomp,
                'monedaMXcomp' => $Producto_monedaMXcomp,
                'unidades' => $almacen_unidades,
                'numeroAlmacen' => $numeroAlmacen,
                'existenciaAlmacen' => $existenciaAlmacen,
                'existenciaTotalProducto' => $Producto_existenciaTotal,
                'nombreProveedor' => $Producto_nombreProveedor,
                'tieneImagen' => $Producto_tieneImagen,
                'numeroUbicacionImagen' => $Producto_numeroUbicacionImagen,
                'nombreImagen' => $Producto_nombreImagen,
                'versionImagen' => $Producto_versionImagen,
                'envioGratisPermitido' => $Producto_envioGratisPermitido,
                'tieneExistencias' => '1',
                'guardado' => '0',
                'registroExiste' => '1'
              ];
            }
          }
        }
      }

      unset($_SESSION['__producto_unidades__']);

      $respuesta = "2";
      $mensaje = "Listo - SESSION carrito";
    }
  }
  
  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>