<?php
if(isset($_POST['accion']) && $_POST['accion'] === "cambiar"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

  $seccion = trim($_POST['seccion']);
  $id = trim($_POST['id']);
  $nombreAlmacen = trim($_POST['almacen']);
  $cantidad = trim($_POST['cantidad']);

  $Conn_mxcomp = new Conexion_mxcomp();
  $proceso_correcto = false;
  $mensaje = '';
  
  // REVISA EL ID
  if($id !== "" && validar_id_producto_carrito($id)){
    $id = (string) $id;
    $nombreAlmacen = desencriptar($nombreAlmacen);
    $proceso_correcto = true;
  }else{
    $respuesta = "1";
    $mensaje = "Existe un problema con el ID recibido.";
    $proceso_correcto = false;
  }
  
  // REVISA EL NOMBRE DEL ALMACÉN
  if($proceso_correcto){
    if($nombreAlmacen !== "" && validar_nombreAlmacen_carrito($nombreAlmacen)){
      $nombreAlmacen = (string) $nombreAlmacen;
      $codigoProducto = desencriptar(trim($_POST['codigo_producto']));
      $proceso_correcto = true;
    }else{
      $respuesta = "1";
      $mensaje = "Existe un problema con el nombre del almacén recibido.";
      $proceso_correcto = false;
    }
  }
  
  // REVISA EL CÓDIGO DEL PRODUCTO
  if($proceso_correcto){
    if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
      $codigoProducto = (string) $codigoProducto;
      $proceso_correcto = true;
    }else{
      $respuesta = "1";
      $mensaje = "Existe un problema con el código del producto.";
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA CANTIDAD
  if($proceso_correcto){
    if($cantidad !== "" && validar_campo_numerico($cantidad)){
      $cantidad = (int) $cantidad;
      $proceso_correcto = true;
    }else{
      $respuesta = "2";
      $proceso_correcto = false;
    }
  }
  
  // REALIZA LOS PROCESOS CORRESPONDIENTES
  if($proceso_correcto){
    $cantidad_generada = 0;
    
    switch($seccion){
      case "escrita":
        if($cantidad === 0)
          $cantidad = 1;
        
        $cantidad_generada = $cantidad;
        break;

      case "disminuir":
        $cantidad_generada = $cantidad - 1;
        break;

      case "aumentar":
        $cantidad_generada = $cantidad + 1;
        break;
    }

    if(isset($_SESSION['__id__'])){
      $idUsuario = desencriptar(trim($_SESSION['__id__']));
      $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

      // REVISA EL ID DEL UISUARIO
      if(validar_campo_numerico($idUsuario)){
        $idUsuario = (int) $idUsuario;
        $proceso_correcto = true;
      }else{
        $respuesta = "1";
        $mensaje = "Hay un problema al buscar al usuario.";
        $proceso_correcto = false;
      }

      // REALIZA EL PROCESO PARA CAMBIAR LA CANTIDAD DE LAS UNIDADES
      if($proceso_correcto){
        try{
          $Conn_mxcomp->pdo->beginTransaction();
          
          $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->execute();
          $usuario_existe = (int) $stmt->fetchColumn();

          if($usuario_existe === 1){
            $fechaActual = date("Y-m-d H:i:s");
            
            $sql = "SELECT COUNT(id) AS conteo, id FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND nombreAlmacen = :nombreAlmacen";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
            $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
            $stmt->execute();
            $datos_carrito = $stmt->fetch(PDO::FETCH_ASSOC);
            $producto_existe = (int) $datos_carrito['conteo'];
            $id_producto = (int) $datos_carrito['id'];

            if($producto_existe === 1){
              $sql = "UPDATE __carrito SET unidades = :unidades, fechaActualizacion = :fechaActualizacion WHERE id = :id AND idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND nombreAlmacen = :nombreAlmacen";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':unidades', $cantidad_generada, PDO::PARAM_INT);
              $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
              $stmt->bindParam(':id', $id_producto, PDO::PARAM_INT);
              $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
              $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
              $stmt->execute();
              
              $respuesta = "3";
            }else{
              $respuesta = "1";
              $mensaje = "El producto no existe en el carrito.";
            }

          }else{
            $respuesta = "1";
            $mensaje = "El usuario no existe.";
          }

          $stmt = null;
          $Conn_mxcomp->pdo->commit();
        }catch(PDOException $error){
          $Conn_mxcomp->pdo->rollBack();
          $respuesta = "1";
          //$mensaje = "Error: " . $error->getMessage();
          $mensaje = "Hay un problema al cambiar la cantidad del producto en el carrito.";
        }
      }
    }else{
      foreach($_SESSION['__carrito__'] as $nombreAlmacen_indice=>$productos_carritoAlmacen){
        if($nombreAlmacen === $nombreAlmacen_indice){
          foreach($productos_carritoAlmacen as $indice_1=>$informacion_producto){
            if($informacion_producto['codigoProducto'] === $codigoProducto){
              $_SESSION['__carrito__'][$nombreAlmacen_indice][$indice_1]['unidades'] = $cantidad_generada;
              break;
            }
          }
        }
      }

      $respuesta = "3";
    }
  }

  unset($_POST['accion']);
  unset($_POST['seccion']);
  unset($_POST['id']);
  unset($_POST['almacen']);
  unset($_POST['codigo_producto']);
  unset($_POST['cantidad']);

  $json = [ 'respuesta' => $respuesta, 'mensaje' => $mensaje ];
  echo json_encode($json);
}
?>