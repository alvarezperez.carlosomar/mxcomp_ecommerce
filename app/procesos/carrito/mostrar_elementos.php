<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  session_start();

  include dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  include dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  include dirname(__DIR__, 2) . '/global/config.php';
  include dirname(__DIR__, 2) . '/conn.php';

  date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO
  
  $Conn_mxcomp = new Conexion_mxcomp();
  $carrito_vacio = false;
  $cantidad_title = 0;
  $html_productos = '';
  $boton_resumen = '';
  
  $error_cantidad = 0;
  $costo_envio_estandar = COSTO_ENVIO_ESTANDAR;
  $total_compra_envioGratis = (float) TOTAL_COMPRA;
  
  /////////////////////////////////////////////////////////////////////////
  
  if(isset($_SESSION['__id__'])){
    $idUsuario = desencriptar(trim($_SESSION['__id__']));
    
    if(validar_campo_numerico($idUsuario)){
      $idUsuario = (int) $idUsuario;
      
      try{
        $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->execute();
        $usuario_existe = (int) $stmt->fetchColumn();
        
        if($usuario_existe === 1){
          try{
            unset($_SESSION['__carrito__']);
            
            $sql = "SELECT COUNT(id) FROM __carrito WHERE BINARY idUsuario = :idUsuario";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->execute();
            $productos_existen = (int) $stmt->fetchColumn();
            
            if($productos_existen > 0){
              $success_consulta = false;

              try{
                $Conn_mxcomp->pdo->beginTransaction();
                $fechaActual = date("Y-m-d H:i:s");

                $sql = "SELECT codigoProducto, unidades, numeroAlmacen, nombreAlmacen, nombreProveedor, guardado FROM __carrito WHERE BINARY idUsuario = :idUsuario";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                $stmt->execute();

                while($datos_carrito = $stmt->fetch(PDO::FETCH_ASSOC)){
                  $carrito_codigoProducto = (string) $datos_carrito['codigoProducto'];
                  $carrito_unidades = (int) $datos_carrito['unidades'];
                  $carrito_numeroAlmacen = (int) $datos_carrito['numeroAlmacen'];
                  $carrito_nombreAlmacen = (string) $datos_carrito['nombreAlmacen'];
                  $carrito_nombreProveedor = (string) $datos_carrito['nombreProveedor'];
                  $carrito_guardado = (string) $datos_carrito['guardado'];
                  
                  $sql = "SELECT COUNT(id) AS conteo, descripcion, descripcionURL, nombreMarca, precioProveedor, precioMXcomp, existenciaTotal, almacenes, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, envioGratisPermitido, activo FROM __productos WHERE BINARY codigoProducto = :codigoProducto AND nombreProveedor = :nombreProveedor";
                  $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
                  $stmt_temporal->bindParam(':codigoProducto', $carrito_codigoProducto, PDO::PARAM_STR);
                  $stmt_temporal->bindParam(':nombreProveedor', $carrito_nombreProveedor, PDO::PARAM_STR);
                  $stmt_temporal->execute();
                  $datos_producto = $stmt_temporal->fetch(PDO::FETCH_ASSOC);
                  
                  if((int) $datos_producto['conteo'] === 1){
                    $tieneExistencias = '1';
                    
                    $producto_descripcion = (string) trim($datos_producto['descripcion'], " \xC2\xA0");
                    $producto_descripcionURL = (string) trim($datos_producto['descripcionURL'], " \xC2\xA0");
                    $producto_nombreMarca = (string) trim($datos_producto['nombreMarca']);
                    $producto_precioProveedor = (string) trim($datos_producto['precioProveedor']);
                    $producto_precioMXcomp = (string) trim($datos_producto['precioMXcomp']);
                    $producto_existenciaTotal = (int) trim($datos_producto['existenciaTotal']);
                    $producto_almacenes = is_null($datos_producto['almacenes']) ? NULL : json_decode(trim($datos_producto['almacenes']), true);
                    $producto_tieneImagen = (string) trim($datos_producto['tieneImagen']);
                    $producto_numeroUbicacionImagen = $datos_producto['numeroUbicacionImagen'];
                    $producto_nombreImagen = (string) trim($datos_producto['nombreImagen']);
                    $producto_versionImagen = $datos_producto['versionImagen'];
                    $producto_envioGratisPermitido = (string) trim($datos_producto['envioGratisPermitido']);
                    $producto_activo = (int) trim($datos_producto['activo']);

                    $producto_numeroUbicacionImagen = is_null($producto_numeroUbicacionImagen) ? NULL : trim($producto_numeroUbicacionImagen);
                    $producto_versionImagen = is_null($producto_versionImagen) ? NULL : trim($producto_versionImagen);
                    
                    $carrito_unidades = $carrito_unidades === 0 ? 1 : $carrito_unidades;
                    
                    $producto_existenciaAlmacen = 0;
                    if(!is_null($producto_almacenes)){
                      foreach($producto_almacenes as $datos_almacen){
                        if($datos_almacen['nombre_almacen'] === $carrito_nombreAlmacen){
                          $producto_existenciaAlmacen = (int) $datos_almacen['existencia'];
                          break;
                        } 
                      }
                    }
                    
                    $registroExiste = $producto_activo === 0 ? '0' : '1';

                    if($producto_activo === 0 || ( $producto_activo === 1 && ( $producto_existenciaTotal === 0 || $producto_existenciaAlmacen === 0 ) ) ){
                      $tieneExistencias = '0';
                      $carrito_guardado = '0';
                      $carrito_unidades = 0;
                    }
                    
                    $sql = "UPDATE __carrito SET descripcion = :descripcion, descripcionURL = :descripcionURL, nombreMarca = :nombreMarca, precioProveedor = :precioProveedor, precioMXcomp = :precioMXcomp, unidades = :unidades, existenciaAlmacen = :existenciaAlmacen, existenciaTotalProducto = :existenciaTotalProducto, tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, nombreImagen = :nombreImagen, versionImagen = :versionImagen, envioGratisPermitido = :envioGratisPermitido, tieneExistencias = :tieneExistencias, guardado = :guardado, registroExiste = :registroExiste, fechaActualizacion = :fechaActualizacion WHERE BINARY idUsuario = :idUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
                    $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
                    $stmt_temporal->bindParam(':descripcion', $producto_descripcion, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':descripcionURL', $producto_descripcionURL, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreMarca', $producto_nombreMarca, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':precioProveedor', $producto_precioProveedor, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':precioMXcomp', $producto_precioMXcomp, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':unidades', $carrito_unidades, PDO::PARAM_INT);
                    $stmt_temporal->bindParam(':existenciaAlmacen', $producto_existenciaAlmacen, PDO::PARAM_INT);
                    $stmt_temporal->bindParam(':existenciaTotalProducto', $producto_existenciaTotal, PDO::PARAM_INT);
                    $stmt_temporal->bindParam(':tieneImagen', $producto_tieneImagen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':numeroUbicacionImagen', $producto_numeroUbicacionImagen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreImagen', $producto_nombreImagen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':versionImagen', $producto_versionImagen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':envioGratisPermitido', $producto_envioGratisPermitido, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':guardado', $carrito_guardado, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                    $stmt_temporal->bindParam(':codigoProducto', $carrito_codigoProducto, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':numeroAlmacen', $carrito_numeroAlmacen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreAlmacen', $carrito_nombreAlmacen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreProveedor', $carrito_nombreProveedor, PDO::PARAM_STR);
                    $stmt_temporal->execute();
                  }else{
                    $registroExiste = '0';
                    
                    $sql = "UPDATE __carrito SET precioProveedor = NULL, monedaProveedor = NULL, precioMXcomp = NULL, monedaMXcomp = NULL, unidades = NULL, existenciaAlmacen = NULL, existenciaTotalProducto = NULL, envioGratisPermitido = NULL, registroExiste = :registroExiste, fechaActualizacion = :fechaActualizacion WHERE BINARY idUsuario = :idUsuario AND codigoProducto = :codigoProducto AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
                    $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
                    $stmt_temporal->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                    $stmt_temporal->bindParam(':codigoProducto', $carrito_codigoProducto, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':numeroAlmacen', $carrito_numeroAlmacen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreAlmacen', $carrito_nombreAlmacen, PDO::PARAM_STR);
                    $stmt_temporal->bindParam(':nombreProveedor', $carrito_nombreProveedor, PDO::PARAM_STR);
                    $stmt_temporal->execute();
                  }
                }
                
                $stmt_temporal = null;
                $success_consulta = true;
                $Conn_mxcomp->pdo->commit();
              }catch(PDOException $error){
                $Conn_mxcomp->pdo->rollback();
                $respuesta = "2";
                //$error_msg = "Error: " . $error->getMessage();
                $error_msg = "Hay un problema al actualizar un producto del carrito.";

                $html_productos = '
      <p class="p-notification p-notification_letter_error">
        <span><b>' . $error_msg . '</b></span>
      </p>';
              }
              
              if($success_consulta){
                try{
                  $sql = "SELECT codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, unidades, numeroAlmacen, nombreAlmacen, existenciaAlmacen, existenciaTotalProducto, nombreProveedor, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, envioGratisPermitido, tieneExistencias, guardado, registroExiste FROM __carrito WHERE BINARY idUsuario = :idUsuario";
                  $stmt = $Conn_mxcomp->pdo->prepare($sql);
                  $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
                  $stmt->execute();
                  
                  while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $producto_codigoProducto = (string) trim($datos_producto['codigoProducto']);
                    $producto_skuProveedor = (string) trim($datos_producto['skuProveedor']);
                    $producto_descripcion = (string) trim($datos_producto['descripcion']);
                    $producto_descripcionURL = (string) trim($datos_producto['descripcionURL']);
                    $producto_nombreMarca = (string) trim($datos_producto['nombreMarca']);
                    $producto_precioProveedor = (float) trim($datos_producto['precioProveedor']);
                    $producto_monedaProveedor = (string) trim($datos_producto['monedaProveedor']);
                    $producto_precioMXcomp = (float) trim($datos_producto['precioMXcomp']);
                    $producto_monedaMXcomp = (string) trim($datos_producto['monedaMXcomp']);
                    $producto_unidades = (int) trim($datos_producto['unidades']);
                    $producto_numeroAlmacen = (string) trim($datos_producto['numeroAlmacen']);
                    $producto_nombreAlmacen = (string) trim($datos_producto['nombreAlmacen']);
                    $producto_existenciaAlmacen = (int) trim($datos_producto['existenciaAlmacen']);
                    $producto_existenciaTotalProducto = (int) trim($datos_producto['existenciaTotalProducto']);
                    $producto_nombreProveedor = (string) trim($datos_producto['nombreProveedor']);
                    $producto_tieneImagen = (string) trim($datos_producto['tieneImagen']);
                    $producto_numeroUbicacionImagen = (string) trim($datos_producto['numeroUbicacionImagen']);
                    $producto_nombreImagen = (string) trim($datos_producto['nombreImagen']);
                    $producto_versionImagen = (string) trim($datos_producto['versionImagen']);
                    $producto_envioGratisPermitido = (string) trim($datos_producto['envioGratisPermitido']);
                    $producto_tieneExistencias = (string) trim($datos_producto['tieneExistencias']);
                    $producto_guardado = (string) trim($datos_producto['guardado']);
                    $producto_registroExiste = (string) trim($datos_producto['registroExiste']);

                    if( !isset($_SESSION['__carrito__']) || !array_key_exists($producto_nombreAlmacen, $_SESSION['__carrito__']) ){
                      $_SESSION['__carrito__'][$producto_nombreAlmacen][0] = [
                        'codigoProducto' => $producto_codigoProducto,
                        'skuProveedor' => $producto_skuProveedor,
                        'descripcion' => $producto_descripcion,
                        'descripcionURL' => $producto_descripcionURL,
                        'nombreMarca' => $producto_nombreMarca,
                        'precioProveedor' => $producto_precioProveedor,
                        'monedaProveedor' => $producto_monedaProveedor,
                        'precioMXcomp' => $producto_precioMXcomp,
                        'monedaMXcomp' => $producto_monedaMXcomp,
                        'unidades' => $producto_unidades,
                        'numeroAlmacen' => $producto_numeroAlmacen,
                        'existenciaAlmacen' => $producto_existenciaAlmacen,
                        'existenciaTotalProducto' => $producto_existenciaTotalProducto,
                        'nombreProveedor' => $producto_nombreProveedor,
                        'tieneImagen' => $producto_tieneImagen,
                        'numeroUbicacionImagen' => $producto_numeroUbicacionImagen,
                        'nombreImagen' => $producto_nombreImagen,
                        'versionImagen' => $producto_versionImagen,
                        'envioGratisPermitido' => $producto_envioGratisPermitido,
                        'tieneExistencias' => $producto_tieneExistencias,
                        'guardado' => $producto_guardado,
                        'registroExiste' => $producto_registroExiste
                      ];
                    }else{
                      $posicion = count($_SESSION['__carrito__'][$producto_nombreAlmacen]);
                      $_SESSION['__carrito__'][$producto_nombreAlmacen][$posicion] = [
                        'codigoProducto' => $producto_codigoProducto,
                        'skuProveedor' => $producto_skuProveedor,
                        'descripcion' => $producto_descripcion,
                        'descripcionURL' => $producto_descripcionURL,
                        'nombreMarca' => $producto_nombreMarca,
                        'precioProveedor' => $producto_precioProveedor,
                        'monedaProveedor' => $producto_monedaProveedor,
                        'precioMXcomp' => $producto_precioMXcomp,
                        'monedaMXcomp' => $producto_monedaMXcomp,
                        'unidades' => $producto_unidades,
                        'numeroAlmacen' => $producto_numeroAlmacen,
                        'existenciaAlmacen' => $producto_existenciaAlmacen,
                        'existenciaTotalProducto' => $producto_existenciaTotalProducto,
                        'nombreProveedor' => $producto_nombreProveedor,
                        'tieneImagen' => $producto_tieneImagen,
                        'numeroUbicacionImagen' => $producto_numeroUbicacionImagen,
                        'nombreImagen' => $producto_nombreImagen,
                        'versionImagen' => $producto_versionImagen,
                        'envioGratisPermitido' => $producto_envioGratisPermitido,
                        'tieneExistencias' => $producto_tieneExistencias,
                        'guardado' => $producto_guardado,
                        'registroExiste' => $producto_registroExiste
                      ];
                    }
                  }
                  
                  $respuesta = "1";
                }catch(PDOException $error){
                  $respuesta = "2";
                  //$error_msg = "Error: " . $error->getMessage();
                  $error_msg = "Hay un problema al mostrar los productos del carrito.";

                  $html_productos = '
      <p class="p-notification p-notification_letter_error">
        <span><b>' . $error_msg . '</b></span>
      </p>';
                }
              }
            }
          }catch(PDOException $error){
            $respuesta = "2";
            //$error_msg = "Error: " . $error->getMessage();
            $error_msg = "Hay un problema al buscar los productos en el carrito.";

            $html_productos = '
      <p class="p-notification p-notification_letter_error">
        <span><b>' . $error_msg . '</b></span>
      </p>';
          }
        }else{
          $respuesta = "2";
          $html_productos = '
      <p class="p-notification p-notification_letter_info">
        <span><b>Tu usuario no fué encontrado.</b></span>
      </p>';
        }
        
        $stmt = null;
      }catch(PDOException $error){
        $respuesta = "2";
        //$error_msg = "Error: " . $error->getMessage();
        $error_msg = "Hay un problema al buscar al usuario.";
        
        $html_productos = '
      <p class="p-notification p-notification_letter_error">
        <span><b>' . $error_msg . '</b></span>
      </p>';
      }
    }else{
      $respuesta = "2";
      $html_productos = '
      <p class="p-notification p-notification_letter_error">
        <span><b>Hay un problema al buscar al usuario.</b></span>
      </p>';
    }
  }

  /////////////////////////////////////////////////////////////////////////

  //// SI LA VARIABLE DE SESION DEL CARRITO, EL CARRITO TIENE PRODUCTOS Y SE MUESTRAN
  if( isset($_SESSION['__carrito__']) ){
    $respuesta = "1";
    $_SESSION['__carrito_variables_almacen__'] = [];
    $id_almacen_consecutivo = 1;
    
    foreach($_SESSION['__carrito__'] as $nombreAlmacen=>$productos_carritoAlmacen){
      $id_generado = 1;

      $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen] = [
        'total_unidades' => 0,
        'total_pagar' => 0.00,
        'envioGratis_permitido' => 0,
        'productos_guardados' => 0
      ];

      // SE GENERAN LAS VARIABLES QUE SE USARÁN EN EL RESUMEN DEL CARRITO, POR ALMACÉN
      foreach($productos_carritoAlmacen as $informacion_producto){
        // NO SE ENCUENTRA GUARDADO
        if((string) $informacion_producto['guardado'] === '0' && (string) $informacion_producto['tieneExistencias'] === '1'){
          $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['total_unidades'] += (int) $informacion_producto['unidades'];
          
          $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['total_pagar'] += ($informacion_producto['precioMXcomp'] * $informacion_producto['unidades']);
          $cantidad_title += (int) $informacion_producto['unidades'];

          if((int) $informacion_producto['envioGratisPermitido'] === 0){
            $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['envioGratis_permitido']++;
          }
        }
      }

      $suma_unidadesAlmacen = (int) $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['total_unidades'];
      
      $html_productos .= '
      <div class="p-acordeon-cabecera p-acordeon-cabecera_success g-acordeon_cabecera p-acordeon-cabecera_activa" data-seccion="' . $id_almacen_consecutivo . '">
        <h3>Almacén ' . ucwords(mb_strtolower($nombreAlmacen)) . ' <span>(' . $suma_unidadesAlmacen . ')</span></h3>
      </div>
      <div class="p-acordeon-contenido_contenedor" data-contenido="' . $id_almacen_consecutivo . '" style="display: block">';

      foreach($productos_carritoAlmacen as $informacion_producto){
        $Producto_codigoProducto = trim($informacion_producto['codigoProducto']);
        $Producto_codigoProducto_encriptado = (string) encriptar($Producto_codigoProducto);
        $Producto_guardado = (string) $informacion_producto['guardado'];
        
        $Producto_existenciaAlmacen = (int) $informacion_producto['existenciaAlmacen'];
        $Producto_unidades = (int) $informacion_producto['unidades'];
        $cantidadMaxima_reducir = (int) $Producto_existenciaAlmacen + 1;
        
        $Producto_precioMXcomp = trim($informacion_producto['precioMXcomp']);
        $Producto_monedaMXcomp = trim($informacion_producto['monedaMXcomp']);
        $Producto_tieneExistencias = (string) trim($informacion_producto['tieneExistencias']);
        $Producto_registroExiste = (string) trim($informacion_producto['registroExiste']);
  
        // SE CUENTAN LOS PRODUCTOS GUARDADOS
        if($Producto_guardado === '1'){
          $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['productos_guardados'] += $Producto_unidades;
        }

        // SE OBTIENEN SI HAY UN ERROR EN UNA CANTIDAD
        if($Producto_unidades > $Producto_existenciaAlmacen && $Producto_guardado === '0'){
          $error_cantidad++;
        }
  
        $Producto_tieneImagen = (int) trim($informacion_producto['tieneImagen']);
        $Producto_numeroUbicacionImagen = (int) trim($informacion_producto['numeroUbicacionImagen']);
        $Producto_nombreMarca = trim($informacion_producto['nombreMarca']);
        $Producto_nombreImagen = trim($informacion_producto['nombreImagen']);
        $Producto_versionImagen = trim($informacion_producto['versionImagen']);
  
        // SI ES NULL NO TIENE UBICACION LA IMAGEN Y NO EXISTE
        if($Producto_numeroUbicacionImagen === 0 && $Producto_tieneImagen === 0){
          $Producto_imagen = 'images/no_imagen_disponible.png';
        }else{
          $Producto_numeroUbicacionImagen = (int) $Producto_numeroUbicacionImagen;
  
          switch($Producto_numeroUbicacionImagen){
            case 1: // UBICACION NUEVA
              $Producto_imagen = 'images/imagenes_productos/' . $Producto_nombreMarca . '/' . $Producto_nombreImagen . '.jpg';
              break;
          }
        }
  
        $Producto_descripcion = trim($informacion_producto['descripcion'], " \xC2\xA0");
        $Producto_descripcion_editada = str_replace('"', '', $Producto_descripcion);
        $Producto_descripcionURL = trim($informacion_producto['descripcionURL']);
        
        $Producto_descripcion_acortada = $Producto_descripcion;
  
        $Producto_alt = ucwords(strtolower( $Producto_descripcion_editada . ', ' . str_replace('"', '', $Producto_nombreMarca) ));
        $Producto_title = "title='" . $Producto_descripcion . "'";

        $Producto_dataID = $id_almacen_consecutivo .  '_' . $id_generado;
        
        $html_productos .= '
        <div class="p-box-productos_contenedor_elementos" id="id-carrito-product_' . $Producto_dataID . '">';
  
        // SE ENCUENTRA GUARDADO EL PRODUCTO
        $html_productos .= ($Producto_guardado === '1') ? 
          '<div class="p-box-productos_title p-carrito-div_product_titulo_opacity" id="id-carrito-product_titulo_' . $Producto_dataID . '">' :
          '<div class="p-box-productos_title" id="id-carrito-product_titulo_' . $Producto_dataID . '">';

        $html_productos .= '
            <input type="hidden" id="id-carrito-codigoProducto_' . $Producto_dataID . '" value="' . $Producto_codigoProducto_encriptado . '" autocomplete="off">
            <div class="p-box-productos_title_image">
              <picture>
                <!-- <source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                <img src="' . $Producto_imagen . '?v=' . $Producto_versionImagen . '" alt="' . $Producto_alt . '" style="width: 40px; min-width: 40px;">
              </picture>
            </div>
            <h3 class="p-box-productos_title_h3 p-carrito-box_productos_title_h3" ' . $Producto_title . ' style="white-space: nowrap; line-height: 40px; height: 40px; overflow: hidden; text-overflow: ellipsis;">' . $Producto_descripcion_acortada . '</h3>
            <div class="p-carrito-product_contenedor_elementos_opciones">
              <a class="p-carrito-titulo_opciones g-carrito-product_opciones_button" id="id-carrito-product_opciones_' . $Producto_dataID . '" title="Más opciones...">
                <span>
                  <i class="fas fa-ellipsis-v"></i>
                </span>
              </a>
              <div class="p-carrito-product_opciones_fondo g-carrito-product_opciones_fondo" id="id-carrito-product_opciones_' . $Producto_dataID . '_fondo"></div>
              <div class="p-carrito-product_contenedor_links_opciones id-carrito-product_opciones_' . $Producto_dataID . '">';
  
        // SI TIENE EXISTENCIAS, SE MUESTRA ESTO
        if($Producto_tieneExistencias === '1'){
          $guardado_opcion = $Producto_guardado === '1' ? 'data-opcion="no_guardar"' : 'data-opcion="guardar"';
          $guardado_icono = $Producto_guardado === '1' ? 'fa-cart-plus' : 'fa-save';
          $guardado_texto_boton = $Producto_guardado === '1' ? 'Agregar al carrito' : 'Guardar para después';
          $guardado_texto_alert = $Producto_guardado === '1' ? 'No es posible agregar el producto. Se recargará la página, después vuelve a intentarlo.' : 'No es posible guardar el producto. Se recargará la página, después vuelve a intentarlo.';

          $html_productos .= '
                <a class="p-carrito-product_link_opcion_opciones g-carrito-producto_guardado" ' . $guardado_opcion . ' data-id="' . $Producto_dataID . '" data-almacen="' . encriptar($nombreAlmacen) . '">
                  <span>
                    <i class="fas fa-fw ' . $guardado_icono . '"></i>
                  </span>
                  <span><b>' . $guardado_texto_boton . '</b></span>
                </a>
                <p class="p-text p-text_error">
                  <span>' . $guardado_texto_alert . '</span>
                </p>';
        }
  
        if($Producto_registroExiste === '1'){
          $html_productos .= '
                <a href="' . $Producto_codigoProducto . '/' . $Producto_descripcionURL . '/" class="p-carrito-product_link_opcion_opciones">
                  <span>
                    <i class="fas fa-fw fa-eye"></i>
                  </span>
                  <span><b>Ver producto</b></span>
                </a>';
        }
        
        $html_productos .= '
                <a class="p-carrito-product_link_opcion_opciones g-carrito-eliminar_producto" data-id="' . $Producto_dataID . '" data-almacen="' . encriptar($nombreAlmacen) . '">
                  <span>
                    <i class="fas fa-fw fa-trash-alt"></i>
                  </span>
                  <span><b>Eliminar</b></span>
                </a>
                <p class="p-text p-text_error">
                  <span>No es posible eliminar el producto. Se recargará la página, después vuelve a intentarlo.</span>
                </p>
              </div>
            </div>
          </div>';

        // SI EL PRODUCTO ESTÁ DESACTIVADO, SE MUESTRA ESTO
        if($Producto_registroExiste === '0'){
          $html_productos .= '
          <div class="p-box-productos_info">
            <div class="p-box-productos_info_div_3">
              <label class="p-label p-producto-p_avisoCarrito p-producto-cantidad_cantidad_noDisp">Este producto ya no se encuentra en nuestro catálogo.</label>
            </div>
          </div>';
        }else if($Producto_tieneExistencias === '0'){
          // SI NO TIENE EXISTENCIAS, SE MUESTRA ESTO
          $html_productos .= '
          <div class="p-box-productos_info" id="id-carrito-product_info_' . $Producto_dataID . '">
            <div class="p-box-productos_info_div_3">
              <label class="p-label p-producto-p_avisoCarrito">No se cuenta con productos por el momento en este almacén.</label>
              
              <div class="p-buttons p-producto-buttons_contenedor">
                <a href="atencion-a-clientes" class="p-button p-button_success">
                  <span>
                    <i class="fas fa-clipboard"></i>
                  </span>
                  <span><b>Solicitar existencia</b></span>
                </a>
              </div>
            </div>
          </div>';
        }else{
          // SE ENCUENTRA GUARDADO EL PRODUCTO
          $html_productos .= $Producto_guardado === '1' ? (
          '<div class="p-box-productos_info p-carrito-div_product_info_opacity" id="id-carrito-product_info_' . $Producto_dataID . '" style="display: none;">'
          ) : (
          '<div class="p-box-productos_info" id="id-carrito-product_info_' . $Producto_dataID . '">'
          );

          // GENERAR OPCIONES DEL BOTÓN DISMINUIR
          if( $Producto_guardado === '0' && ( ($Producto_unidades > 1 && $Producto_unidades <= $Producto_existenciaAlmacen) || ($Producto_unidades === $cantidadMaxima_reducir) ) ){
            $boton_disminuir = ' g-carrito-cambiar_cantidad_boton" data-seccion="disminuir" data-id="' . $Producto_dataID . '" data-almacen="' . encriptar($nombreAlmacen) . '"';
          }else{
            $boton_disminuir = '" disabled';
          }

          // GENERAR OPCIONES DEL INPUT CANTIDAD Y SU LOADING
          if($Producto_guardado === '0'){
            $input_cantidad = ' g-carrito-cambiar_cantidad_input" data-seccion="escrita" data-id="' . $Producto_dataID . '" data-almacen="' . encriptar($nombreAlmacen) . '"';
            $loading_cantidad = ' g-carrito-cantidad_loading" data-id="' . $Producto_dataID . '"';
          }else{
            $input_cantidad = '" disabled';
            $loading_cantidad = '"';
          }

          // GENERAR OPCIONES DEL BOTÓN AUMENTAR
          if( ($Producto_guardado === '0') && ($Producto_unidades < $Producto_existenciaAlmacen) ){
            $boton_aumentar = ' g-carrito-cambiar_cantidad_boton" data-seccion="aumentar" data-id="' . $Producto_dataID . '" data-almacen="' . encriptar($nombreAlmacen) . '"';
          }else{
            $boton_aumentar = '" disabled';
          }

          $html_productos .= '
            <div class="p-box-productos_info_div_1">
              <div class="p-producto-cantidadAlmacenes_campo_cantidad">
                <div>
                  <button class="p-button p-button_account p-button_square p-button_cantidad_square' . $boton_disminuir . '>
                    <span>
                      <i class="fas fa-minus"></i>
                    </span>
                  </button>

                  <div class="p-cantidadAlmacen_contenedor">
                    <input type="text" class="p-input p-producto-cantidadAlmacenes_input' . $input_cantidad . ' value="' . $Producto_unidades . '" autocomplete="off">
                    <div class="p-loading-general p-cantidadAlmacen_loading' . $loading_cantidad . '>
                      <div></div>
                    </div>
                  </div>

                  <button class="p-button p-button_account p-button_square p-button_cantidad_square' . $boton_aumentar . '>
                    <span>
                      <i class="fas fa-plus"></i>
                    </span>
                  </button>
                </div>
                <label class="p-label p-producto-cantidadAlmacenes_label">' . ($Producto_unidades > 1 ? 'unidades' : 'unidad') . '</label>
              </div>';
  
          if($Producto_unidades > $Producto_existenciaAlmacen){
            $html_productos .= '
              <p class="p-producto-cantidad_cantidad_noDisp">
                <b>Cantidad no disponible.</b>
              </p>';
          }
  
          $html_productos .= '
              <p class="p-producto-cantidad_cantidad_disp">';
  
          $html_productos .= ($Producto_existenciaAlmacen > 1) ? 
                '<b>' . $Producto_existenciaAlmacen . ' unidades disponibles</b>' : 
                '<b>' . $Producto_existenciaAlmacen . ' unidad disponible</b>';

          $html_productos .= '
              </p>
            </div>

            <div class="p-box-productos_info_div_2">
              <p class="p-producto-p_precio_unitario_color">
                <span>Precio unitario:</span>
                <span>$ ' . number_format($Producto_precioMXcomp, 2, '.', ',') . '</span>
                <span>' . $Producto_monedaMXcomp . '</span>
              </p>';
  
          if($Producto_unidades > $Producto_existenciaAlmacen){
            $html_productos .= '';
          }else{
            $html_productos .= '
              <p class="p-producto-p_precio_total_color">
                <span>Total:</span>
                <span>$ ' . number_format($Producto_precioMXcomp * $Producto_unidades, 2, '.', ',') . '</span>
                <span>' . $Producto_monedaMXcomp . '</span>
              </p>';
          }
  
          $html_productos .= '
            </div>
          </div>';
        }
  
        $html_productos .= '
        </div>';
  
        $id_generado ++;
      }
  
      $html_productos .= '
      </div>';

      $id_almacen_consecutivo++;
    }
  
  //// SI NO EXISTEN LA VARIABLES DE SESION DEL CARRITO, EL CARRITO ESTÁ VACÍO Y LA RESPUESTA CAMBIA
  }else{
    $respuesta = "2";
    $html_productos .= '
      <div class="p-carrito-contenedor_no_productos">
        <img src="images/carrito_no_productos.png" alt="No hay productos en carrito.">
      </div>';

    $carrito_vacio = true;
  }
  
  ////////////////////////////////////   GENERAR RESUMEN DEL PEDIDO   ////////////////////////////////////

  if($carrito_vacio){
    $html_resumen = '
      <div class="p-carrito-div_fijo_contenedor">
        <div class="p-carrito-resumen_contenedor_contenido_superior">
          <h3>
            <span>Resumen</span>

            <a class="p-carrito-modal_cerrar g-carrito-resumen_cerrar">
              <span>
                <i class="fas fa-times"></i>
              </span>
            </a>
          </h3>
        </div>

        <div class="p-carrito-resumen_contenedor_contenido_medio">
          <p class="p-text p-producto-cantidad_cantidad_noDisp" style="color: #0F9C8F; font-family: Ubuntu, sans-serif;">No hay productos agregados en tu carrito.</p>
        </div>

        <div class="p-carrito-resumen_contenedor_contenido_inferior">
          <div>
            <p>
              <span>Productos (0):</span>
              <span>$ 0.00</span>
            </p>
            <p>
              <span>Envío total:</span>
              <span>$ 0.00</span>
            </p>
            <p>
              <span class="p-carrito-total_pagar">Total a pagar:</span>
              <span class="p-carrito-total_pagar">$ 0.00</span>
            </p>
          </div>
          <div class="p-buttons">
            <a class="p-button p-button_success" disabled>
              <span>
                <i class="fas fa-dollar-sign"></i>
              </span>
              <span><b>Continuar con la compra</b></span>
            </a>
          </div>
        </div>
      </div>';
    
    $boton_miCarrito = "0";
    $boton_resumen = '';
  }else{
    $boton_resumen = '
      <a class="p-button p-button_detalles p-carrito-resumen_button" id="id-carrito-resumen_button">
        <span>
          <i class="fas fa-list-ol"></i>
        </span>
        <span><b>Ver resumen</b></span>
      </a>';
    
    if($error_cantidad === 0){
      $costo_envio_total = 0.00;
      $resumenCarrito_total_unidades = (int) array_sum(array_column($_SESSION['__carrito_variables_almacen__'], 'total_unidades'));
      $resumenCarrito_total_pagar = (float) array_sum(array_column($_SESSION['__carrito_variables_almacen__'], 'total_pagar'));

      $html_resumen = '
      <div class="p-carrito-resumen_contenedor_contenido_superior">
        <h3>
          <span>Resumen</span>

          <a class="p-carrito-modal_cerrar g-carrito-resumen_cerrar">
            <span>
              <i class="fas fa-times"></i>
            </span>
          </a>
        </h3>
      </div>
      <div class="p-carrito-resumen_contenedor_contenido_medio">';
      
      $html_resumen .= $resumenCarrito_total_unidades === 0 ?
        '<p class="p-text p-producto-cantidad_cantidad_noDisp" style="color: #0F9C8F; font-family: Ubuntu, sans-serif;">No hay productos disponibles para comprar.</p>' : 
        '<p class="p-text p-text_carrito p-text_carrito_azul">Cada almacén tiene un costo de envío de $ ' . $costo_envio_estandar . ' + IVA. El "Envío total" es la suma de cada almacén. La moneda es MXN.</p>';

      foreach($_SESSION['__carrito_variables_almacen__'] as $nombreAlmacen=>$variables_almacen){
        $total_unidades_almacen = (int) $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['total_unidades'];
        $total_pagar_almacen = (float) $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['total_pagar'];
        $envioGratis_permitido_almacen = (int) $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['envioGratis_permitido'];
        $productos_guardados_almacen = (int) $_SESSION['__carrito_variables_almacen__'][$nombreAlmacen]['productos_guardados'];

        if($total_unidades_almacen === 0){
          $html_resumen .= '';
        }else{
          $html_resumen .= '
        <h4>Almacén ' . ucwords(mb_strtolower($nombreAlmacen)) . '</h4>
        <div>';

          if($productos_guardados_almacen > 0){
            $html_resumen .= $productos_guardados_almacen === 1 ? 
          '<small class="p-producto-cantidad_cantidad_noDisp" style="color: #1A5276;">
            <b style="font-family: Ubuntu, sans-serif;">* Guardaste 1 producto.</b>
          </small>' :
          '<small class="p-producto-cantidad_cantidad_noDisp" style="color: #1A5276;">
            <b style="font-family: Ubuntu, sans-serif;">* Guardaste ' . $productos_guardados_almacen . ' productos.</b>
          </small>';
          }
        
          $html_resumen .= '
          <p>
            <span>Total (' . ( $total_unidades_almacen === 1 ? $total_unidades_almacen . ' producto' : $total_unidades_almacen . ' productos' ) . '):</span>
            <span>$ ' . number_format($total_pagar_almacen, 2) . '</span>
          </p>';
    
          if($total_pagar_almacen > $total_compra_envioGratis){
            if($envioGratis_permitido_almacen > 0){
              $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);

              $html_resumen .= '
          <p class="p-text p-text_carrito p-text_carrito_azul p-margin-top_1rem">Se cobra el costo del envío porque ' . $envioGratis_permitido_almacen . ' producto(s) no permite el envío gratis, apesar de que el total a pagar supera los $ ' . number_format($total_compra_envioGratis, 2, '.', ',') . '.</p>
              
          <p>
            <span>Costo del envío:</span>
            <span>$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . '</span>
          </p>';
            }else{
              $html_resumen .= '
          <p class="p-text p-text_carrito p-text_carrito_verde">¡Tu envío es gratis! El total supera los $ ' . number_format($total_compra_envioGratis, 2, '.', ',') . '</p>';
            }
          }else{
            $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);
            
            $html_resumen .= '
          <p>
            <span>Costo del envío:</span>
            <span>$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . '</span>
          </p>';
          }

          $html_resumen .= '
        </div>';
        }
      }
      
      $texto_envio_total = $costo_envio_total === 0.00 ? ( $resumenCarrito_total_unidades === 0 ? "$ 0.00" : "Envío gratis" ) : '$ ' . number_format($costo_envio_total, 2);
      
      $html_resumen .= '
      </div>
      <div class="p-carrito-resumen_contenedor_contenido_inferior">
        <div>
          <p>
            <span>Productos (' . $resumenCarrito_total_unidades . '):</span>
            <span>$ ' . number_format($resumenCarrito_total_pagar, 2, '.', ',') . '</span>
          </p>
          <p>
            <span>Envío total:</span>
            <span>' . $texto_envio_total . '</span>
          </p>
          <p>
            <span class="p-carrito-total_pagar">Total a pagar:</span>
            <span class="p-carrito-total_pagar">$ ' . number_format($resumenCarrito_total_pagar + $costo_envio_total, 2, '.', ',') . '</span>
          </p>
        </div>
        <div class="p-buttons">';
      
      if($resumenCarrito_total_pagar === 0 || $resumenCarrito_total_unidades === 0){
        $html_resumen .= '
          <a class="p-button p-button_success" disabled>
            <span>
              <i class="fas fa-dollar-sign"></i>
            </span>
            <span><b>Continuar con la compra</b></span>
          </a>';
      }else{
        $html_resumen .= '
          <a href="comprar/2" class="p-button p-button_success">
            <span>
              <i class="fas fa-dollar-sign"></i>
            </span>
            <span><b>Continuar con la compra</b></span>
          </a>';
      }
      
      $html_resumen .= '
        </div>
      </div>';
      
      if((int) $cantidad_title >= 10){
        $cantidad_title = "9+";
      }
      
      $boton_miCarrito = (string) $cantidad_title;
    }else{
      $texto_cantidad = $error_cantidad === 1 ? $error_cantidad . " producto" : $error_cantidad . " productos";
      $texto_problema = $error_cantidad === 1 ? 'PROBLEMA EN UNA CANTIDAD' : 'PROBLEMA EN VARIAS CANTIDADES';
      
      $html_resumen = '
        <div class="p-carrito-resumen_contenedor_contenido_superior">
          <h3>
            <span>Resumen</span>

            <a class="p-carrito-modal_cerrar g-carrito-resumen_cerrar">
              <span>
                <i class="fas fa-times"></i>
              </span>
            </a>
          </h3>
          <div class="p-notification p-notification_direction_column p-notification_letter_info p-margin-top_2rem" style="width: 90%; margin: auto;">
            <h4 class="p-text-align_center">' . $texto_problema . '</h4>
            
            <span>Existe un problema en la cantidad ingresada en <b>' . $texto_cantidad . '</b>, revísala y cambiala para seguir con la compra.</span>
          </div>
        </div>
        <div class="p-carrito-resumen_contenedor_contenido_inferior">
          <div class="p-buttons">
            <a class="p-button p-button_success" disabled>
              <span>
                <i class="fas fa-dollar-sign"></i>
              </span>
              <span><b>Continuar con la compra</b></span>
            </a>
          </div>
        </div>';
      
      $boton_miCarrito = "!";
    }
  }

  if(isset($_SESSION['__id__'])){
    unset($_SESSION['__carrito__']);
    unset($_SESSION['__carrito_variables_almacen__']);
  }
  
  $json = [ 'respuesta' => $respuesta, 'html_productos' => $html_productos, 'html_resumen' => $html_resumen, 'boton_miCarrito' => $boton_miCarrito, 'boton_resumen' => $boton_resumen ];
  echo json_encode($json);
}
?>