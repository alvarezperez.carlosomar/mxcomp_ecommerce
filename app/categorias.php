<?php
session_start();
require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Categorías</title>
    <meta name="description" content="Busca el producto que necesitas en las diferentes categorías que manejamos.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, categorías, notificación">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 3;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-busqueda-fondo">
          <h1 class="p-titulo">Categorías</h1>
          <h2 class="p-subtitulo p-text_p">Selecciona la categoría de la cual quieres ver productos.</h2>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-box-options_contenedor">
<?php
$Conn_mxcomp = new Conexion_mxcomp();

try{
  $sql = "SELECT id, nombre, activo FROM __categorias ORDER BY nombre ASC";
  $stmt = $Conn_mxcomp->pdo->prepare($sql);
  $stmt->execute();
  while($datos_categorias = $stmt->fetch(PDO::FETCH_ASSOC)){
    if((int) $datos_categorias['activo'] === 1){
?>
            <a title="Categoría: <?php echo trim($datos_categorias['nombre']); ?>" class="g-categorias-opciones" data-id="<?php echo trim($datos_categorias['id']); ?>">
              <div class="p-box-options_opcion p-box-options_categorias_opcion">
                <span><?php echo trim($datos_categorias['nombre']); ?></span>
              </div>
            </a>
<?php
    }
  }
  
  $stmt = null;
}catch(PDOException $e){
  echo '<p class="p-text_p"><b>Error en la consulta de categorías</b></p>';
}
?>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>  
</html>