<?php
if(isset($_POST['accion']) && $_POST['accion'] === "validar"){
  include dirname(__DIR__, 1) . '/funciones/validaciones_campos.php';
  $valor = trim($_POST['valor']);
  
  // 1: ES VÁLIDO | 2: NO ES VÁLIDO
  $respuesta = validar_campo_letras_espacios_simbolos($valor) ? "1" : "2"; 
  
  $json = [ 'respuesta' => $respuesta ];
  echo json_encode($json);
}
?>