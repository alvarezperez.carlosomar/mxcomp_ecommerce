<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  // PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
  if(isset($_SESSION['__producto_unidades__'])){
    unset($_SESSION['__producto_unidades__']);
  }

  // INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }

  // INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }
  
  require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  require_once dirname(__DIR__, 1) . '/conn.php'; // NECESARIO PARA NAVBAR

  $Conn_mxcomp = new Conexion_mxcomp();
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Eliminar datos de facturación</title>
<?php include dirname(__DIR__, 1) . '/templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include dirname(__DIR__, 1) . '/templates/navbar.php';
?>
    
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include dirname(__DIR__, 1) . '/templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Eliminar datos de facturación</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        try{
          $sql = "SELECT COUNT(id) FROM __direcciones WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datosFacturacion_existen = (int) $stmt->fetchColumn();

          if($datosFacturacion_existen === 1){
?>
          <h3>¿Deseas eliminar tus datos de facturación?</h3>
          <p class="p-text p-text_help p-text_help_margin">
            <span>Ingresa tu contraseña para llevar acabo la eliminación.</span>
          </p>
          <div class="p-notification_contenedor g-facturacion-status">
            <div class="p-notification p-notification_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <span class="p-notification_p"><b>Datos eliminados correctamente.</b></span>
            </div>
          </div>

          <form id="id-facturacion-form_eliminar">
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Contraseña:</span>
                    <span class="p-text p-text_help">(Obligatorio)</span>
                  </label>
                  <div class="p-control">
                    <input type="password" class="p-input g-input_focus g-password-mostrar_button g-facturacion-password_input" placeholder="Contraseña" autocomplete="off">
                    <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-password-mostrar_button">
                      <span>
                        <i class="fas fa-eye"></i>
                      </span>
                      <span>
                        <i class="fas fa-eye-slash"></i>
                      </span>
                    </a>
                  </div>
                  <p class="p-text p-text_info g-facturacion-password_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                  </p>
                  <p class="p-text p-text_info g-facturacion-password_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Los datos de facturación ya han sido eliminados.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-password_alert_error_1">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>La contraseña es incorrecta.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-password_alert_error_2">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>
            
            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button_inverso p-button_delete_inverso">
                <span>
                  <i class="far fa-trash-alt"></i>
                </span>
                <span><b>Eliminar datos</b></span>
              </button>
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </a>
            </div>
          </form>
<?php
          }else{
?>
          <p class="p-text_p">
            <b>No existen datos de facturación para eliminar.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
          }
        }catch(PDOEXception $error){
          //$mensaje_error = $error->getMessage();
          $mensaje_error = "Problema al buscar la información a eliminar.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        }
      }else{
?>
          <p class="p-text_p">
            <b>El usuario no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar el usuario";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
    }
  }
?>
        </div>
      </div>
    </section>
<?php include dirname(__DIR__, 1) . '/templates/footer_pag.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_jquery.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/scripts_facturacion.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>