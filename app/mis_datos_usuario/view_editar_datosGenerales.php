<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  // PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
  if(isset($_SESSION['__producto_unidades__'])){
    unset($_SESSION['__producto_unidades__']);
  }

  // INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }

  // INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }
  
  require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  require_once dirname(__DIR__, 1) . '/conn.php'; // NECESARIO PARA NAVBAR

  $Conn_mxcomp = new Conexion_mxcomp();
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Editar datos generales</title>
<?php include dirname(__DIR__, 1) . '/templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include dirname(__DIR__, 1) . '/templates/navbar.php';
?>
    
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include dirname(__DIR__, 1) . '/templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Editar datos generales</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        //ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar(trim($codigoUsuario));
        
        try{
          $sql = "SELECT Usuarios.nombreS, Usuarios.apellidoPaterno, Usuarios.apellidoMaterno, Usuarios.sexo, Usuarios.fechaNacimiento, Usuarios.noTelefonico1, Usuarios.noTelefonico2, Usuarios.noTelefonico3, Direcciones.tipoVialidad, Direcciones.nombreVialidad, Direcciones.noExterior, Direcciones.noInterior, Direcciones.codigoPostal, Direcciones.colonia, Direcciones.ciudadMunicipio, Direcciones.nombreEstado, Direcciones.entreCalle1, Direcciones.entreCalle2, Direcciones.referenciasAdicionales
          FROM __usuarios Usuarios
          INNER JOIN __direcciones Direcciones ON Direcciones.idUsuario = Usuarios.id
          WHERE Usuarios.id = :idUsuario AND Usuarios.codigoUsuario = :codigoUsuario1 AND Direcciones.codigoUsuario = :codigoUsuario2 AND Direcciones.tipoDireccion = 'particular'";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario1', $codigoUsuario, PDO::PARAM_STR);
          $stmt->bindParam(':codigoUsuario2', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datos_usuarioDireccion = $stmt->fetch(PDO::FETCH_ASSOC);
          
          // DESENCRIPTAR LOS CAMPOS DE DATOS GENERALES
          $nombreS = desencriptar_con_clave(trim($datos_usuarioDireccion['nombreS']), $codigoUsuario_encriptado);
          $apellidoPaterno = desencriptar_con_clave(trim($datos_usuarioDireccion['apellidoPaterno']), $codigoUsuario_encriptado);
          $apellidoMaterno = desencriptar_con_clave(trim($datos_usuarioDireccion['apellidoMaterno']), $codigoUsuario_encriptado);
          $fechaNacimiento = desencriptar_con_clave(trim($datos_usuarioDireccion['fechaNacimiento']), $codigoUsuario_encriptado);
          $noTelefonico1 = desencriptar_con_clave(trim($datos_usuarioDireccion['noTelefonico1']), $codigoUsuario_encriptado);

          $noTelefonico2 = is_null($datos_usuarioDireccion['noTelefonico2']) ? '' : desencriptar_con_clave(trim($datos_usuarioDireccion['noTelefonico2']), $codigoUsuario_encriptado);

          $noTelefonico3 = is_null($datos_usuarioDireccion['noTelefonico3']) ? '' : desencriptar_con_clave(trim($datos_usuarioDireccion['noTelefonico3']), $codigoUsuario_encriptado);
          
          // DESENCRIPTAR LOS CAMPOS DE DOMICILIO PARTICULAR
          $tipoVialidad = trim($datos_usuarioDireccion['tipoVialidad']);
          $nombreVialidad = desencriptar_con_clave(trim($datos_usuarioDireccion['nombreVialidad']), $codigoUsuario_encriptado);
          $noExterior = desencriptar_con_clave(trim($datos_usuarioDireccion['noExterior']), $codigoUsuario_encriptado);
          $noInterior = desencriptar_con_clave(trim($datos_usuarioDireccion['noInterior']), $codigoUsuario_encriptado);
          $codigoPostal = desencriptar_con_clave(trim($datos_usuarioDireccion['codigoPostal']), $codigoUsuario_encriptado);
          $colonia = desencriptar_con_clave(trim($datos_usuarioDireccion['colonia']), $codigoUsuario_encriptado);
          $ciudadMunicipio = desencriptar_con_clave(trim($datos_usuarioDireccion['ciudadMunicipio']), $codigoUsuario_encriptado);
          $nombreEstado = desencriptar_con_clave(trim($datos_usuarioDireccion['nombreEstado']), $codigoUsuario_encriptado);
          
          $entreCalle1 = is_null($datos_usuarioDireccion['entreCalle1']) ? '' : desencriptar_con_clave(trim($datos_usuarioDireccion['entreCalle1']), $codigoUsuario_encriptado);

          $entreCalle2 = is_null($datos_usuarioDireccion['entreCalle2']) ? '' : desencriptar_con_clave(trim($datos_usuarioDireccion['entreCalle2']), $codigoUsuario_encriptado);

          $referenciasAdicionales = is_null($datos_usuarioDireccion['referenciasAdicionales']) ? '' : desencriptar_con_clave(trim($datos_usuarioDireccion['referenciasAdicionales']), $codigoUsuario_encriptado);

          $noExterior = $noExterior === 'S/N' ? NULL : $noExterior;
          $noInterior = $noInterior === 'S/N' ? NULL : $noInterior;
?>
          <p class="p-text p-text_help p-text_help_margin p-text-align_right">
            <span>* Obligatorio</span>
          </p>
          <div class="g-contenedor-aviso"></div>

          <form class="g-editDatosGene-form">
            
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre(s):</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-input_focus g-editDatosGene-nombres_input" placeholder="Nombre(s)" value="<?php echo $nombreS; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-nombres_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-nombres_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>

              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Apellido paterno:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-apellido_paterno_input" placeholder="Apellido paterno" value="<?php echo $apellidoPaterno; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-apellido_paterno_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-apellido_paterno_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>

              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Apellido materno:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-apellido_materno_input" placeholder="Apellido materno" value="<?php echo $apellidoMaterno; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-apellido_materno_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-apellido_materno_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Sexo:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="radio" class="p-input-radio_sexo" id="id-sexo-hombre_radio" name="sexo" value="Hombre"<?php echo trim($datos_usuarioDireccion['sexo']) === 'Hombre' ? ' checked' : ''; ?>>
                    <label class="p-input-radio_sexo_label g-editDatosGene-sexo_radio" for="id-sexo-hombre_radio">
                      <span>Hombre</span>
                    </label>
                    <input type="radio" class="p-input-radio_sexo" id="id-sexo-mujer_radio" name="sexo" value="Mujer"<?php echo trim($datos_usuarioDireccion['sexo']) === 'Mujer' ? ' checked' : ''; ?>>
                    <label class="p-input-radio_sexo_label g-editDatosGene-sexo_radio" for="id-sexo-mujer_radio">
                      <span>Mujer</span>
                    </label>
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-sexo_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Esta opción no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-sexo_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado tu sexo.</span>
                  </p>
                </div>
              </div>

              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Fecha de nacimiento:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="date" class="p-input p-date g-editDatosGene-fecha_nacimiento_input" value="<?php echo $fechaNacimiento; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_error g-editDatosGene-fecha_nacimiento_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has escrito/seleccionado tu fecha de nacimiento.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. telefónico(s):</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <p class="p-text p-text_help">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
                  <div class="p-columnas">
                    <div class="p-columna p-col_4_12">
                      <div class="p-control">
                        <input type="tel" class="p-input g-editDatosGene-no_telefonico_1_input" placeholder="No. telefónico #1" maxlength="10" value="<?php echo $noTelefonico1; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_1_alert_info_1">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo números, por favor.</span>
                      </p>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_1_alert_info_2">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>El número telefónico debe de contener 10 dígitos.</span>
                      </p>
                      <p class="p-text p-text_error g-editDatosGene-no_telefonico_1_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>Al menos ingresa 1 número telefónico, por favor.</span>
                      </p>
                    </div>

                    <div class="p-columna">
                      <div class="p-control">
                        <input type="tel" class="p-input g-editDatosGene-no_telefonico_2_input" placeholder="No. telefónico #2" maxlength="10" value="<?php echo $noTelefonico2; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_2_alert_info_1">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo números, por favor.</span>
                      </p>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_2_alert_info_2">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>El número telefónico debe de contener 10 dígitos.</span>
                      </p>
                    </div>

                    <div class="p-columna">
                      <div class="p-control">
                        <input type="tel" class="p-input g-editDatosGene-no_telefonico_3_input" placeholder="No. telefónico #3" maxlength="10" value="<?php echo $noTelefonico3; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_3_alert_info_1">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo números, por favor.</span>
                      </p>
                      <p class="p-text p-text_info g-editDatosGene-no_telefonico_3_alert_info_2">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>El número telefónico debe de contener 10 dígitos.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Domicilio particular</h3>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Tipo de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-editDatosGene-tipo_vialidad_div">
                      <select class="p-input g-form_select g-editDatosGene-tipo_vialidad_select">
                        <option value="">Selecciona el tipo</option>
<?php
          try{
            $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->execute();
            
            while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
              $selected = $tipoVialidad === trim($datos_tipoVialidad['nombreTipoVialidad']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_tipoVialidad['id']); ?>"><?php echo trim($datos_tipoVialidad['nombreTipoVialidad']); ?></option>
<?php
            }
          }catch(PDOException $error){
            //$mensaje_error = $error->getMessage();
            $mensaje_error = "Problema al buscar los tipos de vialidad";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
          }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-tipo_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este tipo de vialidad no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-tipo_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un tipo de vialidad.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-nombre_vialidad_input" placeholder="Nombre de vialidad" value="<?php echo $nombreVialidad; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-nombre_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-nombre_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. exterior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
          if(is_null($noExterior)){
?>
                    <input type="text" class="p-input g-form-noExterior_input g-editDatosGene-no_exterior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
          }else{
?>
                    <input type="text" class="p-input g-form-noExterior_input g-editDatosGene-no_exterior_input" placeholder="No. exterior" value="<?php echo $noExterior; ?>" autocomplete="off">
<?php
          }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noExterior_alert_info g-editDatosGene-no_exterior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noExterior_alert_error g-editDatosGene-no_exterior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
          if(is_null($noExterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-editDatosGene-no_exterior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Agregar no. exterior</label>
<?php
          }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-editDatosGene-no_exterior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>
<?php
          }
?>
                </div>
              </div>

              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. interior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
          if(is_null($noInterior)){
?>
                    <input type="text" class="p-input g-form-noInterior_input g-editDatosGene-no_interior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
          }else{
?>
                    <input type="text" class="p-input g-form-noInterior_input g-editDatosGene-no_interior_input" placeholder="No. interior" value="<?php echo $noInterior; ?>" autocomplete="off">
<?php
          }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noInterior_alert_info g-editDatosGene-no_interior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noInterior_alert_error g-editDatosGene-no_interior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
          if(is_null($noInterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-editDatosGene-no_interior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Agregar no. interior</label>
<?php
          }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-editDatosGene-no_interior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>
<?php
          }
?>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Código postal:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-codigo_postal_input" placeholder="Código postal" value="<?php echo $codigoPostal; ?>" maxlength="5" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-codigo_postal_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números, por favor.</span>
                  </p>
                  <p class="p-text p-text_info g-editDatosGene-codigo_postal_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El código postal debe de contener 5 números.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-codigo_postal_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Colonia:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-colonia_input" placeholder="Colonia" value="<?php echo $colonia; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-colonia_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-colonia_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>

              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Ciudad o municipio:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-editDatosGene-ciudad_municipio_input" placeholder="Ciudad o municipio" value="<?php echo $ciudadMunicipio; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-ciudad_municipio_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-ciudad_municipio_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Estado:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-editDatosGene-estado_div">
                      <select class="p-input g-form_select g-editDatosGene-estado_select">
                        <option value="">Selecciona tu estado</option>
<?php
          try{
            $sql = "SELECT id, nombreEstado FROM __estados_codigos";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->execute();
            
            while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
              $selected = $nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_estado['id']); ?>"><?php echo trim($datos_estado['nombreEstado']); ?></option>
<?php
            }
          }catch(PDOException $error){
            //$mensaje_error = $error->getMessage();
            $mensaje_error = "Problema al buscar los estados";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
          }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-estado_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este estado no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-editDatosGene-estado_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un estado.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Entre calles:</span>
                    <span class="p-text p-text_help">(Opcional)</span>
                  </label>
                  <div class="p-columnas">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-control">
                        <input type="text" class="p-input g-editDatosGene-entre_calles_1_input" placeholder="Calle #1" value="<?php echo $entreCalle1; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-editDatosGene-entre_calles_1_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                      </p>
                      <p class="p-text p-text_error g-editDatosGene-entre_calles_1_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La calle #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>

                    <div class="p-columna">
                      <div class="p-control">
                        <input type="text" class="p-input g-editDatosGene-entre_calles_2_input" placeholder="Calle #2" value="<?php echo $entreCalle2; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-editDatosGene-entre_calles_2_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                      </p>
                      <p class="p-text p-text_error g-editDatosGene-entre_calles_2_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La calle #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Referencias adicionales:</span>
                    <span class="p-text p-text_help">(Opcional)</span>
                  </label>
                  <div class="p-control">
                    <textarea class="p-input p-textarea g-editDatosGene-referencias_adicionales_textarea" placeholder="Ejemplo: frente a tienda..."><?php echo $referenciasAdicionales; ?></textarea>
                  </div>
                  <p class="p-text p-text_info g-editDatosGene-referencias_adicionales_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                </div>
              </div>
            </div>
            
            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button p-button_info g-editDatosGene-guardar_cambios_button">
                <span>
                  <i class="fas fa-save"></i>
                </span>
                <span><b>Guardar cambios</b></span>
              </button>
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </a>
            </div>
          </form>
<?php
        }catch(PDOException $error){
          //$mensaje_error = $error->getMessage();
          $mensaje_error = "Problema al buscar la información a editar.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        }
      }else{
?>
          <p class="p-text_p">
            <b>El usuario no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar el usuario";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
    }
  }
?>
        </div>
      </div>
    </section>
<?php include dirname(__DIR__, 1) . '/templates/footer_pag.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_jquery.php'; ?>

    <script src="scripts/mis_datos/scripts_editarDatosGenerales.js?v=2.1"></script>
<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>