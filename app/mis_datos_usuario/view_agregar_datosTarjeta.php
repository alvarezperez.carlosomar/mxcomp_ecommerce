<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  if(isset($_SESSION['cantidad_Almacen_1']) || isset($_SESSION['cantidad_Almacen_7']) || isset($_SESSION['cantidad_Almacen_56']) || isset($_SESSION['cantidad_Almacen_74'])){
    unset($_SESSION['cantidad_Almacen_1']);
    unset($_SESSION['cantidad_Almacen_7']);
    unset($_SESSION['cantidad_Almacen_56']);
    unset($_SESSION['cantidad_Almacen_74']);
  }
  
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }
  
  include '../funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  include '../funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  include '../global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  include '../conn.php'; // NECESARIO PARA NAVBAR
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Agregar tarjeta</title>
<?php include '../templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include '../templates/navbar.php';
?>
    
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include '../templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Agregar tarjeta</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  
  if(validar_campo_numerico($idUser)){
    $idUser = (int) $idUser;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUser AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        // ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);
        
        try{
          $sql = "SELECT COUNT(id) FROM __tarjetas_u WHERE BINARY userID = :idUser AND codigoUsuario = :codigoUsuario";
          $stmt = $conexion->prepare($sql);
          $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $tarjetas_cantidad = (int) $stmt->fetchColumn();

          if($tarjetas_cantidad < 3){
            try{
              $sql = "SELECT COUNT(Usuarios.id) AS conteo, Usuarios.nombreS, Usuarios.apellidoPaterno, Usuarios.apellidoMaterno, Direcciones.tipoVialidad, Direcciones.nomVialidad, Direcciones.noExterior, Direcciones.noInterior, Direcciones.codigoPostal, Direcciones.colonia, Direcciones.ciudadMunicipio, Direcciones.estado
              FROM __usuarios Usuarios
              INNER JOIN __direcciones Direcciones ON Direcciones.userID = Usuarios.id
              WHERE Usuarios.id = :idUser AND Usuarios.codigoUsuario = :codigoUsuario1 AND Direcciones.codigoUsuario = :codigoUsuario2 AND Direcciones.tipoDireccion = 'particular'";
              $stmt = $conexion->prepare($sql);
              $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
              $stmt->bindParam(':codigoUsuario1', $codigoUsuario, PDO::PARAM_STR);
              $stmt->bindParam(':codigoUsuario2', $codigoUsuario, PDO::PARAM_STR);
              $stmt->execute();
              $datos_tarjeta = $stmt->fetch(PDO::FETCH_ASSOC);
              $info_cantidad = (int) $datos_tarjeta['conteo'];

              if($info_cantidad === 1){
                // DESENCRIPTAR LOS CAMPOS DE DATOS GENERALES
                $nombreS = desencriptar_con_clave(trim($datos_tarjeta['nombreS']), $codigoUsuario_encriptado);
                $apellidoPaterno = desencriptar_con_clave(trim($datos_tarjeta['apellidoPaterno']), $codigoUsuario_encriptado);
                $apellidoMaterno = desencriptar_con_clave(trim($datos_tarjeta['apellidoMaterno']), $codigoUsuario_encriptado);
                
                // DESENCRIPTAR LOS CAMPOS DEL DOMICILIO PARTICULAR
                $nomVialidad = desencriptar_con_clave(trim($datos_tarjeta['nomVialidad']), $codigoUsuario_encriptado);
                $noExterior = desencriptar_con_clave(trim($datos_tarjeta['noExterior']), $codigoUsuario_encriptado);
                $noInterior = desencriptar_con_clave(trim($datos_tarjeta['noInterior']), $codigoUsuario_encriptado);
                $codigoPostal = desencriptar_con_clave(trim($datos_tarjeta['codigoPostal']), $codigoUsuario_encriptado);
                $colonia = desencriptar_con_clave(trim($datos_tarjeta['colonia']), $codigoUsuario_encriptado);
                $ciudadMunicipio = desencriptar_con_clave(trim($datos_tarjeta['ciudadMunicipio']), $codigoUsuario_encriptado);
                $estado = desencriptar_con_clave(trim($datos_tarjeta['estado']), $codigoUsuario_encriptado);
?>
          <p class="p-text p-text_help p-text_help_margin">
            <span>Algunos campos se llenan con información ya registrada anteriormente, cambie sus valores si es necesario.</span>
          </p>
          <p class="p-text p-text_help p-text_help_margin p-text-align_right">
            <span>* Obligatorio</span>
          </p>
          
          <div class="p-loading-general" id="id-agrTarjeta_loading">
            <div></div>
            <p>Espere por favor...</p>
          </div>
          
          <div class="p-notification_contenedor" id="id-agrTarjeta-status">
            <div class="p-notification p-notification_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <p class="p-notification_p">
                <span>Tarjeta agregada correctamente.</span>
              </p>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-agrTarjeta-msg_info">
            <div class="p-notification p-notification_letter_info">
              <span>
                <i class="fas fa-info-circle"></i> 
              </span>
              <p class="p-notification_p">
                <span id="id-agrTarjeta-msg_info_span"></span>
              </p>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-agrTarjeta-msg_error">
            <div class="p-notification p-notification_letter_error">
              <span>
                <i class="fas fa-times-circle"></i> 
              </span>
              <p class="p-notification_p">
                <span id="id-agrTarjeta-msg_error_span"></span>
              </p>
            </div>
          </div>
          
          <div class="p-columnas">
            <div class="p-columna p-columna_openpay p-tres_columnas">
              <label class="p-label">
                <span>Tarjetas de crédito aceptadas:</span>
              </label>
              <img src="images/openpay/cards1.png" alt="Imágen tarjetas 1">
            </div>
            <div class="p-columna p-columna_openpay">
              <label class="p-label">
                <span>Tarjetas de débito aceptadas:</span>
              </label>
              <div class="p-contenedor_tarjetas">
                <img src="images/openpay/cards2.png" alt="Imágen tarjetas 2">
              </div>
            </div>
          </div>
          
          <form id="id-agrTarjeta-form">
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre(s) y apellidos del titular:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <p class="p-text p-text_help">
                    <span>Máximo 80 caracteres.</span>
                  </p>
                  <div class="p-control">
                    <input type="text" class="p-input g-input_focus" id="id-agrTarjeta-nom_titu_input" placeholder="Nombre(s) y apellidos del titular" value="<?php echo $nombreS." ".$apellidoPaterno." ".$apellidoMaterno;
                    ?>" autocomplete="off" maxlength="80">
                    <label class="p-label p-label_tipo_tarjeta" style="margin-left: .5rem;">
                      <span id="id-agrTarjeta-nom_titu_contador_span">0</span>
                    </label>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-nom_titu_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-nom_titu_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Número de la tarjeta:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-num_tar_input" placeholder="Número de la tarjeta" autocomplete="off" maxlength="19">
                    <label class="p-label p-label_tipo_tarjeta" style="margin-left: .5rem;">
                      <span id="id-agrTarjeta-num_tar_contador_span">0</span>
                    </label>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-num_tar_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números, por favor.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-num_tar_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El número de la tarjeta está incompleto o es incorrecto, por favor verifíquelo.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-num_tar_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Tipo de tarjeta:</span>
                  </label>
                  <div class="p-control">
                    <label class="p-label p-text_p p-label_tipo_tarjeta">
                      <span id="id-agrTarjeta-tipo_tarjeta_span">Aún no se tiene el dato</span>
                    </label>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-tipo_tarjeta_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>No se generó el tipo de tarjeta.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-tipo_tarjeta_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>No es válido este tipo de tarjeta.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_7_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Fecha de vencimiento:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-mes_venc_input" placeholder="Mes (MM)" autocomplete="off" maxlength="2" style="margin-right: 5px;">
                    <input type="text" class="p-input" id="id-agrTarjeta-anio_venc_input" placeholder="Año (AA ó AAAA)" autocomplete="off" maxlength="4" style="margin-left: 5px;" disabled>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-mes_venc_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números en el mes, por favor.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-mes_venc_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El mes debe de ser de 2 digitos.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-anio_venc_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números en el año, por favor.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-anio_venc_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El año debe de ser de 2 o 4 digitos.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-fecha_vencimiento_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>La fecha de expiración no es válida, revisa el mes o el año.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-mes_venc_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-anio_venc_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
              <div class="p-columna p-col_5_12 p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Código de seguridad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-cvc_input" placeholder="CVC" autocomplete="off" maxlength="4" style="margin-right: .5rem;">
                    <img src="images/openpay/cvv1.png" alt="Imágen CVV 1" style="margin-right: .5rem;">
                    <img src="images/openpay/cvv2.png" alt="Imágen CVV 2">
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-cvc_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números son permitidos.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-cvc_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El código de seguridad sólo puede ser de 3 o 4 digitos.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-cvc_alert_info_3">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El código de seguridad no es válido, revísalo por favor.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-cvc_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Dirección de la tarjeta</h3>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Tipo de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select" id="id-agrTarjeta-tipo_vialidad_div">
                      <select class="p-input g-form_select" id="id-agrTarjeta-tipo_vialidad_select">
                        <option value="">Selecciona el tipo</option>
<?php
                $tipoVialidad = trim($datos_tarjeta['tipoVialidad']);
                
                try{
                  $sql = "SELECT id, nom_tipoVialidad FROM __tipos_vialidad";
                  $stmt = $conexion->prepare($sql);
                  $stmt->execute();

                  while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $selected = '';

                    if($tipoVialidad === trim($datos_tipoVialidad['nom_tipoVialidad'])){
                      $selected = ' selected="selected"';
                    }
?>
                        <option<?php echo $selected; ?> value="<?php echo trim($datos_tipoVialidad['id']); ?>"><?php echo trim($datos_tipoVialidad['nom_tipoVialidad']); ?></option>
<?php
                  }
                }catch(PDOException $error){
                  //$mensaje_error = $error->getMessage();
                  $mensaje_error = "Problema al buscar los tipos de vialidad";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
                }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-tipo_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este tipo de vialidad no existe.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-tipo_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un tipo de vialidad.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-nom_vialidad_input" placeholder="Nombre de vialidad" value="<?php echo $nomVialidad; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-nom_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-nom_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. exterior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
                if($noExterior === "S/N"){
?>
                    <input type="text" class="p-input" id="id-agrTarjeta-no_ext_input" placeholder="S/N" autocomplete="off" disabled>
<?php
                }else{
?>
                    <input type="text" class="p-input" id="id-agrTarjeta-no_ext_input" placeholder="No. exterior" value="<?php echo $noExterior; ?>" autocomplete="off">
<?php
                }
?>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-no_ext_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-no_ext_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
                if($noExterior === "S/N"){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-numero_exterior" id="id-agrTarjeta-no_ext_checkbox" checked>
                  <label for="id-agrTarjeta-no_ext_checkbox" class="p-label-checkbox_numeroCalle">Agregar No. exterior</label>
<?php
                }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-numero_exterior" id="id-agrTarjeta-no_ext_checkbox">
                  <label for="id-agrTarjeta-no_ext_checkbox" class="p-label-checkbox_numeroCalle">Sin No. exterior</label>
<?php
                }
?>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. interior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
                if($noInterior === "S/N"){
?>
                    <input type="text" class="p-input" id="id-agrTarjeta-no_int_input" placeholder="S/N" autocomplete="off" disabled>
<?php
                }else{
?>
                    <input type="text" class="p-input" id="id-agrTarjeta-no_int_input" placeholder="No. interior" value="<?php echo $noInterior; ?>" autocomplete="off">
<?php
                }
?>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-no_int_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-no_int_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
                if($noInterior === "S/N"){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-numero_interior" id="id-agrTarjeta-no_int_checkbox" checked>
                  <label for="id-agrTarjeta-no_int_checkbox" class="p-label-checkbox_numeroCalle">Agregar No. interior</label>
<?php
                }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-numero_interior" id="id-agrTarjeta-no_int_checkbox">
                  <label for="id-agrTarjeta-no_int_checkbox" class="p-label-checkbox_numeroCalle">Sin No. interior</label>
<?php
                }
?>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Código Postal:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-codigo_postal_input" placeholder="Código Postal" value="<?php echo $codigoPostal; ?>" autocomplete="off" maxlength="5">
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-codigo_postal_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números, por favor.</span>
                  </p>
                  <p class="p-text p-text_info" id="id-agrTarjeta-codigo_postal_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El Código Postal debe de contener 5 números.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-codigo_postal_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Colonia:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-colonia_input" placeholder="Colonia" value="<?php echo $colonia; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-colonia_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-colonia_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Ciudad o Municipio:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input" id="id-agrTarjeta-ciudad_municipio_input" placeholder="Ciudad o Municipio" value="<?php echo $ciudadMunicipio; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-ciudad_municipio_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-ciudad_municipio_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Estado:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select" id="id-agrTarjeta-estado_div">
                      <select class="p-input g-form_select" id="id-agrTarjeta-estado_select">
                        <option value="">Selecciona tu estado</option>
<?php
                try{
                  $sql = "SELECT id, nomEstado FROM __estados_codigos";
                  $stmt = $conexion->prepare($sql);
                  $stmt->execute();

                  while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
                    $selected = '';

                    if($estado === trim($datos_estado['nomEstado'])){
                      $selected = ' selected="selected"';
                    }
?>
                        <option<?php echo $selected; ?> value="<?php echo trim($datos_estado['id']); ?>"><?php echo trim($datos_estado['nomEstado']); ?></option>
<?php
                  }
                }catch(PDOException $error){
                  //$mensaje_error = $error->getMessage();
                  $mensaje_error = "Problema al buscar los estados";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
                }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info" id="id-agrTarjeta-estado_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este estado no existe.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-agrTarjeta-estado_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un estado.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas">
              <div class="p-columna p-dos_columnas" style="padding: 0 !important;"></div>
              <div class="p-columna p-columna_openpay">
                <label class="p-label p-label_openpay">
                  <span>Transacciones realizadas vía:</span>
                </label>
                <img src="images/openpay/openpay.png" alt="Logo Openpay">
              </div>
              <div class="p-columna p-columna_openpay">
                <article class="p-media-contenedor">
                  <figure class="p-media-div_left" style="margin-right: .5rem !important;">
                    <img src="images/openpay/security.png" alt="Seguridad pago">
                  </figure>
                  <div class="p-media-div_info">
                    <label class="p-label p-label_openpay">
                      <span>Tus pagos se realizan de forma segura con encriptación de 256 bits</span>
                    </label>
                  </div>
                </article>
              </div>
            </div>
            
            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button p-button_success">
                <span>
                  <i class="fas fa-plus"></i>
                </span>
                <span><b>Agregar tarjeta</b></span>
              </button>
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </a>
            </div>
            
            <input type="hidden" name="id-tokenID" id="id-tokenID">
          </form>
<?php
              }
            }catch(PDOException $error){
              //$mensaje_error = $error->getMessage();
              $mensaje_error = "Problema al buscar la información de la dirección particular.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
            }
          }else{
?>
          <p class="p-text_p">
            <b>No se permite agregar más de 3 tarjetas. Elimine la que ya no use y agrege una nueva.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
          }
        }catch(PDOException $error){
          //$mensaje_error = $error->getMessage();
          $mensaje_error = "Problema al buscar las tarjetas.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        }
      }else{
?>
          <p class="p-text_p">
            <b>El usuario no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar el usuario";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
    }
  }
?>
        </div>
      </div>
    </section>
<?php include '../templates/footer_pag.php'; ?>

<?php include '../templates/footer_scripts_jquery.php'; ?>

    <script>
      var seccion = "modificar_info";
    </script>
<?php include '../templates/scripts_agrTarjeta.php'; ?>

<?php include '../templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>