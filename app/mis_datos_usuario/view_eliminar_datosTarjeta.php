<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  if(isset($_SESSION['cantidad_Almacen_1']) || isset($_SESSION['cantidad_Almacen_7']) || isset($_SESSION['cantidad_Almacen_56']) || isset($_SESSION['cantidad_Almacen_74'])){
    unset($_SESSION['cantidad_Almacen_1']);
    unset($_SESSION['cantidad_Almacen_7']);
    unset($_SESSION['cantidad_Almacen_56']);
    unset($_SESSION['cantidad_Almacen_74']);
  }
  
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }
  
  if(!isset($_GET['n'])){
    echo '<script> window.history.go(-1); </script>';
  }
  
  include '../funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  include '../funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  include '../global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  include '../conn.php'; // NECESARIO PARA NAVBAR
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Eliminar tarjeta</title>
<?php include '../templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include '../templates/navbar.php';
?>
    
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include '../templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Eliminar tarjeta</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUser = desencriptar(trim($_SESSION['__id__']));
  
  if(validar_campo_numerico($idUser)){
    $idUser = (int) $idUser;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUser";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        if(validar_campo_numerico(trim($_GET['n']))){
          $idTar = (int) substr(trim($_GET['n']), 1);
          
          try{
            $sql = "SELECT COUNT(id) AS conteo, tipoTar, ultimosDigitos_numTar FROM __tarjetas_u WHERE BINARY id = :idTar AND userID = :idUser";
            $stmt = $conexion->prepare($sql);
            $stmt->bindParam(':idTar', $idTar, PDO::PARAM_INT);
            $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
            $stmt->execute();
            $datos_tarjeta = $stmt->fetch(PDO::FETCH_ASSOC);

            if((int) $datos_tarjeta['conteo'] === 1){
?>
          <p class="p-text p-text_help p-text_help_margin">
            <span>Ingresa tu contraseña para llevar acabo la eliminación.</span>
          </p>
          
          <div class="p-loading-general" id="id-elimTarjeta_loading">
            <div></div>
            <p><b>Espere por favor...</b></p>
          </div>
          
          <div class="p-notification_contenedor" id="id-elimTarjeta-status">
            <div class="p-notification p-notification_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <p class="p-notification_p">
                <span><b>Tarjeta eliminada correctamente.</b></span>
              </p>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-elimTarjeta-msg_info">
            <div class="p-notification p-notification_letter_info">
              <span>
                <i class="fas fa-info-circle"></i> 
              </span>
              <p class="p-notification_p">
                <span id="id-elimTarjeta-msg_info_span"></span>
              </p>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-elimTarjeta-msg_error">
            <div class="p-notification p-notification_letter_error">
              <span>
                <i class="fas fa-times-circle"></i> 
              </span>
              <p class="p-notification_p">
                <span id="id-elimTarjeta-msg_error_span"></span>
              </p>
            </div>
          </div>

          <p class="p-text_p">Tarjeta <strong><?php echo $datos_tarjeta['tipoTar']; ?></strong> con terminación: <strong><?php echo $datos_tarjeta['ultimosDigitos_numTar']; ?></strong></p>

          <form id="id-elimTarjeta-form">
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Contraseña:</span>
                    <span class="p-text p-text_help">(Obligatorio)</span>
                  </label>
                  <div class="p-control">
                    <input type="hidden" id="id-elimTarjeta-valor_input" value="<?php echo encriptar($idTar); ?>">
                    <input type="password" class="p-input g-input_focus g-elimTarjeta-password_mostrar_button" id="id-elimTarjeta-password_input" placeholder="Contraseña" autocomplete="off">
                    <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-elimTarjeta-password_mostrar_button">
                      <span>
                        <i class="fas fa-eye"></i>
                      </span>
                      <span>
                        <i class="fas fa-eye-slash"></i>
                      </span>
                    </a>
                  </div>
                  <p class="p-text p-text_info" id="id-elimTarjeta-password_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                  </p>
                  <p class="p-text p-text_info" id="id-elimTarjeta-password_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>La tarjeta no existe, debió de haberla eliminado anteriormente.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-elimTarjeta-password_alert_error_1">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>La contraseña es incorrecta.</span>
                  </p>
                  <p class="p-text p-text_error" id="id-elimTarjeta-password_alert_error_2">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>
            
            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button_inverso p-button_delete_inverso">
                <span>
                  <i class="far fa-trash-alt"></i>
                </span>
                <span><b>Eliminar tarjeta</b></span>
              </button>
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </a>
            </div>
          </form>
<?php
            }else{
?>
          <p class="p-text_p">
            <b>La tarjeta ya se eliminó.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
            }
          }catch(PDOException $error){
            //$mensaje_error = $error->getMessage();
            $mensaje_error = "Problema al buscar la tarjeta a eliminar.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
          }
        }else{
?>
          <p class="p-text_p">
            <b>La tarjeta no se encontró.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        }
      }else{
?>
          <p class="p-text_p">
            <b>El usuario no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar el usuario";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
    }
  }
?>
        </div>
      </div>
    </section>
<?php include '../templates/footer_pag.php'; ?>

<?php include '../templates/footer_scripts_jquery.php'; ?>

    <script>
      var seccion = "modificar_info";
    </script>
<?php include '../templates/scripts_elimTarjeta.php'; ?>

<?php include '../templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>