<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  // PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
  if(isset($_SESSION['__producto_unidades__'])){
    unset($_SESSION['__producto_unidades__']);
  }

  // INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }

  // INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }

  require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  require_once dirname(__DIR__, 1) . '/conn.php'; // NECESARIO PARA NAVBAR

  require_once dirname(__DIR__, 1) . '/clases/usuario/metodos_usuario.php';
  require_once dirname(__DIR__, 1) . '/clases/direcciones/metodos_direcciones.php';
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Editar domicilio de envío</title>
<?php include dirname(__DIR__, 1) . '/templates/head.php';?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include dirname(__DIR__, 1) . '/templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include dirname(__DIR__, 1) . '/templates/menu_lateral.php';?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Editar domicilio de envío</h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $proceso_exitoso = false;
  $opcion = 0;
  $mensaje_error = '';
  
  // REVISA SI EL ID DE USUARIO ES NUMERICO
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario = new Usuario($idUsuario, $codigoUsuario);
    $direccionUsuario = new Direccion($idUsuario, $codigoUsuario);
    $proceso_exitoso = true;
  }else{
    $opcion = 1;
    $mensaje_error = 'EL id del usuario no es numérico';
    $proceso_exitoso = false;
  }

  // REVISA LA EXISTENCIA DEL USUARIO
  if($proceso_exitoso){
    if($usuario->buscarUsuario()){
      $proceso_exitoso = true;
    }else{
      $opcion = (int) $usuario->buscarUsuario_opcion;
      $mensaje_error = (string) $usuario->buscarUsuario_mensaje;
      $proceso_exitoso = false;
    }
  }

  // SI EXISTE EL USUARIO, SE BUSCA EL DOMICILIO DE ENVIO
  if($proceso_exitoso){
    if($direccionUsuario->ver_domicilioEnvio()){
      $domiEnvio_nombreDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_desencriptado;
      $domiEnvio_tipoVialidad = $direccionUsuario->ver_domEnv_tipoVialidad;
      $domiEnvio_nombreVialidad = $direccionUsuario->ver_domEnv_nombreVialidad;
      $domiEnvio_noExterior = $direccionUsuario->ver_domEnv_noExterior === 'S/N' ? NULL : $direccionUsuario->ver_domEnv_noExterior;
      $domiEnvio_noInterior = $direccionUsuario->ver_domEnv_noInterior === 'S/N' ? NULL : $direccionUsuario->ver_domEnv_noInterior;
      $domiEnvio_codigoPostal = $direccionUsuario->ver_domEnv_codigoPostal;
      $domiEnvio_colonia = $direccionUsuario->ver_domEnv_colonia;
      $domiEnvio_ciudadMunicipio = $direccionUsuario->ver_domEnv_ciudadMunicipio;
      $domiEnvio_nombreEstado = $direccionUsuario->ver_domEnv_nombreEstado;
      $domiEnvio_entreCalle1 = $direccionUsuario->ver_domEnv_entreCalle1;
      $domiEnvio_entreCalle2 = $direccionUsuario->ver_domEnv_entreCalle2;
      $domiEnvio_referenciasAdicionales = $direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado;
      $domiEnvio_noTelefonico = $direccionUsuario->ver_domEnv_noTelefonico_desencriptado;
      $domiEnvio_horaEntregaEnvio_1 = $direccionUsuario->ver_domEnv_horaEntregaEnvio1;
      $domiEnvio_horaEntregaEnvio_2 = $direccionUsuario->ver_domEnv_horaEntregaEnvio2;
      $proceso_exitoso = true;
    }else{
      $opcion = (int) $direccionUsuario->ver_domEnv_opcion;
      $mensaje_error = (string) $direccionUsuario->ver_domEnv_mensaje;
      $proceso_exitoso = false;
    }
  }

  // SI EL PROCESO NO FUE EXITOSO
  if(!$proceso_exitoso){
    switch($opcion){
      case 1: // MENSAJE DE INFO
?>
          <p class="p-notification p-notification_letter_info"><b><?php echo $mensaje_error; ?></b></p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span><i class="fas fa-undo"></i></span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        break;

      case 2: // MENSAJE DE ERROR
?>
          <p class="p-notification p-notification_letter_error"><b><?php echo $mensaje_error; ?></b></p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span><i class="fas fa-undo"></i></span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        break;
    }
  }else{
    $Conn_mxcomp = new Conexion_mxcomp();
?>
          <p class="p-text p-text_help p-text_help_margin">
            <span>Edite los datos en caso de ser necesario, ya que este domicilio será usada para enviar su pedido.</span>
          </p>
          <p class="p-text p-text_help p-text_help_margin p-text-align_right">
            <span>* Obligatorio</span>
          </p>

          <div class="p-notification_contenedor g-domiEnvio-status">
            <div class="p-notification p-notification_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <p class="p-notification_p">
                <span><b>Cambios guardados correctamente.</b></span>
              </p>
            </div>
          </div>

          <form id="g-domiEnvio-form_editar">
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre del destinatario (con apellidos):</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-input_focus g-domiEnvio-nombre_destinatario_input" placeholder="Nombre del destinatario (con apellidos)" value="<?php echo $domiEnvio_nombreDestinatario; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-nombre_destinatario_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-nombre_destinatario_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Tipo de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-domiEnvio-tipo_vialidad_div">
                      <select class="p-input g-form_select g-domiEnvio-tipo_vialidad_select">
                        <option value="">Selecciona el tipo</option>
<?php
    try{
      $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while ($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $selected = $domiEnvio_tipoVialidad === trim($datos_tipoVialidad['nombreTipoVialidad']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_tipoVialidad['id']); ?>"><?php echo trim($datos_tipoVialidad['nombreTipoVialidad']); ?></option>
<?php
      }
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar los tipos de vialidad";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
    }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-tipo_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este tipo de vialidad no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-tipo_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un tipo de vialidad.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-domiEnvio-nombre_vialidad_input" placeholder="Nombre de vialidad" value="<?php echo $domiEnvio_nombreVialidad; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-nombre_vialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-nombre_vialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. exterior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
    if(is_null($domiEnvio_noExterior)){
?>
                    <input type="text" class="p-input g-form-noExterior_input g-domiEnvio-no_exterior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
    }else{
?>
                    <input type="text" class="p-input g-form-noExterior_input g-domiEnvio-no_exterior_input" placeholder="No. exterior" value="<?php echo $domiEnvio_noExterior; ?>" autocomplete="off">
<?php
    }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noExterior_alert_info g-domiEnvio-no_exterior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noExterior_alert_error g-domiEnvio-no_exterior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
    if(is_null($domiEnvio_noExterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-domiEnvio-no_exterior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Agregar no. exterior</label>
<?php
    }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-domiEnvio-no_exterior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>
<?php
    }
?>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. interior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
    if(is_null($domiEnvio_noInterior)){
?>
                    <input type="text" class="p-input g-form-noInterior_input g-domiEnvio-no_interior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
    }else{
?>
                    <input type="text" class="p-input g-form-noInterior_input g-domiEnvio-no_interior_input" placeholder="No. interior" value="<?php echo $domiEnvio_noInterior; ?>" autocomplete="off">
<?php
    }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noInterior_alert_info g-domiEnvio-no_interior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noInterior_alert_error g-domiEnvio-no_interior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
    if(is_null($domiEnvio_noInterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-domiEnvio-no_interior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Agregar no. interior</label>
<?php
    }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-domiEnvio-no_interior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>
<?php
    }
?>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Código postal:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-domiEnvio-codigo_postal_input" placeholder="Código postal" value="<?php echo $domiEnvio_codigoPostal; ?>" autocomplete="off" maxlength="5">
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-codigo_postal_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números, por favor.</span>
                  </p>
                  <p class="p-text p-text_info g-domiEnvio-codigo_postal_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El código postal debe de contener 5 números.</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-codigo_postal_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Colonia:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-domiEnvio-colonia_input" placeholder="Colonia" value="<?php echo $domiEnvio_colonia; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-colonia_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-colonia_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Ciudad o municipio:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-domiEnvio-ciudad_municipio_input" placeholder="Ciudad o municipio" value="<?php echo $domiEnvio_ciudadMunicipio; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-ciudad_municipio_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-ciudad_municipio_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Estado:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-domiEnvio-estado_div">
                      <select class="p-input g-form_select g-domiEnvio-estado_select">
                        <option value="">Selecciona tu estado</option>
<?php
    try{
      $sql = "SELECT id, nombreEstado FROM __estados_codigos";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->execute();

      while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
        $selected = $domiEnvio_nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_estado['id']); ?>"><?php echo trim($datos_estado['nombreEstado']); ?></option>
<?php
      }
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar los estados";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
    }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-estado_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este estado no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-domiEnvio-estado_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un estado.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Entre calles:</span>
                    <span class="p-text p-text_help">(Opcional)</span>
                  </label>
                  <div class="p-columnas">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-control">
                        <input type="text" class="p-input g-domiEnvio-entre_calles_1_input" placeholder="Calle #1" value="<?php echo $domiEnvio_entreCalle1; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-domiEnvio-entre_calles_1_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                      </p>
                      <p class="p-text p-text_error g-domiEnvio-entre_calles_1_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La calle #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>
                    <div class="p-columna">
                      <div class="p-control">
                        <input type="text" class="p-input g-domiEnvio-entre_calles_2_input" placeholder="Calle #2" value="<?php echo $domiEnvio_entreCalle2; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-domiEnvio-entre_calles_2_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                      </p>
                      <p class="p-text p-text_error g-domiEnvio-entre_calles_2_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La calle #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Referencias adicionales:</span>
                    <span class="p-text p-text_help">(Opcional)</span>
                  </label>
                  <div class="p-control">
                    <textarea class="p-input p-textarea g-domiEnvio-referencias_adicionales_textarea" placeholder="Ejemplo: frente a tienda..."><?php echo $domiEnvio_referenciasAdicionales; ?></textarea>
                  </div>
                  <p class="p-text p-text_info g-domiEnvio-referencias_adicionales_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. telefónico:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <p class="p-text p-text_help">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
                  <div class="p-columnas">
                    <div class="p-columna p-col_4_12">
                      <div class="p-control">
                        <input type="text" class="p-input g-domiEnvio-no_telefonico_input" placeholder="No. telefónico" value="<?php echo $domiEnvio_noTelefonico; ?>" autocomplete="off" maxlength="10">
                      </div>
                      <p class="p-text p-text_info g-domiEnvio-no_telefonico_alert_info_1">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>Sólo números, por favor.</span>
                      </p>
                      <p class="p-text p-text_info g-domiEnvio-no_telefonico_alert_info_2">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>El número telefónico debe de contener 10 dígitos.</span>
                      </p>
                      <p class="p-text p-text_error g-domiEnvio-no_telefonico_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>Este campo se encuentra vacío.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Rango horario de entrega:</span>
                    <span class="p-text p-text_help">(Opcional)</span>
                  </label>
                  <div class="p-columnas">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-control">
                        <input type="time" class="p-input p-date g-domiEnvio-rango_horario_entrega_1_time" value="<?php echo $domiEnvio_horaEntregaEnvio_1; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-domiEnvio-rango_horario_entrega_1_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>La hora no tiene el formato permitido.</span>
                      </p>
                      <p class="p-text p-text_error g-domiEnvio-rango_horario_entrega_1_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La hora de entrega #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>
                    <div class="p-columna">
                      <div class="p-control">
                        <input type="time" class="p-input p-date g-domiEnvio-rango_horario_entrega_2_time" value="<?php echo $domiEnvio_horaEntregaEnvio_2; ?>" autocomplete="off">
                      </div>
                      <p class="p-text p-text_info g-domiEnvio-rango_horario_entrega_2_alert_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>La hora no tiene el formato permitido.</span>
                      </p>
                      <p class="p-text p-text_error g-domiEnvio-rango_horario_entrega_2_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>La hora de entrega #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button p-button_info g-domiEnvio-guardar_cambios_button">
                <span>
                  <i class="fas fa-save"></i>
                </span>
                <span><b>Guardar cambios</b></span>
              </button>
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </a>
            </div>
          </form>
<?php
  }
?>
        </div>
      </div>
    </section>
<?php include dirname(__DIR__, 1) . '/templates/footer_pag.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_jquery.php'; ?>

    <script>
      var seccion = "mis_datos";
    </script>
<?php include dirname(__DIR__, 1) . '/templates/scripts_domicilioEnvio.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>