<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.history.go(-1); </script>';
}else{
  // PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
  if(isset($_SESSION['__producto_unidades__'])){
    unset($_SESSION['__producto_unidades__']);
  }

  // INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
  if(isset($_SESSION['__array_productos__'])){
    unset($_SESSION['__array_productos__']);
  }

  // INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
  if(isset($_SESSION['__contador_recarga__'])){
    unset($_SESSION['__contador_recarga__']);
  }
  
  require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
  require_once dirname(__DIR__, 1) . '/global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
  require_once dirname(__DIR__, 1) . '/conn.php'; // NECESARIO PARA NAVBAR

  $Conn_mxcomp = new Conexion_mxcomp();
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Agregar datos de facturación</title>
<?php include dirname(__DIR__, 1) . '/templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 6;
  $op_menu = 0;

  include dirname(__DIR__, 1) . '/templates/navbar.php';
?>
    
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include dirname(__DIR__, 1) . '/templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Agregar datos de facturación</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $usuario_existe = (int) $stmt->fetchColumn();

      if($usuario_existe === 1){
        //ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar(trim($codigoUsuario));
        
        try{
          $sql = "SELECT COUNT(id) FROM __direcciones WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $direccionFacturacion_existe = (int) $stmt->fetchColumn();

          if($direccionFacturacion_existe === 0){
            $sql = "SELECT Direcciones.tipoVialidad, Direcciones.nombreVialidad, Direcciones.noExterior, Direcciones.noInterior, Direcciones.codigoPostal, Direcciones.colonia, Direcciones.ciudadMunicipio, Direcciones.nombreEstado, Usuarios.nombreS, Usuarios.apellidoPaterno, Usuarios.apellidoMaterno, Usuarios.correo, Usuarios.noTelefonico1 AS noTelefonico
            FROM __usuarios Usuarios
            INNER JOIN __direcciones Direcciones ON Direcciones.idUsuario = Usuarios.id
            WHERE Usuarios.id = :idUsuario AND Usuarios.codigoUsuario = :codigoUsuario AND Direcciones.tipoDireccion = 'particular'";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
            $stmt->execute();
            $datos_facturacion = $stmt->fetch(PDO::FETCH_ASSOC);
            
            // DESENCRIPTAR LOS CAMPOS DE DATOS GENERALES
            $nombreS = desencriptar_con_clave(trim($datos_facturacion['nombreS']), $codigoUsuario_encriptado);
            $apellidoPaterno = desencriptar_con_clave(trim($datos_facturacion['apellidoPaterno']), $codigoUsuario_encriptado);
            $apellidoMaterno = desencriptar_con_clave(trim($datos_facturacion['apellidoMaterno']), $codigoUsuario_encriptado);
            $correo = desencriptar(trim($datos_facturacion['correo']));
            $noTelefonico = desencriptar_con_clave(trim($datos_facturacion['noTelefonico']), $codigoUsuario_encriptado);
            
            $nombreRazonSocial = $nombreS . ' ' . $apellidoPaterno . ' ' . $apellidoMaterno;
            
            // DESENCRIPTAR LOS CAMPOS DEL DOMICILIO PARTICULAR
            $tipoVialidad = trim($datos_facturacion['tipoVialidad']);
            $nombreVialidad = desencriptar_con_clave(trim($datos_facturacion['nombreVialidad']), $codigoUsuario_encriptado);
            $noExterior = desencriptar_con_clave(trim($datos_facturacion['noExterior']), $codigoUsuario_encriptado);
            $noInterior = desencriptar_con_clave(trim($datos_facturacion['noInterior']), $codigoUsuario_encriptado);
            $codigoPostal = desencriptar_con_clave(trim($datos_facturacion['codigoPostal']), $codigoUsuario_encriptado);
            $colonia = desencriptar_con_clave(trim($datos_facturacion['colonia']), $codigoUsuario_encriptado);
            $ciudadMunicipio = desencriptar_con_clave(trim($datos_facturacion['ciudadMunicipio']), $codigoUsuario_encriptado);
            $nombreEstado = desencriptar_con_clave(trim($datos_facturacion['nombreEstado']), $codigoUsuario_encriptado);
            
            $noExterior = $noExterior === 'S/N' ? NULL : $noExterior;
            $noInterior = $noInterior === 'S/N' ? NULL : $noInterior;
?>
          <p class="p-text p-text_help p-text_help_margin">
            <span>Los datos cargados por defecto son del domicilio particular. Revísalos por si la información de facturación es diferente.</span>
          </p>
          
          <p class="p-text p-text_help p-text_help_margin p-text-align_right">
            <span>* Obligatorio</span>
          </p>
          
          <div class="p-notification_contenedor g-facturacion-status">
            <div class="p-notification p-notification_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <span class="p-notification_p"><b>Datos agregados correctamente.</b></span>
            </div>
          </div>
          
          <form id="id-facturacion-form_agregar">
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre o razón social:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-input_focus g-facturacion-nombreRazonSocial_input" placeholder="Nombre o razón social" value="<?php echo $nombreRazonSocial; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-facturacion-nombreRazonSocial_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-nombreRazonSocial_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>
            
            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>RFC:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-RFC_input" placeholder="RFC" autocomplete="off" maxlength="13" style="text-transform: uppercase;">
                  </div>
                  <p class="p-text p-text_info g-facturacion-RFC_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números y letras, por favor.</span>
                  </p>
                  <p class="p-text p-text_info g-facturacion-RFC_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Debe de contener 13 caracteres.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-RFC_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Uso de la factura:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-facturacion-usoFactura_div">
                      <select class="p-input g-form_select g-facturacion-usoFactura_select">
                        <option selected="selected" value="">Selecciona una opción</option>
<?php
            try{
              $sql = "SELECT id, clave, nombreUsoFactura FROM __uso_factura";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->execute();
              
              while($datos_usoFactura = $stmt->fetch(PDO::FETCH_ASSOC)){
                $usoFactura_id = trim($datos_usoFactura['id']);
                $usoFactura_clave = trim($datos_usoFactura['clave']);
                $usoFactura_nombre = trim($datos_usoFactura['nombreUsoFactura']);
                
                $nombreCompleto_usoFactura = $usoFactura_clave . ' ' . $usoFactura_nombre;
?>
                        <option value="<?php echo $usoFactura_id; ?>"><?php echo $nombreCompleto_usoFactura; ?></option>
<?php
              }
            }catch(PDOEXception $error){
              //$mensaje_error = $error->getMessage();
              $mensaje_error = "Problema al buscar los usos de factura";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
            }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-facturacion-usoFactura_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Esta opción no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-usoFactura_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado una opción.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Tipo de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-facturacion-tipoVialidad_div">
                      <select class="p-input g-form_select g-facturacion-tipoVialidad_select">
                        <option value="">Selecciona el tipo</option>
<?php
            try{
              $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->execute();
              
              while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
                $selected = $tipoVialidad === trim($datos_tipoVialidad['nombreTipoVialidad']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_tipoVialidad['id']); ?>"><?php echo trim($datos_tipoVialidad['nombreTipoVialidad']); ?></option>
<?php
              }
            }catch(PDOEXception $error){
              //$mensaje_error = $error->getMessage();
              $mensaje_error = "Problema al buscar los tipos de vialidad";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
            }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-facturacion-tipoVialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este tipo de vialidad no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-tipoVialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un tipo de vialidad.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Nombre de vialidad:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-nombreVialidad_input" placeholder="Nombre de vialidad" value="<?php echo $nombreVialidad; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-facturacion-nombreVialidad_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-nombreVialidad_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. exterior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
            if(is_null($noExterior)){
?>
                    <input type="text" class="p-input g-form-noExterior_input g-facturacion-noExterior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
            }else{
?>
                    <input type="text" class="p-input g-form-noExterior_input g-facturacion-noExterior_input" placeholder="No. exterior" value="<?php echo $noExterior; ?>" autocomplete="off">
<?php
            }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noExterior_alert_info g-facturacion-noExterior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noExterior_alert_error g-facturacion-noExterior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
            if(is_null($noExterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-facturacion-noExterior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Agregar no. exterior</label>
<?php
            }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-facturacion-noExterior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>
<?php
            }
?>
                </div>
              </div>
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. interior:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
<?php
            if(is_null($noInterior)){
?>
                    <input type="text" class="p-input g-form-noInterior_input g-facturacion-noInterior_input" placeholder="S/N" autocomplete="off" disabled>
<?php
            }else{
?>
                    <input type="text" class="p-input g-form-noInterior_input g-facturacion-noInterior_input" placeholder="No. interior" value="<?php echo $noInterior; ?>" autocomplete="off">
<?php
            }
?>
                  </div>
                  <p class="p-text p-text_info g-form-noInterior_alert_info g-facturacion-noInterior_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                  </p>
                  <p class="p-text p-text_error g-form-noInterior_alert_error g-facturacion-noInterior_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
<?php
            if(is_null($noInterior)){
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-facturacion-noInterior_checkbox" checked>
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Agregar no. interior</label>
<?php
            }else{
?>
                  <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-facturacion-noInterior_checkbox">
                  <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>
<?php
            }
?>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-col_4_12">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Código postal:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-codigoPostal_input" placeholder="Código postal" value="<?php echo $codigoPostal; ?>" autocomplete="off" maxlength="5">
                  </div>
                  <p class="p-text p-text_info g-facturacion-codigoPostal_alert_info_1">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo números, por favor.</span>
                  </p>
                  <p class="p-text p-text_info g-facturacion-codigoPostal_alert_info_2">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El código postal debe de contener 5 números.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-codigoPostal_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna p-dos_columnas">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Colonia:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-colonia_input" placeholder="Colonia" value="<?php echo $colonia; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-facturacion-colonia_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-colonia_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
              
              <div class="p-columna p-columna-marginTop_1rem">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Ciudad o municipio:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-ciudadMunicipio_input" placeholder="Ciudad o municipio" value="<?php echo $ciudadMunicipio; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-facturacion-ciudadMunicipio_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Sólo letras y espacios, por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-ciudadMunicipio_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Estado:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <div class="p-select g-facturacion-estado_div">
                      <select class="p-input g-form_select g-facturacion-estado_select">
                        <option value="">Selecciona tu estado</option>
<?php
            try{
              $sql = "SELECT id, nombreEstado FROM __estados_codigos";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->execute();
              
              while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
                $selected = $nombreEstado === trim($datos_estado['nombreEstado']) ? 'selected="selected"' : '';
?>
                        <option <?php echo $selected; ?> value="<?php echo trim($datos_estado['id']); ?>"><?php echo trim($datos_estado['nombreEstado']); ?></option>
<?php
              }
            }catch(PDOEXception $error){
              //$mensaje_error = $error->getMessage();
              $mensaje_error = "Problema al buscar los estados";
?>
                        <option value=""><?php echo $mensaje_error; ?></option>
<?php
            }
?>
                      </select>
                    </div>
                  </div>
                  <p class="p-text p-text_info g-facturacion-estado_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>Este estado no existe.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-estado_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>No has seleccionado un estado.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>Correo para enviar tu factura:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-facturacion-correoFactura_input" placeholder="Correo para enviar tu factura" value="<?php echo $correo; ?>" autocomplete="off">
                  </div>
                  <p class="p-text p-text_info g-facturacion-correoFactura_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El correo no cumple con el formato estándar, revísalo por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-facturacion-correoFactura_alert_error">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="p-columnas p-field_marginBottom">
              <div class="p-columna">
                <div class="p-field">
                  <label class="p-label p-text_p">
                    <span>No. telefónico:</span>
                    <span class="p-text p-text_help">*</span>
                  </label>
                  <p class="p-text p-text_help">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
                  <div class="p-columnas">
                    <div class="p-columna p-col_4_12">
                      <div class="p-control">
                        <input type="text" class="p-input g-facturacion-noTelefonico_input" placeholder="No. telefónico" value="<?php echo $noTelefonico; ?>" autocomplete="off" maxlength="10">
                      </div>
                      <p class="p-text p-text_info g-facturacion-noTelefonico_alert_info_1">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                       <span>Sólo números, por favor.</span>
                      </p>
                      <p class="p-text p-text_info g-facturacion-noTelefonico_alert_info_2">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span>El número telefónico debe de contener 10 dígitos.</span>
                      </p>
                      <p class="p-text p-text_error g-facturacion-noTelefonico_alert_error">
                        <span>
                          <i class="fas fa-times-circle"></i>
                        </span>
                        <span>Este campo se encuentra vacío.</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-field_marginTop p-buttons p-buttons_right p-cuenta-contenedor_botones">
              <button type="submit" class="p-button p-button_success">
                <span>
                  <i class="fas fa-plus"></i>
                </span>
                <span><b>Agregar datos</b></span>
              </button>
              <button type="button" class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-times"></i>
                </span>
                <span><b>Cancelar</b></span>
              </button>
            </div>
          </form>
<?php
          }else{
?>
          <p class="p-text_p">
            <b>Ya se tiene datos de facturación agregados.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
          }
        }catch(PDOEXception $error){
          //$mensaje_error = $error->getMessage();
          $mensaje_error = "Problema al buscar la información de facturación.";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
        }
      }else{
?>
          <p class="p-text_p">
            <b>El usuario no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$mensaje_error = $error->getMessage();
      $mensaje_error = "Problema al buscar el usuario";
?>
          <p class="p-notification p-notification_letter_error">
            <b><?php echo $mensaje_error; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
<?php
    }
  }
?>
        </div>
      </div>
    </section>
<?php include dirname(__DIR__, 1) . '/templates/footer_pag.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_jquery.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/scripts_facturacion.php'; ?>

<?php include dirname(__DIR__, 1) . '/templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}
?>