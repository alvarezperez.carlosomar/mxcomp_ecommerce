<?php
session_start();
if(!isset($_SESSION['__id__'])){
  echo '<script> window.location = "/"; </script>';
}

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Mi cuenta</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 5;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Mi cuenta</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-box-options_contenedor">
            <a href="mis-compras" class="p-box-options_cuenta_a" title="Mis compras">
              <div class="p-box-options_opcion p-box-options_cuenta_opcion">
                <span>
                  <i class="fas fa-2x fa-dolly"></i>
                </span>
                <span>Mis compras</span>
              </div>
            </a>
            <!--<a class="p-box-options_cuenta_a" title="Productos favoritos">
              <div class="p-box-options_opcion p-box-options_cuenta_opcion">
                <span>
                  <i class="fas fa-2x fa-heart"></i>
                </span>
                <span>Productos favoritos</span>
              </div>
            </a>-->
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>