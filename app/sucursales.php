<?php
session_start();
include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Sucursales</title>
    <meta name="description" content="Se muestran las sucursales físicas con la ubicación y el horario de atención.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, ecommerce, sucursales">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 8;
$op_menu = 2;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-atencionClientes-fondo">
          <h1 class="p-titulo">Sucursales</h1>
          <h2 class="p-subtitulo p-text_p">Nos puedes encontrar en las siguientes sucursales físicas:</h2>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
$conexion_siga = new Conexion_siga();

try{
  $id_emp_sf = 8;
  $tipo_suc_f = 'SUCURSAL';
  
  $sql = "SELECT sucursal, calle, no_ext, no_int, cp, colonia, municipio, estado, tel1, tel2, tel3 FROM sucursal_fija WHERE id_emp_sf = :id_emp_sf AND tipo_suc_f = :tipo_suc_f";
  $stmt = $conexion_siga->pdo->prepare($sql);
  $stmt->bindParam(':id_emp_sf', $id_emp_sf, PDO::PARAM_INT);
  $stmt->bindParam(':tipo_suc_f', $tipo_suc_f, PDO::PARAM_STR);
  $stmt->execute();
  
  while($datos = $stmt->fetch(PDO::FETCH_ASSOC)){
    $sucursal = $datos['sucursal'];
    $calle = ucwords(strtolower($datos['calle']));
    $no_ext = $datos['no_ext'];
    $no_int = $datos['no_int'];
    $cp = $datos['cp'];
    $colonia = ucwords(strtolower($datos['colonia']));
    $municipio = ucwords(strtolower($datos['municipio']));
    $estado = ucwords(strtolower($datos['estado']));
    $tel1 = $datos['tel1'];
    $tel2 = $datos['tel2'];
    $tel3 = $datos['tel3'];
    
    $acento_mayusculas = ["Á", "É", "Í", "Ó", "Ú"];
    $acento_minusculas = ["á", "é", "í", "ó", "ú"];
    
    $calle = str_replace($acento_mayusculas, $acento_minusculas, $calle);
    $colonia = str_replace($acento_mayusculas, $acento_minusculas, $colonia);
    $municipio = str_replace($acento_mayusculas, $acento_minusculas, $municipio);
    $estado = str_replace($acento_mayusculas, $acento_minusculas, $estado);
    
    $direccion = $calle . " No. Ext. " . $no_ext . ", No. Int. " . $no_int . ", C.P. " . $cp . ", " . $colonia . ", " . $municipio . ", " . $estado . ".";
?>
          <div class="p-box-productos_contenedor_elementos p-cuenta-divLargo_contenedor_elementos p-cuenta-divLargo_contenedor_elementos_fondo">
            <div class="p-cuenta-divLargo_contenedor_div">
              <div class="p-box-productos_title">
                <h3 class="p-box-productos_title_h3 p-cuenta-divLargo_titulo"><?php echo $sucursal; ?></h3>
              </div>
              <div class="p-box-productos_info p-cuenta-divLargo_box_info">
                <div class="p-box-productos_info_div_1 p-cuenta-divLargo_box_info_div">
                  <p class="p-text_p">
                    <span>
                      <i class="fas fa-lg fa-map-marker-alt"></i>
                    </span>
                    <span><?php echo $direccion; ?></span>
                  </p>
                  
                  <div class="p-buttons p-atencionClientes-contenedor_botones p-margin-top_1rem">
                    <a class="p-button p-button_account" href="tel:+52<?php echo $tel1; ?>">
                      <span>
                        <i class="fas fa-mobile-alt"></i>
                      </span>
                      <span><b><?php echo $tel1; ?></b></span>
                    </a>
<?php
    if(!is_null($tel2) && $tel2 !== ""){
?>
                    <a class="p-button p-button_account" href="tel:+52<?php echo $tel2; ?>">
                      <span>
                        <i class="fas fa-mobile-alt"></i>
                      </span>
                      <span><b><?php echo $tel2; ?></b></span>
                    </a>
<?php
    }

    if(!is_null($tel3) && $tel3 !== ""){
?>
                    <a class="p-button p-button_account" href="tel:+52<?php echo $tel3; ?>">
                      <span>
                        <i class="fas fa-mobile-alt"></i>
                      </span>
                      <span><b><?php echo $tel3; ?></b></span>
                    </a>
<?php
    }
?>
                  </div>
                  
                  <p class="p-text_p">
                    <span>
                      <i class="far fa-clock"></i>
                    </span>
                    <span>Lunes a Sábado: 9:00 am - 7:00 pm</span>
                  </p>
                  
                  <p class="p-text_p">
                    <span>
                      <i class="far fa-clock"></i>
                    </span>
                    <span>Domingo: 11:00 am - 5:00 pm</span>
                  </p>
                  
                  <div class="p-notification_contenedor g-horario-sucursal_abierta">
                    <div class="p-notification p-notification_letter_success">
                      <span><b>Sucursal abierta</b></span>
                    </div>
                  </div>
                  
                  <div class="p-notification_contenedor g-horario-sucursal_cerrada">
                    <div class="p-notification p-notification_letter_error">
                      <span><b>Sucursal cerrada</b></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<?php
  }
  
  $stmt = null;
}catch(PDOException $error){
  //$mensaje = "Error: " . $error->getMessage();
  $mensaje = "No se encontraron las sucursales. Contacta con atención a clientes.";
?>
          <div class="p-notification p-notification_letter_error">
            <span>
              <i class="fas fa-times-circle"></i>
            </span>
            <span class="p-notification_p"><b><?php echo $mensaje; ?></b></span>
          </div>
<?php
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

    <script>
      setTimeout(function(){
        revisarHorario();
      }, 0);

      setInterval(function(){
        revisarHorario();
      }, 1000);

      function formato_AMPM(fecha){
        let horas = fecha.getHours(), minutos = fecha.getMinutes(), dia_semana = fecha.getDay(), am_pm = horas >= 12 ? 'pm' : 'am';

        horas = horas < 10 ? '0' + horas : horas;
        minutos = minutos < 10 ? '0' + minutos : minutos;

        let hora_completa = horas + ':' + minutos;

        // 0 = Domingo, 1 = Lunes, 2 = Martes, 3 = Miercoles, 4 = Jueves, 5 = Viernes, 6 = Sábado
        // SI ES DOMINGO
        if(dia_semana === 0){
          return am_pm === 'am' ? ( hora_completa >= '11:00' ? true : false ) : ( hora_completa < '17:00' ? true : false );
        }else{
          return am_pm === 'am' ? ( hora_completa >= '09:00' ? true : false ) : ( hora_completa < '19:00' ? true : false );
        }
      }

      function revisarHorario(){
        let fecha = new Date(), sucursal_abierta = formato_AMPM(fecha);

        if(sucursal_abierta){
          $('document, .g-horario-sucursal_abierta').slideDown('fast');
          $('document, .g-horario-sucursal_cerrada').slideUp('fast');
        }else{
          $('document, .g-horario-sucursal_abierta').slideUp('fast');
          $('document, .g-horario-sucursal_cerrada').slideDown('fast');
        }
      }
    </script>
  </body>  
</html>