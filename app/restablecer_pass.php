<?php
session_start();
if(isset($_SESSION['__id__'])){
  echo '<script> window.location = "/"; </script>';
}

require_once 'funciones/encriptacion.php';
require_once 'global/config.php'; // NECESARIO PARA HEAD
require_once 'conn.php';

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}

$Conn_mxcomp = new Conexion_mxcomp();
$error = false;
$variables_get = false;
$correo = "";
$token = "";

if(isset($_GET['correo']) && isset($_GET['token'])){
  $correo = trim($_GET['correo']);
  $token = trim($_GET['token']);
  
  try{
    // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
    $correo_consulta = encriptar($correo);
    
    $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY correo = :correo AND token = :token";
    $stmt = $Conn_mxcomp->pdo->prepare($sql);
    $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
    $stmt->bindParam(':token', $token, PDO::PARAM_STR);
    $stmt->execute();
    $cuenta_existe = (int) $stmt->fetchColumn();
    
    $datos_validos = $cuenta_existe === 1 ? true : false;

    $stmt = null;
  }catch(PDOException $e){
    $error = true;
  }
  
  $variables_get = true;
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php
if($variables_get && $error){
?>
    <title>Ocurrió un error</title>
<?php
}else{
?>
    <title>Restablecer contraseña</title>
<?php
}
?>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;

include 'templates/navbar_formularios.php';
?>

    <section class="p-section-columns">
      <div class="p-contenido-contenedor_formularios">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Restablecer contraseña</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-section-div_formularios_contenedor">
<?php
if($variables_get){
  if($error){
?>
            <h3 class="p-text_p">Ocurrió un error, recarga la página o vuelve a realizar el proceso de restablecer contraseña.</h3>
            <p class="p-text_p">Si este mensaje sigue apareciendo, contacta con <a class="p-text_p p-link_texto" href="atencion-clientes"><b>atención a clientes</b></a>.</p>
<?php
  }else{
    if($datos_validos){
?>
            <div class="p-loading-general g-restablecerPassword-loading">
              <div></div>
              <p><b>Procesando solicitud...</b></p>
            </div>

            <div class="g-restablecerPassword-contenedor_notification"></div>

            <p class="p-text p-text_help p-text_help_margin">
              <span>Ingresa la nueva contraseña que usarás en tu cuenta de MXcomp.</span>
            </p>

            <form class="g-restablecerPassword-password_form">
              <input type="hidden" readonly autocomplete="off" disabled value="<?php echo $correo; ?>" class="g-restablecerPassword-correo_input">
              <input type="hidden" readonly autocomplete="off" disabled value="<?php echo $token; ?>" class="g-restablecerPassword-token_input">
              
              <div class="p-columnas p-field_marginBottom">
                <div class="p-columna">
                  <div class="p-field">
                    <label class="p-label p-text_p">
                      <span>Nueva contraseña:</span>
                      <span class="p-button_help g-button_help" id="g-nuevaPassword-ayuda_span">
                        <i class="fas fa-question"></i>
                      </span>
                      <span class="p-text p-text_help">*</span>
                    </label>
                    <p class="p-text p-text_help_square g-nuevaPassword-ayuda_span">
                      <span>Debe tener una longitud mínima de 8 caracteres y tener al menos: 1 número, 1 letra minúscula, 1 letra mayúscula y/o 1 símbolo como <i>! " @ # $ % * _</i></span>
                    </p>
                    <p class="p-text p-text_help">No utilices tu nombre, tu fecha de nacimiento o algún otro dato privado.</p>
                    <div class="p-control">
                      <input type="password" class="p-input g-input_focus g-nuevaPassword-mostrar_button g-restablecerPassword-nueva_password_input" placeholder="Nueva contraseña" autocomplete="off">

                      <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-nuevaPassword-mostrar_button">
                        <span>
                          <i class="fas fa-eye"></i>
                        </span>
                        <span>
                          <i class="fas fa-eye-slash"></i>
                        </span>
                      </a>
                    </div>
                    <p class="p-text p-text_info g-restablecerPassword-nueva_password_alert_info">
                      <span>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                    </p>
                    <p class="p-text p-text_error g-restablecerPassword-nueva_password_alert_error">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este campo se encuentra vacío.</span>
                    </p>
                    <div class="p-password-seguridad_contenedor">
                      <label class="p-password-seguridad_label">Seguridad: </label>
                      <p class="p-password-seguridad_p g-nuevaPassword-porcentaje_seguridad">0%</p>
                      <div class="p-password-seguridad_div">
                        <div class="p-password-seguridad_div_progreso g-nuevaPassword-barra_progreso"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="p-columnas p-field_marginBottom">
                <div class="p-columna">
                  <div class="p-field">
                    <label class="p-label p-text_p">
                      <span>Confirmar contraseña:</span>
                      <span class="p-text p-text_help">*</span>
                    </label>
                    <div class="p-control">
                      <input type="password" class="p-input g-confirmarPassword-mostrar_button g-restablecerPassword-confirmar_password_input" placeholder="Confirmar contraseña" autocomplete="off">

                      <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-confirmarPassword-mostrar_button">
                        <span>
                          <i class="fas fa-eye"></i>
                        </span>
                        <span>
                          <i class="fas fa-eye-slash"></i>
                        </span>
                      </a>
                    </div>
                    <p class="p-text p-text_info g-restablecerPassword-confirmar_password_alert_info">
                      <span>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                    </p>
                    <p class="p-text p-text_error g-restablecerPassword-confirmar_password_alert_error">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este campo se encuentra vacío.</span>
                    </p>
                  </div>
                </div>
              </div>
              
              <button type="submit" class="p-button p-button_info p-button_largo g-restablecerPassword-restablecer_button">
                <span>
                  <i class="fas fa-key"></i>
                </span>
                <span><b>Restablecer contraseña</b></span>
              </button>
            </form>
<?php
    }else{
?>
            <p class="p-text_p">Los datos que se recibieron son incorrectos, vuelve a realizar el proceso de restablecer contraseña.</p>
            <p class="p-text_p">En caso de que siga apareciendo este mensaje contacte con <a class="p-text_p p-link_texto" href="atencion-clientes"><b>atención a clientes</b></a>.</p>
<?php
    }
  }
}else{
?>
            <div class="p-loading-general g-restablecerPassword-loading">
              <div></div>
              <p><b>Procesando solicitud...</b></p>
            </div>

            <div class="g-restablecerPassword-contenedor_notification"></div>

            <p class="p-text p-text_help p-text_help_margin">
              <span>Ingresa el correo electrónico con el que creaste tu cuenta para enviarte un mail con las instrucciones ha seguir. Si no has activado tu cuenta y no llegó el correo de activación, puedes reenviarlo de nuevo desde esta sección.</span>
            </p>

            <form class="g-restablecerPassword-correo_form">
              <div class="p-columnas p-field_marginBottom">
                <div class="p-columna">
                  <div class="p-field">
                    <label class="p-label p-text_p">
                      <span>Correo electrónico:</span>
                      <span class="p-text p-text_help">*</span>
                    </label>
                    <div class="p-control">
                      <input type="text" class="p-input g-input_focus g-restablecerPassword-correo_input" placeholder="Correo electrónico" autocomplete="off">
                    </div>
                    <p class="p-text p-text_info g-restablecerPassword-correo_alert_info">
                      <span>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      <span>No cumple con el formato estándar, revísalo por favor.</span>
                    </p>
                    <p class="p-text p-text_error g-restablecerPassword-correo_alert_error_1">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este correo no se encuentra registrado.</span>
                    </p>
                    <p class="p-text p-text_error g-restablecerPassword-correo_alert_error_2">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este campo se encuentra vacío.</span>
                    </p>
                  </div>
                </div>
              </div>
              
              <button type="submit" class="p-button p-button_info p-button_largo g-restablecerPassword-enviar_correo_button">
                <span>
                  <i class="fas fa-envelope"></i>
                </span>
                <span><b>Enviar correo</b></span>
              </button>
            </form>
            <div class="p-buttons p-buttons_right p-buttons_margin_top">
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-undo-alt"></i>
                </span>
                <span><b>Regresar</b></span>
              </a>
            </div>
<?php
}
?>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag_formularios.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

    <script src="scripts/restablecer_password/scripts_restablecer_password.js?v=2.5"></script>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>