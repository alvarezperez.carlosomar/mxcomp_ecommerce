<?php
session_start();

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// VARIABLE DE SESSION PARA SABER SI SE RECARGO INDEX.PHP
// SI EXISTE LA VARIABLE, SE AUMENTA 1
if(isset($_SESSION['__contador_recarga__'])){
  $_SESSION['__contador_recarga__'] += 1;
}else{
  // SI NO EXISTE SE INICIA A 1
  $_SESSION['__contador_recarga__'] = 1;
}

// SI EL VALOR DE LA VARIABLE SESSION ES 2, REINICIAMOS LA VARIABLE A 1 Y ELIMINAMOS EL ARRAY DE PRODUCTOS
if($_SESSION['__contador_recarga__'] === 2){
  $_SESSION['__contador_recarga__'] = 1;
  unset($_SESSION['__array_productos__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>MXcomp; la tecnología en tus manos</title>
    <meta name="description" content="MXcomp es una división de negocio de MEXIcomp que ofrece productos para su compra online. Encontrarás desde laptops, bocinas, impresoras, y mucho más.">
    <meta name="keywords" content="tecnología, drones, impresoras, mouse, móviles, software, hardware, laptops, computadora, MXcomp, MEXIcomp, mxcomp, mexicomp, ventas en linea, ecommerce">
<?php include 'templates/head.php';?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 1;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php';?>

      </div>
      <div class="p-contenido-contenedor">

        <div class="p-carrusel-contenedor">
          <ul class="p-carrusel-imagenes">
            <li>
              <picture>
                <source media="(max-width: 426px)" srcset="images/banners/banner-productos_existentes_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-productos_existentes.jpg?v=1.0" alt="Los mejores regalos">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-compra_online_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-compra_online.jpg?v=1.0" alt="Comprar online">
              </picture>
            </li>
<?php
            /* <li>
              <a href="VI-663178-1/control-bt-gamepad-acteck-g200win-ios-android-switch-negro-ac-929837/">
                <picture>
                  <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                  <source media="(max-width: 426px)" srcset="images/banners/banner-producto_VI-663178-1_m.jpg?v=1.0" type="image/jpg">
                  <img src="images/banners/banner-producto_VI-663178-1.jpg?v=1.0" alt="Producto: VI-663178-1">
                </picture>
              </a>
            </li> */
?>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-marcas_m.gif?v=1.0" type="image/jpg">
                <img src="images/banners/banner-marcas.gif?v=1.0" alt="Marcas">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-GameFactor_1_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-GameFactor_1.jpg?v=1.0" alt="Game Factor">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-envios_gratis_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-envios_gratis.jpg?v=1.0" alt="Envíos gratis">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-Vorago_conectando_contigo_1_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-Vorago_conectando_contigo_1.jpg?v=1.0" alt="Vorago: Conectando contigo">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-envios_asegurados_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-envios_asegurados.jpg?v=1.0" alt="Envíos asegurados">
              </picture>
            </li>
            <li>
              <picture>
                <!--<source srcset="images/03-slider.webp" type="image/webp">-->
                <source media="(max-width: 426px)" srcset="images/banners/banner-garantia_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-garantia.jpg?v=1.0" alt="Año de garantía">
              </picture>
            </li>
            <li>
              <picture>
                <source media="(max-width: 426px)" srcset="images/banners/banner-ensambles_cotizaciones_m.jpg?v=1.0" type="image/jpg">
                <img src="images/banners/banner-ensambles_cotizaciones.jpg?v=1.0" alt="Ensambles y cotizaciones">
              </picture>
            </li>
          </ul>
          <div class="p-carrusel-flecha_izquierda">
            <span>
              <i class="fas fa-angle-left"></i>
            </span>
          </div>
          <div class="p-carrusel-flecha_derecha">
            <span>
              <i class="fas fa-angle-right"></i>
            </span>
          </div>
        </div>
        <ol class="p-carrusel-paginacion"></ol>

        <section class="p-index-fondo p-index-padding">
          <h1 class="p-titulo">MXcomp; la tecnología en tus manos</h1>
        </section>

        <div class="p-index-section">
<?php
$Conn_mxcomp = new Conexion_mxcomp();

try{
  $error_existe = false;
  if(!isset($_SESSION['__array_productos__'])){
    $sql = "SELECT MAX(id) FROM __productos";
    $stmt = $Conn_mxcomp->pdo->prepare($sql);
    $stmt->execute();
    $max_id = (int) $stmt->fetchColumn();

    $x = 0;
    $IDs_generados = array();
    $error_existe = false;
    while($x < 50){
      $num_aleatorio = (int) rand(1, $max_id);
      if(!in_array($num_aleatorio, $IDs_generados)){
        try{
          //$sql = "SELECT COUNT(id) FROM __productos WHERE BINARY id = :id AND existenciaTotal != 0 AND activo != 0 AND tieneImagen = '1'";
          $sql = "SELECT COUNT(id) FROM __productos WHERE BINARY id = :id AND existenciaTotal != 0 AND activo != 0";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':id', $num_aleatorio, PDO::PARAM_INT);
          $stmt->execute();
          $id_encontrado = $stmt->fetchColumn();
          $id_encontrado = (int) $id_encontrado;

          if($id_encontrado === 1){
            array_push($IDs_generados, $num_aleatorio);
            $x++;
          }
        }catch(PDOException $error){
          $error_existe = true;
          //$error_msg = "Error: " . $error->getMessage();
          $error_msg = "Error al revisar si el producto existe.";
?>
          <p class="p-text_p">
            <b><?php echo $error_msg; ?></b>
          </p>
<?php
          break;
        }
      }
    }

    if(!$error_existe){
      $x = 0;
      while($x < 50){
        $_SESSION['__array_productos__'][$x] = $IDs_generados[$x];
        $x++;
      }
      unset($IDs_generados);
    }
  }
?>
          <div class="p-columns-products_container">
<?php
  if($error_existe === false){
    foreach($_SESSION['__array_productos__'] as $indice=>$id){
      try{
        $sql = "SELECT codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioMXcomp, monedaMXcomp, numeroUbicacionImagen, nombreImagen, versionImagen FROM __productos WHERE BINARY id = :id";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);

        $codigoProducto = trim($datos_producto['codigoProducto']);
        $skuProveedor = trim($datos_producto['skuProveedor']);
        $nombreMarca = trim($datos_producto['nombreMarca']);
        $numeroUbicacionImagen = $datos_producto['numeroUbicacionImagen']; // PARA REVISAR SI ES NULL
        $nombreImagen = trim($datos_producto['nombreImagen']);
        $versionImagen = trim($datos_producto['versionImagen']);

        $descripcion = trim($datos_producto['descripcion'], " \xC2\xA0");
        $descripcion_editada = str_replace('"', '', $descripcion);
        $descripcionURL = trim($datos_producto['descripcionURL']);

        // SI ES NULL NO TIENE UBICACION LA IMAGEN
        if(is_null($numeroUbicacionImagen)){
          $imagen = "images/no_imagen_disponible.png";
          $alt = "Imagen no disponible.";
        }else{
          $numeroUbicacionImagen = (int) $numeroUbicacionImagen;

          $alt_editado = ucwords(strtolower( $descripcion_editada . ', ' . $nombreMarca));
          $alt = $alt_editado;

          switch($numeroUbicacionImagen){
            case 1: // UBICACION NUEVA
              $imagen = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '.jpg';
              break;
          }
        }
?>
            <div class="p-column-product">
              <div class="p-column-product_card">
<?php
        if(isset($_SESSION['__id__'])){
                /*<div class="p-column-product_card_contenedor_fav">
                  <a class="p-column-product_card_fav">
                    <span>
                      <i class="far fa-heart"></i>
                    </span>
                    <span>
                      <i class="fas fa-heart"></i>
                    </span>
                  </a>
                </div>*/
        }
?>
                <a href="<?php echo $codigoProducto . '/' . $descripcionURL . '/'; ?>" title="<?php echo $descripcion_editada; ?>">
                  <figure class="p-text-align_center">
                    <picture>
                      <!--<source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                      <img src="<?php echo $imagen . '?v=' . $versionImagen; ?>" alt="<?php echo $alt; ?>" width="154">
                    </picture>
                  </figure>
                  <div class="p-column-product_card_content">
                    <div class="p-column-product_card_content_info">
                      <p class="p-column-product_title_prod"><?php
        $lim = 60;
        $tam = mb_strlen($descripcion);
        if($tam <= $lim){
          echo $descripcion;
        }else{
          $n_txt = mb_substr($descripcion, 0, $lim, 'UTF-8');
          echo $n_txt . '...';
        }
        ?></p>
                      <p class="p-column-product_precio_color">
                        <span>$ <?php echo number_format($datos_producto['precioMXcomp'], 2, '.', ','); ?></span>
                        <span><?php echo $datos_producto['monedaMXcomp']; ?></span>
                      </p>
                    </div>
                  </div>
                </a>
              </div>
            </div>
<?php
      }catch(PDOException $error){
        $error_existe = true;
        //$error_msg = "Error: " . $error->getMessage();
        $error_msg = "Error al buscar información de los productos.";
?>
            <p class="p-text_p">
              <b><?php echo $error_msg; ?></b>
            </p>
<?php
        break;
      }
    }
  }
?>
          </div>
<?php
  $stmt = null;
}catch(PDOException $error){
  //$error_msg = "Error: " . $error->getMessage();
  $error_msg = "Error al generar los productos a mostrar.";
?>
          <p class="p-text_p">
            <b><?php echo $error_msg; ?></b>
          </p>
<?php
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php';?>

<?php include 'templates/footer_scripts_jquery.php';?>

    <script src="scripts/carrusel.js?v=1.9"></script>
<?php include 'templates/footer_scripts_principales.php';?>

  </body>
</html>