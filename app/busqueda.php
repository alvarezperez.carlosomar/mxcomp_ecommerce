<?php
session_start();
require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

$Conn_mxcomp = new Conexion_mxcomp();

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}

//VARIABLES CON VALORES DE GET
$opcion = "";
if(!isset($_GET['buscar']) && !isset($_GET['opcion'])){
  echo '<script> history.go(-1); </script>';
}else{
  if( isset($_GET['opcion']) ){
    
    $opcion = trim($_GET['opcion']);
    unset($_GET['opcion']);
    
    if($opcion === ""){
      echo '<script> history.go(-1); </script>';
    }else{
      $ejecutar_consulta = false;
      
      if($opcion === "catalogo"){
        if(isset($_SESSION['__terminos_buscados'])){
          unset($_SESSION['__terminos_buscados']);
          unset($_SESSION['__productosBusqueda__']);
          unset($_SESSION['__productosImprimir_Busqueda__']);
          unset($_SESSION['__categorias_Busqueda__']);
          unset($_SESSION['__subcat_tipos_Busqueda__']);
          unset($_SESSION['__subcat_especificas_Busqueda__']);
          unset($_SESSION['__marcas_Busqueda__']);
          unset($_SESSION['__productos_por_pagina__']);
          unset($_SESSION['__total_productos__']);
          unset($_SESSION['__pagina_actual']);
          unset($_SESSION['__categoria_seleccionada']);
          unset($_SESSION['__subcat_tipo_seleccionadas']);
          unset($_SESSION['__subcat_especificas_seleccionadas']);
          unset($_SESSION['__marca_seleccionada']);
          unset($_SESSION['__rango_precios__']);
          unset($_SESSION['__activacion_precio_menor_mayor']);
          unset($_SESSION['__activacion_precio_mayor_menor']);
        }
        
        if(isset($_SESSION['__palabra_clave_opcion__'])){
          if((string) $_SESSION['__palabra_clave_opcion__'] !== "sinPalabras_catalogo" || $_SESSION['__opcion_busqueda__'] !== $opcion){
            $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_catalogo";
            $_SESSION['__opcion_busqueda__'] = "catalogo";
            unset($_SESSION['__id_categoria__']);
            unset($_SESSION['__id_marca__']);
            unset($_SESSION['__productosBusqueda__']);
            unset($_SESSION['__productosImprimir_Busqueda__']);
            unset($_SESSION['__categorias_Busqueda__']);
            unset($_SESSION['__subcat_tipos_Busqueda__']);
            unset($_SESSION['__subcat_especificas_Busqueda__']);
            unset($_SESSION['__marcas_Busqueda__']);
            unset($_SESSION['__productos_por_pagina__']);
            unset($_SESSION['__total_productos__']);
            unset($_SESSION['__pagina_actual']);
            unset($_SESSION['__categoria_seleccionada']);
            unset($_SESSION['__subcat_tipo_seleccionadas']);
            unset($_SESSION['__subcat_especificas_seleccionadas']);
            unset($_SESSION['__marca_seleccionada']);
            unset($_SESSION['__rango_precios__']);
            unset($_SESSION['__activacion_precio_menor_mayor']);
            unset($_SESSION['__activacion_precio_mayor_menor']);

            $ejecutar_consulta = true;
          }
        }else{
          $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_catalogo";
          $_SESSION['__opcion_busqueda__'] = "catalogo";
          unset($_SESSION['__id_categoria__']);
          unset($_SESSION['__id_marca__']);
          unset($_SESSION['__productosBusqueda__']);
          unset($_SESSION['__productosImprimir_Busqueda__']);
          unset($_SESSION['__categorias_Busqueda__']);
          unset($_SESSION['__subcat_tipos_Busqueda__']);
          unset($_SESSION['__subcat_especificas_Busqueda__']);
          unset($_SESSION['__marcas_Busqueda__']);
          unset($_SESSION['__productos_por_pagina__']);
          unset($_SESSION['__total_productos__']);
          unset($_SESSION['__pagina_actual']);
          unset($_SESSION['__categoria_seleccionada']);
          unset($_SESSION['__subcat_tipo_seleccionadas']);
          unset($_SESSION['__subcat_especificas_seleccionadas']);
          unset($_SESSION['__marca_seleccionada']);
          unset($_SESSION['__rango_precios__']);
          unset($_SESSION['__activacion_precio_menor_mayor']);
          unset($_SESSION['__activacion_precio_mayor_menor']);

          $ejecutar_consulta = true;
        }
      }else{
        if( ( isset($_GET['id']) && validar_campo_numerico(trim($_GET['id'])) ) && ( isset($_GET['nombreCat']) || isset($_GET['nombreMar']) ) ){
          if($opcion === "categorias"){
            $_SESSION['__id_categoria__'] = (int) trim($_GET['id']);

            if(isset($_SESSION['__terminos_buscados'])){
              unset($_SESSION['__terminos_buscados']);
              unset($_SESSION['__productosBusqueda__']);
              unset($_SESSION['__productosImprimir_Busqueda__']);
              unset($_SESSION['__categorias_Busqueda__']);
              unset($_SESSION['__subcat_tipos_Busqueda__']);
              unset($_SESSION['__subcat_especificas_Busqueda__']);
              unset($_SESSION['__marcas_Busqueda__']);
              unset($_SESSION['__productos_por_pagina__']);
              unset($_SESSION['__total_productos__']);
              unset($_SESSION['__pagina_actual']);
              unset($_SESSION['__categoria_seleccionada']);
              unset($_SESSION['__subcat_tipo_seleccionadas']);
              unset($_SESSION['__subcat_especificas_seleccionadas']);
              unset($_SESSION['__marca_seleccionada']);
              unset($_SESSION['__rango_precios__']);
              unset($_SESSION['__activacion_precio_menor_mayor']);
              unset($_SESSION['__activacion_precio_mayor_menor']);
            }
            
            if(isset($_SESSION['__palabra_clave_opcion__'])){
              if((string) $_SESSION['__palabra_clave_opcion__'] !== "sinPalabras_categorias" || $_SESSION['__opcion_busqueda__'] !== $opcion){
                $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_categorias";
                $_SESSION['__opcion_busqueda__'] = "categorias";
                unset($_SESSION['__id_marca__']);
                unset($_SESSION['__productosBusqueda__']);
                unset($_SESSION['__productosImprimir_Busqueda__']);
                unset($_SESSION['__categorias_Busqueda__']);
                unset($_SESSION['__subcat_tipos_Busqueda__']);
                unset($_SESSION['__subcat_especificas_Busqueda__']);
                unset($_SESSION['__marcas_Busqueda__']);
                unset($_SESSION['__productos_por_pagina__']);
                unset($_SESSION['__total_productos__']);
                unset($_SESSION['__pagina_actual']);
                unset($_SESSION['__categoria_seleccionada']);
                unset($_SESSION['__subcat_tipo_seleccionadas']);
                unset($_SESSION['__subcat_especificas_seleccionadas']);
                unset($_SESSION['__marca_seleccionada']);
                unset($_SESSION['__rango_precios__']);
                unset($_SESSION['__activacion_precio_menor_mayor']);
                unset($_SESSION['__activacion_precio_mayor_menor']);

                $ejecutar_consulta = true;
              }
            }else{
              $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_categorias";
              $_SESSION['__opcion_busqueda__'] = "categorias";
              unset($_SESSION['__id_marca__']);
              unset($_SESSION['__productosBusqueda__']);
              unset($_SESSION['__productosImprimir_Busqueda__']);
              unset($_SESSION['__categorias_Busqueda__']);
              unset($_SESSION['__subcat_tipos_Busqueda__']);
              unset($_SESSION['__subcat_especificas_Busqueda__']);
              unset($_SESSION['__marcas_Busqueda__']);
              unset($_SESSION['__productos_por_pagina__']);
              unset($_SESSION['__total_productos__']);
              unset($_SESSION['__pagina_actual']);
              unset($_SESSION['__categoria_seleccionada']);
              unset($_SESSION['__subcat_tipo_seleccionadas']);
              unset($_SESSION['__subcat_especificas_seleccionadas']);
              unset($_SESSION['__marca_seleccionada']);
              unset($_SESSION['__rango_precios__']);
              unset($_SESSION['__activacion_precio_menor_mayor']);
              unset($_SESSION['__activacion_precio_mayor_menor']);

              $ejecutar_consulta = true;
            }
          }
          
          if($opcion === "marcas"){
            $_SESSION['__id_marca__'] = (int) trim($_GET['id']);
            
            if(isset($_SESSION['__terminos_buscados'])){
              unset($_SESSION['__terminos_buscados']);
              unset($_SESSION['__productosBusqueda__']);
              unset($_SESSION['__productosImprimir_Busqueda__']);
              unset($_SESSION['__categorias_Busqueda__']);
              unset($_SESSION['__subcat_tipos_Busqueda__']);
              unset($_SESSION['__subcat_especificas_Busqueda__']);
              unset($_SESSION['__marcas_Busqueda__']);
              unset($_SESSION['__productos_por_pagina__']);
              unset($_SESSION['__total_productos__']);
              unset($_SESSION['__pagina_actual']);
              unset($_SESSION['__categoria_seleccionada']);
              unset($_SESSION['__subcat_tipo_seleccionadas']);
              unset($_SESSION['__subcat_especificas_seleccionadas']);
              unset($_SESSION['__marca_seleccionada']);
              unset($_SESSION['__rango_precios__']);
              unset($_SESSION['__activacion_precio_menor_mayor']);
              unset($_SESSION['__activacion_precio_mayor_menor']);
            }
            
            if(isset($_SESSION['__palabra_clave_opcion__'])){
              if((string) $_SESSION['__palabra_clave_opcion__'] !== "sinPalabras_marcas" || $_SESSION['__opcion_busqueda__'] !== $opcion){
                $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_marcas";
                $_SESSION['__opcion_busqueda__'] = "marcas";
                unset($_SESSION['__id_categoria__']);
                unset($_SESSION['__productosBusqueda__']);
                unset($_SESSION['__productosImprimir_Busqueda__']);
                unset($_SESSION['__categorias_Busqueda__']);
                unset($_SESSION['__subcat_tipos_Busqueda__']);
                unset($_SESSION['__subcat_especificas_Busqueda__']);
                unset($_SESSION['__marcas_Busqueda__']);
                unset($_SESSION['__productos_por_pagina__']);
                unset($_SESSION['__total_productos__']);
                unset($_SESSION['__pagina_actual']);
                unset($_SESSION['__categoria_seleccionada']);
                unset($_SESSION['__subcat_tipo_seleccionadas']);
                unset($_SESSION['__subcat_especificas_seleccionadas']);
                unset($_SESSION['__marca_seleccionada']);
                unset($_SESSION['__rango_precios__']);
                unset($_SESSION['__activacion_precio_menor_mayor']);
                unset($_SESSION['__activacion_precio_mayor_menor']);

                $ejecutar_consulta = true;
              }
            }else{
              $_SESSION['__palabra_clave_opcion__'] = "sinPalabras_marcas";
              $_SESSION['__opcion_busqueda__'] = "marcas";
              unset($_SESSION['__id_categoria__']);
              unset($_SESSION['__productosBusqueda__']);
              unset($_SESSION['__productosImprimir_Busqueda__']);
              unset($_SESSION['__categorias_Busqueda__']);
              unset($_SESSION['__subcat_tipos_Busqueda__']);
              unset($_SESSION['__subcat_especificas_Busqueda__']);
              unset($_SESSION['__marcas_Busqueda__']);
              unset($_SESSION['__productos_por_pagina__']);
              unset($_SESSION['__total_productos__']);
              unset($_SESSION['__pagina_actual']);
              unset($_SESSION['__categoria_seleccionada']);
              unset($_SESSION['__subcat_tipo_seleccionadas']);
              unset($_SESSION['__subcat_especificas_seleccionadas']);
              unset($_SESSION['__marca_seleccionada']);
              unset($_SESSION['__rango_precios__']);
              unset($_SESSION['__activacion_precio_menor_mayor']);
              unset($_SESSION['__activacion_precio_mayor_menor']);

              $ejecutar_consulta = true;
            }
          }
        }else{
          echo '<script> history.go(-1); </script>';
        }
      }
      
      if($ejecutar_consulta){
        try{
          if($opcion === "catalogo"){
            $op_navbar = 2;

            /* $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.tieneImagen = '1' AND Productos.activo = 1"; */
            $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.activo = 1";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->execute();

            $contador = 0;
            while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
              $datos = array(
                'codigoProducto' => $datos_producto['codigoProducto'],
                'skuProveedor' => $datos_producto['skuProveedor'],
                'descripcion' => $datos_producto['descripcion'],
                'descripcionURL' => $datos_producto['descripcionURL'],
                'categoriaID' => $datos_producto['categoriaID'],
                'marcaID' => $datos_producto['marcaID'],
                'nombreMarca' => $datos_producto['nombreMarca'],
                'subcat_tipoID' => $datos_producto['subcat_tipoID'],
                'subcat_especificaID' => $datos_producto['subcat_especificaID'],
                'precioMXcomp' => $datos_producto['precioMXcomp'],
                'monedaMXcomp' => $datos_producto['monedaMXcomp'],
                'existenciaTotal' => $datos_producto['existenciaTotal'],
                'tieneImagen' => $datos_producto['tieneImagen'],
                'numeroUbicacionImagen' => $datos_producto['numeroUbicacionImagen'],
                'nombreImagen' => $datos_producto['nombreImagen'],
                'versionImagen' => $datos_producto['versionImagen'],
                'activo' => $datos_producto['activo'],
                'mostrarBusqueda' => '1'
              );
              $_SESSION['__productosBusqueda__'][$contador] = $datos;
              $_SESSION['__productosImprimir_Busqueda__'][$contador] = $datos;
              $contador++;
            }
          }

          if($opcion === "categorias"){
            $op_navbar = 0;
            $id = (int) $_SESSION['__id_categoria__'];
            
            $sql = "SELECT COUNT(id) AS conteo, id, nombre FROM __categorias WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if((int) $datos_categoria['conteo'] > 0){
              $datosCat = array(
                'id' => $datos_categoria['id'],
                'nombre' => $datos_categoria['nombre']
              );
              $_SESSION['__categoria_seleccionada'][0] = $datosCat;
              
              $id_cat = (int) $_SESSION['__categoria_seleccionada'][0]['id'];

              /* $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.categoriaID = :id_cat AND Productos.tieneImagen = '1' AND Productos.activo = 1"; */
              $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.categoriaID = :id_cat AND Productos.activo = 1";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':id_cat', $id_cat, PDO::PARAM_INT);
              $stmt->execute();

              $contador = 0;
              while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'codigoProducto' => $datos_producto['codigoProducto'],
                  'skuProveedor' => $datos_producto['skuProveedor'],
                  'descripcion' => $datos_producto['descripcion'],
                  'descripcionURL' => $datos_producto['descripcionURL'],
                  'categoriaID' => $datos_producto['categoriaID'],
                  'marcaID' => $datos_producto['marcaID'],
                  'nombreMarca' => $datos_producto['nombreMarca'],
                  'subcat_tipoID' => $datos_producto['subcat_tipoID'],
                  'subcat_especificaID' => $datos_producto['subcat_especificaID'],
                  'precioMXcomp' => $datos_producto['precioMXcomp'],
                  'monedaMXcomp' => $datos_producto['monedaMXcomp'],
                  'existenciaTotal' => $datos_producto['existenciaTotal'],
                  'tieneImagen' => $datos_producto['tieneImagen'],
                  'numeroUbicacionImagen' => $datos_producto['numeroUbicacionImagen'],
                  'nombreImagen' => $datos_producto['nombreImagen'],
                  'versionImagen' => $datos_producto['versionImagen'],
                  'activo' => $datos_producto['activo'],
                  'mostrarBusqueda' => '1'
                );
                $_SESSION['__productosBusqueda__'][$contador] = $datos;
                $_SESSION['__productosImprimir_Busqueda__'][$contador] = $datos;
                $contador++;
              }
            }else{
              $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
              $mensaje = "No existe";
            }
          }

          if($opcion === "marcas"){
            $op_navbar = 0;
            $id = (int) $_SESSION['__id_marca__'];

            $sql = "SELECT COUNT(id) AS conteo, id, nombre FROM __marcas WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            $datos_marca = $stmt->fetch(PDO::FETCH_ASSOC);

            if((int) $datos_marca['conteo'] > 0){
              $mensaje = "marca/" . $datos_marca['id'];
              $_SESSION['__opcion_busqueda__'] = "marcas";

              $datosMar = array(
                'id' => $datos_marca['id'],
                'nombre' => $datos_marca['nombre']
              );
              $_SESSION['__marca_seleccionada'][0] = $datosMar;
            }else{
              $respuesta = "2"; // NO EXISTE LA SKU BUSCADA
              $mensaje = "No existe";
            }

            $id_mar = (int) $_SESSION['__marca_seleccionada'][0]['id'];

            /* $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.marcaID = :id_mar AND Productos.tieneImagen = '1' AND Productos.activo = 1"; */
            $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE Productos.marcaID = :id_mar AND Productos.activo = 1";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':id_mar', $id_mar, PDO::PARAM_INT);
            $stmt->execute();

            $contador = 0;
            while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
              $datos = array(
                'codigoProducto' => $datos_producto['codigoProducto'],
                'skuProveedor' => $datos_producto['skuProveedor'],
                'descripcion' => $datos_producto['descripcion'],
                'descripcionURL' => $datos_producto['descripcionURL'],
                'categoriaID' => $datos_producto['categoriaID'],
                'marcaID' => $datos_producto['marcaID'],
                'nombreMarca' => $datos_producto['nombreMarca'],
                'subcat_tipoID' => $datos_producto['subcat_tipoID'],
                'subcat_especificaID' => $datos_producto['subcat_especificaID'],
                'precioMXcomp' => $datos_producto['precioMXcomp'],
                'monedaMXcomp' => $datos_producto['monedaMXcomp'],
                'existenciaTotal' => $datos_producto['existenciaTotal'],
                'tieneImagen' => $datos_producto['tieneImagen'],
                'numeroUbicacionImagen' => $datos_producto['numeroUbicacionImagen'],
                'nombreImagen' => $datos_producto['nombreImagen'],
                'versionImagen' => $datos_producto['versionImagen'],
                'activo' => $datos_producto['activo'],
                'mostrarBusqueda' => '1'
              );
              $_SESSION['__productosBusqueda__'][$contador] = $datos;
              $_SESSION['__productosImprimir_Busqueda__'][$contador] = $datos;
              $contador++;
            }
          }

          if(isset($_SESSION['__productosBusqueda__'])){
            $idsCategoria_array = array_column($_SESSION['__productosBusqueda__'], 'categoriaID');
            $idsMarca_array = array_column($_SESSION['__productosBusqueda__'], 'marcaID');
            $idsSubcat_tipo_array = array_column($_SESSION['__productosBusqueda__'], 'subcat_tipoID');
            $idsSubcat_especificas_array = array_column($_SESSION['__productosBusqueda__'], 'subcat_especificaID');
            unset($_SESSION['__categorias_Busqueda__']);
            unset($_SESSION['__marcas_Busqueda__']);
            unset($_SESSION['__subcat_tipos_Busqueda__']);
            unset($_SESSION['__subcat_especificas_Busqueda__']);

            try{
              $Conn_mxcomp->pdo->beginTransaction();

              // GENERAR ARRAY DE CATEGORIAS
              $sql = "SELECT Categorias.id, Categorias.nombre FROM __categorias Categorias WHERE Categorias.id IN(";
              foreach($idsCategoria_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsCategoria_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Categorias.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsCategoria_array as $id){
                $idCat = (int) $id;
                $stmt->bindValue($r, $idCat, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_categoria['id'],
                  'nombre' => $datos_categoria['nombre']
                );
                $_SESSION['__categorias_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE MARCAS
              $sql = "SELECT Marcas.id, Marcas.nombre FROM __marcas Marcas WHERE Marcas.id IN(";
              foreach($idsMarca_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsMarca_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Marcas.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsMarca_array as $id){
                $idMarca = (int) $id;
                $stmt->bindValue($r, $idMarca, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_marca = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_marca['id'],
                  'nombre' => $datos_marca['nombre']
                );
                $_SESSION['__marcas_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE SUBCATEGORIA DE TIPOS
              $sql = "SELECT Subcat_tipos.id, Subcat_tipos.nombre FROM __subcat_tipos Subcat_tipos WHERE Subcat_tipos.id IN(";
              foreach($idsSubcat_tipo_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsSubcat_tipo_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Subcat_tipos.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsSubcat_tipo_array as $id){
                $idSubcat_tipo = (int) $id;
                $stmt->bindValue($r, $idSubcat_tipo, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_subcat_tipo = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_subcat_tipo['id'],
                  'nombre' => $datos_subcat_tipo['nombre']
                );
                $_SESSION['__subcat_tipos_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE SUBCATEGORIA ESPECIFICAS
              $sql = "SELECT Subcat_especificas.id, Subcat_especificas.nombre FROM __subcat_especificas Subcat_especificas WHERE Subcat_especificas.id IN(";
              foreach($idsSubcat_especificas_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsSubcat_especificas_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Subcat_especificas.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsSubcat_especificas_array as $id){
                $idSubcat_especifica = (int) $id;
                $stmt->bindValue($r, $idSubcat_especifica, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_subcat_especifica = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_subcat_especifica['id'],
                  'nombre' => $datos_subcat_especifica['nombre']
                );
                $_SESSION['__subcat_especificas_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              $Conn_mxcomp->pdo->commit();
            }catch(PDOException $error){
              $Conn_mxcomp->pdo->rollBack();
              $respuesta = "2";
              //echo $error->getMessage();
              //$mensaje = "Error en consulta de productos.";
            }
          }

          $stmt = null;
        }catch(PDOException $error){
          $respuesta = "2";
          //echo $error->getMessage();
          //$mensaje = "Error en consulta de productos.";
        }
      }else{
        if($opcion === "catalogo"){
          $op_navbar = 2;
        }
        
        if($opcion === "categorias"){
          $op_navbar = 0;
        }
        
        if($opcion === "marcas"){
          $op_navbar = 0;
        }
      }
    }
  }else if(isset($_GET['buscar'])){
    $busqueda = "";
    $busqueda = trim($_GET['buscar']);
    
    if($busqueda === ""){
      echo '<script> history.go(-1); </script>';
    }else{
      $op_navbar = 0;

      if(isset($_SESSION['__opcion_busqueda__'])){
        unset($_SESSION['__opcion_busqueda__']);
        unset($_SESSION['__palabra_clave_opcion__']);
        unset($_SESSION['__productosBusqueda__']);
        unset($_SESSION['__productosImprimir_Busqueda__']);
        unset($_SESSION['__categorias_Busqueda__']);
        unset($_SESSION['__subcat_tipos_Busqueda__']);
        unset($_SESSION['__subcat_especificas_Busqueda__']);
        unset($_SESSION['__marcas_Busqueda__']);
        unset($_SESSION['__productos_por_pagina__']);
        unset($_SESSION['__total_productos__']);
        unset($_SESSION['__pagina_actual']);
        unset($_SESSION['__categoria_seleccionada']);
        unset($_SESSION['__subcat_tipo_seleccionadas']);
        unset($_SESSION['__subcat_especificas_seleccionadas']);
        unset($_SESSION['__marca_seleccionada']);
        unset($_SESSION['__rango_precios__']);
        unset($_SESSION['__activacion_precio_menor_mayor']);
        unset($_SESSION['__activacion_precio_mayor_menor']);
      }
      
      $palabras = explode(" ", $busqueda);

      $palabrasBuscadas = "";
      $contador = 0;
      foreach($palabras as $indice=>$palabra){
        if($palabra !== ""){
          //TEXTO DE BUSQUEDA
          $palabrasBuscadas .= $palabra;

          if((int) count($palabras) !== (int) ($indice + 1)){
            $palabrasBuscadas .= " ";
          }

          //ARRAY DE LAS PALABRAS DE BUSQUEDA
          $palabras_Busqueda[$contador] = $palabra;
          $contador++;
        }
      }

      $ejecutar_consulta = false;

      if(isset($_SESSION['__terminos_buscados'])){
        if((string) $_SESSION['__terminos_buscados'] !== (string) $palabrasBuscadas){
          $_SESSION['__terminos_buscados'] = $palabrasBuscadas;
          unset($_SESSION['__productosBusqueda__']);
          unset($_SESSION['__productosImprimir_Busqueda__']);
          unset($_SESSION['__categorias_Busqueda__']);
          unset($_SESSION['__subcat_tipos_Busqueda__']);
          unset($_SESSION['__subcat_especificas_Busqueda__']);
          unset($_SESSION['__marcas_Busqueda__']);
          unset($_SESSION['__productos_por_pagina__']);
          unset($_SESSION['__total_productos__']);
          unset($_SESSION['__pagina_actual']);
          unset($_SESSION['__categoria_seleccionada']);
          unset($_SESSION['__subcat_tipo_seleccionadas']);
          unset($_SESSION['__subcat_especificas_seleccionadas']);
          unset($_SESSION['__marca_seleccionada']);
          unset($_SESSION['__rango_precios__']);
          unset($_SESSION['__activacion_precio_menor_mayor']);
          unset($_SESSION['__activacion_precio_mayor_menor']);

          $ejecutar_consulta = true;
        }
      }else{
        $_SESSION['__terminos_buscados'] = $palabrasBuscadas;
        unset($_SESSION['__productosBusqueda__']);
        unset($_SESSION['__productosImprimir_Busqueda__']);
        unset($_SESSION['__categorias_Busqueda__']);
        unset($_SESSION['__subcat_tipos_Busqueda__']);
        unset($_SESSION['__subcat_especificas_Busqueda__']);
        unset($_SESSION['__marcas_Busqueda__']);
        unset($_SESSION['__productos_por_pagina__']);
        unset($_SESSION['__total_productos__']);
        unset($_SESSION['__pagina_actual']);
        unset($_SESSION['__categoria_seleccionada']);
        unset($_SESSION['__subcat_tipo_seleccionadas']);
        unset($_SESSION['__subcat_especificas_seleccionadas']);
        unset($_SESSION['__marca_seleccionada']);
        unset($_SESSION['__rango_precios__']);
        unset($_SESSION['__activacion_precio_menor_mayor']);
        unset($_SESSION['__activacion_precio_mayor_menor']);
        
        $ejecutar_consulta = true;
      }

      if($ejecutar_consulta){
        try{
          $sql = "SELECT COUNT(Productos.id) FROM __productos Productos WHERE ";

          foreach($palabras_Busqueda as $indice=>$palabra){
            $sql .= "Productos.descripcion LIKE ?";

            if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
              $sql .= " AND ";
            }
          }
          
          //$sql .= " AND tieneImagen = '1' AND activo = 1";
          $sql .= " AND activo = 1";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);

          $r = 1;
          foreach($palabras_Busqueda as $palabra){
            $termino_desc = "%" . trim($palabra, " \t\xC2\xA0") . "%";
            $stmt->bindValue($r, $termino_desc, PDO::PARAM_STR);
            $r++;
          }

          $stmt->execute();
          $productos_encontrados = (int) $stmt->fetchColumn();
          
          if($productos_encontrados > 0){
            $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE ";

            foreach($palabras_Busqueda as $indice=>$palabra){
              $sql .= "Productos.descripcion LIKE ?";

              if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
                $sql .= " AND ";
              }
            }

            //$sql .= " AND tieneImagen = '1' AND activo = 1";
            $sql .= " AND activo = 1";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);

            $r = 1;
            foreach($palabras_Busqueda as $palabra){
              $termino_desc = "%" . trim($palabra, " \t\xC2\xA0") . "%";
              $stmt->bindValue($r, $termino_desc, PDO::PARAM_STR);
              $r++;
            }

            $stmt->execute();
          }else{
            $sql = "SELECT Productos.codigoProducto, Productos.skuProveedor, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.subcat_tipoID, Productos.subcat_especificaID, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.existenciaTotal, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.activo FROM __productos Productos WHERE ";

            foreach($palabras_Busqueda as $indice=>$palabra){
              $sql .= "Productos.descripcion LIKE ?";

              if((int) count($palabras_Busqueda) !== (int) ($indice + 1)){
                $sql .= " OR ";
              }
            }

            //$sql .= " AND tieneImagen = '1' AND activo = 1";
            $sql .= " AND activo = 1";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);

            $r = 1;
            foreach($palabras_Busqueda as $palabra){
              $termino_desc = "%" . trim($palabra, " \t\xC2\xA0") . "%";
              $stmt->bindValue($r, $termino_desc, PDO::PARAM_STR);
              $r++;
            }

            $stmt->execute();
          }
          

          $contador = 0;
          while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
            $datos = array(
              'codigoProducto' => $datos_producto['codigoProducto'],
              'skuProveedor' => $datos_producto['skuProveedor'],
              'descripcion' => $datos_producto['descripcion'],
              'descripcionURL' => $datos_producto['descripcionURL'],
              'categoriaID' => $datos_producto['categoriaID'],
              'marcaID' => $datos_producto['marcaID'],
              'nombreMarca' => $datos_producto['nombreMarca'],
              'subcat_tipoID' => $datos_producto['subcat_tipoID'],
              'subcat_especificaID' => $datos_producto['subcat_especificaID'],
              'precioMXcomp' => $datos_producto['precioMXcomp'],
              'monedaMXcomp' => $datos_producto['monedaMXcomp'],
              'existenciaTotal' => $datos_producto['existenciaTotal'],
              'tieneImagen' => $datos_producto['tieneImagen'],
              'numeroUbicacionImagen' => $datos_producto['numeroUbicacionImagen'],
              'nombreImagen' => $datos_producto['nombreImagen'],
              'versionImagen' => $datos_producto['versionImagen'],
              'activo' => $datos_producto['activo'],
              'mostrarBusqueda' => '1'
            );
            $_SESSION['__productosBusqueda__'][$contador] = $datos;
            $_SESSION['__productosImprimir_Busqueda__'][$contador] = $datos;
            $contador++;
          }

          if(isset($_SESSION['__productosBusqueda__'])){
            $idsCategoria_array = array_column($_SESSION['__productosBusqueda__'], 'categoriaID');
            $idsMarca_array = array_column($_SESSION['__productosBusqueda__'], 'marcaID');
            $idsSubcat_tipo_array = array_column($_SESSION['__productosBusqueda__'], 'subcat_tipoID');
            $idsSubcat_especificas_array = array_column($_SESSION['__productosBusqueda__'], 'subcat_especificaID');
            unset($_SESSION['__categorias_Busqueda__']);
            unset($_SESSION['__marcas_Busqueda__']);
            unset($_SESSION['__subcat_tipos_Busqueda__']);
            unset($_SESSION['__subcat_especificas_Busqueda__']);

            try{
              $Conn_mxcomp->pdo->beginTransaction();

              // GENERAR ARRAY DE CATEGORIAS
              $sql = "SELECT Categorias.id, Categorias.nombre FROM __categorias Categorias WHERE Categorias.id IN(";
              foreach($idsCategoria_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsCategoria_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Categorias.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsCategoria_array as $id){
                $idCat = (int) $id;
                $stmt->bindValue($r, $idCat, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_categoria['id'],
                  'nombre' => $datos_categoria['nombre']
                );
                $_SESSION['__categorias_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE MARCAS
              $sql = "SELECT Marcas.id, Marcas.nombre FROM __marcas Marcas WHERE Marcas.id IN(";
              foreach($idsMarca_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsMarca_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Marcas.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsMarca_array as $id){
                $idMarca = (int) $id;
                $stmt->bindValue($r, $idMarca, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_marca = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_marca['id'],
                  'nombre' => $datos_marca['nombre']
                );
                $_SESSION['__marcas_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE SUBCATEGORIA DE TIPOS
              $sql = "SELECT Subcat_tipos.id, Subcat_tipos.nombre FROM __subcat_tipos Subcat_tipos WHERE Subcat_tipos.id IN(";
              foreach($idsSubcat_tipo_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsSubcat_tipo_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Subcat_tipos.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsSubcat_tipo_array as $id){
                $idSubcat_tipo = (int) $id;
                $stmt->bindValue($r, $idSubcat_tipo, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_subcat_tipo = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_subcat_tipo['id'],
                  'nombre' => $datos_subcat_tipo['nombre']
                );
                $_SESSION['__subcat_tipos_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              // GENERAR ARRAY DE SUBCATEGORIA ESPECIFICAS
              $sql = "SELECT Subcat_especificas.id, Subcat_especificas.nombre FROM __subcat_especificas Subcat_especificas WHERE Subcat_especificas.id IN(";
              foreach($idsSubcat_especificas_array as $indice=>$id){
                $sql .= "?";
                if((int) count($idsSubcat_especificas_array) !== (int) ($indice + 1)){
                  $sql .= ", ";
                }
              }
              $sql .= ") ORDER BY Subcat_especificas.nombre ASC";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);

              $r = 1;
              foreach($idsSubcat_especificas_array as $id){
                $idSubcat_especifica = (int) $id;
                $stmt->bindValue($r, $idSubcat_especifica, PDO::PARAM_INT);
                $r++;
              }
              $stmt->execute();

              $contador = 0;
              while($datos_subcat_especifica = $stmt->fetch(PDO::FETCH_ASSOC)){
                $datos = array(
                  'id' => $datos_subcat_especifica['id'],
                  'nombre' => $datos_subcat_especifica['nombre']
                );
                $_SESSION['__subcat_especificas_Busqueda__'][$contador] = $datos;
                $contador++;
              }

              $Conn_mxcomp->pdo->commit();
            }catch(PDOException $error){
              $Conn_mxcomp->pdo->rollBack();
              $respuesta = "2";
              //echo $error->getMessage();
              //$mensaje = "Error en consulta de productos.";
            }
          }

          $stmt = null;
        }catch(PDOException $error){
          $respuesta = "2";
          //echo $error->getMessage();
          //$mensaje = "Error en consulta de productos.";
        }
      }

      unset($palabras);
    }
  }else{
    echo '<script> history.go(-1); </script>';
  }

  $_SESSION['__productos_por_pagina__'] = 100;
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php
  switch($opcion){
    case 'catalogo':
      $title = "Catálogo";
      $titulo = "Catálogo";
      break;
    case 'categorias':
      $nombre = trim($_GET['nombreCat']);
      $nombre = str_replace("_", " ", $nombre);
      $title = "Categorías (" . ucfirst(mb_strtolower($nombre, 'UTF-8')) . ")";
      $titulo = "Categorías (" . ucfirst(mb_strtolower($nombre, 'UTF-8')) . ")";
      break;
    case 'marcas':
      $nombre = trim($_GET['nombreMar']);
      $nombre = str_replace("_", " ", $nombre);
      $title = "Marcas (" . ucfirst(mb_strtolower($nombre, 'UTF-8')) . ")";
      $titulo = "Marcas (" . ucfirst(mb_strtolower($nombre, 'UTF-8')) . ")";
      break;
    default:
      $title = 'Búsqueda | ' . $palabrasBuscadas;
      $titulo = $palabrasBuscadas;
  }
?>
    <title><?php echo $title; ?></title>
    <meta name="description" content="Realiza una búsqueda de productos y conoce el catálogo con el que contamos.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, búsqueda, productos">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
  //VARIABLES PARA USO DEL NAVBAR Y MENU LATERAL
  $navbar = "1";
  $op_menu = 0;

  include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-busqueda-fondo" id="id-contenedor-titulo_busqueda">
          <h1 class="p-titulo"><?php echo $titulo; ?></h1>
          <div id="id-busqueda-contenedor_texto_resultados"></div>
          <input type="hidden" autocomplete="off" value="<?php echo $palabrasBuscadas; ?>">
        </div>
        <div class="p-busqueda-section">
          <div class="p-columnas">
            <div class="p-columna p-cuatro_columnas">
              <div id="id-busqueda-filtros_button_abrirFiltros"></div>
              <div class="p-busqueda-filtros_movil_contenedor" id="id-busqueda-contenedor_filtros"></div>
            </div>
            <div class="p-columna">
              <div class="p-loading-general p-busqueda-loading_padding" id="id-busqueda-load_productos">
                <div></div>
                <p>Cargando productos</p>
              </div>
              <div id="id-busqueda-contenedor_productos"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

    <div class="p-busqueda-loading" id="id-busqueda-load_datos_generales">
      <div class="p-busqueda-loading_contenedor">
        <div class="p-busqueda-loading_ball"></div>
        <span>Realizando búsqueda</span>
      </div>
    </div>

<?php include 'templates/footer_scripts_jquery.php'; ?>

    <script src="scripts/busqueda/scripts_busqueda.js?v=4.10"></script>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>
<?php
}
?>