<?php
session_start();
if(!isset($_SESSION['__id__']) || !isset($_GET['orden_compra'])){
  echo '<script> history.go(-1); </script>';
}

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Estado de la compra</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">ESTADO DE LA COMPRA</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
if(!isset($_SESSION['__id__'])){
?>
          <p class="p-text_p">
            <b>Para poder comprar es necesario que inicies sesión. En caso de no contar con una cuenta, regístrala dando clic en el botón correspondiente.</b>
          </p>
          <div class="p-buttons">
            <a href="iniciar-sesion" class="p-button p-button_info">
              <span>
                <i class="fas fa-user"></i>
              </span>
              <span><b>Iniciar sesión</b></span>
            </a>
            <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
              <span>
                <i class="fas fa-user-plus"></i>
              </span>
              <span><b>Registrar cuenta</b></span>
            </a>
          </div>
<?php
}else{
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $ordenCompra = trim($_GET['orden_compra']);

  $Conn_MXcomp = new Conexion_mxcomp();
  $Conn_Admin = new Conexion_admin();
  
  $proceso_correcto = false;
  $msg_error = "";
  $bandera_info = false;
  
  // REVISA SI EXISTE EL USUARIO
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario FROM __usuarios WHERE id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_INT);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];
      
      if($usuario_existe === 1){
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];
        $proceso_correcto = true;
      }else{
        $msg_error = "Este usuario no existe.";
        $bandera_info = true;
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$msg_error = 'Error: ' . $error->getMessage();
      $msg_error = "Hubo un problema al buscar al usuario.";
      $bandera_info = true;
      $proceso_correcto = false;
    }
  }else{
    $msg_error = "No es numérica la clave del usuario.";
    $bandera_info = true;
    $proceso_correcto = false;
  }
  
  // REVISA SI LA ORDEN DE COMPRA ES VÁLIDA
  if($proceso_correcto){
    if(validar_campo_numerico($ordenCompra)){
      $ordenCompra = (int) $ordenCompra;
      $proceso_correcto = true;
    }else{
      $msg_error = "No es numérica la orden de compra.";
      $bandera_info = false;
      $proceso_correcto = false;
    }
  }
  
  // REVISA SI EXISTEN LOS DATOS DE COMPRAS
  if($proceso_correcto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, id, ordenCompra FROM __ordenes_compra WHERE idCliente = :idCliente AND codigoCliente = :codigoCliente AND ordenCompra = :ordenCompra";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->execute();
      $datos_ordenCompra = $stmt->fetch(PDO::FETCH_ASSOC);
      $orden_compra_existe = (int) $datos_ordenCompra['conteo'];

      if($orden_compra_existe === 1){
        $id = (int) trim($datos_ordenCompra['id']);
        $ordenCompra = (string) trim($datos_ordenCompra['ordenCompra']);

        $proceso_correcto = true;
      }else{
        $msg_error = "La orden de compra no existe. Si este mensaje vuelve a aparecer ponte en contacto con atención a clientes.";
        $bandera_info = true;
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$msg_error = 'Error: ' . $error->getMessage();
      $msg_error = "Ocurrió un problema al tratar de obtener la información de tu compra. Si este mensaje vuelve a aparecer ponte en contacto con atención a clientes.";
      $bandera_info = true;
      $proceso_correcto = false;
    }
  }
  
  // IMPRIMIR LOS MENSAJES ADECUADOS
  if($proceso_correcto){
?>
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #3EBA50; font-size: 3rem;">
              <i class="fas fa-check-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem;">¡Compra finalizada! Sólo esperamos que realices tu pago.</h3>
          </div>
          
          <div class="p-buttons p-buttons_center">
            <a href="compra-detalles/<?php echo $id . '/' . $ordenCompra; ?>" class="p-button p-button_account p-button_largo">
              <span>
                <i class="far fa-file-alt"></i>
              </span>
              <span><b>Ver detalles de la compra</b></span>
            </a>
          </div>
<?php
  }else{
    if($bandera_info){
?>
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #254F95; font-size: 3rem;">
              <i class="fas fa-info-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem; color: #254F95 !important;">¡Upps! Hay un problema...</h3>
            <p class="p-notification p-notification_letter_info" style="width: 100%;"><b><?php echo $msg_error; ?></b></p>
          </div>
<?php
    }else{
?>
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #A11B2C; font-size: 3rem;">
              <i class="fas fa-times-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem; color: #A11B2C !important;">¡Upps! Hay un error...</h3>
            <p class="p-notification p-notification_letter_error" style="width: 100%;"><b><?php echo $msg_error; ?></b></p>
          </div>
<?php
    }
  }
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>