        <aside class="p-menulateral-aside_fondo">
          <div>
            <figure class="p-menulateral-figure_logo">
              <a href="<?php echo HOST_LINK; ?>">
                <img src="images/logo_mxcomp.png?v=2" alt="MXcomp; la tecnología en tus manos">
              </a>
            </figure>
          </div>
          <ul class="p-menulateral-menu">
            <li>
              <a <?php echo $op_menu === 1 ? 'class="p-menulateral-opcion_activo"' : 'href="quienes-somos"'; ?>>
                <span>
                  <i class="fas fa-users"></i>
                </span>
                <span>¿Quiénes somos?</span>
              </a>
            </li>
            <li>
              <a <?php echo $op_menu === 2 ? 'class="p-menulateral-opcion_activo"' : 'href="sucursales"'; ?>>
                <span>
                  <i class="fas fa-store-alt"></i>
                </span>
                <span>Sucursales</span>
              </a>
            </li>
            <li>
              <a <?php echo $op_menu === 3 ? 'class="p-menulateral-opcion_activo"' : 'href="atencion-a-clientes"'; ?>>
                <span>
                  <i class="fas fa-headset"></i>
                </span>
                <span>Atención a clientes</span>
              </a>
            </li>
            <li>
              <a <?php echo $op_menu === 4 ? 'class="p-menulateral-opcion_activo"' : 'href="aviso-de-privacidad"'; ?>>
                <span>
                  <i class="fas fa-shield-alt"></i>
                </span>
                <span>Aviso de privacidad</span>
              </a>
            </li>
            <li>
              <a <?php echo $op_menu === 5 ? 'class="p-menulateral-opcion_activo"' : 'href="terminos-y-condiciones"'; ?>>
                <span>
                  <i class="fas fa-file-alt"></i>
                </span>
                <span>Términos y condiciones</span>
              </a>
            </li>
          </ul>
          <div class="p-social-net_contenedor">
            <a href="https://www.facebook.com/mxcompoficial/" target="_blank" class="p-social-net_button p-social-net_button_f">
              <span>
                <i class="fab fa-lg fa-facebook-f"></i>
              </span>
            </a>
            <a href="https://twitter.com/XcompM" target="_blank" class="p-social-net_button p-social-net_button_t">
              <span>
                <i class="fab fa-lg fa-twitter"></i>
              </span>
            </a>
            <a href="https://api.whatsapp.com/send?phone=5215587965174&text=Hola%20MXcomp,%20requiero%20de%20m%C3%A1s%20informaci%C3%B3n..." target="_blank" class="p-social-net_button p-social-net_button_w">
              <span>
                <i class="fab fa-lg fa-whatsapp"></i>
              </span>
            </a>
            <a href="https://www.instagram.com/mxcomp_oficial/" target="_blank" class="p-social-net_button p-social-net_button_i">
              <span>
                <i class="fab fa-lg fa-instagram"></i>
              </span>
            </a>
          </div>
        </aside>