    <base href="<?php echo HOST_LINK; ?>">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <link rel="manifest" href="manifest.json?v=1.2">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="MXcomp">
    <link rel="icon" sizes="192x192" href="images/touch/chrome-touch-icon-192x192.png?v=1.1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="MXcomp">
    <link rel="apple-touch-icon" sizes="57x57" href="images/touch/apple-touch-icon-57x57.png?v=1.1">
    <link rel="apple-touch-icon" sizes="60x60" href="images/touch/apple-touch-icon-60x60.png?v=1.1">
    <link rel="apple-touch-icon" sizes="72x72" href="images/touch/apple-touch-icon-72x72.png?v=1.1">
    <link rel="apple-touch-icon" sizes="76x76" href="images/touch/apple-touch-icon-76x76.png?v=1.1">
    <link rel="apple-touch-icon" sizes="114x114" href="images/touch/apple-touch-icon-114x114.png?v=1.1">
    <link rel="apple-touch-icon" sizes="120x120" href="images/touch/apple-touch-icon-120x120.png?v=1.1">
    <link rel="apple-touch-icon" sizes="144x144" href="images/touch/apple-touch-icon-144x144.png?v=1.1">
    <link rel="apple-touch-icon" sizes="152x152" href="images/touch/apple-touch-icon-152x152.png?v=1.1">
    <link rel="apple-touch-icon" sizes="180x180" href="images/touch/apple-touch-icon-180x180.png?v=1.1">
    <meta name="msapplication-TileColor" content="white">
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png?v=1.1">
    <meta name="theme-color" content="white">
    <link rel="icon" type="image/png" sizes="16x16" href="images/touch/icon-16x16.png?v=1.1">
    <link rel="icon" type="image/png" sizes="32x32" href="images/touch/icon-32x32.png?v=1.1">
    <link rel="icon" type="image/png" sizes="96x96" href="images/touch/icon-96x96.png?v=1.1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu:700&display=swap" rel="stylesheet">
    <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" data-auto-replace-svg="nest"></script>
    <link rel="stylesheet" href="styles/main.min.css?v=4.5">
    <!--<script defer src="scripts/all.min.js"></script>-->