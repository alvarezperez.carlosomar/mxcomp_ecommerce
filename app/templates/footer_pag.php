    <footer class="p-footer<?php echo isset($footer_carrito) && $footer_carrito ? ' p-carrito-footer' : ''; ?>">
<?php
  $fecha_inicio_pagina = "2019";
  $fecha_footer = date('Y') > $fecha_inicio_pagina ? $fecha_inicio_pagina . '-' . date('Y') : $fecha_inicio_pagina;
?>
      <small>
        <b>Copyright © <?php echo $fecha_footer; ?> MXcomp. Todos los derechos reservados.</b>
      </small>
    </footer>
    <div class="p-boton-arriba<?php echo isset($botonArriba_carrito) && $botonArriba_carrito ? ' p-boton-arriba_carrito' : ''; ?>" id="id-boton_up">
      <span class="icon">
        <i class="fas fa-chevron-up"></i>
      </span>
    </div>