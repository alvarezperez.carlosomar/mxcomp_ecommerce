<?php
if (isset($navbar) && $navbar === "1") {
  $navbar_cantidadCarrito = 0;
  $error_cantidad = 0;

  if (isset($_SESSION['__id__'])) {
    $idUser = desencriptar(trim($_SESSION['__id__']));
    $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
    $Conexion_mxcomp = new Conexion_mxcomp();

    if (validar_campo_numerico($idUser)) {
      $idUser = (int) $idUser;
      $error_consulta = false;

      try {
        $sql = "SELECT COUNT(id) FROM __usuarios WHERE BINARY id = :idUser";
        $stmt = $Conexion_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUser', $idUser, PDO::PARAM_INT);
        $stmt->execute();
        $usuario_existe = (int) $stmt->fetchColumn();

        if ($usuario_existe === 1) {
          $sql = "SELECT COUNT(id) FROM __carrito WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario";
          $stmt = $Conexion_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUser, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $productos_existenCarrito = (int) $stmt->fetchColumn();

          if ($productos_existenCarrito > 0) {
            $tieneExistencias = '1';
            $guardado = '0';

            $sql = "SELECT unidades, existenciaAlmacen FROM __carrito WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado";
            $stmt = $Conexion_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':idUsuario', $idUser, PDO::PARAM_INT);
            $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
            $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
            $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
            $stmt->execute();

            while ($dat_producto = $stmt->fetch(PDO::FETCH_ASSOC)) {
              if ($dat_producto['unidades'] > $dat_producto['existenciaAlmacen']) {
                $error_cantidad += 1;
              }

              $navbar_cantidadCarrito += (int) $dat_producto['unidades'];
            }
          }

          if ($error_cantidad === 0) {
            if ((int) $navbar_cantidadCarrito >= 10) {
              $navbar_cantidadCarrito = "+9";
            }
          } else {
            $navbar_cantidadCarrito = "!";
          }
        } else {
          $navbar_cantidadCarrito = "!";
        }

        $stmt = null;
      } catch (PDOException $error) {
        //$navbar_cantidadCarrito = "Error: " . $error->getMessage();
        $navbar_cantidadCarrito = "!";
      }
    } else {
      $navbar_cantidadCarrito = "!";
    }

    unset($Conexion_mxcomp);
  } else {
    if(isset($_SESSION['__carrito__'])){
      foreach($_SESSION['__carrito__'] as $productos_carritoAlmacen){
        foreach($productos_carritoAlmacen as $informacion_producto){
          if ($informacion_producto['unidades'] > $informacion_producto['existenciaAlmacen']) {
            $error_cantidad++;
          }

          // SI NO SE ENCUENTRA GUARDADO
          if ((string) $informacion_producto['guardado'] === '0') {
            $navbar_cantidadCarrito += $informacion_producto['unidades'];
          }
        }
      }

      if ($error_cantidad === 0) {
        if ((int) $navbar_cantidadCarrito >= 10) {
          $navbar_cantidadCarrito = "+9";
        }
      } else {
        $navbar_cantidadCarrito = "!";
      }
    }else {
      $navbar_cantidadCarrito = "0";
    }
  }
  ?>
    <nav class="p-navbar-menu">
      <div class="p-navbar-menu_start">
        <a <?php echo $op_navbar !== 1 ? 'href="' . HOST_LINK . '"' : ''; ?> class="p-navbar-menu_inicio_opcion p-navbar-opcion_oculto">
          <img class="p-navbar-menu_inicio_opcion_logo" src="images/logo_mxcomp.png" alt="MXcomp; la tecnología en tus manos">
        </a>
        <a <?php echo $op_navbar !== 1 ? 'href="' . HOST_LINK . '"' : ''; ?> class="p-navbar-menu_opcion p-navbar-opcion_mostrado<?php echo $op_navbar === 1 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-home"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Inicio</span>
        </a>
        <form class="p-navbar-busqueda_form" id="id-navbar-busqueda_form">
          <div class="p-navbar-busqueda_contenedor_input">
            <input type="text" class="p-navbar-busqueda_buscar_input" id="id-navbar-busqueda_form_input" placeholder="¿Qué estás buscando?" autocomplete="off"<?php echo isset($palabrasBuscadas) ? 'value="' . $palabrasBuscadas . '"' : ''; ?>>
          </div>
          <div class="p-navbar-busqueda_contenedor_button">
            <button type="submit" class="p-button p-button_search p-button_square" id="id-navbar-busqueda_form_button">
              <span>
                <i class="fas fa-search"></i>
              </span>
            </button>
          </div>
          <div class="p-navbar-busqueda_form_div" id="id-navbar-busqueda_resultados"></div>
          <div class="p-navbar-busqueda_form_div" id="id-navbar-busqueda_sku"></div>
        </form>
        <a class="p-navbar-menu_opcion p-navbar-opcion_mostrado<?php echo $op_navbar === 2 ? ' p-navbar-menu_opcion_activa' : ' g-navbar-busqueda_catalogo'; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-boxes"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Catálogo</span>
        </a>
        <a <?php echo $op_navbar !== 3 ? 'href="categorias"' : ''; ?> class="p-navbar-menu_opcion p-navbar-opcion_mostrado<?php echo $op_navbar === 3 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-list-alt"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Categorías</span>
        </a>
        <a <?php echo $op_navbar !== 4 ? 'href="marcas"' : ''; ?> class="p-navbar-menu_opcion p-navbar-opcion_mostrado<?php echo $op_navbar === 4 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-tags"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Marcas</span>
        </a>
      </div>
      <div class="p-navbar-menu_end">
        <a class="p-navbar-menu_opciones" id="g-navbar-hamburger_button">
          <span class="hamburger"></span>
        </a>
        <a href="mi-carrito" class="p-navbar-menu_opcion p-navbar-menu_opcion_icono p-navbar-opcion_mostrado" title="Mi carrito">
          <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_icon_fontSize p-navbar-menu_opcion_icon_carrito">
            <i class="fas fa-shopping-cart"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto_numCarrito p-navbar-menu_opcion_texto_numCarrito_flotante g-navbar_carrito_cantidad"><?php echo $navbar_cantidadCarrito; ?></span>
        </a>
<?php
if (isset($_SESSION['__id__'])) {
    $nom_usu = (string) desencriptar_con_clave(trim($_SESSION['__nombre_usu__']), trim($_SESSION['__codigo_usu__']));
    $nom_usu = explode(" ", $nom_usu);
        /* <a class="p-navbar-menu_opcion p-navbar-menu_opcion_icono p-navbar-menu_opcion_icono_notificacion p-navbar-opcion_mostrado" style="padding-right: 1em;" title="Notificaciones">
          <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_icon_fontSize">
            <i class="fas fa-bell"></i>
          </span>
        </a> */
    ?>
        <div class="p-navbar-menu_opcion_contDesplegable">
          <a class="p-navbar-menu_opcion p-navbar-opcion_user p-navbar-opcion_mostrado">
            <span class="p-navbar-menu_opcion_icon">
              <i class="fas fa-2x fa-user-circle"></i>
            </span>
            <!--<div class="p-navbar-menu_opcion_contDesplegable_contenedor_img">
              <img src="images/logo_mxcomp.jpg" alt="Avatar usuario">
            </div>-->
            <span class="p-navbar-menu_opcion_texto">
              <b><?php echo $nom_usu[0]; ?></b>
            </span>
          </a>
          <div class="p-navbar-menu_opcion_contDesplegable_contenido">
            <div class="p-navbar-menu_opcion_contDesplegable_contenedor_titulo">
              <span class="p-navbar-menu_opcion_contDesplegable_titulo">¡Hola, <?php echo $nom_usu[0]; ?>!</span>
            </div>
            <hr class="p-navbar-hr">
            <a <?php echo $op_navbar !== 5 ? 'href="mi-cuenta"' : ''; ?> class="p-navbar-menu_opcion p-navbar-menu_opcion_contDesplegable_button p-navbar-opcion_mostrado<?php echo $op_navbar === 5 ? ' p-navbar-menu_opcion_user_activo' : ''; ?>">
              <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cuenta">
                <i class="fas fa-fw fa-house-user"></i>
              </span>
              <span class="p-navbar-menu_opcion_texto">Mi cuenta</span>
            </a>
            <a <?php echo $op_navbar !== 6 ? 'href="mis-datos"' : ''; ?> class="p-navbar-menu_opcion p-navbar-menu_opcion_contDesplegable_button p-navbar-opcion_mostrado<?php echo $op_navbar === 6 ? ' p-navbar-menu_opcion_user_activo' : ''; ?>">
              <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cuenta">
                <i class="fas fa-fw fa-address-book"></i>
              </span>
              <span class="p-navbar-menu_opcion_texto">Mis datos</span>
            </a>
            <hr class="p-navbar-hr">
            <a class="p-navbar-menu_opcion p-navbar-menu_opcion_contDesplegable_button p-navbar-opcion_mostrado g-navbar-cerrar_sesion">
              <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cerrarSesion">
                <i class="fas fa-fw fa-sign-out-alt"></i>
              </span>
              <span class="p-navbar-menu_opcion_texto">
                <b>Cerrar sesión</b>
              </span>
            </a>
          </div>
        </div>
<?php
} else {
        /* <div class="p-navbar-menu_contenedor_buttons p-navbar-opcion_mostrado">
          <a href="iniciar-sesion" class="p-button p-button_info">
            <span>
              <i class="fas fa-user"></i>
            </span>
            <span><b>Iniciar sesión</b></span>
          </a>
        </div> */
}
  ?>
      </div>
      <div class="p-navbar-menu_contenedor_opciones_fondo" id="g-navbar-menu_contenedor_opciones_fondo"></div>
      <div class="p-navbar-menu_contenedor_opciones" id="g-navbar-menu_contenedor_opciones">
        <a href="mi-carrito" class="p-navbar-menu_opcion" title="Mi carrito">
          <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_icon_carrito">
            <i class="fas fa-fw fa-2x fa-shopping-cart"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Mi carrito</span>
          <span class="p-navbar-menu_opcion_texto_numCarrito g-navbar_carrito_cantidad"><?php echo $navbar_cantidadCarrito; ?></span>
        </a>
<?php
if (isset($_SESSION['__id__'])) {
    ?>
        <a class="p-navbar-menu_opcion p-navbar-opcion_user_movil<?php echo $op_navbar === 5 || $op_navbar === 6 ? ' p-navbar-opcion_user_movil_activo' : ''; ?>" id="g-navbar-menu_opcion_user_movil">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-2x fa-user-circle"></i>
          </span>
          <!--<div class="p-navbar-menu_opcion_contDesplegable_contenedor_img">
            <img src="images/logo_mxcomp.jpg" alt="Avatar usuario">
          </div>-->
          <span class="p-navbar-menu_opcion_texto">
            <b><?php echo $nom_usu[0]; ?></b>
          </span>
        </a>
        <div class="p-navbar-menu_user_contenedor_opciones" id="g-navbar-menu_user_contenedor_opciones"<?php echo $op_navbar === 5 || $op_navbar === 6 ? ' style="display: block;"' : ''; ?>>
          <a <?php echo $op_navbar !== 5 ? 'href="mi-cuenta"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 5 ? ' p-navbar-menu_opcion_user_activo' : ''; ?>">
            <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cuenta">
              <i class="fas fa-fw fa-house-user"></i>
            </span>
            <span class="p-navbar-menu_opcion_texto">Mi cuenta</span>
          </a>
          <a <?php echo $op_navbar !== 6 ? 'href="mis-datos"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 6 ? ' p-navbar-menu_opcion_user_activo' : ''; ?>">
            <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cuenta">
              <i class="fas fa-fw fa-address-book"></i>
            </span>
            <span class="p-navbar-menu_opcion_texto">Mis datos</span>
          </a>
          <a class="p-navbar-menu_opcion g-navbar-cerrar_sesion">
            <span class="p-navbar-menu_opcion_icon p-navbar-menu_opcion_contDesplegable_icon_cerrarSesion">
              <i class="fas fa-fw fa-sign-out-alt"></i>
            </span>
            <span class="p-navbar-menu_opcion_texto">
              <b>Cerrar sesión</b>
            </span>
          </a>
        </div>
<?php
} else {
    ?>
<?php
}
  ?>
        <!-- <hr class="p-navbar-hr"> -->

        <a class="p-navbar-menu_opcion<?php echo $op_navbar === 2 ? ' p-navbar-menu_opcion_activa' : ' g-navbar-busqueda_catalogo'; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-boxes"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Catálogo</span>
        </a>
        <a <?php echo $op_navbar !== 3 ? 'href="categorias"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 3 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-list-alt"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Categorías</span>
        </a>
        <a <?php echo $op_navbar !== 4 ? 'href="marcas"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 4 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-tags"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Marcas</span>
        </a>
        <hr class="p-navbar-hr">
        <a <?php echo $op_navbar !== 7 ? 'href="quienes-somos"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 7 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-users"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">¿Quiénes somos?</span>
        </a>
        <a <?php echo $op_navbar !== 8 ? 'href="sucursales"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 8 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-store-alt"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Sucursales</span>
        </a>
        <a <?php echo $op_navbar !== 9 ? 'href="atencion-a-clientes"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 9 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-headset"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Atención a clientes</span>
        </a>
        <a <?php echo $op_navbar !== 10 ? 'href="aviso-de-privacidad"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 10 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-shield-alt"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Aviso de Privacidad</span>
        </a>
        <a <?php echo $op_navbar !== 11 ? 'href="terminos-y-condiciones"' : ''; ?> class="p-navbar-menu_opcion<?php echo $op_navbar === 11 ? ' p-navbar-menu_opcion_activa' : ''; ?>">
          <span class="p-navbar-menu_opcion_icon">
            <i class="fas fa-fw fa-lg fa-file-alt"></i>
          </span>
          <span class="p-navbar-menu_opcion_texto">Términos y condiciones</span>
        </a>
        <hr class="p-navbar-hr">
<?php
if (!isset($_SESSION['__id__'])) {
        /* <div class="p-navbar-menu_contenedor_buttons">
          <a href="iniciar-sesion" class="p-button p-button_info">
            <span>
              <i class="fas fa-user"></i>
            </span>
            <span><b>Iniciar sesión</b></span>
          </a>
        </div> */
    ?>
<?php
}
  ?>
        <div class="p-botones-redes_sociales">
          <a href="https://www.facebook.com/mxcompoficial/" target="_blank" class="p-social-net_button p-social-net_button_f">
            <span>
              <i class="fab fa-lg fa-facebook-f"></i>
            </span>
          </a>
          <a href="https://twitter.com/XcompM" target="_blank" class="p-social-net_button p-social-net_button_t">
            <span>
              <i class="fab fa-lg fa-twitter"></i>
            </span>
          </a>
          <a href="https://api.whatsapp.com/send?phone=5215587965174&text=Hola%20MXcomp,%20requiero%20de%20m%C3%A1s%20informaci%C3%B3n..." target="_blank" class="p-social-net_button p-social-net_button_w">
            <span>
              <i class="fab fa-lg fa-whatsapp"></i>
            </span>
          </a>
          <a href="https://www.instagram.com/mxcomp_oficial/" target="_blank" class="p-social-net_button p-social-net_button_i">
            <span>
              <i class="fab fa-lg fa-instagram"></i>
            </span>
          </a>
        </div>
      </div>
    </nav>
<?php
}
?>