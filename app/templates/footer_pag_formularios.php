    <footer class="p-footer-formularios">
<?php
  $fecha_inicio_pagina = "2019";
  $fecha_footer = date('Y') > $fecha_inicio_pagina ? $fecha_inicio_pagina . '-' . date('Y') : $fecha_inicio_pagina;
?>
      <small>
        <b>Copyright © <?php echo $fecha_footer; ?> MXcomp. Todos los derechos reservados.</b>
      </small>
      <div class="p-footer-social_net">
        <a href="https://www.facebook.com/mxcompoficial/" target="_blank" class="p-footer-social_net_button p-social-net_button_f">
          <span>
            <i class="fab fa-lg fa-facebook-f"></i>
          </span>
        </a>
        <a href="https://twitter.com/XcompM" target="_blank" class="p-footer-social_net_button p-social-net_button_t">
          <span>
            <i class="fab fa-lg fa-twitter"></i>
          </span>
        </a>
        <a href="https://api.whatsapp.com/send?phone=5215587965174&text=Hola%20MXcomp,%20requiero%20de%20m%C3%A1s%20informaci%C3%B3n..." target="_blank" class="p-footer-social_net_button p-social-net_button_w">
          <span>
            <i class="fab fa-lg fa-whatsapp"></i>
          </span>
        </a>
        <a href="https://www.instagram.com/mxcomp_oficial/" target="_blank" class="p-footer-social_net_button p-social-net_button_i">
          <span>
            <i class="fab fa-lg fa-instagram"></i>
          </span>
        </a>
      </div>
    </footer>