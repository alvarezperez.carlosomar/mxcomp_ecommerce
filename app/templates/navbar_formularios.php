<?php
if (isset($navbar) && $navbar === "1") {
  ?>
    <nav class="p-navbar-menu">
      <div class="p-navbar-menu_start">
        <a href="/" class="p-navbar-menu_inicio_opcion" style="display:flex;">
          <img class="p-navbar-menu_inicio_opcion_logo" src="images/logo_mxcomp.png" alt="MXcomp: Tu tienda en línea">
        </a>
      </div>
      <div class="p-navbar-menu_end">
        <a class="p-navbar-menu_opciones" id="g-navbar-hamburger_button">
          <span class="hamburger"></span>
        </a>
        <div class="p-navbar-menu_contenedor_buttons p-navbar-opcion_mostrado">
          <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
            <span>
              <i class="fas fa-user-plus"></i>
            </span>
            <span><b>Registrar cuenta</b></span>
          </a>
          <a href="iniciar-sesion" class="p-button p-button_info">
            <span>
              <i class="fas fa-user"></i>
            </span>
            <span><b>Iniciar sesión</b></span>
          </a>
        </div>
      </div>
      <div class="p-navbar-menu_contenedor_opciones_fondo" id="g-navbar-menu_contenedor_opciones_fondo"></div>
      <div class="p-navbar-menu_contenedor_opciones" id="g-navbar-menu_contenedor_opciones">
        <div class="p-navbar-menu_contenedor_buttons">
          <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
            <span>
              <i class="fas fa-user-plus"></i>
            </span>
            <span><b>Registrar cuenta</b></span>
          </a>
          <a href="iniciar-sesion" class="p-button p-button_info">
            <span>
              <i class="fas fa-user"></i>
            </span>
            <span><b>Iniciar sesión</b></span>
          </a>
        </div>
        <div class="p-botones-redes_sociales">
          <a href="https://www.facebook.com/mxcompoficial/" target="_blank" class="p-social-net_button p-social-net_button_f">
            <span>
              <i class="fab fa-lg fa-facebook-f"></i>
            </span>
          </a>
          <a href="https://twitter.com/XcompM" target="_blank" class="p-social-net_button p-social-net_button_t">
            <span>
              <i class="fab fa-lg fa-twitter"></i>
            </span>
          </a>
          <a href="https://api.whatsapp.com/send?phone=5215587965174&text=Hola%20MXcomp,%20requiero%20de%20m%C3%A1s%20informaci%C3%B3n..." target="_blank" class="p-social-net_button p-social-net_button_w">
            <span>
              <i class="fab fa-lg fa-whatsapp"></i>
            </span>
          </a>
          <a href="https://www.instagram.com/mxcomp_oficial/" target="_blank" class="p-social-net_button p-social-net_button_i">
            <span>
              <i class="fab fa-lg fa-instagram"></i>
            </span>
          </a>
        </div>
      </div>
    </nav>
<?php
}
?>