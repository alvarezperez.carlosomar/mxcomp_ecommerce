<?php
session_start();

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR
require_once 'clases/usuario/metodos_usuario.php';
require_once 'clases/proveedor/metodos_PCH.php';
require_once 'clases/producto/metodos_producto.php';
require_once 'clases/carrito/metodos_carrito.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

$seccion = isset($_GET['id_seccion']) ? ( $_GET['id_seccion'] === '1' ? 'producto' : ( $_GET['id_seccion'] === '2' ? 'carrito' : 'No existe' ) ) : 'No existe';

if(!isset($_GET['id_seccion']) || $seccion === 'No existe'){
  die('<script> history.go(-1); </script>');
}

$bandera_sinExistencia = false;
$bandera_sinStock = false;

if(isset($_SESSION['__id__'])){
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  $proceso_exitoso = false;

  // REVISA SI EL ID DE USUARIO ES NUMERICO Y QUE EXISTA EL USUARIO
  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $usuario_Objeto = new Usuario($idUsuario, $codigoUsuario);
    $producto_Objeto = new Producto($idUsuario, $codigoUsuario);
    $carrito_Objeto = new Carrito($idUsuario, $codigoUsuario);

    if($usuario_Objeto->buscarUsuario()){
      $proceso_exitoso = true;
    }else{
      $tipo_mensaje = 'error';
      $mensaje = (string) $usuario_Objeto->buscarUsuario_mensaje;
      $proceso_exitoso = false;
    }
  }else{
    $tipo_mensaje = 'error';
    $mensaje = 'EL id del usuario no es numérico';
    $proceso_exitoso = false;
  }

  // SE BUSCAN LAS SKUS
  $arrayProductos = [];
  if($proceso_exitoso){
    if($seccion === 'producto'){
      $codigoProducto = '';
      if(!isset($_SESSION['__producto_unidades__'])){
        die('<script> history.go(-1); </script>');
      }else{
        $almacenes_conUnidades = 0;
        foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $nombreAlmacen=>$data_almacen){
          if((int) $data_almacen['unidades'] > 0){
            $almacenes_conUnidades++;
          }
        }

        if($almacenes_conUnidades > 0){
          $codigoProducto = $_SESSION['__producto_unidades__']['codigoProducto'];
        }else{
          die('<script> history.go(-1); </script>');
        }
      }

      if($producto_Objeto->buscarProducto($codigoProducto)){
        $skuProveedor = $producto_Objeto->buscarProducto_skuProveedor;
        $nombreProveedor = $producto_Objeto->buscarProducto_nombreProveedor;

        $arrayProductos[0] = [
          'codigoProducto' => (string) $codigoProducto,
          'skuProveedor' => (string) $skuProveedor,
          'precioProveedor' => 0.00,
          'monedaProveedor' => '',
          'precioMXcomp' => 0.00,
          'monedaMXcomp' => '',
          'existenciaTotal' => 0,
          'almacenes' => null,
          'nombreProveedor' => (string) $nombreProveedor,
          'tieneExistencia' => 1
        ];
        $proceso_exitoso = true;
      }else{
        $opcion = (int) $producto_Objeto->buscarProducto_opcion;
        $tipo_mensaje = $opcion === 1 ? 'info' : 'error';
        $mensaje = $producto_Objeto->buscarProducto_mensaje;
        $proceso_exitoso = false;
      }
    }else{
      if($carrito_Objeto->buscarSkuProveedorUnica()){
        $arrayProductos = $carrito_Objeto->buscarSkuProveedorUnica_array;
        $proceso_exitoso = true;
      }else{
        $opcion = (int) $carrito_Objeto->buscarSkuProveedorUnica_opcion;
        $tipo_mensaje = $opcion === 1 ? 'info' : 'error';
        $mensaje = $carrito_Objeto->buscarSkuProveedorUnica_mensaje;
        $proceso_exitoso = false;
      }
    }
  }

  // SE BUSCA EL TIPO DE CAMBIO
  if($proceso_exitoso){
    $proveedorPCH = new PCH_Mayoreo();

    if($proveedorPCH->tipoCambio()){
      $tipo_cambio = round(trim($proveedorPCH->tCambio_valorMoneda), 2);
      $proceso_exitoso = true;
    }else{
      $tipo_mensaje = 'error';
      $mensaje = $proveedorPCH->tCambio_mensajeDetalles;;
      $proceso_exitoso = false;
    }
  }

  // SE BUSCA LA INFO CON EL PROVEEDOR
  $actualizar_productos = false;
  if($proceso_exitoso){
    $arraySkus = array_column($arrayProductos, 'skuProveedor');
    $skusProveedor = implode(',', $arraySkus);

    if($proveedorPCH->listaProductos($skusProveedor)){
      $array_skus_problemas = [];

      if(count($proveedorPCH->lProductos_no_stock) > 0 || count($proveedorPCH->lProductos_skuErrores) > 0){
        $array_skus_problemas = array_merge($array_skus_problemas, $proveedorPCH->lProductos_no_stock);
        $array_skus_problemas = array_merge($array_skus_problemas, $proveedorPCH->lProductos_skuErrores);

        $bandera_sinStock = true;
      }

      $lista_productos = $proveedorPCH->lProductos_productos;

      $productos_sinStock = 0;
      $productos_sinExistencia = 0;
      foreach($lista_productos as $datos_producto){
        $skuProveedor = trim($datos_producto->sku);

        if(count($datos_producto->inventario) > 0){
          $existenciaTotal = 0;
          $almacenes = [];

          // SE GENERAN LOS ALMACENES CON SU EXISTENCIA, ASI COMO LA EXISTENCIA TOTAL
          foreach($datos_producto->inventario as $datos_inventario){
            $almacen_id = (int) $datos_inventario->almacen_id;
            if($almacen_id === 1 || $almacen_id === 5 || $almacen_id === 23 || $almacen_id === 36 || $almacen_id === 15 || $almacen_id === 19){
              $existenciaAlmacen = (int) $datos_inventario->cantidad;

              if($existenciaAlmacen > 0){
                switch($almacen_id){
                  case 1: // GUADALAJARA
                    $array_almacen = [
                      'id_almacen' => 1,
                      'nombre_almacen' => 'GUADALAJARA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;

                  case 5: // DF AEROPUERTO
                    $array_almacen = [
                      'id_almacen' => 5,
                      'nombre_almacen' => 'DF AEROPUERTO',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;

                  case 11: // MONTERREY
                    $array_almacen = [
                      'id_almacen' => 11,
                      'nombre_almacen' => 'MONTERREY',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;

                  case 15: // MÉRIDA
                    $array_almacen = [
                      'id_almacen' => 15,
                      'nombre_almacen' => 'MÉRIDA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;

                  case 23: // PUEBLA
                    $array_almacen = [
                      'id_almacen' => 23,
                      'nombre_almacen' => 'PUEBLA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;

                  case 36: // LEÓN
                    $array_almacen = [
                      'id_almacen' => 36,
                      'nombre_almacen' => 'LEÓN',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
                }

                array_push($almacenes, $array_almacen);
                $existenciaTotal += $existenciaAlmacen;
              }
            }
          }

          // SI HAY EXISTENCIAS, SE BUSCA LA INFORMACION RESTANTE
          if($existenciaTotal > 0){
            $almacenes = json_encode($almacenes);

            $precioProveedor = round(trim($datos_producto->precio), 2);
            $monedaProveedor = trim($datos_producto->moneda);
            $precioMXcomp = round(trim($datos_producto->precio), 2);

            if($monedaProveedor === "USD"){
              $precioMXcomp = round($precioMXcomp * $tipo_cambio, 2);
            }

            $precioMXcomp = round($precioMXcomp * 1.32, 2);
            $monedaMXcomp = "MXN";

            foreach($arrayProductos as $indice=>$datos){
              if($datos['skuProveedor'] === $skuProveedor){
                $arrayProductos[$indice]['precioProveedor'] = (float) $precioProveedor;
                $arrayProductos[$indice]['monedaProveedor'] = (string) $monedaProveedor;
                $arrayProductos[$indice]['precioMXcomp'] = (float) $precioMXcomp;
                $arrayProductos[$indice]['monedaMXcomp'] = (string) $monedaMXcomp;
                $arrayProductos[$indice]['existenciaTotal'] = (int) $existenciaTotal;
                $arrayProductos[$indice]['almacenes'] = (string) $almacenes;
                break;
              }
            }
          }else{
            $productos_sinExistencia++;
            $posicion = count($array_skus_problemas);
            $array_skus_problemas[$posicion] = $skuProveedor;
          }
        }else{
          $productos_sinStock++;
          $posicion = count($array_skus_problemas);
          $array_skus_problemas[$posicion] = $skuProveedor;
        }
      }

      if($productos_sinStock > 0){
        $bandera_sinStock = true;
      }

      if($productos_sinExistencia > 0){
        $bandera_sinExistencia = true;
      }

      $actualizar_productos = true;
      $proceso_exitoso = true;

      if(count($array_skus_problemas) > 0){
        foreach($arrayProductos as $indice=>$datos){
          if(in_array($datos['skuProveedor'], $array_skus_problemas)){
            $arrayProductos[$indice]['tieneExistencia'] = 0;
          }
        }
      }
    }else{
      $opcion = (int) $proveedorPCH->lProductos_opcion;
      $tipo_mensaje = $opcion === 1 ? 'info' : 'error';
      $mensaje = $opcion === 1 ? $proveedorPCH->lProductos_mensajeDetalles : 'Error al pedir la información al proveedor.';

      if($mensaje === 'productos no encontrados'){
        $fechaActualizacion = date("Y-m-d H:i:s");

        $proceso_exitoso = true;
        foreach($arraySkus as $array_skuProveedor){
          $codigoProducto = '';

          foreach($arrayProductos as $datos){
            if($array_skuProveedor === $datos['skuProveedor']){
              $codigoProducto = (string) $datos['codigoProducto'];
              break;
            }
          }

          if($codigoProducto !== ''){
            if( !$producto_Objeto->colocarSinExistencia($fechaActualizacion, $codigoProducto, $array_skuProveedor) ){
              $tipo_mensaje = 'error';
              $mensaje = $producto_Objeto->colocarSinExistencia_mensaje;
              $proceso_exitoso = false;
              break;
            }
          }
        }

        $bandera_sinStock = true;
      }else{
        $proceso_exitoso = false;
      }
    }
  }

  // SE ACTUALIZAN LOS PRODUCTO EN SU TABLA
  if($actualizar_productos){
    $fechaActualizacion = date("Y-m-d H:i:s");

    $proceso_exitoso = true;
    foreach($arrayProductos as $datos){
      $codigoProducto = (string) $datos['codigoProducto'];
      $skuProveedor = (string) $datos['skuProveedor'];
      $precioProveedor = (float) $datos['precioProveedor'];
      $monedaProveedor = (string) $datos['monedaProveedor'];
      $precioMXcomp = (float) $datos['precioMXcomp'];
      $monedaMXcomp = (string) $datos['monedaMXcomp'];
      $existenciaTotal = (int) $datos['existenciaTotal'];
      $almacenes = (string) $datos['almacenes'];
      $nombreProveedor = (string) $datos['nombreProveedor'];
      $tieneExistencia = (int) $datos['tieneExistencia'];

      // SI HAY STOCK O EXISTENCIAS
      if($tieneExistencia === 1){
        if( !$producto_Objeto->actualizarPrecioExistencia($precioProveedor, $monedaProveedor, $precioMXcomp, $monedaMXcomp, $existenciaTotal, $almacenes, $fechaActualizacion, $codigoProducto, $skuProveedor) ){
          $tipo_mensaje = 'error';
          $mensaje = $producto_Objeto->actualizarPrecioExistencia_mensaje;
          $proceso_exitoso = false;
          break;
        }

        if($proceso_exitoso && $seccion === 'carrito'){
          $proceso_exitoso = true;
          $almacenes_array = (array) json_decode($almacenes, true);

          foreach($almacenes_array as $datos_almacen){
            $numeroAlmacen = (int) $datos_almacen['id_almacen'];
            $nombreAlmacen = (string) $datos_almacen['nombre_almacen'];
            $existenciaAlmacen = (int) $datos_almacen['existencia'];

            if( !$carrito_Objeto->actualizarPrecioExistencia($precioProveedor, $monedaProveedor, $precioMXcomp, $monedaMXcomp, $existenciaAlmacen, $existenciaTotal, $fechaActualizacion, $codigoProducto, $skuProveedor, $numeroAlmacen, $nombreAlmacen, $nombreProveedor) ){
              $tipo_mensaje = 'error';
              $mensaje = $carrito_Objeto->actualizarPrecioExistencia_mensaje;
              $proceso_exitoso = false;
              break 2;
            }
          }
        }
      }else{
        if( !$producto_Objeto->colocarSinExistencia($fechaActualizacion, $codigoProducto, $skuProveedor) ){
          $tipo_mensaje = 'error';
          $mensaje = $producto_Objeto->colocarSinExistencia_mensaje;
          $proceso_exitoso = false;
          break;
        }

        if($proceso_exitoso && $seccion === 'carrito'){
          $proceso_exitoso = true;
          $almacenes_array = (array) json_decode($almacenes, true);

          foreach($almacenes_array as $datos_almacen){
            $numeroAlmacen = (int) $datos_almacen['id_almacen'];
            $nombreAlmacen = (string) $datos_almacen['nombre_almacen'];
            $existenciaAlmacen = (int) $datos_almacen['existencia'];

            if( !$carrito_Objeto->colocarSinExistencia($fechaActualizacion, $codigoProducto, $skuProveedor, $numeroAlmacen, $nombreAlmacen, $nombreProveedor) ){
              $tipo_mensaje = 'error';
              $mensaje = $carrito_Objeto->colocarSinExistencia_mensaje;
              $proceso_exitoso = false;
              break 2;
            }
          }
        }
      }
    }
  }
}else{
  $proceso_exitoso = true;
}
?>
<!DOCTYPE html>
<html lang="es-MX">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title>Confirmar compra</title>
<?php include 'templates/head.php';?>

	</head>
	<body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>
		<section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php';?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-comprar-fondo">
          <h1 class="p-titulo">Confirmar compra</h1>
        </div>
        <div class="p-section-div_formularios p-contenido-hero">
<?php
if(!$proceso_exitoso){
  $msj_alert_class = $tipo_mensaje === 'error' ? 'p-notification_letter_error' : 'p-notification_letter_info';
  $msj_alert_icon = $tipo_mensaje === 'error' ? 'fa-times-circle' : 'fa-info-circle';
?>
          <div class="p-notification <?php echo $msj_alert_class; ?>">
            <span>
              <i class="fas <?php echo $msj_alert_icon; ?>"></i>
            </span>
            <span class="p-notification_p"><b><?php echo $mensaje; ?></b></span>
          </div>
<?php
}else{
  if(!isset($_SESSION['__id__'])){
?>
          <p class="p-text_p">Para poder comprar es necesario que inicies sesión. En caso de no contar con una cuenta, regístrala dando clic en el botón correspondiente.</p>
          <div class="p-buttons p-cuenta-contenedor_botones">
            <a href="iniciar-sesion" class="p-button p-button_info">
              <span>
                <i class="fas fa-user"></i>
              </span>
              <span><b>Iniciar sesión</b></span>
            </a>
            <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
              <span>
                <i class="fas fa-user-plus"></i>
              </span>
              <span><b>Registrar cuenta</b></span>
            </a>
          </div>
<?php
  }else{
?>
          <div class="p-notification p-notification_letter_info">
            <span><b>Antes de hacer tu compra revisa la información del producto, puede que haya cambiado al actualizarlo con el proveedor.</b></span>
          </div>

          <div class="p-acordeon-contenedor">
            <div class="p-acordeon-cabecera p-acordeon-cabecera_success g-acordeon_cabecera p-acordeon-cabecera_activa" data-seccion="1">
              <h3>
                <span>
                  <i class="fas fa-fw fa-list-alt"></i>
                </span>
                <span>Pedido</span>
              </h3>
            </div>
            <div class="p-acordeon-contenido_contenedor" data-contenido="1" style="display: block">
<?php
    $proceso_correcto = false;

    // SI HAY PRODUCTOS SIN EXISTENCIA, SE MUESTRA EL SIGUIENTE MENSAJE
    if($bandera_sinExistencia || $bandera_sinStock){
      if($bandera_sinExistencia){
        $mensaje_notificacion_existencia = $seccion === 'producto' ? 'Se actualizó la información del producto y no cuenta con existencias o es posible la cantidad a comprar es mayor a la existente en uno de sus almacenes. Regresa al producto y reacomoda tu pedido.' : 'Se actualizó la información de los productos y algunos no cuentan con existencias, o la cantidad a comprar es mayor a la existente en uno de sus almacenes. Regresa al carrito y reacomoda tu pedido.';
?>
              <div class="p-notification p-notification_letter_info">
                <span><b><?php echo $mensaje_notificacion_existencia; ?></b></span>
              </div>
<?php
      }

      if($bandera_sinStock){
        $mensaje_notificacion_stock = $seccion === 'producto' ? 'El producto ya no se encuentra disponible. Revisa tu pedido por favor.' : 'Algunos productos ya no se encuentran disponibles. Revisa tu pedido por favor.';
?>
              <div class="p-notification p-notification_letter_info">
                <span><b><?php echo $mensaje_notificacion_stock; ?></b></span>
              </div>
<?php
      }

      $proceso_correcto = false;
    }else{
      $costo_envio_estandar = (float) COSTO_ENVIO_ESTANDAR;
      $total_compra_envioGratis = (float) TOTAL_COMPRA;
      $total_pagar = 0;
      $costo_envio_total = 0;
      $proceso_correcto = false;
      $totalUnidades = 0;

      if($seccion === 'producto'){
        $codigoProducto = $_SESSION['__producto_unidades__']['codigoProducto'];

        if($producto_Objeto->ver_productoComprar($codigoProducto)){
          $BD_descripcion = (string) $producto_Objeto->ver_productoComprar_descripcion;
          $BD_precioMXcomp = (float) $producto_Objeto->ver_productoComprar_precioMXcomp;
          $BD_monedaMXcomp = (string) $producto_Objeto->ver_productoComprar_monedaMXcomp;
          $BD_almacenes = (array) $producto_Objeto->ver_productoComprar_almacenes;
          $BD_envioGratisPermitido = (int) $producto_Objeto->ver_productoComprar_envioGratisPermitido;

          $precioUnitario = '$ ' . number_format($BD_precioMXcomp, 2) . ' ' . $BD_monedaMXcomp;
?>
              <article class="p-comprar-pedido_contenedor">
                <div class="p-comprar-pedido_contenedor_titulo">
                  <h4><?php echo $BD_descripcion; ?></h4>
                  <p>
                    <span><b>Precio unitario:</b></span>
                    <span class="p-text_help"><b><?php echo $precioUnitario; ?></b></span>
                  </p>
                </div>

<?php
          $proceso_correcto = true;

          foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $nombreAlmacen=>$data_almacen){
            $almacen_unidades = (int) $data_almacen['unidades'];

            if($almacen_unidades > 0){
              $Producto_existenciaAlmacen = 0;
              foreach($BD_almacenes as $datos_almacen){
                if($datos_almacen['nombre_almacen'] === $nombreAlmacen){
                  $Producto_existenciaAlmacen = (int) $datos_almacen['existencia'];
                  break;
                }
              }

              $totalUnidades += $almacen_unidades;
              $totalPago_almacen = $BD_precioMXcomp * $almacen_unidades;

              if($totalPago_almacen > $total_compra_envioGratis){
                if($BD_envioGratisPermitido === 0){
                  $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);
                  $costo_envio_almacen = '$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . ' ' . $BD_monedaMXcomp . ' (El producto no aplica para envío gratis).';
                }else{
                  $costo_envio_total += 0.00;
                  $costo_envio_almacen = 'Envío gratis';
                }
              }else{
                $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);
                $costo_envio_almacen = '$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . ' ' . $BD_monedaMXcomp;
              }

              $texto_nombreAlmacen = ucwords(mb_strtolower($nombreAlmacen));
              $texto_unidades = $almacen_unidades === 1 ? $almacen_unidades . ' unidad' : $almacen_unidades . ' unidades';
              $texto_total = '$ ' . number_format($totalPago_almacen, 2) . ' ' . $BD_monedaMXcomp;
?>
                <div class="p-comprar-pedido_contenedor_info">
                  <h5>Almacén <?php echo $texto_nombreAlmacen; ?></h5>
                  <p>Comprarás <b><?php echo $texto_unidades; ?></b> y el total será de <b><?php echo $texto_total; ?></b></p>
                  <p style="margin-bottom: .3rem;">Importe del envío: <span class="p-text_help"><b><?php echo $costo_envio_almacen; ?></b></span></p>
<?php
              if($Producto_existenciaAlmacen === 0){
                $proceso_correcto = false;
                echo '<p style="color: #E51313;"><b>No hay existencias para este almacén.</b></p>';
              }

              if($almacen_unidades > $Producto_existenciaAlmacen){
                $proceso_correcto = false;
                echo '<p style="color: #F16D43;"><b>Sólo se cuenta con ' . ( $Producto_existenciaAlmacen === 1 ? $Producto_existenciaAlmacen . ' unidad disponible' : $Producto_existenciaAlmacen . ' unidades disponibles' ) . ', no se puede proceder con la compra.</b></p>';
              }
?>
                </div>
<?php
            }
          }
?>
              </article>
<?php
          $total_pagar = $BD_precioMXcomp * $totalUnidades;
        }else{
          $mensaje_error = $producto_Objeto->ver_productoComprar_mensaje;
          $proceso_correcto = false;
?>
              <div class="p-notification p-notification_letter_info">
                <span><i class="fas fa-info-circle"></i></span>
                <span class="p-notification_p"><b><?php echo $mensaje_error; ?></b></span>
              </div>
<?php
        }
      }else{
        if($carrito_Objeto->ver_productosComprar()){
          $arrayProductosCarrito = $carrito_Objeto->ver_productosComprar_array;

          $proceso_correcto = true;
          foreach($arrayProductosCarrito as $nombreAlmacen=>$datos_almacen){
            $texto_nombreAlmacen = ucwords(mb_strtolower($nombreAlmacen));
            $importe_total = round($datos_almacen['importe_total'], 2);
            $envioGratisPermitido_almacen = (bool) $datos_almacen['envioGratisPermitido_almacen'];
            $array_productos = $datos_almacen['productos'];

            $total_pagar += $importe_total;

            if($importe_total > $total_compra_envioGratis){
              if($envioGratisPermitido_almacen === false){
                $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);
                $costo_envio_almacen = '$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . ' MXN (No aplica para envío gratis).';
              }else{
                $costo_envio_total += 0.00;
                $costo_envio_almacen = 'Envío gratis';
              }
            }else{
              $costo_envio_total += (float) round($costo_envio_estandar * 1.16, 2);
              $costo_envio_almacen = '$ ' . number_format($costo_envio_estandar * 1.16, 2, '.', ',') . ' MXN';
            }

            $texto_importe_total = '$' . number_format($importe_total, 2) . ' MXN';
?>
              <article class="p-comprar-pedido_contenedor">
                <div class="p-comprar-pedido_contenedor_titulo">
                  <h4>Almacén <?php echo $texto_nombreAlmacen; ?></h4>
                  <p><span><b>Importe total:</b></span></p>
                  <p><span class="p-text_help"><b><?php echo $texto_importe_total; ?></b></span></p>
                  <p><span><b>Importe del envío:</b></span></p>
                  <p><span class="p-text_help"><b><?php echo $costo_envio_almacen; ?></b></span></p>
                </div>
<?php
            foreach($array_productos as $datos){
              $BD_descripcion = (string) trim($datos['descripcion'], " \xC2\xA0");
              $BD_precioMXcomp = (float) trim($datos['precioMXcomp']);
              $BD_monedaMXcomp = (string) trim($datos['monedaMXcomp']);
              $BD_unidades = (int) trim($datos['unidades']);
              $BD_existenciaAlmacen = (int) trim($datos['existenciaAlmacen']);

              $totalUnidades += $BD_unidades;

              $texto_unidades = $BD_unidades === 1 ? $BD_unidades . ' unidad' : $BD_unidades . ' unidades';
              $texto_total = '$ ' . number_format($BD_precioMXcomp * $BD_unidades, 2) . ' ' . $BD_monedaMXcomp;
?>
                <div class="p-comprar-pedido_contenedor_info">
                  <h5><?php echo $BD_descripcion; ?></h5>
                  <span>Precio unitario: <b>$ <?php echo number_format($BD_precioMXcomp, 2); ?> <?php echo $BD_monedaMXcomp; ?></b></span>
                  <span style="margin-bottom: .3rem;">Comprarás <b><?php echo $texto_unidades; ?></b> y el total será de <b><?php echo $texto_total; ?></b></span>
<?php
              if($BD_existenciaAlmacen === 0){
                $proceso_correcto = false;
                echo '<span style="color: #E51313;"><b>No hay existencias para este producto.</b></span>';
              }

              if($BD_unidades > $BD_existenciaAlmacen){
                $proceso_correcto = false;
                echo '<span style="color: #F16D43;"><b>Sólo se cuenta con ' . ( $BD_existenciaAlmacen === 1 ? $BD_existenciaAlmacen . ' unidad disponible' : $BD_existenciaAlmacen . ' unidades disponibles' ) . ', no se puede proceder con la compra.</b></span>';
              }
?>
                </div>
<?php
            }
?>
              </article>
<?php
          }
        }else{
          $mensaje_error = $carrito_Objeto->ver_productosComprar_mensaje;
          $proceso_correcto = false;
?>
              <div class="p-notification p-notification_letter_info">
                <span><i class="fas fa-info-circle"></i></span>
                <span class="p-notification_p"><b><?php echo $mensaje_error; ?></b></span>
              </div>
<?php
        }
      }
    }
?>
              <input type="hidden" id="id-comprar-unidades_comprar" autocomplete="off" value="<?php echo $totalUnidades; ?>">
            </div>
<?php
	  if($proceso_correcto){
?>
            <div class="p-acordeon-cabecera g-acordeon_cabecera" data-seccion="2">
              <h3>
                <span>
                  <i class="fas fa-fw fa-truck"></i>
                </span>
                <span>Método de envío</span>
              </h3>
            </div>
            <div class="p-acordeon-contenido_contenedor" data-contenido="2">
              <div class="p-notification_contenedor" id="id-comprar-metodoEnvio_contenedor_message">
                <div class="p-notification p-notification_info">
                  <span><b>No has seleccionado el método de envío.</b></span>
                </div>
              </div>

              <h4 class="p-comprar-aviso p-text_p">Selecciona el método de envío:</h4>

              <div class="p-notification p-notification_letter_success" style="margin-bottom: 1rem !important; font-weight: normal;">
                <span>Para <b>'Enviar por paquetería'</b>, el costo estándar es de <b>$ <?php echo number_format($costo_envio_estandar * 1.16, 2, '.', ','); ?> MXN ($ <?php echo $costo_envio_estandar; ?> MXN + IVA)</b>. Recuerda que el envío es gratis en compras mayores a <b>$ <?php echo number_format($total_compra_envioGratis, 2, '.', ','); ?> MXN (aplica por almacén). Algunos productos no entran en esta promoción.</b></span>
              </div>

              <div class="p-comprar-radioButton_contenedor" id="id-comprar-contenedor_envio">
<?php
							/*<input type="radio" class="p-comprar-radioButton_oculto" id="id-comprar-label_envio_1" name="modo_envio" value="Sucursal" required>
							<label class="p-comprar-radioButton_label p-comprar-radioButton_label_envio" for="id-comprar-label_envio_1">
								<span><b>Recoger en sucursal ($ 0.00 MXN)</b></span>
							</label>*/

      $texto_costo_envio = number_format($costo_envio_total, 2) === '0.00' ? 'Envío gratis' : '$ ' . number_format($costo_envio_total, 2, '.', ',') . ' MXN';
?>
                <input type="radio" class="p-comprar-radioButton_oculto" id="id-comprar-label_envio_2" name="modo_envio" value="Paqueteria" required>
                <label class="p-comprar-radioButton_label p-comprar-radioButton_label_envio" for="id-comprar-label_envio_2">
                  <span><b>Enviar por paquetería (<?php echo $texto_costo_envio; ?>)</b></span>
                </label>
              </div>
            </div>
            <div class="p-acordeon-cabecera g-acordeon_cabecera" data-seccion="3" style="display: none;">
              <h3>
                <span>
                  <i class="fas fa-fw fa-map-marker-alt"></i>
                </span>
                <span>Domicilio de envío</span>
              </h3>
            </div>
            <div class="p-acordeon-contenido_contenedor" data-contenido="3">
              <div class="p-notification_contenedor" id="id-comprar-domicilioEnvio_contenedor_message">
                <div class="p-notification p-notification_info">
                  <span><b>No has confirmado tu domicilio de envío.</b></span>
                </div>
              </div>

              <div class="g-comprar-mostrar_domEnvio"></div>
              <input type="hidden" id="id-confirmar-domicilio_envio" autocomplete="off">
              <p class="p-text p-text_p p-text_success g-domiEnvio-status">
                <span>
                  <i class="fas fa-check-circle"></i>
                </span>
                <span>Cambios realizados.</span>
              </p>

              <div class="p-cuenta-contenedor_botones">
                <button class="p-button p-button_success g-modal-derecho_open_button" id="id-comprar-modal_domicilio">
                  <span>
                    <i class="fas fa-edit"></i>
                  </span>
                  <span><b>Editar datos</b></span>
                </button>

                <button class="p-button p-button_info" id="id-comprar-confirmar_domicilio">
                  <span>
                    <i class="fas fa-check"></i>
                  </span>
                  <span><b>Confirmar datos</b></span>
                </button>
              </div>
            </div>
            <div class="p-acordeon-cabecera g-acordeon_cabecera" data-seccion="4">
              <h3>
                <span>
                  <i class="fas fa-fw fa-credit-card"></i>
                </span>
                <span>Método de pago</span>
              </h3>
            </div>
            <div class="p-acordeon-contenido_contenedor" data-contenido="4">
              <div class="p-notification_contenedor" id="id-comprar-metodoPago_contenedor_message">
                <div class="p-notification p-notification_info">
                  <span id="id-comprar-metodoPago_contenedor_message_span"></span>
                </div>
              </div>

              <h4 class="p-comprar-aviso p-text_p">Selecciona el método de pago:</h4>

              <div class="p-comprar-radioButton_contenedor" id="id-comprar-contenedor_pago">
<?php
                /* <input type="radio" class="p-comprar-radioButton_oculto" id="id-comprar-label_pago_1" name="modo_pago" value="Tarjeta" required>
                <label class="p-comprar-radioButton_label p-comprar-radioButton_label_pago" for="id-comprar-label_pago_1">
                  <span><b>Tarjeta de crédito/débito</b></span>
                </label> */
?>
                <input type="radio" class="p-comprar-radioButton_oculto" id="id-comprar-label_pago_2" name="modo_pago" value="Depósito/Transferencia" required>
                <label class="p-comprar-radioButton_label p-comprar-radioButton_label_pago" for="id-comprar-label_pago_2">
                  <span><b>Depósito/Transferencia</b></span>
                </label>
              </div>
<?php
              /* <div class="p-comprar-contenedor_pago_elementos" id="id-pago_tarjeta_contenedor">
                <div class="p-notification p-notification_letter_info">
                  <span>Seleccionaste pagar con tarjeta, después de dar clic en el botón <b>Confirmar compra</b>, se mostrarán los campos para que la ingreses.</span>
                </div>
              </div> */
?>
              <div class="p-comprar-contenedor_pago_elementos" id="id-pago_bancario_contenedor">
                <div class="p-notification p-notification_letter_info p-margin-bottom_1rem">
                  <span>
                    <i class="fas fa-info-circle"></i>
                  </span>
                  <span class="p-notification_p"><b>Recuerda realizar tu depósito/transferencia en menos de 24 horas con los datos que se te facilitarán al concretar la compra.</b></span>
                </div>

                <div class="p-notification p-notification_letter_warning">
                  <span>
                    <i class="fas fa-exclamation-triangle"></i>
                  </span>
                  <span class="p-notification_p"><b>AVISO: Considera que para depósito/transferencia debe ser dentro de días hábiles de Lunes a Jueves en horario hábil y los viernes antes de las 12:00 pm. De lo contrario la compra será cancelada.</b></span>
                </div>
              </div>
            </div>
            <div class="p-acordeon-cabecera p-acordeon-cabecera_success g-acordeon_cabecera" data-seccion="5">
              <h3>
                <span>
                  <i class="fas fa-fw fa-file-alt"></i>
                </span>
                <span>Resumen de compra</span>
              </h3>
            </div>
            <div class="p-acordeon-contenido_contenedor" data-contenido="5">

              <div class="p-columnas">
                <div class="p-columna p-col_6_12">
                  <div class="p-comprar-cont_detalles">
                    <h4>MÉTODO DE ENVÍO</h4>
                    <p class="p-text_p" id="id-resumen-metodo_envio_texto">Aún no has seleccionado un método de envío.</p>
                  </div>

                  <div class="p-comprar-cont_detalles p-elemento-oculto" id="id-resumen-contenedor_domicilioEnvio">
                    <h4>DOMICILIO DE ENVÍO</h4>
                    <div class="g-comprar-mostrar_domEnvio"></div>
                  </div>

                  <div class="p-comprar-cont_detalles">
                    <h4>MÉTODO DE PAGO</h4>
                    <p class="p-text_p" id="id-resumen-metodo_pago_texto" style="margin: 0;">Aún no has seleccionado un método de pago.</p>
                  </div>
                </div>
<?php
      $total_pagar_sinIVA = $total_pagar / 1.16;
      $iva = $total_pagar - round($total_pagar_sinIVA, 2);
?>
                <div class="p-columna">
                  <div class="p-comprar-resumen">
                    <h4>DETALLE</h4>
                    <div class="p-comprar-resumen_div">
                      <p class="p-comprar-resumen_div_texto">
                        <span><b>Productos (sin IVA):</b></span><span>$ <?php echo number_format($total_pagar_sinIVA, 2); ?></span>
                      </p>

                      <p class="p-comprar-resumen_div_texto">
                        <span><b>Envío (sin IVA):</b></span><span id="id-comprar-span_text_envio">----</span>
                      </p>

                      <p class="p-comprar-resumen_div_texto">
                        <span><b>IVA 16%:</b></span><span id="id-comprar-span_iva">$ <?php echo number_format($iva, 2); ?></span>
                      </p>

                      <p class="p-comprar-resumen_div_texto p-comprar-resumen_total">
                        <span>Total a pagar:</span><span id="id-comprar-span_text_tot">$ <?php echo number_format($total_pagar, 2); ?></span>
                      </p>

                      <input type="hidden" name="conektaTokenId" id="conektaTokenId">
                      <input type="hidden" id="id-comprar-total_precio_productos_sinIVA" value="<?php echo number_format($total_pagar_sinIVA, 2, '.', ''); ?>" autocomplete="off">

                      <input type="hidden" id="id-comprar-seccion" autocomplete="off" value="<?php echo $seccion; ?>">
                      <input type="hidden" id="id-comprar-metodo_envio" autocomplete="off">
                      <input type="hidden" id="id-comprar-costo_envio_total" value="<?php echo number_format($costo_envio_total, 2); ?>">
                      <input type="hidden" id="id-comprar-metodo_pago" autocomplete="off">
                      <input type="hidden" id="id-comprar-total_precio_productos" value="<?php echo number_format($total_pagar, 2, '.', ''); ?>" autocomplete="off">
                      <input type="hidden" id="id-comprar-total_pagar" value="<?php echo number_format($total_pagar, 2, '.', ''); ?>" autocomplete="off">
                      <div class="p-buttons_center p-cuenta-contenedor_botones" style="padding: 0;">
                        <button class="p-button p-button_buy p-button_detalles" id="id-comprar-finalizar_compra_button">
                          <span>
                            <i class="fas fa-check"></i>
                          </span>
                          <span><b>Confirmar compra</b></span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <!--Modal confirmacion-->
              <div class="p-modal" id="id-modal-comprar">
                <div class="p-modal-fondo g-modal-cerrar_comprar"></div>
                <div class="p-modal-contenido">
                  <div class="p-modal-contenido_elementos">
                    <p class="p-label p-modal-label">Revisa que todos los datos sean correctos, ¿deseas continuar con la compra?</p>
                    <div>
                      <button class="p-button p-button_info" id="id-modal-confirmar_comprar">Si</button>
                      <button class="p-button p-button_delete g-modal-cerrar_comprar">No</button>
                    </div>
                  </div>
                </div>
              </div>

              <!--Modal loading-->
              <div class="p-modal" id="id-modal-loading">
                <div class="p-modal-fondo"></div>
                <div class="p-modal-contenido" style="text-align: left !important;">
                  <div class="p-modal-contenido_elementos">
                    <div class="p-loading-general p-producto-loading" id="id-modal-loading_activar">
                      <div></div>
                      <p><b>Procesando compra</b></p>
                    </div>

                    <!-- 1 -->
                    <div class="p-notification_contenedor" id="id-comprar-contenedor_msg_info">
                      <div class="p-notification p-notification_letter_info">
                        <span><i class="fas fa-info-circle"></i></span>
                        <p class="p-notification_p"><span id="id-comprar-contenedor_msg_info_span"></span></p>
                      </div>
                    </div>

                    <!-- 2 -->
                    <div class="p-notification_contenedor" id="id-comprar-contenedor_msg_error">
                      <div class="p-notification p-notification_letter_error">
                        <span><i class="fas fa-times-circle"></i></span>
                        <p class="p-notification_p"><span id="id-comprar-contenedor_msg_error_span"></span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div class="p-modal-derecho_contenedor" id="id-comprar-modal_general">
            <div class="p-modal-derecho_contenedor_fondo" id="id-comprar-modal_general_fondo"></div>
            <div class="p-modal-derecho_contenedor_contenido" id="id-comprar-modal_general_contenido">
              <div class="p-comprar-modal_cont" id="id-comprar-modal_general_contElementos">
                <div class="p-loading-general" id="id-comprar-comprando_loading" style="display: block;">
                  <div></div>
                </div>
              </div>
            </div>
          </div>
<?php
	  }
  }
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php';?>

<?php include 'templates/footer_scripts_jquery.php';?>

<?php include 'templates/scripts_validarTarjeta.php';?>

		<script>
			var seccion = "comprar";
		</script>

<?php include 'templates/scripts_domicilioEnvio.php';?>

		<script>
      $(document).ready(function(){
        /*
        * BOTON PARA ABRIR MODAL DE CONFIRMACION DE LA COMPRA
        */
        $(document).on('click', '#id-comprar-finalizar_compra_button', function(){
          $('document, #id-modal-comprar').addClass('p-modal-open');
          $('html').addClass('p-modal-derecho_window_no_scroll');
        });
        /*
        * BOTON "NO" PARA CANCELAR LA CONFIRMACION DE LA COMPRA
        */
        $(document).on('click', '.g-modal-cerrar_comprar', function(){
          $('document, #id-modal-comprar').removeClass('p-modal-open');
          $('html').removeClass('p-modal-derecho_window_no_scroll');
        });
        /*
        * BOTON "SI" PARA CONFIRMAR LA COMPRA
        */
        $(document).on('click', '#id-modal-confirmar_comprar', function(){
          $('document, #id-modal-comprar').removeClass('p-modal-open');
          $('html').removeClass('p-modal-derecho_window_no_scroll');

          var proceso_completo = false;
          // PREGUNTA SI EL METODO DE ENVIO SE ELIGIO
          if($('#id-comprar-metodo_envio').val() === ""){
            // SI NO SE HA SELECCIONADO EL METODO DE ENVIO SE HACE LO SIGUIENTE
            $('html, body').animate({ scrollTop: $('document, .g-acordeon_cabecera[data-seccion=2]').offset().top - 100 }, 600);
            setTimeout(function(){ $('document, .g-acordeon_cabecera[data-seccion=2]').click(); }, 1000);

            setTimeout(function(){
              $('document, #id-comprar-metodoEnvio_contenedor_message').addClass('p-notification_margin_bottom_cont');
              $('document, #id-comprar-metodoEnvio_contenedor_message').slideDown('fast');
            }, 1500);

            setTimeout(function(){
              $('document, #id-comprar-metodoEnvio_contenedor_message').slideUp('fast');
              $('document, #id-comprar-metodoEnvio_contenedor_message').removeClass('p-notification_margin_bottom_cont');
            }, 5000);

            proceso_completo = false;
          }else{
            proceso_completo = true;
          }

          // PREGUNTA SI CONFIRMÓ EL DOMICILIO DE ENVIO
          if(proceso_completo){
            if($('document, #id-confirmar-domicilio_envio').val() === "1"){
              proceso_completo = true;
            }else{
              $('html, body').animate({ scrollTop: $('document, .g-acordeon_cabecera[data-seccion=3]').offset().top - 100 }, 600);
              setTimeout(function(){ $('document, .g-acordeon_cabecera[data-seccion=3]').click(); }, 1000);

              setTimeout(function(){
                $('document, #id-comprar-domicilioEnvio_contenedor_message').addClass('p-notification_margin_bottom_cont');
                $('document, #id-comprar-domicilioEnvio_contenedor_message').slideDown('fast');
              }, 1500);

              setTimeout(function(){
                $('document, #id-comprar-domicilioEnvio_contenedor_message').slideUp('fast');
                $('document, #id-comprar-domicilioEnvio_contenedor_message').removeClass('p-notification_margin_bottom_cont');
              }, 5000);

              proceso_completo = false;
            }
          }

          // PREGUNTA SI EXISTE UN TIPO DE METODO DE PAGO ELEGIDO
          if(proceso_completo){
            if($('document, #id-comprar-metodo_pago').val() === ""){
              // SI NO SE HA SELECCIONADO EL METODO DE PAGO SE HACE LO SIGUIENTE
              $('html, body').animate({ scrollTop: $('document, .g-acordeon_cabecera[data-seccion=4]').offset().top - 100 }, 600);
              setTimeout(function(){ $('document, .g-acordeon_cabecera[data-seccion=4]').click(); }, 1000);

              setTimeout(function(){
                $('document, #id-comprar-metodoPago_contenedor_message_span').html('<b>No has seleccionado el método de pago.</b>');
                $('document, #id-comprar-metodoPago_contenedor_message').addClass('p-notification_margin_bottom_cont');
                $('document, #id-comprar-metodoPago_contenedor_message').slideDown('fast');
              }, 1500);

              setTimeout(function(){
                $('document, #id-comprar-metodoPago_contenedor_message').slideUp('fast');
                $('document, #id-comprar-metodoPago_contenedor_message').removeClass('p-notification_margin_bottom_cont');
                $('document, #id-comprar-metodoPago_contenedor_message_span').html('');
              }, 5000);

              proceso_completo = false;
            }else{
              proceso_completo = true;
            }

            // PREGUNTA QUE TIPO DE METODO DE PAGO SE ELIGIO
            if(proceso_completo){
              let metodo_pago = $('document, #id-comprar-metodo_pago').val();

              switch(metodo_pago){
                case 'Depósito/Transferencia':
                  let seccion = $('document, #id-comprar-seccion').val(),
                      metodo_envio = $('document, #id-comprar-metodo_envio').val(),
                      costo_envio_total = $('document, #id-comprar-costo_envio_total').val(),
                      total_precio_productos = $('document, #id-comprar-total_precio_productos').val(),
                      total_pagar = $('document, #id-comprar-total_pagar').val(),
                      unidades_comprar = $('document, #id-comprar-unidades_comprar').val(),
                      data = new FormData();

                  data.append('accion', 'comprar');
                  data.append('seccion', seccion);
                  data.append('metodo_envio', metodo_envio);
                  data.append('costo_envio_total', costo_envio_total);
                  data.append('metodo_pago', metodo_pago);
                  data.append('total_precio_productos', total_precio_productos);
                  data.append('total_pagar', total_pagar);
                  data.append('unidades_comprar', unidades_comprar);

                  // Muestra el loading de procesando compra
                  $('document, #id-modal-loading').addClass('p-modal-open');
                  $('html').addClass('p-modal-derecho_window_no_scroll');
                  $('document, #id-modal-loading_activar').slideDown('fast');

                  $('document, #id-comprar-finalizar_compra_button').prop('disabled', true);

                  fetch('procesos/comprar/proceso_comprar_productos.php', {
                    method: 'POST',
                    body: data
                  }).then(response => {
                    return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
                  }).then(datos => {
                    let respuesta = datos.respuesta, mensaje = datos.mensaje, link = datos.link;

                    switch(respuesta){
                      case '0':
                        console.error(mensaje);
                        $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);

                        // Oculta loading de procesando compra
                        $('document, #id-modal-loading_activar').slideDown('fast');
                        // Oculta el modal del loading
                        $('document, #id-modal-loading').removeClass('p-modal-open');
                        $('html').removeClass('p-modal-derecho_window_no_scroll');
                        break;

                      case '1':
                        // Oculta loading de procesando compra
                        $('document, #id-modal-loading_activar').slideUp('fast');

                        $('document, #id-comprar-contenedor_msg_info_span').html(mensaje);
                        $('document, #id-comprar-contenedor_msg_info').slideDown('fast');

                        setTimeout(function(){
                          $('document, #id-comprar-contenedor_msg_info').slideUp('fast');

                          // Oculta el modal del loading
                          $('document, #id-modal-loading').removeClass('p-modal-open');
                          $('html').removeClass('p-modal-derecho_window_no_scroll');
                        }, 10000);

                        $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
                        break;

                      case '2':
                        // Oculta loading de procesando compra
                        $('document, #id-modal-loading_activar').slideUp('fast');

                        $('document, #id-comprar-contenedor_msg_error_span').html(mensaje);
                        $('document, #id-comprar-contenedor_msg_error').slideDown('fast');

                        setTimeout(function(){
                          $('document, #id-comprar-contenedor_msg_error').slideUp('fast');

                          // Oculta el modal del loading
                          $('document, #id-modal-loading').removeClass('p-modal-open');
                          $('html').removeClass('p-modal-derecho_window_no_scroll');
                        }, 10000);

                        $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
                        break;

                      case '3':
                        window.location.href = link;
                        break;
                    }
                  }).catch(error => {
                    console.error('[Error] - ', error);
                  });
                  break;

                case 'Tarjeta':
                  console.log(...data);
                  alert('Aún no se habilita la compra desde la tarjeta.');
                  /* mostrar_campos_tarjeta();
                  $('html').toggleClass('p-modal-derecho_window_no_scroll');
                  $('document, #id-comprar-modal_general').slideToggle('fast');
                  $('.p-modal-derecho_contenedor_contenido').toggleClass('p-modal-derecho_contenedor_activo'); */
                  break;

                default:
                  // Muestra el loading de procesando compra
                  $('document, #id-modal-loading').addClass('p-modal-open');
                  $('html').addClass('p-modal-derecho_window_no_scroll');

                  $('document, #id-comprar-contenedor_msg_info_span').html('Sólo se permiten los métodos de pago con <b>Depósito/Transferencia</b> o <b>Tarjeta</b>. Éste método no es válido.');
                  $('document, #id-comprar-contenedor_msg_info').slideDown('fast');

                  setTimeout(function(){
                    $('document, #id-comprar-contenedor_msg_info').slideUp('fast');

                    // Oculta el modal del loading
                    $('document, #id-modal-loading').removeClass('p-modal-open');
                    $('html').removeClass('p-modal-derecho_window_no_scroll');
                  }, 10000);

                  $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
              }
            }
          }
        });
        /*
        * BOTON - CONFIRMAR PAGO TARJETA
        */
        /* $(document).on('click', '#id-confirmar-pago', function(){
          var name = $('document, .g-tarjeta-nombre_input').val().toUpperCase(),
              number = $('document, .g-tarjeta-numero_input').val(),
              card_type = Conekta.card.getBrand(number),
              month = $('document, .g-tarjeta-mes_exp_input').val(),
              year = $('document, .g-tarjeta-anio_exp_input').val(),
              cvc = $('document, .g-tarjeta-cvc_input').val(),
              correcto = false;

          var conektaSuccessResponseHandler = function(token) {
            $('document, #conektaTokenId').val(token.id);
            $('document, #id-pasarela-pagos_alert_success').slideDown('fast');
            $('document, #id-pasarela-pagos_loading').slideUp('fast');
            $('document, #id-comprar-finalizar_compra_button').prop("disabled", true);

            setTimeout(function(){
              $('document, #id-comprar-modal_button_cancelar').click();

              // Muestra el loading de procesando compra
              $('document, #id-modal-loading').addClass('p-modal-open');
              $('html').addClass('p-modal-derecho_window_no_scroll');

              let seccion = $('document, #id-comprar-seccion').val(),
                  sku_prod = $('document, #T-comprar_sku_prod').val(),
                  metodo_pago = $('document, #id-comprar-metodo_pago').val(),
                  metodo_envio = $('document, #id-comprar-metodo_envio').val(),
                  costo_envio_almacen_1 = $('document, #id-comprar-costoEnvio_almacen_1').val(),
                  costo_envio_almacen_7 = $('document, #id-comprar-costoEnvio_almacen_7').val(),
                  costo_envio_almacen_56 = $('document, #id-comprar-costoEnvio_almacen_56').val(),
                  costo_envio_almacen_74 = $('document, #id-comprar-costoEnvio_almacen_74').val(),
                  costo_envio_total = $('document, #id-comprar-costo_envio_total').val(),
                  precio_pedido = $('document, #id-comprar-total_precio_productos').val(),
                  total_pagar = $('document, #id-comprar-total_pagar').val(),
                  token_id = $('document, #conektaTokenId').val(),
                  array_bloques = number.split(' '),
                  ultimos_digitos = array_bloques[array_bloques.length - 1],
                  data = new FormData();

              switch(card_type){
                case 'visa':
                  card_type = "Visa";
                  break;

                case 'visa_electron':
                  card_type = "Visa Electron";
                  break;

                case 'mastercard':
                  card_type = "Mastercard";
                  break;

                case 'amex':
                  card_type = "American Express";
                  break;

                case 'carnet':
                  card_type = "Carnet";
                  break;
              }

              array_bloques = [];

              data.append('accion', 'comprar');
              data.append('seccion', seccion);
              data.append('sku_prod', sku_prod);
              data.append('metodo_pago', metodo_pago);
              data.append('metodo_envio', metodo_envio);
              data.append('costo_envio_almacen_1', costo_envio_almacen_1);
              data.append('costo_envio_almacen_7', costo_envio_almacen_7);
              data.append('costo_envio_almacen_56', costo_envio_almacen_56);
              data.append('costo_envio_almacen_74', costo_envio_almacen_74);
              data.append('costo_envio_total', costo_envio_total);
              data.append('precio_pedido', precio_pedido);
              data.append('total_pagar', total_pagar);
              data.append('card_ultimos_digitos', ultimos_digitos);
              data.append('card_tipo', card_type);
              data.append('token_id', token_id);

              fetch('procesos/comprar/proceso_tarjeta_producto.php', {
                method: 'POST',
                body: data
              }).then(response => {
                return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
              }).then(datos => {
                let respuesta = datos.respuesta, mensaje = datos.mensaje, link = datos.link;

                // Oculta el loading de procesando compra
                $('document, #id-modal-loading').removeClass('p-modal-open');
                $('html').removeClass('p-modal-derecho_window_no_scroll');

                switch(respuesta){
                  case '0':
                    console.error(mensaje);
                    $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
                    break;

                  case '1':
                    $('document, #id-comprar-contenedor_msg_info').addClass('p-notification_margin_bottom_cont');
                    $('document, #id-comprar-contenedor_msg_info_span').html(mensaje);
                    $('document, #id-comprar-contenedor_msg_info').slideDown('fast');

                    setTimeout(function(){
                      $('document, #id-comprar-contenedor_msg_info').slideUp('fast');
                      $('document, #id-comprar-contenedor_msg_info').removeClass('p-notification_margin_bottom_cont');
                    }, 10000);
                    $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
                    break;

                  case '2':
                    $('document, #id-comprar-contenedor_msg_error').addClass('p-notification_margin_bottom_cont');
                    $('document, #id-comprar-contenedor_msg_error_span').html(mensaje);
                    $('document, #id-comprar-contenedor_msg_error').slideDown('fast');

                    setTimeout(function(){
                      $('document, #id-comprar-contenedor_msg_error').slideUp('fast');
                      $('document, #id-comprar-contenedor_msg_error').removeClass('p-notification_margin_bottom_cont');
                    }, 10000);
                    $('document, #id-comprar-finalizar_compra_button').prop("disabled", false);
                    break;

                  case '3':
                    window.location.href = link;
                    break;
                }
              }).catch(error => {
                console.error('[Error] - ', error);
              });
            }, 2500);
          };

          var conektaErrorResponseHandler = function(response) {
            $('document, #id-pasarela-pagos_alert_error_span').html('<b>' + response.message_to_purchaser + '</b>');

            setTimeout(function(){
              $('document, #id-pasarela-pagos_alert_error').slideDown('fast');
              $('document, #id-pasarela-pagos_loading').slideUp('fast');
            }, 500);

            setTimeout(function(){ $('document, #id-pasarela-pagos_alert_error').slideUp('fast'); }, 6500);
            setTimeout(function(){ $('document, #id-pasarela-pagos_alert_error_span').html(''); }, 6700);
          };

          $('document, #id-pasarela-pagos_alert_success').slideUp('fast');
          $('document, #id-pasarela-pagos_alert_error').slideUp('fast');
          $('document, #id-pasarela-pagos_alert_error_span').html('');
          $('document, #id-pasarela-pagos_loading').slideDown('fast');

          (name !== "") ? correcto = true : (
            $('document, .g-tarjeta-nombre_alert_error').slideDown('fast'),
            $('document, .g-tarjeta-nombre_input').addClass('p-input_error'),
            $('document, .g-tarjeta-nombre_input').focus(),
            $('document, #id-pasarela-pagos_loading').slideUp('fast'),
            correcto = false
          );

          if(correcto){
            (/^[a-zA-Z\ ]+$/.test(name)) ? correcto = true : (
              $('document, .g-tarjeta-nombre_alert_info').slideDown('fast'),
              $('document, .g-tarjeta-nombre_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (number !== "") ? correcto = true : (
              $('document, .g-tarjeta-numero_alert_error').slideDown('fast'),
              $('document, .g-tarjeta-numero_input').addClass('p-input_error'),
              $('document, .g-tarjeta-numero_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (Conekta.card.validateNumber(number)) ? correcto = true : (
              $('document, .g-tarjeta-numero_alert_info').slideDown('fast'),
              $('document, .g-tarjeta-numero_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (card_type === "visa" || card_type === "visa_electron" || card_type === "mastercard" || card_type === "amex" || card_type === "carnet") ? correcto = true : (
              $('document, #id-pasarela-pagos_alert_info').slideDown('fast'),
              $('document, .g-tarjeta-numero_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              setTimeout(function(){ $('document, #id-pasarela-pagos_alert_info').slideUp('fast') }, 5000),
              correcto = false
            );
          }

          if(correcto){
            (month !== "") ? correcto = true : (
              $('document, .g-tarjeta-mes_exp_alert_error').slideDown('fast'),
              $('document, .g-tarjeta-mes_exp_input').addClass('p-input_error'),
              $('document, .g-tarjeta-mes_exp_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (year !== "") ? correcto = true : (
              $('document, .g-tarjeta-anio_exp_alert_error').slideDown('fast'),
              $('document, .g-tarjeta-anio_exp_input').addClass('p-input_error'),
              $('document, .g-tarjeta-anio_exp_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (Conekta.card.validateExpirationDate(month, year)) ? correcto = true : (
              $('document, .g-tarjeta-fecha_exp_alert_info').slideDown('fast'),
              (month === "01" || month === "02" || month === "03" || month === "04" || month === "05" || month === "06" || month === "07" || month === "08" || month === "09" || month === "10" || month === "11" || month === "12") ? $('document, .g-tarjeta-anio_exp_input').focus() : $('document, .g-tarjeta-mes_exp_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto){
            (cvc !== "") ? correcto = true : (
              $('document, .g-tarjeta-cvc_alert_error').slideDown('fast'),
              $('document, .g-tarjeta-cvc_input').addClass('p-input_error'),
              $('document, .g-tarjeta-cvc_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );
          }

          if(correcto)
            (Conekta.card.validateCVC(cvc)) ? correcto = true : (
              $('document, .g-tarjeta-cvc_alert_info_1').slideDown('fast'),
              $('document, .g-tarjeta-cvc_input').focus(),
              $('document, #id-pasarela-pagos_loading').slideUp('fast'),
              correcto = false
            );

          if(correcto){
            var tokenParams = {
              "card": {
                "number": number,
                "name": name,
                "exp_year": year,
                "exp_month": month,
                "cvc": cvc
              }
            };

            $('document, .p-comprar-modal_cont_campos').animate({ scrollTop: 0 }, 600);

            Conekta.Token.create(tokenParams, conektaSuccessResponseHandler, conektaErrorResponseHandler);
          }
        }); */
        ////////////////// SECCION REALIZAR COMPRA //////////////////
        /*
        * Modal
        */
        $(document).on('click', '#id-comprar-modal_general_fondo', function() {
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
          $('document, #id-comprar-modal_general').slideToggle('fast');
          $('document, #id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
          $('document, #id-comprar-modal_general_contElementos').html('');
        });

        $(document).on('click', '#id-comprar-modal_button_cancelar', function(){
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
          $('document, #id-comprar-modal_general').slideToggle('fast');
          $('document, #id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
          $('document, #id-comprar-modal_general_contElementos').html('');
        });
        /*
        * ACORDEON PARA REALIZAR COMPRA
        */
        $(document).on('click', '.g-acordeon_cabecera', function(){
          let seccion = $(this).attr('data-seccion');

          if($(this).hasClass('p-acordeon-cabecera_activa')){
            $(this).removeClass('p-acordeon-cabecera_activa');
            $('document, div[data-contenido=' + seccion + ']').slideUp('fast');
          }else{
            for(let posicion = 1; posicion <= 5; posicion++){
              $('document, .g-acordeon_cabecera[data-seccion=' + posicion + ']').removeClass('p-acordeon-cabecera_activa');
              $('document, div[data-contenido=' + posicion + ']').slideUp('fast');
            }

            $(this).addClass('p-acordeon-cabecera_activa');
            $('document, div[data-contenido=' + seccion + ']').slideDown('fast');
          }
        });
        /*
        * Domicilio de envio - Ejecutar funcion para mostrar domicilio
        */
        mostrar_domicilioEnvio();
        /*
        * Domicilio de envio - Boton de editar
        */
        $(document).on('click', '#id-comprar-modal_domicilio', function(){
          mostrar_campos_editarDomicilioEnvio();
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
          $('document, #id-comprar-modal_general').slideToggle('fast');
          $('document, #id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
        });
        /*
        * Metodo de envio - Radio Button de Sucursal y Paqueteria
        */
        $(document).on('click', '#id-comprar-contenedor_envio label', function(){
          let id = $(this).attr('for'), metodo_envio = $('document, #'.concat(id)).val(), total_precio_productos = $('document, #id-comprar-total_precio_productos').val(), total_precio_productos_sinIVA = $('document, #id-comprar-total_precio_productos_sinIVA').val(), total_pagar_calculado, costo_envio_sinIVA, total_pagar_sinFormato, total_pagar_conFormato, iva_compra;

          $('document, #id-comprar-metodo_envio').val(metodo_envio);

          switch(metodo_envio){
            case 'Sucursal':
              total_pagar_calculado = parseFloat(total_precio_productos) + 0.00;
              costo_envio_sinIVA = 0.00;

              $('document, #id-comprar-span_text_envio').html('$ 0.00');
              $('document, #id-resumen-metodo_envio_texto').html('Recoger en sucursal ($ 0.00 MXN).');
              $('document, .g-acordeon_cabecera[data-seccion=3]').slideUp('fast');
              $('document, #id-resumen-contenedor_domicilioEnvio').addClass('p-elemento-oculto');
              break;

            case 'Paqueteria':
              let costo_envio = $('document, #id-comprar-costo_envio_total').val(), texto_span_envio_sinIVA = '', texto_detalles_metodo_envio = '';

              total_pagar_calculado = parseFloat(total_precio_productos) + parseFloat(costo_envio);
              costo_envio_sinIVA = costo_envio === '0.00' ? 0.00 : parseFloat(costo_envio) / 1.16;
              texto_span_envio_sinIVA = costo_envio === '0.00' ? 'Envío gratis' : '$ ' + costo_envio_sinIVA.toFixed(2);
              texto_detalles_metodo_envio = costo_envio === '0.00' ? 'Envío gratis' : '$ ' + costo_envio + ' MXN';

              $('document, #id-comprar-span_text_envio').html(texto_span_envio_sinIVA);
              $('document, #id-resumen-metodo_envio_texto').html('Enviar por paquetería (' + texto_detalles_metodo_envio + ').');
              $('document, .g-acordeon_cabecera[data-seccion=3]').slideDown('fast');
              setTimeout(function(){ $('document, .g-acordeon_cabecera[data-seccion=3]').click(); }, 500);
              $('document, #id-resumen-contenedor_domicilioEnvio').removeClass('p-elemento-oculto');
              break;
          }

          $('document, .g-acordeon_cabecera[data-seccion=2]').addClass('p-acordeon-cabecera_success');
          $('document, #id-comprar-metodoEnvio_contenedor_message').slideUp('fast');
          $('document, #id-comprar-metodoEnvio_contenedor_message').removeClass('p-notification_margin_bottom_cont');

          iva_compra = parseFloat(total_pagar_calculado) - ( parseFloat(total_precio_productos_sinIVA) + parseFloat(costo_envio_sinIVA.toFixed(2)) );

          total_pagar_sinFormato = parseFloat(total_precio_productos_sinIVA) + parseFloat(costo_envio_sinIVA) + parseFloat(iva_compra);
          total_pagar_conFormato = cantidadMilesSeparados(total_pagar_sinFormato.toFixed(2));

          iva_compra = cantidadMilesSeparados(iva_compra.toFixed(2));

          $('document, #id-comprar-span_iva').html('$ ' + iva_compra);
          $('document, #id-comprar-span_text_tot').html('$ ' + total_pagar_conFormato);
          $('document, #id-comprar-total_pagar').val(total_pagar_sinFormato.toFixed(2));
        });
        /*
        * Domicilio de envio - Confirmar domicilio
        */
        $(document).on('click', '#id-comprar-confirmar_domicilio', function(){
          $('document, .g-acordeon_cabecera[data-seccion=3]').addClass('p-acordeon-cabecera_success');
          $('document, #id-confirmar-domicilio_envio').val('1');
          $(this).slideUp('fast');
          $('document, #id-comprar-domicilioEnvio_contenedor_message').slideUp('fast');
          $('document, #id-comprar-domicilioEnvio_contenedor_message').removeClass('p-notification_margin_bottom_cont');
          setTimeout(function(){ $('document, .g-acordeon_cabecera[data-seccion=4]').click(); }, 500);
        });
        /*
        * Metodo de pago - Radio Button de Depósito / Transferencia
        */
        $(document).on('click', '#id-comprar-contenedor_pago label', function(){
          let id = $(this).attr('for'), input = $('document, #'.concat(id)).val();

          $('document, #id-comprar-metodo_pago').val(input);

          switch(input){
            case "Tarjeta":
              $('document, .g-acordeon_cabecera[data-seccion=4]').addClass('p-acordeon-cabecera_success');
              $('document, #id-resumen-metodo_pago_texto').html('Tarjeta de crédito / débito');
              $('document, #id-pago_tarjeta_contenedor').slideUp('fast');
              $('document, #id-pago_bancario_contenedor').slideUp('fast');
              setTimeout(function(){ $('document, #id-pago_tarjeta_contenedor').slideDown('fast'); }, 200);
              break;

            case "Depósito/Transferencia":
              $('document, .g-acordeon_cabecera[data-seccion=4]').addClass('p-acordeon-cabecera_success');
              $('document, #id-resumen-metodo_pago_texto').html('Depósito / Transferencia');
              $('document, #id-pago_tarjeta_contenedor').slideUp('fast');
              $('document, #id-pago_bancario_contenedor').slideUp('fast');
              setTimeout(function(){ $('document, #id-pago_bancario_contenedor').slideDown('fast'); }, 200);
              break;
          }

          $('document, #id-comprar-metodoPago_contenedor_message').slideUp('fast');
          $('document, #id-comprar-metodoPago_contenedor_message').removeClass('p-notification_margin_bottom_cont');
          setTimeout(function(){ $('document, #id-comprar-metodoPago_contenedor_message').html(''); }, 1000);
        });
        ////////////////// FUNCIONES ////////////////// - [COMPLETO]
        /*
        * Funcion para formatear cantidades en miles con coma
        */
        function cantidadMilesSeparados(cantidad_redondeada){
          let cantidad_miles = "", numeros_enteros = cantidad_redondeada.substring(0, cantidad_redondeada.length - 3), decimales = "";

					// Se obtiene del punto decimal en adelante - Numeros decimales con punto decimal
          decimales = cantidad_redondeada.substring(cantidad_redondeada.length - 3);

          for(var contador = numeros_enteros.length - 1; contador >= 0; contador--){
						let mil = ','.concat(cantidad_miles);

						cantidad_miles = cantidad_miles.length === 0 ? numeros_enteros.substring(contador) : ( cantidad_miles.length === 3 || cantidad_miles.length === 7 ? numeros_enteros.substring(contador, contador + 1).concat(mil) : numeros_enteros.substring(contador, contador + 1).concat(cantidad_miles) );
          }

          return cantidad_miles + decimales;
        }
        /*
        * Domicilio de envio - Funcion para mostrar domicilio
        */
        function mostrar_domicilioEnvio(){
          let data = new FormData();

          data.append('accion', 'mostrar');

          fetch('procesos/comprar/mostrar_domicilioEnvio.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta, mensaje = datos.mensaje;

            if(respuesta === '1'){
              $('document, .g-comprar-mostrar_domEnvio').html(mensaje);
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        }
        /*
        * Domicilio de envio - Funcion para mostrar campos para editar el domicilio
        */
        function mostrar_campos_editarDomicilioEnvio(){
          let data = new FormData();

          data.append('accion', 'mostrar');

          fetch('procesos/comprar/mostrar_campos_editarDomicilioEnvio.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta, mensaje = datos.mensaje;

            if(respuesta === '1'){
              $('document, #id-comprar-modal_general_contElementos').html(mensaje);
							$('document, #id-comprar-ediDom_nombre_apellidos_input').focus();
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        }
        /*
        * Tarjeta - Funcion para mostrar campos para la tarjeta
        */
        function mostrar_campos_tarjeta(){
          let data = new FormData();

          data.append('accion', 'mostrar');

          fetch('procesos/comprar/mostrar_campos_tarjeta.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta, mensaje = datos.mensaje;

            if(respuesta === '1'){
              $('document, #id-comprar-modal_general_contElementos').html(mensaje);
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        }
      });
    </script>
<?php include 'templates/footer_scripts_principales.php';?>

	</body>
</html>