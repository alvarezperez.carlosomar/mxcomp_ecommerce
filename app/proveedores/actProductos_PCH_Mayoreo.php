<?php
require_once dirname(__DIR__, 1) . '/clases/producto/metodos_producto.php';
require_once dirname(__DIR__, 1) . '/funciones/validaciones_campos.php';
require_once dirname(__DIR__, 1) . '/clases/proveedor/metodos_PCH.php';
require_once dirname(__DIR__, 1) . '/funciones/convertir_url.php';
require_once dirname(__DIR__, 1) . '/conn.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('max_execution_time', '300');

date_default_timezone_set('America/Mexico_City'); // HORARIO DE MEXICO

$estado_proceso = "";
$mensaje_error = "";
$productosActualizados = 0;
$productosInsertados = 0;
$fechaScript_inicio = "";
$fechaScript_final = "";
$fechaCreacionBitacora = "";
$array_productos = [];
$proceso_correcto = false;

$codigoProveedor = '001';
$nombreProveedor = 'PCH MAYOREO';

echo "Código del proveedor: " . $codigoProveedor . " | Nombre del proveedor: " . $nombreProveedor . "<br><br>";

// SE CREA EL OBJETO DE PCH MAYOREO
$PCH_Mayoreo = new PCH_Mayoreo();

// SE CREAN OBJETOS PARA CONEXIONES DE BD
$Conn_mxcomp = new Conexion_mxcomp();
$Conn_admin = new Conexion_admin();
$metodosProducto = new Producto();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SE OBTIENE EL TIPO DE CAMBIO
if ($PCH_Mayoreo->tipoCambio()) {
  $tipo_cambio = round(trim($PCH_Mayoreo->tCambio_valorMoneda), 2);
  $proceso_correcto = true;
} else {
  $estado_proceso = "Error";
  $mensaje_error = "[Error PCH Mayoreo] - Tipo de cambio: " . $PCH_Mayoreo->tCambio_mensajeDetalles;
  echo $mensaje_error;
  $proceso_correcto = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SE OBTIENEN LOS DATOS DE LOS PRODUCTOS
if($proceso_correcto){
  if($PCH_Mayoreo->listaProductos()){
    $lista_productos = $PCH_Mayoreo->lProductos_productos;

    $posicion = 0;

    // RECORRER LA LISTA DE PRODUCTOS
    foreach($lista_productos as $producto){
      $skuProveedor = trim($producto->sku);
      $categoria = (string) trim($producto->seccion);
      $id_categoria = (string) trim($producto->id_seccion);

      $arrayNombreCategorias = array("OPEN BOX", "ARTICULOS PROMOCIONALES", "REP/ABIERTO");
      $arrayIDCategorias = array("6852", "6861", "6857");
      
      if( !preg_match('/\(RF\)/i', $skuProveedor) && !preg_match('/\(RE\)/i', $skuProveedor) && !preg_match('/\(RA\)/i', $skuProveedor) && !preg_match('/\(OB\)/i', $skuProveedor) && !preg_match('/\(O.B\)/i', $skuProveedor) && !preg_match('/\(OB.\)/i', $skuProveedor) && !preg_match('/\(O.B.\)/i', $skuProveedor) ){
        // SI NO SE ENCONTRARON ESOS CARACTERES, SE HACE LO SIGUIENTE

        // SI SE ENCUENTRA EL SKU EN ALGUNO DE LOS ARRAYS, SE HACE LO SIGUIENTE
        if(in_array($categoria, $arrayNombreCategorias) || in_array($id_categoria, $arrayIDCategorias)){
          $producto_valido = false;
        }else{
          $producto_valido = true;
        }
      }else{
        // SI SE ENCONTRO, NO SE GUARDA
        $producto_valido = false;
      }
      
      if($producto_valido){
        // SI TIENE DATOS EL INVENTARIO, SE GENERAN LOS ALMACENES
        if(count($producto->inventario) > 0){
          $existenciaTotal = 0;
          $almacenes = [];
  
          // SE GENERAN LOS ALMACENES CON SU EXISTENCIA, ASI COMO LA EXISTENCIA TOTAL
          foreach ($producto->inventario as $datos_inventario) {
            if ($datos_inventario->almacen_id === 1 || $datos_inventario->almacen_id === 5 || $datos_inventario->almacen_id === 23 || $datos_inventario->almacen_id === 36 || $datos_inventario->almacen_id === 15 || $datos_inventario->almacen_id === 19) {
              $existenciaAlmacen = (int) $datos_inventario->cantidad;

              if($existenciaAlmacen > 0){
                switch ($datos_inventario->almacen_id) {
                  case 1: // GUADALAJARA
                    $array_almacen = [
                      'id_almacen' => 1,
                      'nombre_almacen' => 'GUADALAJARA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
        
                  case 5: // DF AEROPUERTO
                    $array_almacen = [
                      'id_almacen' => 5,
                      'nombre_almacen' => 'DF AEROPUERTO',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
  
                  case 11: // MONTERREY
                    $array_almacen = [
                      'id_almacen' => 11,
                      'nombre_almacen' => 'MONTERREY',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
  
                  case 15: // MÉRIDA
                    $array_almacen = [
                      'id_almacen' => 15,
                      'nombre_almacen' => 'MÉRIDA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
        
                  case 23: // PUEBLA
                    $array_almacen = [
                      'id_almacen' => 23,
                      'nombre_almacen' => 'PUEBLA',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
        
                  case 36: // LEÓN
                    $array_almacen = [
                      'id_almacen' => 36,
                      'nombre_almacen' => 'LEÓN',
                      'existencia' => $existenciaAlmacen,
                    ];
                    break;
                }
                
                array_push($almacenes, $array_almacen);
                $existenciaTotal += $existenciaAlmacen;
              }
            }
          }

          // SI HAY EXISTENCIAS, SE BUSCA LA INFORMACION RESTANTE
          if($existenciaTotal > 0){
            $almacenes = json_encode($almacenes);

            $precioProveedor = round(trim($producto->precio), 2);
            $monedaProveedor = trim($producto->moneda);
            $precioMXcomp = round(trim($producto->precio), 2);
            $monedaMXcomp = trim($producto->moneda);

            if($monedaProveedor === "USD"){
              $precioMXcomp = round($precioMXcomp * $tipo_cambio, 2);
              $monedaMXcomp = "MXN";
            }

            if($monedaProveedor === "MN" || $monedaProveedor === "MXN"){
              $monedaMXcomp = "MXN";
            }
            
            $precioMXcomp = round($precioMXcomp * 1.32, 2);
            $activo = 1;
            
            $descripcion = (string) trim($producto->descripcion);
            $skuFabricante = (string) trim($producto->skuFabricante);
            $marca = (string) trim($producto->marca);
            $subcat_tipo = (string) trim($producto->linea);
            $subcat_especifica = (string) trim($producto->serie);

            $descripcion_editada = strpos($descripcion, $marca) ? $descripcion : $descripcion . ' ' . $marca;
            $descripcion_editada = strpos($descripcion_editada, $categoria) ? $descripcion_editada : $descripcion_editada . ' ' . $categoria;
            $descripcion_editada = strpos($descripcion_editada, $subcat_tipo) ? $descripcion_editada : $descripcion_editada . ' ' . $subcat_tipo;
            $descripcion_editada = strpos($descripcion_editada, $subcat_especifica) ? $descripcion_editada : $descripcion_editada . ' ' . $subcat_especifica;
            $descripcion_editada = $skuFabricante !== "" ? ( strpos($descripcion_editada, $skuFabricante) ? $descripcion_editada : $descripcion_editada . ' ' . $skuFabricante ) : $descripcion_editada;

            $descripcionURL = convertir_URL($descripcion_editada);

            // REVISA SI LOS CAMPOS ESTAN VACIOS
            $peso = trim($producto->peso_bruto) === "" ? (string) trim($producto->peso_bruto) : (float) round(trim($producto->peso_bruto), 2);
            $alto = trim($producto->alto) === "" ? (string) trim($producto->alto) : (float) round(trim($producto->alto), 2);
            $largo = trim($producto->largo) === "" ? (string) trim($producto->largo) : (float) round(trim($producto->largo), 2);
            $ancho = trim($producto->ancho) === "" ? (string) trim($producto->ancho) : (float) round(trim($producto->ancho), 2);
  
            $array_productos[$posicion] = array(
              'skuProveedor' => (string) $skuProveedor,
              'skuFabricante' => (string) $skuFabricante,
              'descripcion' => (string) $descripcion,
              'descripcionURL' => (string) $descripcionURL,
              'categoria' => (string) $categoria,
              'marca' => (string) $marca,
              'subcat_tipo' => (string) $subcat_tipo,
              'subcat_especifica' => (string) $subcat_especifica,
              'peso' => $peso,
              'alto' => $alto,
              'largo' => $largo,
              'ancho' => $ancho,
              'precioProveedor' => (float) $precioProveedor,
              'monedaProveedor' => (string) $monedaProveedor,
              'precioMXcomp' => (float) $precioMXcomp,
              'monedaMXcomp' => (string) $monedaMXcomp,
              'existenciaTotal' => (int) $existenciaTotal,
              'almacenes' => (string) $almacenes,
              'activo' => (int) $activo
            );
            
            $posicion++;
          }
        }
      }
    }

    $proceso_correcto = true;
  }else{
    $estado_proceso = "Error";
    $mensaje_error = "[Error PCH Mayoreo] - Productos: " . $PCH_Mayoreo->lProductos_mensajeDetalles;
    echo $mensaje_error;
    $proceso_correcto = false;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SE TRAEN LOS CAMPOS DE LA TABLA __sin_envio_gratis_subcategorias
$array_subcategorias_sinEnvioGratis = [];

if($proceso_correcto){
  try{
    $sql = "SELECT idSubcategoriaTipo, nombre, pesoMayor FROM __sin_envio_gratis_subcategorias";
    $stmt = $Conn_admin->pdo->prepare($sql);
    $stmt->execute();
    
    $posicion = 0;
    while($datos_subcategorias = $stmt->fetch(PDO::FETCH_ASSOC)){
      $array_subcategorias_sinEnvioGratis[$posicion] = [
        'idSubcatTipo' => $datos_subcategorias['idSubcategoriaTipo'],
        'nombre' => $datos_subcategorias['nombre'],
        'pesoMayor' => $datos_subcategorias['pesoMayor']
      ];
      $posicion++;
    }
    
    $estado_proceso = "Exito";
    $mensaje_error = NULL;
    $proceso_correcto = true;
    
    $stmt = null;
  }catch(PDOException $error){
    $estado_proceso = "Error";
    $mensaje_error = "[Error BD] - No se encontraron las subcategorías sin envío gratis: " . $error->getMessage();
    echo $mensaje_error;
    $proceso_correcto = false;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// SI TODO RESULTO BIEN, SE COMIENZA A ACTUALIZAR LOS DATOS DE LOS PRODUCTOS
if($proceso_correcto){
  if(count($array_productos) > 0){
    $tipo_error = "";
    $codigoProducto = '';

    // SE GENERA EL CÓDIGO DE PRODUCTO NUEVO
    if($metodosProducto->generarCodigo_consulta($nombreProveedor, $codigoProveedor)){
      $codigoProducto = (string) $metodosProducto->codigoProducto;
      $proceso_correcto = true;
    }else{
      $estado_proceso = "Error";
      $mensaje_error = $metodosProducto->mensajeError;
      echo $mensaje_error;
      $proceso_correcto = false;
    }

    // SE REALIZAN LOS REGISTROS CORRESPONDIENTES
    if($proceso_correcto){
      try{
        $Conn_mxcomp->pdo->beginTransaction();
        
        $fechaScript_inicio = date("Y-m-d H:i:s");
        
        foreach($array_productos as $indice=>$datos){
          $skuProveedor = (string) $datos['skuProveedor'];
          $skuFabricante = (string) $datos['skuFabricante'];
          $descripcion = (string) $datos['descripcion'];
          $descripcionURL = (string) $datos['descripcionURL'];
          $categoria = (string) $datos['categoria'];
          $marca = (string) $datos['marca'];
          $subcat_tipo = (string) $datos['subcat_tipo'];
          $subcat_especifica = (string) $datos['subcat_especifica'];
          $peso = (string) $datos['peso'];
          $alto = (string) $datos['alto'];
          $largo = (string) $datos['largo'];
          $ancho = (string) $datos['ancho'];
          $precioProveedor = (float) $datos['precioProveedor'];
          $monedaProveedor = (string) $datos['monedaProveedor'];
          $precioMXcomp = (float) $datos['precioMXcomp'];
          $monedaMXcomp = (string) $datos['monedaMXcomp'];
          $existenciaTotal = (int) $datos['existenciaTotal'];
          $almacenes = (string) $datos['almacenes'];
          $activo = (int) $datos['activo'];
          
          // SE GENERA EL NOMBRE DE LA IMAGEN APARTIR DE LA SKU DEL FABRICANTE
          $caracteres_reemplazar = array("\\", "/", ":", "?", "*", '"', "<", ">", "|", "#");
          
          if($skuFabricante === "" || $skuFabricante === "0"){
            $nombreImagen = NULL;
            $skuFabricante = NULL;
  
            $tieneImagen = "0";
            $numeroUbicacionImagen = NULL; // NO EXISTE UBICACION
            $cantidadImagenes = NULL;
            $versionImagen = NULL;
          }else{
            $nombreImagen = str_replace($caracteres_reemplazar, "_", $skuFabricante);
            
            // PRIMERO SE GENERA LA UBICACION DE LA IMAGEN PARA QUE SEA BUSCADA - TIPO 1
            $imagen = dirname(__DIR__, 1) . '/images/imagenes_productos/' . $marca . '/' . $nombreImagen . '.jpg';
            
            // SE PREGUNTA SI EXISTE ESA IMAGEN
            if(file_exists($imagen)){
              $tieneImagen = "1";
              $numeroUbicacionImagen = "1"; // NUEVA UBICACION
              $cantidadImagenes = "1";
              $versionImagen = "1";
    
              // PREGUNTA SI EXISTE A IMAGEN 2
              $imagen = dirname(__DIR__, 1) . '/images/imagenes_productos/' . $marca . '/' . $nombreImagen . '_2.jpg';
    
              if(file_exists($imagen)){
                $cantidadImagenes = "2";
              }
    
              // PREGUNTA SI EXISTE A IMAGEN 3
              $imagen = dirname(__DIR__, 1) . '/images/imagenes_productos/' . $marca . '/' . $nombreImagen . '_3.jpg';
    
              if(file_exists($imagen)){
                $cantidadImagenes = "3";
              }
            }else{
              $tieneImagen = "0";
              $numeroUbicacionImagen = NULL; // NO EXISTE UBICACION
              $cantidadImagenes = NULL;
              $versionImagen = NULL;
            }
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          
          // Actualiza la categoria correspondiente al producto
          $tipo_error = "[Error BD] - Categorias (Select): "; // En caso de que sea error
          
          $sql = "SELECT id FROM __categorias WHERE nombre = :nombre GROUP BY id";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':nombre', $categoria, PDO::PARAM_STR);
          $stmt->execute();
          $datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC);
          $activo_categoria = 1;
          $fecha_consulta = date("Y-m-d H:i:s");
  
          if($datos_categoria === false){
            $tipo_error = "[Error BD] - Categorias (Insert): ";
            
            $sql = "INSERT INTO __categorias(nombre, valor_BD, fechaCreacion) VALUES(:nombre, :valor_BD, :fechaCreacion)";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $categoria, PDO::PARAM_STR);
            $stmt->bindParam(':valor_BD', $activo_categoria, PDO::PARAM_INT);
            $stmt->bindParam(':fechaCreacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->execute();
            
            $tipo_error = "";
            $categoriaID = (int) $Conn_mxcomp->pdo->lastInsertId();
          }else{
            $tipo_error = "[Error BD] - Categorias (Update): ";
            $categoriaID = (int) $datos_categoria['id'];
            
            $sql = "UPDATE __categorias SET nombre = :nombre, valor_BD = :valor_BD, fechaActualizacion = :fechaActualizacion WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $categoria, PDO::PARAM_STR);
            $stmt->bindParam(':valor_BD', $activo_categoria, PDO::PARAM_INT);
            $stmt->bindParam(':fechaActualizacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->bindParam(':id', $categoriaID, PDO::PARAM_INT);
            $stmt->execute();
  
            $tipo_error = "";
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          
          // Actualiza la marca correspondiente al producto
          $tipo_error = "[Error BD] - Marcas (Select): "; // En caso de que sea error
  
          $sql = "SELECT id FROM __marcas WHERE nombre = :nombre GROUP BY id";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':nombre', $marca, PDO::PARAM_STR);
          $stmt->execute();
          $datos_marca = $stmt->fetch(PDO::FETCH_ASSOC);
          $activo_marca = 1;
          $fecha_consulta = date("Y-m-d H:i:s");
  
          if($datos_marca === false){
            $tipo_error = "[Error BD] - Marcas (Insert): ";
  
            $sql = "INSERT INTO __marcas(nombre, valor_BD, fechaCreacion) VALUES(:nombre, :valor_BD, :fechaCreacion)";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $marca, PDO::PARAM_STR);
            $stmt->bindParam(':valor_BD', $activo_marca, PDO::PARAM_INT);
            $stmt->bindParam(':fechaCreacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->execute();
  
            $tipo_error = "";
            $marcaID = (int) $Conn_mxcomp->pdo->lastInsertId();
          }else{
            $tipo_error = "[Error BD] - Marcas (Update): ";
            $marcaID = (int) $datos_marca['id'];
  
            $sql = "UPDATE __marcas SET nombre = :nombre, valor_BD = :valor_BD, fechaActualizacion = :fechaActualizacion WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $marca, PDO::PARAM_STR);
            $stmt->bindParam(':valor_BD', $activo_marca, PDO::PARAM_INT);
            $stmt->bindParam(':fechaActualizacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->bindParam(':id', $marcaID, PDO::PARAM_INT);
            $stmt->execute();
  
            $tipo_error = "";
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          
          // Actualiza la subcategoria de tipo correspondiente al producto
          $tipo_error = "[Error BD] - Subcategorias de tipo (Select): "; // En caso de que sea error
  
          $sql = "SELECT id FROM __subcat_tipos WHERE nombre = :nombre GROUP BY id";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':nombre', $subcat_tipo, PDO::PARAM_STR);
          $stmt->execute();
          $datos_subcat_tipo = $stmt->fetch(PDO::FETCH_ASSOC);
          $fecha_consulta = date("Y-m-d H:i:s");
  
          if($datos_subcat_tipo === false){
            $tipo_error = "[Error BD] - Subcategorias de tipo (Insert): ";
  
            $sql = "INSERT INTO __subcat_tipos(nombre, fechaCreacion) VALUES(:nombre, :fechaCreacion)";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $subcat_tipo, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCreacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->execute();
  
            $tipo_error = "";
            $subcat_tipoID = (int) $Conn_mxcomp->pdo->lastInsertId();
          }else{
            $tipo_error = "[Error BD] - Subcategorias de tipo (Update): ";
            $subcat_tipoID = (int) $datos_subcat_tipo['id'];
  
            $sql = "UPDATE __subcat_tipos SET nombre = :nombre, fechaActualizacion = :fechaActualizacion WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $subcat_tipo, PDO::PARAM_STR);
            $stmt->bindParam(':fechaActualizacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->bindParam(':id', $subcat_tipoID, PDO::PARAM_INT);
            $stmt->execute();
  
            $tipo_error = "";
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          // REVISAR SI SE PERMITE EL ENVIO GRATIS AL PRODUCTO
          $envioGratisPermitido = '1';
          
          foreach($array_subcategorias_sinEnvioGratis as $datos_subcategoria_tipo){
            if((int) $subcat_tipoID === (int) $datos_subcategoria_tipo['idSubcatTipo']){
              if(is_null($datos_subcategoria_tipo['pesoMayor']) || $datos_subcategoria_tipo['pesoMayor'] === ""){
                $envioGratisPermitido = '0';
                break;
              }else{
                if((float) $peso > (float) $datos_subcategoria_tipo['pesoMayor']){
                  $envioGratisPermitido = '0';
                  break;
                }else{
                  $envioGratisPermitido = '1';
                }
              }
            }else{
              $envioGratisPermitido = '1';
            }
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          
          // Actualiza la subcategoria especifica correspondiente al producto
          $tipo_error = "[Error BD] - Subcategorias especificas (Select): "; // En caso de que sea error
  
          $sql = "SELECT id FROM __subcat_especificas WHERE nombre = :nombre GROUP BY id";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':nombre', $subcat_especifica, PDO::PARAM_STR);
          $stmt->execute();
          $datos_subcat_especifica = $stmt->fetch(PDO::FETCH_ASSOC);
          $fecha_consulta = date("Y-m-d H:i:s");
  
          if($datos_subcat_especifica === false){
            $tipo_error = "[Error BD] - Subcategorias especificas (Insert): ";
  
            $sql = "INSERT INTO __subcat_especificas(nombre, fechaCreacion) VALUES(:nombre, :fechaCreacion)";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $subcat_especifica, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCreacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->execute();
  
            $tipo_error = "";
            $subcat_especificaID = (int) $Conn_mxcomp->pdo->lastInsertId();
          }else{
            $tipo_error = "[Error BD] - Subcategorias especificas (Update): ";
            $subcat_especificaID = (int) $datos_subcat_especifica['id'];
  
            $sql = "UPDATE __subcat_especificas SET nombre = :nombre, fechaActualizacion = :fechaActualizacion WHERE id = :id";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':nombre', $subcat_especifica, PDO::PARAM_STR);
            $stmt->bindParam(':fechaActualizacion', $fecha_consulta, PDO::PARAM_STR);
            $stmt->bindParam(':id', $subcat_especificaID, PDO::PARAM_INT);
            $stmt->execute();
  
            $tipo_error = "";
          }
          
          //////////////////////////////////////////////////////////////////////////////////////////////////////
          
          //Se hara la actualizacion del producto
          $fechaConsultaDatos = date("Y-m-d H:i:s");
          $tipo_error = "[Error BD] - Productos (Select): "; // En caso de que sea error
  
          $sql = "SELECT COUNT(id) AS conteo, id, tieneImagen, noModificarCampos FROM __productos WHERE skuProveedor = :skuProveedor";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
          $stmt->execute();
          $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
          $producto_existe = (int) $datos_producto['conteo'];
          $productoID = (int) $datos_producto['id'];
          $tieneImagen_BD = (int) $datos_producto['tieneImagen'];
          $noModificarCampos = $datos_producto['noModificarCampos'];
  
          if($producto_existe > 0){
            if($noModificarCampos === "1"){
              $tipo_error = "[Error BD] - Productos (Update 1): ";
              
              $sql = "UPDATE __productos SET skuProveedor = :skuProveedor, categoriaID = :categoriaID, marcaID = :marcaID, nombreMarca = :nombreMarca, subcat_tipoID = :subcat_tipoID, subcat_especificaID = :subcat_especificaID, peso = :peso, alto = :alto, largo = :largo, ancho = :ancho, precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, existenciaTotal = :existenciaTotal, almacenes = :almacenes, activo = :activo, envioGratisPermitido = :envioGratisPermitido, fechaActualizacion = :fechaActualizacion WHERE skuProveedor = :skuProveedor";
              $stmt = $Conn_mxcomp->pdo->prepare($sql);
              $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':categoriaID', $categoriaID, PDO::PARAM_INT);
              $stmt->bindParam(':marcaID', $marcaID, PDO::PARAM_INT);
              $stmt->bindParam(':nombreMarca', $marca, PDO::PARAM_STR);
              $stmt->bindParam(':subcat_tipoID', $subcat_tipoID, PDO::PARAM_INT);
              $stmt->bindParam(':subcat_especificaID', $subcat_especificaID, PDO::PARAM_INT);
              $stmt->bindParam(':peso', $peso, PDO::PARAM_STR);
              $stmt->bindParam(':alto', $alto, PDO::PARAM_STR);
              $stmt->bindParam(':largo', $largo, PDO::PARAM_STR);
              $stmt->bindParam(':ancho', $ancho, PDO::PARAM_STR);
              $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
              $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
              $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
              $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
              $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
              $stmt->bindParam(':envioGratisPermitido', $envioGratisPermitido, PDO::PARAM_STR);
              $stmt->bindParam(':fechaActualizacion', $fechaConsultaDatos, PDO::PARAM_STR);
              $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
              $stmt->execute();
            }else{
              if($tieneImagen_BD === 0){
                $tipo_error = "[Error BD] - Productos (Update 2): ";
                
                $sql = "UPDATE __productos SET skuProveedor = :skuProveedor, skuFabricante = :skuFabricante, descripcion = :descripcion, descripcionURL = :descripcionURL, categoriaID = :categoriaID, marcaID = :marcaID, nombreMarca = :nombreMarca, subcat_tipoID = :subcat_tipoID, subcat_especificaID = :subcat_especificaID, peso = :peso, alto = :alto, largo = :largo, ancho = :ancho, precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, existenciaTotal = :existenciaTotal, almacenes = :almacenes, activo = :activo, tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, nombreImagen = :nombreImagen, versionImagen = :versionImagen, cantidadImagenes = :cantidadImagenes, envioGratisPermitido = :envioGratisPermitido, fechaActualizacion = :fechaActualizacion WHERE skuProveedor = :skuProveedor";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':skuFabricante', $skuFabricante, PDO::PARAM_STR);
                $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
                $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
                $stmt->bindParam(':categoriaID', $categoriaID, PDO::PARAM_INT);
                $stmt->bindParam(':marcaID', $marcaID, PDO::PARAM_INT);
                $stmt->bindParam(':nombreMarca', $marca, PDO::PARAM_STR);
                $stmt->bindParam(':subcat_tipoID', $subcat_tipoID, PDO::PARAM_INT);
                $stmt->bindParam(':subcat_especificaID', $subcat_especificaID, PDO::PARAM_INT);
                $stmt->bindParam(':peso', $peso, PDO::PARAM_STR);
                $stmt->bindParam(':alto', $alto, PDO::PARAM_STR);
                $stmt->bindParam(':largo', $largo, PDO::PARAM_STR);
                $stmt->bindParam(':ancho', $ancho, PDO::PARAM_STR);
                $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
                $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
                $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
                $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
                $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
                $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
                $stmt->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
                $stmt->bindParam(':envioGratisPermitido', $envioGratisPermitido, PDO::PARAM_STR);
                $stmt->bindParam(':fechaActualizacion', $fechaConsultaDatos, PDO::PARAM_STR);
                $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
                $stmt->execute();
              }else{
                $tipo_error = "[Error BD] - Productos (Update 3): ";
                
                $sql = "UPDATE __productos SET skuProveedor = :skuProveedor, skuFabricante = :skuFabricante, descripcion = :descripcion, descripcionURL = :descripcionURL, categoriaID = :categoriaID, marcaID = :marcaID, nombreMarca = :nombreMarca, subcat_tipoID = :subcat_tipoID, subcat_especificaID = :subcat_especificaID, peso = :peso, alto = :alto, largo = :largo, ancho = :ancho, precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, existenciaTotal = :existenciaTotal, almacenes = :almacenes, activo = :activo, envioGratisPermitido = :envioGratisPermitido, fechaActualizacion = :fechaActualizacion WHERE skuProveedor = :skuProveedor";
                $stmt = $Conn_mxcomp->pdo->prepare($sql);
                $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':skuFabricante', $skuFabricante, PDO::PARAM_STR);
                $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
                $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
                $stmt->bindParam(':categoriaID', $categoriaID, PDO::PARAM_INT);
                $stmt->bindParam(':marcaID', $marcaID, PDO::PARAM_INT);
                $stmt->bindParam(':nombreMarca', $marca, PDO::PARAM_STR);
                $stmt->bindParam(':subcat_tipoID', $subcat_tipoID, PDO::PARAM_INT);
                $stmt->bindParam(':subcat_especificaID', $subcat_especificaID, PDO::PARAM_INT);
                $stmt->bindParam(':peso', $peso, PDO::PARAM_STR);
                $stmt->bindParam(':alto', $alto, PDO::PARAM_STR);
                $stmt->bindParam(':largo', $largo, PDO::PARAM_STR);
                $stmt->bindParam(':ancho', $ancho, PDO::PARAM_STR);
                $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
                $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
                $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
                $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
                $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
                $stmt->bindParam(':envioGratisPermitido', $envioGratisPermitido, PDO::PARAM_STR);
                $stmt->bindParam(':fechaActualizacion', $fechaConsultaDatos, PDO::PARAM_STR);
                $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
                $stmt->execute();
              }
            }
  
            $tipo_error = "";
            $productosActualizados++;
          }else{
            // SE GENERA EL CODIGO DEL PRODUCTO NUEVO
            if($productosInsertados > 0){
              $codigoProducto = $metodosProducto->generarCodigo_unionInfo('2', $codigoProveedor, $codigoProducto);
            }
  
            $tipo_error = "[Error BD] - Productos (Insert): ";
  
            $sql = "INSERT INTO __productos(codigoProducto, skuProveedor, skuFabricante, descripcion, descripcionURL, categoriaID, marcaID, nombreMarca, subcat_tipoID, subcat_especificaID, peso, alto, largo, ancho, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, existenciaTotal, almacenes, codigoProveedor, nombreProveedor, activo, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, cantidadImagenes, envioGratisPermitido, fechaCreacion) 
            VALUES(:codigoProducto, :skuProveedor, :skuFabricante, :descripcion, :descripcionURL, :categoriaID, :marcaID, :nombreMarca, :subcat_tipoID, :subcat_especificaID, :peso, :alto, :largo, :ancho, :precioProveedor, :monedaProveedor, :precioMXcomp, :monedaMXcomp, :existenciaTotal, :almacenes, :codigoProveedor, :nombreProveedor, :activo, :tieneImagen, :numeroUbicacionImagen, :nombreImagen, :versionImagen, :cantidadImagenes, :envioGratisPermitido, :fechaCreacion)";
            $stmt = $Conn_mxcomp->pdo->prepare($sql);
            $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
            $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':skuFabricante', $skuFabricante, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':categoriaID', $categoriaID, PDO::PARAM_INT);
            $stmt->bindParam(':marcaID', $marcaID, PDO::PARAM_INT);
            $stmt->bindParam(':nombreMarca', $marca, PDO::PARAM_STR);
            $stmt->bindParam(':subcat_tipoID', $subcat_tipoID, PDO::PARAM_INT);
            $stmt->bindParam(':subcat_especificaID', $subcat_especificaID, PDO::PARAM_INT);
            $stmt->bindParam(':peso', $peso, PDO::PARAM_STR);
            $stmt->bindParam(':alto', $alto, PDO::PARAM_STR);
            $stmt->bindParam(':largo', $largo, PDO::PARAM_STR);
            $stmt->bindParam(':ancho', $ancho, PDO::PARAM_STR);
            $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
            $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
            $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
            $stmt->bindParam(':codigoProveedor', $codigoProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
            $stmt->bindParam(':activo', $activo, PDO::PARAM_INT);
            $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
            $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen, PDO::PARAM_STR);
            $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
            $stmt->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
            $stmt->bindParam(':envioGratisPermitido', $envioGratisPermitido, PDO::PARAM_STR);
            $stmt->bindParam(':fechaCreacion', $fechaConsultaDatos, PDO::PARAM_STR);
            $stmt->execute();
  
            $tipo_error = "";
            $productosInsertados++;
          }
        }

        $proceso_correcto = true;
        $fechaScript_final = date("Y-m-d H:i:s");
        $Conn_mxcomp->pdo->commit();
  
        $stmt = null;
      }catch(PDOException $error){
        $Conn_mxcomp->pdo->rollBack();
        $estado_proceso = "Error";
        $mensaje_error = $tipo_error . $error->getMessage();
        echo $mensaje_error;
        $proceso_correcto = false;
      }
    }

    // MODIFICA LAS EXISTENCIAS DE LOS PRODUCTOS SI ES NECESARIO, LAS CATEGORIAS Y MARCAS ACTIVAS/INACTIVAS
    if($proceso_correcto){
      $array_SKUS = array_column($array_productos, 'skuProveedor');

      try{
        $Conn_mxcomp->pdo->beginTransaction();
        
        // MODIFICA LOS PRODUCTOS QUE PCH NO MANDO, PERO AUN SIGUEN REGISTRADOS EN MXCOMP
        $sql = "SELECT id, skuProveedor FROM __productos WHERE nombreProveedor = 'PCH MAYOREO'";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->execute();

        while($datos_producto = $stmt->fetch(PDO::FETCH_ASSOC)){
          $id = (int) trim($datos_producto['id']);
          $skuProveedor = (string) trim($datos_producto['skuProveedor']);

          if(!in_array($skuProveedor, $array_SKUS)){
            $sql = "UPDATE __productos SET existenciaTotal = 0, almacenes = NULL WHERE id = :id AND skuProveedor = :skuProveedor";
            $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
            $stmt_temporal->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt_temporal->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
            $stmt_temporal->execute();
          }
        }
        
        // MODIFICA LAS CATEGORIAS QUE AUN SIGUEN ACTIVAS
        $sql = "SELECT id, valor_BD FROM __categorias";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->execute();

        while($datos_categoria = $stmt->fetch(PDO::FETCH_ASSOC)){
          $id = (int) trim($datos_categoria['id']);
          $valor_BD = $datos_categoria['valor_BD'];
          
          if(is_null($valor_BD)){
            $valor_BD = 0;
          }else{
            $valor_BD = 1;
          }
          
          $sql = "UPDATE __categorias SET activo = :activo, valor_BD = NULL WHERE id = :id";
          $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
          $stmt_temporal->bindParam(':activo', $valor_BD, PDO::PARAM_INT);
          $stmt_temporal->bindParam(':id', $id, PDO::PARAM_INT);
          $stmt_temporal->execute();
        }
        
        // MODIFICA LAS MARCAS QUE AUN SIGUEN ACTIVAS
        $sql = "SELECT id, valor_BD FROM __marcas";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->execute();

        while($datos_marca = $stmt->fetch(PDO::FETCH_ASSOC)){
          $id = (int) trim($datos_marca['id']);
          $valor_BD = $datos_marca['valor_BD'];
          
          if(is_null($valor_BD)){
            $valor_BD = 0;
          }else{
            $valor_BD = 1;
          }
          
          $sql = "UPDATE __marcas SET activo = :activo, valor_BD = NULL WHERE id = :id";
          $stmt_temporal = $Conn_mxcomp->pdo->prepare($sql);
          $stmt_temporal->bindParam(':activo', $valor_BD, PDO::PARAM_INT);
          $stmt_temporal->bindParam(':id', $id, PDO::PARAM_INT);
          $stmt_temporal->execute();
        }

        $estado_proceso = "Exito";
        echo $estado_proceso;
        $mensaje_error = NULL;
        $stmt_temporal = null;
        $stmt = null;
        $Conn_mxcomp->pdo->commit();
      }catch(PDOException $error){
        $Conn_mxcomp->pdo->rollBack();
        $estado_proceso = "Error";
        $mensaje_error = "[Error BD] - No se modificaron los productos desactualizados, las categorias y/o marcas activas: " . $error->getMessage();
        echo $mensaje_error;
      }
    }else{
      $productosActualizados = 0;
      $productosInsertados = 0;
      $fechaScript_inicio = "";
      $fechaScript_final = "";
    }
  }else{
    $estado_proceso = "Error";
    $mensaje_error = "[Error PCH] - PCH no mandó la lista de los productos, se encuentra vacía.";
    echo $mensaje_error;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//GUARDA EL ESTADO DE LA EJECUCION - ADMIN MXCOMP
try{
  $fechaCreacionBitacora = date("Y-m-d H:i:s");
  $sql = "INSERT INTO __bitacora_actualizacion_productos(estadoProceso, mensajeError, productosActualizados, productosInsertados, fechaInicioEjecucion, fechaFinalEjecucion, fechaCreacion) VALUES (:estadoProceso, :mensajeError, :productosActualizados, :productosInsertados, :fechaInicioEjecucion, :fechaFinalEjecucion, :fechaCreacion);";
  $stmt_admin = $Conn_admin->pdo->prepare($sql);
  $stmt_admin->bindParam(':estadoProceso', $estado_proceso, PDO::PARAM_STR);
  $stmt_admin->bindParam(':mensajeError', $mensaje_error, PDO::PARAM_STR);
  $stmt_admin->bindParam(':productosActualizados', $productosActualizados, PDO::PARAM_INT);
  $stmt_admin->bindParam(':productosInsertados', $productosInsertados, PDO::PARAM_INT);
  $stmt_admin->bindParam(':fechaInicioEjecucion', $fechaScript_inicio, PDO::PARAM_STR);
  $stmt_admin->bindParam(':fechaFinalEjecucion', $fechaScript_final, PDO::PARAM_STR);
  $stmt_admin->bindParam(':fechaCreacion', $fechaCreacionBitacora, PDO::PARAM_STR);
  $stmt_admin->execute();

  $stmt_admin = null;
}catch(PDOException $error){
  echo $error->getMessage();
}
?>