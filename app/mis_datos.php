<?php
session_start();
if (!isset($_SESSION['__id__'])) {
  echo '<script> window.location = "/"; </script>';
}

require_once 'funciones/fecha_hora_formatos.php';
require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

require_once 'clases/usuario/metodos_usuario.php';
require_once 'clases/direcciones/metodos_direcciones.php';

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if (isset($_SESSION['__producto_unidades__'])) {
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if (isset($_SESSION['__array_productos__'])) {
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if (isset($_SESSION['__contador_recarga__'])) {
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Mis datos</title>
<?php include 'templates/head.php';?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 6;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php';?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Mis datos</h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
<?php
if(isset($_SESSION['__id__'])){
  $idUsuario = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));

  if(validar_campo_numerico($idUsuario)){
    $idUsuario = (int) $idUsuario;
    $error_consulta = false;

    $Conn_mxcomp = new Conexion_mxcomp();

    try{
      $sql = "SELECT COUNT(id) AS conteo, nombreS, apellidoPaterno, apellidoMaterno, correo, sexo, fechaNacimiento, noTelefonico1, noTelefonico2, noTelefonico3 FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $Conn_mxcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];

      if($usuario_existe === 1){
        //ENCRIPTAMOS EL CODIGO DE USUARIO
        $codigoUsuario_encriptado = encriptar($codigoUsuario);

        $nombreS = desencriptar_con_clave($datos_usuario['nombreS'], $codigoUsuario_encriptado);
        $apellidoPaterno = desencriptar_con_clave($datos_usuario['apellidoPaterno'], $codigoUsuario_encriptado);
        $apellidoMaterno = desencriptar_con_clave($datos_usuario['apellidoMaterno'], $codigoUsuario_encriptado);
        $correo = desencriptar($datos_usuario['correo']);
        $fechaNacimiento = desencriptar_con_clave($datos_usuario['fechaNacimiento'], $codigoUsuario_encriptado);
        $noTelefonico1 = desencriptar_con_clave($datos_usuario['noTelefonico1'], $codigoUsuario_encriptado);

        $noTelefonico2 = is_null($datos_usuario['noTelefonico2']) ? '' : desencriptar_con_clave($datos_usuario['noTelefonico2'], $codigoUsuario_encriptado);

        $noTelefonico3 = is_null($datos_usuario['noTelefonico3']) ? '' : desencriptar_con_clave($datos_usuario['noTelefonico3'], $codigoUsuario_encriptado);

        $numeros_telefonicos = $noTelefonico1;

        if($noTelefonico2 !== ""){
          $numeros_telefonicos = $numeros_telefonicos . ", " . $noTelefonico2;
        }

        if($noTelefonico3 !== ""){
          $numeros_telefonicos = $numeros_telefonicos . ", " . $noTelefonico3;
        }
?>
          <div class="p-columnas">
            <div class="p-columna p-dos_columnas">
              <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Datos generales</h3>
              <p class="p-text_p">
                <span><b>Código de usuario:</b></span><span> <?php echo $codigoUsuario; ?></span>
                <br>
                <span><b>Nombre completo:</b></span><span> <?php echo $nombreS . " " . $apellidoPaterno . " " . $apellidoMaterno; ?></span>
                <br>
                <span><b>Correo:</b></span> <span> <?php echo $correo; ?></span>
                <br>
                <span><b>Sexo:</b></span> <span> <?php echo trim($datos_usuario['sexo']); ?></span>
                <br>
                <span><b>Fecha de nacimiento:</b></span> <span> <?php echo fecha($fechaNacimiento); ?></span>
                <br>
                <span><b>No. telefónico(s):</b></span> <span> <?php echo $numeros_telefonicos; ?></span>
              </p>

              <div class="p-cuenta-contenedor_botones">
                <a href="mis-datos/cambiar-password" class="p-button_inverso p-button_success_inverso">
                  <span>
                    <i class="fas fa-key"></i>
                  </span>
                  <span><b>Cambiar contraseña</b></span>
                </a>
              </div>
            </div>
            <div class="p-columna">
              <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Domicilio particular</h3>
<?php
        try{
          $sql = "SELECT COUNT(id) AS conteo, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, nombreEstado, entreCalle1, entreCalle2, referenciasAdicionales FROM __direcciones WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'particular'";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datos_domicicilioParticular = $stmt->fetch(PDO::FETCH_ASSOC);
          $domicilioParticular_existe = (int) $datos_domicicilioParticular['conteo'];

          if($domicilioParticular_existe === 1){
            $tipoVialidad = trim($datos_domicicilioParticular['tipoVialidad']);
            $nombreVialidad = desencriptar_con_clave($datos_domicicilioParticular['nombreVialidad'], $codigoUsuario_encriptado);
            $noExterior = desencriptar_con_clave($datos_domicicilioParticular['noExterior'], $codigoUsuario_encriptado);
            $noInterior = desencriptar_con_clave($datos_domicicilioParticular['noInterior'], $codigoUsuario_encriptado);
            $codigoPostal = desencriptar_con_clave($datos_domicicilioParticular['codigoPostal'], $codigoUsuario_encriptado);
            $colonia = desencriptar_con_clave($datos_domicicilioParticular['colonia'], $codigoUsuario_encriptado);
            $ciudadMunicipio = desencriptar_con_clave($datos_domicicilioParticular['ciudadMunicipio'], $codigoUsuario_encriptado);
            $nombreEstado = desencriptar_con_clave($datos_domicicilioParticular['nombreEstado'], $codigoUsuario_encriptado);

            $entreCalle1 = is_null($datos_domicicilioParticular['entreCalle1']) ? '' : desencriptar_con_clave($datos_domicicilioParticular['entreCalle1'], $codigoUsuario_encriptado);

            $entreCalle2 = is_null($datos_domicicilioParticular['entreCalle2']) ? '' : desencriptar_con_clave($datos_domicicilioParticular['entreCalle2'], $codigoUsuario_encriptado);

            $referenciasAdicionales = is_null($datos_domicicilioParticular['referenciasAdicionales']) ? '' : desencriptar_con_clave($datos_domicicilioParticular['referenciasAdicionales'], $codigoUsuario_encriptado);
?>
              <p class="p-text_p">
                <span><?php echo $tipoVialidad . " " . $nombreVialidad . " No. Ext. " . $noExterior . ", No. Int. " . $noInterior . ", C.P. " . $codigoPostal . ", " . $colonia . ", " . $ciudadMunicipio . ", " . $nombreEstado; ?></span>
                <?php $datosAdicionales = "";
            $datosAdicionales .= $entreCalle1 === "" && $entreCalle2 === "" ? '' : '<br><span><b>Entre calles:</b> ' . $entreCalle1 . ' y ' . $entreCalle2 . '</span>';

            $datosAdicionales .= $referenciasAdicionales === "" ? '' : ( $entreCalle1 === "" && $entreCalle2 === "" ? '<br><span><b>Referencias adicionales:</b> ' . $referenciasAdicionales . '</span>' : '<br><span><b>Referencias adicionales:</b> ' . $referenciasAdicionales . '</span>' );

            echo $datosAdicionales;?>

              </p>
<?php
          }else{
?>
              <div class="p-notification p-notification_letter_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b>El domicilio particular no existe.</b>
                </span>
              </div>
<?php
          }
        }catch(PDOException $error){
          $error_consulta = true;
          //$error_msg = $error->getMessage();
          $error_msg = "Ocurrió un problema al buscar el domicilio particular del usuario.";
?>
              <div class="p-notification p-notification_letter_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b><?php echo $error_msg; ?></b>
                </span>
              </div>
<?php
        }
?>
            </div>
          </div>
<?php
        if(!$error_consulta){
?>
          <div class="p-buttons p-buttons_right p-cuenta-contenedor_botones">
            <a href="mis-datos/editar-datos-generales" class="p-button p-button_info">
              <span>
                <i class="fas fa-edit"></i>
              </span>
              <span><b>Editar datos y domicilio</b></span>
            </a>
          </div>
<?php
        }
?>
          <div class="p-columnas">
            <div class="p-columna p-dos_columnas">
              <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Domicilio de envío</h3>
<?php
        $direccionUsuario = new Direccion($idUsuario, $codigoUsuario);

        if($direccionUsuario->ver_domicilioEnvio()){
          $domiEnvio_nombreDestinatario = $direccionUsuario->ver_domEnv_nombreDestinatario_desencriptado;
          $domiEnvio_noTelefonico = $direccionUsuario->ver_domEnv_noTelefonico_desencriptado;
          $domiEnvio_direccionCompleta = $direccionUsuario->ver_domEnv_direccionCompleta_desencriptado;
?>
              <p class="p-text_p">
                <span><b><?php echo $domiEnvio_nombreDestinatario; ?></b></span>
                <br>
                <span><?php echo $domiEnvio_direccionCompleta; ?></span>
                <br>
                <span><b>Tel.:</b> <?php echo $domiEnvio_noTelefonico; ?></span>
<?php
          if(!is_null($direccionUsuario->ver_domEnv_entreCalle1) && !is_null($direccionUsuario->ver_domEnv_entreCalle2)){
?>
                <br>
                <span><b>Entre calles:</b> <?php echo $direccionUsuario->ver_domEnv_entreCalle1 . " y " . $direccionUsuario->ver_domEnv_entreCalle2; ?></span>
<?php
          }

          if(!is_null($direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado)){
?>
                <br>
                <span><b>Referencias adicionales:</b> <?php echo $direccionUsuario->ver_domEnv_referenciasAdicionales_desencriptado; ?></span>
<?php
          }

          if(!is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio1) && !is_null($direccionUsuario->ver_domEnv_horaEntregaEnvio2)){
?>
                <br>
                <span><b>Horario de entrega:</b> De <?php echo hora($direccionUsuario->ver_domEnv_horaEntregaEnvio1) . " a " . hora($direccionUsuario->ver_domEnv_horaEntregaEnvio2); ?></span>
<?php
          }else{
?>
                <br>
                <span><b>Horario de entrega:</b> No está establecido</span>
<?php
          }
?>
              </p>
              <div class="p-buttons p-cuenta-contenedor_botones">
                <a href="mis-datos/editar-domicilio-envio" class="p-button p-button_info">
                  <span>
                    <i class="fas fa-edit"></i>
                  </span>
                  <span><b>Editar</b></span>
                </a>
              </div>
<?php
        }else{
          $opcion = (int) $direccionUsuario->ver_domEnv_opcion;
          $msg_error = (string) $direccionUsuario->ver_domEnv_mensaje;

          switch($opcion){
            case 1: // EL DOMICILIO NO EXISTE
?>
              <div class="p-notification p-notification_letter_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b><?php echo $msg_error; ?></b>
                </span>
              </div>
<?php
              break;

            case 2: // ERROR DE CONSULTA
?>
              <div class="p-notification p-notification_letter_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b><?php echo $msg_error; ?></b>
                </span>
              </div>
<?php
              break;
          }
        }
?>
            </div>
            <div class="p-columna">
              <h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Datos de facturación</h3>
<?php
        try{
          $sql = "SELECT COUNT(id) AS conteo, nombreRazonSocial, RFC, usoFactura, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, nombreEstado, entreCalle1, entreCalle2, referenciasAdicionales, correoFactura, noTelefonico FROM __direcciones WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'facturacion'";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datos_facturacion = $stmt->fetch(PDO::FETCH_ASSOC);
          $datosFacturacion_existen = (int) $datos_facturacion['conteo'];

          if($datosFacturacion_existen === 1){
            $nombreRazonSocial = desencriptar_con_clave($datos_facturacion['nombreRazonSocial'], $codigoUsuario_encriptado);
            $RFC = desencriptar_con_clave($datos_facturacion['RFC'], $codigoUsuario_encriptado);
            $usoFactura = desencriptar_con_clave($datos_facturacion['usoFactura'], $codigoUsuario_encriptado);
            $tipoVialidad = trim($datos_facturacion['tipoVialidad']);
            $nombreVialidad = desencriptar_con_clave($datos_facturacion['nombreVialidad'], $codigoUsuario_encriptado);
            $noExterior = desencriptar_con_clave($datos_facturacion['noExterior'], $codigoUsuario_encriptado);
            $noInterior = desencriptar_con_clave($datos_facturacion['noInterior'], $codigoUsuario_encriptado);
            $codigoPostal = desencriptar_con_clave($datos_facturacion['codigoPostal'], $codigoUsuario_encriptado);
            $colonia = desencriptar_con_clave($datos_facturacion['colonia'], $codigoUsuario_encriptado);
            $ciudadMunicipio = desencriptar_con_clave($datos_facturacion['ciudadMunicipio'], $codigoUsuario_encriptado);
            $nombreEstado = desencriptar_con_clave($datos_facturacion['nombreEstado'], $codigoUsuario_encriptado);

            $entreCalle1 = is_null($datos_facturacion['entreCalle1']) ? '' : desencriptar_con_clave($datos_facturacion['entreCalle1'], $codigoUsuario_encriptado);

            $entreCalle2 = is_null($datos_facturacion['entreCalle2']) ? '' : desencriptar_con_clave($datos_facturacion['entreCalle2'], $codigoUsuario_encriptado);

            $referenciasAdicionales = is_null($datos_facturacion['referenciasAdicionales']) ? '' : desencriptar_con_clave($datos_facturacion['referenciasAdicionales'], $codigoUsuario_encriptado);

            $correoFactura = desencriptar_con_clave($datos_facturacion['correoFactura'], $codigoUsuario_encriptado);
            $noTelefonico = desencriptar_con_clave($datos_facturacion['noTelefonico'], $codigoUsuario_encriptado);
?>
              <p class="p-text_p">
                <span><b>Nombre o razón social:</b> <?php echo $nombreRazonSocial; ?></span>
                <br>
                <span><b>RFC:</b> <?php echo $RFC; ?></span>
                <br>
                <span><b>Uso de la factura:</b> <?php echo $usoFactura; ?></span>
                <br>
                <span><b>Dirección:</b> <?php echo $tipoVialidad . " " . $nombreVialidad . " No. Ext. " . $noExterior . ", No. Int. " . $noInterior . ", C.P. " . $codigoPostal . ", " . $colonia . ", " . $ciudadMunicipio . ", " . $nombreEstado; ?></span>
                <?php $datosAdicionales = "";
            $datosAdicionales .= $entreCalle1 === "" && $entreCalle2 === "" ? '' : '<br><span><b>Entre calles:</b> ' . $entreCalle1 . ' y ' . $entreCalle2 . '</span>';

            $datosAdicionales .= $referenciasAdicionales === "" ? '' : ( $entreCalle1 === "" && $entreCalle2 === "" ? '<br><span><b>Referencias adicionales:</b> ' . $referenciasAdicionales . '</span>' : '<br><span><b>Referencias adicionales:</b> ' . $referenciasAdicionales . '</span>' );

            echo $datosAdicionales . '<br>'; ?>

                <span><b>Correo para enviar tu factura:</b> <?php echo $correoFactura; ?></span>
                <br>
                <span><b>No. telefónico:</b> <?php echo $noTelefonico; ?></span>
              </p>
              <div class="p-buttons p-cuenta-contenedor_botones">
                <a href="mis-datos/editar-datos-facturacion" class="p-button p-button_info">
                  <span>
                    <i class="fas fa-edit"></i>
                  </span>
                  <span><b>Editar</b></span>
                </a>
                <a href="mis-datos/eliminar-datos-facturacion" class="p-button p-button_delete">
                  <span>
                    <i class="far fa-trash-alt"></i>
                  </span>
                  <span><b>Eliminar</b></span>
                </a>
              </div>
<?php
          }else{
?>
              <div class="p-notification p-notification_letter_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b>No hay datos de facturación, agrégalos si es necesario.</b>
                </span>
              </div>
              <div class="p-buttons p-cuenta-contenedor_botones">
                <a href="mis-datos/agregar-datos-facturacion" class="p-button p-button_success">
                  <span>
                    <i class="fas fa-plus"></i>
                  </span>
                  <span><b>Agregar datos</b></span>
                </a>
              </div>
<?php
          }
        }catch(PDOException $error){
          //$error_msg = $error->getMessage();
          $error_msg = "Ocurrió un problema al buscar los datos de facturación del usuario.";
?>
              <div class="p-notification p-notification_letter_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span class="p-notification_p">
                  <b><?php echo $error_msg; ?></b>
                </span>
              </div>
<?php
        }
?>
            </div>
          </div>
<?php
//////////////////////////////////////////////////////////////////////////////////////////
        /////////////////// HTML
        /*<h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Tarjetas de crédito o débito</h3>*/

        /*try{
        $sql = "SELECT COUNT(id) FROM __tarjetas_u WHERE BINARY userID = :idUsuario";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->execute();
        $cantidad_tarjetas = (int) $stmt->fetchColumn();

        if($cantidad_tarjetas > 0){
        $sql = "SELECT id, nomTitu, tipoTar, ultimosDigitos_numTar, mesExpiracion, anioExpiracion, tipoVialidad, nomVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, estado FROM __tarjetas_u WHERE BINARY userID = :idUsuario AND codigoUsuario = :codigoUsuario";
        $stmt = $Conn_mxcomp->pdo->prepare($sql);
        $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();

        $contador = 1;*/

        //////////////////////////////////////////////////////////////////////////////////////////
        /////////////////// HTML
        //<div class="p-cuenta-contenedor_tarjetas">

        //while($datos_tarjeta = $stmt->fetch(PDO::FETCH_ASSOC)){

        //////////////////////////////////////////////////////////////////////////////////////////
        /////////////////// HTML
        /*<article class="p-media-contenedor">
        <div class="p-media-div_left">
        <p><?php echo $contador; </p>
        </div>
        <div class="p-media-div_info">
        <p class="p-text_p">
        <span class="p-text_help"><b><?php echo trim($datos_tarjeta['tipoTar']) === "" ? 'Tarjeta' : trim($datos_tarjeta['tipoTar']);  con terminación <?php echo $datos_tarjeta['ultimosDigitos_numTar']; </b></span>
        <br>
        <span>Pertenece a <b><?php echo trim($datos_tarjeta['nomTitu']); </b> y vence el <b><?php echo trim($datos_tarjeta['mesExpiracion'])."/".trim($datos_tarjeta['anioExpiracion']); </b></span>
        <br>
        <span><b>Dirección de la tarjeta:</b></span> <span><?php
        echo trim($datos_tarjeta['tipoVialidad'])." ".trim($datos_tarjeta['nomVialidad'])." No. Ext. ".trim($datos_tarjeta['noExterior']).", No. Int. ".trim($datos_tarjeta['noInterior']); , C.P. <?php echo trim($datos_tarjeta['codigoPostal']); , <?php echo trim($datos_tarjeta['colonia']).", ".trim($datos_tarjeta['ciudadMunicipio']).", ".trim($datos_tarjeta['estado']); </span>
        </p>
        <div class="p-buttons p-cuenta-contenedor_botones">
        <a href="mis-datos/eliminar-tarjeta/<?php echo "1".trim($datos_tarjeta['id']); " class="p-button p-button_delete">
        <span>
        <i class="far fa-trash-alt"></i>
        </span>
        <span><b>Eliminar</b></span>
        </a>
        </div>
        </div>
        </article>*/

        /*$contador++;
        }*/

        //////////////////////////////////////////////////////////////////////////////////////////
        /////////////////// HTML
        //</div>

        //if($cantidad_tarjetas < 3){

        /*<div class="p-buttons p-cuenta-contenedor_botones">
        <a href="mis-datos/agregar-tarjeta" class="p-button p-button_success">
        <span>
        <i class="fas fa-plus"></i>
        </span>
        <span><b>Agregar otra tarjeta</b></span>
        </a>
        </div>*/

        /*}
        }else{*/

        /// HTML
        /*<div class="p-notification p-notification_letter_info">
        <span>
        <i class="fas fa-info-circle"></i>
        </span>
        <span class="p-notification_p">
        <b>No hay tarjetas agregadas.</b>
        </span>
        </div>
        <div class="p-buttons p-cuenta-contenedor_botones">
        <a href="mis-datos/agregar-tarjeta" class="p-button p-button_success">
        <span>
        <i class="fas fa-plus"></i>
        </span>
        <span><b>Agregar tarjeta</b></span>
        </a>
        </div>*/

        /*}
        }catch(PDOException $e){
        //$error_msg = $e->getMessage();
        $error_msg = "Ocurrió un problema al buscar las tarjetas de crédito o débito del usuario.";*/

        //////////////////////////////////////////////////////////////////////////////////////////
        /////////////////// HTML
        //<div class="p-notification p-notification_letter_error">
        //  <span>
        //    <i class="fas fa-times-circle"></i>
        //  </span>
        //  <span class="p-notification_p">
        //    <b><?php //echo $error_msg; </b>
        //  </span>
        //</div>

        //}

        /////////////// ELIMINAR CUENTA
        /*<h3 class="p-cuenta-subtitulo p-cuenta-subtitulo_linea">Eliminar cuenta</h3>
      <p class="p-text p-text_help p-text_help_margin">
      <span>Este proceso tarda 30 días en concretarse, durante ese tiempo puedes volver a iniciar sesión si no deseas que se elimine. Después de ese plazo, toda tu información será eliminada permanentemente.</span>
      </p>

      <div class="p-buttons p-cuenta-contenedor_botones">
      <a class="p-button_inverso p-button_delete_inverso">
      <span class="icon">
      <i class="fas fa-user-times"></i>
      </span>
      <span><b>Eliminar cuenta</b></span>
      </a>
      </div>*/

      }else{
?>
          <div class="p-notification p-notification_letter_error">
            <span>
              <i class="fas fa-times-circle"></i>
            </span>
            <span class="p-notification_p">
              <b>El usuario no existe.</b>
            </span>
          </div>
<?php
      }

      $stmt = null;
    }catch(PDOException $error){
      //$error_msg = $error->getMessage();
      $error_msg = "Ocurrio un problema al revisar la existencia del usuario.";
?>
          <div class="p-notification p-notification_letter_error">
            <span>
              <i class="fas fa-times-circle"></i>
            </span>
            <span class="p-notification_p">
              <b><?php echo $error_msg; ?></b>
            </span>
          </div>
<?php
    }
  }else{
?>
          <div class="p-notification p-notification_letter_error">
            <span>
              <b>Ocurrió un problema al revisar el formato del ID.</b>
            </span>
          </div>
<?php
  }
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php';?>

<?php include 'templates/footer_scripts_jquery.php';?>

<?php include 'templates/footer_scripts_principales.php';?>

  </body>
</html>