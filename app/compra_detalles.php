<?php
session_start();

include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/fecha_hora_formatos.php';
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

$datosGET_existen = false;
$mensaje_error = false;
$sesion_no_iniciada = false;

$Conn_Admin = new Conexion_admin();

if(!isset($_SESSION['__id__'])){
  //echo '<script> window.location = "' . HOST_LINK . 'iniciar-sesion"; </script>';
  $sesion_no_iniciada = true;
}

if($sesion_no_iniciada === false){
  if(isset($_GET['id']) && isset($_GET['o_compra'])){
    $id = trim($_GET['id']);
    $idUsuario = desencriptar(trim($_SESSION['__id__']));
    $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
    $ordenCompra = trim($_GET['o_compra']);

    if(validar_campo_numerico($id) && validar_campo_numerico($idUsuario) && validar_codigoUsuario($codigoUsuario) && validar_campo_numerico($ordenCompra)){
      $id = (int) $id;
      $idUsuario = (int) $idUsuario;
      $codigoUsuario = (string) $codigoUsuario;
      $ordenCompra = (string) $ordenCompra;

      try{
        $sql = "SELECT COUNT(id) AS conteo, id, ordenCompra, totalPagado, metodoEnvio, costoEnvio, direccionCompleta, nombreDestinatario, telefono, entreCalles, referenciasAdicionales, horarioEntrega, necesitaFactura, fechaCompra FROM __ordenes_compra WHERE id = :id AND ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
        $stmt = $Conn_Admin->pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
        $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();
        $datos_ordenCompra = $stmt->fetch(PDO::FETCH_ASSOC);
        $orden_compra_existe = (int) $datos_ordenCompra['conteo'];

        if($orden_compra_existe === 1){
          $DOrden_id_ordenCompra = (int) trim($datos_ordenCompra['id']);
          $DOrden_ordenCompra = (string) trim($datos_ordenCompra['ordenCompra']);
          $DOrden_totalPagado = (float) trim($datos_ordenCompra['totalPagado']);
          $DOrden_metodoEnvio = (string) trim($datos_ordenCompra['metodoEnvio']);
          $DOrden_costoEnvio = trim($datos_ordenCompra['costoEnvio']);
          $DOrden_direccionCompleta = (string) trim($datos_ordenCompra['direccionCompleta']);
          $DOrden_nombreDestinatario = (string) trim($datos_ordenCompra['nombreDestinatario']);
          $DOrden_telefono = (string) trim($datos_ordenCompra['telefono']);
          $DOrden_entreCalles = $datos_ordenCompra['entreCalles']; // PARA REVISAR QUE SEAN NULL O NO
          $DOrden_referenciasAdicionales = $datos_ordenCompra['referenciasAdicionales']; // PARA REVISAR QUE SEAN NULL O NO
          $DOrden_horarioEntrega = $datos_ordenCompra['horarioEntrega']; // PARA REVISAR QUE SEAN NULL O NO
          $DOrden_necesitaFactura = $datos_ordenCompra['necesitaFactura']; // PARA REVISAR QUE SEAN NULL O NO
          $DOrden_fechaCompra = (string) trim($datos_ordenCompra['fechaCompra']);

          $sql = "SELECT COUNT(id) AS conteo, metodoPago, numeroEstadoPago, datosPago, comprobantePago, ubicacionComprobantePago, nombreComprobante, esImagen, oportunidadesComprobante, comprobanteRechazado, clienteCanceloPago, mxcompCanceloCompra, fechaProceso, fechaRegistro FROM __orden_compra_datos_pago WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
          $stmt = $Conn_Admin->pdo->prepare($sql);
          $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt->execute();
          $datos_ordenCompra_datos_pago = $stmt->fetch(PDO::FETCH_ASSOC);
          $datos_ordenCompra_existen = (int) $datos_ordenCompra_datos_pago['conteo'];

          if($datos_ordenCompra_existen === 1){
            $DPago_metodoPago = (string) trim($datos_ordenCompra_datos_pago['metodoPago']);
            $DPago_numeroEstadoPago = (int) trim($datos_ordenCompra_datos_pago['numeroEstadoPago']);
            $DPago_datosPago = (string) trim($datos_ordenCompra_datos_pago['datosPago']);
            $DPago_comprobantePago = (int) trim($datos_ordenCompra_datos_pago['comprobantePago']);
            $DPago_ubicacionComprobantePago = (string) trim($datos_ordenCompra_datos_pago['ubicacionComprobantePago']);
            $DPago_nombreComprobante = (string) trim($datos_ordenCompra_datos_pago['nombreComprobante']);
            $DPago_esImagen = (string) trim($datos_ordenCompra_datos_pago['esImagen']);
            $DPago_oportunidadesComprobante = (int) trim($datos_ordenCompra_datos_pago['oportunidadesComprobante']);
            $DPago_comprobanteRechazado = (int) trim($datos_ordenCompra_datos_pago['comprobanteRechazado']);
            $DPago_clienteCanceloPago = (int) trim($datos_ordenCompra_datos_pago['clienteCanceloPago']);
            $DPago_mxcompCanceloCompra = (int) trim($datos_ordenCompra_datos_pago['mxcompCanceloCompra']);
            $DPago_fechaProceso = (string) trim($datos_ordenCompra_datos_pago['fechaProceso']);
            $DPago_fechaRegistro = (string) trim($datos_ordenCompra_datos_pago['fechaRegistro']);

            $datosGET_existen = true;
          }else{
            $msg_error = 'No se encontraron los datos del pago.';
            $mensaje_error = true;
            $datosGET_existen = false;
          }
        }else{
          $datosGET_existen = false;
        }

        $stmt = null;
      }catch (PDOException $error){
        //$msg_error = 'Error: ' . $error->getMessage();
        $msg_error = 'Ocurrió un problema al buscar los datos de la orden de compra.';
        $mensaje_error = true;
        $datosGET_existen = false;
      }
    }else{
      $datosGET_existen = false;
    }
  }
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php
$texto_title = $sesion_no_iniciada ? 'No has iniciado sesión' : ( $datosGET_existen ? 'Detalles de la compra #' . $DOrden_ordenCompra : 'No se encontró la compra' );
?>
    <title><?php echo $texto_title; ?></title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php';?>

      </div>
      <div class="p-contenido-contenedor">
<?php
if($sesion_no_iniciada){
?>
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-cuenta-fondo">
          <h1 class="p-titulo">No has iniciado sesión</h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-text_p">Para poder ver los detalles de la compra es necesario que inicies sesión.</p>
          <div class="p-buttons p-cuenta-contenedor_botones">
            <a href="iniciar-sesion" class="p-button p-button_info">
              <span><i class="fas fa-user"></i></span>
              <span><b>Iniciar sesión</b></span>
            </a>
          </div>
        </div>
<?php
}else{
  if($datosGET_existen === false){
    $texto_p = $mensaje_error ? $msg_error : 'Los datos para buscar la compra no existen.';
?>
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-cuenta-fondo">
          <h1 class="p-titulo">No se encontró la compra</h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-text_p"><b><?php echo $texto_p; ?></b></p>

          <button class="p-button p-button_delete" onclick="history.go(-1)">
            <span><i class="fas fa-undo"></i></span>
            <span><b>Regresar</b></span>
          </button>
        </div>
<?php
  }else{
    $consulta_correcta = false;

    $array_ordenCompra_productos = [];
    $array_remision_envio = [];

    try{
      // SE OBTIENEN LOS PRODUCTOS DE LA ORDEN DE COMPRA
      $sql = "SELECT nombreAlmacen, codigoProducto, descripcion, descripcionURL, nombreMarca, cantidadComprada, precioTotalMXcomp, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __orden_compra_productos WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
      $stmt = $Conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $array_productos_ordenCompra = $stmt->fetchALL();

      foreach($array_productos_ordenCompra as $datos_productos){
        $nombreAlmacen = (string) $datos_productos['nombreAlmacen'];

        $array = [
          'codigoProducto' => (string) $datos_productos['codigoProducto'],
          'descripcion' => (string) $datos_productos['descripcion'],
          'descripcionURL' => (string) $datos_productos['descripcionURL'],
          'nombreMarca' => (string) $datos_productos['nombreMarca'],
          'cantidadComprada' => (int) $datos_productos['cantidadComprada'],
          'precioTotalMXcomp' => (float) $datos_productos['precioTotalMXcomp'],
          'tieneImagen' => (string) $datos_productos['tieneImagen'],
          'numeroUbicacionImagen' => $datos_productos['numeroUbicacionImagen'],
          'nombreImagen' => (string) $datos_productos['nombreImagen'],
          'versionImagen' => (string) $datos_productos['versionImagen']
        ];

        if(count($array_ordenCompra_productos) === 0 || !array_key_exists($nombreAlmacen, $array_ordenCompra_productos)){
          $array_ordenCompra_productos[$nombreAlmacen][0] = $array;
        }else{
          $array_ordenCompra_productos[$nombreAlmacen][count($array_ordenCompra_productos[$nombreAlmacen])] = $array;
        }

        ///////////////////////////////////////////////////////////////////////////////////////

        if(!array_key_exists($nombreAlmacen, $array_remision_envio)){
          // SE OBTIENE EL ESTADO DE LAS REMISIONES QUE PERTENECE A LA ORDEN DE COMPRA DE ESTE ALMACEN
          $sql = "SELECT numeroEstadoRemision FROM __remisiones WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente AND nombreAlmacen = :nombreAlmacen LIMIT 1";
          $stmt_temporal = $Conn_Admin->pdo->prepare($sql);
          $stmt_temporal->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt_temporal->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
          $stmt_temporal->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt_temporal->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
          $stmt_temporal->execute();
          $numeroEstadoRemision = (int) $stmt_temporal->fetchColumn();

          $array_remision_envio[$nombreAlmacen]['numeroEstadoRemision'] = $numeroEstadoRemision;
          $array_remision_envio[$nombreAlmacen]['datosEnvioEntrega'] = 0;

          // SE OBTIENEN LOS DATOS DE ENVIO EN ESTE ALMACEN, EN CASO DE QUE EXISTAN
          $sql = "SELECT COUNT(id) AS conteo, datosEnvioEntrega, idPaqueteria, nombrePaqueteria, numeroGuia, fechaLlegada, fechaEntrega, fechaCancelacion FROM __datos_factura_envio WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente AND nombreAlmacen = :nombreAlmacen AND datosEnvioEntrega = '1'";
          $stmt_temporal = $Conn_Admin->pdo->prepare($sql);
          $stmt_temporal->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
          $stmt_temporal->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
          $stmt_temporal->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
          $stmt_temporal->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
          $stmt_temporal->execute();
          $datos_factura_envio = $stmt_temporal->fetch(PDO::FETCH_ASSOC);
          $datos_existen = (int) $datos_factura_envio['conteo'];

          if($datos_existen === 1){
            $idPaqueteria = (int) $datos_factura_envio['idPaqueteria'];

            $array_remision_envio[$nombreAlmacen]['datosEnvioEntrega'] = (int) $datos_factura_envio['datosEnvioEntrega'];
            $array_remision_envio[$nombreAlmacen]['nombrePaqueteria'] = (string) $datos_factura_envio['nombrePaqueteria'];
            $array_remision_envio[$nombreAlmacen]['numeroGuia'] = (string) $datos_factura_envio['numeroGuia'];
            $array_remision_envio[$nombreAlmacen]['fechaLlegada'] = (string) $datos_factura_envio['fechaLlegada'];
            $array_remision_envio[$nombreAlmacen]['fechaEntrega'] = (string) $datos_factura_envio['fechaEntrega'];
            $array_remision_envio[$nombreAlmacen]['fechaCancelacion'] = (string) $datos_factura_envio['fechaCancelacion'];

            $sql = "SELECT urlRastreo FROM __paqueterias WHERE id = :idPaqueteria";
            $stmt = $Conn_Admin->pdo->prepare($sql);
            $stmt->bindParam(':idPaqueteria', $idPaqueteria, PDO::PARAM_INT);
            $stmt->execute();
            $array_remision_envio[$nombreAlmacen]['urlRastreo'] = (string) $stmt->fetchColumn();
          }
        }
      }

      $consulta_correcta = true;

      $stmt = null;
    }catch(PDOException $error){
      //$msg_error = 'Error: ' . $error->getMessage();
      $msg_error = 'Ocurrió un problema al buscar las compras.';
      $consulta_correcta = false;
    }

    if(!$consulta_correcta){
?>
        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-notification p-notification_letter_info">
            <b><?php echo $msg_error; ?></b>
          </p>
        </div>
<?php
    }else{
?>
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-cuenta-fondo">
          <h1 class="p-titulo">Compra #<?php echo $DOrden_ordenCompra; ?></h1>
          <h2 class="p-subtitulo p-text_p">Se compró el <b><?php echo fecha_con_hora($DOrden_fechaCompra); ?></b></h2>
          <input type="hidden" value="<?php echo encriptar($codigoUsuario); ?>" id="id-comprar-CU">
          <input type="hidden" value="<?php echo encriptar($DOrden_id_ordenCompra); ?>" id="id-comprar-IOC">
          <input type="hidden" value="<?php echo encriptar($ordenCompra); ?>" id="id-comprar-OC">
          <input type="hidden" value="<?php echo encriptar($DPago_metodoPago); ?>" id="id-comprar-metodoPago">
        </div>

        <div class="p-section-div_formularios p-contenido-hero p-cuenta-pedidoDetalles_section">
          <div class="p-columnas">
            <div class="p-columna p-dos_columnas">
              <div class="p-cuenta-pedidoDetalles_envio_contenedor">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-credit-card"></i>
                  </span>
                  <h4>Método de pago</h4>
                </div>

                <div class="p-loading-general" id="id-loading-metodo_pago" style="padding-bottom: 0;">
                  <div></div>
                </div>

                <div id="id-contenedor-metodo_pago">
                  <p class="p-text_p">
                    <span>El método elegido fue <b><?php echo $DPago_metodoPago; ?></b>.</span>
                  </p>
<?php
      if($DPago_metodoPago === 'Tarjeta'){
?>
                  <p class="p-text_p">
                    <span>Se pagó por medio de una <b><?php echo $DPago_datosPago; ?></b>.</span>
                  </p>
<?php
      }

      switch($DPago_numeroEstadoPago){
        case 1: // PENDIENTE
          $hora_espera_pago = date("Y-m-d H:i:s", strtotime($DPago_fechaRegistro . "+24 hour"));
          $fechaActual = date("Y-m-d H:i:s");

          $ver_campos_esperaAcreditacion = false;

          if($fechaActual > $hora_espera_pago){
            if($DPago_comprobantePago === 0){
?>
                <div class="p-notification p-notification_letter_warning">
                  <span>
                    <i class="fas fa-exclamation-triangle"></i>
                  </span>
                  <span class="p-notification_p"><b>No subiste tu comprobante antes del rango de 24 horas establecidas para acreditar tu pago o no se acreditó el pago antes de tiempo, así que tu compra será cancelada.</b></span>
                </div>
<?php
            }else{
              $ver_campos_esperaAcreditacion = true;
            }
          }else{
            if($DPago_comprobantePago === 0){
              if($DPago_comprobanteRechazado === 1){
                $url_archivo = $DPago_ubicacionComprobantePago . $DPago_nombreComprobante;

                // Revisa si existe el comprobante de pago
                if(file_exists($url_archivo)){
                  unlink($url_archivo);
                }

                try{
                  $Conn_Admin->pdo->beginTransaction();

                  $sql = "UPDATE __orden_compra_datos_pago SET ubicacionComprobantePago = NULL, nombreComprobante = NULL WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
                  $stmt = $Conn_Admin->pdo->prepare($sql);
                  $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
                  $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
                  $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
                  $stmt->execute();

                  $stmt = null;
                  $Conn_Admin->pdo->commit();
                }catch(PDOException $error){
                  $Conn_Admin->pdo->rollBack();
                  //$mensaje = 'Error: ' . $error->getMessage();
                  $mensaje = 'No se pudo eliminar el comprobante de pago';
?>
                  <div class="p-notification p-notification_letter_error">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="p-notification_p"><b><?php echo $mensaje; ?></b></span>
                  </div>
<?php
                }
?>
                  <div class="p-notification p-notification_letter_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="p-notification_p"><b>Tu pago fue rechazado.</b></span>
                  </div>
<?php
              }
?>
                  <input type="hidden" value="<?php echo encriptar($DPago_oportunidadesComprobante); ?>" id="id-comprar-oportunidades_comprobante">
<?php
              if($DPago_oportunidadesComprobante < 3){
                $texto_oportunidades = $DPago_oportunidadesComprobante === 1 ? 'queda ' . $DPago_oportunidadesComprobante . ' oportunidad' : 'quedan ' . $DPago_oportunidadesComprobante . ' oportunidades';
?>
                  <div class="p-notification p-notification_letter_info">
                    <span>
                      <b>Por favor sube de nuevo tu comprobante. Te <?php echo $texto_oportunidades; ?>, después la compra será cancelada.</b>
                    </span>
                  </div>
<?php
              }

              $hora_pago_texto = fecha_con_hora($hora_espera_pago);
?>
                  <div class="p-notification p-notification_letter_warning">
                    <span><b>AVISO:</b> La compra tiene validez <b>hasta el <?php echo $hora_pago_texto; ?> (24 horas después de la compra)</b>, en caso de no subir tu comprobante será cancelada y tendrás que hacer el proceso de compra nuevamente.</span>
                  </div>

                  <form id="id-comprobante-subir_archivo_form" method="POST" enctype="multipart/form-data">
                    <div style="margin-bottom: .5rem;">
                      <label class="p-label">Comprobante de pago:</label>
                      <div class="p-control" style="display: block;">
                        <input type="file" id="id-comprobante-subir_archivo_input_file" accept="image/png, image/jpeg, application/pdf" name="archivo" hidden>
                        <div class="p-cuenta-contenedor_botones">
                          <button type="button" class="p-button p-button_success" id="id-comprobante-subir_archivo_seleccionar_button" title="Seleccionar comprobante de pago" style="margin-bottom: 0;">
                            <span>
                              <i class="fas fa-file"></i>
                            </span>
                            <span><b>Seleccionar archivo</b></span>
                          </button>
                        </div>
                        <label class="p-text p-text-span_compra_detalles" id="id-comprobante-subir_archivo_span">
                          <span>No se eligió el archivo</span>
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                        </label>
                      </div>
                    </div>

                    <div class="p-field_margin_comprobante" id="id-comprobante-subir_archivo_contenedor_subir" style="display: none; margin-top: .5rem;"></div>
                  </form>

                  <p class="mx-etiqueta mx-etiqueta-info">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-spinner"></i>
                    </span>
                    <span><b>Esperando el comprobante de pago</b></span>
                  </p>

                  <hr class="p-cuenta-divisor">

                  <div class="p-buttons_margin_top p-cuenta-contenedor_botones p-margin-bottom_1rem">
                    <a class="p-button p-button_account" id="id-comprar-pdf_datos">
                      <span>
                        <i class="far fa-file-pdf"></i>
                      </span>
                      <span><b>Ver ticket para pagar</b></span>
                    </a>
                  </div>

                  <div class="p-text-align_center" style="margin-top: 2rem;">
                    <div class="p-comprar-logos_horarios">
                      <img src="images/svg_logos/logo_SPEI.svg" alt="Logo SPEI" title="SPEI">
                      <img src="images/svg_logos/logo_Telecomm-Mexico.svg" alt="Logo Telecomm México" title="Telecomm México">
                      <img src="images/svg_logos/logo_OXXO.svg" alt="Logo OXXO" title="OXXO">
                      <img src="images/svg_logos/logo_7-Eleven.svg" alt="Logo 7-Eleven" title="7-Eleven">
                      <img src="images/svg_logos/logo_Circle-K.svg" alt="Logo Circle K" title="Circle K">
                      <img src="images/svg_logos/logo_Extra.svg" alt="Logo Extra" title="Extra">
                      <img src="images/svg_logos/logo_Farmacias-Guadalajara.svg" alt="Logo Farmacias Guadalajara" title="Farmacias Guadalajara">
                      <img src="images/svg_logos/logo_Farmacias-del-ahorro.svg" alt="Logo Farmacias del Ahorro" title="Farmacias del Ahorro">
                    </div>

                    <a class="p-button_inverso p-button_info_inverso p-button_largo" id="id-comprar-modal_formas_pago_abrir">
                      <span><b>Montos y horarios de establecimientos</b></span>
                    </a>
                  </div>
<?php
            }else{
              $ver_campos_esperaAcreditacion = true;
            }
          }

          if($ver_campos_esperaAcreditacion){
?>
                  <div class="p-notification p-notification_letter_info">
                    <span><b>Acabas de subir tu comprobante de pago, se va a revisar y te notificaremos si fue rechazado o aceptado.</b></span>
                  </div>

                  <div class="p-cuenta-contenedor_botones">
                    <button class="p-button p-button_info g-modal-archivo" title="Ver comprobante de pago">
                      <span>
                        <i class="fas fa-eye"></i>
                      </span>
                      <span><b>Ver comprobante de pago</b></span>
                    </button>
                  </div>

                  <p class="mx-etiqueta mx-etiqueta-warning">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-spinner"></i>
                    </span>
                    <span><b>Esperando que se acredite el pago</b></span>
                  </p>

                  <div class="mx-modal" id="id-modal-archivo_contenedor">
                    <div class="mx-modal-fondo g-modal-archivo_cerrar"></div>
                    <div class="mx-modal-contenido" id="id-modal-archivo_contenido">
                      <div class="mx-modal-contenido_archivo">
                        <button class="p-button p-button_square p-button_delete mx-modal-button_close g-modal-archivo_cerrar" title="Cerrar modal">
                          <span>
                            <i class="fas fa-lg fa-times"></i>
                          </span>
                        </button>
<?php
            $url_archivo = HOST_LINK . $DPago_ubicacionComprobantePago . $DPago_nombreComprobante;

            if($DPago_esImagen === '1'){
?>
                        <img src="<?php echo $url_archivo; ?>" alt="Comprobante de pago">
<?php
            }else{
?>
                        <object data="<?php echo $url_archivo; ?>" type="application/pdf"></object>
<?php
            }
?>
                      </div>
                    </div>
                  </div>
<?php
          }
          break;

        case 2: // CANCELADO
          $url_archivo = $DPago_ubicacionComprobantePago . $DPago_nombreComprobante;

          // Revisa si existe el comprobante de pago
          if(file_exists($url_archivo)){
            unlink($url_archivo);
          }

          try{
            $Conn_Admin->pdo->beginTransaction();

            $sql = "UPDATE __orden_compra_datos_pago SET ubicacionComprobantePago = NULL, nombreComprobante = NULL WHERE ordenCompra = :ordenCompra AND idCliente = :idCliente AND codigoCliente = :codigoCliente";
            $stmt = $Conn_Admin->pdo->prepare($sql);
            $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
            $stmt->bindParam(':idCliente', $idUsuario, PDO::PARAM_INT);
            $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
            $stmt->execute();

            $stmt = null;
            $Conn_Admin->pdo->commit();
          }catch(PDOException $error){
            $Conn_Admin->pdo->rollBack();
            //$mensaje = 'Error: ' . $error->getMessage();
            $mensaje = 'No se pudo eliminar el comprobante de pago';
?>
                  <div class="p-notification p-notification_letter_error">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="p-notification_p"><b><?php echo $mensaje; ?></b></span>
                  </div>
<?php
          }
?>
                  <p class="p-text_p">
                    <span>Se canceló el <b><?php echo fecha_con_hora($DPago_fechaProceso); ?></b>.</span>
                  </p>
<?php
          if($DPago_comprobanteRechazado === 1){
?>
                  <div class="p-notification p-notification_letter_warning">
                    <span>
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
                    <span class="p-notification_p"><b>La compra fué cancelada porque tu comprobante de pago fue rechazado.</b></span>
                  </div>
<?php
          }

          if($DPago_clienteCanceloPago === 1){
?>
                  <div class="p-notification p-notification_letter_warning">
                    <span>
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
                    <span class="p-notification_p"><b>Cancelaste tu compra.</b></span>
                  </div>
<?php
          }

          $texto_warning = $DPago_mxcompCanceloCompra === 1 ? 'La compra fué cancelada porque no subiste a tiempo tu comprobante de pago.' : ( $DPago_mxcompCanceloCompra === 2  ? 'La compra fue cancelada por cuestiones bancarias, se hará el reembolso total en un rango de 30 a 60 días dependiendo de tu entidad bancaria.' : '' );

          if($texto_warning !== ''){
?>
                  <div class="p-notification p-notification_letter_warning">
                    <span>
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
                    <span class="p-notification_p"><b><?php echo $texto_warning; ?></b></span>
                  </div>
<?php
          }
?>
                  <div>
                    <p class="mx-etiqueta mx-etiqueta-error">
                      <span class="mx-etiqueta-icon">
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span><b>Pago cancelado</b></span>
                    </p>
                  </div>
<?php
          break;

        case 3: // ACREDITADO
?>
                  <p>
                    <span>Se acreditó el <b><?php echo fecha_con_hora($DPago_fechaProceso); ?></b>.</span>
                  </p>

                  <p class="mx-etiqueta mx-etiqueta-turquesa">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span><b>Pago acreditado</b></span>
                  </p>
<?php
          break;
      }
?>
                </div>
              </div>

              <div class="p-cuenta-pedidoDetalles_envio_contenedor">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-truck"></i>
                  </span>
                  <h4><?php echo $DOrden_metodoEnvio === 'Paqueteria' ? 'Detalles del envío' : 'Detalles de la entrega'; ?></h4>
                </div>
<?php
      foreach($array_remision_envio as $nombreAlmacen=>$datos_remision){
        $numeroEstadoRemision = (int) $datos_remision['numeroEstadoRemision'];
        $datosEnvioEntrega = (int) $datos_remision['datosEnvioEntrega'];

        if($datosEnvioEntrega === 1){
          $nombrePaqueteria = (string) $datos_remision['nombrePaqueteria'];
          $numeroGuia = (string) $datos_remision['numeroGuia'];
          $fechaLlegada = (string) $datos_remision['fechaLlegada'];
          $fechaEntrega = (string) $datos_remision['fechaEntrega'];
          //$fechaCancelacion = (string) $datos_remision['fechaCancelacion'];
          $urlRastreo = (string) $datos_remision['urlRastreo'];
        }

        if($numeroEstadoRemision === 4){
          $fechaCancelacion = $DPago_fechaProceso;
        }
?>
                <h5 class="p-cuenta-pedidoDetalles_envio_subtitulo">Almacén <?php echo ucwords(mb_strtolower($nombreAlmacen)); ?></h5>
                <div class="p-cuenta-pedidoDetalles_envio_contenido">
<?php
        switch($numeroEstadoRemision){
          case 1: // PENDIENTE
            $texto_span = $DOrden_metodoEnvio === 'Paqueteria' ? 'Esperando información del envío' : 'Esperando llegada a la sucursal';
?>
                  <p class="mx-etiqueta mx-etiqueta-info">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-clock"></i>
                    </span>
                    <span><b><?php echo $texto_span; ?></b></span>
                  </p>
<?php
            break;

          case 2: // FACTURADA
            $texto_span = $DOrden_metodoEnvio === 'Paqueteria' ? 'Esperando información del envío' : 'Esperando llegada a la sucursal';
?>
                  <p class="mx-etiqueta mx-etiqueta-info">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-clock"></i>
                    </span>
                    <span><b><?php echo $texto_span; ?></b></span>
                  </p>
<?php
            break;

          case 3: // EN CAMINO / LLEGÓ A LA SUCURSAL
            $texto_llegada = $DOrden_metodoEnvio === 'Paqueteria' ? 'Llega por <b>' . $nombrePaqueteria . '</b>, aprox. el <b>' . fecha($fechaLlegada) . '</b>' : 'Llegó el <b>' . fecha_con_hora($fechaLlegada) . '</b>';
?>
                  <p><span><?php echo $texto_llegada; ?></span></p>
<?php
            if($DOrden_metodoEnvio === 'Paqueteria'){
?>
                  <p>
                    <span><b>Número(s) de guía:</b></span>
                    <span><?php echo $numeroGuia; ?></span>
                  </p>

                  <div class="p-cuenta-contenedor_botones p-buttons_margin_top">
                    <a href="<?php echo $urlRastreo; ?>" class="p-button p-button_info" target="_blank">
                      <span><b>Ir a rastrear paquete(s)</b></span>
                    </a>
                  </div>
<?php
            }

            $texto_icon = $DOrden_metodoEnvio === 'Paqueteria' ? 'fa-shipping-fast' : 'fa-store-alt';
            $texto_span = $DOrden_metodoEnvio === 'Paqueteria' ? 'En camino' : 'Llegó a la sucursal';
?>
                  <p class="mx-etiqueta mx-etiqueta-turquesa">
                    <span class="mx-etiqueta-icon">
                      <i class="fas <?php echo $texto_icon; ?>"></i>
                    </span>
                    <span><b><?php echo $texto_span; ?></b></span>
                  </p>
<?php
            break;

          case 4: // CANCELADA
            $texto_span = $DOrden_metodoEnvio === 'Paqueteria' ? 'Envío cancelado' : 'Pedido cancelado';
?>
                  <p>
                    <span>Se canceló el <b><?php echo fecha_con_hora($fechaCancelacion); ?></b></span>
                  </p>
                  <p class="mx-etiqueta mx-etiqueta-error">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span><b><?php echo $texto_span; ?></b></span>
                  </p>
<?php
            break;

          case 5: // ENVIADA / ENTREGADA
?>
                  <p>
                    <span>Los productos se entregaron el <b><?php echo fecha_con_hora($fechaEntrega); ?></b></span>
                  </p>
                  <p class="mx-etiqueta mx-etiqueta-success">
                    <span class="mx-etiqueta-icon">
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span><b>Productos entregados</b></span>
                  </p>
<?php
              break;
        }
?>
                </div>
<?php
      }
?>
              </div>
<?php
      ////////////////////// DOMICILIO DE ENVIO/SUCURSAL PARA LA ORDEN DE COMPRA //////////////////////

      if($DOrden_metodoEnvio === 'Paqueteria'){
        $codigoUsuario_encriptado = encriptar($codigoUsuario);

        $ordenCompra_direccionCompleta = desencriptar_con_clave($DOrden_direccionCompleta, $codigoUsuario_encriptado);
        $ordenCompra_nombreDestinatario = desencriptar_con_clave($DOrden_nombreDestinatario, $codigoUsuario_encriptado);
        $ordenCompra_telefono = desencriptar_con_clave($DOrden_telefono, $codigoUsuario_encriptado);
?>
              <div class="p-cuenta-pedidoDetalles_envio_contenedor">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-map-marker-alt"></i>
                  </span>
                  <h4>Domicilio de envío</h4>
                </div>
                <p class="p-cuenta-pedidoDetalles_envio_direccion">
                  <span><?php echo $ordenCompra_direccionCompleta; ?></span>
                </p>
                <p>
                  <span><b>Recibe:</b></span>
                  <span><?php echo $ordenCompra_nombreDestinatario; ?></span>
                </p>
                <p>
                  <span><b>Tel.:</b></span>
                  <span><?php echo $ordenCompra_telefono; ?></span>
                </p>
<?php
        $texto_horario = is_null($DOrden_horarioEntrega) ? 'No está establecido' : desencriptar_con_clave($DOrden_horarioEntrega, $codigoUsuario_encriptado);
?>
                <p>
                  <span><b>Horario de entrega:</b></span>
                  <span><?php echo $texto_horario; ?></span>
                </p>
<?php

        if(!is_null($DOrden_entreCalles)){
?>
                <p>
                  <span><b>Entre calles:</b></span>
                  <span><?php echo desencriptar_con_clave($DOrden_entreCalles, $codigoUsuario_encriptado); ?></span>
                </p>
<?php
        }

        if(!is_null($DOrden_referenciasAdicionales)){
?>
                <p>
                  <span><b>Referencias adicionales:</b></span>
                  <span><?php echo desencriptar_con_clave($DOrden_referenciasAdicionales, $codigoUsuario_encriptado); ?></span>
                </p>
<?php
        }
?>
              </div>
<?php
      }
?>
            </div>
            <div class="p-columna p-dos_columnas">
              <div class="p-cuenta-pedidoDetalles_resumen_contenedor" style="margin-bottom: 1.5rem;">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-file-alt"></i>
                  </span>
                  <h4>Resumen de Compra</h4>
                </div>
<?php
      ////////////////////// PRODUCTOS DE LA ORDEN DE COMPRA //////////////////////

      foreach($array_ordenCompra_productos as $nombreAlmacen=>$productos_almacen){
?>
                <h5 class="p-cuenta-pedidoDetalles_envio_subtitulo">Almacén <?php echo ucwords(mb_strtolower($nombreAlmacen)); ?></h5>
                <ul>
<?php
        foreach($productos_almacen as $datos_producto){
          $codigoProducto = (string) $datos_producto['codigoProducto'];
          $descripcion = (string) $datos_producto['descripcion'];
          $descripcionURL = (string) $datos_producto['descripcionURL'];
          $nombreMarca = (string) $datos_producto['nombreMarca'];
          $cantidadComprada = (int) $datos_producto['cantidadComprada'];
          $precioTotalMXcomp = (float) $datos_producto['precioTotalMXcomp'];
          $tieneImagen = (int) $datos_producto['tieneImagen'];
          $numeroUbicacionImagen = $datos_producto['numeroUbicacionImagen']; // PARA REVISAR SI ES NULL
          $nombreImagen = (string) $datos_producto['nombreImagen'];
          $versionImagen = (string) $datos_producto['versionImagen'];

          // SI ES NULL NO TIENE UBICACION LA IMAGEN Y NO EXISTE
          if( (is_null($numeroUbicacionImagen) || $numeroUbicacionImagen === '') && $tieneImagen === 0){
            $ruta_imagen = 'images/no_imagen_disponible.png';
            $alt = 'Imagen no disponible.';
          }else{
            $numeroUbicacionImagen = (int) $numeroUbicacionImagen;
            $alt = $descripcion;

            switch($numeroUbicacionImagen){
              case 1: // UBICACION NUEVA
                $ruta_imagen = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '.jpg';
                break;
            }
          }
?>
                  <li>
                    <div>
                      <picture>
                        <img src="<?php echo $ruta_imagen . '?v=' . $versionImagen; ?>" alt="<?php echo $alt; ?>">
                      </picture>
                      <p>
<?php
          if($codigoProducto === ''){
?>
                        <span style="font-size: 10px; color: #363636; margin-bottom: .5rem;"><b><?php echo $descripcion; ?></b></span>
<?php
          }else{
?>
                        <a href="<?php echo $codigoProducto . '/' . $descripcionURL . '/'; ?>" target="_blank">
                          <span><b><?php echo $descripcion; ?></b></span>
                        </a>
<?php
          }
?>
                        <span><b>Cantidad:</b> <?php echo $cantidadComprada; ?></span>
                      </p>
                    </div>
                    <div>
                      <span><b>$ <?php echo number_format($precioTotalMXcomp, 2); ?></b></span>
                    </div>
                  </li>
<?php
        }
?>
                </ul>
<?php
      }

      ////////////////////// COSTO DE ENVIO PARA ORDEN DE COMPRA //////////////////////
      $texto_costo_envio = $DOrden_costoEnvio === 'Envío gratis' ? $DOrden_costoEnvio : ( $DOrden_costoEnvio ===  '0.00' ? '$ ' . $DOrden_costoEnvio : '$ ' . number_format($DOrden_costoEnvio, 2) );
?>
                <p style="padding-top: 1rem;">
                  <span>Costo total del envío</span>
                  <span><?php echo $texto_costo_envio; ?></span>
                </p>
                <p>
                  <span>Total</span>

                  <input type="hidden" id="id-detalles-total_pago_input" value="<?php echo number_format($DOrden_totalPagado, 2, '.', ''); ?>" autocomplete="off" readonly>
                  <span>$ <?php echo number_format($DOrden_totalPagado, 2); ?></span>
                </p>
              </div>
<?php
      // SI EL PAGO YA ESTÁ ACREDITADO
      $ver_facturacion = $DPago_numeroEstadoPago === 3 ? true : false;

      // SI SE VA A MOSTRAR LA VISTA DE FACTURACION
      if($ver_facturacion){
        // SI NO HA SELECCIONADO NADA
        if(is_null($DOrden_necesitaFactura)){
          $mes_fecha_pago = date('m', strtotime($DPago_fechaProceso));
          $mes_actual = date('m');
  
          if($mes_fecha_pago === $mes_actual){
?>
              <div class="p-cuenta-pedidoDetalles_envio_contenedor">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-file-invoice-dollar"></i>
                  </span>
                  <h4>Facturación</h4>
                </div>

                <div id="id-contenedor-facturar">
                  <p class="p-text p-text_p">¿Necesitas facturar esta compra?</p>

                  <div class="p-buttons p-cuenta-contenedor_botones">
                    <button class="p-button p-button_info g-si-facturar">
                      <span><b>Sí, necesito facturarla</b></span>
                    </button>

                    <button class="p-button_inverso p-button_info_inverso g-no-facturar">
                      <span><b>No, gracias</b></span>
                    </button>
                  </div>
                </div>

                <div class="p-notification_contenedor g-notificacion-no_facturar">
                  <div class="p-notification p-notification_letter_success">
                    <span>
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span class="p-notification_p"><b>Tu respuesta fue recibida, no se preguntará de nuevo para esta compra.</b></span>
                  </div>
                </div>

                <div class="p-notification_contenedor g-notificacion-si_facturar">
                  <div class="p-notification p-notification_letter_success">
                    <span>
                      <i class="fas fa-check-circle"></i>
                    </span>
                    <span class="p-notification_p"><b>Se solicitó la facturación de tu compra. Cuando se tenga lista tu factura, te llegará por el correo que nos facilitaste en el formulario.</b></span>
                  </div>
                </div>

                <div class="p-notification_contenedor g-notificacion-correo_noEnviado" style="margin-top: 1rem;">
                  <div class="p-notification p-notification_letter_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="p-notification_p"><b>La información se guardó correctamente, pero no se pudo enviar el correo que es para nosotros. Vuelve a realizar el proceso.</b></span>
                  </div>
                </div>

                <div class="p-modal-derecho_contenedor" id="id-modal-faturacion">
                  <div class="p-modal-derecho_contenedor_fondo g-modal-faturacion_cerrar"></div>
                  <div class="p-modal-derecho_contenedor_contenido" id="id-modal-faturacion_contenido">
                    <div class="p-loading-general" id="id-loading-facturacion" style="margin-top: 3rem;">
                      <div></div>
                    </div>
                    <div class="p-comprar-modal_cont" id="id-modal-faturacion_contenedor_elementos"></div>
                  </div>
                </div>
              </div>
<?php
          }
        }else{
          $DOrden_necesitaFactura = (int) $DOrden_necesitaFactura;

          $factura_class = $DOrden_necesitaFactura === 0 ? 'p-notification_letter_info' : 'p-notification_letter_success';
          $factura_icon = $DOrden_necesitaFactura === 0 ? 'fa-info-circle' : 'fa-check-circle';
          $factura_texto = $DOrden_necesitaFactura === 0 ? 'Indicaste que no necesitas facturar esta compra.' : 'Tu factura está en proceso o es posible que ya fue enviada al correo que facilitaste.';
?>
              <div class="p-cuenta-pedidoDetalles_envio_contenedor">
                <div class="p-cuenta-pedidoDetalles_contenedor_div_titulo">
                  <span>
                    <i class="fas fa-file-invoice-dollar"></i>
                  </span>
                  <h4>Facturación</h4>
                </div>

                <div class="p-notification <?php echo $factura_class; ?>">
                  <span>
                    <i class="fas <?php echo $factura_icon; ?>"></i>
                  </span>
                  <span class="p-notification_p"><b><?php echo $factura_texto; ?></b></span>
                </div>
              </div>
<?php
        }
      }
?>
            </div>
          </div>
        </div>
<?php
    }
  }
}
?>

      </div>

      <!--MODAL PARA MONTOS Y HORARIOS-->
      <div class="p-modal-derecho_contenedor" id="id-comprar-modal_formas_pago">
        <div class="p-modal-derecho_contenedor_fondo g-comprar-modal_formas_pago_cerrar"></div>
        <div class="p-modal-derecho_contenedor_contenido" id="id-comprar-modal_formas_pago_contenido">
          <h3 class="p-comprar-modal_titulo">
            <span>Montos y horarios para establecimientos</span>
            <a class="p-comprar-modal_titulo_cerrar g-comprar-modal_formas_pago_cerrar" title="Cerrar">
              <span>
                <i class="fas fa-times"></i>
              </span>
            </a>
          </h3>

          <div class="p-comprar-modal_cont_elementos">
<?php
            /*<div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Santander.svg" alt="Logo Santander" title="Santander" style="height: 20px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p">Santander</p>
                  </div>
                </div>
              </div>
            </div>*/
?>
            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_SPEI.svg" alt="Logo SPEI" title="SPEI" style="height: 30px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p">Con SPEI, los pagos pueden ser por cualquier monto y en cualquier momento; sin embargo, en horarios ampliados (entre la hora de apertura del sistema y las 6:00 am) así como en días inhábiles bancarios, solo las instituciones de crédito al igual que las cámaras de compensación, están obligadas a procesar los pagos que reciban a través de canales electrónicos por un monto hasta de $8,000 pesos.</p>
                    <p class="p-text_p">Si utilizas SPEI por <b>Banca en Línea</b>, podrás hacer tus operaciones en día y horario hábil bancario (de 6 AM a 5:30 PM); si es por <b>Banca Móvil o teléfono celular</b>, las transacciones puedes realizarse las 24 horas los 365 días del año.</p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Telecomm-Mexico.svg" alt="Logo Telecomm México" title="Telecomm México" style="height: 30px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 15 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Consultar en establecimiento, depende la ubicación.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_OXXO.svg" alt="Logo OXXO" title="OXXO" style="height: 50px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 10 mil pesos (Por cuenta y por día), 5 mil pesos por transacción.</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 10 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Lunes a Domingo de 8:00 am a 8:00 pm, no importa si es día festivo.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_7-Eleven.svg" alt="Logo 7-Eleven" title="7-Eleven" style="height: 70px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Lunes a Domingo de 8:00 am a 8:00 pm.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Circle-K.svg" alt="Logo Circle K" title="Circle K" style="height: 40px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Lunes a Domingo de 8:00 am a 8:00 pm.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Extra.svg" alt="Logo Extra" title="Extra" style="height: 50px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Lunes a Domingo de 8:00 am a 8:00 pm.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Farmacias-Guadalajara.svg" alt="Logo Farmacias Guadalajara" title="Farmacias Guadalajara" style="height: 80px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Depósitos a tarjeta de débito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Pago de tarjeta de crédito:</b> Máximo 5 mil pesos (Por cuenta y por día).</p>
                    <p class="p-text_p"><b>Horario:</b> Consultar en establecimiento.</p>
                    <p class="p-text_p"><b>No hay montos mínimos en depósitos o pagos.</b></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="p-comprar-modal_notificacion_cuadro">
              <div class="p-comprar-modal_contenedor_columnas">
                <div class="p-columnas">
                  <div class="p-columna p-col_4_12 p-text-align_center">
                    <img src="images/svg_logos/logo_Farmacias-del-ahorro.svg" alt="Logo Farmacias del Ahorro" title="Logo Farmacias del Ahorro" style="height: 50px;">
                  </div>
                  <div class="p-columna p-col_8_12">
                    <p class="p-text_p"><b>Preguntar en establecimiento.</b></p>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php';?>

<?php include 'templates/footer_scripts_jquery.php';?>

<?php include 'templates/footer_scripts_principales.php';?>

<?php include 'templates/scripts_facturacion.php';?>

    <script>
      $(document).ready(function(){
        /*
        * Modal Montos y horarios - Abrir modal
        */
        $(document).on('click', '#id-comprar-modal_formas_pago_abrir', function(){
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
          $('document, #id-comprar-modal_formas_pago').slideToggle('fast');
          $('document, #id-comprar-modal_formas_pago_contenido').toggleClass('p-modal-derecho_contenedor_activo');
        });
        /*
        * Modal Montos y horarios - Abrir modal
        */
        $(document).on('click', '.g-comprar-modal_formas_pago_cerrar', function(){
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
          $('document, #id-comprar-modal_formas_pago').slideToggle('fast');
          $('document, #id-comprar-modal_formas_pago_contenido').toggleClass('p-modal-derecho_contenedor_activo');
        });
        // MODAL ARCHIVOS
        $(document).on('click', '.g-modal-archivo', function(){
          let id = $(this).attr('id');

          $('document, #id-modal-archivo_contenedor').css('display', 'block');
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
        });

        $(document).on('click', '.g-modal-archivo_cerrar', function(){
          let id = $(this).attr('id');

          $('#id-modal-archivo_contenedor').css('display', 'none');
          $('html').toggleClass('p-modal-derecho_window_no_scroll');
        });

        ///////// SUBIR COMPROBANTE DE PAGO
        $(document).on('click', '#id-comprobante-subir_archivo_seleccionar_button', function(){
          $('document, #id-comprobante-subir_archivo_input_file').click();
          $('document, #id-comprobante-subir_contenedor_span_ok').slideUp('fast');
          $('document, #id-comprobante-subir_contenedor_span_error').slideUp('fast');
          $('document, #id-comprobante-subir_contenedor_span_error').html('');
        });
        // SE DETECTA CAMBIO EN INPUT FILE
        $(document).on('change', '#id-comprobante-subir_archivo_input_file', function(){
          let nombre, size, extension, nombre_completo, archivo = $(this)[0].files[0];

          if(archivo){
            $('document, #id-comprobante-subir_archivo_span').html('<span>Cargando</span><span><i class="fas fa-spinner"></i></span>');
            $('document, #id-comprobante-subir_archivo_span').addClass('p-text-span_compra_detalles_ok');

            nombre = archivo.name;
            extension = nombre.split('.').pop();
            extension = extension.toLowerCase();
            size = archivo.size;

            // Si es igual a jpg, jpeg, png o pdf
            if(extension === 'jpg' || extension === 'jpeg' || extension === 'png' || extension === 'pdf'){

              // Si es menor a los 5 MB, 5242880 bytes en binario
              if(size < 5242880){
                nombre_completo = archivo.name;

                if(nombre.length > 20){
                  nombre = nombre.substr(0, 10) + '...' + nombre.substr(-10);
                }

                $('document, #id-comprobante-subir_archivo_span').html('<span title="' + nombre_completo + '">' + nombre + '</span><span><i class="fas fa-check-circle"></i></span>');
                $('document, #id-comprobante-subir_archivo_span').addClass('p-text-span_compra_detalles_ok');

                let data = new FormData();
                data.append('accion', 'mostrar');

                fetch('procesos/comprar/campos_subir_comprobante_elementos.php', {
                  method: 'POST',
                  body: data
                }).then(response => {
                  return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
                }).then(datos => {
                  let respuesta = datos.respuesta, mensaje = datos.mensaje;

                  if(respuesta === '1'){
                    $('document, #id-comprobante-subir_archivo_contenedor_subir').html(mensaje);
                    $('document, #id-comprobante-subir_archivo_contenedor_subir').slideDown('fast');
                  }
                }).catch(error => {
                  console.error('[Error] - ', error);
                });
              }else{
                $('document, #id-comprobante-subir_archivo_span').html('<span>La extensíon del archivo es .' + extension + ', pero supera los 5 MB.</span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-comprobante-subir_archivo_span').removeClass('p-text-span_compra_detalles_ok');
                $('document, #id-comprobante-subir_archivo_contenedor_subir').slideUp('fast');
                $('document, #id-comprobante-subir_archivo_contenedor_subir').html('');
              }
            }else{
              $('document, #id-comprobante-subir_archivo_span').html('<span>La extensión no es válida, sólo se permite jpg/jpeg, png o pdf.</span><span><i class="fas fa-times-circle"></i></span>');
              $('document, #id-comprobante-subir_archivo_span').removeClass('p-text-span_compra_detalles_ok');
              $('document, #id-comprobante-subir_archivo_contenedor_subir').slideUp('fast');
              $('document, #id-comprobante-subir_archivo_contenedor_subir').html('');
            }
          }else{
            $('document, #id-comprobante-subir_archivo_span').html('<span>No se eligió el archivo</span><span><i class="fas fa-times-circle"></i></span>');
            $('document, #id-comprobante-subir_archivo_span').removeClass('p-text-span_compra_detalles_ok');
            $('document, #id-comprobante-subir_archivo_contenedor_subir').slideUp('fast');
            $('document, #id-comprobante-subir_archivo_contenedor_subir').html('');
          }
        });
        // SUBMIT PARA SUBIR COMPROBANTE DE PAGO
        $(document).on('submit', '#id-comprobante-subir_archivo_form', function(e){
          e.preventDefault();

          let archivo = $('document, #id-comprobante-subir_archivo_input_file')[0].files[0], codigo_usuario = $('document, #id-comprar-CU').val(), ID_orden_compra = $('document, #id-comprar-IOC').val(), orden_compra = $('document, #id-comprar-OC').val(), oportunidades_comprobante = $('document, #id-comprar-oportunidades_comprobante').val(), data = new FormData();

          $('document, #id-comprobante-subir_contenedor_loading').slideDown('fast');

          data.append('accion', 'subir');
          data.append('archivo', archivo);
          data.append('codigo_usuario', codigo_usuario);
          data.append('ID_orden_compra', ID_orden_compra);
          data.append('orden_compra', orden_compra);
          data.append('oportunidades_comprobante', oportunidades_comprobante);

          fetch('procesos/comprar/proceso_subir_comprobante.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta, mensaje = datos.mensaje;

            $('document, #id-comprobante-subir_contenedor_loading').slideUp('fast');

            switch(respuesta){
              case '0':
                $('document, #id-comprobante-subir_contenedor_span_error').html(mensaje);
                $('document, #id-comprobante-subir_contenedor_span_error').slideDown('fast');
                break;

              case '1':
                $('document, #id-comprobante-subir_contenedor_span_error').html('');
                $('document, #id-comprobante-subir_contenedor_span_error').slideUp('fast');
                $('document, #id-comprobante-subir_contenedor_span_ok').slideDown('fast');

                setTimeout(function(){
                  $('document, #id-loading-metodo_pago').slideDown('fast');
                  $('document, #id-contenedor-metodo_pago').css('opacity', '0.5');
                }, 2000);

                setTimeout(function(){ mostrar_elementos_metodo_pago(); }, 2800);
                break;
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        });
        /*
        * Abrir PDF - Datos bancarios
        */
        $(document).on('click', '#id-comprar-pdf_datos', function(){
          let orden_compra = $('document, #id-comprar-OC').val(), pago_compra = $('document, #id-detalles-total_pago_input').val(), data = new FormData();

          data.append('accion', 'enviar');
          data.append('orden_compra', orden_compra);
          data.append('pago_compra', pago_compra);

          fetch('procesos/comprar/pdf_datosPago_enviarDatos.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta;

            if(respuesta === '1'){
              window.open('confirmar-compra/mxcomp_datos_pago.pdf', 'Datos bancarios' , 'width=800, height=600, scrollbars=no, toolbar=yes');
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        });
        //////////////////////////////////////////////////////////////////
        /////////// FUNCIONES ///////////
        /*
        * FUNCION PARA MOSTRAR ELEMENTOS DEL METODO DEL PAGO
        */
        function mostrar_elementos_metodo_pago(){
          let orden_compra = $('document, #id-comprar-OC').val(), codigo_usuario = $('document, #id-comprar-CU').val(), data = new FormData();

          data.append('accion', 'mostrar');
          data.append('orden_compra', orden_compra);
          data.append('codigo_usuario', codigo_usuario);

          fetch('procesos/comprar/elementos_metodo_pago.php', {
            method: 'POST',
            body: data
          }).then(response => {
            return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
          }).then(datos => {
            let respuesta = datos.respuesta, mensaje = datos.mensaje;

            $('document, #id-loading-metodo_pago').slideUp('fast');
            $('document, #id-contenedor-metodo_pago').css('opacity', '1');

            if(respuesta === '1'){
              $('document, #id-contenedor-metodo_pago').html(mensaje);
            }
          }).catch(error => {
            console.error('[Error] - ', error);
          });
        }
      });
    </script>
  </body>
</html>