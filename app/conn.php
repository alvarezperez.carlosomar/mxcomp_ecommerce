<?php
require dirname(__DIR__, 1) . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 1));
$dotenv->load();

// MXCOMP - VARIABLES
$BD_mxcomp_host = $_ENV['BD_MXCOMP_HOST'];
$BD_mxcomp_name = $_ENV['BD_MXCOMP_NAME'];
$BD_mxcomp_user = $_ENV['BD_MXCOMP_USER'];
$BD_mxcomp_pass = $_ENV['BD_MXCOMP_PASS'];

// MXCOMP - CONEXION PDO
$conexion = new PDO("mysql:host=$BD_mxcomp_host;dbname=$BD_mxcomp_name;charset=utf8", $BD_mxcomp_user, $BD_mxcomp_pass);
$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

class Conexion_mxcomp{
  private $host;
  private $name;
  private $user;
  private $pass;
  public $pdo = null;
  
  public function __construct(){
    require dirname(__DIR__, 1) . '/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 1));
    $dotenv->load();

    // MXCOMP - VARIABLES
    $this->host = $_ENV['BD_MXCOMP_HOST'];
    $this->name = $_ENV['BD_MXCOMP_NAME'];
    $this->user = $_ENV['BD_MXCOMP_USER'];
    $this->pass = $_ENV['BD_MXCOMP_PASS'];
    
    $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->name;charset=utf8", $this->user, $this->pass);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $this->pdo;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////

// ADMIN MXCOMP - VARIABLES
$BD_mxcomp_admin_host = $_ENV['BD_MXCOMP_ADMIN_HOST'];
$BD_mxcomp_admin_name = $_ENV['BD_MXCOMP_ADMIN_NAME'];
$BD_mxcomp_admin_user = $_ENV['BD_MXCOMP_ADMIN_USER'];
$BD_mxcomp_admin_pass = $_ENV['BD_MXCOMP_ADMIN_PASS'];

// ADMIN MXCOMP - CONEXION PDO
$conexion_admin = new PDO("mysql:host=$BD_mxcomp_admin_host;dbname=$BD_mxcomp_admin_name;charset=utf8", $BD_mxcomp_admin_user, $BD_mxcomp_admin_pass);
$conexion_admin->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

class Conexion_admin{
  private $host;
  private $name;
  private $user;
  private $pass;
  public $pdo = null;
  
  public function __construct(){
    require dirname(__DIR__, 1) . '/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 1));
    $dotenv->load();

    // ADMIN MXCOMP - VARIABLES
    $this->host = $_ENV['BD_MXCOMP_ADMIN_HOST'];
    $this->name = $_ENV['BD_MXCOMP_ADMIN_NAME'];
    $this->user = $_ENV['BD_MXCOMP_ADMIN_USER'];
    $this->pass = $_ENV['BD_MXCOMP_ADMIN_PASS'];
    
    $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->name;charset=utf8", $this->user, $this->pass);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $this->pdo;
  }
}

/////////////////////////////////////////////////////////////////////////////////////////

// SIGA BASE DE DATOS

class Conexion_siga{
  private $host;
  private $name;
  private $user;
  private $pass;
  public $pdo = null;
  
  public function __construct(){
    require dirname(__DIR__, 1) . '/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 1));
    $dotenv->load();

    // ADMIN MXCOMP - VARIABLES
    $this->host = $_ENV['BD_SIGA_HOST'];
    $this->name = $_ENV['BD_SIGA_NAME'];
    $this->user = $_ENV['BD_SIGA_USER'];
    $this->pass = $_ENV['BD_SIGA_PASS'];
    
    $this->pdo = new PDO("mysql:host=$this->host;dbname=$this->name;charset=utf8", $this->user, $this->pass);
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $this->pdo;
  }
}
?>