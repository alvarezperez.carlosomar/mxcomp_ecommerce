<?php
session_start();
include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Atención a clientes</title>
    <meta name="description" content="Se detalla el aviso de privacidad, para dar a conocer la finalidad de la obtención de ciertos datos para poder proveerle los servicios requeridos.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, aviso de privacidad, notificación">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 9;
$op_menu = 3;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-atencionClientes-fondo">
          <h1 class="p-titulo">Atención a clientes</h1>
          <h2 class="p-subtitulo p-text_p">Si estás interesado en alguno de nuestros productos, o si tienes alguna duda o aclaración, por favor comunícate con nosotros a:</h2>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          
          <div class="p-atencionClientes-contenedor_tarjetas">
            <div class="p-atencionClientes-tarjeta">
              <div class="p-atencionClientes-tarjeta_contenido">
                <div class="p-atencionClientes_titulo">
                  <span class="p-atencionClientes-tarjeta_icono">
                    <i class="fas fa-envelope"></i>
                  </span>
                  <h3>Correo electrónico</h3>
                </div>
                
                <a class="p-button p-button_account p-button_largo" href="mailto:atencionaclientes@mxcomp.com.mx" title="Clic para mandar correo">
                  <span>
                    <i class="fas fa-envelope"></i>
                  </span>
                  <span><b>Mandar correo</b></span>
                </a>
              </div>
            </div>
            
            <div class="p-atencionClientes-tarjeta">
              <div class="p-atencionClientes-tarjeta_contenido">
                <div class="p-atencionClientes_titulo">
                  <span class="p-atencionClientes-tarjeta_icono">
                    <i class="fab fa-whatsapp"></i>
                  </span>
                  <h3>WhatsApp</h3>
                </div>
                
                <a class="p-button p-button_success p-button_largo" href="https://api.whatsapp.com/send?phone=5215587965174&amp;text=Hola%20MXcomp,%20requiero%20de%20m%C3%A1s%20informaci%C3%B3n..." target="_blank" title="Clic para mandar WhatsApp">
                  <span>
                    <i class="fab fa-whatsapp"></i>
                  </span>
                  <span><b>Mandar mensaje</b></span>
                </a>
              </div>
            </div>
            
            <div class="p-atencionClientes-tarjeta">
              <div class="p-atencionClientes-tarjeta_contenido">
                <div class="p-atencionClientes_titulo">
                  <span class="p-atencionClientes-tarjeta_icono">
                    <i class="fas fa-phone"></i>
                  </span>
                  <h3>Teléfonos</h3>
                </div>
                
                <div class="p-buttons p-buttons_center p-atencionClientes-contenedor_botones">
                  <a class="p-button p-button_info" href="tel:+525587965174">
                    <span>
                      <i class="fas fa-mobile-alt"></i>
                    </span>
                    <span><b>55 8796 5174</b></span>
                  </a>
                  <a class="p-button p-button_info" href="tel:+52014271480756">
                    <span>
                      <i class="fas fa-phone"></i>
                    </span>
                    <span><b>01 (427) 148 07 56</b></span>
                  </a>
                  <a class="p-button p-button_info" href="tel:+52014272741936">
                    <span>
                      <i class="fas fa-phone"></i>
                    </span>
                    <span><b>01 (427) 274 19 36</b></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>  
</html>