<?php
session_start();
include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Términos y condiciones de uso</title>
    <meta name="description" content="Se detallan los términos y condiciones de uso con respecto al sitio de MXcomp.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, términos, condiciones, términos y condiciones, notificación">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 11;
$op_menu = 5;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-avisoPrivacidad-fondo">
          <h1 class="p-titulo">Términos y condiciones de uso</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero p-light_fondo">
          <p class="p-text_p">Una vez ingresando a <strong><a class="p-text_p p-link_texto" href="https://mxcomp.com.mx/">www.mxcomp.com.mx</a></strong>, en lo sucesivo <strong>EL SITIO WEB</strong>, se aceptan expresamente estos Términos y Condiciones de Uso, por lo que se solicita al cibernauta, en lo sucesivo <strong>EL USUARIO</strong>, leer detenidamente el contenido de cada una de sus partes. En caso de no estar de acuerdo completa o parcialmente con estos Términos y Condiciones de Uso, se solicita a <strong>EL USUARIO</strong> abstenerse de acceder y usar <strong>EL SITIO WEB</strong>.</p>
          
          <p class="p-text_p"><strong>El SITIO WEB</strong> es propiedad exclusiva de MXCOMP; LA TECNOLOGÍA EN TUS MANOS S.A. DE C.V., en lo sucesivo <strong>MXCOMP</strong>, con domicilio fiscal en Calle Río Galindo 4-A, Col. San Cayetano, C.P. 76806, San Juan del Río, Qro.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> se reserva el derecho de modificar, añadir, eliminar o realizar cualquier cambio a los presentes Términos y Condiciones de Uso siempre que así lo estime conveniente, sin previo aviso y libre de cualquier responsabilidad por ello, siendo <strong>EL USUARIO</strong> el único responsable de consultar y enterarse de dichos cambios oportunamente.</p>
          
          
          
          <h3>Licencia</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> concede permiso limitado, no exclusivo y revocable a <strong>EL USUARIO</strong> para hacer uso de <strong>EL SITIO WEB</strong> únicamente bajo el propósito con que fue creado y que se estipula en los presentes Términos y Condiciones de Uso.</p>
          
          
          
          <h3>Propósito de EL SITIO WEB</h3>
          <p class="p-text_p"><strong>EL SITIO WEB</strong> ha sido creado con la finalidad de que <strong>MXCOMP</strong> promueva y comercialice productos tecnológicos de diversos proveedores y fabricantes, como lo son: hardware, equipos informáticos, software, periféricos, redes, accesorios y otros productos y servicios disponibles en México, a través de procesos digitales que permiten a <strong>EL USUARIO</strong> consultar imágenes, fichas técnicas, precios, hacer búsquedas, hacer cotizaciones, crear una cuenta, ingresar datos personales, levantar órdenes de compra, contactar al personal de <strong>MXCOMP</strong>, realizar pagos y dar seguimiento a sus órdenes de compra.</p>
          
          
          
          <h3>Cuenta de usuario</h3>
          <p class="p-text_p"><strong>EL USUARIO</strong> deberá crear una Cuenta de Usuario dentro de <strong>EL SITIO WEB</strong> para poder acceder a los servicios ofrecidos en el mismo.</p>
          
          <p class="p-text_p">Es responsabilidad de <strong>EL USUARIO</strong> mantener la confidencialidad de la información proporcionada en <strong>EL SITIO WEB</strong>, como; nombre de usuario, correo electrónico, contraseñas, número de tarjetas de crédito o débito, domicilios, y demás datos proporcionados con motivo de apertura o complemento de información de su Cuenta de Usuario. Para conocer la responsabilidad de <strong>MXCOMP</strong> sobre la confidencialidad, uso y protección de la información personal que es proporcionada en <strong>EL SITIO WEB</strong>, puede consultar el <strong><a class="p-text_p p-link_texto" href="aviso-privacidad" target="_blank">Aviso de Privacidad</a></strong>.</p>
          
          <p class="p-text_p"><strong>EL USUARIO</strong> podrá eliminar su cuenta en cualquier momento ingresando a la misma y seleccionando la opción de "Eliminar esta cuenta". Para poder llevar a cabo este procedimiento no deben existir Órdenes de Compra activas dentro de la cuenta a eliminar. En ese supuesto éstas deberán ser concluidas o eliminadas previamente.</p>
          
          <p class="p-text_p">Una cuenta eliminada permanecerá en "stand by" durante los 30 días naturales posteriores a su eliminación, durante este periodo <strong>EL USUARIO</strong> podrá iniciar sesión en su cuenta sin que ésta haya sufrido modificación alguna. Pasado el tiempo de "stand by" la cuenta pasará a estado de "eliminación".</p>
          
          <p class="p-text_p">Las cuentas de usuario que presenten inactividad, es decir, aquellas en las que <strong>EL USUARIO</strong> no haya iniciado sesión durante un año, entrarán automáticamente en estado de "eliminación".</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> se reserva el derecho a su exclusivo juicio de bloquear el acceso de <strong>EL USUARIO</strong> a su cuenta o incluso ponerla en estado de "eliminación" si presume el incumplimiento por parte de este último de forma total o parcial con los presentes Términos y Condiciones de Uso.</p>
          
          <p class="p-text_p">En el momento en que una Cuenta de Usuario entre en estado de "eliminación", <strong>EL SITIO WEB</strong> eliminará automáticamente toda la información personal que <strong>EL USUARIO</strong> hubiera guardado en su cuenta. <strong>EL USUARIO</strong> no podrá acceder más a su cuenta de usuario, consultar su historial de Órdenes de Compra, ni recuperar la información que ingresó en su Cuenta de Usuario.</p>
          
          
          
          <h3>Derechos de autor y propiedad industrial</h3>
          <p class="p-text_p">Todo el contenido de <strong>EL SITIO WEB</strong>, sin limitación, es propiedad de <strong>MXCOMP</strong> y de sus respectivos titulares y está protegido por las leyes de los Estados Unidos Mexicanos y por los tratados internacionales, por lo que queda prohibida la modificación, alteración, eliminación, reproducción o descarga completa o parcial de cualquiera de los materiales contenidos en <strong>EL SITIO WEB</strong>, como lo son; logotipos, imágenes, fotografías, texto, software, enlaces u otros contenidos, salvo la impresión simple de los elementos creados para ese fin y que sean para uso personal y no comercial, manteniendo siempre todos los avisos de copyright y de propiedad.</p>
          
          <p class="p-text_p">Cualquier información, programa, aplicación, software, comentarios, reseñas, anuncios de chat, mensajes de correo electrónico, cualquier sugerencia en forma creativa, ideas, notas, dibujos, conceptos y en general cualquier material que <strong>EL USUARIO</strong> trasmita a <strong>MXCOMP</strong>, lo hará con una licencia perpetua, irrevocable, universal, gratuita, no exclusiva, libre de regalías y de cualquier coste, que incluye los derechos de sublicenciar, vender, reproducir, modificar, distribuir, trasmitir, exhibir y ejecutar públicamente. <strong>EL USUARIO</strong> renuncia expresamente en este acto a llevar a cabo cualquier acción, demanda o reclamación en contra de <strong>MXCOMP</strong>, sus afiliados o proveedores por cualquier actual o eventual violación de cualquier derecho de autor o propiedad intelectual derivado de la trasmisión por parte de <strong>EL USUARIO</strong> de cualquiera de los elementos mencionados en este párrafo.</p>
          
          <p class="p-text_p">En caso de que <strong>EL USUARIO</strong> desee notificar sobre alguna supuesta infracción de derechos de autor o de propiedad industrial por parte de <strong>MXCOMP</strong>, deberá hacerlo a través del correo electrónico <strong><a class="p-text_p p-link_texto" href="mailto:mesadecontrol@mxcomp.com.mx">mesadecontrol@mxcomp.com.mx</a></strong> en donde se indique: </p>
          
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Nombre completo</li>
            <li>Domicilio</li>
            <li>Teléfono</li>
            <li>Correo electrónico</li>
            <li>Una firma física o electrónica de la persona autorizada para actuar en nombre del titular de un derecho exclusivo que presuntamente se ha infringido.</li>
            <li>Descripción precisa del contenido protegido mediante los derechos supuestamente violados, así como la identificación gráfica de dichas violaciones en <strong>El SITIO WEB</strong></li>
            <li>La declaración de que la parte demandante tiene una creencia de buena fe que el uso del material descrito en la reclamación no está autorizado por el propietario de los derechos de autor.</li>
            <li>La declaración de que la información en la notificación es exacta y bajo pena de perjurio, de que se autorice la parte reclamante para actuar en nombre del titular de un derecho exclusivo que presuntamente se ha infringido.</li>
          </ol>
          
          
          
          <h3>Información contenida en EL SITIO WEB</h3>
          <p class="p-text_p">Es responsabilidad de <strong>EL USUARIO</strong> identificar y asegurarse de que la información contenida en <strong>EL SITIO WEB</strong> proviene y ha sido publicada por <strong>MXCOMP</strong> o sus proveedores, anunciantes o patrocinadores, aclarando que dicha información puede conllevar errores, omisiones, o cambios sin previo aviso, por lo cual ni <strong>MXCOMP</strong> ni sus proveedores, anunciantes o patrocinadores ofrecen garantía alguna sobre la información publicada en <strong>EL SITIO WEB</strong> o los enlaces que este contiene, y recomiendan a <strong>EL USUARIO</strong> cerciorarse de la exactitud de la misma, en el entendido de que el uso y seguimiento de esta es bajo riesgo y responsabilidad de <strong>EL USUARIO</strong>.</p>
          
          
          
          <h3>Publicidad contenida en EL SITIO WEB</h3>
          <p class="p-text_p"><strong>EL USUARIO</strong> podrá encontrar dentro de <strong>EL SITIO WEB</strong> material publicitario propio y de terceros que pueden ser proveedores, anunciantes o patrocinadores independientes a <strong>MXCOMP</strong>. <strong>EL USUARIO</strong> reconoce y acepta que dicho material se encuentra protegido por las leyes que en materia de propiedad intelectual e industrial resulten aplicables.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> no garantiza ni avala de ninguna manera la veracidad, precisión, legalidad, moralidad o ninguna otra característica del contenido publicado en <strong>EL SITIO WEB</strong>.</p>
          
          
          
          <h3>Métodos de pago</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> ofrece a <strong>EL USUARIO</strong> dos métodos para realizar sus pagos; con tarjeta de crédito o débito y con depósito o transferencia.</p>
          
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>
              <p class="p-text_p"><strong>Pago con tarjeta de crédito o débito VISA®, MasterCard® o American Express®.</strong></p>
              
              <p class="p-text_p">Para este método de pago <strong>EL SITIO WEB</strong> solicitará a <strong>EL USUARIO</strong> de forma directa o a través de las instituciones financieras legalmente constituidas y contratadas por <strong>MXCOMP</strong> los datos pertinentes para poder efectuar el cargo a la tarjeta, dichos datos son:</p>
              
              <ol type="I" start="1" class="p-text_p" style="list-style: circle;">
                <li>Nombre completo del titular de la tarjeta.</li>
                <li>Domicilio del estado de cuenta de la tarjeta.</li>
                <li>Los 15 o 16 dígitos de la tarjeta.</li>
                <li>La clave de seguridad de la tarjeta (CVV).</li>
                <li>La fecha de vencimiento de la tarjeta.</li>
              </ol>
              
              <p class="p-text_p">Es probable que se aplique el cargo de alguna comisión por pagar con tarjeta, esto debido a los costes adicionales que implica realizar este tipo de transacciones.</p>
              
              <p class="p-text_p" style="margin-bottom: 1rem;">Los pagos con tarjeta serán aplicados al momento siempre y cuando se cumpla con los requisitos que solicite el banco, reservándose <strong>MXCOMP</strong> el derecho de cancelar cualquier pago que no cumpla con los mismos.</p>
            </li>
            <li>
              <p class="p-text_p"><strong>Pago con depósito o transferencia.</strong></p>
              
              <p class="p-text_p"><strong>EL USUARIO</strong> puede optar por realizar su pago directamente a la cuenta bancaria de <strong>MXCOMP</strong> utilizando para ello los siguientes datos:</p>
              
              <ol type="I" start="1" class="p-text_p">
                <li><b>Banco:</b> Santander</li>
                <li><b>Titular:</b> MXCOMP; LA TECNOLOGÍA EN TUS MANOS S.A. DE C.V.</li>
                <li><b>No. de Tarjeta:</b> 5579 0890 0171 8835</li>
                <li><b>Número de cuenta:</b> 65508000346</li>
                <li><b>Clave Interbancaria:</b> 014685655080003469</li>
                <li><b>Sucursal:</b> 7896</li>
              </ol>
              
              <p class="p-text_p">Los pagos realizados directamente a la cuenta de <strong>MXCOMP</strong> serán siempre libres de cualquier comisión o cargo adicional, sin embargo, este tipo de transacciones pueden demorar en su aplicación como abono en firme, por lo que es posible que el proceso de compra sea más lento.</p>
              
              <p class="p-text_p"><strong>EL USUARIO</strong> debe asegurarse de adjuntar en un formato digital compatible, el comprobante de pago emitido al momento de realizar su depósito de efectivo o transferencia bancaria en un lapso no mayor a 20 hrs. contadas a partir de haber generado su orden de compra. Transcurrido ese tiempo, la orden será cancelada y <strong>EL USUARIO</strong> deberá iniciar el proceso de compra nuevamente.</p>
              
              <p class="p-text_p"><strong>MXCOMP</strong> no garantiza de ninguna forma la existencia de "stock" para surtir la orden de compra de <strong>EL USUARIO</strong>, por lo cual, en caso de que <strong>EL USUARIO</strong> haya realizado su pago exitosamente y <strong>MXCOMP</strong> no pueda cumplir con la entrega de la mercancía, este último se compromete a realizar la devolución del importe total pagado por <strong>EL USUARIO</strong> por dicha mercancía, en un lapso no mayor a 30 días hábiles.</p>
            </li>
          </ol>
          
          
          
          <h3>Seguridad en datos personales y financieros</h3>
          <p class="p-text_p">Para proteger la seguridad de su información, <strong>MXCOMP</strong> emplea el estándar de la industria de Secure Sockets Layer (SSL), además de encriptar sus datos personales al momento de almacenar su solicitud. También es posible que <strong>MXCOMP</strong> contrate los servicios de terceros especialistas en seguridad cibernética e implemente códigos de programación desarrollados con la finalidad de incrementar la seguridad sobre los datos personales ingresados en <strong>EL SITIO WEB</strong>.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong>, de conformidad con la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, es el responsable del tratamiento de los datos personales que recaba de <strong>EL USUARIO</strong>. Para mayor información acerca del tratamiento y de los derechos que <strong>EL USUARIO</strong> puede hacer valer, se puede consultar el <strong><a class="p-text_p p-link_texto" href="aviso-privacidad" target="_blank">Aviso de Privacidad</a></strong> completo contenido en <strong>EL SITIO WEB</strong>.</p>
          
          
          
          <h3>Condiciones de envío</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> ofrece la entrega a domicilio de los productos adquiridos en <strong>EL SITIO WEB</strong> por medio de un tercero proveedor de servicios de paquetería, a cualquier parte de la República Mexicana siempre y cuando sea dentro de la zona de cobertura de dicho proveedor y únicamente a partir de haber recibido el pago en firme por parte de <strong>EL USUARIO</strong> conforme a los presentes Términos y Condiciones de Uso.</p>
          
          <p class="p-text_p">El costo del servicio será el que el proveedor del servicio de paquetería estipule al momento de recibir la solicitud del servicio en función del domicilio de recolección, el domicilio de entrega y las dimensiones y peso del(los) paquete(s).</p>
          
          <p class="p-text_p">Los tiempos de traslado y entrega de la mercancía, así como la prestación del servicio de paquetería están sujetos a las políticas y disponibilidad del proveedor del servicio de paquetería.</p>
          
          <p class="p-text_p">Es responsabilidad de <strong>EL USUARIO</strong> facilitar a <strong>MXCOMP</strong> y al proveedor del servicio de paquetería tanta información como estos puedan requerirle con la finalidad de lograr la entrega de la mercancía en el domicilio proporcionado por <strong>EL USUARIO</strong>.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> contrata a nombre de <strong>EL USUARIO</strong> un seguro por el 100% del valor de la mercancía contra cualquier eventualidad que pudiera ocurrir con la misma durante el proceso de envío, lo que puede generar algunos costos adicionales imputables a <strong>EL USUARIO</strong> y que le serán comunicados previo al pago de su orden de compra. Debido a lo anterior y a que <strong>MXCOMP</strong> por medio de sus proveedores entrega la mercancía al proveedor de servicios de paquetería debidamente embalada, empacada y etiquetada, <strong>MXCOMP</strong> no se responsabiliza por mercancía robada, extraviada, alterada o dañada por el proveedor del servicio de paquetería.</p>
          
          <p class="p-text_p">En caso de requerir hacer válido el seguro de paquetería, <strong>EL USUARIO</strong> deberá:</p>
          
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Verificar la cantidad y condiciones en las que recibe la mercancía en presencia del encargado de realizar la entrega.</li>
            <li>Tomar notas de todas las anomalías detectadas.</li>
            <li>En su caso, tomar fotografías del paquete y la mercancía dañados o incompletos en el momento de la recepción.</li>
            <li>No firmar de conformidad.</li>
            <li>Reportar el incidente al correo <strong><a class="p-text_p p-link_texto" href="mailto:atencionaclientes@mxcomp.com.mx">atencionaclientes@mxcomp.com.mx</a></strong>, y en caso de que aplique, enviar las anotaciones junto con las fotografías antes de transcurridas 24 hrs. a partir de que se suscitó el incidente.</li>
          </ol>
          
          
          
          <h3>Garantía sobre defectos de fabricación</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> es un intermediario entre el fabricante y el usuario final, a quien traslada directamente la garantía sobre defectos de fabricación otorgada por las diversas marcas de los productos comercializados en <strong>EL SITIO WEB</strong>.</p>
          
          <p class="p-text_p">La garantía directa con <strong>MXCOMP</strong> es de 90 días contados a partir de la recepción de los productos por parte de <strong>EL USUARIO</strong>, y aplica sobre las normas internacionales de fabricación de todos los productos, a excepción de software, tintas, cartuchos y otros consumibles similares que por su naturaleza no cuentan con garantía.</p>
          
          <p class="p-text_p">Para ampliar la garantía directa con <strong>MXCOMP</strong> de 90 días a un año para los productos que cuentan con ella, <strong>EL USUARIO</strong> deberá registrar sus productos en el apartado destinado para ello dentro de su cuenta de usuario, en un lapso no mayor a 10 días naturales a partir de la recepción de los mismos.</p>
          
          <p class="p-text_p">Para poder hacer válida la garantía, <strong>EL USUARIO</strong> deberá presentar personalmente en cualquier sucursal <strong>MXCOMP</strong> el(los) producto(s) defectuoso(s) junto con sus empaques, manuales, controladores y accesorios que originalmente se incluían al momento de recibirlo, o bien, en caso de que haya disponibilidad, podrá recurrir a un centro de servicio autorizado por el fabricante, en cuyo caso deberá proceder conforme éste último le indique. En este último supuesto, <strong>EL USUARIO</strong> libera a <strong>MXCOMP</strong> de toda responsabilidad durante dicho trámite.</p>
          
          <p class="p-text_p">En caso de hacer válida la garantía en alguna de las sucursales de <strong>MXCOMP</strong>, el personal revisará físicamente el(los) producto(s) al momento de su ingreso en presencia de <strong>EL USUARIO</strong>, si todo está en orden, el personal procederá a recibir el(los) producto(s) generando una Orden de Servicio por Garantía (OSG), que es el único documento con que las parte amparan la entrega-recepción de los productos con supuesto defecto de fabricación.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> no se hará responsable por la pérdida de información, archivos, imágenes, música, programas y demás contenido, que pueda suscitarse derivado de un defecto de fabricación, por lo que se sugiere a <strong>EL USUARIO</strong> respaldar constantemente todo aquel contenido digital que le represente algún valor.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> no se hace responsable por ningún tipo de daño general, punitivo, incidental o especial, derivado de algún producto defectuoso, esto incluye la pérdida de información, interrupción del uso, el costo de la recuperación de la información perdida, pérdida de beneficios y el costo de la instalación o desmontaje de productos de reemplazo o cualquier otro efecto causado por cualquier defecto o por la reparación o reemplazo de los productos.</p>
          
          <p class="p-text_p">El tiempo y resolución de las garantías serán determinados por el fabricante de cada producto, y esto no implicará en ningún caso responsabilidad para <strong>MXCOMP</strong>.</p>
          
          
          
          <h3>Cancelaciones y devoluciones</h3>
          <p class="p-text_p">En caso de que <strong>EL USUARIO</strong> desee revocar su consentimiento otorgado al aceptar estos Términos y Condiciones de Uso, podrá hacerlo sin responsabilidad alguna dentro de los primeros cinco días hábiles a partir de la recepción de la mercancía. <strong>EL USUARIO</strong> deberá hacer la revocación mediante aviso y la entrega de la mercancía en perfecto funcionamiento y sin rastros de uso junto con sus empaques, manuales, controladores y accesorios que originalmente se incluían al momento de recibirla. Podrá hacerlo de forma personal en cualquiera de las sucursales de <strong>MXCOMP</strong> o por correo registrado o certificado. En este caso, los costos de flete y seguro correrán a cargo de <strong>EL USUARIO</strong>.</p>
          
          <p class="p-text_p">Una vez recibido el aviso de revocación y la mercancía conforme a los presentes Términos y Condiciones de Uso, <strong>MXCOMP</strong> se compromete a reintegrar el importe total pagado por <strong>EL USUARIO</strong> únicamente por concepto de la mercancía en cuestión sin incluir comisiones y costos adicionales derivados de la operación, en un lapso no mayor a 30 días hábiles.</p>
          
          
          
          <h3>Garantía de EL SITIO WEB</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> no garantiza a <strong>EL USUARIO</strong> que <strong>EL SITIO WEB</strong> satisfaga sus requerimientos, o que los servicios que ahí se presentan y ofrecen se encuentren exentos de errores, que sean seguros, que sean constantes, ni que su contenido o sus servidores se encuentren libres de virus, "haking" u otros elementos perjudiciales.</p>
          
          <p class="p-text_p"><strong>MXCOMP</strong> se deslinda expresamente de cualquier responsabilidad por la exactitud, integridad, el contenido o la disponibilidad de la información contenida en <strong>EL SITIO WEB</strong>. Incluyendo sin limitación alguna:</p>
          
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>La disponibilidad de uso de <strong>EL SITIO WEB</strong></li>
            <li>La ausencia de virus, errores, desactivadores, o cualquier otro material contaminante o con funciones destructivas en la información o programas disponibles en o a través de <strong>EL SITIO WEB</strong> o en general cualquier falla en dicho sitio.</li>
            <li>La actualización constante del contenido de <strong>EL SITIO WEB</strong> que pudiera provocar que parte de la información publicada o contenida en <strong>EL SITIO WEB</strong> haya quedado obsoleta y/o contener imprecisiones o errores tipográficos u ortográficos.</li>
          </ol>
          
          
          
          <h3>Limitación de responsabilidad</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> no será responsable, hasta el máximo permitido por las leyes aplicables, de ningún daño directo, accidental, incidental, indirecto o consecuente que surjan del uso de <strong>EL SITIO WEB</strong> o que se relacionen con:</p>
          
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>La alteración o modificación, total o parcial, de la información después de haber sido incluida en <strong>EL SITIO WEB</strong>.</li>
            <li>El uso o ejecución de <strong>EL SITIO WEB</strong> con retraso o la falta de disponibilidad de el mismo.</li>
            <li>La proveeduría o falta de la misma de servicios de cualquier material contenido o publicado en <strong>EL SITIO WEB</strong>.</li>
            <li>La actualización o desactualización de dichos materiales.</li>
            <li>Cualquier otro aspecto o característica de los materiales contenidos o publicados a través de las ligas que eventualmente se incluyan en <strong>EL SITIO WEB</strong>.</li>
            <li>Cuando <strong>EL USUARIO</strong> utilice <strong>EL SITIO WEB</strong> con fines distintos al propósito de <strong>EL SITIO WEB</strong>.</li>
          </ol>
          
          <p class="p-text_p">Todos los supuestos anteriores serán vigentes aun en los casos en que se notifique a <strong>MXCOMP</strong> acerca de la posibilidad de ocurrencia de los mismos.</p>
          
          
          
          <h3>Cesión de derechos</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> podrá, en cualquier momento y cuando así lo estime conveniente, ceder total o parcialmente sus derechos y obligaciones derivados de los presentes Términos y Condiciones de Uso. En virtud de dicha cesión, <strong>MXCOMP</strong> quedará liberada de cualquier obligación a favor de <strong>EL USUARIO</strong>, establecida en los presentes Términos y Condiciones de Uso.</p>
          
          
          
          <h3>Indemnización</h3>
          <p class="p-text_p"><strong>El USUARIO</strong> indemnizará a <strong>MXCOMP</strong>, sus representantes, accionistas, afiliados, proveedores, agentes, patrocinadores y funcionarios por cualquier acción, demanda o reclamación (incluyendo, pero no limitado a, los costos de cobranza, cargos de abogados y otros costos razonables de la defensa o la aplicación de sus obligaciones bajo los presentes) derivadas de cualquier incumplimiento por parte de <strong>EL USUARIO</strong> a los presentes Términos y Condiciones de Uso, o del mal uso que haga de <strong>EL SITIO WEB</strong>.</p>
          
          
          
          <h3>Terminación</h3>
          <p class="p-text_p">Los presentes Términos y Condiciones de Uso tendrán vigencia hasta que sean terminados por cualquiera de las partes. <strong>MXCOMP</strong> podrá restringir el acceso de <strong>EL USUARIO</strong> a <strong>EL SITIO WEB</strong> a su exclusiva discreción y sin necesidad de aviso o notificación si éste último incumpliera completa o parcialmente con los presentes Términos y Condiciones de Uso. Así mismo, <strong>EL USUARIO</strong> podrá dejar de usar <strong>EL SITIO WEB</strong> en cualquier momento en que lo estime conveniente.</p>
          
          <p class="p-text_p">En cualquiera de los dos supuestos anteriores <strong>EL USUARIO</strong> deberá destruir inmediatamente todos los materiales obtenidos de <strong>EL SITIO WEB</strong>, además de toda la documentación relacionada y todas las copias e instalaciones, si se hicieron bajo estos Términos y Condiciones de Uso.</p>
          
          
          
          <h3>No renuncia de derechos</h3>
          <p class="p-text_p">La inactividad por parte de <strong>MXCOMP</strong>, sus representantes, accionistas, afiliados, proveedores, agentes, patrocinadores y funcionarios al ejercicio de cualquier derecho o acción derivados de los presentes Términos y Condiciones de Uso, en ningún momento deberá interpretarse como renuncia a dichos derechos o acciones.</p>
          
          
          
          <h3>Legislación y jurisdicción aplicables</h3>
          <p class="p-text_p">Los presentes Términos y Condiciones de Uso se regirán e interpretarán por las leyes del Estado de Querétaro y cualquier acción legal o de equidad que surja de o en relación a estos Términos y Condiciones de Uso y al Aviso de Privacidad, se presentará sólo en los tribunales estatales o federales ubicados en dicha entidad.</p>
          
          <p class="p-text_p">Si cualquier disposición de estos Términos y Condiciones de Uso es ilegal, nula o inaplicable por cualquier razón, dicha disposición se considerará separable de estos Términos y Condiciones de Uso y no afectará a la validez y aplicabilidad de las disposiciones restantes. Estos Términos y Condiciones de Uso constituyen el acuerdo completo entre <strong>MXCOMP</strong> y <strong>EL USUARIO</strong> en relación con los temas aquí mencionados.</p>
          
          <br>
          
          <hr class="p-avisoPrivacidad-divisor">
          <small><em>Última actualización 10/12/2020</em></small>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>  
</html>