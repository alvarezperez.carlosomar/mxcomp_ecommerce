$(document).ready(function(){
  /*
  * Abrir modal de filtros
  */
  $(document).on('click', '#id-busqueda-filtros_button_open', function(){
    $('html').addClass('p-busqueda-paginacion_window_no_scroll');
    $('document, .p-busqueda-filtros_movil_contenedor').addClass('p-busqueda-filtros_movil_contenedor_visible');
    $('document, #id-busqueda-filtros_button_close').addClass('p-busqueda-filtros_movil_fondo_activo');
  });
  /*
  * Cerrar modal de filtros
  */
  $(document).on('click', '#id-busqueda-filtros_button_close', function(){
    $('html').removeClass('p-busqueda-paginacion_window_no_scroll');
    $('document, .p-busqueda-filtros_movil_contenedor').removeClass('p-busqueda-filtros_movil_contenedor_visible');
    $('document, #id-busqueda-filtros_button_close').removeClass('p-busqueda-filtros_movil_fondo_activo');
  });
  /*
  * Boton de paginacion
  */
  $(document).on('click', '.g-busqueda-paginacion', function(){
    let pagina = $(this).attr('data-pagina'), data = new FormData();

    data.append('accion', 'paginacion');
    data.append('pagina', pagina);
    
    $('document, #id-busqueda-load_productos').slideDown('fast');
    $('document, #id-busqueda-contenedor_productos').css('display', 'none');
    $('document, #id-busqueda-contenedor_productos').html('');

    fetch('procesos/busqueda/editar_paginacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '1' ? (
        $('html, body').animate({ scrollTop: 0 }, 600),
        mostrar_elementos()
      ) : (
        console.log(mensaje),
        location.reload()
      );
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Select de paginacion
  */
  $(document).on('change', '#id-busqueda-paginacion_select', function(){
    let pagina = $(this).val(), data = new FormData();

    data.append('accion', 'paginacion');
    data.append('pagina', pagina);

    if(pagina !== ""){
      $('document, #id-busqueda-load_productos').slideDown('fast');
      $('document, #id-busqueda-contenedor_productos').css('display', 'none');
      $('document, #id-busqueda-contenedor_productos').html('');
      
      fetch('procesos/busqueda/editar_paginacion.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;
  
        respuesta === '1' ? (
          $('html, body').animate({ scrollTop: 0 }, 600),
          mostrar_elementos()
        ) : (
          console.log(mensaje),
          location.reload()
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }
  });
  /*
  * Filtro de opciones por seccion
  */
  $(document).on('click', '.g-busqueda-filtro_opcion', function(){
    let seccion = $(this).attr('data-seccion'), id = $(this).attr('data-id'), data = new FormData(), url = '';
    
    $('document, #id-busqueda-load_productos').slideDown('fast');
    $('document, #id-busqueda-contenedor_productos').css('display', 'none');
    $('document, #id-busqueda-contenedor_productos').html('');
    
    $(window).width() >= 769 ? $('document, #id-busqueda_loading_filros_mensaje_desktop').slideDown(100) : $('document, #id-busqueda_loading_filros_mensaje_movil').slideDown(100);

    data.append('accion', 'filtrar');
    data.append('id', id);

    switch(seccion){
      case 'categoria':
        url = 'procesos/busqueda/editar_filtro_categorias.php';
        break;

      case 'subcat_tipo':
        url = 'procesos/busqueda/editar_filtro_subcategorias_tipo.php';
        break;

      case 'subcat_especifica':
        url = 'procesos/busqueda/editar_filtro_subcategorias_especificas.php';
        break;

      case 'marca':
        url = 'procesos/busqueda/editar_filtro_marcas.php';
        break;
    }

    fetch(url, {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '1':
          location.reload();
          break;

        case '2':
          console.log(mensaje);
          break;

        case '3':
          $('html, body').animate({ scrollTop: 0 }, 600);
          setTimeout(function(){ mostrar_elementos(); }, 300);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Filtro de ordenar por precio menor/mayor
  */
  $(document).on('click', '.g-busqueda-ordenar_precio', function(){
    let seccion = $(this).attr('data-seccion'), activado = $(this).attr('data-activado'), data = new FormData();
    
    $('document, #id-busqueda-load_productos').slideDown('fast');
    $('document, #id-busqueda-contenedor_productos').css('display', 'none');
    $('document, #id-busqueda-contenedor_productos').html('');
    
    $(window).width() >= 769 ? $('document, #id-busqueda_loading_filros_mensaje_desktop').slideDown(100) : $('document, #id-busqueda_loading_filros_mensaje_movil').slideDown(100);
    
    data.append('accion', 'ordenar');
    data.append('activado', activado);

    switch(seccion){
      case 'precio_menor':
        url = 'procesos/busqueda/editar_filtro_precio_menor.php';
        break;

      case 'precio_mayor':
        url = 'procesos/busqueda/editar_filtro_precio_mayor.php';
        break;
    }

    fetch(url, {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '1' ? location.reload() : (
        $('html, body').animate({ scrollTop: 0 }, 600),
        setTimeout(function(){ mostrar_elementos(); }, 300)
      );
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Input - Precio inicial - click
  */
  $(document).on('click', '.g-busqueda-precio_inicial_input', function(){
    $(this).select();
  });
  /*
  * Input - Precio inicial - keyup
  */
  $(document).on('keyup', '.g-busqueda-precio_inicial_input', function(e){
    if(e.keyCode === 13){
      $('document, .g-busqueda-rangoPrecio_filtrar').click();
    }else{
      let precio_inicial = $(this).val(), data = new FormData();
  
      if(precio_inicial.length !== 0){
        $('document, .g-busqueda-precio_inicial_alert_error').slideUp('fast');
        $(this).removeClass('p-input_error');
  
        data.append('accion', 'validar');
        data.append('valor', precio_inicial);
  
        fetch('validaciones/validar_campo_precio.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);
  
          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;
  
          respuesta === '1' ? $('document, .g-busqueda-precio_inicial_alert_info').slideUp('fast') : $('document, .g-busqueda-precio_inicial_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-busqueda-precio_inicial_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Input - Precio final - click
  */
  $(document).on('click', '.g-busqueda-precio_final_input', function(){
    $(this).select();
  });
  /*
  * Input - Precio final - keyup
  */
  $(document).on('keyup', '.g-busqueda-precio_final_input', function(e){
    if(e.keyCode === 13){
      $('document, .g-busqueda-rangoPrecio_filtrar').click();
    }else{
      let precio_final = $(this).val(), data = new FormData();
      
      if(precio_final.length !== 0){
        $('document, .g-busqueda-precio_final_alert_error').slideUp('fast');
        $(this).removeClass('p-input_error');

        data.append('accion', 'validar');
        data.append('valor', precio_final);

        fetch('validaciones/validar_campo_precio.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);

          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;

          respuesta === '1' ? $('document, .g-busqueda-precio_final_alert_info').slideUp('fast') : $('document, .g-busqueda-precio_final_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-busqueda-precio_final_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Button - Filtrar por rango de precio
  */
  $(document).on('click', '.g-busqueda-rangoPrecio_filtrar', function(){
    let precio_inicial = $('document, .g-busqueda-precio_inicial_input').val(), precio_final = $('document, .g-busqueda-precio_final_input').val(), data = new FormData();

    data.append('accion', 'filtrar');
    data.append('opcion', 'agregar');
    data.append('precio_inicial', precio_inicial);
    data.append('precio_final', precio_final);

    fetch('procesos/busqueda/editar_filtro_rango_precios.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);

      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta;

      switch(respuesta){
        case '1':
          $('document, .g-busqueda-precio_inicial_alert_error').slideDown('fast');
          $('document, .g-busqueda-precio_inicial_input').addClass('p-input_error');
          $('document, .g-busqueda-precio_inicial_input').focus();
          break;

        case '2':
          $('document, .g-busqueda-precio_inicial_alert_info').slideDown('fast');
          $('document, .g-busqueda-precio_inicial_input').focus();
          break;

        case '3':
          $('document, .g-busqueda-precio_final_alert_error').slideDown('fast');
          $('document, .g-busqueda-precio_final_input').addClass('p-input_error');
          $('document, .g-busqueda-precio_final_input').focus();
          break;

        case '4':
          $('document, .g-busqueda-precio_final_alert_info').slideDown('fast');
          $('document, .g-busqueda-precio_final_input').focus();
          break;

        case '5':
          $('document, #id-busqueda-load_productos').slideDown('fast');
          $(window).width() >= 769 ? $('document, #id-busqueda_loading_filros_mensaje_desktop').slideDown(100) : $('document, #id-busqueda_loading_filros_mensaje_movil').slideDown(100);

          setTimeout(function(){
            $('document, #id-busqueda_load_prueba').css('display', 'flex');
            $('document, #id-busqueda-contenedor_productos').css('display', 'none');
            $('document, #id-busqueda-contenedor_productos').html('');
            $('html, body').animate({ scrollTop: 0 }, 600);
          }, 500);
          
          setTimeout(function(){ mostrar_elementos(); }, 800);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Button - Eliminar filtro por rango de precio
  */
  $(document).on('click', '.g-busqueda-rangoPrecio_eliminar', function(){
    let data = new FormData();

    $('document, #id-busqueda-load_productos').slideDown('fast');
    $('document, #id-busqueda-contenedor_productos').css('display', 'none');
    $('document, #id-busqueda-contenedor_productos').html('');
    
    $(window).width() >= 769 ? $('document, #id-busqueda_loading_filros_mensaje_desktop').slideDown(100) : $('document, #id-busqueda_loading_filros_mensaje_movil').slideDown(100);

    data.append('accion', 'filtrar');
    data.append('opcion', 'eliminar');

    fetch('procesos/busqueda/editar_filtro_rango_precios.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);

      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta;

      if(respuesta === '1'){
        $('html, body').animate({ scrollTop: 0 }, 600);
        setTimeout(function(){ mostrar_elementos(); }, 300);
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });

  mostrar_elementos();
  $('html').addClass('p-modal-derecho_window_no_scroll');

  function mostrar_elementos(){
    let data = new FormData();

    data.append('accion', 'mostrar');

    let promesa_showDatos = new Promise((resolve, reject) => {
      fetch('procesos/busqueda/mostrar_resultados.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, html1 = datos.html1, html2 = datos.html2, html3 = datos.html3, html4 = datos.html4, total_productos = datos.total_productos, filtro_rango_precio = datos.filtro_rango_precio;
  
        switch(respuesta){
          case '1':
            parseInt(total_productos) > 0 || (parseInt(total_productos) === 0 && parseInt(filtro_rango_precio) === 1) ? $('document, #id-contenedor-titulo_busqueda').addClass('p-busqueda-fondo_marginTop') : $('document, #id-contenedor-titulo_busqueda').removeClass('p-busqueda-fondo_marginTop');
            
            $('document, #id-busqueda-contenedor_texto_resultados').html(html1);
            $('document, #id-busqueda-contenedor_filtros').html(html2);
            $('document, #id-busqueda-filtros_button_abrirFiltros').html(html3);
            $('document, #id-busqueda-contenedor_productos').html(html4);
            $('document, #id-busqueda-contenedor_filtros').removeClass('p-busqueda-filtros_movil_contenedor_visible');
            $('html').removeClass('p-busqueda-paginacion_window_no_scroll');
            resolve("resuelto");
            break;
  
          case '2':
            history.go(-1);
            break;
  
          default:
            reject(respuesta);
        }
      }).catch(error => {
        reject('[Error] - ', error);
      });
    });
    
    promesa_showDatos
    .then((respuesta) => {
      setTimeout(function(){
        $('document, #id-busqueda-load_datos_generales').addClass('p-busqueda-loading_stop');
      }, 1500);
      
      setTimeout(function(){
        $('html').removeClass('p-modal-derecho_window_no_scroll');
        $('document, #id-busqueda-load_productos').css('display', 'none');
        $('document, #id-busqueda-contenedor_productos').slideDown('fast');
      }, 1550);
    }).catch((error) => {
      console.error(error);
    });
  }
  /*
  * Opciones desplegables del menú
  */
  $(document).on('click', '.g-busqueda-filtros', function(){
    let filtro = $(this).attr('data-filtro');
    
    if($(this).hasClass('p-busqueda-p_option_active')){
      $(this).removeClass('p-busqueda-p_option_active');
      $('document, .g-busqueda-menu_opciones[data-filtro=' + filtro + ']').slideUp('fast');
    }else{
      $(this).addClass('p-busqueda-p_option_active');
      $('document, .g-busqueda-menu_opciones[data-filtro=' + filtro + ']').slideDown('fast');
    }
  });
});