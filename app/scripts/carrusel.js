$(document).ready(function(){
  var cantidadImagenes = $('document, .p-carrusel-imagenes li').length, imagenPosicion = 1, sliderTime = "";

  for(let i = 1; i <= cantidadImagenes; i++){
    $('document, .p-carrusel-paginacion').append('<li><span class="icon"><i class="fas fa-circle"></i></span></li>');
  }

  $('document, .p-carrusel-imagenes li').hide();
  $('document, .p-carrusel-imagenes li:first').show();
  $('document, .p-carrusel-paginacion li:first').css('color', '#4D4D4D');
  $('document, .p-carrusel-paginacion li').click(paginacion);
  $('document, .p-carrusel-flecha_izquierda span').click(anteriorImagen);
  $('document, .p-carrusel-flecha_derecha span').click(siguienteImagen);

  slideEjecucion();
  
  function paginacion(){
    let posicionPaginacion = $(this).index() + 1;
    $('document, .p-carrusel-imagenes li').hide();
    $('document, .p-carrusel-imagenes li:nth-child(' + posicionPaginacion + ')').fadeIn();
    $('document, .p-carrusel-paginacion li').css('color', '#9EA5A5');
    $(this).css('color', '#4D4D4D');
    imagenPosicion = posicionPaginacion;
    clearInterval(sliderTime);
    slideEjecucion();
  }

  function siguienteImagen(){
    imagenPosicion >= cantidadImagenes ? imagenPosicion = 1 : imagenPosicion++;
    $('document, .p-carrusel-imagenes li').hide();
    $('document, .p-carrusel-imagenes li:nth-child(' + imagenPosicion + ')').fadeIn();
    $('document, .p-carrusel-paginacion li').css('color', '#9EA5A5');
    $('document, .p-carrusel-paginacion li:nth-child(' + imagenPosicion + ')').css('color', '#4D4D4D');
    clearInterval(sliderTime);
    slideEjecucion();
  }

  function anteriorImagen(){
    imagenPosicion <= 1 ? imagenPosicion = cantidadImagenes : imagenPosicion--;
    $('document, .p-carrusel-imagenes li').hide();
    $('document, .p-carrusel-imagenes li:nth-child(' + imagenPosicion + ')').fadeIn();
    $('document, .p-carrusel-paginacion li').css('color', '#9EA5A5');
    $('document, .p-carrusel-paginacion li:nth-child(' + imagenPosicion + ')').css('color', '#4D4D4D');
    clearInterval(sliderTime);
    slideEjecucion();
  }

  function slideEjecucion(){
    sliderTime = setInterval(function(){ siguienteImagen(); }, 8000);
  }
});