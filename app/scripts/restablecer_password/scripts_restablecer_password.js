$(document).ready(function(){
  var correo_escrito1 = "", correo_escrito2 = "";
  /*
  * Restablecer password - Detecta cuando se desenfoca el campo "Correo electronico"
  */
  $(document).on('blur', '.g-restablecerPassword-correo_input', function(){
    let correo = $(this).val(), data = new FormData();

    if(correo.length !== 0){
      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast') : $('document, .g-restablecerPassword-correo_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast');
    }
  });
  /*
  * Restablecer password - Detecta cuando se escribe en el campo "Correo electronico"
  */
  $(document).on('keyup', '.g-restablecerPassword-correo_input', function(){
    let correo = $(this).val(), data = new FormData();

    if(correo.length !== 0){
      if(correo_escrito1 === correo){
        $('document, .g-restablecerPassword-correo_alert_error_1').slideDown('fast');
      }else{
        $('document, .g-restablecerPassword-correo_alert_error_1').slideUp('fast');
        correo_escrito1 = "";
      }

      if(correo_escrito2 === correo){
        $('document, .g-restablecerPassword-notification_contenedor').slideDown('fast');
      }else{
        $('document, .g-restablecerPassword-notification_contenedor').slideUp('fast');
        correo_escrito2 = "";
      }

      $('document, .g-restablecerPassword-correo_alert_error_2').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast') : $('document, .g-restablecerPassword-correo_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast');
      $('document, .g-restablecerPassword-correo_alert_error_1').slideUp('fast');
    }
  });
  /*
  * Restablecer password - Acciones al enviar la informacion
  */
  $(document).on('submit', '.g-restablecerPassword-correo_form', function(e){
    let correo = $('document, .g-restablecerPassword-correo_input').val(), data = new FormData();

    $('document, .g-restablecerPassword-notification_contenedor').slideUp('fast');
    $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast');

    $('document, .g-restablecerPassword-loading').slideDown('fast');
    $('document, .g-restablecerPassword-correo_input').prop('disabled', true);
    $('document, .g-restablecerPassword-enviar_correo_button').prop('type', 'button');
    $('document, .g-restablecerPassword-enviar_correo_button').prop('disabled', true);

    data.append('accion', 'recuperar');
    data.append('correo', correo);
    
    fetch('procesos/usuario/proceso_restablecerPassword_enviarCorreos.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      $('document, .g-restablecerPassword-loading').slideUp('fast');
      
      setTimeout(function(){
        $('document, .g-restablecerPassword-correo_input').prop('disabled', false);
        $('document, .g-restablecerPassword-enviar_correo_button').prop('type', 'submit');
        $('document, .g-restablecerPassword-enviar_correo_button').prop('disabled', false);
      }, 400);

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('document, .g-restablecerPassword-correo_alert_error_2').slideDown('fast');
          $('document, .g-restablecerPassword-correo_input').addClass('p-input_error');
          break;

        case '2':
          $('document, .g-restablecerPassword-correo_alert_info').slideDown('fast');
          break;

        case '3':
          $('document, .g-restablecerPassword-correo_alert_error_1').slideDown('fast');
          $('document, .g-restablecerPassword-correo_input').addClass('p-input_error');
          correo_escrito1 = correo;
          break;

        case '4':
          $('document, .g-restablecerPassword-contenedor_notification').html(mensaje);
          $('document, .g-restablecerPassword-notification_contenedor').slideDown('fast');
          correo_escrito2 = "";
          break;

        case '5':
          $('document, .g-restablecerPassword-contenedor_notification').html(mensaje);
          $('document, .g-restablecerPassword-notification_contenedor').slideDown('fast');
          correo_escrito2 = "";
          break;
      }

      setTimeout(function(){ $('document, .g-restablecerPassword-correo_input').focus(); }, 400);
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Nueva password"
  */
  $(document).on('keyup', '.g-restablecerPassword-nueva_password_input', function(){
    let password = $(this).val(), data = new FormData();
    
    if(password.length !== 0){
      $('document, .g-restablecerPassword-nueva_password_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'password');
      data.append('password', password);

      fetch('procesos/general_password_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, width = datos.width, color = datos.color;

        if(respuesta === "1" || respuesta === "3"){
          $('document, .g-restablecerPassword-nueva_password_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('document, .g-restablecerPassword-nueva_password_alert_info').slideDown('fast');
        }

        $('document, .g-nuevaPassword-porcentaje_seguridad').text(width);
        $('document, .g-nuevaPassword-barra_progreso').css({
          "width": width,
          "background-color": color
        });
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-restablecerPassword-nueva_password_alert_info').slideUp('fast');
      $('document, .g-nuevaPassword-porcentaje_seguridad').text('0%');
      $('document, .g-nuevaPassword-barra_progreso').css({
        "width": "0%",
        "background-color": "#D9534F"
      });
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Confirmar password"
  */
  $(document).on('keyup', '.g-restablecerPassword-confirmar_password_input', function(){
    let password = $(this).val(), data = new FormData();
    
    if(password.length !== 0){
      $('document, .g-restablecerPassword-confirmar_password_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', password);

      fetch('validaciones/validar_password.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-restablecerPassword-confirmar_password_alert_info').slideUp('fast') : $('document, .g-restablecerPassword-confirmar_password_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-restablecerPassword-confirmar_password_alert_info').slideUp('fast');
    }
  });
  /*
  * Restablecer password - Acciones al enviar la informacion
  */
  $(document).on('submit', '.g-restablecerPassword-password_form', function(e){
    let nueva_password = $('document, .g-restablecerPassword-nueva_password_input').val(), confirmar_password = $('document, .g-restablecerPassword-confirmar_password_input').val(), correo = $('document, .g-restablecerPassword-correo_input').val(), token = $('document, .g-restablecerPassword-token_input').val(), data = new FormData();

    $('document, .g-restablecerPassword-notification_contenedor').slideUp('fast');
    $('document, .g-restablecerPassword-correo_alert_info').slideUp('fast');
    $('document, .g-restablecerPassword-loading').slideDown('fast');
    $('document, .g-restablecerPassword-nueva_password_input').prop('disabled', true);
    $('document, .g-restablecerPassword-confirmar_password_input').prop('disabled', true);
    $('document, .g-restablecerPassword-restablecer_button').prop('type', 'button');
    $('document, .g-restablecerPassword-restablecer_button').prop('disabled', true);

    data.append('accion', 'restablecer');
    data.append('nueva_password', nueva_password);
    data.append('confirmar_password', confirmar_password);
    data.append('correo', correo);
    data.append('token', token);

    fetch('procesos/usuario/proceso_restablecerPassword_cambiarDatos.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      $('document, .g-restablecerPassword-loading').slideUp('fast');

      setTimeout(function(){
        $('document, .g-restablecerPassword-nueva_password_input').prop('disabled', false);
        $('document, .g-restablecerPassword-confirmar_password_input').prop('disabled', false);
        $('document, .g-restablecerPassword-restablecer_button').prop('type', 'submit');
        $('document, .g-restablecerPassword-restablecer_button').prop('disabled', false);
      }, 400);

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('document, .g-restablecerPassword-nueva_password_alert_error').slideDown('fast');
          $('document, .g-restablecerPassword-nueva_password_input').addClass('p-input_error');
          
          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-restablecerPassword-nueva_password_input').focus(); }, 400);
          }
          break;

        case '2':
          $('document, .g-restablecerPassword-nueva_password_alert_info').slideDown('fast');
          
          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-restablecerPassword-nueva_password_input').focus(); }, 400);
          }
          break;

        case '3':
          $('document, .g-restablecerPassword-confirmar_password_alert_error').slideDown('fast');
          $('document, .g-restablecerPassword-confirmar_password_input').addClass('p-input_error');
          
          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-restablecerPassword-confirmar_password_input').focus(); }, 400);
          }
          break;

        case '4':
          $('document, .g-restablecerPassword-confirmar_password_alert_info').slideDown('fast');
          
          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-restablecerPassword-confirmar_password_input').focus(); }, 400);
          }
          break;

        case '5':
          $('document, .g-restablecerPassword-contenedor_notification').html(mensaje);
          $('document, .g-restablecerPassword-notification_contenedor').slideDown('fast');
          
          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-restablecerPassword-nueva_password_input').focus(); }, 400);
          }
          break;

        case '6':
          let segundo = 5, texto = "", contador;
          $('document, .g-restablecerPassword-contenedor_notification').html(mensaje);
          $('document, .g-restablecerPassword-notification_contenedor').slideDown('fast');
          
          setTimeout(function(){
            $('document, .g-restablecerPassword-nueva_password_input').prop('disabled', true);
            $('document, .g-restablecerPassword-confirmar_password_input').prop('disabled', true);
            $('document, .g-restablecerPassword-restablecer_button').prop('type', 'button');
            $('document, .g-restablecerPassword-restablecer_button').prop('disabled', true);
          }, 600);

          contador = setInterval(function(){
            segundo--;
            texto = segundo === 1 ? String(segundo).concat(' segundo') : String(segundo).concat(' segundos');
            $('document, .g-contenedor-tiempo_inicio').text(texto);

            if(segundo === 0){
              clearInterval(contador);
              location.href = "iniciar-sesion";
            }
          }, 1000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
});