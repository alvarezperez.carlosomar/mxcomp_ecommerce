$(document).ready(function(){
  mostrar_elementos();
  /*
  * Carrito - Al dar click en una opcion del acordeon
  */
  $(document).on('click', '.g-acordeon_cabecera', function(){
    let seccion = $(this).attr('data-seccion');
  
    $(this).toggleClass('p-acordeon-cabecera_activa');
  
    ( $('document, div[data-contenido=' + seccion + ']').css('display') === "none" ) ? $('document, div[data-contenido=' + seccion + ']').slideDown('fast') : $('document, div[data-contenido=' + seccion + ']').slideUp('fast');
  });
  /*
  * Carrito - Click en el campo de cantidad
  */
  $(document).on('click', '.g-carrito-cambiar_cantidad_input', function(){
    $(this).select();
  });
  /*
  * Carrito - Obtener la cantidad escrita
  */
  var timeEjecucion = "";
  $(document).on('keyup', '.g-carrito-cambiar_cantidad_input', function(){
    let seccion = $(this).attr('data-seccion'), id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-carrito-codigoProducto_' + id).val(), cantidad = $(this).val();

    if(cantidad.length !== 0){
      clearTimeout(timeEjecucion);

      timeEjecucion = setTimeout(function(){
        enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad);
        clearTimeout(timeEjecucion);
      }, 800);
    }
  });
  /*
  * Carrito - Boton para disminuir/aumentar la cantidad
  */
  $(document).on('click', '.g-carrito-cambiar_cantidad_boton', function(){
    let seccion = $(this).attr('data-seccion'), id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-carrito-codigoProducto_' + id).val(), cantidad = $('document, .g-carrito-cambiar_cantidad_input[data-id=' + id + ']').val();

    if(cantidad.length !== 0){
      enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad);
    }
  });
  /*
  * Carrito - Al dar click en el icono de opciones de producto
  */
  $(document).on('click', '.g-carrito-product_opciones_button', function(){
    let id = $(this).attr('id');

    $('document, .' + id).slideToggle(100);
    $('document, #' + id + "_fondo").slideToggle(100);
  });
  /*
  * Carrito - Al dar click en el fondo de las opciones de un producto
  */
  $(document).on('click', '.g-carrito-product_opciones_fondo', function(){
    let id = $(this).attr('id').replace('_fondo', '');

    $('document, .' + id).slideToggle(100);
    $(this).slideToggle(100);
  });
  /*
  * Carrito - Guarda/agrega el producto al carrito
  */
  $(document).on('click', '.g-carrito-producto_guardado', function(){
    let opcion = $(this).attr('data-opcion'), id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-carrito-codigoProducto_' + id).val(), data = new FormData();
    
    data.append('accion', 'cambiar');
    data.append('opcion', opcion);
    data.append('id', id);
    data.append('almacen', almacen);
    data.append('codigo_producto', codigo_producto);

    fetch('procesos/carrito/cambiar_estado_guardado.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '1':
          console.log(mensaje);
          $('document, .g-carrito-producto_guardado[data-id=' + id + ']').next().slideDown('fast');
          setTimeout(function(){ location.reload(); }, 5000);
          break;

        case '2':
          $('document, #id-carrito-product_opciones_' + id + '_fondo').slideToggle(100);
          $('document, .id-carrito-product_opciones_' + id).slideToggle(100);
          $('document, #id-carrito-product_titulo_' + id).toggleClass('p-carrito-div_product_titulo_opacity');
          $('document, #id-carrito-product_info_' + id).toggleClass('p-carrito-div_product_info_opacity');
          $('document, #id-carrito-product_info_' + id).slideToggle('fast');
          setTimeout(function(){ mostrar_elementos(); }, 100);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Carrito - Boton para eliminar el producto
  */
  var temp_notificacion_eliminacion;
  $(document).on('click', '.g-carrito-eliminar_producto', function(){
    let id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-carrito-codigoProducto_' + id).val(), data = new FormData();
    
    data.append('accion', 'eliminar');
    data.append('id', id);
    data.append('almacen', almacen);
    data.append('codigo_producto', codigo_producto);

    clearTimeout(temp_notificacion_eliminacion);
    $('document, #id-carrito_status_error_div').removeClass('p-notification_fixed_abled');
    $('document, #id-carrito_status_error').slideUp('fast');

    fetch('procesos/carrito/eliminar_producto.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje, ultimo_producto = datos.ultimo_producto;

      switch(respuesta){
        case '1':
          console.log(mensaje);
          $('document, .g-carrito-eliminar_producto[data-id=' + id + ']').next().slideDown('fast');
          setTimeout(function(){ location.reload(); }, 5000);
          break;

        case '2':
          $('document, #id-carrito-product_' + id).slideUp('fast');

          if(ultimo_producto === "1"){
            let seccion_id = id.split('_');
            $('document, #g-carrito-acordeon_almacen_' + seccion_id).slideUp('fast');
            $('document, .g-carrito-acordeon_almacen_' + seccion_id + '_contenido').slideUp('fast');
          }
  
          $('document, #id-carrito_status_error').slideDown('fast');
          $('document, #id-carrito_status_error_div').addClass('p-notification_fixed_abled');

          temp_notificacion_eliminacion = setTimeout(() => {
            $('document, #id-carrito_status_error').slideUp('fast');
            $('document, #id-carrito_status_error_div').removeClass('p-notification_fixed_abled');
          }, 2000);

          setTimeout(function(){ mostrar_elementos(); }, 400);
          $('document, #id-carrito-product_opciones_' + id + '_fondo').slideToggle(100);
          $('document, .id-carrito-product_opciones_' + id).slideToggle(100);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Carrito - Boton para ir a seccion de comprar
  */
  $(document).on('click', '#id-carrito-button_comprar', function() {
    let data = new FormData();
    
    data.append('seccion', 'carrito');
    
    fetch('procesos/comprar/seleccion_seccionComprar.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;
      
      if(respuesta === '1'){
        //setTimeout(function(){ window.location = "compra-producto"; }, 500);
        setTimeout(function(){ window.location = "confirmar-compra/" + mensaje; }, 500);
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Carrito - Al dar click en boton de abrir modal
  */
  $(document).on('click', '#id-carrito-resumen_button', function(){
    $('html').toggleClass('p-modal-derecho_window_no_scroll_carrito');
    $('document, #id-carrito-resumen_contenedor').slideToggle('fast');
    $('document, .p-modal-derecho_contenedor_contenido').toggleClass('p-modal-derecho_contenedor_activo');
  });
  /*
  * Carrito - Al dar click para cerrar el modal
  */
  $(document).on('click', '.g-carrito-resumen_cerrar', function(){
    $('html').toggleClass('p-modal-derecho_window_no_scroll_carrito');
    $('document, #id-carrito-resumen_contenedor').slideToggle('fast');
    $('document, .p-modal-derecho_contenedor_contenido').toggleClass('p-modal-derecho_contenedor_activo');
  });
  /*
  * Funcion - Mandar la cantidad a cambiar
  */
  function enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad){
    $('document, .g-carrito-cantidad_loading[data-id=' + id + ']').slideDown('fast');
    $('document, .g-carrito-cambiar_cantidad_input').prop('disabled', true);
    $('document, .g-carrito-cambiar_cantidad_boton').prop('disabled', true);
    $('document, #id-carrito-button_comprar').prop('disabled', true);

    let data = new FormData();

    data.append('accion', 'cambiar');
    data.append('seccion', seccion);
    data.append('id', id);
    data.append('almacen', almacen);
    data.append('codigo_producto', codigo_producto);
    data.append('cantidad', cantidad);

    fetch('procesos/carrito/cambiar_cantidad_unidades.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '1':
          console.log(mensaje);
          break;

        case '2':
          mostrar_elementos();
          break;

        case '3':
          setTimeout(function(){
            mostrar_elementos();
            $('document, .g-carrito-cantidad_loading[data-id=' + id + ']').slideUp('fast');
            $('document, .g-carrito-cambiar_cantidad_input').prop('disabled', false);
            $('document, .g-carrito-cambiar_cantidad_boton').prop('disabled', false);
            $('document, #id-carrito-button_comprar').prop('disabled', false);
          }, 200);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  }
  /*
  * Funcion - Mostrar los elementos del carrito
  */
  function mostrar_elementos(){
    let data = new FormData();

    data.append('accion', 'mostrar');

    fetch('procesos/carrito/mostrar_elementos.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, html_productos = datos.html_productos, html_resumen = datos.html_resumen, boton_miCarrito = datos.boton_miCarrito, boton_resumen = datos.boton_resumen;

      switch(respuesta){
        case '1':
          $('document, #id-carrito-contenedor_elementos').html(html_productos);
          $('document, #id-carrito-contenedor_resumen_1').html(html_resumen);
          $('document, #id-carrito-contenedor_resumen_2').html(html_resumen);
          $('document, #id-carrito-contenedor_button_resumen').html(boton_resumen);
          $('document, .p-footer').addClass('p-carrito-footer');
          break;
          
        case '2':
          $('document, #id-carrito-contenedor_info').html(html_productos);
          $('document, #id-carrito-contenedor_resumen_2').html(html_resumen);
          $('document, .p-footer').removeClass('p-carrito-footer');
          break;
          
        case '3':
          console.log(html_productos);
          break;
      }
      
      $('document, .g-navbar_carrito_cantidad').text(boton_miCarrito);
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  }
});