$(document).ready(function(){
  /*
  * Input - Detecta si se escribe en el campo "Nombre del destinatario (con apellidos)"
  */
  $(document).on('keyup', '.g-domiEnvio-nombre_destinatario_input', function(){
    let nombres_destinatario = $(this).val(), data = new FormData();
    
    if(nombres_destinatario.length !== 0){
      $('document, .g-domiEnvio-nombre_destinatario_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', nombres_destinatario);
      
      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-nombre_destinatario_alert_info').slideUp('fast') : $('document, .g-domiEnvio-nombre_destinatario_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-nombre_destinatario_alert_info').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Tipo de vialidad"
  */
  $(document).on('change', '.g-domiEnvio-tipo_vialidad_select', function(){
    let tipo_vialidad = $(this).val();
    
    $('document, .g-domiEnvio-tipo_vialidad_alert_info').slideUp('fast');
    
    if(tipo_vialidad.length !== 0){
      $('document, .g-domiEnvio-tipo_vialidad_alert_error').slideUp('fast');
      $('document, .g-domiEnvio-tipo_vialidad_div').removeClass('p-select_error');
    }else{
      $('document, .g-domiEnvio-tipo_vialidad_alert_error').slideDown('fast');
      $('document, .g-domiEnvio-tipo_vialidad_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Nombre de vialidad"
  */
  $(document).on('keyup', '.g-domiEnvio-nombre_vialidad_input', function(){
    let nombre_vialidad = $(this).val(), data = new FormData();
    
    if(nombre_vialidad.length !== 0){
      $('document, .g-domiEnvio-nombre_vialidad_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', nombre_vialidad);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-nombre_vialidad_alert_info').slideUp('fast') : $('document, .g-domiEnvio-nombre_vialidad_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-nombre_vialidad_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. exterior"
  */
  $(document).on('keyup', '.g-domiEnvio-no_exterior_input', function(){
    let no_exterior = $(this).val(), data = new FormData();
    
    if(no_exterior.length !== 0){
      $('document, .g-domiEnvio-no_exterior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', no_exterior);
      
      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-no_exterior_alert_info').slideUp('fast') : $('document, .g-domiEnvio-no_exterior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-no_exterior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. interior"
  */
  $(document).on('keyup', '.g-domiEnvio-no_interior_input', function(){
    let no_interior = $(this).val(), data = new FormData();
    
    if(no_interior.length !== 0){
      $('document, .g-domiEnvio-no_interior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', no_interior);
      
      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-no_interior_alert_info').slideUp('fast') : $('document, .g-domiEnvio-no_interior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-no_interior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Codigo postal"
  */
  $(document).on('keyup', '.g-domiEnvio-codigo_postal_input', function(){
    let codigo_postal = $(this).val(), data = new FormData();
    
    if(codigo_postal.length !== 0){
      $('document, .g-domiEnvio-codigo_postal_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'generar');
      data.append('codigo_postal', codigo_postal);
      
      fetch('procesos/general_codigoPostal_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;
        
        if(respuesta === "0"){
          console.log(mensaje);
        }else if(respuesta === "1"){
          $('document, .g-domiEnvio-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-domiEnvio-codigo_postal_alert_info_2').slideUp('fast');
        }else if(respuesta === "2" || respuesta === "3" || respuesta === "4"){
          respuesta === "4" ? $('document, .g-domiEnvio-estado_select').val('') : $('document, .g-domiEnvio-estado_select').html(mensaje);
  
          if(codigo_postal.length === 5){
            $('document, .g-domiEnvio-codigo_postal_alert_info_2').slideUp('fast');
          }else{
            $('document, .g-domiEnvio-codigo_postal_alert_info_1').slideUp('fast');
            $('document, .g-domiEnvio-codigo_postal_alert_info_2').slideDown('fast');
          }
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-codigo_postal_alert_info_1').slideUp('fast');
      $('document, .g-domiEnvio-codigo_postal_alert_info_2').slideUp('fast');
      $('document, .g-domiEnvio-estado_select').val('');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Colonia"
  */
  $(document).on('keyup', '.g-domiEnvio-colonia_input', function(){
    let colonia = $(this).val(), data = new FormData();
    
    if(colonia.length !== 0){
      $('document, .g-domiEnvio-colonia_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', colonia);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-colonia_alert_info').slideUp('fast') : $('document, .g-domiEnvio-colonia_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-colonia_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Ciudad o Municipio"
  */
  $(document).on('keyup', '.g-domiEnvio-ciudad_municipio_input', function(){
    let ciudad_municipio = $(this).val(), data = new FormData();
    
    if(ciudad_municipio.length !== 0){
      $('document, .g-domiEnvio-ciudad_municipio_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', ciudad_municipio);
      
      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-ciudad_municipio_alert_info').slideUp('fast') : $('document, .g-domiEnvio-ciudad_municipio_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-ciudad_municipio_alert_info').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Estado"
  */
  $(document).on('change', '.g-domiEnvio-estado_select', function(){
    let estado = $(this).val();
    
    $('document, .g-domiEnvio-estado_alert_info').slideUp('fast');
    
    if(estado.length !== 0){
      $('document, .g-domiEnvio-estado_alert_error').slideUp('fast');
      $('document, .g-domiEnvio-estado_div').removeClass('p-select_error');
    }else{
      $('document, .g-domiEnvio-estado_alert_error').slideDown('fast');
      $('document, .g-domiEnvio-estado_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #1"
  */
  $(document).on('keyup', '.g-domiEnvio-entre_calles_1_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-domiEnvio-entre_calles_1_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-entre_calles_1_alert_info').slideUp('fast') : $('document, .g-domiEnvio-entre_calles_1_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-entre_calles_1_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #2"
  */
  $(document).on('keyup', '.g-domiEnvio-entre_calles_2_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-domiEnvio-entre_calles_2_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-domiEnvio-entre_calles_2_alert_info').slideUp('fast') : $('document, .g-domiEnvio-entre_calles_2_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-entre_calles_2_alert_info').slideUp('fast');
    }
  });
  /*
  * Textarea - Detecta si se teclea Enter en el campo "Referencias adicionales"
  */
  $(document).on('keydown', '.g-domiEnvio-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      return false;
    }
  });
  /*
  * Textarea - Detecta si se escribe en el campo "Referencias adicionales"
  */
  $(document).on('keyup', '.g-domiEnvio-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      $('document, .g-domiEnvio-guardar_cambios_button').click();
    }else{
      let referencias_adicionales = $(this).val(), data = new FormData();
      
      if(referencias_adicionales.length !== 0){
        data.append('accion', 'validar');
        data.append('valor', referencias_adicionales);

        fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);

          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;

          respuesta === '1' ? $('document, .g-domiEnvio-referencias_adicionales_alert_info').slideUp('fast') : $('document, .g-domiEnvio-referencias_adicionales_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-domiEnvio-referencias_adicionales_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefonico"
  */
  $(document).on('keyup', '.g-domiEnvio-no_telefonico_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      $('document, .g-domiEnvio-no_telefonico_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? (
          $('document, .g-domiEnvio-no_telefonico_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-domiEnvio-no_telefonico_alert_info_2').slideUp('fast') : $('document, .g-domiEnvio-no_telefonico_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-domiEnvio-no_telefonico_alert_info_1').slideDown('fast'),
          $('document, .g-domiEnvio-no_telefonico_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-domiEnvio-no_telefonico_alert_info_1').slideUp('fast');
      $('document, .g-domiEnvio-no_telefonico_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Time - Detecta si se desenfoca el campo "Rango horario de entrega #1"
  */
  $(document).on('blur', '.g-domiEnvio-rango_horario_entrega_1_time', function(){
    let hora_entrega_1 = $(this).val();
    
    if(hora_entrega_1.length !== 0){
      $('document, .g-domiEnvio-rango_horario_entrega_1_alert_info').slideUp('fast');
      $('document, .g-domiEnvio-rango_horario_entrega_1_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Time - Detecta si se escribe/selecciona en el campo "Rango horario de entrega #1"
  */
  $(document).on('keyup', '.g-domiEnvio-rango_horario_entrega_1_time', function(){
    let hora_entrega_1 = $(this).val();
    
    if(hora_entrega_1.length !== 0){
      $('document, .g-domiEnvio-rango_horario_entrega_1_alert_info').slideUp('fast');
      $('document, .g-domiEnvio-rango_horario_entrega_1_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Time - Detecta si se desenfoca el campo "Rango horario de entrega #2"
  */
  $(document).on('blur', '.g-domiEnvio-rango_horario_entrega_2_time', function(){
    let hora_entrega_2 = $(this).val();
    
    if(hora_entrega_2.length !== 0){
      $('document, .g-domiEnvio-rango_horario_entrega_2_alert_info').slideUp('fast');
      $('document, .g-domiEnvio-rango_horario_entrega_2_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Time - Detecta si se escribe/selecciona en el campo "Rango horario de entrega #2"
  */
  $(document).on('keyup', '.g-domiEnvio-rango_horario_entrega_2_time', function(){
    let hora_entrega_2 = $(this).val();
    
    if(hora_entrega_2.length !== 0){
      $('document, .g-domiEnvio-rango_horario_entrega_2_alert_info').slideUp('fast');
      $('document, .g-domiEnvio-rango_horario_entrega_2_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Editar domicilio de envio - Evento submit para formulario "Editar domicilio de envio"
  */
  $(document).on('submit', '#g-domiEnvio-form_editar', function(e){
    let nombre_destinatario = $('document, .g-domiEnvio-nombre_destinatario_input').val(),
        tipo_vialidad = $('document, .g-domiEnvio-tipo_vialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-domiEnvio-nombre_vialidad_input').val(),
        no_exterior = $('document, .g-domiEnvio-no_exterior_input').val(),
        no_exterior_estado_check = $('document, .g-domiEnvio-no_exterior_checkbox').prop('checked'),
        no_interior = $('document, .g-domiEnvio-no_interior_input').val(),
        no_interior_estado_check = $('document, .g-domiEnvio-no_interior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-domiEnvio-codigo_postal_input').val(),
        colonia = $('document, .g-domiEnvio-colonia_input').val(),
        ciudad_municipio = $('document, .g-domiEnvio-ciudad_municipio_input').val(),
        estado = $('document, .g-domiEnvio-estado_select option:selected').val(),
        entre_calle_1 = $('document, .g-domiEnvio-entre_calles_1_input').val(),
        entre_calle_2 = $('document, .g-domiEnvio-entre_calles_2_input').val(),
        referencias_adicionales = $('document, .g-domiEnvio-referencias_adicionales_textarea').val(),
        no_telefonico = $('document, .g-domiEnvio-no_telefonico_input').val(),
        horario_entrega_1 = $('document, .g-domiEnvio-rango_horario_entrega_1_time').val(),
        horario_entrega_2 = $('document, .g-domiEnvio-rango_horario_entrega_2_time').val(),
        data = new FormData();
    
    data.append('accion', 'editar');
    data.append('nombre_destinatario', nombre_destinatario);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estado_check', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estado_check', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('entre_calle_1', entre_calle_1);
    data.append('entre_calle_2', entre_calle_2);
    data.append('referencias_adicionales', referencias_adicionales);
    data.append('no_telefonico', no_telefonico);
    data.append('horario_entrega_1', horario_entrega_1);
    data.append('horario_entrega_2', horario_entrega_2);
    
    fetch('procesos/mis_datos/proceso_editar_domicilioEnvio.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.log(mensaje);
          break;

        case '1':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-nombre_destinatario_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-nombre_destinatario_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-nombre_destinatario_input').addClass('p-input_error');
          $('document, .g-domiEnvio-nombre_destinatario_input').focus();
          break;

        case '2':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-nombre_destinatario_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-nombre_destinatario_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-nombre_destinatario_input').focus();
          break;

        case '3':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-tipo_vialidad_select').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-tipo_vialidad_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-tipo_vialidad_div').addClass('p-select_error');
          $('document, .g-domiEnvio-tipo_vialidad_select').focus();
          break;

        case '4':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-tipo_vialidad_select').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-tipo_vialidad_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-tipo_vialidad_select').focus();
          break;

        case '5':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-nombre_vialidad_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-nombre_vialidad_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-nombre_vialidad_input').addClass('p-input_error');
          $('document, .g-domiEnvio-nombre_vialidad_input').focus();
          break;

        case '6':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-nombre_vialidad_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-nombre_vialidad_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-nombre_vialidad_input').focus();
          break;

        case '7':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_exterior_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_exterior_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-no_exterior_input').addClass('p-input_error');
          $('document, .g-domiEnvio-no_exterior_input').focus();
          break;

        case '8':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_exterior_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_exterior_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-no_exterior_input').focus();
          break;

        case '9':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_interior_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_interior_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-no_interior_input').addClass('p-input_error');
          $('document, .g-domiEnvio-no_interior_input').focus();
          break;

        case '10':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_interior_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_interior_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-no_interior_input').focus();
          break;

        case '11':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-codigo_postal_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-codigo_postal_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-codigo_postal_input').addClass('p-input_error');
          $('document, .g-domiEnvio-codigo_postal_input').focus();
          break;

        case '12':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-codigo_postal_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-domiEnvio-codigo_postal_input').focus();
          break;

        case '13':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-codigo_postal_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-codigo_postal_alert_info_2').slideDown('fast');
          $('document, .g-domiEnvio-codigo_postal_input').focus();
          break;

        case '14':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-colonia_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-colonia_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-colonia_input').addClass('p-input_error');
          $('document, .g-domiEnvio-colonia_input').focus();
          break;

        case '15':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-colonia_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-colonia_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-colonia_input').focus();
          break;

        case '16':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-ciudad_municipio_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-ciudad_municipio_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-ciudad_municipio_input').addClass('p-input_error');
          $('document, .g-domiEnvio-ciudad_municipio_input').focus();
          break;

        case '17':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-ciudad_municipio_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-ciudad_municipio_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-ciudad_municipio_input').focus();
          break;

        case '18':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-estado_select').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-estado_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-estado_div').addClass('p-select_error');
          $('document, .g-domiEnvio-estado_select').focus();
          break;

        case '19':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-estado_select').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-estado_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-estado_select').focus();
          break;

        case '20':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-entre_calles_1_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-entre_calles_1_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-entre_calles_1_input').addClass('p-input_error');
          $('document, .g-domiEnvio-entre_calles_1_input').focus();
          break;

        case '21':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-entre_calles_1_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-entre_calles_1_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-entre_calles_1_input').focus();
          break;

        case '22':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-entre_calles_2_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-entre_calles_2_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-entre_calles_2_input').addClass('p-input_error');
          $('document, .g-domiEnvio-entre_calles_2_input').focus();
          break;

        case '23':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-entre_calles_2_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-entre_calles_2_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-entre_calles_2_input').focus();
          break;

        case '24':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-referencias_adicionales_textarea').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-referencias_adicionales_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-referencias_adicionales_textarea').focus();
          break;

        case '25':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_telefonico_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_telefonico_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-no_telefonico_input').addClass('p-input_error');
          $('document, .g-domiEnvio-no_telefonico_input').focus();
          break;

        case '26':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_telefonico_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_telefonico_alert_info_1').slideDown('fast');
          $('document, .g-domiEnvio-no_telefonico_input').focus();
          break;

        case '27':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-no_telefonico_input').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-no_telefonico_alert_info_2').slideDown('fast');
          $('document, .g-domiEnvio-no_telefonico_input').focus();
          break;

        case '28':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-rango_horario_entrega_1_time').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-rango_horario_entrega_1_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-rango_horario_entrega_1_time').addClass('p-date_error');
          $('document, .g-domiEnvio-rango_horario_entrega_1_time').focus();
          break;

        case '29':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-rango_horario_entrega_1_time').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-rango_horario_entrega_1_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-rango_horario_entrega_1_time').focus();
          break;

        case '30':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-rango_horario_entrega_2_time').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-rango_horario_entrega_2_alert_error').slideDown('fast');
          $('document, .g-domiEnvio-rango_horario_entrega_2_time').addClass('p-date_error');
          $('document, .g-domiEnvio-rango_horario_entrega_2_time').focus();
          break;

        case '31':
          if(seccion === "mis_datos"){
            $('html, body').animate({ scrollTop: $('document, .g-domiEnvio-rango_horario_entrega_2_time').offset().top - 100 }, 600);
          }
          
          $('document, .g-domiEnvio-rango_horario_entrega_2_alert_info').slideDown('fast');
          $('document, .g-domiEnvio-rango_horario_entrega_2_time').focus();
          break;

        case '32':
          if(seccion === "mis_datos"){
            $("html, body").animate({ scrollTop: 0 }, 600);

            setTimeout(function(){ $('document, .g-domiEnvio-status').slideDown('fast'); }, 700);
            setTimeout(function(){ history.go(-1); }, 2000);
          }
          
          if(seccion === "comprar"){
            $('html').toggleClass('p-modal-derecho_window_no_scroll');
            $('#id-comprar-modal_general').slideToggle('fast');
            $('#id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
            $('#id-comprar-modal_general_contElementos').html('');
  
            setTimeout(function(){
              $('document, .g-domiEnvio-status').slideDown(400);
              $('document, #g-comprar-acordeon_3').removeClass('p-acordeon-cabecera_success');
              $('document, #T-comprar-domicilio_envio').val('');
              $('document, #id-comprar-confirmar_domicilio').slideDown('fast');
              mostrar_domicilioEnvio();
            }, 400);

            setTimeout(function(){ $('document, .g-domiEnvio-status').slideUp(400); }, 3000);
          }
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
  /*
  * Domicilio de envio - Funcion para mostrar domicilio
  */
  function mostrar_domicilioEnvio(){
    let data = new FormData();

    data.append('accion', 'mostrar');

    fetch('procesos/comprar/mostrar_domicilioEnvio.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);

      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      if(respuesta === '1'){
        $('document, .g-comprar-mostrar_domEnvio').html(mensaje);
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  }
});