$(document).ready(function(){
  /*
  * Input - Detecta si se escribe en el campo "Contrasenia actual"
  */
  $(document).on('keyup', '.g-cambPass-passActual_input', function(e){
    if(e.keyCode !== 13){
      let password = $(this).val(), data = new FormData();
      
      if(password.length !== 0){
        $('document, .g-cambPass-passActual_alert_error_1').slideUp('fast');
        $('document, .g-cambPass-passActual_alert_error_2').slideUp('fast');
        $(this).removeClass('p-input_error');

        data.append('accion', 'validar');
        data.append('valor', password);

        fetch('validaciones/validar_password.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);

          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;
          
          respuesta === '1' ? $('document, .g-cambPass-passActual_alert_info').slideUp('fast') : $('document, .g-cambPass-passActual_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-cambPass-passActual_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Nueva contrasenia"
  */
  $(document).on('keyup', '.g-cambPass-passNueva_input', function(){
    let password = $(this).val(), confirmar_password = $('document, .g-cambPass-confPassNueva_input').val(), data = new FormData();
    
    if(password.length !== 0){
      $('document, .g-cambPass-passNueva_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'password');
      data.append('password', password);

      fetch('procesos/general_password_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, width = datos.width, color = datos.color;

        respuesta === "1" || respuesta === "3" ? $('document, .g-cambPass-passNueva_alert_info').slideUp('fast') : $('document, .g-cambPass-passNueva_alert_info').slideDown('fast');
        
        $('document, .g-passNueva-porcentaje_seguridad').text(width);
        $('document, .g-passNueva-barra_progreso').css({
          "width": width,
          "background-color": color
        });
      }).catch(error => {
        console.error('[Error] - ', error);
      });

      if(confirmar_password !== ""){
        password === confirmar_password ? $('document, .g-cambPass-confPassNueva_alert_info_2').slideUp('fast') : $('document, .g-cambPass-confPassNueva_alert_info_2').slideDown('fast');
      }
    }else{
      $('document, .g-cambPass-passNueva_alert_info').slideUp('fast');
      $('document, .g-passNueva-porcentaje_seguridad').text('0%');
      $('document, .g-passNueva-barra_progreso').css({
        "width": "0%",
        "background-color": "#D9534F"
      });
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Confirmar nueva contrasenia"
  */
  $(document).on('keyup', '.g-cambPass-confPassNueva_input', function(){
    let password = $('document, .g-cambPass-passNueva_input').val(), confirmar_password = $(this).val(), data = new FormData();
    
    if(confirmar_password.length != 0){
      $('document, .g-cambPass-confPassNueva_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', confirmar_password);

      fetch('validaciones/validar_password.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-cambPass-confPassNueva_alert_info_1').slideUp('fast') : $('document, .g-cambPass-confPassNueva_alert_info_1').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });

      password === confirmar_password ? $('document, .g-cambPass-confPassNueva_alert_info_2').slideUp('fast') : $('document, .g-cambPass-confPassNueva_alert_info_2').slideDown('fast');
    }else{
      $('document, .g-cambPass-confPassNueva_alert_info_1').slideUp('fast');
      $('document, .g-cambPass-confPassNueva_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Submit - Evento submit para formulario "Cambiar contraseña"
  */
  $(document).on('submit', '.g-cambPass-form', function(e){
    let password_actual = $('document, .g-cambPass-passActual_input').val(), password_nueva = $('document, .g-cambPass-passNueva_input').val(), confirmar_password_nueva = $('document, .g-cambPass-confPassNueva_input').val(), data = new FormData();
    
    data.append('accion', 'modificar');
    data.append('password_actual', password_actual);
    data.append('password_nueva', password_nueva);
    data.append('confirmar_password_nueva', confirmar_password_nueva);

    fetch('procesos/mis_datos/proceso_cambiar_password.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-passActual_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-passActual_alert_error_2').slideDown('fast');
          $('document, .g-cambPass-passActual_input').addClass('p-input_error');
          $('document, .g-cambPass-passActual_input').focus();
          break;

        case '2':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-passActual_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-passActual_alert_info').slideDown('fast');
          $('document, .g-cambPass-passActual_input').focus();
          break;

        case '3':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-passNueva_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-passNueva_alert_error').slideDown('fast');
          $('document, .g-cambPass-passNueva_input').addClass('p-input_error');
          $('document, .g-cambPass-passNueva_input').focus();
          break;

        case '4':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-passNueva_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-passNueva_alert_info').slideDown('fast');
          $('document, .g-cambPass-passNueva_input').focus();
          break;

        case '5':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-confPassNueva_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-confPassNueva_alert_error').slideDown('fast');
          $('document, .g-cambPass-confPassNueva_input').addClass('p-input_error');
          $('document, .g-cambPass-confPassNueva_input').focus();
          break;

        case '6':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-confPassNueva_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-confPassNueva_alert_info_1').slideDown('fast');
          $('document, .g-cambPass-confPassNueva_input').focus();
          break;

        case '7':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-confPassNueva_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-confPassNueva_alert_info_2').slideDown('fast');
          $('document, .g-cambPass-confPassNueva_input').focus();
          break;

        case '8':
          $('html, body').animate({ scrollTop: $('document, .g-cambPass-passActual_input').offset().top - 100 }, 600);
          $('document, .g-cambPass-passActual_alert_error_1').slideDown('fast');
          $('document, .g-cambPass-passActual_input').addClass('p-input_error');
          $('document, .g-cambPass-passActual_input').focus();
          break;

        case '9':
          $('html, body').animate({ scrollTop: 0 }, 600);
          $('document, .g-cambPass-passActual_input').val('');
          $('document, .g-cambPass-passActual_input').prop('disabled', true);
          $('document, .g-cambPass-passNueva_input').val('');
          $('document, .g-cambPass-passNueva_input').prop('disabled', true);
          $('document, .g-cambPass-confPassNueva_input').val('');
          $('document, .g-cambPass-confPassNueva_input').prop('disabled', true);
          $('document, .g-cambPass-guardar_cambios_button').prop('disabled', true);
          $('document, #id-contenedor-aviso').html(mensaje);
          $('document, .g-aviso-contenedor_mensaje').slideDown('fast');
    
          let segundo = 5, texto = '', contador;
          
          contador = setInterval(function(){
            segundo--;
            texto = segundo === 1 ? String(segundo).concat(' segundo') : String(segundo).concat(' segundos');
            $('document, .g-contenedor-tiempo_span').text(texto);
            
            if(segundo === 0){
              clearInterval(contador);
              $('document, .g-navbar-cerrar_sesion').click();
            }
          }, 1000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
});