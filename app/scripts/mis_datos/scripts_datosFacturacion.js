$(document).ready(function(){
  /*
  * Input - Detecta si se escribe en el campo "Nombre o razón social"
  */
  $(document).on('keyup', '.g-facturacion-nombreRazonSocial_input', function(){
    let nombres = $(this).val(), data = new FormData();

    if(nombres.length !== 0){
      $('document, .g-facturacion-nombreRazonSocial_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombres);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-facturacion-nombreRazonSocial_alert_info').slideUp('fast') : $('document, .g-facturacion-nombreRazonSocial_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      })
    }else{
      $('document, .g-facturacion-nombreRazonSocial_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "RFC"
  */
  $(document).on('keyup', '.g-facturacion-RFC_input', function(){
    let RFC = $(this).val(), data = new FormData();
    
    if(RFC.length !== 0){
      $('document, .g-facturacion-RFC_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', RFC.toUpperCase());

      fetch('validaciones/validar_RFC.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-facturacion-RFC_alert_info_1').slideUp('fast'),
          RFC.length < 13 ? $('document, .g-facturacion-RFC_alert_info_2').slideDown('fast') : $('document, .g-facturacion-RFC_alert_info_2').slideUp('fast')
        ) : (
          $('document, .g-facturacion-RFC_alert_info_1').slideDown('fast'),
          $('document, .g-facturacion-RFC_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-RFC_alert_info_1').slideUp('fast');
      $('document, .g-facturacion-RFC_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Uso de la factura"
  */
  $(document).on('change', '.g-facturacion-usoFactura_select', function(){
    let uso_fac = $(this).val();
    
    $('document, .g-facturacion-usoFactura_alert_info').slideUp('fast');
    
    if(uso_fac.length !== 0){
      $('document, .g-facturacion-usoFactura_alert_error').slideUp('fast');
      $('document, .g-facturacion-usoFactura_div').removeClass('p-select_error');
    }else{
      $('document, .g-facturacion-usoFactura_alert_error').slideDown('fast');
      $('document, .g-facturacion-usoFactura_div').addClass('p-select_error');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Forma de pago"
  */
  $(document).on('change', '.g-facturacion-formaPago_select', function(){
    let forma_pago = $(this).val();
    
    $('document, .g-facturacion-formaPago_alert_info').slideUp('fast');
    
    if(forma_pago.length !== 0){
      $('document, .g-facturacion-formaPago_alert_error').slideUp('fast');
      $('document, .g-facturacion-formaPago_div').removeClass('p-select_error');
    }else{
      $('document, .g-facturacion-formaPago_alert_error').slideDown('fast');
      $('document, .g-facturacion-formaPago_div').addClass('p-select_error');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Tipo de vialidad"
  */
  $(document).on('change', '.g-facturacion-tipoVialidad_select', function(){
    let tipo_vialidad = $(this).val();
    
    $('document, .g-facturacion-tipoVialidad_alert_info').slideUp('fast');
    
    if(tipo_vialidad.length !== 0){
      $('document, .g-facturacion-tipoVialidad_alert_error').slideUp('fast');
      $('document, .g-facturacion-tipoVialidad_div').removeClass('p-select_error');
    }else{
      $('document, .g-facturacion-tipoVialidad_alert_error').slideDown('fast');
      $('document, .g-facturacion-tipoVialidad_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Nombre de vialidad"
  */
  $(document).on('keyup', '.g-facturacion-nombreVialidad_input', function(){
    let nombre_vialidad = $(this).val(), data = new FormData();
    
    if(nombre_vialidad.length !== 0){
      $('document, .g-facturacion-nombreVialidad_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_vialidad);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-facturacion-nombreVialidad_alert_info').slideUp('fast') : $('document, .g-facturacion-nombreVialidad_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-nombreVialidad_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "No. exterior"
  */
  $(document).on('keyup', '.g-facturacion-noExterior_input', function(){
    let no_exterior = $(this).val(), data = new FormData();
    
    if(no_exterior.length !== 0){
      $('document, .g-facturacion-noExterior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_exterior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-facturacion-noExterior_alert_info').slideUp('fast') : $('document, .g-facturacion-noExterior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-noExterior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "No. interior"
  */
  $(document).on('keyup', '.g-facturacion-noInterior_input', function(){
    let no_interior = $(this).val(), data = new FormData();
    
    if(no_interior.length !== 0){
      $('document, .g-facturacion-noInterior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_interior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-facturacion-noInterior_alert_info').slideUp('fast') : $('document, .g-facturacion-noInterior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-noInterior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Codigo postal"
  */
  $(document).on('keyup', '.g-facturacion-codigoPostal_input', function(){
    let codigo_postal = $(this).val(), data = new FormData();
    
    if(codigo_postal.length !== 0){
      $('document, .g-facturacion-codigoPostal_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'generar');
      data.append('codigo_postal', codigo_postal);

      fetch('procesos/general_codigoPostal_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;
        
        if(respuesta === "0"){
          console.log(mensaje);
        }else if(respuesta === "1"){
          $('document, .g-facturacion-codigoPostal_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_alert_info_2').slideUp('fast');
        }else if(respuesta === "2" || respuesta === "3" || respuesta === "4"){
          respuesta === "4" ? $('document, .g-facturacion-estado_select').val('') : $('document, .g-facturacion-estado_select').html(mensaje);
          
          if(codigo_postal.length === 5){
            $('document, .g-facturacion-codigoPostal_alert_info_2').slideUp('fast');
          }else{
            $('document, .g-facturacion-codigoPostal_alert_info_1').slideUp('fast');
            $('document, .g-facturacion-codigoPostal_alert_info_2').slideDown('fast');
          }
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-codigoPostal_alert_info_1').slideUp('fast');
      $('document, .g-facturacion-codigoPostal_alert_info_2').slideUp('fast');
      $('document, .g-facturacion-estado_select').val('');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Colonia"
  */
  $(document).on('keyup', '.g-facturacion-colonia_input', function(){
    let colonia = $(this).val(), data = new FormData();
    
    if(colonia.length != 0){
      $('document, .g-facturacion-colonia_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', colonia);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-facturacion-colonia_alert_info').slideUp('fast') : $('document, .g-facturacion-colonia_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-colonia_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Ciudad o municipio"
  */
  $(document).on('keyup', '.g-facturacion-ciudadMunicipio_input', function(){
    let ciudad_municipio = $(this).val(), data = new FormData();
    
    if(ciudad_municipio.length != 0){
      $('document, .g-facturacion-ciudadMunicipio_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', ciudad_municipio);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-facturacion-ciudadMunicipio_alert_info').slideUp('fast') : $('document, .g-facturacion-ciudadMunicipio_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-ciudadMunicipio_alert_info').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Estado"
  */
  $(document).on('change', '.g-facturacion-estado_select', function(){
    let estado = $(this).val();
    
    $('document, .g-facturacion-estado_alert_info').slideUp('fast');
    
    if(estado.length !== 0){
      $('document, .g-facturacion-estado_alert_error').slideUp('fast');
      $('document, .g-facturacion-estado_div').removeClass('p-select_error');
    }else{
      $('document, .g-facturacion-estado_alert_error').slideDown('fast');
      $('document, .g-facturacion-estado_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Correo electrónico para envío de factura"
  */
  $(document).on('keyup', '.g-facturacion-correoFactura_input', function(){
    let correo_factura = $(this).val(), data = new FormData();
    
    if(correo_factura.length !== 0){
      $('document, .g-facturacion-correoFactura_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', correo_factura);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-facturacion-correoFactura_alert_info').slideUp('fast') : $('document, .g-facturacion-correoFactura_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-correoFactura_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico"
  */
  $(document).on('keyup', '.g-facturacion-noTelefonico_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();

    if(no_telefonico.length !== 0){
      $('document, .g-facturacion-noTelefonico_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? (
          $('document, .g-facturacion-noTelefonico_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-facturacion-noTelefonico_alert_info_2').slideUp('fast') : $('document, .g-facturacion-noTelefonico_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-facturacion-noTelefonico_alert_info_1').slideDown('fast'),
          $('document, .g-facturacion-noTelefonico_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-facturacion-noTelefonico_alert_info_1').slideUp('fast');
      $('document, .g-facturacion-noTelefonico_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Submit - Envia la informacion para agregar los datos de facturacion
  */
  $(document).on('submit', '#id-facturacion-form_agregar', function(e){
    let nombre_razon_social = $('document, .g-facturacion-nombreRazonSocial_input').val(),
        RFC = $('document, .g-facturacion-RFC_input').val().toUpperCase(),
        uso_factura = $('document, .g-facturacion-usoFactura_select option:selected').val(),
        tipo_vialidad = $('document, .g-facturacion-tipoVialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-facturacion-nombreVialidad_input').val(),
        no_exterior = $('document, .g-facturacion-noExterior_input').val(),
        no_exterior_estado_check = $('document, .g-facturacion-noExterior_checkbox').prop('checked'),
        no_interior = $('document, .g-facturacion-noInterior_input').val(),
        no_interior_estado_check = $('document, .g-facturacion-noInterior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-facturacion-codigoPostal_input').val(),
        colonia = $('document, .g-facturacion-colonia_input').val(),
        ciudad_municipio = $('document, .g-facturacion-ciudadMunicipio_input').val(),
        estado = $('document, .g-facturacion-estado_select option:selected').val(),
        correo_factura = $('document, .g-facturacion-correoFactura_input').val(),
        no_telefonico = $('document, .g-facturacion-noTelefonico_input').val(),
        data = new FormData();

    data.append('accion', 'agregar');
    data.append('nombre_razon_social', nombre_razon_social);
    data.append('RFC', RFC);
    data.append('uso_factura', uso_factura);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estado_check', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estado_check', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('correo_factura', correo_factura);
    data.append('no_telefonico', no_telefonico);
    
    fetch('procesos/mis_datos/proceso_agregar_datosFacturacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreRazonSocial_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreRazonSocial_alert_error').slideDown('fast');
          $('document, .g-facturacion-nombreRazonSocial_input').addClass('p-input_error');
          $('document, .g-facturacion-nombreRazonSocial_input').focus();
          break;

        case '2':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreRazonSocial_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreRazonSocial_alert_info').slideDown('fast');
          $('document, .g-facturacion-nombreRazonSocial_input').focus();
          break;

        case '3':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_error').slideDown('fast');
          $('document, .g-facturacion-RFC_input').addClass('p-input_error');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '4':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '5':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '6':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-usoFactura_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-usoFactura_alert_error').slideDown('fast');
          $('document, .g-facturacion-usoFactura_div').addClass('p-select_error');
          $('document, .g-facturacion-usoFactura_select').focus();
          break;

        case '7':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-usoFactura_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-usoFactura_alert_info').slideDown('fast');
          $('document, .g-facturacion-usoFactura_select').focus();
          break;

        case '8':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-tipoVialidad_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-tipoVialidad_alert_error').slideDown('fast');
          $('document, .g-facturacion-tipoVialidad_div').addClass('p-select_error');
          $('document, .g-facturacion-tipoVialidad_select').focus();
          break;

        case '9':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-tipoVialidad_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-tipoVialidad_alert_info').slideDown('fast');
          $('document, .g-facturacion-tipoVialidad_select').focus();
          break;

        case '10':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreVialidad_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreVialidad_alert_error').slideDown('fast');
          $('document, .g-facturacion-nombreVialidad_input').addClass('p-input_error');
          $('document, .g-facturacion-nombreVialidad_input').focus();
          break;

        case '11':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreVialidad_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreVialidad_alert_info').slideDown('fast');
          $('document, .g-facturacion-nombreVialidad_input').focus();
          break;

        case '12':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noExterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noExterior_alert_error').slideDown('fast');
          $('document, .g-facturacion-noExterior_input').addClass('p-input_error');
          $('document, .g-facturacion-noExterior_input').focus();
          break;

        case '13':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noExterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noExterior_alert_info').slideDown('fast');
          $('document, .g-facturacion-noExterior_input').focus();
          break;

        case '14':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noInterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noInterior_alert_error').slideDown('fast');
          $('document, .g-facturacion-noInterior_input').addClass('p-input_error');
          $('document, .g-facturacion-noInterior_input').focus();
          break;

        case '15':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noInterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noInterior_alert_info').slideDown('fast');
          $('document, .g-facturacion-noInterior_input').focus();
          break;

        case '16':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_error').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').addClass('p-input_error');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '17':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '18':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '19':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-colonia_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-colonia_alert_error').slideDown('fast');
          $('document, .g-facturacion-colonia_input').addClass('p-input_error');
          $('document, .g-facturacion-colonia_input').focus();
          break;

        case '20':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-colonia_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-colonia_alert_info').slideDown('fast');
          $('document, .g-facturacion-colonia_input').focus();
          break;

        case '21':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-ciudadMunicipio_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-ciudadMunicipio_alert_error').slideDown('fast');
          $('document, .g-facturacion-ciudadMunicipio_input').addClass('p-input_error');
          $('document, .g-facturacion-ciudadMunicipio_input').focus();
          break;

        case '22':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-ciudadMunicipio_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-ciudadMunicipio_alert_info').slideDown('fast');
          $('document, .g-facturacion-ciudadMunicipio_input').focus();
          break;

        case '23':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-estado_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-estado_alert_error').slideDown('fast');
          $('document, .g-facturacion-estado_div').addClass('p-select_error');
          $('document, .g-facturacion-estado_select').focus();
          break;

        case '24':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-estado_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-estado_alert_info').slideDown('fast');
          $('document, .g-facturacion-estado_select').focus();
          break;

        case '25':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-correoFactura_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-correoFactura_alert_error').slideDown('fast');
          $('document, .g-facturacion-correoFactura_input').addClass('p-input_error');
          $('document, .g-facturacion-correoFactura_input').focus();
          break;

        case '26':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-correoFactura_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-correoFactura_alert_info').slideDown('fast');
          $('document, .g-facturacion-correoFactura_input').focus();
          break;

        case '27':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_error').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').addClass('p-input_error');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '28':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '29':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '30':
          $('html, body').animate({ scrollTop: 0 }, 600);
          setTimeout(function(){ $('document, .g-facturacion-status').slideDown('fast'); }, 700);
          setTimeout(function(){ history.go(-1); }, 2000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });

    e.preventDefault();
  });
  /*
  * Submit - Envia la informacion para editar los datos de facturacion
  */
  $(document).on('submit', '#id-facturacion-form_editar', function(e){
    let nombre_razon_social = $('document, .g-facturacion-nombreRazonSocial_input').val(),
        RFC = $('document, .g-facturacion-RFC_input').val().toUpperCase(),
        uso_factura = $('document, .g-facturacion-usoFactura_select option:selected').val(),
        tipo_vialidad = $('document, .g-facturacion-tipoVialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-facturacion-nombreVialidad_input').val(),
        no_exterior = $('document, .g-facturacion-noExterior_input').val(),
        no_exterior_estado_check = $('document, .g-facturacion-noExterior_checkbox').prop('checked'),
        no_interior = $('document, .g-facturacion-noInterior_input').val(),
        no_interior_estado_check = $('document, .g-facturacion-noInterior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-facturacion-codigoPostal_input').val(),
        colonia = $('document, .g-facturacion-colonia_input').val(),
        ciudad_municipio = $('document, .g-facturacion-ciudadMunicipio_input').val(),
        estado = $('document, .g-facturacion-estado_select option:selected').val(),
        correo_factura = $('document, .g-facturacion-correoFactura_input').val(),
        no_telefonico = $('document, .g-facturacion-noTelefonico_input').val(),
        data = new FormData();

    data.append('accion', 'editar');
    data.append('nombre_razon_social', nombre_razon_social);
    data.append('RFC', RFC);
    data.append('uso_factura', uso_factura);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estado_check', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estado_check', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('correo_factura', correo_factura);
    data.append('no_telefonico', no_telefonico);
    
    fetch('procesos/mis_datos/proceso_editar_datosFacturacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreRazonSocial_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreRazonSocial_alert_error').slideDown('fast');
          $('document, .g-facturacion-nombreRazonSocial_input').addClass('p-input_error');
          $('document, .g-facturacion-nombreRazonSocial_input').focus();
          break;

        case '2':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreRazonSocial_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreRazonSocial_alert_info').slideDown('fast');
          $('document, .g-facturacion-nombreRazonSocial_input').focus();
          break;

        case '3':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_error').slideDown('fast');
          $('document, .g-facturacion-RFC_input').addClass('p-input_error');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '4':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '5':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-RFC_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-RFC_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-RFC_input').focus();
          break;

        case '6':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-usoFactura_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-usoFactura_alert_error').slideDown('fast');
          $('document, .g-facturacion-usoFactura_div').addClass('p-select_error');
          $('document, .g-facturacion-usoFactura_select').focus();
          break;

        case '7':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-usoFactura_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-usoFactura_alert_info').slideDown('fast');
          $('document, .g-facturacion-usoFactura_select').focus();
          break;

        case '8':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-tipoVialidad_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-tipoVialidad_alert_error').slideDown('fast');
          $('document, .g-facturacion-tipoVialidad_div').addClass('p-select_error');
          $('document, .g-facturacion-tipoVialidad_select').focus();
          break;

        case '9':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-tipoVialidad_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-tipoVialidad_alert_info').slideDown('fast');
          $('document, .g-facturacion-tipoVialidad_select').focus();
          break;

        case '10':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreVialidad_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreVialidad_alert_error').slideDown('fast');
          $('document, .g-facturacion-nombreVialidad_input').addClass('p-input_error');
          $('document, .g-facturacion-nombreVialidad_input').focus();
          break;

        case '11':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-nombreVialidad_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-nombreVialidad_alert_info').slideDown('fast');
          $('document, .g-facturacion-nombreVialidad_input').focus();
          break;

        case '12':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noExterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noExterior_alert_error').slideDown('fast');
          $('document, .g-facturacion-noExterior_input').addClass('p-input_error');
          $('document, .g-facturacion-noExterior_input').focus();
          break;

        case '13':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noExterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noExterior_alert_info').slideDown('fast');
          $('document, .g-facturacion-noExterior_input').focus();
          break;

        case '14':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noInterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noInterior_alert_error').slideDown('fast');
          $('document, .g-facturacion-noInterior_input').addClass('p-input_error');
          $('document, .g-facturacion-noInterior_input').focus();
          break;

        case '15':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noInterior_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noInterior_alert_info').slideDown('fast');
          $('document, .g-facturacion-noInterior_input').focus();
          break;

        case '16':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_error').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').addClass('p-input_error');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '17':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '18':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-codigoPostal_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-codigoPostal_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-codigoPostal_input').focus();
          break;

        case '19':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-colonia_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-colonia_alert_error').slideDown('fast');
          $('document, .g-facturacion-colonia_input').addClass('p-input_error');
          $('document, .g-facturacion-colonia_input').focus();
          break;

        case '20':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-colonia_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-colonia_alert_info').slideDown('fast');
          $('document, .g-facturacion-colonia_input').focus();
          break;

        case '21':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-ciudadMunicipio_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-ciudadMunicipio_alert_error').slideDown('fast');
          $('document, .g-facturacion-ciudadMunicipio_input').addClass('p-input_error');
          $('document, .g-facturacion-ciudadMunicipio_input').focus();
          break;

        case '22':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-ciudadMunicipio_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-ciudadMunicipio_alert_info').slideDown('fast');
          $('document, .g-facturacion-ciudadMunicipio_input').focus();
          break;

        case '23':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-estado_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-estado_alert_error').slideDown('fast');
          $('document, .g-facturacion-estado_div').addClass('p-select_error');
          $('document, .g-facturacion-estado_select').focus();
          break;

        case '24':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-estado_select').offset().top - 100 }, 600);
          $('document, .g-facturacion-estado_alert_info').slideDown('fast');
          $('document, .g-facturacion-estado_select').focus();
          break;

        case '25':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-correoFactura_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-correoFactura_alert_error').slideDown('fast');
          $('document, .g-facturacion-correoFactura_input').addClass('p-input_error');
          $('document, .g-facturacion-correoFactura_input').focus();
          break;

        case '26':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-correoFactura_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-correoFactura_alert_info').slideDown('fast');
          $('document, .g-facturacion-correoFactura_input').focus();
          break;

        case '27':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_error').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').addClass('p-input_error');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '28':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '29':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-noTelefonico_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-noTelefonico_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-noTelefonico_input').focus();
          break;

        case '30':
          $('html, body').animate({ scrollTop: 0 }, 600);
          setTimeout(function(){ $('document, .g-facturacion-status').slideDown('fast'); }, 700);
          setTimeout(function(){ history.go(-1); }, 2000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });

    e.preventDefault();
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Contraseña"
  */
  $(document).on('keyup', '.g-facturacion-password_input', function(e){
    //Si no es la tecla de enter, se entra al if
    if(e.keyCode !== 13){
      let password = $(this).val(), data = new FormData();

      if(password.length != 0){
        $('document, .g-facturacion-password_alert_error_1').slideUp('fast');
        $('document, .g-facturacion-password_alert_error_2').slideUp('fast');
        $(this).removeClass('p-input_error');

        data.append('accion', 'validar');
        data.append('valor', password);

        fetch('validaciones/validar_password.php', {
          method: 'POST',
          body: data
        }).then(response => {
          return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
        }).then(datos => {
          let respuesta = datos.respuesta;

          respuesta === '1' ? $('document, .g-facturacion-password_alert_info_1').slideUp('fast') : $('document, .g-facturacion-password_alert_info_1').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-facturacion-password_alert_info_1').slideUp('fast');
      }
    }
  });
  /*
  * Submit - Envia la informacion para eliminar los datos de facturacion 
  */
  $(document).on('submit', '#id-facturacion-form_eliminar', function(e){
    let password = $('document, .g-facturacion-password_input').val(), data = new FormData();

    data.append('accion', 'eliminar');
    data.append('password', password);

    fetch('procesos/mis_datos/proceso_eliminar_datosFacturacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-password_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-password_alert_error_2').slideDown('fast');
          $('document, .g-facturacion-password_input').addClass('p-input_error');
          $('document, .g-facturacion-password_input').focus();
          break;

        case '2':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-password_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-password_alert_info_1').slideDown('fast');
          $('document, .g-facturacion-password_input').focus();
          break;

        case '3':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-password_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-password_alert_error_1').slideDown('fast');
          $('document, .g-facturacion-password_input').focus();
          setTimeout(function(){ $('document, .g-facturacion-password_input').addClass('p-input_error'); }, 100);
          break;

        case '4':
          $('html, body').animate({ scrollTop: $('document, .g-facturacion-password_input').offset().top - 100 }, 600);
          $('document, .g-facturacion-password_alert_info_2').slideDown('fast');
          $('document, .g-facturacion-password_input').focus();
          break;

        case '5':
          $('html, body').animate({ scrollTop: 0 }, 600);
          setTimeout(function(){ $('document, .g-facturacion-status').slideDown('fast'); }, 700);
          setTimeout(function(){ history.go(-1); }, 2000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });

    e.preventDefault();
  });
  /*
  * NO QUIERE FACTURAR
  */
  $(document).on('click', '.g-no-facturar', function(){
    let orden_compra = $('document, #id-comprar-OC').val(), data = new FormData();

    data.append('accion', 'guardar');
    data.append('orden_compra', orden_compra);

    fetch('procesos/mis_datos/proceso_noFacturar.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '0' ? console.error(mensaje) : (
        $('document, #id-contenedor-facturar').slideUp('fast'),
        $('document, .g-notificacion-no_facturar').slideDown('fast')
      );
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * SI QUIERE FACTURAR
  */
  $(document).on('click', '.g-si-facturar', function(){
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
    $('document, #id-loading-facturacion').slideDown('fast');
    $('document, #id-modal-faturacion').slideToggle('fast');
    $('document, #id-modal-faturacion_contenido').toggleClass('p-modal-derecho_contenedor_activo');
    $('document, #id-notificacion-correo_noEnviado').slideUp('fast');

    let orden_compra = $('document, #id-comprar-OC').val(), metodo_pago = $('document, #id-comprar-metodoPago').val(), data = new FormData();

    data.append('accion', 'mostrar');
    data.append('orden_compra', orden_compra);
    data.append('metodo_pago', metodo_pago);

    fetch('procesos/mis_datos/mostrar_camposDatosFacturacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          setTimeout(function(){
            $('document, #id-loading-facturacion').css('display', 'none');
            $('document, #id-modal-faturacion_contenedor_elementos').html(mensaje);
          }, 300);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * CERRAR MODAL DE FACTURACION
  */
  $(document).on('click', '.g-modal-faturacion_cerrar', function(){
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
    $('document, #id-loading-facturacion').slideToggle('fast');
    $('document, #id-modal-faturacion').slideToggle('fast');
    $('document, #id-modal-faturacion_contenido').toggleClass('p-modal-derecho_contenedor_activo');
    $('document, #id-modal-faturacion_contenedor_elementos').html('');
  });
  /*
  * Submit - Envia la informacion para agregar o editar los datos de facturacion y enviar el correo [FALTA]
  */
  $(document).on('submit', '#id-facturacion-form_correoDatos', function(e){
    let orden_compra = $('document, #id-comprar-OC').val(),
        seccion = $('document, .g-facturacion-seccion_input').val(),
        nombre_razon_social = $('document, .g-facturacion-nombreRazonSocial_input').val(),
        RFC = $('document, .g-facturacion-RFC_input').val().toUpperCase(),
        uso_factura = $('document, .g-facturacion-usoFactura_select option:selected').val(),
        forma_pago = $('document, .g-facturacion-formaPago_select option:selected').val(),
        tipo_vialidad = $('document, .g-facturacion-tipoVialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-facturacion-nombreVialidad_input').val(),
        no_exterior = $('document, .g-facturacion-noExterior_input').val(),
        no_exterior_estado_check = $('document, .g-facturacion-noExterior_checkbox').prop('checked'),
        no_interior = $('document, .g-facturacion-noInterior_input').val(),
        no_interior_estado_check = $('document, .g-facturacion-noInterior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-facturacion-codigoPostal_input').val(),
        colonia = $('document, .g-facturacion-colonia_input').val(),
        ciudad_municipio = $('document, .g-facturacion-ciudadMunicipio_input').val(),
        estado = $('document, .g-facturacion-estado_select option:selected').val(),
        correo_factura = $('document, .g-facturacion-correoFactura_input').val(),
        no_telefonico = $('document, .g-facturacion-noTelefonico_input').val(),
        data = new FormData();

    data.append('accion', 'enviar');
    data.append('orden_compra', orden_compra);
    data.append('seccion', seccion);
    data.append('nombre_razon_social', nombre_razon_social);
    data.append('RFC', RFC);
    data.append('uso_factura', uso_factura);
    data.append('forma_pago', forma_pago);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estado_check', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estado_check', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('correo_factura', correo_factura);
    data.append('no_telefonico', no_telefonico);

    $('document, .p-comprar-modal_cont_campos').animate({ scrollTop: 0 }, 200);
    $('document, .p-input').prop('disabled', true);
    $('document, .g-facturacion-submit').prop('disabled', true);
    $('document, .g-facturacion-loading').slideDown('fast');

    fetch('procesos/mis_datos/proceso_form_datosFacturacion_enviarCorreo.php', {
      method: 'POST',
      body: data
    }).then(response => {
      return response.ok ? response.json() : Promise.reject(response.statusText + ' - ' + response.url);
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      $('document, .p-input').prop('disabled', false);
      $('document, .g-facturacion-submit').prop('disabled', false);
      $('document, .g-facturacion-loading').slideUp('fast');

      setTimeout(function(){
        switch(respuesta){
          case '0':
            console.error(mensaje);
            break;

          case '1':
            $('document, .g-facturacion-nombreRazonSocial_alert_error').slideDown('fast');
            $('document, .g-facturacion-nombreRazonSocial_input').addClass('p-input_error');
            $('document, .g-facturacion-nombreRazonSocial_input').focus();
            break;

          case '2':
            $('document, .g-facturacion-nombreRazonSocial_alert_info').slideDown('fast');
            $('document, .g-facturacion-nombreRazonSocial_input').focus();
            break;

          case '3':
            $('document, .g-facturacion-RFC_alert_error').slideDown('fast');
            $('document, .g-facturacion-RFC_input').addClass('p-input_error');
            $('document, .g-facturacion-RFC_input').focus();
            break;

          case '4':
            $('document, .g-facturacion-RFC_alert_info_1').slideDown('fast');
            $('document, .g-facturacion-RFC_input').focus();
            break;

          case '5':
            $('document, .g-facturacion-RFC_alert_info_2').slideDown('fast');
            $('document, .g-facturacion-RFC_input').focus();
            break;

          case '6':
            $('document, .g-facturacion-usoFactura_alert_error').slideDown('fast');
            $('document, .g-facturacion-usoFactura_select').focus();
            $('document, .g-facturacion-usoFactura_div').addClass('p-select_error');
            break;

          case '7':
            $('document, .g-facturacion-usoFactura_alert_info').slideDown('fast');
            $('document, .g-facturacion-usoFactura_select').focus();
            break;

          case '8':
            $('document, .g-facturacion-formaPago_alert_error').slideDown('fast');
            $('document, .g-facturacion-formaPago_select').focus();
            $('document, .g-facturacion-formaPago_div').addClass('p-select_error');
            break;

          case '9':
            $('document, .g-facturacion-formaPago_alert_info').slideDown('fast');
            $('document, .g-facturacion-formaPago_select').focus();
            break;

          case '10':
            $('document, .g-facturacion-tipoVialidad_alert_error').slideDown('fast');
            $('document, .g-facturacion-tipoVialidad_select').focus();
            $('document, .g-facturacion-tipoVialidad_div').addClass('p-select_error');
            break;

          case '11':
            $('document, .g-facturacion-tipoVialidad_alert_info').slideDown('fast');
            $('document, .g-facturacion-tipoVialidad_select').focus();
            break;

          case '12':
            $('document, .g-facturacion-nombreVialidad_alert_error').slideDown('fast');
            $('document, .g-facturacion-nombreVialidad_input').addClass('p-input_error');
            $('document, .g-facturacion-nombreVialidad_input').focus();
            break;

          case '13':
            $('document, .g-facturacion-nombreVialidad_alert_info').slideDown('fast');
            $('document, .g-facturacion-nombreVialidad_input').focus();
            break;

          case '14':
            $('document, .g-facturacion-noExterior_alert_error').slideDown('fast');
            $('document, .g-facturacion-noExterior_input').addClass('p-input_error');
            $('document, .g-facturacion-noExterior_input').focus();
            break;

          case '15':
            $('document, .g-facturacion-noExterior_alert_info').slideDown('fast');
            $('document, .g-facturacion-noExterior_input').focus();
            break;

          case '16':
            $('document, .g-facturacion-noInterior_alert_error').slideDown('fast');
            $('document, .g-facturacion-noInterior_input').addClass('p-input_error');
            $('document, .g-facturacion-noInterior_input').focus();
            break;

          case '17':
            $('document, .g-facturacion-noInterior_alert_info').slideDown('fast');
            $('document, .g-facturacion-noInterior_input').focus();
            break;

          case '18':
            $('document, .g-facturacion-codigoPostal_alert_error').slideDown('fast');
            $('document, .g-facturacion-codigoPostal_input').addClass('p-input_error');
            $('document, .g-facturacion-codigoPostal_input').focus();
            break;

          case '19':
            $('document, .g-facturacion-codigoPostal_alert_info_1').slideDown('fast');
            $('document, .g-facturacion-codigoPostal_input').focus();
            break;

          case '20':
            $('document, .g-facturacion-codigoPostal_alert_info_2').slideDown('fast');
            $('document, .g-facturacion-codigoPostal_input').focus();
            break;

          case '21':
            $('document, .g-facturacion-colonia_alert_error').slideDown('fast');
            $('document, .g-facturacion-colonia_input').addClass('p-input_error');
            $('document, .g-facturacion-colonia_input').focus();
            break;

          case '22':
            $('document, .g-facturacion-colonia_alert_info').slideDown('fast');
            $('document, .g-facturacion-colonia_input').focus();
            break;

          case '23':
            $('document, .g-facturacion-ciudadMunicipio_alert_error').slideDown('fast');
            $('document, .g-facturacion-ciudadMunicipio_input').addClass('p-input_error');
            $('document, .g-facturacion-ciudadMunicipio_input').focus();
            break;

          case '24':
            $('document, .g-facturacion-ciudadMunicipio_alert_info').slideDown('fast');
            $('document, .g-facturacion-ciudadMunicipio_input').focus();
            break;

          case '25':
            $('document, .g-facturacion-estado_alert_error').slideDown('fast');
            $('document, .g-facturacion-estado_select').focus();
            $('document, .g-facturacion-estado_div').addClass('p-select_error');
            break;

          case '26':
            $('document, .g-facturacion-estado_alert_info').slideDown('fast');
            $('document, .g-facturacion-estado_select').focus();
            break;

          case '27':
            $('document, .g-facturacion-correoFactura_alert_error').slideDown('fast');
            $('document, .g-facturacion-correoFactura_input').addClass('p-input_error');
            $('document, .g-facturacion-correoFactura_input').focus();
            break;

          case '28':
            $('document, .g-facturacion-correoFactura_alert_info').slideDown('fast');
            $('document, .g-facturacion-correoFactura_input').focus();
            break;

          case '29':
            $('document, .g-facturacion-noTelefonico_alert_error').slideDown('fast');
            $('document, .g-facturacion-noTelefonico_input').addClass('p-input_error');
            $('document, .g-facturacion-noTelefonico_input').focus();
            break;

          case '30':
            $('document, .g-facturacion-noTelefonico_alert_info_1').slideDown('fast');
            $('document, .g-facturacion-noTelefonico_input').focus();
            break;

          case '31':
            $('document, .g-facturacion-noTelefonico_alert_info_2').slideDown('fast');
            $('document, .g-facturacion-noTelefonico_input').focus();
            break;

          case '32':
            $('document, .g-modal-faturacion_cerrar').click();
            $('document, .g-notificacion-correo_noEnviado').slideDown('fast');
            break;

          case '33':
            $('document, .g-modal-faturacion_cerrar').click();
            setTimeout(function(){
              $('document, #id-contenedor-facturar').slideUp('fast');
              $('document, .g-notificacion-si_facturar').slideDown('fast');
            }, 200);
            break;
        }
      }, 300);
    }).catch(error => {
      console.error('[Error] - ', error);
    });

    e.preventDefault();
  });
});