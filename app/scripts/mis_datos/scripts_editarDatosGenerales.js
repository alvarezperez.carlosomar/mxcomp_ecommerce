$(document).ready(function(){
  /*
  * Input - Detecta si se escribe en el campo "Nombre(s)"
  */
  $(document).on('keyup', '.g-editDatosGene-nombres_input', function(){
    let nombres = $(this).val(), data = new FormData();
    
    if(nombres.length !== 0){
      $('document, .g-editDatosGene-nombres_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombres);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-editDatosGene-nombres_alert_info').slideUp('fast') : $('document, .g-editDatosGene-nombres_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-nombres_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Apellido paterno"
  */
  $(document).on('keyup', '.g-editDatosGene-apellido_paterno_input', function(){
    let apellido_paterno = $(this).val(), data = new FormData();
    
    if(apellido_paterno.length !== 0){
      $('document, .g-editDatosGene-apellido_paterno_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', apellido_paterno);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-apellido_paterno_alert_info').slideUp('fast') : $('document, .g-editDatosGene-apellido_paterno_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-apellido_paterno_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Apellido materno"
  */
  $(document).on('keyup', '.g-editDatosGene-apellido_materno_input', function(){
    let apellido_materno = $(this).val(), data = new FormData();
    
    if(apellido_materno.length !== 0){
      $('document, .g-editDatosGene-apellido_materno_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', apellido_materno);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-apellido_materno_alert_info').slideUp('fast') : $('document, .g-editDatosGene-apellido_materno_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-apellido_materno_alert_info').slideUp('fast');
    }
  });
  /*
  * RadioButton - Detecta si se dio click en alguna opcion de "sexo"
  */
  $(document).on('click', '.g-editDatosGene-sexo_radio', function() {  
    $('document, .g-editDatosGene-sexo_alert_info').slideUp('fast');
    $('document, .g-editDatosGene-sexo_alert_error').slideUp('fast');
  });
  /*
  * Date - Detecta si se desenfoca el campo "Fecha de nacimiento"
  */
  $(document).on('blur', '.g-editDatosGene-fecha_nacimiento_input', function(){
    let fecha_nacimiento = $(this).val();
    
    if(fecha_nacimiento.length !== 0){
      $('document, .g-editDatosGene-fecha_nacimiento_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Date - Detecta si se escribe en el campo "Fecha de nacimiento"
  */
  $(document).on('keyup', '.g-editDatosGene-fecha_nacimiento_input', function(){
    let fecha_nacimiento = $(this).val();
    
    if(fecha_nacimiento.length !== 0){
      $('document, .g-editDatosGene-fecha_nacimiento_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico #1"
  */
  $(document).on('keyup', '.g-editDatosGene-no_telefonico_1_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();

    if(no_telefonico.length !== 0){
      $('document, .g-editDatosGene-no_telefonico_1_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-editDatosGene-no_telefonico_1_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-editDatosGene-no_telefonico_1_alert_info_2').slideUp('fast') : $('document, .g-editDatosGene-no_telefonico_1_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-editDatosGene-no_telefonico_1_alert_info_1').slideDown('fast'),
          $('document, .g-editDatosGene-no_telefonico_1_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-no_telefonico_1_alert_info_1').slideUp('fast');
      $('document, .g-editDatosGene-no_telefonico_1_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico #2"
  */
  $(document).on('keyup', '.g-editDatosGene-no_telefonico_2_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-editDatosGene-no_telefonico_2_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-editDatosGene-no_telefonico_2_alert_info_2').slideUp('fast') : $('document, .g-editDatosGene-no_telefonico_2_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-editDatosGene-no_telefonico_2_alert_info_1').slideDown('fast'),
          $('document, .g-editDatosGene-no_telefonico_2_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-no_telefonico_2_alert_info_1').slideUp('fast');
      $('document, .g-editDatosGene-no_telefonico_2_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico #3"
  */
  $(document).on('keyup', '.g-editDatosGene-no_telefonico_3_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-editDatosGene-no_telefonico_3_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-editDatosGene-no_telefonico_3_alert_info_2').slideUp('fast') : $('document, .g-editDatosGene-no_telefonico_3_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-editDatosGene-no_telefonico_3_alert_info_1').slideDown('fast'),
          $('document, .g-editDatosGene-no_telefonico_3_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-no_telefonico_3_alert_info_1').slideUp('fast');
      $('document, .g-editDatosGene-no_telefonico_3_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Tipo de vialidad"
  */
  $(document).on('change', '.g-editDatosGene-tipo_vialidad_select', function(){
    let tipo_vialidad = $(this).val();
    
    $('document, .g-editDatosGene-tipo_vialidad_alert_info').slideUp('fast');
    
    if(tipo_vialidad.length !== 0){
      $('document, .g-editDatosGene-tipo_vialidad_alert_error').slideUp('fast');
      $('document, .g-editDatosGene-tipo_vialidad_div').removeClass('p-select_error');
    }else{
      $('document, .g-editDatosGene-tipo_vialidad_alert_error').slideDown('fast');
      $('document, .g-editDatosGene-tipo_vialidad_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Nombre de vialidad"
  */
  $(document).on('keyup', '.g-editDatosGene-nombre_vialidad_input', function(){
    let nombre_vialidad = $(this).val(), data = new FormData();
    
    if(nombre_vialidad.length !== 0){
      $('document, .g-editDatosGene-nombre_vialidad_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_vialidad);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-nombre_vialidad_alert_info').slideUp('fast') : $('document, .g-editDatosGene-nombre_vialidad_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-nombre_vialidad_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. exterior"
  */
  $(document).on('keyup', '.g-editDatosGene-no_exterior_input', function(){
    let no_exterior = $(this).val(), data = new FormData();
    
    if(no_exterior.length !== 0){
      $('document, .g-editDatosGene-no_exterior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_exterior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-no_exterior_alert_info').slideUp('fast') : $('document, .g-editDatosGene-no_exterior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-no_exterior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. interior"
  */
  $(document).on('keyup', '.g-editDatosGene-no_interior_input', function(){
    let no_interior = $(this).val(), data = new FormData();
    
    if(no_interior.length !== 0){
      $('document, .g-editDatosGene-no_interior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_interior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-no_interior_alert_info').slideUp('fast') : $('document, .g-editDatosGene-no_interior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-no_interior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Código postal"
  */
  $(document).on('keyup', '.g-editDatosGene-codigo_postal_input', function(){
    let codigo_postal = $(this).val(), data = new FormData();
    
    if(codigo_postal.length !== 0){
      $('document, .g-editDatosGene-codigo_postal_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'generar');
      data.append('codigo_postal', codigo_postal);

      fetch('procesos/general_codigoPostal_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;
        
        if(respuesta === "0"){
          console.log(mensaje);
        }else if(respuesta === "1"){
          $('document, .g-editDatosGene-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-editDatosGene-codigo_postal_alert_info_2').slideUp('fast');
        }else if(respuesta === "2" || respuesta === "3" || respuesta === "4"){
          respuesta === "4" ? $('document, .g-editDatosGene-estado_select').val('') : $('document, .g-editDatosGene-estado_select').html(mensaje);
          
          if(codigo_postal.length === 5){
            $('document, .g-editDatosGene-codigo_postal_alert_info_2').slideUp('fast');
          }else{
            $('document, .g-editDatosGene-codigo_postal_alert_info_1').slideUp('fast');
            $('document, .g-editDatosGene-codigo_postal_alert_info_2').slideDown('fast');
          }
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-codigo_postal_alert_info_1').slideUp('fast');
      $('document, .g-editDatosGene-codigo_postal_alert_info_2').slideUp('fast');
      $('document, .g-editDatosGene-estado_select').val('');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Colonia"
  */
  $(document).on('keyup', '.g-editDatosGene-colonia_input', function(){
    let colonia = $(this).val(), data = new FormData();
    
    if(colonia.length !== 0){
      $('document, .g-editDatosGene-colonia_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', colonia);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-colonia_alert_info').slideUp('fast') : $('document, .g-editDatosGene-colonia_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-colonia_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Ciudad o municipio"
  */
  $(document).on('keyup', '.g-editDatosGene-ciudad_municipio_input', function(){
    let ciudad_municipio = $(this).val(), data = new FormData();
    
    if(ciudad_municipio.length !== 0){
      $('document, .g-editDatosGene-ciudad_municipio_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', ciudad_municipio);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-ciudad_municipio_alert_info').slideUp('fast') : $('document, .g-editDatosGene-ciudad_municipio_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-ciudad_municipio_alert_info').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Estado"
  */
  $(document).on('change', '.g-editDatosGene-estado_select', function(){
    let estado = $(this).val();
    
    $('document, .g-editDatosGene-estado_alert_info').slideUp('fast');
    
    if(estado.length !== 0){
      $('document, .g-editDatosGene-estado_alert_error').slideUp('fast');
      $('document, .g-editDatosGene-estado_div').removeClass('p-select_error');
    }else{
      $('document, .g-editDatosGene-estado_alert_error').slideDown('fast');
      $('document, .g-editDatosGene-estado_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #1"
  */
  $(document).on('keyup', '.g-editDatosGene-entre_calles_1_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-editDatosGene-entre_calles_1_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-entre_calles_1_alert_info').slideUp('fast') : $('document, .g-editDatosGene-entre_calles_1_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-entre_calles_1_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #2"
  */
  $(document).on('keyup', '.g-editDatosGene-entre_calles_2_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-editDatosGene-entre_calles_2_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);
      
      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-editDatosGene-entre_calles_2_alert_info').slideUp('fast') : $('document, .g-editDatosGene-entre_calles_2_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-editDatosGene-entre_calles_2_alert_info').slideUp('fast');
    }
  });
  /*
  * Textarea - Detecta si se teclea Enter en el campo "Referencias adicionales"
  */
  $(document).on('keypress', '.g-editDatosGene-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      return false;
    }
  });
  /*
  * Textarea - Detecta si se escribe en el campo "Referencias adicionales"
  */
  $(document).on('keyup', '.g-editDatosGene-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      $('document, .g-editDatosGene-guardar_cambios_button').click();
    }else{
      let referencias_adicionales = $(this).val(), data = new FormData();
      
      if(referencias_adicionales.length !== 0){
        data.append('accion', 'validar');
        data.append('valor', referencias_adicionales);

        fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);

          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;

          respuesta === '1' ? $('document, .g-editDatosGene-referencias_adicionales_alert_info').slideUp('fast') : $('document, .g-editDatosGene-referencias_adicionales_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-editDatosGene-referencias_adicionales_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Editar datos generales - Evento submit para formulario "Editar datos generales"
  */
  $(document).on('submit', '.g-editDatosGene-form', function(e){
    let nombres = $('document, .g-editDatosGene-nombres_input').val(),
        apellido_paterno = $('document, .g-editDatosGene-apellido_paterno_input').val(),
        apellido_materno = $('document, .g-editDatosGene-apellido_materno_input').val(),
        sexo = String($('input:radio[name=sexo]:checked').val()),
        fecha_nacimiento = $('document, .g-editDatosGene-fecha_nacimiento_input').val(),
        no_telefonico_1 = $('document, .g-editDatosGene-no_telefonico_1_input').val(),
        no_telefonico_2 = $('document, .g-editDatosGene-no_telefonico_2_input').val(),
        no_telefonico_3 = $('document, .g-editDatosGene-no_telefonico_3_input').val(),
        tipo_vialidad = $('document, .g-editDatosGene-tipo_vialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-editDatosGene-nombre_vialidad_input').val(),
        no_exterior = $('document, .g-editDatosGene-no_exterior_input').val(),
        no_exterior_estado_check = $('document, .g-editDatosGene-no_exterior_checkbox').prop('checked'),
        no_interior = $('document, .g-editDatosGene-no_interior_input').val(),
        no_interior_estado_check = $('document, .g-editDatosGene-no_interior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-editDatosGene-codigo_postal_input').val(),
        colonia = $('document, .g-editDatosGene-colonia_input').val(),
        ciudad_municipio = $('document, .g-editDatosGene-ciudad_municipio_input').val(),
        estado = $('document, .g-editDatosGene-estado_select option:selected').val(),
        entre_calle_1 = $('document, .g-editDatosGene-entre_calles_1_input').val(),
        entre_calle_2 = $('document, .g-editDatosGene-entre_calles_2_input').val(),
        referencias_adicionales = $('document, .g-editDatosGene-referencias_adicionales_textarea').val(),
        data = new FormData();

    sexo = sexo === 'undefined' ? "" : sexo;

    data.append('accion', 'modificar');
    data.append('nombres', nombres);
    data.append('apellido_paterno', apellido_paterno);
    data.append('apellido_materno', apellido_materno);
    data.append('sexo', sexo);
    data.append('fecha_nacimiento', fecha_nacimiento);
    data.append('no_telefonico_1', no_telefonico_1);
    data.append('no_telefonico_2', no_telefonico_2);
    data.append('no_telefonico_3', no_telefonico_3);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estado_check', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estado_check', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('entre_calle_1', entre_calle_1);
    data.append('entre_calle_2', entre_calle_2);
    data.append('referencias_adicionales', referencias_adicionales);

    fetch('procesos/mis_datos/proceso_editar_datosGenerales.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-nombres_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-nombres_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-nombres_input').addClass('p-input_error');
          $('document, .g-editDatosGene-nombres_input').focus();
          break;

        case '2':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-nombres_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-nombres_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-nombres_input').focus();
          break;

        case '3':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-apellido_paterno_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-apellido_paterno_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-apellido_paterno_input').addClass('p-input_error');
          $('document, .g-editDatosGene-apellido_paterno_input').focus();
          break;

        case '4':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-apellido_paterno_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-apellido_paterno_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-apellido_paterno_input').focus();
          break;

        case '5':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-apellido_materno_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-apellido_materno_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-apellido_materno_input').addClass('p-input_error');
          $('document, .g-editDatosGene-apellido_materno_input').focus();
          break;

        case '6':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-apellido_materno_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-apellido_materno_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-apellido_materno_input').focus();
          break;

        case '7':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-sexo_radio').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-sexo_alert_error').slideDown('fast');
          break;

        case '8':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-sexo_radio').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-sexo_alert_info').slideDown('fast');
          break;

        case '9':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-fecha_nacimiento_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-fecha_nacimiento_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-fecha_nacimiento_input').addClass('p-date_error');
          $('document, .g-editDatosGene-fecha_nacimiento_input').focus();
          break;

        case '10':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_1_input').offset().top - 135 }, 600);
          $('document, .g-editDatosGene-no_telefonico_1_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_1_input').addClass('p-input_error');
          $('document, .g-editDatosGene-no_telefonico_1_input').focus();
          break;

        case '11':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_1_input').offset().top - 135 }, 600);
          $('document, .g-editDatosGene-no_telefonico_1_alert_info_1').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_1_input').focus();
          break;

        case '12':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_1_input').offset().top - 135 }, 600);
          $('document, .g-editDatosGene-no_telefonico_1_alert_info_2').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_1_input').focus();
          break;

        case '13':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_2_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_telefonico_2_alert_info_1').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_2_input').focus();
          break;

        case '14':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_2_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_telefonico_2_alert_info_2').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_2_input').focus();
          break;

        case '15':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_3_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_telefonico_3_alert_info_1').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_3_input').focus();
          break;

        case '16':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_telefonico_3_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_telefonico_3_alert_info_2').slideDown('fast');
          $('document, .g-editDatosGene-no_telefonico_3_input').focus();
          break;

        case '17':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-tipo_vialidad_select').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-tipo_vialidad_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-tipo_vialidad_select').focus();
          break;

        case '18':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-tipo_vialidad_select').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-tipo_vialidad_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-tipo_vialidad_select').focus();
          break;

        case '19':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-nombre_vialidad_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-nombre_vialidad_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-nombre_vialidad_input').addClass('p-input_error');
          $('document, .g-editDatosGene-nombre_vialidad_input').focus();
          break;

        case '20':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-nombre_vialidad_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-nombre_vialidad_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-nombre_vialidad_input').focus();
          break;

        case '21':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_exterior_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_exterior_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-no_exterior_input').addClass('p-input_error');
          $('document, .g-editDatosGene-no_exterior_input').focus();
          break;

        case '22':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_exterior_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_exterior_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-no_exterior_input').focus();
          break;

        case '23':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_interior_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_interior_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-no_interior_input').addClass('p-input_error');
          $('document, .g-editDatosGene-no_interior_input').focus();
          break;

        case '24':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-no_interior_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-no_interior_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-no_interior_input').focus();
          break;

        case '25':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-codigo_postal_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-codigo_postal_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-codigo_postal_input').addClass('p-input_error');
          $('document, .g-editDatosGene-codigo_postal_input').focus();
          break;

        case '26':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-codigo_postal_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-editDatosGene-codigo_postal_input').focus();
          break;

        case '27':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-codigo_postal_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-codigo_postal_alert_info_2').slideDown('fast');
          $('document, .g-editDatosGene-codigo_postal_input').focus();
          break;

        case '28':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-colonia_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-colonia_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-colonia_input').addClass('p-input_error');
          $('document, .g-editDatosGene-colonia_input').focus();
          break;

        case '29':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-colonia_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-colonia_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-colonia_input').focus();
          break;

        case '30':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-ciudad_municipio_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-ciudad_municipio_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-ciudad_municipio_input').addClass('p-input_error');
          $('document, .g-editDatosGene-ciudad_municipio_input').focus();
          break;

        case '31':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-ciudad_municipio_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-ciudad_municipio_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-ciudad_municipio_input').focus();
          break;

        case '32':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-estado_select').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-estado_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-estado_select').focus();
          break;

        case '33':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-estado_select').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-estado_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-estado_select').focus();
          break;

        case '34':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-entre_calles_1_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-entre_calles_1_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-entre_calles_1_input').addClass('p-input_error');
          $('document, .g-editDatosGene-entre_calles_1_input').focus();
          break;

        case '35':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-entre_calles_1_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-entre_calles_1_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-entre_calles_1_input').focus();
          break;

        case '36':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-entre_calles_2_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-entre_calles_2_alert_error').slideDown('fast');
          $('document, .g-editDatosGene-entre_calles_2_input').addClass('p-input_error');
          $('document, .g-editDatosGene-entre_calles_2_input').focus();
          break;

        case '37':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-entre_calles_2_input').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-entre_calles_2_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-entre_calles_2_input').focus();
          break;

        case '38':
          $('html, body').animate({ scrollTop: $('document, .g-editDatosGene-referencias_adicionales_textarea').offset().top - 100 }, 600);
          $('document, .g-editDatosGene-referencias_adicionales_alert_info').slideDown('fast');
          $('document, .g-editDatosGene-referencias_adicionales_textarea').focus();
          break;

        case '39':
          $('document, .g-contenedor-aviso').html(mensaje);
          $('html, body').animate({ scrollTop: 0 }, 600);

          setTimeout(function(){ $('document, .g-contenedor-aviso_success').slideDown('fast'); }, 700);
          setTimeout(function(){ history.go(-1); }, 2000);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
});