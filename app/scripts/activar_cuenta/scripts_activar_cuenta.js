$(document).ready(function(){
  /*
  * Input - Detecta cuando se escribe en el campo "Correo electrónico"
  */
  var correo_mal = "";
  $(document).on('keyup', '.g-activarCuenta-correo_input', function(){
    let correo = $(this).val(), data = new FormData();
    
    if(correo.length !== 0){
      $('document, .g-activarCuenta-correo_alert_error_2').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        switch(respuesta){
          case '1':
            $('document, .g-notification-correo_success').slideUp('fast');
            $('document, .g-activarCuenta-correo_alert_info').slideUp('fast');
            
            if($('document, .g-activarCuenta-correo_alert_error_1').css('display') === 'block' && correo_mal === correo){
              $('document, .g-activarCuenta-correo_alert_error_1').slideDown('fast');
            }else{
              correo_mal = "";
              $('document, .g-activarCuenta-correo_alert_error_1').slideUp('fast');
            }
            break;

          case '2':
            $('document, .g-notification-correo_success').slideUp('fast');
            $('document, .g-activarCuenta-correo_alert_info').slideDown('fast');
            $('document, .g-activarCuenta-correo_alert_error_1').slideUp('fast');
            break;
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-notification-correo_success').slideUp('fast');
      $('document, .g-activarCuenta-correo_alert_info').slideUp('fast');
      $('document, .g-activarCuenta-correo_alert_error_1').slideUp('fast');
    }
  });
  /*
  * Submit - Evento submit para formulario de "Tiempo expirado para activación"
  */
  $(document).on('submit', '.g-activarCuenta-form', function(e){
    let correo = $('document, .g-activarCuenta-correo_input').val(), data = new FormData();
    
    $('document, .g-activarCuenta-loading').slideDown('fast');

    data.append('accion', 'enviar');
    data.append('correo', correo);

    fetch('procesos/usuario/proceso_tiempoExpirado_activacion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;
      
      $('document, .g-activarCuenta-loading').slideUp('fast');
      
      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('document, .g-activarCuenta-correo_alert_error_2').slideDown('fast');
          $('document, .g-activarCuenta-correo_input').addClass('p-input_error');
          $('document, .g-activarCuenta-correo_input').focus();
          break;
          
        case '2':
          $('document, .g-activarCuenta-correo_alert_info').slideDown('fast');
          $('document, .g-activarCuenta-correo_input').focus();
          break;

        case '3':
          $('document, .g-activarCuenta-correo_alert_error_1').slideDown('fast');
          $('document, .g-activarCuenta-correo_input').addClass('p-input_error');
          $('document, .g-activarCuenta-correo_input').focus();
          correo_mal = correo;
          break;

        case '4':
          $('document, .g-notification-correo_error').slideDown('fast');
          break;

        case '5':
          $('document, .g-activarCuenta-contenedor_informacion').slideUp('fast');
          $('document, .g-activarCuenta-correo_input').prop('disabled', true);
          $('document, .g-activarCuenta-enviar_button').prop('type', 'button');
          $('document, .g-activarCuenta-enviar_button').prop('disabled', true);
          $('document, .g-notification-correo_success').slideDown('fast');
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
});