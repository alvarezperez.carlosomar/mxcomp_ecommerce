$(document).ready(function(){
  /*
  * Eliminar tarjeta - Detecta cuando se escribe en el campo "Contraseña"
  */
  $(document).on('keyup', '#id-elimTarjeta-password_input', function(e){
    if(e.keyCode !== 13){
      let pass = $(this).val();

      if(pass.length != 0){
        $('#id-elimTarjeta-password_alert_error_1').slideUp('fast');
        $('#id-elimTarjeta-password_alert_error_2').slideUp('fast');
        $(this).removeClass('p-input_error');

        $.ajax({
          url: 'procesos/general_password_validacion.php',
          type: 'POST',
          data: {
            accion: "validar",
            password: pass
          }
        })
        .done(function(data, textStatus, jqXHR){
          let datos = JSON.parse(data),
              respuesta = datos[0].respuesta;

          if(respuesta === "1"){
            $('#id-elimTarjeta-password_alert_info_1').slideUp('fast');
          }else if(respuesta === "2"){
            $('#id-elimTarjeta-password_alert_info_1').slideDown('fast');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.status + " - " + errorThrown);
        });
      }else{
        $('#id-elimTarjeta-password_alert_info_1').slideUp('fast');
      }
    }
  });
  /*
  * Eliminar tarjeta - Boton de eliminar tarjeta
  */
  $(document).on('submit', '#id-elimTarjeta-form', function(e){
    let valor = $('#id-elimTarjeta-valor_input').val(),
        password = $('#id-elimTarjeta-password_input').val();

    $('#id-elimTarjeta-password_alert_error_1').slideUp('fast');
    $('document, #id-elimTarjeta_loading').slideDown('fast');
    
    if(seccion === "comprar"){
      $('.p-comprar-modal_cont_campos').animate({ scrollTop: 0 }, 600);
    }

    $.ajax({
      url: 'procesos/general_deleteTar.php',
      type: 'POST',
      data: {
        accion: "eliminar",
        valor: valor,
        pass: password
      }
    })
    .done(function(data, textStatus, jqXHR){
      let datos = JSON.parse(data),
          respuesta = datos[0].respuesta,
          mensaje = datos[0].mensaje;
      
      if(respuesta === "0"){
        console.log(mensaje);
        $('document, #id-elimTarjeta_loading').slideUp('fast');
      }else if(respuesta === "1"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('#id-elimTarjeta-password_input').offset().top - 100
            }, 600);
        }
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-password_alert_error_2').slideDown('fast');
        $('#id-elimTarjeta-password_input').addClass('p-input_error');
        $('#id-elimTarjeta-password_input').focus();

      }else if(respuesta === "2"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('#id-elimTarjeta-password_input').offset().top - 100
            }, 600);
        }
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-password_alert_info_1').slideDown('fast');
        $('#id-elimTarjeta-password_input').focus();

      }else if(respuesta === "3"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('#id-elimTarjeta-password_input').offset().top - 100
            }, 600);
        }
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-password_alert_error_1').slideDown('fast');
        $('#id-elimTarjeta-password_input').addClass('p-input_error');
        $('#id-elimTarjeta-password_input').focus();

      }else if(respuesta === "4"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('#id-elimTarjeta-password_input').offset().top - 100
            }, 600);
        }
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-password_alert_info_2').slideDown('fast');
        $('#id-elimTarjeta-password_input').focus();

      }else if(respuesta === "5"){
        if(seccion === "modificar_info"){
          $('html, body').animate({ scrollTop: 0 }, 600);
        }
        
        $('document, #id-elimTarjeta-msg_info_span').html(mensaje);
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-msg_info').slideDown('fast');
        
        setTimeout(function(){
          $('#id-elimTarjeta-msg_info').slideDown('fast');
        }, 10000);
      }else if(respuesta === "6"){
        if(seccion === "modificar_info"){
          $('html, body').animate({ scrollTop: 0 }, 600);
        }
        
        $('document, #id-elimTarjeta-msg_error_span').html(mensaje);
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-msg_error').slideDown('fast');
        
        setTimeout(function(){
          $('#id-elimTarjeta-msg_error').slideDown('fast');
        }, 10000);
      }else if(respuesta === "7"){
        if(seccion === "modificar_info"){
          $('html, body').animate({ scrollTop: 0 }, 600);
        }
        
        $('document, #id-elimTarjeta_loading').slideUp('fast');
        $('#id-elimTarjeta-status').slideDown('fast');

        if(seccion === "modificar_info"){
          setTimeout(function(){
            history.go(-1);
          }, 1600);
        }
        
        if(seccion === "comprar"){
          setTimeout(function(){
            $('#id-elimTarjeta-status').slideUp('fast');
            mostrar_tarjetas_credito_debito();
            $('html').toggleClass('p-modal-derecho_window_no_scroll');
            $('#id-comprar-modal_general').slideToggle('fast');
            $('#id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
            $('#id-comprar-modal_general_contElementos').html('');
          }, 1600);
        }
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.status + " - " + errorThrown);
    });
    
    e.preventDefault();
  });
  /*
  * Metodo de pago - Funcion para mostrar Tarjeta(s)
  */
  function mostrar_tarjetas_credito_debito(){
    $('#T-comprar_check_tar').val('');
    $('#T-comprar_cvc_tar').val('');
    $('#T-comprar_OP_Cli').val('');
    $('#T-comprar_OP_Tar').val('');
    $('#id-comprar-detalles_pago_p').html('Elegiste pagar con tarjeta, pero aún no la seleccionas.');
    
    let data = new FormData();

    data.append('accion', 'mostrar');

    fetch('procesos/comprar/mostrar_tarjetas_credito_debito.php', {
      method: 'POST',
      body: data
    })
    .then(response => {
      if(response.ok){
        return response.json();
      }else{
        return [{
          "respuesta": "status",
          "mensaje": response.status
        }];
      }
    })
    .then(datos => {
      let respuesta = datos[0].respuesta,
          mensaje = datos[0].mensaje;

      switch(respuesta){
        case 'status':
          console.log("Status code: ", mensaje);
          break;

        case '1':
          $('#id-comprar-load_datos_tar').slideUp('fast');
          $('#id-comprar-contenedor_tarjeta_datos').html(mensaje);
          break;
      }
    })
    .catch(error => {
      console.error("Ocurrió un error en la petición: ", error);
    });

    /*$.ajax({
      url: 'procesos/comprar/mostrar_tarjetas_credito_debito.php',
      type: 'POST',
      data: {
        accion: "mostrar"
      }
    })
    .done(function(data, textStatus, jqXHR){
      $('#id-comprar-load_datos_tar').slideUp('fast');
      $('#id-comprar-contenedor_tarjeta_datos').html(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.status + " - " + errorThrown);
    });*/
  }
});