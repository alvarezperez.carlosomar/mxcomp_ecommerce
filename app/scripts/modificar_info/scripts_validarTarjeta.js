$(document).ready(function(){
  /*
  * Detecta cuando se da click en un campo
  */
  $(document).on('click', '.g-input-click', function(){
    let that = this;
    
    setTimeout(function(){
      that.selectionStart = that.selectionEnd = 10000;
    }, 0);
  });
  /*
  * Detecta cuando se hace algo en el campo "Nombre del tarjetahabiente"
  */
  $(document).on('input', '.g-tarjeta-nombre_input', function(){
    this.value = this.value.replace(/[^a-zA-Z\ ]/g, '');
  });
  /*
  * Detecta cuando se presiona una tecla en el campo "Nombre del tarjetahabiente"
  */
  $(document).on('keydown', '.g-tarjeta-nombre_input', function(){
    let nom_tarjetahabiente = $(this).val();
    
    $('.g-tarjeta-nombre_contador_span').text(nom_tarjetahabiente.length);
  });
  /*
  * Detecta cuando se suelta una tecla en el campo "Nombre del tarjetahabiente"
  */
  $(document).on('keyup', '.g-tarjeta-nombre_input', function(){
    let nom_tarjetahabiente = $(this).val(),
        data = new FormData();
    
    if(nom_tarjetahabiente.length !== 0){
      $('.g-tarjeta-nombre_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
    }
    
    $('.g-tarjeta-nombre_alert_info').slideUp('fast');
    $('.g-tarjeta-nombre_contador_span').text(nom_tarjetahabiente.length);
  });
  /*
  * Detecta cuando se hace algo en el campo "Numero de la tarjeta"
  */
  $(document).on('input', '.g-tarjeta-numero_input', function(){
    this.value = this.value.replace(/[^0-9\ ]/g, '');
  });
  /*
  * Detecta cuando se presiona una tecla en el campo "Numero de la tarjeta"
  */
  $(document).on('keydown', '.g-tarjeta-numero_input', function(e){
    let num_tarjeta = $(this).val(),
        valor_length = 0, bloque_1 = "", bloque_2 = "", bloque_3 = "", bloque_4 = "";
    
    let codigo_ascii = e.keyCode;
    
    // Si es espacio, no lo toma en cuenta
    if(codigo_ascii === 32)
      return false;
    
    num_tarjeta = num_tarjeta.replace(/[^0-9]/g, '');
    valor_length = num_tarjeta.length;
    
    if(valor_length !== 0){
      let dos_valores = num_tarjeta.substr(0, 2);
      
      if(dos_valores === "34" || dos_valores === "37"){
        
        if(valor_length > 15)
          return false;
        
        $(this).attr('maxlength', '17'); // LONGITUD AMERICAN EXPRESS
        
        bloque_1 = num_tarjeta.substring(0, 4);
        bloque_2 = num_tarjeta.substring(4, 10);
        bloque_3 = num_tarjeta.substring(10, 15);
        
        num_tarjeta = bloque_1 + ' ' + bloque_2 + ' ' + bloque_3;
      }else{
        if(valor_length > 16)
          return false;
        
        $(this).attr('maxlength', '19'); // LONGITUD VISA, MASTERCARD, CARNET
        
        bloque_1 = num_tarjeta.substring(0, 4);
        bloque_2 = num_tarjeta.substring(4, 8);
        bloque_3 = num_tarjeta.substring(8, 12);
        bloque_4 = num_tarjeta.substring(12, 16);
        
        num_tarjeta = bloque_1 + ' ' + bloque_2 + ' ' + bloque_3 + ' ' + bloque_4;
      }
      
      $(this).val(num_tarjeta.trim());
    }
  });
  /*
  * Detecta cuando se suelta una tecla en el campo "Numero de la tarjeta"
  */
  $(document).on('keyup', '.g-tarjeta-numero_input', function(){
    let num_tarjeta = $(this).val(),
        valor_length = 0, bloque_1 = "", bloque_2 = "", bloque_3 = "", bloque_4 = "";
    
    num_tarjeta = num_tarjeta.replace(/[^0-9]/g, '');
    valor_length = num_tarjeta.length;
    
    if(valor_length !== 0){
      let dos_valores = num_tarjeta.substr(0, 2);
      
      $('.g-tarjeta-numero_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      if(dos_valores === "34" || dos_valores === "37"){
        if(valor_length > 15)
          return false;
        
        $(this).attr('maxlength', '17'); // LONGITUD AMERICAN EXPRESS
        
        bloque_1 = num_tarjeta.substring(0, 4);
        bloque_2 = num_tarjeta.substring(4, 10);
        bloque_3 = num_tarjeta.substring(10, 15);
        
        num_tarjeta = bloque_1 + ' ' + bloque_2 + ' ' + bloque_3;
      }else{
        if(valor_length > 16)
          return false;
        
        $(this).attr('maxlength', '19'); // LONGITUD VISA, MASTERCARD, CARNET
        
        bloque_1 = num_tarjeta.substring(0, 4);
        bloque_2 = num_tarjeta.substring(4, 8);
        bloque_3 = num_tarjeta.substring(8, 12);
        bloque_4 = num_tarjeta.substring(12, 16);
        
        num_tarjeta = bloque_1 + ' ' + bloque_2 + ' ' + bloque_3 + ' ' + bloque_4;
      }
      
      $(this).val(num_tarjeta.trim());
    }
    
    $('.g-tarjeta-numero_alert_info').slideUp('fast');
  });
  /*
  * Detecta cuando se hace algo en el campo "Mes" de la fecha de expiracion
  */
  $(document).on('input', '.g-tarjeta-mes_exp_input', function(){
    this.value = this.value.replace(/[^0-9]/g, '');
  });
  /*
  * Detecta cuando se suelta la tecla en el campo "Mes" de la fecha de expiracion
  */
  $(document).on('keyup', '.g-tarjeta-mes_exp_input', function(){
    let mes_exp = $(this).val();

    if(mes_exp.length !== 0){
      $('.g-tarjeta-fecha_exp_alert_info').slideUp('fast');
      $('.g-tarjeta-mes_exp_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $('.g-tarjeta-anio_exp_alert_error').slideUp('fast');
      $('.g-tarjeta-anio_exp_input').removeClass('p-input_error');
      
      if(mes_exp.length === 1){
        (parseInt(mes_exp) === 1) ? $('.g-tarjeta-mes_exp_alert_info_2').slideDown('fast') : $('.g-tarjeta-mes_exp_alert_info_2').slideUp('fast');
        
        if(parseInt(mes_exp) >= 2 && parseInt(mes_exp) <= 9)
          $(this).val("0" + mes_exp);
      }
      
      if(mes_exp.length === 2){
        $('.g-tarjeta-mes_exp_alert_info_2').slideUp('fast');
        
        if(parseInt(mes_exp) > 12)
          mes_exp = "12";

        $(this).val(mes_exp);
      }
    }else{
      $('.g-tarjeta-mes_exp_alert_info_2').slideUp('fast');
    }
    
    $('.g-tarjeta-mes_exp_alert_info_1').slideUp('fast');
  });
  /*
  * Detecta cuando se hace algo en el campo "Año" de la fecha de expiracion
  */
  $(document).on('input', '.g-tarjeta-anio_exp_input', function(){
    this.value = this.value.replace(/[^0-9]/g, '');
  });
  /*
  * Detecta cuando se suelta la tecla en el campo "Año" de la fecha de vencimiento
  */
  $(document).on('keyup', '.g-tarjeta-anio_exp_input', function(){
    let anio_exp = $(this).val();

    if(anio_exp.length !== 0){
      $('.g-tarjeta-fecha_exp_alert_info').slideUp('fast');
      $('.g-tarjeta-anio_exp_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $('.g-tarjeta-mes_exp_alert_error').slideUp('fast');
      $('.g-tarjeta-mes_exp_input').removeClass('p-input_error');
    }else{
      $('.g-tarjeta-anio_exp_alert_info_2').slideUp('fast');
    }
    
    $('.g-tarjeta-anio_exp_alert_info_1').slideUp('fast');
  });
  /*
  * Detecta cuando se desenfoca el campo "Año" de la fecha de vencimiento
  */
  $(document).on('blur', '.g-tarjeta-anio_exp_input', function(){
    let anio_exp = $(this).val();
    
    (anio_exp.length === 1 || anio_exp.length === 3) ? $('.g-tarjeta-anio_exp_alert_info_2').slideDown('fast') :  $('.g-tarjeta-anio_exp_alert_info_2').slideUp('fast');
  });
  /*
  * Detecta cuando se hace algo en el campo "Código de seguridad (CVC)"
  */
  $(document).on('input', '.g-tarjeta-cvc_input', function(){
    this.value = this.value.replace(/[^0-9]/g, '');
  });
  /*
  * Detecta cuando se suelta la tecla en el campo "Código de seguridad (CVC)"
  */
  $(document).on('keyup', '.g-tarjeta-cvc_input', function(){
    let cvc = $(this).val();

    if(cvc.length !== 0){
      $('.g-tarjeta-cvc_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      (cvc.length < 3) ?  $('.g-tarjeta-cvc_alert_info_2').slideDown('fast') : $('.g-tarjeta-cvc_alert_info_2').slideUp('fast');
    }else{
      $('.g-tarjeta-cvc_alert_info_2').slideUp('fast');
    }
    
    $('.g-tarjeta-cvc_alert_info_1').slideUp('fast');
  });
});