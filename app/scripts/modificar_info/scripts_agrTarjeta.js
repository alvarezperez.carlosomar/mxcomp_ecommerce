$(document).ready(function(){
  if(seccion === "modificar_info"){
    let nom_titu = $('#id-agrTarjeta-nom_titu_input').val();

    if(nom_titu.length !== 0){
      $('#id-agrTarjeta-nom_titu_contador_span').text(nom_titu.length);
    }else{
      $('#id-agrTarjeta-nom_titu_contador_span').text('0');
    }
  }
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "Nombre y apellido del titular"
  */
  $(document).on('keyup', '#id-agrTarjeta-nom_titu_input', function(){
    let nom_titu = $(this).val();
    
    if(nom_titu.length !== 0){
      $('#id-agrTarjeta-nom_titu_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_letras_espacios_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: nom_titu
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-nom_titu_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-nom_titu_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
      
      $('#id-agrTarjeta-nom_titu_contador_span').text(nom_titu.length);
    }else{
      $('#id-agrTarjeta-nom_titu_alert_info').slideUp('fast');
      $('#id-agrTarjeta-nom_titu_contador_span').text('0');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se suelta la tecla en el campo "Numero de la tarjeta"
  */
  $(document).on('keyup', '#id-agrTarjeta-num_tar_input', function(){
    let num_tar = $(this).val();
    
    if(num_tar.length !== 0){
      $('#id-agrTarjeta-num_tar_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_numerico_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: num_tar
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-num_tar_alert_info_1').slideUp('fast');
          $('#id-agrTarjeta-num_tar_alert_info_2').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-num_tar_alert_info_1').slideDown('fast');
          $('#id-agrTarjeta-num_tar_alert_info_2').slideUp('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
      
      $('#id-agrTarjeta-num_tar_contador_span').text(num_tar.length);
    }else{
      $('#id-agrTarjeta-num_tar_alert_info_1').slideUp('fast');
      $('#id-agrTarjeta-num_tar_alert_info_2').slideUp('fast');
      $('#id-agrTarjeta-num_tar_contador_span').text('0');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando no está enfocado el campo "Numero de la tarjeta"
  */
  $(document).on('blur', '#id-agrTarjeta-num_tar_input', function(){
    let num_tar = $(this).val();
    
    if(num_tar.length !== 0){
      let validar_num_tar = OpenPay.card.validateCardNumber(num_tar);
      
      if(validar_num_tar){
        let tipo_tarjeta = OpenPay.card.cardType(num_tar);

        $('document, #id-agrTarjeta-tipo_tarjeta_span').text(tipo_tarjeta);
        $('document, #id-agrTarjeta-num_tar_alert_info_2').slideUp('fast');
      }else{
        $('document, #id-agrTarjeta-num_tar_alert_info_2').slideDown('fast');
        $('document, #id-agrTarjeta-tipo_tarjeta_span').text('Aún no se tiene el dato');
      }
    }else{
      $('#id-agrTarjeta-tipo_tarjeta_span').text('Aún no se tiene el dato');
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando se suelta la tecla en el campo "Mes" de la fecha de vencimiento
  */
  $(document).on('keyup', '#id-agrTarjeta-mes_venc_input', function(e){
    if(e.keyCode !== 13){
      let mes_venc = $(this).val();

      if(mes_venc.length !== 0){
        $('#id-agrTarjeta-mes_venc_alert_info_2').slideUp('fast');
        $('#id-agrTarjeta-mes_venc_alert_error').slideUp('fast');
        $('#id-agrTarjeta-anio_venc_alert_error').slideUp('fast');
        $('#id-agrTarjeta-anio_venc_input').removeClass('p-input_error');
        $(this).removeClass('p-input_error');

        $.ajax({
          url: 'procesos/general_campo_numerico_validacion.php',
          type: 'POST',
          data: {
            accion: "validar",
            texto: mes_venc
          }
        })
        .done(function(data, textStatus, jqXHR){
          let datos = JSON.parse(data),
              respuesta = datos[0].respuesta;

          if(respuesta === "1"){
            $('#id-agrTarjeta-mes_venc_alert_info_1').slideUp('fast');

            if(mes_venc.length === 1){
              mes_venc = parseInt(mes_venc);
              if(mes_venc >= 2 && mes_venc <= 9){
                $('#id-agrTarjeta-mes_venc_input').val("0" + mes_venc);
                $('#id-agrTarjeta-anio_venc_input').prop('disabled', false);
              }else{
                $('#id-agrTarjeta-anio_venc_input').prop('disabled', true);
                $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
              }
            }

            if(mes_venc.length === 2){
              if(parseInt(mes_venc) > 12){
                mes_venc = "12";
              }
              
              $('#id-agrTarjeta-mes_venc_input').val(mes_venc);
              $('#id-agrTarjeta-anio_venc_input').prop('disabled', false);
            }
          }else if(respuesta === "2"){
            $('#id-agrTarjeta-anio_venc_input').val('');
            $('#id-agrTarjeta-mes_venc_alert_info_1').slideDown('fast');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.status + " - " + errorThrown);
        });
      }else{
        $('#id-agrTarjeta-mes_venc_alert_info_1').slideUp('fast');
        $('#id-agrTarjeta-mes_venc_alert_info_2').slideUp('fast');
        $('#id-agrTarjeta-anio_venc_input').prop('disabled', true);
        $(this).val('');
      }
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando no está enfocado el campo "Mes" de la fecha de vencimiento
  */
  $(document).on('blur', '#id-agrTarjeta-mes_venc_input', function(e){
    if(e.keyCode !== 13){
      let mes_venc = $(this).val(),
          anio_venc = $('#id-agrTarjeta-anio_venc_input').val();

      if(mes_venc.length !== 0 && anio_venc.length !== 0){
        if(OpenPay.card.validateExpiry(mes_venc, anio_venc)){
          $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
        }else{
          $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideDown('fast');
        }
      }else{
        $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
      }
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando se suelta la tecla en el campo "Año" de la fecha de vencimiento
  */
  $(document).on('keyup', '#id-agrTarjeta-anio_venc_input', function(e){
    if(e.keyCode !== 13){
      let anio_venc = $(this).val(),
          mes_venc = $('#id-agrTarjeta-mes_venc_input').val();

      if(anio_venc.length !== 0){
        $('#id-agrTarjeta-anio_venc_alert_info_2').slideUp('fast');
        $('#id-agrTarjeta-anio_venc_alert_error').slideUp('fast');
        $(this).removeClass('p-input_error');

        $.ajax({
          url: 'procesos/general_campo_numerico_validacion.php',
          type: 'POST',
          data: {
            accion: "validar",
            texto: anio_venc
          }
        })
        .done(function(data, textStatus, jqXHR){
          let datos = JSON.parse(data),
              respuesta = datos[0].respuesta;

          if(respuesta === "1"){
            $('#id-agrTarjeta-anio_venc_alert_info_1').slideUp('fast');
            
            if(anio_venc.length === 1 || anio_venc.length === 3){
              $('#id-agrTarjeta-anio_venc_alert_info_2').slideDown('fast');
            }else{
              $('#id-agrTarjeta-anio_venc_alert_info_2').slideUp('fast');
            }
            
          }else if(respuesta === "2"){
            $('#id-agrTarjeta-anio_venc_alert_info_1').slideDown('fast');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.status + " - " + errorThrown);
        });
      }else{
        $('#id-agrTarjeta-anio_venc_alert_info_1').slideUp('fast');
        $('#id-agrTarjeta-anio_venc_alert_info_2').slideUp('fast');
      }
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando no está enfocado el campo "Año" de la fecha de vencimiento
  */
  $(document).on('blur', '#id-agrTarjeta-anio_venc_input', function(e){
    if(e.keyCode !== 13){
      let anio_venc = $(this).val(),
          mes_venc = $('#id-agrTarjeta-mes_venc_input').val();

      if(anio_venc.length !== 0){
        if(OpenPay.card.validateExpiry(mes_venc, anio_venc)){
          $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
        }else{
          $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideDown('fast');
        }
      }else{
        $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
      }
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando se suelta la tecla en el campo "Código de seguridad"
  */
  $(document).on('keyup', '#id-agrTarjeta-cvc_input', function(e){
    if(e.keyCode !== 13){
      let cvc = $(this).val();

      if(cvc.length !== 0){
        $('#id-agrTarjeta-cvc_alert_info_3').slideUp('fast');
        $('#id-agrTarjeta-cvc_alert_error').slideUp('fast');
        $(this).removeClass('p-input_error');

        $.ajax({
          url: 'procesos/general_campo_numerico_validacion.php',
          type: 'POST',
          data: {
            accion: "validar",
            texto: cvc
          }
        })
        .done(function(data, textStatus, jqXHR){
          let datos = JSON.parse(data),
              respuesta = datos[0].respuesta;

          if(respuesta === "1"){
            $('#id-agrTarjeta-cvc_alert_info_1').slideUp('fast');
            
            if(cvc.length === 1 || cvc.length === 2){
              $('#id-agrTarjeta-cvc_alert_info_2').slideDown('fast');
            }else{
              $('#id-agrTarjeta-cvc_alert_info_2').slideUp('fast');
            }
            
          }else if(respuesta === "2"){
            $('#id-agrTarjeta-cvc_alert_info_1').slideDown('fast');
            $('#id-agrTarjeta-cvc_alert_info_2').slideUp('fast');
          }
        })
        .fail(function(jqXHR, textStatus, errorThrown){
          console.log(jqXHR.status + " - " + errorThrown);
        });
      }else{
        $('#id-agrTarjeta-cvc_alert_info_1').slideUp('fast');
        $('#id-agrTarjeta-cvc_alert_info_2').slideUp('fast');
        $('#id-agrTarjeta-cvc_alert_info_3').slideUp('fast');
      }
    }
  });
  /*
  *  Agregar tarjeta - Detecta cuando no está enfocado el campo "Código de seguridad"
  */
  $(document).on('blur', '#id-agrTarjeta-cvc_input', function(e){
    if(e.keyCode !== 13){
      let cvc = $(this).val(),
          num_tar = $('#id-agrTarjeta-num_tar_input').val();

      if(cvc.length !== 0){
        if(OpenPay.card.validateCVC(cvc, num_tar)){
          $('#id-agrTarjeta-cvc_alert_info_3').slideUp('fast');
        }else{
          $('#id-agrTarjeta-cvc_alert_info_3').slideDown('fast');
        }
      }else{
        $('#id-agrTarjeta-cvc_alert_info_3').slideUp('fast');
      }
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se selecciona el select "Tipo de vialidad"
  */
  $(document).on('change', '#id-agrTarjeta-tipo_vialidad_select', function(){
    let tipo_vialidad = $(this).val();
    
    $('#id-agrTarjeta-tipo_vialidad_alert_info').slideUp('fast');
    
    if(tipo_vialidad.length !== 0){
      $('#id-agrTarjeta-tipo_vialidad_alert_error').slideUp('fast');
      $('#id-agrTarjeta-tipo_vialidad_div').removeClass('p-select_error');
    }else{
      $('#id-agrTarjeta-tipo_vialidad_alert_error').slideDown('fast');
      $('#id-agrTarjeta-tipo_vialidad_div').addClass('p-select_error');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "Nombre de vialidad"
  */
  $(document).on('keyup', '#id-agrTarjeta-nom_vialidad_input', function(){
    let nom_vialidad = $(this).val();
    
    if(nom_vialidad.length !== 0){
      $('#id-agrTarjeta-nom_vialidad_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_letras_espacios_simbolos_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: nom_vialidad
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-nom_vialidad_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-nom_vialidad_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-nom_vialidad_alert_info').slideUp('fast');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "No. exterior"
  */
  $(document).on('keyup', '#id-agrTarjeta-no_ext_input', function(){
    let no_ext = $(this).val();
    
    if(no_ext.length !== 0){
      $('document, #id-noExterior_BD').val(no_ext);
      
      $('#id-agrTarjeta-no_ext_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_noExt_Int_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: no_ext
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-no_ext_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-no_ext_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-no_ext_alert_info').slideUp('fast');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "No. interior"
  */
  $(document).on('keyup', '#id-agrTarjeta-no_int_input', function(){
    let no_int = $(this).val();
    
    if(no_int.length !== 0){
      $('document, #id-noInterior_BD').val(no_int);
      
      $('#id-agrTarjeta-no_int_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_noExt_Int_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: no_int
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-no_int_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-no_int_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-no_int_alert_info').slideUp('fast');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "Codigo Postal"
  */
  $(document).on('keyup', '#id-agrTarjeta-codigo_postal_input', function(){
    let codigoPostal = $(this).val();
    
    if(codigoPostal.length !== 0){
      $('#id-agrTarjeta-codigo_postal_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_codigoPostal_proceso.php',
        type: 'POST',
        data: {
          accion: "generar",
          codigo_postal: codigoPostal
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta,
            mensaje = datos[0].mensaje;

        if(respuesta === "0"){
          console.log(mensaje);
        }else if(respuesta === "1"){
          $('#id-agrTarjeta-codigo_postal_alert_info_1').slideDown('fast');
          $('#id-agrTarjeta-codigo_postal_alert_info_2').slideUp('fast');
        }else if(respuesta === "2" || respuesta === "3"){
          $('#id-agrTarjeta-estado_select').html(mensaje);
          
          if(codigoPostal.length === 5){
            $('#id-agrTarjeta-codigo_postal_alert_info_2').slideUp('fast');
          }else{
            $('#id-agrTarjeta-codigo_postal_alert_info_1').slideUp('fast');
            $('#id-agrTarjeta-codigo_postal_alert_info_2').slideDown('fast');
          }
        }else if(respuesta === "4"){
          $('#id-agrTarjeta-estado_select').val('');
          
          if(codigoPostal.length === 5){
            $('#id-agrTarjeta-codigo_postal_alert_info_2').slideUp('fast');
          }else{
            $('#id-agrTarjeta-codigo_postal_alert_info_1').slideUp('fast');
            $('#id-agrTarjeta-codigo_postal_alert_info_2').slideDown('fast');
          }
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-codigo_postal_alert_info_1').slideUp('fast');
      $('#id-agrTarjeta-codigo_postal_alert_info_2').slideUp('fast');
      $('#id-agrTarjeta-estado_select').val('');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "Colonia"
  */
  $(document).on('keyup', '#id-agrTarjeta-colonia_input', function(){
    let colonia = $(this).val();
    
    if(colonia.length !== 0){
      $('#id-agrTarjeta-colonia_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_letras_espacios_simbolos_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: colonia
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-colonia_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-colonia_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-colonia_alert_info').slideUp('fast');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se escribe en el campo "Ciudad o Municipio"
  */
  $(document).on('keyup', '#id-agrTarjeta-ciudad_municipio_input', function(){
    let ciudad_municipio = $(this).val();
    
    if(ciudad_municipio.length !== 0){
      $('#id-agrTarjeta-ciudad_municipio_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      $.ajax({
        url: 'procesos/general_campo_letras_espacios_validacion.php',
        type: 'POST',
        data: {
          accion: "validar",
          texto: ciudad_municipio
        }
      })
      .done(function(data, textStatus, jqXHR){
        let datos = JSON.parse(data),
            respuesta = datos[0].respuesta;

        if(respuesta === "1"){
          $('#id-agrTarjeta-ciudad_municipio_alert_info').slideUp('fast');
        }else if(respuesta === "2"){
          $('#id-agrTarjeta-ciudad_municipio_alert_info').slideDown('fast');
        }
      })
      .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.status + " - " + errorThrown);
      });
    }else{
      $('#id-agrTarjeta-ciudad_municipio_alert_info').slideUp('fast');
    }
  });
  /*
  * Agregar tarjeta - Detecta cuando se selecciona el select "Estado"
  */
  $(document).on('change', '#id-agrTarjeta-estado_select', function(){
    let estado = $(this).val();
    
    $('#id-agrTarjeta-estado_alert_info').slideUp('fast');
    
    if(estado.length !== 0){
      $('#id-agrTarjeta-estado_alert_error').slideUp('fast');
      $('#id-agrTarjeta-estado_div').removeClass('p-select_error');
    }else{
      $('#id-agrTarjeta-estado_alert_error').slideDown('fast');
      $('#id-agrTarjeta-estado_div').addClass('p-select_error');
    }
  });
  /*
  * Agregar tarjeta - Evento submit para formulario "Agregar tarjeta"
  */
  $(document).on('submit', '#id-agrTarjeta-form', function(e){
    let nom_titu_val = $('#id-agrTarjeta-nom_titu_input').val(),
        num_tarjeta_val = $('#id-agrTarjeta-num_tar_input').val(),
        tipo_tarjeta_val = $('#id-agrTarjeta-tipo_tarjeta_span').text(),
        mes_vencimiento_val = $('#id-agrTarjeta-mes_venc_input').val(),
        anio_vencimiento_val = $('#id-agrTarjeta-anio_venc_input').val(),
        cvc_val = $('#id-agrTarjeta-cvc_input').val(),
        tipo_vialidad_val = $('#id-agrTarjeta-tipo_vialidad_select option:selected').val(),
        nom_vialidad_val = $('#id-agrTarjeta-nom_vialidad_input').val(),
        no_ext_val = $('#id-agrTarjeta-no_ext_input').val(),
        no_ext_check_val = $('#id-agrTarjeta-no_ext_checkbox').prop('checked'),
        no_int_val = $('#id-agrTarjeta-no_int_input').val(),
        no_int_check_val = $('#id-agrTarjeta-no_int_checkbox').prop('checked'),
        codigo_postal_val = $('#id-agrTarjeta-codigo_postal_input').val(),
        colonia_val = $('#id-agrTarjeta-colonia_input').val(),
        ciudad_municipio_val = $('#id-agrTarjeta-ciudad_municipio_input').val(),
        estado_val = $('#id-agrTarjeta-estado_select option:selected').val();

    $.ajax({
      url: 'procesos/general_validarCampos_tarjeta.php',
      type: 'POST',
      data: {
        accion: "validar",
        nom_titu: nom_titu_val,
        num_tarjeta: num_tarjeta_val,
        tipo_tarjeta: tipo_tarjeta_val,
        mes_vencimiento: mes_vencimiento_val,
        anio_vencimiento: anio_vencimiento_val,
        cvc: cvc_val,
        tipo_vialidad: tipo_vialidad_val,
        nom_vialidad: nom_vialidad_val,
        no_ext: no_ext_val,
        no_ext_estadoCheck: no_ext_check_val,
        no_int: no_int_val,
        no_int_estadoCheck: no_int_check_val,
        codigo_postal: codigo_postal_val,
        colonia: colonia_val,
        ciudad_municipio: ciudad_municipio_val,
        estado: estado_val
      }
    })
    .done(function(data, textStatus, jqXHR){
      let datos = JSON.parse(data),
          respuesta = datos[0].respuesta,
          mensaje = datos[0].mensaje,
          nom_tipoVialidad = datos[0].nom_tipoVialidad,
          noExterior = datos[0].noExterior,
          noInterior = datos[0].noInterior,
          nom_estado = datos[0].nom_estado;

      if(respuesta === "0"){
        console.log(mensaje);
      }else if(respuesta === "1"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-nom_titu_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-nom_titu_alert_error').slideDown('fast');
        $('#id-agrTarjeta-nom_titu_input').addClass('p-input_error');
        $('#id-agrTarjeta-nom_titu_input').focus();

      }else if(respuesta === "2"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-nom_titu_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-nom_titu_alert_info').slideDown('fast');
        $('#id-agrTarjeta-nom_titu_input').focus();

      }else if(respuesta === "3"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-num_tar_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-num_tar_alert_error').slideDown('fast');
        $('#id-agrTarjeta-num_tar_input').addClass('p-input_error');
        $('#id-agrTarjeta-num_tar_input').focus();

      }else if(respuesta === "4"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-num_tar_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-num_tar_alert_info_1').slideDown('fast');
        $('#id-agrTarjeta-num_tar_input').focus();

      }else if(respuesta === "5"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-tipo_tarjeta_span').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-tipo_tarjeta_alert_info_1').slideDown('fast');

      }else if(respuesta === "6"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-tipo_tarjeta_span').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-tipo_tarjeta_alert_info_2').slideDown('fast');

      }else if(respuesta === "7"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-mes_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-mes_venc_alert_error').slideDown('fast');
        $('#id-agrTarjeta-mes_venc_input').addClass('p-input_error');
        $('#id-agrTarjeta-mes_venc_input').focus();

      }else if(respuesta === "8"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-mes_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-mes_venc_alert_info_1').slideDown('fast');
        $('#id-agrTarjeta-mes_venc_input').focus();

      }else if(respuesta === "9"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-mes_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-mes_venc_alert_info_2').slideDown('fast');
        $('#id-agrTarjeta-mes_venc_input').focus();

      }else if(respuesta === "10"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-anio_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-anio_venc_alert_error').slideDown('fast');
        $('#id-agrTarjeta-anio_venc_input').addClass('p-input_error');
        $('#id-agrTarjeta-anio_venc_input').focus();

      }else if(respuesta === "11"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-anio_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-anio_venc_alert_info_1').slideDown('fast');
        $('#id-agrTarjeta-anio_venc_input').focus();

      }else if(respuesta === "12"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-anio_venc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-anio_venc_alert_info_2').slideDown('fast');
        $('#id-agrTarjeta-anio_venc_input').focus();

      }else if(respuesta === "13"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-cvc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-cvc_alert_error').slideDown('fast');
        $('#id-agrTarjeta-cvc_input').addClass('p-input_error');
        $('#id-agrTarjeta-cvc_input').focus();

      }else if(respuesta === "14"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-cvc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-cvc_alert_info_1').slideDown('fast');
        $('#id-agrTarjeta-cvc_input').focus();

      }else if(respuesta === "15"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-cvc_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-cvc_alert_info_2').slideDown('fast');
        $('#id-agrTarjeta-cvc_input').focus();

      }else if(respuesta === "16"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-tipo_vialidad_select').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-tipo_vialidad_alert_error').slideDown('fast');
        $('#id-agrTarjeta-tipo_vialidad_div').addClass('p-select_error');
        $('#id-agrTarjeta-tipo_vialidad_select').focus();

      }else if(respuesta === "17"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-tipo_vialidad_select').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-tipo_vialidad_alert_info').slideDown('fast');
        $('#id-agrTarjeta-tipo_vialidad_select').focus();

      }else if(respuesta === "18"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-nom_vialidad_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-nom_vialidad_alert_error').slideDown('fast');
        $('#id-agrTarjeta-nom_vialidad_input').addClass('p-input_error');
        $('#id-agrTarjeta-nom_vialidad_input').focus();

      }else if(respuesta === "19"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-nom_vialidad_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-nom_vialidad_alert_info').slideDown('fast');
        $('#id-agrTarjeta-nom_vialidad_input').focus();

      }else if(respuesta === "20"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-no_ext_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-no_ext_alert_error').slideDown('fast');
        $('#id-agrTarjeta-no_ext_input').addClass('p-input_error');
        $('#id-agrTarjeta-no_ext_input').focus();

      }else if(respuesta === "21"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-no_ext_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-no_ext_alert_info').slideDown('fast');
        $('#id-agrTarjeta-no_ext_input').focus();

      }else if(respuesta === "22"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-no_int_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-no_int_alert_error').slideDown('fast');
        $('#id-agrTarjeta-no_int_input').addClass('p-input_error');
        $('#id-agrTarjeta-no_int_input').focus();

      }else if(respuesta === "23"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-no_int_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-no_int_alert_info').slideDown('fast');
        $('#id-agrTarjeta-no_int_input').focus();

      }else if(respuesta === "24"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-codigo_postal_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-codigo_postal_alert_error').slideDown('fast');
        $('#id-agrTarjeta-codigo_postal_input').addClass('p-input_error');
        $('#id-agrTarjeta-codigo_postal_input').focus();

      }else if(respuesta === "25"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-codigo_postal_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-codigo_postal_alert_info_1').slideDown('fast');
        $('#id-agrTarjeta-codigo_postal_input').focus();

      }else if(respuesta === "26"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-codigo_postal_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-codigo_postal_alert_info_2').slideDown('fast');
        $('#id-agrTarjeta-codigo_postal_input').focus();

      }else if(respuesta === "27"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-colonia_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-colonia_alert_error').slideDown('fast');
        $('#id-agrTarjeta-colonia_input').addClass('p-input_error');
        $('#id-agrTarjeta-colonia_input').focus();

      }else if(respuesta === "28"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-colonia_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-colonia_alert_info').slideDown('fast');
        $('#id-agrTarjeta-colonia_input').focus();

      }else if(respuesta === "29"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-ciudad_municipio_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-ciudad_municipio_alert_error').slideDown('fast');
        $('#id-agrTarjeta-ciudad_municipio_input').addClass('p-input_error');
        $('#id-agrTarjeta-ciudad_municipio_input').focus();

      }else if(respuesta === "30"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-ciudad_municipio_input').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-ciudad_municipio_alert_info').slideDown('fast');
        $('#id-agrTarjeta-ciudad_municipio_input').focus();

      }else if(respuesta === "31"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-estado_select').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-estado_alert_error').slideDown('fast');
        $('#id-agrTarjeta-estado_div').addClass('p-select_error');
        $('#id-agrTarjeta-estado_select').focus();

      }else if(respuesta === "32"){
        if(seccion === "modificar_info"){
          $('html, body').animate(
            {
              scrollTop: $('document, #id-agrTarjeta-estado_select').offset().top - 100
            }, 600);
        }

        $('#id-agrTarjeta-estado_alert_info').slideDown('fast');
        $('#id-agrTarjeta-estado_select').focus();

      }else if(respuesta === "33"){
        let proceso_completo = false;
        
        if(OpenPay.card.validateCardNumber(num_tarjeta_val)){
          proceso_completo = true;
        }else{
          if(seccion === "modificar_info"){
            $('html, body').animate(
              {
                scrollTop: $('document, #id-agrTarjeta-num_tar_input').offset().top - 100
              }, 600);
          }
          
          setTimeout(function(){
            $('#id-agrTarjeta-num_tar_alert_info_2').slideDown('fast');
            $('#id-agrTarjeta-num_tar_input').focus();
          }, 300);
        }
        
        if(proceso_completo){
          if(OpenPay.card.validateExpiry(mes_vencimiento_val, anio_vencimiento_val)){
            proceso_completo = true;
          }else{
            if(seccion === "modificar_info"){
              $('html, body').animate(
                {
                  scrollTop: $('document, #id-agrTarjeta-anio_venc_input').offset().top - 100
                }, 600);
            }
            
            setTimeout(function(){
              $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideDown('fast');
              $('#id-agrTarjeta-anio_venc_input').focus();
            }, 300);
            
            proceso_completo = false;
          }
        }
        
        if(proceso_completo){
          if(OpenPay.card.validateCVC(cvc_val, num_tarjeta_val)){
            proceso_completo = true;
          }else{
            if(seccion === "modificar_info"){
              $('html, body').animate(
                {
                  scrollTop: $('document, #id-agrTarjeta-cvc_input').offset().top - 100
                }, 600);
            }
            
            setTimeout(function(){
              $('#id-agrTarjeta-cvc_alert_info_3').slideDown('fast');
              $('#id-agrTarjeta-cvc_input').focus();
            }, 300);
            
            proceso_completo = false;
          }
        }
        
        if(proceso_completo){
          $('#id-agrTarjeta-num_tar_alert_info_2').slideUp('fast');
          $('#id-agrTarjeta-fecha_vencimiento_alert_info').slideUp('fast');
          $('#id-agrTarjeta-cvc_alert_info_3').slideUp('fast');
          
          if(seccion === "modificar_info"){
            $('html, body').animate({ scrollTop: 0 }, 600);
          }
          
          let linea1 = nom_tipoVialidad + " " + nom_vialidad_val + " No. Ext. " + noExterior + " No. Int. " + noInterior;

          $('document, #id-agrTarjeta_loading').slideDown('fast');
          
          if(seccion === "modificar_info"){
            $("html, body").animate({ scrollTop: 0 }, 600);
          }
          
          if(seccion === "comprar"){
            $('document, .p-comprar-modal_cont_campos').animate({ scrollTop: 0 }, 600);
          }

          OpenPay.token.create({
            "card_number": num_tarjeta_val,
            "holder_name": nom_titu_val,
            "expiration_year": anio_vencimiento_val,
            "expiration_month": mes_vencimiento_val,
            "cvv2": cvc_val,
            "address": {
              "line1": linea1,
              "line2": "",
              "line3": colonia_val,
              "postal_code": codigo_postal_val,
              "city": ciudad_municipio_val,
              "state": nom_estado,
              "country_code": "MX"
            }
          },
          function(response){ // En caso de que sea correcto
            let token_id = response.data.id;

            $("#id-tokenID").val(token_id);
            
            $.ajax({
              url: 'procesos/general_addTar.php',
              type: 'POST',
              data: {
                accion: "agregar",
                nom_titu: nom_titu_val,
                num_tarjeta: num_tarjeta_val,
                tipo_tarjeta: tipo_tarjeta_val,
                mes_vencimiento: mes_vencimiento_val,
                anio_vencimiento: anio_vencimiento_val,
                cvc: cvc_val,
                id_tipo_vialidad: tipo_vialidad_val,
                nom_tipo_vialidad: nom_tipoVialidad,
                nom_vialidad: nom_vialidad_val,
                no_ext: noExterior,
                no_int: noInterior,
                codigo_postal: codigo_postal_val,
                colonia: colonia_val,
                ciudad_municipio: ciudad_municipio_val,
                id_estado: estado_val,
                nom_estado: nom_estado,
                linea1: linea1,
                token_id: token_id,
                DSID: $('document, #deviceSessionId').val()
              }
            })
            .done(function(data, textStatus, jqXHR){
              let datos = JSON.parse(data),
                  respuesta = datos[0].respuesta,
                  mensaje = datos[0].mensaje;

              if(respuesta === "0"){
                console.log(mensaje);
              }else if(respuesta === "1"){
                $('document, #id-agrTarjeta-msg_info_span').html(mensaje);
                
                $('document, #id-agrTarjeta_loading').slideUp('fast');
                $('#id-agrTarjeta-msg_info').slideDown('fast');

                setTimeout(function(){
                  $('#id-agrTarjeta-msg_info').slideUp('fast');
                }, 10000);
              }else if(respuesta === "2"){
                $('document, #id-agrTarjeta-msg_error_span').html(mensaje);
                
                $('document, #id-agrTarjeta_loading').slideUp('fast');
                $('#id-agrTarjeta-msg_error').slideDown('fast');

                setTimeout(function(){
                  $('#id-agrTarjeta-msg_error').slideUp('fast');
                }, 10000);
              }else if(respuesta === "3"){
                $('document, #id-agrTarjeta_loading').slideUp('fast');
                $('#id-agrTarjeta-status').slideDown('fast');
                
                if(seccion === "modificar_info"){
                  setTimeout(function(){
                    history.go(-1);
                  }, 1000);
                }

                if(seccion === "comprar"){
                  setTimeout(function(){
                    $('#id-agrTarjeta-status').slideUp('fast');
                    mostrar_tarjetas_credito_debito();
                    $('html').toggleClass('p-modal-derecho_window_no_scroll');
                    $('#id-comprar-modal_general').slideToggle('fast');
                    $('#id-comprar-modal_general_contenido').toggleClass('p-modal-derecho_contenedor_activo');
                    $('#id-comprar-modal_general_contElementos').html('');
                  }, 1000);
                }
              }
            })
            .fail(function(jqXHR, textStatus, errorThrown){
              console.log(jqXHR.status + " - " + errorThrown);
            });
          },
          function(response){ // En caso de que suceda un error
            let desc = response.data.descripcion != undefined ? response.data.descripcion : response.message;

            console.log("ERROR [" + response.status + "] " + desc + " - " + response.data.error_code);
          });
        }
      }
    })
    .fail(function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.status + " - " + errorThrown);
    });

    e.preventDefault();
  });
  /*
  * Metodo de pago - Funcion para mostrar Tarjeta(s)
  */
  function mostrar_tarjetas_credito_debito(){
    $('#T-comprar_check_tar').val('');
    $('#T-comprar_cvc_tar').val('');
    $('#T-comprar_OP_Cli').val('');
    $('#T-comprar_OP_Tar').val('');
    $('#id-comprar-detalles_pago_p').html('Elegiste pagar con tarjeta, pero aún no la seleccionas.');
    
    let data = new FormData();

    data.append('accion', 'mostrar');

    fetch('procesos/comprar/mostrar_tarjetas_credito_debito.php', {
      method: 'POST',
      body: data
    })
    .then(response => {
      if(response.ok){
        return response.json();
      }else{
        return [{
          "respuesta": "status",
          "mensaje": response.status
        }];
      }
    })
    .then(datos => {
      let respuesta = datos[0].respuesta,
          mensaje = datos[0].mensaje;

      switch(respuesta){
        case 'status':
          console.log("Status code: ", mensaje);
          break;

        case '1':
          $('#id-comprar-load_datos_tar').slideUp('fast');
          $('#id-comprar-contenedor_tarjeta_datos').html(mensaje);
          break;
      }
    })
    .catch(error => {
      console.error("Ocurrió un error en la petición: ", error);
    });

    /*$.ajax({
      url: 'procesos/comprar/mostrar_tarjetas_credito_debito.php',
      type: 'POST',
      data: {
        accion: "mostrar"
      }
    })
    .done(function(data, textStatus, jqXHR){
      $('#id-comprar-load_datos_tar').slideUp('fast');
      $('#id-comprar-contenedor_tarjeta_datos').html(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown){
      console.log(jqXHR.status + " - " + errorThrown);
    });*/
  }
});