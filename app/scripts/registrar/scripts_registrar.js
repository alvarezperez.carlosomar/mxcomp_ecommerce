$(document).ready(function(){
  /*
  * Input - Detecta cuando se escribe en el campo "Nombre(s)"
  */
  $(document).on('keyup', '.g-registrar-nombres_input', function(){
    let nombres = $(this).val(), data = new FormData();
    
    if(nombres.length !== 0){
      $('document, .g-registrar-nombres_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombres);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-registrar-nombres_alert_info').slideUp('fast') : $('document, .g-registrar-nombres_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-nombres_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Apellido paterno"
  */
  $(document).on('keyup', '.g-registrar-apellido_paterno_input', function(){
    let apellido_paterno = $(this).val(), data = new FormData();
    
    if(apellido_paterno.length !== 0){
      $('document, .g-registrar-apellido_paterno_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', apellido_paterno);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-registrar-apellido_paterno_alert_info').slideUp('fast') : $('document, .g-registrar-apellido_paterno_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-apellido_paterno_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Apellido materno"
  */
  $(document).on('keyup', '.g-registrar-apellido_materno_input', function(){
    let apellido_materno = $(this).val(), data = new FormData();
    
    if(apellido_materno.length !== 0){
      $('document, .g-registrar-apellido_materno_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', apellido_materno);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-apellido_materno_alert_info').slideUp('fast') : $('document, .g-registrar-apellido_materno_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-apellido_materno_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta cuando se desenfoca el campo "Correo electronico"
  */
  var correo_success = "";
  $(document).on('blur', '.g-registrar-correo_electronico_input', function(){
    let correo = $(this).val(), data = new FormData();

    $('document, .g-registrar-span_notificacion').text('');
    if(correo.length !== 0){
      data.append('accion', 'validar');
      data.append('correo', correo);

      fetch('procesos/general_email_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;

        switch(respuesta){
          case '1':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideDown('fast');
            $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
            break;

          case '2':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
            break;

          case '3':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_error_1').slideDown('fast');
            $('document, .g-registrar-correo_electronico_input').addClass('p-input_error');
            correo_success = correo;
            break;

          case '4':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_success').slideDown('fast');
            $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
            correo_success = correo;
            $('document, .g-registrar-span_notificacion').text(correo);
            break;
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }
  });
  /*
  * Input - Detecta cuando se escribe en el campo "Correo electronico"
  */
  $(document).on('keyup', '.g-registrar-correo_electronico_input', function(){
    let correo = $(this).val(), data = new FormData();

    // SI TIENE DATOS, HARÁ LO SIGUIENTE
    if(correo.length !== 0){
      $('document, .g-registrar-correo_electronico_alert_error_2').slideUp('fast');
      $('document, .g-registrar-correo_electronico_alert_info_2').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        switch(respuesta){
          case '1':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
            if($('document, .g-registrar-correo_electronico_alert_success').css('display') === 'none' || correo_success !== correo){
              $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
            }
            if($('document, .g-registrar-correo_electronico_alert_error_1').css('display') === 'none' || correo_success !== correo){
              $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
            }
            break;

          case '2':
            $('document, .g-registrar-correo_electronico_alert_info_1').slideDown('fast');
            $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
            $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
            break;
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{ // SI NO TIENE, HARÁ LO SIGUIENTE
      $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
      $('document, .g-registrar-correo_electronico_alert_error_1').slideUp('fast');
      $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Contraseña"
  */
  $(document).on('keyup', '.g-registrar-password_input', function(){
    let password = $(this).val(), confirmar_password = $('document, .g-registrar-confirmar_password_input').val(), data = new FormData();

    if(password.length !== 0){
      $('document, .g-registrar-password_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'password');
      data.append('password', password);

      fetch('procesos/general_password_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, width = datos.width, color = datos.color;

        respuesta === "1" || respuesta === "3" ? $('document, .g-registrar-password_alert_info').slideUp('fast') : $('document, .g-registrar-password_alert_info').slideDown('fast');

        $('document, .g-password-porcentaje_seguridad').text(width);
        $('document, .g-password-barra_progreso').css({
          "width": width,
          "background-color": color
        });
      }).catch(error => {
        console.error('[Error] - ', error);
      });

      if(confirmar_password !== ""){
        password === confirmar_password ? $('document, .g-registrar-confirmar_password_alert_info_2').slideUp('fast') : $('document, .g-registrar-confirmar_password_alert_info_2').slideDown('fast');
      }
    }else{
      $('document, .g-registrar-password_alert_info').slideUp('fast');
      $('document, .g-password-porcentaje_seguridad').text('0%');
      $('document, .g-password-barra_progreso').css({
        "width": "0%",
        "background-color": "#D9534F"
      });
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Confirmar contraseña" y si son iguales las contraseñas
  */
  $(document).on('keyup', '.g-registrar-confirmar_password_input', function(){
    let password = $('document, .g-registrar-password_input').val(), confirmar_password = $(this).val(), data = new FormData();

    if(confirmar_password.length !== 0){
      $('document, .g-registrar-confirmar_password_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', confirmar_password);

      fetch('validaciones/validar_password.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-confirmar_password_alert_info_1').slideUp('fast') : $('document, .g-registrar-confirmar_password_alert_info_1').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });

      password === confirmar_password ? $('document, .g-registrar-confirmar_password_alert_info_2').slideUp('fast') : $('document, .g-registrar-confirmar_password_alert_info_2').slideDown('fast');
    }else{
      $('document, .g-registrar-confirmar_password_alert_info_1').slideUp('fast');
      $('document, .g-registrar-confirmar_password_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Radio button - Detecta si se dio click en alguna opcion de "sexo"
  */
  $(document).on('click', '.g-registrar-sexo_radio', function(){
    $('document, .g-registrar-sexo_alert_info').slideUp('fast');
    $('document, .g-registrar-sexo_alert_error').slideUp('fast');
  });
  /*
  * Date - Detecta si ya no esta enfocado el campo "Fecha de nacimiento"
  */
  $(document).on('blur', '.g-registrar-fecha_nacimiento_input', function(){
    let fecha_nacimiento = $(this).val();
    
    if(fecha_nacimiento.length !== 0){
      $('document, .g-registrar-fecha_nacimiento_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Date - Detecta si se escribe en el campo "Fecha de nacimiento"
  */
  $(document).on('keyup', '.g-registrar-fecha_nacimiento_input', function(){
    let fecha_nacimiento = $(this).val();
    
    if(fecha_nacimiento.length !== 0){
      $('document, .g-registrar-fecha_nacimiento_alert_error').slideUp('fast');
      $(this).removeClass('p-date_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefonico #1"
  */
  $(document).on('keyup', '.g-registrar-no_telefonico_1_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      $('document, .g-registrar-no_telefonico_1_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-registrar-no_telefonico_1_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-registrar-no_telefonico_1_alert_info_2').slideUp('fast') : $('document, .g-registrar-no_telefonico_1_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-registrar-no_telefonico_1_alert_info_1').slideDown('fast'),
          $('document, .g-registrar-no_telefonico_1_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-no_telefonico_1_alert_info_1').slideUp('fast');
      $('document, .g-registrar-no_telefonico_1_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico #2"
  */
  $(document).on('keyup', '.g-registrar-no_telefonico_2_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      data.append('accion', 'validar');
      data.append('valor', no_telefonico);
    
      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-registrar-no_telefonico_2_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-registrar-no_telefonico_2_alert_info_2').slideUp('fast') : $('document, .g-registrar-no_telefonico_2_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-registrar-no_telefonico_2_alert_info_1').slideDown('fast'),
          $('document, .g-registrar-no_telefonico_2_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-no_telefonico_2_alert_info_1').slideUp('fast');
      $('document, .g-registrar-no_telefonico_2_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. telefónico #3"
  */
  $(document).on('keyup', '.g-registrar-no_telefonico_3_input', function(){
    let no_telefonico = $(this).val(), data = new FormData();
    
    if(no_telefonico.length !== 0){
      data.append('accion', 'validar');
      data.append('valor', no_telefonico);

      fetch('validaciones/validar_campo_numerico.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? (
          $('document, .g-registrar-no_telefonico_3_alert_info_1').slideUp('fast'),
          no_telefonico.length === 10 ? $('document, .g-registrar-no_telefonico_3_alert_info_2').slideUp('fast') : $('document, .g-registrar-no_telefonico_3_alert_info_2').slideDown('fast')
        ) : (
          $('document, .g-registrar-no_telefonico_3_alert_info_1').slideDown('fast'),
          $('document, .g-registrar-no_telefonico_3_alert_info_2').slideUp('fast')
        );
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-no_telefonico_3_alert_info_1').slideUp('fast');
      $('document, .g-registrar-no_telefonico_3_alert_info_2').slideUp('fast');
    }
  });
  /*
  * Button - Boton Agregar del 1er campo
  */
  $(document).on('click', '.g-telefono-agregar_1_button', function(){
    $('document, #g-telefono-contenedor_control_2').slideDown('fast');
    $('document, .g-telefono-agregar_1_button').slideUp('fast');
    $('document, .g-registrar-no_telefonico_2_input').attr('placeholder', 'No. telefónico #2');
    $('document, .g-registrar-no_telefonico_3_input').attr('placeholder', 'No. telefónico #3');
  });
  /*
  * Button - Boton Agregar del 2do campo
  */
  $(document).on('click', '.g-telefono-agregar_2_button', function(){
    let elemento = $(this)[0].parentElement;
    
    $('document, #g-telefono-contenedor_control_3').slideDown('fast');
    $('document, .g-telefono-agregar_2_button').slideUp('fast');
    $(elemento).removeClass('p-buttons_margin_left');
  });
  /*
  * Button - Boton Eliminar del 2do campo
  */
  $(document).on('click', '.g-telefono-borrar_1_button', function(){
    $('document, .g-registrar-no_telefonico_2_alert_info_1').slideUp('fast');
    $('document, .g-telefono-agregar_1_button').slideDown('fast');
    $('document, #g-telefono-contenedor_control_2').slideUp('fast');
    $('document, .g-registrar-no_telefonico_2_input').val('');
    $('document, .g-registrar-no_telefonico_3_input').attr('placeholder', 'No. telefónico #2');
  });
  /*
  * Button - Boton Eliminar del 3er campo
  */
  $(document).on('click', '.g-telefono-borrar_2_button', function(){
    let elemento = $('document, .g-telefono-agregar_2_button')[0].parentElement;
    
    $('document, .g-registrar-no_telefonico_3_alert_info_1').slideUp('fast');
    $(elemento).addClass('p-buttons_margin_left');
    $('document, .g-telefono-agregar_2_button').slideDown('fast');
    $('document, #g-telefono-contenedor_control_3').slideUp('fast');
    $('document, .g-registrar-no_telefonico_3_input').val('');
  });
  /*
  * Button - Desplegar campos para "Domicilio particular"
  */
  $(document).on('click', '#g-boton-siguiente_button', function(){
    $('document, .p-registrar-div_datos_1').slideUp(600);
    $('document, .p-registrar-div_datos_2').slideDown(600);
    $('html, body').animate({ scrollTop: 0 }, 1200);
    $('document, .g-registrar-tipo_vialidad_select').focus();
  });
  /*
  * Button - Desplegar campos para "Datos generales"
  */
  $(document).on('click', '#g-boton-atras_button', function(){
    $('document, .p-registrar-div_datos_2').slideUp(600);
    $('document, .p-registrar-div_datos_1').slideDown(600);
    $('document, .g-registrar-nombres_input').focus();
  });
  /*
  * Select - Detecta cuando se selecciona el select "Tipo de vialidad"
  */
  $(document).on('change', '.g-registrar-tipo_vialidad_select', function(){
    let tipo_vialidad = $(this).val();
    
    $('document, .g-registrar-tipo_vialidad_alert_info').slideUp('fast');
    
    if(tipo_vialidad.length !== 0){
      $('document, .g-registrar-tipo_vialidad_alert_error').slideUp('fast');
      $('document, .g-registrar-tipo_vialidad_div').removeClass('p-select_error');
    }else{
      $('document, .g-registrar-tipo_vialidad_alert_error').slideDown('fast');
      $('document, .g-registrar-tipo_vialidad_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Nombre de vialidad"
  */
  $(document).on('keyup', '.g-registrar-nombre_vialidad_input', function(){
    let nombre_vialidad = $(this).val(), data = new FormData();
    
    if(nombre_vialidad.length !== 0){
      $('document, .g-registrar-nombre_vialidad_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_vialidad);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-nombre_vialidad_alert_info').slideUp('fast') : $('document, .g-registrar-nombre_vialidad_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-nombre_vialidad_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. exterior"
  */
  $(document).on('keyup', '.g-registrar-no_exterior_input', function(){
    let no_exterior = $(this).val(), data = new FormData();
    
    if(no_exterior.length !== 0){
      $('document, .g-registrar-no_exterior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_exterior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-no_exterior_alert_info').slideUp('fast') : $('document, .g-registrar-no_exterior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-no_exterior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "No. interior"
  */
  $(document).on('keyup', '.g-registrar-no_interior_input', function(){
    let no_interior = $(this).val(), data = new FormData();
    
    if(no_interior.length !== 0){
      $('document, .g-registrar-no_interior_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', no_interior);

      fetch('validaciones/validar_no_ext_int.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-no_interior_alert_info').slideUp('fast') : $('document, .g-registrar-no_interior_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-no_interior_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Código postal"
  */
  $(document).on('keyup', '.g-registrar-codigo_postal_input', function(){
    let codigo_postal = $(this).val(), data = new FormData();

    if(codigo_postal.length !== 0){
      $('document, .g-registrar-codigo_postal_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'generar');
      data.append('codigo_postal', codigo_postal);

      fetch('procesos/general_codigoPostal_proceso.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta, mensaje = datos.mensaje;

        if(respuesta === "0"){
          console.log(mensaje);
        }else if(respuesta === "1"){
          $('document, .g-registrar-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-registrar-codigo_postal_alert_info_2').slideUp('fast');
        }else if(respuesta === "2" || respuesta === "3" || respuesta === "4"){
          respuesta === "4" ? $('document, .g-registrar-estado_select').val('') :  $('document, .g-registrar-estado_select').html(mensaje);
          
          if(codigo_postal.length === 5){
            $('document, .g-registrar-codigo_postal_alert_info_2').slideUp('fast');
          }else{
            $('document, .g-registrar-codigo_postal_alert_info_1').slideUp('fast');
            $('document, .g-registrar-codigo_postal_alert_info_2').slideDown('fast');
          }
        }
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-codigo_postal_alert_info_1').slideUp('fast');
      $('document, .g-registrar-codigo_postal_alert_info_2').slideUp('fast');
      $('document, .g-registrar-estado_select').val('');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Colonia"
  */
  $(document).on('keyup', '.g-registrar-colonia_input', function(){
    let colonia = $(this).val(), data = new FormData();
    
    if(colonia.length !== 0){
      $('document, .g-registrar-colonia_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', colonia);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-colonia_alert_info').slideUp('fast') : $('document, .g-registrar-colonia_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-colonia_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Ciudad o municipio"
  */
  $(document).on('keyup', '.g-registrar-ciudad_municipio_input', function(){
    let ciudad_municipio = $(this).val(), data = new FormData();
    
    if(ciudad_municipio.length !== 0){
      $('document, .g-registrar-ciudad_municipio_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', ciudad_municipio);

      fetch('validaciones/validar_campo_letras_espacios.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-ciudad_municipio_alert_info').slideUp('fast') : $('document, .g-registrar-ciudad_municipio_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-ciudad_municipio_alert_info').slideUp('fast');
    }
  });
  /*
  * Select - Detecta cuando se selecciona el select "Estado"
  */
  $(document).on('change', '.g-registrar-estado_select', function(){
    let estado = $(this).val();
    
    $('document, .g-registrar-estado_alert_info').slideUp('fast');

    if(estado.length !== 0){
      $('document, .g-registrar-estado_alert_error').slideUp('fast');
      $('document, .g-registrar-estado_div').removeClass('p-select_error');
    }else{
      $('document, .g-registrar-estado_alert_error').slideDown('fast');
      $('document, .g-registrar-estado_div').addClass('p-select_error');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #1"
  */
  $(document).on('keyup', '.g-registrar-entre_calles_1_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-registrar-entre_calles_1_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-entre_calles_1_alert_info').slideUp('fast') : $('document, .g-registrar-entre_calles_1_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-entre_calles_1_alert_info').slideUp('fast');
    }
  });
  /*
  * Input - Detecta si se escribe en el campo "Entre calles #2"
  */
  $(document).on('keyup', '.g-registrar-entre_calles_2_input', function(){
    let nombre_calle = $(this).val(), data = new FormData();
    
    if(nombre_calle.length !== 0){
      $('document, .g-registrar-entre_calles_2_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', nombre_calle);

      fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
          
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;
        
        respuesta === '1' ? $('document, .g-registrar-entre_calles_2_alert_info').slideUp('fast') : $('document, .g-registrar-entre_calles_2_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-registrar-entre_calles_2_alert_info').slideUp('fast');
    }
  });
  /*
  * Textarea - Detecta si se teclea Enter en el campo "Referencias adicionales"
  */
  $(document).on('keypress', '.g-registrar-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      return false;
    }
  });
  /*
  * Textarea - Detecta si se escribe en el campo "Referencias adicionales"
  */
  $(document).on('keyup', '.g-registrar-referencias_adicionales_textarea', function(e){
    if(e.keyCode === 13){
      $('document, .g-registrar-cuenta_button').click();
    }else{
      let referencias_adicionales = $(this).val(), data = new FormData();
      
      if(referencias_adicionales.length !== 0){
        data.append('accion', 'validar');
        data.append('valor', referencias_adicionales);

        fetch('validaciones/validar_campo_letras_espacios_simbolos.php', {
          method: 'POST',
          body: data
        }).then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);
            
          return response.json();
        }).then(datos => {
          let respuesta = datos.respuesta;

          respuesta === '1' ? $('document, .g-registrar-referencias_adicionales_alert_info').slideUp('fast') : $('document, .g-registrar-referencias_adicionales_alert_info').slideDown('fast');
        }).catch(error => {
          console.error('[Error] - ', error);
        });
      }else{
        $('document, .g-registrar-referencias_adicionales_alert_info').slideUp('fast');
      }
    }
  });
  /*
  * Checkbox - Detecta si se cambio el estado de "Acepto los términos y condiciones, así como el aviso de privacidad"
  */
  $(document).on('change', '.g-registrar-termCond_aviPri_checkbox', function(){
    $(this).is(':checked') ? $('document, .g-registrar-termCond_aviPri_alert_error').slideUp('fast') : $('document, .g-registrar-termCond_aviPri_alert_error').slideDown('fast');
  });
  /*
  * Submit - Evento submit para formulario "Registrar"
  */
  $(document).on('submit', '.g-registrar-form', function(e){
    $('document, .g-registrar-loading').slideDown('fast');
    $('document, .g-registrar-cuenta_button').prop('type', 'button');
    $('document, .g-registrar-cuenta_button').prop('disabled', true);
    $('document, .g-registrar-correo_electronico_alert_info_1').slideUp('fast');
    $('document, .g-aviso-registro_exitoso').slideUp('fast');

    let nombres = $('document, .g-registrar-nombres_input').val(),
        apellido_paterno = $('document, .g-registrar-apellido_paterno_input').val(),
        apellido_materno = $('document, .g-registrar-apellido_materno_input').val(),
        correo = $('document, .g-registrar-correo_electronico_input').val(),
        password = $('document, .g-registrar-password_input').val(),
        confirmar_password = $('document, .g-registrar-confirmar_password_input').val(),
        sexo = String($('document, input:radio[name=sexo]:checked').val()),
        fecha_nacimiento = $('document, .g-registrar-fecha_nacimiento_input').val(),
        no_telefonico_1 = $('document, .g-registrar-no_telefonico_1_input').val(),
        no_telefonico_2 = $('document, .g-registrar-no_telefonico_2_input').val(),
        no_telefonico_3 = $('document, .g-registrar-no_telefonico_3_input').val(),
        tipo_vialidad = $('document, .g-registrar-tipo_vialidad_select option:selected').val(),
        nombre_vialidad = $('document, .g-registrar-nombre_vialidad_input').val(),
        no_exterior = $('document, .g-registrar-no_exterior_input').val(),
        no_exterior_estado_check = $('document, .g-registrar-no_exterior_checkbox').prop('checked'),
        no_interior = $('document, .g-registrar-no_interior_input').val(),
        no_interior_estado_check = $('document, .g-registrar-no_interior_checkbox').prop('checked'),
        codigo_postal = $('document, .g-registrar-codigo_postal_input').val(),
        colonia = $('document, .g-registrar-colonia_input').val(),
        ciudad_municipio = $('document, .g-registrar-ciudad_municipio_input').val(),
        estado = $('document, .g-registrar-estado_select option:selected').val(),
        entre_calle_1 = $('document, .g-registrar-entre_calles_1_input').val(),
        entre_calle_2 = $('document, .g-registrar-entre_calles_2_input').val(),
        referencias_adicionales = $('document, .g-registrar-referencias_adicionales_textarea').val(),
        captcha = $('document, .g-registrar-captcha').val(),
        termCond_aviPriv = $('document, .g-registrar-termCond_aviPri_checkbox').prop('checked'),
        data = new FormData();

    sexo = sexo === 'undefined' ? "" : sexo;

    if(nombres !== "" && apellido_paterno !== "" && apellido_materno !== "" && correo !== "" && password !== "" && confirmar_password !== "" && sexo !== "" && fecha_nacimiento !== "" && no_telefonico_1 !== "" && tipo_vialidad !== "" && nombre_vialidad !== "" && (no_exterior !== "" || no_exterior_estado_check === true) && (no_interior !== "" || no_interior_estado_check === true) && codigo_postal !== "" && colonia !== "" && ciudad_municipio !== "" && estado !== "" && termCond_aviPriv === true){
      $('html, body').animate({ scrollTop: $('document, .g-regresar-button').offset().top - 0 }, 600);
    }

    data.append('accion', 'registrar');
    data.append('nombres', nombres);
    data.append('apellido_paterno', apellido_paterno);
    data.append('apellido_materno', apellido_materno);
    data.append('correo', correo);
    data.append('password', password);
    data.append('confirmar_password', confirmar_password);
    data.append('sexo', sexo);
    data.append('fecha_nacimiento', fecha_nacimiento);
    data.append('no_telefonico_1', no_telefonico_1);
    data.append('no_telefonico_2', no_telefonico_2);
    data.append('no_telefonico_3', no_telefonico_3);
    data.append('tipo_vialidad', tipo_vialidad);
    data.append('nombre_vialidad', nombre_vialidad);
    data.append('no_exterior', no_exterior);
    data.append('no_exterior_estadoCheck', no_exterior_estado_check);
    data.append('no_interior', no_interior);
    data.append('no_interior_estadoCheck', no_interior_estado_check);
    data.append('codigo_postal', codigo_postal);
    data.append('colonia', colonia);
    data.append('ciudad_municipio', ciudad_municipio);
    data.append('estado', estado);
    data.append('entre_calle_1', entre_calle_1);
    data.append('entre_calle_2', entre_calle_2);
    data.append('referencias_adicionales', referencias_adicionales);
    data.append('captcha', captcha);
    data.append('termCond_aviPriv', termCond_aviPriv);

    fetch('procesos/usuario/proceso_registrarCuenta.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
        
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje, seccion = 0;

      switch(respuesta){
        case '0':
          console.error(mensaje);
          break;

        case '1':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-nombres_input').offset().top - 150 }, 600);
          $('document, .g-registrar-nombres_alert_error').slideDown('fast');
          $('document, .g-registrar-nombres_input').addClass('p-input_error');
          $('document, .g-registrar-nombres_input').focus();
          break;

        case '2':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-nombres_input').offset().top - 150 }, 600);
          $('document, .g-registrar-nombres_alert_info').slideDown('fast');
          $('document, .g-registrar-nombres_input').focus();
          break;

        case '3':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-apellido_paterno_input').offset().top - 100 }, 600);
          $('document, .g-registrar-apellido_paterno_alert_error').slideDown('fast');
          $('document, .g-registrar-apellido_paterno_input').addClass('p-input_error');
          $('document, .g-registrar-apellido_paterno_input').focus();
          break;

        case '4':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-apellido_paterno_input').offset().top - 100 }, 600);
          $('document, .g-registrar-apellido_paterno_alert_info').slideDown('fast');
          $('document, .g-registrar-apellido_paterno_input').focus();
          break;

        case '5':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-apellido_materno_input').offset().top - 100 }, 600);
          $('document, .g-registrar-apellido_materno_alert_error').slideDown('fast');
          $('document, .g-registrar-apellido_materno_input').addClass('p-input_error');
          $('document, .g-registrar-apellido_materno_input').focus();
          break;

        case '6':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-apellido_materno_input').offset().top - 100 }, 600);
          $('document, .g-registrar-apellido_materno_alert_info').slideDown('fast');
          $('document, .g-registrar-apellido_materno_input').focus();
          break;

        case '7':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-correo_electronico_input').offset().top - 100 }, 600);
          $('document, .g-registrar-correo_electronico_alert_error_2').slideDown('fast');
          $('document, .g-registrar-correo_electronico_input').addClass('p-input_error');
          $('document, .g-registrar-correo_electronico_input').focus();
          break;

        case '8':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-correo_electronico_input').offset().top - 100 }, 600);
          $('document, .g-registrar-correo_electronico_alert_info_1').slideDown('fast');
          $('document, .g-registrar-correo_electronico_input').focus();
          break;

        case '9':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-correo_electronico_input').offset().top - 100 }, 600);
          $('document, .g-registrar-correo_electronico_alert_error_1').slideDown('fast');
          $('document, .g-registrar-correo_electronico_input').addClass('p-input_error');
          $('document, .g-registrar-correo_electronico_input').focus();
          
          if($('document, .g-registrar-correo_electronico_alert_success').css('display') === 'block'){
            $('document, .g-registrar-correo_electronico_alert_success').slideUp('fast');
          }
          break;

        case '10':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-password_input').offset().top - 140 }, 600);
          $('document, .g-registrar-password_alert_error').slideDown('fast');
          $('document, .g-registrar-password_input').addClass('p-input_error');
          $('document, .g-registrar-password_input').focus();
          break;

        case '11':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-password_input').offset().top - 140 }, 600);
          $('document, .g-registrar-password_alert_info').slideDown('fast');
          $('document, .g-registrar-password_input').focus();
          break;

        case '12':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-confirmar_password_input').offset().top - 100 }, 600);
          $('document, .g-registrar-confirmar_password_alert_error').slideDown('fast');
          $('document, .g-registrar-confirmar_password_input').addClass('p-input_error');
          $('document, .g-registrar-confirmar_password_input').focus();
          break;

        case '13':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-confirmar_password_input').offset().top - 100 }, 600);
          $('document, .g-registrar-confirmar_password_alert_info_1').slideDown('fast');
          $('document, .g-registrar-confirmar_password_input').focus();
          break;

        case '14':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-confirmar_password_input').offset().top - 100 }, 600);
          $('document, .g-registrar-confirmar_password_alert_info_2').slideDown('fast');
          $('document, .g-registrar-confirmar_password_input').focus();
          break;

        case '15':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-sexo_radio').offset().top - 100 }, 600);
          $('document, .g-registrar-sexo_alert_error').slideDown('fast');
          break;

        case '16':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-sexo_radio').offset().top - 100 }, 600);
          $('document, .g-registrar-sexo_alert_info').slideDown('fast');
          break;

        case '17':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-fecha_nacimiento_input').offset().top - 100 }, 600);
          $('document, .g-registrar-fecha_nacimiento_alert_error').slideDown('fast');
          $('document, .g-registrar-fecha_nacimiento_input').addClass('p-date_error');
          $('document, .g-registrar-fecha_nacimiento_input').focus();
          break;

        case '18':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_1_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_1_alert_error').slideDown('fast');
          $('document, .g-registrar-no_telefonico_1_input').addClass('p-input_error');
          $('document, .g-registrar-no_telefonico_1_input').focus();
          break;

        case '19':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_1_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_1_alert_info_1').slideDown('fast');
          $('document, .g-registrar-no_telefonico_1_input').focus();
          break;

        case '20':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_1_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_1_alert_info_2').slideDown('fast');
          $('document, .g-registrar-no_telefonico_1_input').focus();
          break;

        case '21':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_2_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_2_alert_info_1').slideDown('fast');
          $('document, .g-registrar-no_telefonico_2_input').focus();
          break;

        case '22':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_2_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_2_alert_info_2').slideDown('fast');
          $('document, .g-registrar-no_telefonico_2_input').focus();
          break;

        case '23':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_3_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_3_alert_info_1').slideDown('fast');
          $('document, .g-registrar-no_telefonico_3_input').focus();
          break;

        case '24':
          $('document, .p-registrar-div_datos_1').slideDown(600);
          $('document, .p-registrar-div_datos_2').slideUp(600);

          $('html, body').animate({ scrollTop: $('document, .g-registrar-no_telefonico_3_input').offset().top - 100 }, 600);
          $('document, .g-registrar-no_telefonico_3_alert_info_2').slideDown('fast');
          $('document, .g-registrar-no_telefonico_3_input').focus();
          break;

        case '25':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-tipo_vialidad_select').offset().top - 200 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-tipo_vialidad_select').offset().top - 200 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-tipo_vialidad_alert_error').slideDown('fast');
          $('document, .g-registrar-tipo_vialidad_div').addClass('p-select_error');
          $('document, .g-registrar-tipo_vialidad_select').focus();
          break;

        case '26':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-tipo_vialidad_select').offset().top - 200 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-tipo_vialidad_select').offset().top - 200 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-tipo_vialidad_alert_info').slideDown('fast');
          $('document, .g-registrar-tipo_vialidad_select').focus();
          break;

        case '27':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-nombre_vialidad_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-nombre_vialidad_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-nombre_vialidad_alert_error').slideDown('fast');
          $('document, .g-registrar-nombre_vialidad_input').addClass('p-input_error');
          $('document, .g-registrar-nombre_vialidad_input').focus();
          break;

        case '28':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-nombre_vialidad_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-nombre_vialidad_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-nombre_vialidad_alert_info').slideDown('fast');
          $('document, .g-registrar-nombre_vialidad_input').focus();
          break;

        case '29':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-no_exterior_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-no_exterior_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-no_exterior_alert_error').slideDown('fast');
          $('document, .g-registrar-no_exterior_input').addClass('p-input_error');
          $('document, .g-registrar-no_exterior_input').focus();
          break;

        case '30':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-no_exterior_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-no_exterior_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-no_exterior_alert_info').slideDown('fast');
          $('document, .g-registrar-no_exterior_input').focus();
          break;

        case '31':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-no_interior_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-no_interior_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-no_interior_alert_error').slideDown('fast');
          $('document, .g-registrar-no_interior_input').addClass('p-input_error');
          $('document, .g-registrar-no_interior_input').focus();
          break;

        case '32':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-no_interior_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-no_interior_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-no_interior_alert_info').slideDown('fast');
          $('document, .g-registrar-no_interior_input').focus();
          break;

        case '33':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-codigo_postal_alert_error').slideDown('fast');
          $('document, .g-registrar-codigo_postal_input').addClass('p-input_error');
          $('document, .g-registrar-codigo_postal_input').focus();
          break;

        case '34':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-codigo_postal_alert_info_1').slideDown('fast');
          $('document, .g-registrar-codigo_postal_input').focus();
          break;

        case '35':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-codigo_postal_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-codigo_postal_alert_info_2').slideDown('fast');
          $('document, .g-registrar-codigo_postal_alert_info_1').slideUp('fast');
          $('document, .g-registrar-codigo_postal_input').focus();
          break;

        case '36':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-colonia_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-colonia_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-colonia_alert_error').slideDown('fast');
          $('document, .g-registrar-colonia_input').addClass('p-input_error');
          $('document, .g-registrar-colonia_input').focus();
          break;

        case '37':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-colonia_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-colonia_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-colonia_alert_info').slideDown('fast');
          $('document, .g-registrar-colonia_input').focus();
          break;

        case '38':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-ciudad_municipio_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-ciudad_municipio_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-ciudad_municipio_alert_error').slideDown('fast');
          $('document, .g-registrar-ciudad_municipio_input').addClass('p-input_error');
          $('document, .g-registrar-ciudad_municipio_input').focus();
          break;

        case '39':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-ciudad_municipio_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-ciudad_municipio_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-ciudad_municipio_alert_info').slideDown('fast');
          $('document, .g-registrar-ciudad_municipio_input').focus();
          break;

        case '40':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-estado_select').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-estado_select').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-estado_alert_error').slideDown('fast');
          $('document, .g-registrar-estado_div').addClass('p-select_error');
          $('document, .g-registrar-estado_select').focus();
          break;

        case '41':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-estado_select').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-estado_select').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-estado_alert_info').slideDown('fast');
          $('document, .g-registrar-estado_select').focus();
          break;

        case '42':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_1_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_1_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-entre_calles_1_alert_error').slideDown('fast');
          $('document, .g-registrar-entre_calles_1_input').addClass('p-input_error');
          $('document, .g-registrar-entre_calles_1_input').focus();
          break;

        case '43':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_1_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_1_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-entre_calles_1_alert_info').slideDown('fast');
          $('document, .g-registrar-entre_calles_1_input').focus();
          break;

        case '44':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_2_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_2_input').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-entre_calles_2_alert_error').slideDown('fast');
          $('document, .g-registrar-entre_calles_2_input').addClass('p-input_error');
          $('document, .g-registrar-entre_calles_2_input').focus();
          break;

        case '45':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_2_input').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-entre_calles_2_input').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-entre_calles_2_alert_info').slideDown('fast');
          $('document, .g-registrar-entre_calles_2_input').focus();
          break;

        case '46':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-referencias_adicionales_textarea').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-referencias_adicionales_textarea').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-referencias_adicionales_alert_info').slideDown('fast');
          $('document, .g-registrar-referencias_adicionales_textarea').focus();
          break;

        case '47':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, .g-registrar-termCond_aviPri_alert_error').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, .g-registrar-termCond_aviPri_alert_error').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-termCond_aviPri_alert_error').slideDown('fast');
          break;

        case '48':
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, #id-recaptcha-contenedor').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, #id-recaptcha-contenedor').offset().top - 100 }, 600);
            }, 600);
          }

          $('document, .g-registrar-recaptcha_alert_info').slideDown('fast');
          break;

        case '49':
          grecaptcha.execute(site_key, {action: 'user_register'})
          .then(function(token) {
            $('document, .g-registrar-captcha').val(token);
          });
          
          seccion = 2;
          $('document, .p-registrar-div_datos_1').slideUp(600);
          $('document, .p-registrar-div_datos_2').slideDown(600);
          
          if($('document, .p-registrar-div_datos_1').css('display') === 'none'){
            $('html, body').animate({ scrollTop: $('document, #id-recaptcha-contenedor').offset().top - 100 }, 600);
          }else{
            setTimeout(function(){
              $('html, body').animate({ scrollTop: $('document, #id-recaptcha-contenedor').offset().top - 100 }, 600);
            }, 600);
          }
          
          $('document, .g-registrar-recaptcha_alert_error').slideDown('fast');
          break;

        case '50':
          $('document, .g-registrar-contenedor_informacion').slideUp('fast');
          $('document, .g-regresar-button').slideUp('fast');
          $('document, .g-registrar-contenedor_informacion').html('');
          $('document, .g-aviso-registro_exitoso').slideDown('fast');
          break;
      }

      $('document, .g-registrar-loading').slideUp('fast');
      $('document, .g-registrar-cuenta_button').prop('type', 'submit');
      $('document, .g-registrar-cuenta_button').prop('disabled', false);

      if(seccion === 2){
        if($('document, .g-registrar-recaptcha_alert_info').css('display') === 'block'){
          setTimeout(function(){
            $('document, .g-registrar-recaptcha_alert_info').slideUp('fast');
          }, 3800);
        }
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
});