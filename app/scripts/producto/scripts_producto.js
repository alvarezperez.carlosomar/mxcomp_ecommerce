$(document).ready(function(){
  $(document).on('click', '#id-producto-stars_votacion', function(){
    $('document, #id-producto-stars_div').slideToggle('fast');
    $('document, #id-producto-stars_fondo').slideToggle('fast');
  });

  $(document).on('click', '#id-producto-stars_fondo', function(){
    $('document, #id-producto-stars_div').slideToggle('fast');
    $('document, #id-producto-stars_fondo').slideToggle('fast');
  });
  
  $(document).on('click', '#id-producto-boton_abrir_modal_pdf', function(){
    $('document, #id-producto-contenedor_modal_pdf').css('display', 'block');
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
  });

  $(document).on('click', '.g-producto-boton_cerrar_modal_pdf', function(){
    $('document, #id-producto-contenedor_modal_pdf').css('display', 'none');
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
  });
  /*
  * GALERIA DE IMÁGENES
  */
  var cantidadImagenes = $('document, .p-producto-carrusel_imagenes li').length, imagenPosicion = 1;
  $(document).on('click', '.contenedor_img img', function(){
    let id = $(this).attr('data-id'), data = new FormData();
    
    data.append('accion', 'validar');
    data.append('valor', id);
    
    fetch('validaciones/validar_campo_numerico.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta;
      
      respuesta === '1' ? (
        imagenPosicion = id,
        $('document, #id-producto-carrusel_num_imagenes_span').text(imagenPosicion + '/' + cantidadImagenes),
        $('document, .p-producto-carrusel_imagenes li').hide(),
        $('document, .p-producto-carrusel_imagenes li:nth-child(' + imagenPosicion + ')').fadeIn(),
        $('document, #id-producto-contenedor_modal_galeria').css('display', 'block'),
        $('html').toggleClass('p-modal-derecho_window_no_scroll')
      ) : location.reload();
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Button - Cerrar modal de galería
  */
  $(document).on('click', '#id-producto-boton_cerrar_galeria', function(){
    $('document, #id-producto-contenedor_modal_galeria').css('display', 'none');
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
  });
  /*
  * Div - Cerrar modal de galería - Fondo
  */
  $(document).on('click', '#id-producto-fondo_modal_galeria', function(){
    $('document, #id-producto-contenedor_modal_galeria').css('display', 'none');
    $('html').toggleClass('p-modal-derecho_window_no_scroll');
  });

  $('document, #id-producto-carrusel_num_imagenes_span').text(imagenPosicion + '/' + cantidadImagenes);
  $('document, .p-producto-carrusel_imagenes li').hide();
  $('document, .p-producto-carrusel_imagenes li:first').show();
  $('document, .p-producto-carrusel_flecha_izquierda span').click(anteriorImagen);
  $('document, .p-producto-carrusel_flecha_derecha span').click(siguienteImagen);

  function siguienteImagen(){
    imagenPosicion >= cantidadImagenes ? imagenPosicion = 1 : imagenPosicion++;
    $('document, #id-producto-carrusel_num_imagenes_span').text(imagenPosicion + '/' + cantidadImagenes);
    $('document, .p-producto-carrusel_imagenes li').hide();
    $('document, .p-producto-carrusel_imagenes li:nth-child(' + imagenPosicion + ')').fadeIn();
  }

  function anteriorImagen(){
    imagenPosicion <= 1 ? imagenPosicion = cantidadImagenes : imagenPosicion--;
    $('document, #id-producto-carrusel_num_imagenes_span').text(imagenPosicion + '/' + cantidadImagenes);
    $('document, .p-producto-carrusel_imagenes li').hide();
    $('document, .p-producto-carrusel_imagenes li:nth-child(' + imagenPosicion + ')').fadeIn();
  }
  /*
  * Producto - Funcion para mostrar los elementos del carrito
  */
  setTimeout(function(){ mostrar_elementos(); }, 200);
  /*
  * Producto - Seleccionar texto de input
  */
  $(document).on('click', '.g-producto-cambiar_cantidad_input', function(){
    $(this).select();
  });
  /*
  * Producto - Obtener cantidad escrita
  */
  var timeEjecucion = "";
  $(document).on('keyup', '.g-producto-cambiar_cantidad_input', function(){
    let seccion = $(this).attr('data-seccion'), id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-producto-codigo').text(), cantidad = $(this).val();
    
    if(cantidad.length !== 0){
      clearTimeout(timeEjecucion);

      timeEjecucion = setTimeout(function(){
        enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad);
        clearTimeout(timeEjecucion);
      }, 800);
    }
  });
  /*
  * Producto - Boton para disminuir/aumentar la cantidad
  */
  $(document).on('click', '.g-producto-cambiar_cantidad_boton', function(){
    let seccion = $(this).attr('data-seccion'), id = $(this).attr('data-id'), almacen = $(this).attr('data-almacen'), codigo_producto = $('document, #id-producto-codigo').text(), cantidad = $('document, .g-producto-cambiar_cantidad_input[data-id=' + id + ']').val();
    
    if(cantidad.length !== 0){
      enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad);
    }
  });
  /*
  * Producto - Boton para agregar el producto al carrito de compras
  */
  $(document).on('click', '#id-producto-agregar_carrito', function(){
    let codigo_producto = $(this).attr('data-codigo'), data = new FormData();
    
    data.append('accion', 'agregar');
    data.append('codigo_producto', codigo_producto);
    
    fetch('procesos/producto/agregar_carrito.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;
      
      switch(respuesta){
        case '1':
          $('document, #id-producto-carrito_status_error_span').html(mensaje);
          $('document, #id-producto-carrito_status_error').slideDown('fast');
          $('document, #id-producto-carrito_status_error_div').addClass('p-notification_fixed_abled');

          setTimeout(function(){
            $('document, #id-producto-carrito_status_error').slideUp('fast');
            $('document, #id-producto-carrito_status_error_div').removeClass('p-notification_fixed_abled');
          }, 5000);
          break;

        case '2':
          $('document, #id-producto-carrito_status_success').slideDown('fast');
          $('document, #id-producto-carrito_status_success_div').addClass('p-notification_fixed_abled');
          setTimeout(function(){ window.location.href = "mi-carrito"; }, 1800);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * Funcion - Mandar la cantidad a cambiar
  */
  function enviar_cantidad(seccion, id, almacen, codigo_producto, cantidad){
    $('document, .g-producto-cantidad_loading[data-id=' + id + ']').slideDown('fast');
    $('document, .g-producto-cambiar_cantidad_input').attr('disabled','disabled');
    $('document, .g-producto-cambiar_cantidad_boton').attr('disabled','disabled');
    $('document, .g-producto-comprar').attr('disabled','disabled');
    $('document, #id-producto-agregar_carrito').attr('disabled','disabled');
    
    let data = new FormData();

    data.append('accion', 'cambiar');
    data.append('seccion', seccion);
    data.append('id', id);
    data.append('almacen', almacen);
    data.append('codigo_producto', codigo_producto);
    data.append('cantidad', cantidad);
    
    fetch('procesos/producto/cambiar_cantidad_unidades.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '0':
          location.reload();
          break;

        case '1':
          mostrar_elementos();
          break;

        case '2':
          console.log(mensaje);
          break;

        case '3':
          setTimeout(function(){
            mostrar_elementos();
            $('document, .g-producto-cantidad_loading[data-id=' + id + ']').slideUp('fast');
            $('document, .g-producto-cambiar_cantidad_input').removeAttr('disabled');
            $('document, .g-producto-cambiar_cantidad_boton').removeAttr('disabled');
            $('document, .g-producto-comprar').removeAttr('disabled');
            $('document, #id-producto-agregar_carrito').removeAttr('disabled');
          }, 200);
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  }
  /*
  * Funcion - Mostrar los elementos del producto
  */
  function mostrar_elementos(){
    let codigo_producto = $('document, #id-producto-codigo').text(), data = new FormData();
    
    data.append('accion', 'mostrar');
    data.append('codigo_producto', codigo_producto);
    
    fetch('procesos/producto/mostrar_elementos.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, html_cCantidad = datos.html_cCantidad, html_pTotal = datos.html_pTotal, html_botones = datos.html_botones, html_botones_ficha_tecnica = datos.html_botones_ficha_tecnica, codigoProveedor = datos.codigoProveedor, skuProveedor = datos.skuProveedor;
      
      respuesta === '1' ? (
        $('document, #id-producto-contenedor_cantidad').html(html_cCantidad),
        $('document, #id-producto-contenedor_precio_total').html(html_pTotal),
        $('document, #id-producto-contenedor_botones').html(html_botones)
      ) : (
        $('document, #id-producto-contenedor_botones').html(html_cCantidad),
        $('document, #id-producto-mensaje_problema').slideDown('fast')
      );

      if(respuesta === '1' && codigoProveedor === '001'){
        let modal_contenedor = document.getElementById('id-producto-contenedor_modal_pdf');

        fetch('https://fichastecnicas.pchmayoreo.com/archivos.php?nombre=' + skuProveedor + '.pdf').then(response => {
          if(!response.ok)
            throw Error(response.statusText + ' - ' + response.url);

          return response.json();
        }).then(data => {
          let estatus = data.estatus, mensaje = data.mensaje, datos = data.datos,
          object_modal = document.getElementById('modal-object-ficha_tecnica');

          estatus ? (
            object_modal.data = datos,
            $('document, #id-producto-contenedor_botones_ficha').html(html_botones_ficha_tecnica)
          ) : (
            console.log(mensaje),
            modal_contenedor === null ? console.log(modal_contenedor) : modal_contenedor.remove()
          );
        }).catch(error => {
          console.error('[Error] - ', error);
          modal_contenedor === null ? console.log(modal_contenedor) : modal_contenedor.remove();
        });
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  }
});