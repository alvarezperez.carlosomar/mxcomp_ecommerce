$(document).ready(function(){
  var correo_escrito1 = "", correo_escrito2 = "", password_escrito1 = "", password_escrito2 = "";
  /*
  * Iniciar sesion - Detecta cuando no esta enfocado el campo "Correo electronico"
  */
  $(document).on('blur', '.g-iniciarSesion-correo_input', function(){
    let correo = $(this).val(), data = new FormData();

    if(correo.length !== 0){
      $('document, .g-iniciarSesion-correo_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-iniciarSesion-correo_alert_info').slideUp('fast') : $('document, .g-iniciarSesion-correo_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-iniciarSesion-correo_alert_info').slideUp('fast');
    }
  });
  /*
  * Iniciar sesion - Detecta cuando se escribe en el campo "Correo electronico"
  */
  $(document).on('keyup', '.g-iniciarSesion-correo_input', function(){
    let correo = $(this).val(), data = new FormData();

    if(correo.length !== 0){
      $('document, .g-iniciarSesion-correo_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');
      
      if(correo_escrito1 === correo){
        $('document, .g-iniciarSesion-alert_error').slideDown('fast');
      }else{
        $('document, .g-iniciarSesion-alert_error').slideUp('fast');
        correo_escrito1 = "";
        password_escrito1 = "";
      }

      if(correo_escrito2 === correo){
        $('document, .g-iniciarSesion-alert_info').slideDown('fast');
      }else{
        $('document, .g-iniciarSesion-alert_info').slideUp('fast');
        correo_escrito2 = "";
        password_escrito2 = "";
      }

      data.append('accion', 'validar');
      data.append('valor', correo);

      fetch('validaciones/validar_correo.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-iniciarSesion-correo_alert_info').slideUp('fast') : $('document, .g-iniciarSesion-correo_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-iniciarSesion-correo_alert_info').slideUp('fast');
    }
  });
  /*
  * Iniciar sesion - Detecta cuando se escribe en el campo "Contraseña"
  */
  $(document).on('keyup', '.g-iniciarSesion-password_input', function(){
    let password = $(this).val(), data = new FormData();
    
    if(password.length !== 0){
      $('document, .g-iniciarSesion-password_alert_error').slideUp('fast');
      $(this).removeClass('p-input_error');

      if(password_escrito1 === password){
        $('document, .g-iniciarSesion-alert_error').slideDown('fast');
      }else{
        $('document, .g-iniciarSesion-alert_error').slideUp('fast');
        correo_escrito1 = "";
        password_escrito1 = "";
      }

      if(password_escrito2 === password){
        $('document, .g-iniciarSesion-alert_info').slideDown('fast');
      }else{
        $('document, .g-iniciarSesion-alert_info').slideUp('fast');
        correo_escrito2 = "";
        password_escrito2 = "";
      }

      data.append('accion', 'validar');
      data.append('valor', password);

      fetch('validaciones/validar_password.php', {
        method: 'POST',
        body: data
      }).then(response => {
        if(!response.ok)
          throw Error(response.statusText + ' - ' + response.url);
        
        return response.json();
      }).then(datos => {
        let respuesta = datos.respuesta;

        respuesta === '1' ? $('document, .g-iniciarSesion-password_alert_info').slideUp('fast') : $('document, .g-iniciarSesion-password_alert_info').slideDown('fast');
      }).catch(error => {
        console.error('[Error] - ', error);
      });
    }else{
      $('document, .g-iniciarSesion-password_alert_info').slideUp('fast');
    }
  });
  /*
  * Iniciar sesion - Acciones al enviar la informacion
  */
  $(document).on('submit', '#id-iniciarSesion-form_proceso', function(e){
    let correo = $('document, .g-iniciarSesion-correo_input').val(), password = $('document, .g-iniciarSesion-password_input').val(), data = new FormData();

    $('document, .g-iniciarSesion-alert_error').slideUp('fast');
    $('document, .g-iniciarSesion-alert_info').slideUp('fast');
    $('document, .g-iniciarSesion-loading').slideDown('fast');

    $('document, .g-iniciarSesion-correo_input').prop('disabled', true);
    $('document, .g-iniciarSesion-password_input').prop('disabled', true);
    $('document, .g-iniciarSesion-iniciar_button').prop('type', 'button');
    $('document, .g-iniciarSesion-iniciar_button').prop('disabled', true);

    data.append('accion', 'iniciar');
    data.append('correo', correo);
    data.append('password', password);

    fetch('procesos/usuario/proceso_iniciarSesion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje, mensaje_intentos = datos.mensaje_intentos;

      $('document, .g-iniciarSesion-loading').slideUp('fast');

      setTimeout(function(){
        $('document, .g-iniciarSesion-correo_input').prop('disabled', false);
        $('document, .g-iniciarSesion-password_input').prop('disabled', false);
        $('document, .g-iniciarSesion-iniciar_button').prop('type', 'submit');
        $('document, .g-iniciarSesion-iniciar_button').prop('disabled', false);
      }, 400);

      switch(respuesta){
        case '1':
          $('document, .g-iniciarSesion-correo_alert_error').slideDown('fast');

          setTimeout(function(){
            $('document, .g-iniciarSesion-correo_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-correo_input').focus();
          }, 400);
          break;

        case '2':
          $('document, .g-iniciarSesion-correo_alert_info').slideDown('fast');
          setTimeout(function(){ $('document, .g-iniciarSesion-correo_input').focus(); }, 400);
          break;

        case '3':
          $('document, .g-iniciarSesion-password_alert_error').slideDown('fast');
        
          setTimeout(function(){
            $('document, .g-iniciarSesion-password_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-password_input').focus();
          }, 400);
          break;

        case '4':
          $('document, .g-iniciarSesion-password_alert_info').slideDown('fast');
          setTimeout(function(){ $('document, .g-iniciarSesion-password_input').focus(); }, 400);
          break;

        case '5':
          $('document, #id-iniciarSesion-contenedor_notificacion').html(mensaje);
          $('document, .g-iniciarSesion-alert_error').slideDown('fast');
          $('document, #id-iniciarSesion-contenedor_intentos').html(mensaje_intentos);
          $('document, .g-iniciarSesion-notification_error').slideDown('fast');

          setTimeout(function(){
            $('document, .g-iniciarSesion-correo_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-password_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-correo_input').focus();
          }, 400);

          correo_escrito1 = correo;
          password_escrito1 = password;
          break;

        case '6':
          $('document, #id-iniciarSesion-contenedor_notificacion').html(mensaje);
          $('document, .g-iniciarSesion-alert_info').slideDown('fast');
          setTimeout(function(){ $('document, .g-iniciarSesion-correo_input').focus(); }, 400);
          correo_escrito2 = correo;
          password_escrito2 = password;
          break;

        case '7':
          let segundo = 0, contador;

          $('document, #id-iniciarSesion-contenedor_intentos').html('');
          $('document, #id-iniciarSesion-contenedor_notificacion').html(mensaje);
          $('document, .g-iniciarSesion-alert_success').slideDown('fast');
          $('document, .g-iniciarSesion-correo_input').removeClass('p-input_error');
          $('document, .g-iniciarSesion-password_input').removeClass('p-input_error');

          setTimeout(function(){
            $('document, .g-iniciarSesion-correo_input').prop('disabled', true);
            $('document, .g-iniciarSesion-password_input').prop('disabled', true);
            $('document, .g-iniciarSesion-iniciar_button').prop('type', 'button');
            $('document, .g-iniciarSesion-iniciar_button').prop('disabled', true);
            $('document, .g-iniciarSesion-iniciar_button').focus();
          }, 400);

          contador = setInterval(function(){
            segundo++;
            if(segundo === 2){
              clearInterval(contador);
              history.go(-1);
            }
          }, 1000);
          break;

        case '8':
          $('document, #id-iniciarSesion-contenedor_intentos').html(mensaje_intentos);
          $('document, .g-iniciarSesion-notification_error').slideDown('fast');

          if($(window).width() >= 1088){
            setTimeout(function(){ $('document, .g-iniciarSesion-correo_input').focus(); }, 400);
          }
          break;

        case '9':
          $('document, #id-iniciarSesion-contenedor_notificacion').html(mensaje);
          $('document, .g-iniciarSesion-alert_error').slideDown('fast');
          $('document, #id-iniciarSesion-contenedor_intentos').html(mensaje_intentos);
          $('document, .g-iniciarSesion-notification_error').slideDown('fast');

          setTimeout(function(){
            $('document, .g-iniciarSesion-correo_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-password_input').addClass('p-input_error');
            $('document, .g-iniciarSesion-correo_input').focus();
          }, 400);

          correo_escrito1 = correo;
          password_escrito1 = password;
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });

    e.preventDefault();
  });
});