if('serviceWorker' in navigator){
  navigator.serviceWorker.register('./service-worker.js')
  //.then(reg => console.log('Registro de SW exitoso', reg))
  .then(reg => console.log('Registro de SW exitoso'))
  .catch(err => console.warn('Error al tratar de registrar el sw', err))
}
$(document).ready(function(){
  /*
  * ENFOQUE - ENFOQUE A CAMP0 DEPENDIENDO EL TAMAÑO DE LA PANTALLA
  */
  if($(window).width() >= 1088){
    $('document, .g-input_focus').focus();
  }
  /*
  * NAVBAR - EFECTO BOTON BURGER
  */
  $(document).on('click', '#g-navbar-hamburger_button', function(){
    $('html').toggleClass('p-window-no_scroll');
    $('document, #g-navbar-hamburger_button').toggleClass('p-navbar-menu_opciones_activo');
    $('document, #g-navbar-menu_contenedor_opciones').toggleClass('p-navbar-menu_contenedor_opciones_activo');
    $('document, #g-navbar-menu_contenedor_opciones_fondo').toggleClass('p-navbar-menu_contenedor_opciones_activo');
  });
  /*
  * NAVBAR - CLICK FONDO MOVIL
  */
  $(document).on('click', '#g-navbar-menu_contenedor_opciones_fondo', function(){
    $('html').toggleClass('p-window-no_scroll');
    $('document, #g-navbar-hamburger_button').toggleClass('p-navbar-menu_opciones_activo');
    $('document, #g-navbar-menu_contenedor_opciones').toggleClass('p-navbar-menu_contenedor_opciones_activo');
    $('document, #g-navbar-menu_contenedor_opciones_fondo').toggleClass('p-navbar-menu_contenedor_opciones_activo');
  });
  /*
  * NAVBAR - APARECER MENU MOVIL
  */
  $(document).on('click', '#g-navbar-menu_opcion_user_movil', function(){
    $(this).toggleClass('p-navbar-opcion_user_movil_activo');

    $('document, #g-navbar-menu_user_contenedor_opciones').css('display') === 'none' ? $('document, #g-navbar-menu_user_contenedor_opciones').slideDown('fast') : $('document, #g-navbar-menu_user_contenedor_opciones').slideUp('fast');
  });
  ////////////////// GENERALES //////////////////
  /*
  * BOTON ARRIBA
  */
  $(window).scroll(function(){
    $(this).scrollTop() > 100 ? $('#id-boton_up').slideDown('fast') : $('#id-boton_up').slideUp('fast');
  });
  $(document).on('click', '#id-boton_up', function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });
  /*
  * Desplegar mensaje de ayuda
  */
  $(document).on('click', '.g-button_help', function(){
    let id = $(this).attr('id');
    
    $(this).toggleClass('p-button_help_activo');
    $('.' + id).slideToggle('fast');
  });
  /*
  * Detecta si se da clic en checkbox de numero exterior
  */
  $(document).on('click', '.g-form-noExterior_label', function(){
    let no_ext_input_text = $('document, .g-form-noExterior_input'), no_ext_alert_info = $('document, .g-form-noExterior_alert_info'), no_ext_alert_error = $('document, .g-form-noExterior_alert_error'), no_ext_input_checkbox = $('document, .g-form-noExterior_check');
    
    if(no_ext_input_checkbox.is(':checked')){
      no_ext_input_text.prop('disabled', false);
      no_ext_input_text.attr('placeholder', 'No. exterior');
      no_ext_input_text.focus();
      $(this).text('Sin no. exterior');
      no_ext_input_checkbox.prop('checked', false);
    }else{
      no_ext_input_text.prop('disabled', true);
      no_ext_input_text.val('');
      no_ext_input_text.attr('placeholder', 'S/N');
      no_ext_input_text.removeClass('p-input_error');
      no_ext_alert_info.slideUp('fast');
      no_ext_alert_error.slideUp('fast');
      $(this).text('Agregar no. exterior');
      no_ext_input_checkbox.prop('checked', true);
    }
  });
  /*
  * Detecta si se da clic en checkbox de numero interior
  */
  $(document).on('click', '.g-form-noInterior_label', function(){
    let no_int_input_text = $('document, .g-form-noInterior_input'), no_int_alert_info = $('document, .g-form-noInterior_alert_info'), no_int_alert_error = $('document, .g-form-noInterior_alert_error'), no_int_input_checkbox = $('document, .g-form-noInterior_check');
    
    if(no_int_input_checkbox.is(':checked')){
      no_int_input_text.prop('disabled', false);
      no_int_input_text.attr('placeholder', 'No. interior');
      no_int_input_text.focus();
      $(this).text('Sin no. interior');
      no_int_input_checkbox.prop('checked', false);
    }else{
      no_int_input_text.prop('disabled', true);
      no_int_input_text.val('');
      no_int_input_text.attr('placeholder', 'S/N');
      no_int_input_text.removeClass('p-input_error');
      no_int_alert_info.slideUp('fast');
      no_int_alert_error.slideUp('fast');
      $(this).text('Agregar no. interior');
      no_int_input_checkbox.prop('checked', true);
    }
  });
  //////////// SELECT /////////////////
  $(document).on('click', '.g-form_select', function(){
    let padre = $(this).parent();

    $(padre).toggleClass('p-select_arrow');
  });

  $(document).on('blur', '.g-form_select', function(){
    let padre = $(this).parent();

    $(padre).removeClass('p-select_arrow');
  });
  //////////// SELECT /////////////////
  /*
  * Botón mostrar/ocultar password
  */
  $(document).on('click', '.g-password-mostrar', function(){
    let id_campo = $(this).attr('id');
    
    $(this).toggleClass('p-button_change_icon_disabled').toggleClass('p-button_change_icon_enabled');
    
    $('document, .' + id_campo).prop('type') === 'password' ? $('document, .' + id_campo).prop('type', 'text') : $('document, .' + id_campo).prop('type', 'password');

    $('document, .' + id_campo).focus();
  });
  /*
  * Cerrar sesion - Se cierra sesion cuando se da clic en el boton "Cerrar sesion"
  */
  $(document).on('click', '.g-navbar-cerrar_sesion', function(){
    let data = new FormData();

    data.append('accion', 'cerrar');

    fetch('procesos/usuario/proceso_cerrarSesion.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, url = datos.url;

      if(respuesta === '1'){
        location.href = url;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  ////////////////// SECCION BUSCAR //////////////////
  $(document).on('keyup', '#id-navbar-busqueda_form_input', function(){
    let terminos = $(this).val();
    
    if(terminos.length !== 0){
      $('document, #id-navbar-busqueda_resultados').slideUp('fast');
      $('document, #id-navbar-busqueda_resultados').html('');
    }else{
      $('document, #id-navbar-busqueda_sku').slideUp('fast');
      $('document, #id-navbar-busqueda_sku').html('');
    }
  });
  
  $(document).on('blur', '#id-navbar-busqueda_form_input', function(){
    $('document, #id-navbar-busqueda_resultados').slideUp('fast');
    $('document, #id-navbar-busqueda_resultados').html('');
    $('document, #id-navbar-busqueda_sku').slideUp('fast');
    $('document, #id-navbar-busqueda_sku').html('');
  });
  
  $(document).on('submit', '#id-navbar-busqueda_form', function(e){
    let busqueda = $('document, #id-navbar-busqueda_form_input').val(), data = new FormData();

    data.append('accion', 'buscar');
    data.append('busqueda', busqueda);

    fetch('procesos/busqueda/seleccionar_opcionBusqueda.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      switch(respuesta){
        case '1':
          $('document, #id-navbar-busqueda_resultados').html(mensaje);
          $('document, #id-navbar-busqueda_resultados').slideDown('fast');
          $('document, #id-navbar-busqueda_form_input').focus();
          break;

        case '2':
          location.href = mensaje;
          break;

        case '3':
          console.log(mensaje);
          break;

        case '4':
          $('document, #id-navbar-busqueda_sku').html(mensaje);
          $('document, #id-navbar-busqueda_sku').slideDown('fast');
          $('document, #id-navbar-busqueda_form_input').focus();
          break;
      }
    }).catch(error => {
      console.error('[Error] - ', error);
    });
    
    e.preventDefault();
  });
  /*
  * BUSQUEDA - BOTON CATALOGO
  */
  $(document).on('click', '.g-navbar-busqueda_catalogo', function(){
    let data = new FormData();

    data.append('accion', 'buscar');
    data.append('opcion', 'catalogo');

    fetch('procesos/busqueda/seleccionar_opcionBusqueda.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '1' ? location.href = mensaje : console.log(mensaje);
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * BUSQUEDA - BOTON OPCION CATEGORIA
  */
  $(document).on('click', '.g-categorias-opciones', function(){
    let id = $(this).attr('data-id'), data = new FormData();
    
    data.append('accion', 'buscar');
    data.append('opcion', 'categorias');
    data.append('categoria', id);

    fetch('procesos/busqueda/seleccionar_opcionBusqueda.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '1' ? location.href = mensaje : console.log(mensaje);
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
  /*
  * BUSQUEDA - BOTON OPCION MARCA
  */
  $(document).on('click', '.g-marcas-opciones', function(){
    let id = $(this).attr('data-id'), data = new FormData();

    data.append('accion', 'buscar');
    data.append('opcion', 'marcas');
    data.append('marca', id);

    fetch('procesos/busqueda/seleccionar_opcionBusqueda.php', {
      method: 'POST',
      body: data
    }).then(response => {
      if(!response.ok)
        throw Error(response.statusText + ' - ' + response.url);
      
      return response.json();
    }).then(datos => {
      let respuesta = datos.respuesta, mensaje = datos.mensaje;

      respuesta === '1' ? location.href = mensaje : console.log(mensaje);
    }).catch(error => {
      console.error('[Error] - ', error);
    });
  });
});