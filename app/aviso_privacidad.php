<?php
session_start();
include 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
include 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
include 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
include 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Aviso de privacidad</title>
    <meta name="description" content="Se detalla el aviso de privacidad, para dar a conocer la finalidad de la obtención de ciertos datos para poder proveerle los servicios requeridos.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, aviso de privacidad, notificación">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 10;
$op_menu = 4;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-avisoPrivacidad-fondo">
          <h1 class="p-titulo">Aviso de privacidad</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero p-light_fondo">
          <h3>Identidad y domicilio del responsable</h3>
          <p class="p-text_p">MXCOMP; LA TECNOLOGÍA EN TUS MANOS S.A. DE C.V. en lo sucesivo <strong>MXCOMP</strong>, con domicilio en calle Río Galindo No. 4-A, colonia San Cayetano, C.P. 76806, en San Juan del Río, Qro. es responsable de la confidencialidad, uso y protección de la información personal que le es proporcionada.</p>
          
          
          <h3>Consentimiento</h3>
          <p class="p-text_p">Para efectos de lo dispuesto en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y demás legislación aplicable, el <strong>Titular</strong> de los datos personales manifiesta que el presente Aviso le ha sido dado a conocer por <strong>MXCOMP</strong> además de haber leído, entendido y acordado los términos expuestos en este Aviso, por lo que otorga su consentimiento respecto del tratamiento de sus datos personales.</p>
          
          
          <h3>Finalidades del tratamiento de los datos personales</h3>
          <p class="p-text_p"><strong>MXCOMP</strong> tratará sus datos personales con la finalidad de llevar a cabo las gestiones enfocadas al cumplimiento de las obligaciones comerciales que se establezcan con motivo de la prestación de servicios por parte de <strong>MXCOMP</strong>. Así como para las siguientes actividades.</p>
          <p><strong>Primarias:</strong></p>
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Creación de una cuenta de usuario.</li>
            <li>Levantar órdenes de compra.</li>
            <li>Envío de mercancías a domicilio.</li>
            <li>Gestión de pagos con medios electrónicos.</li>
            <li>Seguimiento de compras realizadas.</li>
            <li>Servicio postventa.</li>
            <li>Aplicación de Pólizas de Garantía.</li>
            <li>Facturación.</li>
            <li>Atención al cliente.</li>
            <li>Personalización del sitio web.</li>
          </ol>
          <p><strong>Secundarias:</strong></p>
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Para enviarle por diversos medios nuestras promociones.</li>
            <li>Para invitarle a participar en rifas, sorteos y concursos.</li>
            <li>Para realizar publicidad en redes sociales, medios de comunicación e información, periódicos y revistas.</li>
            <li>Para evaluar la calidad de los productos y servicios, así como para llevar a cabo encuestas de satisfacción.</li>
            <li>Para ofrecerle cualquiera de nuestros servicios y productos y/o una línea de crédito.</li>
            <li>Para fines mercadológicos, de prospección comercial y fines estadísticos e históricos.</li>
          </ol>
          
          
          <h3>Datos personales recabados</h3>
          <p class="p-text_p">Los datos personales que <strong>MXCOMP</strong> le puede llegar a solicitar para las finalidades descritas en el punto anterior son:</p>
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Correo electrónico.</li>
            <li>Contraseña personalizada</li>
            <li>Nombre completo</li>
            <li>Número telefónico</li>
            <li>Domicilio completo</li>
            <li>RFC</li>
          </ol>
          
          
          <h3>Transferencias de información</h3>
          <p class="p-text_p">De conformidad al artículo 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, podríamos realizar una transferencia de sus datos nacional o internacional, sin que sea necesario su consentimiento, a Autoridades Gubernamentales, Administrativas y/o Judiciales, municipales, estatales o federales, sociedades filiales, subsidiarias, controladoras o aquellas pertenecientes al mismo grupo de sociedades que operan bajo los mismos procesos y políticas internas, a Autoridades Sanitarias, o bien entes privados de servicios de salud, comerciantes ya sea personas físicas o morales, para dar cumplimiento a cualquiera de las finalidades señaladas en el presente aviso de privacidad, incluyendo aquellas que sean necesarias para cumplir con las leyes o que hubiesen sido legalmente exigidas para salvaguardar el interés público, para la procuración o administración de justicia, cuando sea necesaria para el reconocimiento, ejercicio o defensa de un derecho en un proceso judicial, cuando sea necesaria para la prevención o diagnóstico médico, gestiones y/o prestación de servicios sanitarios o médicos, cuando sea necesaria o resulten de las obligaciones de un contrato celebrado o por celebrarse, o finalmente, para dar mantenimiento o cumplimiento a relaciones jurídicas entre <strong>MXCOMP</strong> y el <strong>Titular</strong> de los datos personales.</p>
          
          
          <h3>Revocación de consentimiento</h3>
          <p class="p-text_p">Para el caso que desee revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, será necesario enviar un correo electrónico al departamento de atención al cliente: <strong><a class="p-text_p p-link_texto" href="mailto:atencionaclientes@mxcomp.com.mx">atencionaclientes@mxcomp.com.mx</a></strong> donde se le informará el procedimiento a seguir, tomando en consideración el tipo de relación que entabló con <strong>MXCOMP</strong>.</p>
          
          
          <h3>Derechos A.R.C.O.</h3>
          <p class="p-text_p">Para ejercitar los derechos de acceso, rectificación, cancelación y oposición respecto a sus datos personales, lo deberá realizar, enviando un correo electrónico al departamento de atención al cliente a la siguiente dirección: <strong><a class="p-text_p p-link_texto" href="mailto:atencionaclientes@mxcomp.com.mx">atencionaclientes@mxcomp.com.mx</a></strong>, adjuntando la siguiente información y documentos:</p>
          <ol type="I" start="1" class="p-avisoPrivacidad-ol p-text_p">
            <li>Identificación Oficial Vigente.</li>
            <li>Nombre completo del Titular de los datos personales.</li>
            <li>Descripción clara de los datos personales sujetos al ejercicio del derecho ejercido.</li>
          </ol>
          <p class="p-text_p">Asimismo, se deberá llenar el formato que le será enviado por correo electrónico. Usted tendrá una respuesta por correo electrónico, respecto a la procedencia o no del ejercicio de su derecho, dentro de los siguientes 20 días hábiles de recibido su correo. Dentro de los siguientes 15 días hábiles posteriores a la respuesta anterior, en el caso de que sea procedente su derecho, será aplicado.</p>
          
          
          <h3 class="p-text_p">Límite al uso y/o divulgación de sus datos personales</h3>
          <p class="p-text_p">Para limitar el uso o divulgación de sus datos personales, será necesario enviar un correo electrónico al departamento de atención al cliente: <strong><a class="p-text_p p-link_texto" href="mailto:atencionaclientes@mxcomp.com.mx">atencionaclientes@mxcomp.com.mx</a></strong>, donde Usted podrá solicitar se le incluya en los listados de exclusión internos de no contactar para fines promocionales y mercadológicos. Así mismo, podrá inscribirse en el registro público para evitar publicidad de la Procuraduría Federal del Consumidor, accediendo a la siguiente liga: <b><a class="p-text_p p-link_texto" href="https://repep.profeco.gob.mx/" target="_blank">repep.profeco.gob.mx</a></b>. En caso de que requiera limitar su uso o divulgación para otros fines, deberá enviar su solicitud al correo citado, donde se le informará el procedimiento a seguir, tomando en consideración el tipo de relación que entabló con <strong>MXCOMP</strong> para el tratamiento de sus datos personales.</p>
          
<?php
          /*<h3 class="p-text_p">Uso de "cookies"</h3>
          <p class="p-text_p">La tecnología denominada "cookies" se refiere a pequeños ficheros que se transfieren a su computadora cuando usted permite que su navegador los acepte. El uso de "cookies" ayuda a <strong>MXCOMP</strong> a brindarle una experiencia más personalizada al permitirnos conocer sus preferencias, rastrear determinados comportamientos, guardar información de uso constante, presentar publicidad que puede ser de su interés, además de que nos permite conocer el tamaño de nuestra audiencia y evaluar las preferencias comunes a fin de mejorar el contenido de <strong><a class="p-text_p p-link_texto" href="https://mxcomp.com.mx/">www.mxcomp.com.mx</a></strong>, por lo que se le sugiere tener habilitadas las "cookies" siempre que nos visite en línea.</p>*/
?>
          
          
          <h3 class="p-text_p">Modificaciones al aviso de privacidad</h3>
          <p class="p-text_p">El presente aviso de privacidad podrá ser modificado y/o actualizado por distintas causas, incluyendo las legales. Por ello, <strong>MXCOMP</strong> pondrá a su disposición, la versión actualizada del aviso de privacidad, en la página de internet: <strong><a class="p-text_p p-link_texto" href="https://mxcomp.com.mx/">www.mxcomp.com.mx</a></strong> y en cualquiera de nuestras sucursales.</p>
          <br>
          <hr class="p-avisoPrivacidad-divisor">
          <small><em>Última actualización 10/12/2020</em></small>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>  
</html>