<?php
session_start();
if(isset($_SESSION['__id__'])){
  echo '<script> window.location = "/"; </script>';
}

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}

include 'global/config.php'; // NECESARIO PARA HEAD
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Iniciar sesión</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;

include 'templates/navbar_formularios.php';
?>

    <section class="p-section-columns">
      <div class="p-contenido-contenedor_formularios">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Iniciar sesión</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-section-div_formularios_contenedor">
            <div class="p-loading-general g-iniciarSesion-loading">
              <div></div>
              <p><b>Procesando solicitud...</b></p>
            </div>

            <div id="id-iniciarSesion-contenedor_notificacion"></div>
            <div id="id-iniciarSesion-contenedor_intentos"></div>

            <form id="id-iniciarSesion-form_proceso">
              <div class="p-columnas p-field_marginBottom">
                <div class="p-columna">
                  <div class="p-field">
                    <label class="p-label p-text_p">
                      <span>Correo electrónico:</span>
                      <span class="p-text p-text_help">*</span>
                    </label>
                    <div class="p-control">
                      <input type="text" class="p-input g-input_focus g-iniciarSesion-correo_input" placeholder="Correo electrónico" autocomplete="off">
                    </div>
                    <p class="p-text p-text_info g-iniciarSesion-correo_alert_info">
                      <span>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      <span>No cumple con el formato estándar: ejemplo@ejemplo.com</span>
                    </p>
                    <p class="p-text p-text_error g-iniciarSesion-correo_alert_error">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este campo se encuentra vacío.</span>
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="p-columnas p-field_marginBottom">
                <div class="p-columna">
                  <div class="p-field">
                    <label class="p-label p-text_p">
                      <span>Contraseña:</span>
                      <span class="p-text p-text_help">*</span>
                    </label>
                    <div class="p-control">
                      <input type="password" class="p-input g-password_mostrar_button g-iniciarSesion-password_input" placeholder="Contraseña" autocomplete="off">
                      <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-password_mostrar_button">
                        <span>
                          <i class="fas fa-eye"></i>
                        </span>
                        <span>
                          <i class="fas fa-eye-slash"></i>
                        </span>
                      </a>
                    </div>
                    <p class="p-text p-text_info g-iniciarSesion-password_alert_info">
                      <span>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                    </p>
                    <p class="p-text p-text_error g-iniciarSesion-password_alert_error">
                      <span>
                        <i class="fas fa-times-circle"></i>
                      </span>
                      <span>Este campo se encuentra vacío.</span>
                    </p>
                  </div>
                </div>
              </div>
              
              <div class="p-field p-contenedor-flex_end">
                <a href="restablecer-password" class="p-link_texto p-text_p"><b>¿Olvidaste tu contraseña?</b></a>
              </div>
              
              <button type="submit" class="p-button p-button_info p-button_largo g-iniciarSesion-iniciar_button">
                <span>
                  <i class="fas fa-sign-in-alt"></i>
                </span>
                <span><b>Iniciar sesión</b></span>
              </button>
            </form>
            <div class="p-buttons p-buttons_right p-buttons_margin_top">
              <a class="p-button p-button_delete" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-undo-alt"></i>
                </span>
                <span><b>Regresar</b></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag_formularios.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

    <script src="scripts/iniciar_sesion/scripts_iniciar_sesion.js?v=2.3"></script>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>