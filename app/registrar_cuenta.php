<?php
session_start();
if(isset($_SESSION['__id__'])){
  echo '<script> window.location = "/"; </script>';
}

require_once 'global/config.php'; // NECESARIO PARA HEAD
require_once 'conn.php';

$Conn_mxcomp = new Conexion_mxcomp();

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Registrar cuenta</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;

include 'templates/navbar_formularios.php';
?>

    <section class="p-section-columns">
      <div class="p-contenido-contenedor_formularios">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">Registrar cuenta</h1>
        </div>

        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-section-div_formularios_contenedor">
            <div class="g-registrar-contenedor_informacion">
              <p class="p-text p-text_help p-text_help_margin p-text-align_right">
                <span>* Obligatorio</span>
              </p>

              <form class="g-registrar-form">
                <div class="p-registrar-div_datos_1">
                  <p class="p-titulo_datos_division">Datos generales</p>
                  
                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Nombre(s):</span>
                          <span class="p-button_help g-button_help" id="g-registrar-nombres_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-nombres_ayuda_span">Escribe el/los nombre(s) de la persona a quien será entregado el producto.</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-input_focus g-registrar-nombres_input" placeholder="Nombre(s)" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-nombres_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo letras y espacios, por favor.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-nombres_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Apellido paterno:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-apellido_paterno_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-apellido_paterno_ayuda_span">Escribe tu apellido paterno, revisa que este correcta esta información.</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-apellido_paterno_input" placeholder="Apellido paterno" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-apellido_paterno_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo letras y espacios, por favor.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-apellido_paterno_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                    <div class="p-columna p-columna-marginTop_1rem">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Apellido materno:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-apellido_materno_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-apellido_materno_ayuda_span">Escribe tu apellido materno, revisa que este correcta esta información.</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-apellido_materno_input" placeholder="Apellido materno" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-apellido_materno_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo letras y espacios, por favor.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-apellido_materno_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Correo electrónico:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-correo_electronico_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-correo_electronico_ayuda_span">El correo se escribe utilizando la forma estándar: ejemplo@ejemplo.com</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-correo_electronico_input" placeholder="Correo electrónico" autocomplete="off">
                        </div>
                        <p class="p-text p-text_success_msg g-registrar-correo_electronico_alert_success">
                          <span>
                            <i class="fas fa-check-circle"></i>
                          </span>
                          <span>Correo no registrado.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-correo_electronico_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>No cumple con el formato estándar, revisalo por favor.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-correo_electronico_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>No se registró la cuenta. Hay un problema al enviar el correo de activación a esta dirección de e-mail, revísalo y si es correcto, vuelve a realizar el registro.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-correo_electronico_alert_error_1">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Ya se encuentra registrado.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-correo_electronico_alert_error_2">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Contraseña:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-password_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-password_ayuda_span">
                          <span>Debe tener una longitud mínima de 8 caracteres y tener al menos: 1 número, 1 letra minúscula, 1 letra mayúscula y/o 1 símbolo como <i>! " @ # $ % * _</i></span>
                        </p>
                        <span class="p-text p-text_help">No utilices tu nombre, tu fecha de nacimiento o algún otro dato privado.</span>
                        <div class="p-control">
                          <input type="password" class="p-input g-password-mostrar_button g-registrar-password_input" placeholder="Contraseña" autocomplete="off">
                          <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-password-mostrar_button">
                            <span>
                              <i class="fas fa-eye"></i>
                            </span>
                            <span>
                              <i class="fas fa-eye-slash"></i>
                            </span>
                          </a>
                        </div>
                        <p class="p-text p-text_info g-registrar-password_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                        </p>
                        <p class="p-text p-text_error g-registrar-password_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                        <div class="p-password-seguridad_contenedor">
                          <label class="p-password-seguridad_label">Seguridad: </label>
                          <p class="p-password-seguridad_p g-password-porcentaje_seguridad">0%</p>
                          <div class="p-password-seguridad_div">
                            <div class="p-password-seguridad_div_progreso g-password-barra_progreso"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Confirmar contraseña:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="password" class="p-input g-confirmarPassword-mostrar_button g-registrar-confirmar_password_input" placeholder="Confirmar contraseña" autocomplete="off">
                          <a class="p-button p-button_account p-button_square p-buttons_margin_left g-password-mostrar p-button_change_icon_disabled" id="g-confirmarPassword-mostrar_button">
                            <span>
                              <i class="fas fa-eye"></i>
                            </span>
                            <span>
                              <i class="fas fa-eye-slash"></i>
                            </span>
                          </a>
                        </div>
                        <p class="p-text p-text_info g-registrar-confirmar_password_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></span>
                        </p>
                        <p class="p-text p-text_info g-registrar-confirmar_password_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>La contraseña no es idéntica, verifíquela por favor.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-confirmar_password_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Sexo:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="radio" class="p-input-radio_sexo" id="id-sexo-hombre_radio" name="sexo" value="Hombre">
                          <label class="p-input-radio_sexo_label g-registrar-sexo_radio" for="id-sexo-hombre_radio">Hombre</label>
                          <input type="radio" class="p-input-radio_sexo" id="id-sexo-mujer_radio" name="sexo" value="Mujer">
                          <label class="p-input-radio_sexo_label g-registrar-sexo_radio" for="id-sexo-mujer_radio">Mujer</label>
                        </div>
                        <p class="p-text p-text_info g-registrar-sexo_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Esta opción no existe.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-sexo_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>No has seleccionado tu sexo.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Fecha de nacimiento:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-fecha_nacimiento_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-fecha_nacimiento_ayuda_span">Siga el formato que se muestra en el campo, la flecha del lado derecho le muestra un calendario para agregar la fecha más fácil.</p>
                        <div class="p-control">
                          <input type="date" class="p-input p-date g-registrar-fecha_nacimiento_input" placeholder="Fecha de nacimiento" autocomplete="off">
                        </div>
                        <p class="p-text p-text_error g-registrar-fecha_nacimiento_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>No has escrito / seleccionado tu fecha de nacimiento.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>No. telefónico:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-no_telefonico_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-no_telefonico_ayuda_span">Sólo números, sin formato. Son permitidos 10 dígitos solamente.</p>
                        <div class="p-control">
                          <div class="p-buttons-contenedor_phone">
                            <input type="tel" class="p-input p-input_phone g-registrar-no_telefonico_1_input" placeholder="No. telefónico #1" autocomplete="off" maxlength="10">
                            <div class="p-buttons_margin_left">
                              <a class="p-button p-button_account p-button_square g-telefono-agregar_1_button">
                                <span>
                                  <i class="fas fa-plus"></i>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <p class="p-text p-text_info g-registrar-no_telefonico_1_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo números, por favor.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-no_telefonico_1_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>El número telefónico debe de contener 10 dígitos.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-no_telefonico_1_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Al menos ingresa 1 número telefónico, por favor.</span>
                        </p>
                        <div class="p-control p-phone_display" id="g-telefono-contenedor_control_2">
                          <div class="p-buttons-contenedor_phone p-phone_margin_top">
                            <input type="tel" class="p-input p-input_phone g-registrar-no_telefonico_2_input" placeholder="No. telefónico #2" autocomplete="off" maxlength="10">
                            <div class="p-buttons_margin_left">
                              <a class="p-button p-button_account p-button_square g-telefono-agregar_2_button">
                                <span>
                                  <i class="fas fa-plus"></i>
                                </span>
                              </a>
                            </div>
                            <div class="p-buttons_margin_left">
                              <a class="p-button p-button_delete p-button_square g-telefono-borrar_1_button">
                                <span>
                                  <i class="far fa-trash-alt"></i>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <p class="p-text p-text_info g-registrar-no_telefonico_2_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo números, por favor.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-no_telefonico_2_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>El número telefónico debe de contener 10 dígitos.</span>
                        </p>
                        <div class="p-control p-phone_display" id="g-telefono-contenedor_control_3">
                          <div class="p-buttons-contenedor_phone p-phone_margin_top">
                            <input type="tel" class="p-input p-input_phone g-registrar-no_telefonico_3_input" placeholder="No. telefónico #3" autocomplete="off" maxlength="10">
                            <div class="p-buttons_margin_left">
                              <a class="p-button p-button_delete p-button_square g-telefono-borrar_2_button">
                                <span>
                                  <i class="far fa-trash-alt"></i>
                                </span>
                              </a>
                            </div>
                          </div>
                        </div>
                        <p class="p-text p-text_info g-registrar-no_telefonico_3_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo números, por favor.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-no_telefonico_3_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>El número telefónico debe de contener 10 dígitos.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <a class="p-field_marginTop p-button p-button_info p-button_largo" id="g-boton-siguiente_button">
                    <span>
                      <i class="fas fa-chevron-circle-down"></i>
                    </span>
                    <span><b>Siguiente</b></span>
                  </a>
                </div>

                <div class="p-registrar-div_datos_2">
                  <div class="p-buttons">
                    <a class="p-button p-button_info p-button_largo" id="g-boton-atras_button">
                      <span>
                        <i class="fas fa-chevron-circle-up"></i>
                      </span>
                      <span><b>Atrás</b></span>
                    </a>
                  </div>
                  <p class="p-field_marginTop p-titulo_datos_division">Domicilio particular</p>
                  
                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Tipo de vialidad:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <div class="p-select g-registrar-tipo_vialidad_div">
                            <select class="p-input g-form_select g-registrar-tipo_vialidad_select">
                              <option value="" selected="selected">Selecciona el tipo</option>
<?php
try{
  $sql = "SELECT id, nombreTipoVialidad FROM __tipos_vialidad";
  $stmt = $Conn_mxcomp->pdo->prepare($sql);
  $stmt->execute();

  while($datos_tipoVialidad = $stmt->fetch(PDO::FETCH_ASSOC)){
?>
                              <option value="<?php echo trim($datos_tipoVialidad['id']); ?>"><?php echo trim($datos_tipoVialidad['nombreTipoVialidad']); ?></option>
<?php
  }
}catch(PDOEXception $error){
  //$mensaje = "Error: " . $error->getMessage();
  $mensaje = "Problema al buscar los tipos de vialidad";
?>
                              <option value=""><?php echo $mensaje; ?></option>
<?php
}
?>
                            </select>
                          </div>
                        </div>
                        <p class="p-text p-text_info g-registrar-tipo_vialidad_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Este tipo de vialidad no existe.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-tipo_vialidad_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>No has seleccionado un tipo de vialidad.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Nombre de vialidad:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-nombre_vialidad_input" placeholder="Nombre de vialidad" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-nombre_vialidad_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-nombre_vialidad_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>No. exterior:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-no_exterior_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-no_exterior_ayuda_span">Si no se cuenta con número exterior, active la opción.</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-form-noExterior_input g-registrar-no_exterior_input" placeholder="No. exterior" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-form-noExterior_alert_info g-registrar-no_exterior_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                        </p>
                        <p class="p-text p-text_error g-form-noExterior_alert_error g-registrar-no_exterior_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                        <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noExterior_check g-registrar-no_exterior_checkbox" autocomplete="off">
                        <label class="p-label-checkbox_numeroCalle g-form-noExterior_label">Sin no. exterior</label>
                      </div>
                    </div>
                    <div class="p-columna p-columna-marginTop_1rem">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>No. interior:</span>
                          <span class="p-button_help g-button_help" id="g-registrar-no_interior_ayuda_span">
                            <i class="fas fa-question"></i>
                          </span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <p class="p-text p-text_help_square g-registrar-no_interior_ayuda_span">Si no se cuenta con número interior, active la opción.</p>
                        <div class="p-control">
                          <input type="text" class="p-input g-form-noInterior_input g-registrar-no_interior_input" placeholder="No. interior" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-form-noInterior_alert_info g-registrar-no_interior_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios y guion corto (-).</span>
                        </p>
                        <p class="p-text p-text_error g-form-noInterior_alert_error g-registrar-no_interior_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                        <input type="checkbox" class="p-input-checkbox_numeroCalle g-form-noInterior_check g-registrar-no_interior_checkbox" autocomplete="off">
                        <label class="p-label-checkbox_numeroCalle g-form-noInterior_label">Sin no. interior</label>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna p-dos_columnas">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Código postal:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="tel" class="p-input g-registrar-codigo_postal_input" placeholder="Código postal" autocomplete="off" maxlength="5">
                        </div>
                        <p class="p-text p-text_info g-registrar-codigo_postal_alert_info_1">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo números, por favor.</span>
                        </p>
                        <p class="p-text p-text_info g-registrar-codigo_postal_alert_info_2">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>El código postal debe de contener 5 números.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-codigo_postal_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Colonia:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-colonia_input" placeholder="Colonia" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-colonia_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-colonia_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Ciudad o municipio:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <input type="text" class="p-input g-registrar-ciudad_municipio_input" placeholder="Ciudad o municipio" autocomplete="off">
                        </div>
                        <p class="p-text p-text_info g-registrar-ciudad_municipio_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo letras y espacios, por favor.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-ciudad_municipio_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>Este campo se encuentra vacío.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Estado:</span>
                          <span class="p-text p-text_help">*</span>
                        </label>
                        <div class="p-control">
                          <div class="p-select p-select_padding g-registrar-estado_div">
                            <select class="p-input g-form_select g-registrar-estado_select">
                              <option value="" selected="selected">Selecciona tu estado</option>
<?php
try{
  $sql = "SELECT id, nombreEstado FROM __estados_codigos";
  $stmt = $Conn_mxcomp->pdo->prepare($sql);
  $stmt->execute();

  while($datos_estado = $stmt->fetch(PDO::FETCH_ASSOC)){
?>
                              <option value="<?php echo trim($datos_estado['id']); ?>"><?php echo trim($datos_estado['nombreEstado']); ?></option>
<?php
  }
}catch(PDOEXception $error){
  //$mensaje = "Error: " . $error->getMessage();
  $mensaje = "Problema al buscar los estados";
?>
                              <option value=""><?php echo $mensaje; ?></option>
<?php
}
?>
                            </select>
                          </div>
                        </div>
                        <p class="p-text p-text_info g-registrar-estado_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Este estado no existe.</span>
                        </p>
                        <p class="p-text p-text_error g-registrar-estado_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>No has seleccionado un estado.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Entre calles:</span>
                          <span class="p-text p-text_help">(Opcional)</span>
                        </label>
                        <div class="p-columnas">
                          <div class="p-columna p-dos_columnas">
                            <div class="p-control">
                              <input type="text" class="p-input g-registrar-entre_calles_1_input" placeholder="Calle #1" autocomplete="off">
                            </div>
                            <p class="p-text p-text_info g-registrar-entre_calles_1_alert_info">
                              <span>
                                <i class="fas fa-info-circle"></i>
                              </span>
                              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                            </p>
                            <p class="p-text p-text_error g-registrar-entre_calles_1_alert_error">
                              <span>
                                <i class="fas fa-times-circle"></i>
                              </span>
                              <span>La calle #2 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                            </p>
                          </div>
                          <div class="p-columna">
                            <div class="p-control">
                              <input type="text" class="p-input g-registrar-entre_calles_2_input" placeholder="Calle #2" autocomplete="off">
                            </div>
                            <p class="p-text p-text_info g-registrar-entre_calles_2_alert_info">
                              <span>
                                <i class="fas fa-info-circle"></i>
                              </span>
                              <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                            </p>
                            <p class="p-text p-text_error g-registrar-entre_calles_2_alert_error">
                              <span>
                                <i class="fas fa-times-circle"></i>
                              </span>
                              <span>La calle #1 tiene datos, por lo tanto este campo no debe de estar vacío.</span>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <label class="p-label p-text_p">
                          <span>Referencias adicionales:</span>
                          <span class="p-text p-text_help">(Opcional)</span>
                        </label>
                        <div class="p-control">
                          <textarea class="p-input p-textarea g-registrar-referencias_adicionales_textarea" placeholder="Ejemplo: frente a tienda..."></textarea>
                        </div>
                        <p class="p-text p-text_info g-registrar-referencias_adicionales_alert_info">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span>Sólo se permite letras mayúsculas, minúsculas, números, espacios, guion corto (-), más (+), diagonal (/), punto (.) y coma (,)</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-columnas p-field_marginBottom">
                    <div class="p-columna">
                      <div class="p-field">
                        <div class="p-control">
                          <input type="checkbox" class="p-input-checkbox_option g-registrar-termCond_aviPri_checkbox" id="id-termCond-aviPri_checkbox">
                          <label class="p-input-checkbox_option_label" for="id-termCond-aviPri_checkbox">
                            <span>Acepto los <a href="terminos-y-condiciones" target="_blank" class="p-link_texto p-link_avisoPrivacidad">términos y condiciones</a>, así como el <a href="aviso-de-privacidad" target="_blank" class="p-link_texto p-link_avisoPrivacidad">aviso de privacidad</a>.</span>
                          </label>
                        </div>
                        <p class="p-text p-text_error g-registrar-termCond_aviPri_alert_error">
                          <span>
                            <i class="fas fa-times-circle"></i>
                          </span>
                          <span>No has aceptado los términos y el aviso de privacidad.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div class="p-field" id="id-recaptcha-contenedor">
                    <input type="hidden" class="g-registrar-captcha" readonly disabled>
                    
                    <div class="p-notification_contenedor g-registrar-recaptcha_alert_info">
                      <div class="p-notification p-notification_letter_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <p class="p-notification_p">
                          <span>No se recibió información del recaptcha de Google, revisa tu conexión a Internet y vuelve a realizar tu registro.</span>
                        </p>
                      </div>
                    </div>
                    
                    <div class="p-notification_contenedor g-registrar-recaptcha_alert_error">
                      <div class="p-notification p-notification_letter_info">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <p class="p-notification_p">
                          <span>Estamos validando que no seas un bot (programa informático), por favor vuelve a dar clic en <strong>"registrar cuenta"</strong>.</span>
                        </p>
                      </div>
                    </div>
                  </div>

                  <button type="submit" class="p-button p-button_info p-button_largo g-registrar-cuenta_button">
                    <span>
                      <i class="fas fa-user-plus"></i>
                    </span>
                    <span><b>Registrar cuenta</b></span>
                  </button>

                  <div class="p-loading-general g-registrar-loading">
                    <div></div>
                    <p><b>Realizando registro...</b></p>
                  </div>
                </div>
              </form>
            </div>
            
            <div class="p-notification_contenedor g-aviso-registro_exitoso">
              <div class="p-notification p-notification_letter_success">
                <span>
                  <i class="fas fa-check-circle"></i>
                </span>
                <p class="p-notification_p">
                  <span>¡Listo! Se registró tu cuenta exitosamente. Se envió un correo electrónico a <b><span class="g-registrar-span_notificacion"></span></b>.</span>
                  <span>Tienes <b>24 HORAS</b> para activarla. Si no encuentras el correo en tu bandeja de entrada, revisa en la carpeta de Spam.</span>
                  <span>En caso que no llegue el correo, entra en <b>iniciar sesió</b>n y da clic en <b>¿Olvidaste tu contraseña?</b> para que ingreses tu correo, el sistema te mandará el correo de activación.</span>
                </p>
              </div>
            </div>
            
            <div class="p-buttons p-buttons_right p-buttons_margin_top">
              <a class="p-button p-button_delete g-regresar-button" onclick="history.go(-1)">
                <span>
                  <i class="fas fa-undo-alt"></i>
                </span>
                <span><b>Regresar</b></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag_formularios.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

    <script>
      var site_key = '<?php echo SITE_KEY; ?>';
    </script>
    <script src="scripts/registrar/scripts_registrar.js?v=2.8"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=<?php echo SITE_KEY; ?>"></script>
    <script>
      grecaptcha.ready(function() {
        grecaptcha.execute(site_key, {action: 'user_register'}).then(function(token) {
          $('document, .g-registrar-captcha').val(token);
        });
      });
    </script>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>