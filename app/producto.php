<?php
session_start();
if(!isset($_GET['p'])){
  echo '<script> history.go(-1); </script>';
}

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// SE ELIMINA LA VARIABLE QUE CONTIENE LOS DATOS DE COMPRA PARA EL PRODUCTO
unset($_SESSION['__seccion_comprar_producto__']);

// SI INICIALIZA LA VARIABLE A 0, QUE PERMITIRÁ QUE CUANDO SE REGRESE A INDEX.PHP, MANTENGA LA VARIABLE DE SESSION QUE CONTIENE A LOS PRODUCTOS
$_SESSION['__contador_recarga__'] = 0;

$codigoProducto = trim($_GET['p']);
$Conn_mxcomp = new Conexion_mxcomp();

$brand_codigoIncorrecto = false;
$brand_consultaError = false;
$brand_productoNoExiste = false;
$html_title = "";
$html_descripcion = "";
$html_keywords = "";
$titulo = "";

if(validar_codigoProducto_caracteres($codigoProducto) && validar_codigoProducto_formato($codigoProducto)){
  $codigoProducto = (string) $codigoProducto;
  
  try{
    $sql = "SELECT COUNT(Productos.id) AS conteo, Productos.skuProveedor, Productos.skuFabricante, Productos.descripcion, Productos.descripcionURL, Productos.categoriaID, Productos.marcaID, Productos.nombreMarca, Productos.precioMXcomp, Productos.monedaMXcomp, Productos.almacenes, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.envioGratisPermitido, Productos.activo, Categorias.nombre AS nombreCategoria, Subcat_tipos.nombre AS nombreSubcategoriaTipo, Subcat_especificas.nombre AS nombreSubcategoriaEspecifica

    FROM __productos Productos
    INNER JOIN __categorias Categorias ON Categorias.id = Productos.categoriaID
    INNER JOIN __subcat_tipos Subcat_tipos ON Subcat_tipos.id = Productos.subcat_tipoID
    INNER JOIN __subcat_especificas Subcat_especificas ON Subcat_especificas.id = Productos.subcat_especificaID
    
    WHERE BINARY Productos.codigoProducto = :codigoProducto AND Productos.activo = 1";
    $stmt = $Conn_mxcomp->pdo->prepare($sql);
    $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
    $stmt->execute();
    $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
    $producto_existe = (int) trim($datos_producto['conteo']);
    
    if($producto_existe === 1){
      $skuProveedor = trim($datos_producto['skuProveedor']);
      $skuFabricante = trim($datos_producto['skuFabricante']);
      $descripcion = trim($datos_producto['descripcion'], " \xC2\xA0");
      $descripcionURL = trim($datos_producto['descripcionURL'], " \xC2\xA0");
      $categoriaID = trim($datos_producto['categoriaID']);
      $marcaID = trim($datos_producto['marcaID']);
      $nombreMarca = trim($datos_producto['nombreMarca']);
      $precioMXcomp = (float) trim($datos_producto['precioMXcomp']);
      $monedaMXcomp = trim($datos_producto['monedaMXcomp']);
      $almacenes = (array) json_decode(trim($datos_producto['almacenes']), true);
      $tieneImagen = (int) trim($datos_producto['tieneImagen']);
      $numeroUbicacionImagen = $datos_producto['numeroUbicacionImagen']; // PARA REVISAR SI ES NULL
      $nombreImagen = trim($datos_producto['nombreImagen']);
      $versionImagen = trim($datos_producto['versionImagen']);
      $envioGratisPermitido = (int) trim($datos_producto['envioGratisPermitido']);
      $activo = (int) trim($datos_producto['activo']);
      $nombreCategoria = trim($datos_producto['nombreCategoria']);
      $nombreSubcategoriaTipo = (string) trim($datos_producto['nombreSubcategoriaTipo']);
      $nombreSubcategoriaEspecifica = (string) trim($datos_producto['nombreSubcategoriaEspecifica']);
      
      if($activo !== 0){
        // SI NO EXISTE LA VARIABLE O EL PRODUCTO NO ES EL MISMO, SE INICIALIZAN EN 0 TODOS LOS ALMACENES
        if(!isset($_SESSION['__producto_unidades__']) || $_SESSION['__producto_unidades__']['codigoProducto'] !== $codigoProducto ){
          $array_almacenes_producto = [];
          foreach($almacenes as $datos_almacen){
            $array_almacenes_producto[ $datos_almacen['nombre_almacen'] ] = [
              'unidades' => 0,
              'activo' => 1
            ];
          }

          $_SESSION['__producto_unidades__'] = [
            'codigoProducto' => $codigoProducto,
            'arrayInputs' => $array_almacenes_producto
          ];
        }else{
          // TODOS LOS ALMACENES SE PONEN INACTIVOS
          foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $ind_nombreAlmacen=>$data_almacen){
            $_SESSION['__producto_unidades__']['arrayInputs'][$ind_nombreAlmacen]['activo'] = 0;
          }

          // LOS ALMACENES QUE COINCIDAN SE PONEN ACTIVOS
          foreach($almacenes as $datos_almacen){
            foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $ind_nombreAlmacen=>$data_almacen){
              if($datos_almacen['nombre_almacen'] === $ind_nombreAlmacen){
                $_SESSION['__producto_unidades__']['arrayInputs'][$ind_nombreAlmacen]['activo'] = 1;
                break;
              }
            }
          }

          // SE ELIMINAN LOS ALMACENES INACTIVOS
          foreach($_SESSION['__producto_unidades__']['arrayInputs'] as $ind_nombreAlmacen=>$data_almacen){
            if($data_almacen['activo'] === 0){
              unset($_SESSION['__producto_unidades__']['arrayInputs'][$ind_nombreAlmacen]);
            }
          }

          // SE AGREGA EL ALMACEN QUE NO SE ENCUENTRE EN EL ARRAY
          foreach($almacenes as $datos_almacen){
            if(!array_key_exists($datos_almacen['nombre_almacen'], $_SESSION['__producto_unidades__']['arrayInputs'])){
              $_SESSION['__producto_unidades__']['arrayInputs'][$datos_almacen['nombre_almacen']] = [
                'unidades' => 0,
                'activo' => 1
              ];
            }
          }
        }
      }

      $html_title = mb_strlen($descripcion) <= 100 ? $descripcion : mb_substr($descripcion, 0, 100) . '...';

      $descripcion_editada = str_replace('"', '', $descripcion);
      $alt_editado = ucwords(strtolower( $descripcion_editada . ', ' . str_replace('"', '', $nombreMarca) . ', ' . str_replace('"', '', $nombreCategoria) . ', ' . str_replace('"', '', $nombreSubcategoriaTipo) . ', ' . str_replace('"', '', $nombreSubcategoriaEspecifica) ));
      
      $html_descripcion = $skuFabricante !== "" ? 'Compra ' . $alt_editado . ', ' . ucwords(strtolower($skuFabricante)) . ' en MXcomp. Encuentra más productos en nuestro sitio web.' : 'Compra ' . $alt_editado . ' en MXcomp. Encuentra más productos en nuestro sitio web.';

      $descripcionURL_editada = str_replace('-', ', ', $descripcionURL);
      $html_keywords = 'mxcomp, ecommerce, ' . $descripcionURL_editada;
      $titulo = $descripcion;

      $alt1 = $alt_editado . ' [Imagen 1]';
      $alt2 = $alt_editado . ' [Imagen 2]';
      $alt3 = $alt_editado . ' [Imagen 3]';
      
      $saldra_modal = false;
      
      // SI ES NULL NO TIENE UBICACION LA IMAGEN Y NO EXISTE
      if(is_null($numeroUbicacionImagen) && $tieneImagen === 0){
        $imagen1 = 'images/no_imagen_disponible.png';
        $alt1 = 'Imagen no disponible.';
        $saldra_modal = false;
      }else{
        $numeroUbicacionImagen = (int) $numeroUbicacionImagen;
        $saldra_modal = true;
        
        switch($numeroUbicacionImagen){
          case 1: // UBICACION NUEVA
            $imagen1 = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '.jpg';
            $imagen2 = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '_2.jpg';
            $imagen3 = 'images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '_3.jpg';
            break;
        }
      }
    }else{
      $brand_productoNoExiste = true;
      $html_title = "Este producto no existe";
      $titulo = "Este producto no existe";
    }
  }catch(PDOException $error){
    $brand_consultaError = true;
    $html_title = "Ocurrió problema al buscar producto";
    $titulo = "Error en consulta";
    //$error_msg = "Error: " . $error->getMessage();
    $error_msg = "Ocurrió un problema al buscar la información del producto. Si sigue apareciendo este mensaje, contacta con atención a clientes.";
  }
}else{
  $brand_codigoIncorrecto = true;
  $html_title = "Producto con código no válido";
  $titulo = "Producto con código no válido";
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo $html_title; ?></title>
    <meta name="description" content="<?php echo $html_descripcion; ?>">
    <meta name="keywords" content="<?php echo $html_keywords; ?>">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>
    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-producto-fondo">
          <h1 class="p-titulo"><?php echo $titulo; ?></h1>
        </div>

<?php
if($brand_codigoIncorrecto){
?>
        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-text_p">
            <b>El código del producto no es válido.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo-alt"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
        </div>
<?php
}else if($brand_consultaError){
?>
        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-text_p">
            <b><?php echo $error_msg; ?></b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo-alt"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
        </div>
<?php
}else if($brand_productoNoExiste){
?>
        <div class="p-section-div_formularios p-contenido-hero">
          <p class="p-text_p">
            <b>Este producto no se encuentra registrado.</b>
          </p>
          <a class="p-button p-button_delete" onclick="history.go(-1)">
            <span>
              <i class="fas fa-undo-alt"></i>
            </span>
            <span><b>Regresar a página anterior</b></span>
          </a>
        </div>
<?php
}else{
?>
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-notification_contenedor" id="id-producto-carrito_status_success">
            <div class="p-notification p-notification_success p-notification_fixed_disabled" id="id-producto-carrito_status_success_div">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <p class="p-notification_p">
                <span>Producto agregado al carrito.</span>
              </p>
            </div>
          </div>

          <div class="p-notification_contenedor" id="id-producto-carrito_status_error">
            <div class="p-notification p-notification_error p-notification_fixed_disabled" id="id-producto-carrito_status_error_div">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <p class="p-notification_p" id="id-producto-carrito_status_error_span"></p>
            </div>
          </div>

          <div class="p-columnas">
            <div class="p-columna p-col_5_12 p-producto-image">
              <div class="p-producto-columnas_info">
                <div class="p-producto-sku_cat_info">
                  <p class="p-text_p">
                    <b>Código: </b><span id="id-producto-codigo"><?php echo $codigoProducto; ?></span>
                  </p>
                  <p class="p-text_p"><b>SKU: </b><?php echo $skuProveedor; ?><?php echo $skuFabricante !== "" ? ' | <b>Modelo: </b>' . $skuFabricante : ''; ?></p>
                  <p class="p-text_p">
                    <b>Categoría: </b><a class="p-producto-link g-categorias-opciones" data-id="<?php echo $categoriaID; ?>"><?php echo $nombreCategoria; ?></a> / <b>Marca: </b><a class="p-producto-link g-marcas-opciones" data-id="<?php echo $marcaID; ?>"><?php echo $nombreMarca; ?></a>
                  </p>
                </div>
<?php
  if(isset($_SESSION['__id__'])){
                /*<div class="p-producto-columna_info_div">
                  <div class="p-producto-favoritos_contenedor_icon">
                    <input type="checkbox" id="id-producto-favoritos_checkbox">
                    <label for="id-producto-favoritos_checkbox">
                      <span><i class="far fa-heart"></i></span>
                      <span><i class="fas fa-heart"></i></span>
                    </label>
                  </div>
                </div>*/
  }
?>
              </div>
              <figure class="p-producto-image_figure">
                <div style="display: flex; justify-content: center;">
                  <div class="p-producto-image_div">
                    <picture <?php echo $saldra_modal ? 'class="contenedor_img"' : '' ; ?>>
                      <!--<source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                      <img src="<?php echo $imagen1 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt1; ?>"<?php echo $saldra_modal ? ' data-id="1"' : '' ; ?>>
                    </picture>
                  </div>
<?php
  if(isset($imagen2) && file_exists($imagen2)){
?>
                  <div class="p-producto-imagenes_contenedor">
                    <div>
                      <figure class="p-producto-image_div">
                        <picture class="contenedor_img">
                        <!--<source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                          <img src="<?php echo $imagen2 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt2; ?>" data-id="2">
                        </picture>
                      </figure>
                    </div>
<?php
    if(isset($imagen3) && file_exists($imagen3)){
?>
                    <div>
                      <figure class="p-producto-image_div">
                        <picture class="contenedor_img">
                        <!--<source srcset="images/laptop.webp" type="image/webp">--> <!-- Formato WebP -->
                          <img src="<?php echo $imagen3 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt3; ?>" data-id="3">
                        </picture>
                      </figure>
                    </div>
<?php
    }
?>
                  </div>
<?php
  }
?>
                </div>
                <figcaption>Las imágenes son ilustrativas y pueden variar respecto al modelo</figcaption>
              </figure>
            </div>
            <div class="p-columna p-producto-info">
              <div class="p-producto-info_div">
                <div class="p-producto-precios_contenedor">
                  <p class="p-producto-p_precio_unitario_color p-producto-p_precio_grande">
                    <span>$ <?php echo number_format($precioMXcomp, 2, '.', ','); ?></span>
                    <span><?php echo $monedaMXcomp; ?></span>
                  </p>
                  <small class="p-producto-aviso_verde">
                    <b>IVA incluido. Precio sujeto a cambios sin previo aviso.</b>
                  </small>
<?php
                  /*<div class="p-producto-stars_contenedor_span">
                    <div class="p-producto-stars_votacion_span" id="id-producto-stars_votacion">
                      <span id="id-producto-stars_votacion_5">
                        <i class="fas fa-star"></i>
                      </span>
                      <span id="id-producto-stars_votacion_4">
                        <i class="fas fa-star"></i>
                      </span>
                      <span id="id-producto-stars_votacion_3">
                        <i class="fas fa-star"></i>
                      </span>
                      <span id="id-producto-stars_votacion_2">
                        <i class="fas fa-star"></i>
                      </span>
                      <span id="id-producto-stars_votacion_1">
                        <i class="fas fa-star"></i>
                      </span>
                    </div>
                    <p class="p-text_p">
                      <span><b>Calificación: </b></span><span id="id-producto-stars_calificacion">0</span><span> de 5 estrellas</span>
                    </p>
                    <p class="p-text_p">
                      <span><b>Votos: </b></span><span id="id-producto-stars_votos">0</span>
                    </p>
                    <div class="p-producto-stars_contenedor_stars_fondo" id="id-producto-stars_fondo"></div>
                    <div class="p-producto-stars_contenedor_stars_total" id="id-producto-stars_div">
                      <div class="p-producto-stars_contenedor_stars_total_div">
                        <p>
                          <span><b>5</b></span>
                          <span>
                            <i class="fas fa-star"></i>
                          </span>
                        </p>
                        <div class="p-producto-stars_total_contenedor_porcentaje">
                          <div id="id-producto-stars_total_porcentaje_5"></div>
                        </div>
                        <p id="id-producto-stars_total_5">0</p>
                      </div>
                      <div class="p-producto-stars_contenedor_stars_total_div">
                        <p>
                          <span><b>4</b></span>
                          <span>
                            <i class="fas fa-star"></i>
                          </span>
                        </p>
                        <div class="p-producto-stars_total_contenedor_porcentaje">
                          <div id="id-producto-stars_total_porcentaje_4"></div>
                        </div>
                        <p id="id-producto-stars_total_4">0</p>
                      </div>
                      <div class="p-producto-stars_contenedor_stars_total_div">
                        <p>
                          <span><b>3</b></span>
                          <span>
                            <i class="fas fa-star"></i>
                          </span>
                        </p>
                        <div class="p-producto-stars_total_contenedor_porcentaje">
                          <div id="id-producto-stars_total_porcentaje_3"></div>
                        </div>
                        <p id="id-producto-stars_total_3">0</p>
                      </div>
                      <div class="p-producto-stars_contenedor_stars_total_div">
                        <p>
                          <span><b>2</b></span>
                          <span>
                            <i class="fas fa-star"></i>
                          </span>
                        </p>
                        <div class="p-producto-stars_total_contenedor_porcentaje">
                          <div id="id-producto-stars_total_porcentaje_2"></div>
                        </div>
                        <p id="id-producto-stars_total_2">0</p>
                      </div>
                      <div class="p-producto-stars_contenedor_stars_total_div">
                        <p>
                          <span><b>1</b></span>
                          <span>
                            <i class="fas fa-star"></i>
                          </span>
                        </p>
                        <div class="p-producto-stars_total_contenedor_porcentaje">
                          <div id="id-producto-stars_total_porcentaje_1"></div>
                        </div>
                        <p id="id-producto-stars_total_1">0</p>
                      </div>
                    </div>
                  </div>*/

  // SI ES IGUAL A 1, NO APLICA ENVIO GRATIS AL PRODUCTO
  if($envioGratisPermitido === 0 && $activo !== 0){
    $total_compra_envioGratis = (float) TOTAL_COMPRA;
?>
          <div class="p-notification p-notification_letter_info p-control_marginTop" style="font-weight: normal;">
            <span>No aplica <b>Envío gratis</b> cuando sobrepasa los <b>$ <?php echo number_format($total_compra_envioGratis, 2, '.', ','); ?> MXN</b> al momento de comprar <b>(aplica por cada almacén)</b>.</span>
          </div>
<?php
  }
?>
                  <div id="id-producto-contenedor_precio_total"></div>
                </div>

                <div id="id-producto-contenedor_cantidad">
                  <div class="p-loading-general p-producto-loading p-busqueda-loading_padding" style="display: block;">
                    <div></div>
                  </div>
                </div>
                <div class="p-buttons p-producto-buttons_contenedor" id="id-producto-contenedor_botones"></div>
                <div class="p-buttons p-producto-buttons_contenedor" id="id-producto-contenedor_botones_ficha" style="margin-top: -1rem;"></div>
              </div>
            </div>
          </div>
        </div>
<?php
}
?>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php
if($brand_codigoIncorrecto === false && $brand_consultaError === false && $brand_productoNoExiste === false){
?>
    <div class="p-producto-contenedor_modal" id="id-producto-contenedor_modal_pdf">
      <div class="p-producto-fondo_modal g-producto-boton_cerrar_modal_pdf"></div>
      <div class="p-producto-modal_elementos">
        <div class="p-producto-modal_archivos_contenedor">
          <a class="p-button p-button_delete p-button_square p-producto-modal_button_delete g-producto-boton_cerrar_modal_pdf">
            <span>
              <i class="fas fa-lg fa-times"></i>
            </span>
          </a>
          <div class="p-producto-modal_pdf_contenedor">
            <object class="p-producto-modal_pdf_archivo" id="modal-object-ficha_tecnica" type="application/pdf"></object>
          </div>
        </div>
      </div>
    </div>
    
    <div class="p-producto-contenedor_modal" id="id-producto-contenedor_modal_galeria">
      <div class="p-producto-fondo_modal" id="id-producto-fondo_modal_galeria"></div>
      <div class="p-producto-modal_elementos">
        <div class="p-producto-modal_carrusel_contenedor">
          <a class="p-button p-button_delete p-button_square p-producto-modal_button_delete" id="id-producto-boton_cerrar_galeria">
            <span>
              <i class="fas fa-lg fa-times"></i>
            </span>
          </a>
          <span class="p-producto-carrusel_num_imagenes" id="id-producto-carrusel_num_imagenes_span"></span>
          <span class="p-producto-carrusel_text_descripcion"><?php echo $descripcion; ?></span>
          <div class="p-producto-carrusel_contenedor">
            <ul class="p-producto-carrusel_imagenes">
              <li>
                <picture>
                  <img src="<?php echo $imagen1 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt1; ?>">
                </picture>
              </li>
<?php
  if(file_exists($imagen2)){
?>
              <li>
                <picture>
                  <img src="<?php echo $imagen2 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt2; ?>">
                </picture>
              </li>
<?php
  }
  
  if(file_exists($imagen3)){
?>
              <li>
                <picture>
                  <img src="<?php echo $imagen3 . '?v=' . $versionImagen; ?>" alt="<?php echo $alt3; ?>">
                </picture>
              </li>
<?php
  }
?>
            </ul>
<?php
  if(file_exists($imagen2)){
?>
            <div class="p-producto-carrusel_flecha_izquierda">
              <span>
                <i class="fas fa-angle-left"></i>
              </span>
            </div>
            <div class="p-producto-carrusel_flecha_derecha">
              <span>
                <i class="fas fa-angle-right"></i>
              </span>
            </div>
<?php
  }
?>
          </div>
        </div>
      </div>
    </div>
<?php
}
?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php
if($brand_codigoIncorrecto === false && $brand_consultaError === false && $brand_productoNoExiste === false){
?>
    <script src="scripts/producto/scripts_producto.js?v=4.2"></script>
<?php
}
?>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html>