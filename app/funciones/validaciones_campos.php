<?php
// Funcion para validar un campo con solo letras mayusculas, minusculas, con acentos y ñ
function validar_campo_letras_espacios($valor_input){
  return preg_match("/^[a-zA-Z\ \á\é\í\ó\ú\ñ\Á\É\Í\Ó\Ú\Ñ]+$/", $valor_input) ? true : false;
}

// Funcion para validar un campo numerico
function validar_campo_numerico($valor_input){
  return preg_match("/^\d+$/", $valor_input) ? true : false;
}

// Funcion para validar un campo binario
function validar_campo_binario($valor_input){
  return preg_match("/^[0|1]{1}$/", $valor_input) ? true : false;
}

// Funcion para validar campos: Nombre de vialidad, Entre calles, Referencias Adicionales
function validar_campo_letras_espacios_simbolos($valor_input){
  return preg_match("/^[a-zA-Z0-9\á\é\í\ó\ú\ñ\Á\É\Í\Ó\Ú\Ñ\ \-\+\/\.\,]+$/", $valor_input) ? true : false;
}

// Funcion para validar campo "No. Exterior/Interior"
function validar_campo_noExt_Int($valor_input){
  return preg_match("/^[a-zA-Z0-9\á\é\í\ó\ú\ñ\Á\É\Í\Ó\Ú\Ñ\ \-]+$/", $valor_input) ? true : false;
}

// Funcion para validar un campo de fecha
function validar_campo_fecha($valor_input){
  return preg_match("/^[0-9\-]+$/", $valor_input) ? true : false;
}

// Funcion para validar un campo de hora
function validar_campo_hora($valor_input){
  return preg_match("/^[0-9\:]+$/", $valor_input) ? true : false;
}

// Funcion para validar un token
function validar_token($valor_input){
  return preg_match("/^[a-zA-Z0-9\_]+$/", $valor_input) ? true : false;
}

// Funcion para validar los caracteres de una sku
function validar_sku_caracteres($valor_input){
  return preg_match("/^[a-zA-Z0-9\-]+$/", $valor_input) ? true : false;
}

// Funcion para validar el formato de una sku de PCH
function validar_sku_PCH($valor_input){
  return preg_match("/^[a-zA-Z]{2}\-{1}[a-zA-Z0-9]+\-{1}[a-zA-Z0-9]{1,10}$/", $valor_input) ? true : false;
}

// Funcion para validar que la descripcion del producto no contenga: @@version | @@VERSION | DELETE | delete | __usuarios | __tarjetas_u | __direcciones
function validar_descripcion_producto($valor_input){
  return preg_match("[@@version|@@VERSION|DELETE|delete|__usuarios|__tarjetas_u|__direcciones]", $valor_input) ? true : false;
}

// Funcion para validar los caracteres del precio: 0.00
function validar_precio_caracteres($valor_input){
  return preg_match("/^[0-9\.]+$/", $valor_input) ? true : false;
}

// Funcion para validar el formato del precio: 0.00
function validar_precio_formato($valor_input){
  return preg_match("/^\d+\.{1}\d{2}$/", $valor_input) ? true : false;
}

// Funcion para validar la moneda del producto: MXN
function validar_moneda_producto($valor_input){
  return preg_match("/^[A-Z]{3}$/", $valor_input) ? true : false;
}

// Funcion para validar el id del producto en el carrito
function validar_id_producto_carrito($valor_input){
  return preg_match("/^\d{1,2}\_{1}\d+$/", $valor_input) ? true : false;
}

// Funcion para validar el nombre del almacén en el carrito
function validar_nombreAlmacen_carrito($valor_input){
  return preg_match("/^[A-Z\Á\É\Í\Ó\Ú\Ñ\ ]+$/", $valor_input) ? true : false;
}

// Funcion para validar el RFC
function validar_rfc($valor_input){
  return preg_match("/^[A-Z0-9\Á\É\Í\Ó\Ú\Ñ]+$/", $valor_input) ? true : false;
}

// Funcion para validar el codigo de usuario
function validar_codigoUsuario($valor_input){
  return preg_match("/^[A-Z0-9\_\-]+$/", $valor_input) ? true : false;
}

// Funcion para validar el numero de guia en el envío
function validar_numero_guia($valor_input){
  return preg_match("/^[a-zA-Z0-9\-\_\,\ ]+$/", $valor_input) ? true : false;
}

// Funcion para validar los caracteres de un codigo de producto
function validar_codigoProducto_caracteres($valor_input){
  return preg_match("/^[0-9\-]+$/", $valor_input) ? true : false;
}

// Funcion para validar el formato de un codigo de producto
function validar_codigoProducto_formato($valor_input){
  return preg_match("/^[0-9]{3}\-{1}[0-9]+$/", $valor_input) ? true : false;
}
?>