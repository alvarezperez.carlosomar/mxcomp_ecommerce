<?php
// Funcion para validar una password
function validar_password($valor_input){
  return preg_match("/^[a-zA-Z0-9\!\"\@\#\$\%\*\_]+$/", $valor_input) ? true : false;
}

// Funcion para determinar la seguridad de una password
function seguridad_password($password){
  $seguridad = 0;
  
  if($password !== ""){
    $numero = false;
    $letraMinuscula = false;
    $letraMayuscula = false;
    $simbolo = false;
    $cantidad_minima = false;
    
    // Si tiene números
    if(preg_match("/[0-9]+/", $password)){
      $seguridad += 5;
      $numero = true;
    }
    
    // Si tiene letras minúsculas
    if(preg_match("/[a-z]+/", $password)){
      $seguridad += 5;
      $letraMinuscula = true;
    }
    
    // Si tiene letras mayúsculas
    if(preg_match("/[A-Z]+/", $password)){
      $seguridad += 5;
      $letraMayuscula = true;
    }
    
    // Si tiene símbolos
    if(preg_match("/[\!|\"|\@|\#|\$|\%|\*|\_]+/", $password)){
      $seguridad += 5;
      $simbolo = true;
    }
    
    // Si al menos tiene una longitud de 8 caracteres
    if(mb_strlen($password) >= 8 && ($numero || $letraMinuscula || $letraMayuscula || $simbolo)){
      $seguridad += 10;
    }
    
    // Dependiendo la longitud de la cadena, se agregará un porcentaje de seguridad
    if($numero && $letraMinuscula && $letraMayuscula && $simbolo){
      $seguridad += mb_strlen($password) < 9 ? 0 : ( mb_strlen($password) === 9 ? 10 : ( mb_strlen($password) === 10 ? 20 : ( mb_strlen($password) === 11 ? 30 : ( mb_strlen($password) === 12 ? 40 : ( mb_strlen($password) === 13 ? 50 : ( mb_strlen($password) === 14 ? 60 : 70 ) ) ) ) ) );
    }
  }
  
  return $seguridad;
}
?>