<?php
// FUNCIÓN PARA CONVERTIR UNA CADENA EN UNA URL VÁLIDA
function convertir_URL($cadena){
	$cadena_salida = mb_strtolower($cadena, 'UTF-8');
  
	// Caracteres inválidos en una URL
  $buscar_caracteres = array('¥', 'µ', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'þ', 'ß', '\'', '"');
  
  // Traducción caracteres válidos
  $caracteres_validos = array('-', '-', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'ni', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'th', 'ss', '', '');
  
	$cadena_salida = str_replace($buscar_caracteres, $caracteres_validos, $cadena_salida);
  
	// Más caracteres inválidos en una url que sustituiremos por guión
	$buscar_caracteres = array(' ', '&', '%', '$', '·', '!', '¡', '(', ')', '?', '¿', '¡', ':', ';', '+', '*', '=', '\n', '\r\n', '\\', '´', '`', '¨', ']', '[', '/', '.', ',', '#', '\'', '@', '[', ']', '‘', '<', '>', '^', '`', '{', '}', '|', '€', 'ƒ', '„', '…', '†', '‡', 'ˆ', '‰', 'Š', '‹', 'Œ', 'Ž', '‘', '’', '’', '“', '”', '•', '–', '—', '˜', '™', 'š', '›', 'œ', 'ž', 'Ÿ', '¢', '£', '¥', '§', '©', 'ª', '«', '¬', '¯', '®', '°', '±', '²', '³', '¶', '¸', '¹', 'º', '»', '¼', '½', '¾', '÷');
  
	$cadena_salida = str_replace($buscar_caracteres, '-', $cadena_salida);
	$cadena_salida = str_replace('--', '-', $cadena_salida);
	$cadena_salida = str_replace('--', '-', $cadena_salida);
  
  if(substr($cadena_salida, mb_strlen($cadena_salida) - 1) === "-"){
    $cadena_salida = substr($cadena_salida, 0, mb_strlen($cadena_salida) - 1);
  }
  return $cadena_salida;
}
?>