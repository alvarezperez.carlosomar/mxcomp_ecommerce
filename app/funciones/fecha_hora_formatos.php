<?php
// GENERA LA FECHA CON NOMBRE DE MES
function fecha($fecha){
  $mes_array = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

  $dia = date('j', strtotime($fecha));
  $mes = $mes_array[date('n', strtotime($fecha)) - 1];
  $anio = date('Y', strtotime($fecha));

  return $dia . ' de ' . $mes . ' de ' . $anio;
}

// GENERA LA HORA CON FORMATO AM/PM
function hora($fecha){
  return date("g:i a", strtotime($fecha));
}

// GENERA LA FECHA CON NOMBRE DE MES Y LA HORA CON FORMATO AM/PM
function fecha_con_hora($fecha){
  $mes_array = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

  $dia = date('j', strtotime($fecha));
  $mes = $mes_array[date('n', strtotime($fecha)) - 1];
  $anio = date('Y', strtotime($fecha));

  $fechaGenerada = $dia . ' de ' . $mes . ' de ' . $anio;
  
  return $fechaGenerada . ', a la(s) ' . date("g:i a", strtotime($fecha));
}
?>