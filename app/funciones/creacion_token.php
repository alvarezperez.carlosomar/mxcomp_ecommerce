<?php
// Creacion de token con 30 caracteres, para uso de activacion o restablecimiento de datos
function token_30(){
  $letras = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_';
  $letrasAleatorias = str_shuffle($letras);
  $token = mb_substr($letrasAleatorias, 0, 30);
  
  return $token;
}

// Creacion de token con 4 caracteres, para uso en el inicio de la password despues de encriptarla
function token_4(){
  $letras = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_';
  $letrasAleatorias = str_shuffle($letras);
  $token = mb_substr($letrasAleatorias, 0, 4);
  
  return $token;
}
?>