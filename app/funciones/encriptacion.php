<?php
// Encriptar campos
function encriptar($valor){
  require dirname(__DIR__, 2) . '/vendor/autoload.php';
  $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 2));
  $dotenv->load();

  //WEBSITE LOCAL
  $METHOD = $_ENV['METHOD'];
  $KEY = $_ENV['KEY'];
  $IV = $_ENV['IV'];
  
  return openssl_encrypt(trim($valor), $METHOD, $KEY, 0, $IV);
}


// Desencriptar campos
function desencriptar($valor){
  require dirname(__DIR__, 2) . '/vendor/autoload.php';
  $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 2));
  $dotenv->load();

  //WEBSITE LOCAL
  $METHOD = $_ENV['METHOD'];
  $KEY = $_ENV['KEY'];
  $IV = $_ENV['IV'];
  
  return openssl_decrypt(trim($valor), $METHOD, $KEY, 0, $IV);
}


// Encriptar campos con codigo de usuario encriptado
function encriptar_con_clave($valor, $clave){
  require dirname(__DIR__, 2) . '/vendor/autoload.php';
  $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 2));
  $dotenv->load();

  //WEBSITE LOCAL
  $METHOD = $_ENV['METHOD'];
  $IV = $_ENV['IV'];
  
  return openssl_encrypt(trim($valor), $METHOD, trim($clave), 0, $IV);
}


// Desencriptar campos con codigo de usuario encriptado
function desencriptar_con_clave($valor, $clave){
  require dirname(__DIR__, 2) . '/vendor/autoload.php';
  $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 2));
  $dotenv->load();

  //WEBSITE LOCAL
  $METHOD = $_ENV['METHOD'];
  $IV = $_ENV['IV'];
  
  return openssl_decrypt(trim($valor), $METHOD, trim($clave), 0, $IV);
}
?>