<?php
//funcion para validar un correo electronico
function validar_correo($correo){
  $correo = filter_var($correo, FILTER_SANITIZE_EMAIL);
  
  $resultado = (false !== filter_var($correo, FILTER_VALIDATE_EMAIL));
  
  if($resultado){
    $correo_dividido = explode('@', $correo);
    
    $mxhosts = array();
    $mxweights = array();
    if(getmxrr($correo_dividido[1], $mxhosts, $mxweights)){
      $resultado = true;
    }else{
      $resultado = false;
    }
  }
  
  return $resultado;
}
?>