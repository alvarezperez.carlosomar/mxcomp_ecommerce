<?php
require_once dirname(__DIR__, 2) . '/conekta-php/lib/Conekta.php';
require_once dirname(__DIR__, 2) . '/conn.php';
require_once dirname(__DIR__, 3) . '/vendor/autoload.php';

date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

class Pago_conekta{
  private $Conekta_key_privada;
  private $Conekta_version;
  private $Conekta_cliente;
  private $Conekta_orden;
  private $idClienteConekta;
  private $idOrdenConekta;
  
  public function __construct($cliente_nombre, $cliente_correo, $cliente_id, $cliente_codigo, $ordenCompra, $token_id, $precio_envio, $obj_listaProductos, $obj_contactoEnvio){
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();
    
    $this->conn_MXcomp = new Conexion_mxcomp();
    $this->conn_Admin = new Conexion_admin();
    $this->Conekta_version = $_ENV['CONEKTA_VERSION'];
    $this->Conekta_key_privada = $_ENV['CONEKTA_KEY_PRIVADA'];
    $this->cliente_nombre = $cliente_nombre;
    $this->cliente_correo = $cliente_correo;
    $this->cliente_id = $cliente_id;
    $this->cliente_codigo = $cliente_codigo;
    $this->ordenCompra = $ordenCompra;
    $this->token_id = $token_id;
    $this->precio_envio = $precio_envio;
    $this->obj_listaProductos = $obj_listaProductos;
    $this->obj_contactoEnvio = $obj_contactoEnvio;
  }
  
  public function Pago(){
    \Conekta\Conekta::setApiKey($this->Conekta_key_privada);
    \Conekta\Conekta::setApiVersion($this->Conekta_version);
    
    if(!$this->CrearCliente())
      return false;
    
    if(!$this->CrearOrden())
      return false;
    
    return true;
  }
  
  public function CrearCliente(){
    $fechaActual = date("Y-m-d H:i:s");
    $idUsuario = $this->cliente_id;
    $codigoUsuario = $this->cliente_codigo;
    $existe_id_conektaCliente = false;
    
    /*try{
      $this->conn_MXcomp->pdo->beginTransaction();
      
      $sql = "SELECT idConektaCliente FROM __usuarios WHERE id = :id AND codigoUsuario = :codigoUsuario";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $id_clienteConekta = $stmt->fetchColumn();
      
      if(!is_null($id_clienteConekta)){
        $existe_id_conektaCliente = true;
        $this->idClienteConekta = $id_clienteConekta;
      }else{
        $existe_id_conektaCliente = false;
      }

      $stmt = null;
      $this->conn_MXcomp->pdo->commit();
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      $this->respuesta = "2"; // MENSAJE DE ERROR
      //$this->mensaje = $error->getMesage();
      $this->mensaje = "Ocurrió un error al buscar tu usuario. Ponte en contacto con atención a clientes si este mensaje sigue apareciendo.";
      return false;
    }
    
    if(!$existe_id_conektaCliente){*/
      try{
        $this->Conekta_cliente = \Conekta\Customer::create(
          array(
            "name" => $this->cliente_nombre,
            "email" => $this->cliente_correo,
            //"phone" => "+52181818181",

            "metadata" => array(
              "MXcomp_idCliente" => $this->cliente_id
            ), // Datos adicionales para el cliente

            "payment_sources" => array(
              array(
                "type" => "card",
                "token_id" => $this->token_id
              )
            )// Objeto Método de pago
          )// Objeto Cliente
        );
      }catch(\Conekta\ProccessingError $error){
        $this->respuesta = "2"; // MENSAJE DE ERROR
        $this->mensaje = $error->getMesage()." [Cliente Conekta]";
        return false;
      }catch(\Conekta\ParameterValidationError $error){
        $this->respuesta = "2"; // MENSAJE DE ERROR
        $this->mensaje = $error->getMessage()." [Cliente Conekta]";
        return false;
      }catch(\Conekta\Handler $error){
        $this->respuesta = "2"; // MENSAJE DE ERROR
        $this->mensaje = $error->getMessage()." [Cliente Conekta]";
        return false;
      }
      
      // Se obtiene el id del cliente en conekta para almacenarse en la tabla __usuarios
      $this->idClienteConekta = $this->Conekta_cliente->id;
      
      /*$id_clienteConekta = $this->idClienteConekta;

      try{
        $this->conn_MXcomp->pdo->beginTransaction();
        
        $sql = "UPDATE __usuarios SET idConektaCliente = :idConektaCliente, fechaActualizacion = :fechaActualizacion WHERE id = :id AND codigoUsuario = :codigoUsuario";
        $stmt = $this->conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':idConektaCliente', $id_clienteConekta, PDO::PARAM_STR);
        $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
        $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
        $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
        $stmt->execute();

        $stmt = null;
        $this->conn_MXcomp->pdo->commit();
      }catch(PDOException $error){
        $this->conn_MXcomp->pdo->rollBack();
        $this->respuesta = "0";
        $this->mensaje = $error->getMesage()." [Cliente BD]";
      }
    }*/
    
    return true;
  }
  
  public function CrearOrden(){
    try{
      $this->Conekta_orden = \Conekta\Order::create(
        array(
          "line_items" => $this->obj_listaProductos, // Objeto Lista de productos

          "shipping_lines" => array(
            array(
              "amount" => $this->precio_envio,
              "carrier" => "Estafeta (Estafeta Mexicana, S.A. de C.V.)" // SE MODIFICA CUANDO SE AGREGAN LOS DATOS DE ENVÍO
            )
          ), // Objeto Envio (Sólo bienes físicos)

          "currency" => "MXN",

          "customer_info" => array(
            "customer_id" => $this->idClienteConekta
          ), // Objeto Información del cliente

          "shipping_contact" => $this->obj_contactoEnvio, // Objeto Contacto de envío (Requerido sólo para bienes físicos)
          
          "metadata" => array(
            "MXcomp_codigo_cliente" => $this->cliente_codigo,
            "MXcomp_orden_compra" => $this->ordenCompra
          ), // Datos adicionales para la orden de pago

          "charges" => array(
            array(
              "payment_method" => array(
                "type" => "default"
              ) // Usa el valor predeterminado del cliente, en este caso -> card
            )
          ) // Objeto Cargo
        )
      );

      $this->idOrdenConekta = $this->Conekta_orden->id;
    }catch(\Conekta\ProcessingError $error){
      $this->respuesta = "2"; // MENSAJE DE ERROR
      $this->mensaje = $error->getMessage()." [Orden Conekta]";
      return false;
    }catch(\Conekta\ParameterValidationError $error){
      $this->respuesta = "2"; // MENSAJE DE ERROR
      $this->mensaje = $error->getMessage()." [Orden Conekta]";
      return false;
    }catch(\Conekta\Handler $error){
      $this->respuesta = "2"; // MENSAJE DE ERROR
      $this->mensaje = $error->getMessage()." [Orden Conekta]";
      return false;
    }
    
    return true;
  }
  
  public function Actualizar_registro_datosPago(){
    try{
      $this->conn_Admin->pdo->beginTransaction();
      
      $idOrdenConekta = $this->idOrdenConekta;
      $ordenCompra = $this->ordenCompra;
      $idUsuario = $this->cliente_id;
      $codigoUsuario = $this->cliente_codigo;

      $sql = "UPDATE __orden_compra_datos_pago SET idOrdenConekta = :idOrdenConekta WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':idOrdenConekta', $idOrdenConekta, PDO::PARAM_STR);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $stmt = null;
      $this->conn_Admin->pdo->commit();
    }catch(PDOException $error){
      $this->conn_Admin->pdo->rollBack();
      $this->mensaje_error = "Pago Conekta: Se procesó el pago con Conekta, pero no se insertó el ID que proporciona en la tabla. - ".$error->getMessage();
      return false;
    }
    
    return true;
  }
  
  public function Eliminar_registros_ordenCompra(){
    $ordenCompra = $this->ordenCompra;
    $idUsuario = $this->cliente_id;
    $codigoUsuario = $this->cliente_codigo;
    
    try{
      $this->conn_Admin->pdo->beginTransaction();

      $sql = "DELETE FROM __remision_estados WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $sql = "DELETE FROM __remisiones WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $sql = "DELETE FROM __orden_compra_productos WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $sql = "DELETE FROM __orden_compra_datos_pago WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $sql = "DELETE FROM __ordenes_compra WHERE ordenCompra = :ordenCompra AND clienteID = :clienteID AND codigoCliente = :codigoCliente";
      $stmt = $this->conn_Admin->pdo->prepare($sql);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->bindParam(':clienteID', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();

      $stmt = null;
      $this->conn_Admin->pdo->commit();
    }catch(PDOException $error){
      $this->conn_Admin->pdo->rollBack();
      $this->mensaje_error = "Pago Conekta: No se eliminaron los registros de la orden de compra #" . $ordenCompra . " cuando no se procesó el pago con Conekta. - " . $error->getMessage();
      return false;
    }
    
    return true;
  }
}
?>