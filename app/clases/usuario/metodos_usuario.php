<?php
require_once dirname(__DIR__, 2) . '/conn.php';

class Usuario{
  private $idUsuario;
  private $codigoUsuario;
  private $conn_MXcomp;

  public function __construct($idUsuario, $codigoUsuario){
    $this->conn_MXcomp = new Conexion_mxcomp();
    $this->idUsuario = $idUsuario;
    $this->codigoUsuario = $codigoUsuario;
  }

  public function buscarUsuario(){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;

    try{
      $sql = "SELECT COUNT(id) AS conteo, correo FROM __usuarios WHERE BINARY id = :idUsuario AND codigoUsuario = :codigoUsuario";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];

      if($usuario_existe === 1){
        $this->buscarUsuario_correo = trim($datos_usuario['correo']);
        return true;
      }else{
        $this->buscarUsuario_opcion = '1';
        $this->buscarUsuario_mensaje = 'El usuario no existe.';
        return false;
      }
    }catch(PDOException $error){
      $this->buscarUsuario_opcion = '2';
      //$this->buscarUsuario_mensaje = 'Error: ' . $error->getMessage();
      $this->buscarUsuario_mensaje = 'Ocurrió un problema al buscar el usuario.';
      return false;
    }
  }
}
?>