<?php
require_once dirname(__DIR__, 2) . '/conn.php';

class Producto{
  private $conn_MXcomp;
  private $conn_Admin;
  
  public function __construct(){
    $this->conn_MXcomp = new Conexion_mxcomp();
    //$this->conn_Admin = new Conexion_admin();
  }

  public function buscarProducto($codigoProducto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, almacenes, nombreProveedor, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __productos WHERE BINARY codigoProducto = :codigoProducto";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) trim($datos_producto['conteo']);

      if($producto_existe){
        $this->buscarProducto_skuProveedor = trim($datos_producto['skuProveedor']);
        $this->buscarProducto_descripcion = trim($datos_producto['descripcion']);
        $this->buscarProducto_descripcionURL = trim($datos_producto['descripcionURL']);
        $this->buscarProducto_nombreMarca = trim($datos_producto['nombreMarca']);
        $this->buscarProducto_precioProveedor = trim($datos_producto['precioProveedor']);
        $this->buscarProducto_monedaProveedor = trim($datos_producto['monedaProveedor']);
        $this->buscarProducto_precioMXcomp = trim($datos_producto['precioMXcomp']);
        $this->buscarProducto_monedaMXcomp = trim($datos_producto['monedaMXcomp']);
        $this->buscarProducto_almacenes = json_decode($datos_producto['almacenes'], true);
        $this->buscarProducto_nombreProveedor = trim($datos_producto['nombreProveedor']);
        $this->buscarProducto_tieneImagen = trim($datos_producto['tieneImagen']);
        $this->buscarProducto_numeroUbicacionImagen = trim($datos_producto['numeroUbicacionImagen']);
        $this->buscarProducto_nombreImagen = trim($datos_producto['nombreImagen']);
        $this->buscarProducto_versionImagen = trim($datos_producto['versionImagen']);
        return true;
      }else{
        $this->buscarProducto_opcion = '1';
        $this->buscarProducto_mensaje = 'El producto no existe.';
        return false;
      }
    }catch(PDOException $error){
      $this->buscarProducto_opcion = '2';
      //$this->buscarProducto_mensaje = 'Error: ' . $error->getMessage();
      $this->buscarProducto_mensaje = 'Ocurrió un problema al buscar el producto.';
      return false;
    }
  }

  public function actualizarPrecioExistencia($precioProveedor, $monedaProveedor, $precioMXcomp, $monedaMXcomp, $existenciaTotal, $almacenes, $fechaActualizacion, $codigoProducto, $skuProveedor){
    try{
      $this->conn_MXcomp->pdo->beginTransaction();

      $sql = "UPDATE __productos SET precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, existenciaTotal = :existenciaTotal, almacenes = :almacenes, activo = 1, fechaActualizacion = :fechaActualizacion WHERE BINARY codigoProducto = :codigoProducto AND skuProveedor = :skuProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
      $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
      $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
      $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
      $stmt->execute();
      $this->conn_MXcomp->pdo->commit();
      return true;
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      //$this->actualizarPrecioExistencia_mensaje = 'Error: ' . $error->getMessage();
      $this->actualizarPrecioExistencia_mensaje = 'No fue posible actualizar el precio y la existencia de un producto.';
      return false;
    }
  }

  public function actualizarExistencia($existenciaTotal, $almacenes, $fechaActualizacion, $codigoProducto, $skuProveedor){
    try{
      $this->conn_MXcomp->pdo->beginTransaction();

      $sql = "UPDATE __productos SET existenciaTotal = :existenciaTotal, almacenes = :almacenes, fechaActualizacion = :fechaActualizacion WHERE BINARY codigoProducto = :codigoProducto AND skuProveedor = :skuProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':existenciaTotal', $existenciaTotal, PDO::PARAM_INT);
      $stmt->bindParam(':almacenes', $almacenes, PDO::PARAM_STR);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
      $stmt->execute();
      $this->conn_MXcomp->pdo->commit();
      return true;
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      //$this->actualizarExistencia_mensaje = 'Error: ' . $error->getMessage();
      $this->actualizarExistencia_mensaje = 'No fue posible actualizar la existencia del producto.';
      return false;
    }
  }

  public function colocarSinExistencia($fechaActualizacion, $codigoProducto, $skuProveedor){
    try{
      $this->conn_MXcomp->pdo->beginTransaction();

      $sql = "UPDATE __productos SET existenciaTotal = 0, almacenes = NULL, activo = 0, fechaActualizacion = :fechaActualizacion WHERE BINARY codigoProducto = :codigoProducto AND skuProveedor = :skuProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
      $stmt->execute();
      $this->conn_MXcomp->pdo->commit();
      return true;
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      //$this->colocarSinExistencia_mensaje = 'Error: ' . $error->getMessage();
      $this->colocarSinExistencia_mensaje = 'No fue posible desactivar un producto.';
      return false;
    }
  }

  public function ver_productoComprar($codigoProducto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, descripcion, precioMXcomp, monedaMXcomp, almacenes, envioGratisPermitido FROM __productos WHERE BINARY codigoProducto = :codigoProducto";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];

      if($producto_existe === 1){
        $this->ver_productoComprar_descripcion = (string) trim($datos_producto['descripcion']);
        $this->ver_productoComprar_precioMXcomp = (float) trim($datos_producto['precioMXcomp']);
        $this->ver_productoComprar_monedaMXcomp = (string) trim($datos_producto['monedaMXcomp']);
        $this->ver_productoComprar_almacenes = (array) json_decode($datos_producto['almacenes'], true);
        $this->ver_productoComprar_envioGratisPermitido = (int) trim($datos_producto['envioGratisPermitido']);
        return true;
      }else{
        $this->ver_productoComprar_opcion = '1';
        $this->ver_productoComprar_mensaje = 'El producto no existe.';
        return false;
      }
    }catch(PDOException $error){
      $this->ver_productoComprar_opcion = '2';
      //$this->ver_productoComprar_mensaje = 'Error: ' . $error->getMessage();
      $this->ver_productoComprar_mensaje = "Hubo un problema al buscar la información del producto.";
      return false;
    }
  }

  public function generarCodigo_consulta($nombreProveedor, $codigoProveedor){
    try{
      $sql = "SELECT COUNT(id) AS conteo FROM __productos WHERE nombreProveedor = :nombreProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
      $stmt->execute();
      $productos_existen = (int) $stmt->fetchColumn();
      $codigoProducto = '';

      if($productos_existen === 0){
        $numeracion = 1;
        $this->codigoProducto = $this->generarCodigo_unionInfo('1', $codigoProveedor, $codigoProducto, $numeracion);
      }else{
        $sql = "SELECT codigoProducto FROM __productos WHERE nombreProveedor = :nombreProveedor ORDER BY id DESC LIMIT 1";
        $stmt = $this->conn_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
        $stmt->execute();
        $codigoProducto = (string) $stmt->fetchColumn();
        $this->codigoProducto = $this->generarCodigo_unionInfo('2', $codigoProveedor, $codigoProducto);
      }

      return true;
    }catch(PDOException $error){
      $this->mensajeError = '[Error BD] - Codigo del producto (Selects): ' . $error->getMessage();
      return false;
    }
  }

  public function generarCodigo_unionInfo($opcion, $codigoProveedor, $codigoProducto, $numeracion = NULL){
    switch($opcion){
      case '1':
        $codigoProducto_generado = $codigoProveedor . '-' . str_pad($numeracion, 8, '0', STR_PAD_LEFT);
        break;

      case '2':
        $codigoProducto_generado = (int) str_replace($codigoProveedor . '-', '', $codigoProducto);
        $numeracion_generada = $codigoProducto_generado + 1;
        $codigoProducto_generado = $codigoProveedor . '-' . str_pad($numeracion_generada, 8, '0', STR_PAD_LEFT);
        break;
    }

    return $codigoProducto_generado;
  }
}
?>