<?php
class PCH_Mayoreo{
  private $cliente;
  private $password;
  private $url;
  private $online;

  public function __construct(){
    require dirname(__DIR__, 3) . '/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();

    $this->cliente = $_ENV['PCH_USUARIO'];
    $this->password = $_ENV['PCH_PASSWORD'];
    $this->url = $_ENV['PCH_URL_SERVIDOR'];
    $this->localhost = $_ENV['PCH_LOCALHOST']; // true o false
  }

  private function conexion($seccion, $datosConexion){
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    ini_set('max_execution_time', '300');

    $url = (string) $this->url;

    switch($seccion){
    case '1': // TIPO DE CAMBIO
      $url .= "getparity/";
      break;

    case '2': // LISTA DE PRODUCTOS
      $url .= "getprodlist/";
      break;

    case '3': // REALIZAR PEDIDO
      $url .= "makeorder/";
      break;
    }

    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json",
    ));

    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($datosConexion));

    // Para localhost, si es true
    if($this->localhost === 'true'){
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    }

    $apiRespuesta = curl_exec($curl);
    curl_close($curl);

    return json_decode($apiRespuesta);
  }

  public function tipoCambio(){
    $datosConexion = [
      'customer' => (int) $this->cliente,
      'key' => (string) $this->password,
    ];

    $informacion = $this->conexion('1', $datosConexion);
    $this->tCambio_informacionArray = $informacion;

    if(!is_null($informacion->status) && !is_null($informacion->message)){
      if($informacion->status === 200 && $informacion->message === "exitoso"){
        $this->tCambio_valorMoneda = $informacion->data[0]->USD;
        return true;
      }else{
        $this->tCambio_mensajeTitulo = $informacion->message;
        $this->tCambio_mensajeDetalles = $informacion->message_detail;
        return false;
      }
    }else{
      $this->tCambio_mensajeTitulo = 'No se obtuvieron datos del API';
      $this->tCambio_mensajeDetalles = 'Error API: No se devolvió la información esperada.';
      return false;
    }
  }

  public function listaProductos($productos = ''){
    $datosConexion = [
      'customer' => (int) $this->cliente,
      'key' => (string) $this->password,
      'sku' => (string) $productos,
    ];

    $informacion = $this->conexion('2', $datosConexion);
    
    if(!empty($informacion)){
      $this->lProductos_informacionArray = $informacion;
      
      if($informacion->status === 200 && $informacion->message === "exitoso"){
        $this->lProductos_productos = $informacion->data->productos;
        $this->lProductos_skuErrores = $informacion->data->sku_errores;
        $this->lProductos_no_stock = $informacion->data->no_stock;
        return true;
      }else{
        $this->lProductos_opcion = '1';
        $this->lProductos_mensajeTitulo = $informacion->message;
        $this->lProductos_mensajeDetalles = $informacion->message_detail;
        return false;
      }
    }else{
      $this->lProductos_opcion = '2';
      $this->lProductos_mensajeTitulo = "No se encontró el producto";
      $this->lProductos_mensajeDetalles = "Error API: no devolvió nada el proveedor PCH.";
      return false;
    }
  }

  public function realizarPedido($id_almacen, $seguro_envio, $orden_compra, $nombre_recibe, $calle_numero, $colonia, $ciudad, $estado, $codigo_postal, $telefono, $productos_pedido){
    $datosConexion = [
      'customer' => (int) $this->cliente,
      'key' => (string) $this->password,
      'warehouse' => (int) $id_almacen,
      'insurance' => (bool) $seguro_envio,
      'ocnumber' => (string) $orden_compra,
      'receive' => (string) $nombre_recibe,
      'street' => (string) $calle_numero,
      'street2' => (string) $colonia,
      'city' => (string) $ciudad,
      'state' => (string) $estado,
      'zip' => (string) $codigo_postal,
      'phone' => (string) $telefono,
      'products' => $productos_pedido,
    ];

    $informacion = $this->conexion('3', $datosConexion);
    $this->rPedido_informacionArray = $informacion;
    
    if(empty($informacion->error)){
      if($informacion->status === 200 && $informacion->message === "exitoso"){
        $this->rPedido_pedidos = $informacion->data->pedidos;
        return true;
      }else{
        $this->rPedido_opcion = '1';
        $this->rPedido_mensajeTitulo = $informacion->message;
        $this->rPedido_mensajeDetalles = $informacion->message_detail;
        return false;
      }
    }else{
      $this->rPedido_opcion = '2';
      $this->rPedido_errorArray = $informacion->error;
      $this->rPedido_mensajeTitulo = $informacion->error->message;
      $this->rPedido_mensajeDetalles = "Error en el servidor de la API PCH.";
      return false;
    }
  }
}
?>