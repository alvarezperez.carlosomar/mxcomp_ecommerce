<?php
require_once dirname(__DIR__, 2) . '/conn.php';

class Carrito{
  private $idUsuario;
  private $codigoUsuario;
  private $conn_MXcomp;

  public function __construct($idUsuario, $codigoUsuario){
    $this->conn_MXcomp = new Conexion_mxcomp();
    $this->idUsuario = $idUsuario;
    $this->codigoUsuario = $codigoUsuario;
  }

  public function buscarProductos_comprar(){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;
    $tieneExistencias = '1';
    $guardado = '0';
    $registroExiste = '1';

    try{
      $sql = "SELECT codigoProducto, skuProveedor, descripcion, descripcionURL, nombreMarca, precioProveedor, monedaProveedor, precioMXcomp, monedaMXcomp, unidades, numeroAlmacen, nombreAlmacen, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
      $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
      $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
      $stmt->execute();
      $datos_carrito = $stmt->fetchAll();

      $this->buscarProductos_comprar_array = $datos_carrito;
      $stmt = null;

      return true;
    }catch(PDOException $error){
      //$this->buscarProductos_comprar_mensaje = 'Error: ' . $error->getMessage();
      $this->buscarProductos_comprar_mensaje = 'Hubo un problema al revisar las existencias de algunos productos en el carrito, vuelve a intentar el proceso de compra. Si este mensaje sigue apareciendo contacta con atención a clientes.';
      return false;
    }
  }

  public function buscarSkuProveedorUnica(){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;
    $tieneExistencias = '1';
    $guardado = '0';
    $registroExiste = '1';

    try{
      $sql = "SELECT DISTINCT skuProveedor, codigoProducto, nombreProveedor FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
      $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
      $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
      $stmt->execute();

      $array_skus_carrito = [];
      while($datos = $stmt->fetch(PDO::FETCH_ASSOC)){
        $posicion = count($array_skus_carrito);
        $array_skus_carrito[$posicion] = [
          'codigoProducto' => $datos['codigoProducto'],
          'skuProveedor' => $datos['skuProveedor'],
          'precioProveedor' => 0.00,
          'monedaProveedor' => '',
          'precioMXcomp' => 0.00,
          'monedaMXcomp' => '',
          'existenciaTotal' => 0,
          'almacenes' => null,
          'nombreProveedor' => $datos['nombreProveedor'],
          'tieneExistencia' => 1
        ];
      }

      if(count($array_skus_carrito) > 0){
        $this->buscarSkuProveedorUnica_array = $array_skus_carrito;
        return true;
      }else{
        $this->buscarSkuProveedorUnica_opcion = '1';
        $this->buscarSkuProveedorUnica_mensaje = 'No se encontraron productos en el carrito.';
        return false;
      }
    }catch(PDOException $error){
      $this->buscarSkuProveedorUnica_opcion = '2';
      //$this->buscarSkuProveedorUnica_mensaje = 'Error: ' . $error->getMessage();
      $this->buscarSkuProveedorUnica_mensaje = 'Ocurrió un problema al buscar los productos de tu carrito.';
      return false;
    }
  }

  public function actualizarPrecioExistencia($precioProveedor, $monedaProveedor, $precioMXcomp, $monedaMXcomp, $existenciaAlmacen, $existenciaTotalProducto, $fechaActualizacion, $codigoProducto, $skuProveedor, $numeroAlmacen, $nombreAlmacen, $nombreProveedor){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;

    try{
      $this->conn_MXcomp->pdo->beginTransaction();

      $sql = "UPDATE __carrito SET precioProveedor = :precioProveedor, monedaProveedor = :monedaProveedor, precioMXcomp = :precioMXcomp, monedaMXcomp = :monedaMXcomp, existenciaAlmacen = :existenciaAlmacen, existenciaTotalProducto = :existenciaTotalProducto, tieneExistencias = 1, registroExiste = 1, fechaActualizacion = :fechaActualizacion WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND skuProveedor = :skuProveedor AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':precioProveedor', $precioProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':monedaProveedor', $monedaProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':precioMXcomp', $precioMXcomp, PDO::PARAM_STR);
      $stmt->bindParam(':monedaMXcomp', $monedaMXcomp, PDO::PARAM_STR);
      $stmt->bindParam(':existenciaAlmacen', $existenciaAlmacen, PDO::PARAM_INT);
      $stmt->bindParam(':existenciaTotalProducto', $existenciaTotalProducto, PDO::PARAM_INT);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
      $stmt->execute();
      $this->conn_MXcomp->pdo->commit();
      return true;
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      //$this->actualizarPrecioExistencia_mensaje = 'Error: ' . $error->getMessage();
      $this->actualizarPrecioExistencia_mensaje = 'No fue posible actualizar el precio y la existencia de un producto en el carrito.';
      return false;
    }
  }

  public function colocarSinExistencia($fechaActualizacion, $codigoProducto, $skuProveedor, $numeroAlmacen, $nombreAlmacen, $nombreProveedor){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;

    try{
      $this->conn_MXcomp->pdo->beginTransaction();

      $sql = "UPDATE __carrito SET unidades = 0, existenciaAlmacen = 0, existenciaTotalProducto = 0, tieneExistencias = 0, fechaActualizacion = :fechaActualizacion WHERE BINARY idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND codigoProducto = :codigoProducto AND skuProveedor = :skuProveedor AND numeroAlmacen = :numeroAlmacen AND nombreAlmacen = :nombreAlmacen AND nombreProveedor = :nombreProveedor";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':fechaActualizacion', $fechaActualizacion, PDO::PARAM_STR);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->bindParam(':skuProveedor', $skuProveedor, PDO::PARAM_STR);
      $stmt->bindParam(':numeroAlmacen', $numeroAlmacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreAlmacen', $nombreAlmacen, PDO::PARAM_STR);
      $stmt->bindParam(':nombreProveedor', $nombreProveedor, PDO::PARAM_STR);
      $stmt->execute();

      $this->conn_MXcomp->pdo->commit();
      return true;
    }catch(PDOException $error){
      $this->conn_MXcomp->pdo->rollBack();
      //$this->colocarSinExistencia_mensaje = 'Error: ' . $error->getMessage();
      $this->colocarSinExistencia_mensaje = 'No fue posible desactivar un producto.';
      return false;
    }
  }

  public function ver_productosComprar(){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;
    $tieneExistencias = '1';
    $guardado = '0';
    $registroExiste = '1';

    try{
      $sql = "SELECT descripcion, precioMXcomp, monedaMXcomp, unidades, nombreAlmacen, existenciaAlmacen, envioGratisPermitido FROM __carrito WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND unidades != 0 AND tieneExistencias = :tieneExistencias AND guardado = :guardado AND registroExiste = :registroExiste";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':tieneExistencias', $tieneExistencias, PDO::PARAM_STR);
      $stmt->bindParam(':guardado', $guardado, PDO::PARAM_STR);
      $stmt->bindParam(':registroExiste', $registroExiste, PDO::PARAM_STR);
      $stmt->execute();

      $array_productos_carrito = [];
      while($datos_carrito = $stmt->fetch(PDO::FETCH_ASSOC)){
        $descripcion = (string) $datos_carrito['descripcion'];
        $precioMXcomp = (float) $datos_carrito['precioMXcomp'];
        $monedaMXcomp = (string) $datos_carrito['monedaMXcomp'];
        $unidades = (int) $datos_carrito['unidades'];
        $nombreAlmacen = (string) $datos_carrito['nombreAlmacen'];
        $existenciaAlmacen = (int) $datos_carrito['existenciaAlmacen'];
        $envioGratisPermitido = (int) $datos_carrito['envioGratisPermitido'];

        if( count($array_productos_carrito) === 0 || !array_key_exists($nombreAlmacen, $array_productos_carrito) ){
          $array_productos_carrito[$nombreAlmacen]['envioGratisPermitido_almacen'] = $envioGratisPermitido === 1 ? true : false;
          $array_productos_carrito[$nombreAlmacen]['importe_total'] = (float) round($precioMXcomp * $unidades, 2);

          $array_productos_carrito[$nombreAlmacen]['productos'][0] = [
            'descripcion' => (string) $descripcion,
            'precioMXcomp' => (float) $precioMXcomp,
            'monedaMXcomp' => (string) $monedaMXcomp,
            'unidades' => (int) $unidades,
            'existenciaAlmacen' => (int) $existenciaAlmacen
          ];
        }else{
          if($array_productos_carrito[$nombreAlmacen]['envioGratisPermitido_almacen'] === true){
            $array_productos_carrito[$nombreAlmacen]['envioGratisPermitido_almacen'] = $envioGratisPermitido === 1 ? true : false;
          }

          $array_productos_carrito[$nombreAlmacen]['importe_total'] += (float) round($precioMXcomp * $unidades, 2);

          $posicion = count($array_productos_carrito[$nombreAlmacen]['productos']);
          $array_productos_carrito[$nombreAlmacen]['productos'][$posicion] = [
            'descripcion' => (string) $descripcion,
            'precioMXcomp' => (float) $precioMXcomp,
            'monedaMXcomp' => (string) $monedaMXcomp,
            'unidades' => (int) $unidades,
            'existenciaAlmacen' => (int) $existenciaAlmacen
          ];
        }
      }

      if(count($array_productos_carrito) > 0){
        $this->ver_productosComprar_array = $array_productos_carrito;
        return true;
      }else{
        $this->ver_productosComprar_mensaje = 'No se encontraron productos en el carrito.';
        return false;
      }
    }catch(PDOException $error){
      //$this->ver_productosComprar_mensaje = 'Error: ' . $error->getMessage();
      $this->ver_productosComprar_mensaje = "Hubo un problema al buscar los productos del carrito.";
      return false;
    }
  }
}
?>