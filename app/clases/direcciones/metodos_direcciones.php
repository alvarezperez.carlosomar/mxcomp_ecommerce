<?php
require_once dirname(__DIR__, 2) . '/funciones/fecha_hora_formatos.php';
require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
require_once dirname(__DIR__, 2) . '/conn.php';

class Direccion{
  private $idUsuario;
  private $codigoUsuario;
  private $conn_MXcomp;

  public function __construct($idUsuario, $codigoUsuario){
    $this->conn_MXcomp = new Conexion_mxcomp();
    $this->idUsuario = $idUsuario;
    $this->codigoUsuario = $codigoUsuario;
  }

  public function ver_domicilioEnvio(){
    $idUsuario = (int) $this->idUsuario;
    $codigoUsuario = (string) $this->codigoUsuario;
    $codigoUsuario_encriptado = (string) encriptar($codigoUsuario);

    try{
      $sql = "SELECT COUNT(id) AS conteo, tipoVialidad, nombreVialidad, noExterior, noInterior, codigoPostal, colonia, ciudadMunicipio, nombreEstado, entreCalle1, entreCalle2, referenciasAdicionales, noTelefonico, nombreDestinatario, horaEntregaEnvio1, horaEntregaEnvio2 FROM __direcciones WHERE idUsuario = :idUsuario AND codigoUsuario = :codigoUsuario AND tipoDireccion = 'envio'";
      $stmt = $this->conn_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_STR);
      $stmt->execute();
      $datos_domicilioEnvio = $stmt->fetch(PDO::FETCH_ASSOC);
      $domicilioEnvio_existe = (int) $datos_domicilioEnvio['conteo'];

      if($domicilioEnvio_existe === 1){
        $tipoVialidad = trim($datos_domicilioEnvio['tipoVialidad']);
        $nombreVialidad = desencriptar_con_clave(trim($datos_domicilioEnvio['nombreVialidad']), $codigoUsuario_encriptado);
        $noExterior = desencriptar_con_clave(trim($datos_domicilioEnvio['noExterior']), $codigoUsuario_encriptado);
        $noInterior = desencriptar_con_clave(trim($datos_domicilioEnvio['noInterior']), $codigoUsuario_encriptado);
        $codigoPostal = desencriptar_con_clave(trim($datos_domicilioEnvio['codigoPostal']), $codigoUsuario_encriptado);
        $colonia = desencriptar_con_clave(trim($datos_domicilioEnvio['colonia']), $codigoUsuario_encriptado);
        $ciudadMunicipio = desencriptar_con_clave(trim($datos_domicilioEnvio['ciudadMunicipio']), $codigoUsuario_encriptado);
        $nombreEstado = desencriptar_con_clave(trim($datos_domicilioEnvio['nombreEstado']), $codigoUsuario_encriptado);

        // SE GENERA EL CAMPO "DIRECCION COMPLETA"
        $direccionCompleta_desencriptado = (string) $tipoVialidad . ' ' . $nombreVialidad . ' No. Ext. ' . $noExterior . ', No. Int. ' . $noInterior . ', C.P. ' . $codigoPostal . ', ' . $colonia . ', ' . $ciudadMunicipio . ', ' . $nombreEstado;

        // SE ENCRIPTA EL CAMPO "DIRECCION COMPLETA"
        $direccionCompleta_encriptado = encriptar_con_clave($direccionCompleta_desencriptado, $codigoUsuario_encriptado);

        //////////////////////////////////////////////////////////////////////////////

        $nombreDestinatario_encriptado = (string) trim($datos_domicilioEnvio['nombreDestinatario']); // ENCRIPTADO
        $nombreDestinatario_desencriptado = (string) desencriptar_con_clave(trim($datos_domicilioEnvio['nombreDestinatario']), $codigoUsuario_encriptado); // DESENCRIPTADO

        $noTelefonico_encriptado = (string) trim($datos_domicilioEnvio['noTelefonico']); // ENCRIPTADO
        $noTelefonico_desencriptado = (string) desencriptar_con_clave(trim($datos_domicilioEnvio['noTelefonico']), $codigoUsuario_encriptado); // DESENCRIPTADO

        //////////////////////////////////////////////////////////////////////////////

        $entreCalle1 = is_null($datos_domicilioEnvio['entreCalle1']) ? NULL : desencriptar_con_clave(trim($datos_domicilioEnvio['entreCalle1']), $codigoUsuario_encriptado);

        $entreCalle2 = is_null($datos_domicilioEnvio['entreCalle2']) ? NULL : desencriptar_con_clave(trim($datos_domicilioEnvio['entreCalle2']), $codigoUsuario_encriptado);

        // DESENCRIPTADO
        $entreCalles_desencriptado = is_null($entreCalle1) || is_null($entreCalle2) ? NULL : $entreCalle1 . ' y ' . $entreCalle2;
        // ENCRIPTADO
        $entreCalles_encriptado = is_null($entreCalle1) || is_null($entreCalle2) ? NULL : encriptar_con_clave($entreCalles_desencriptado, $codigoUsuario_encriptado);

        //////////////////////////////////////////////////////////////////////////////

        // ENCRIPTADO
        $referenciasAdicionales_encriptado = is_null($datos_domicilioEnvio['referenciasAdicionales']) ? NULL : (string) trim($datos_domicilioEnvio['referenciasAdicionales']);
        // DESENCRIPTADO
        $referenciasAdicionales_desencriptado = is_null($datos_domicilioEnvio['referenciasAdicionales']) ? NULL : desencriptar_con_clave($referenciasAdicionales_encriptado, $codigoUsuario_encriptado);

        //////////////////////////////////////////////////////////////////////////////

        $horaEntregaEnvio1 = is_null($datos_domicilioEnvio['horaEntregaEnvio1']) ? NULL : desencriptar_con_clave(trim($datos_domicilioEnvio['horaEntregaEnvio1']), $codigoUsuario_encriptado);

        $horaEntregaEnvio2 = is_null($datos_domicilioEnvio['horaEntregaEnvio2']) ? NULL : desencriptar_con_clave(trim($datos_domicilioEnvio['horaEntregaEnvio2']), $codigoUsuario_encriptado);

        $horarioEntrega = ( is_null($datos_domicilioEnvio['horaEntregaEnvio1']) || is_null($datos_domicilioEnvio['horaEntregaEnvio2']) ) ? 'No está establecido' : (string) 'De ' . hora($horaEntregaEnvio1) . ' a ' . hora($horaEntregaEnvio2);

        // SE ENCRIPTA EL CAMPO
        $horarioEntrega = encriptar_con_clave($horarioEntrega, $codigoUsuario_encriptado);

        // DESENCRIPTADO
        $this->ver_domEnv_tipoVialidad = $tipoVialidad;
        $this->ver_domEnv_nombreVialidad = $nombreVialidad;
        $this->ver_domEnv_noExterior = $noExterior;
        $this->ver_domEnv_noInterior = $noInterior;
        $this->ver_domEnv_codigoPostal = $codigoPostal;
        $this->ver_domEnv_colonia = $colonia;
        $this->ver_domEnv_ciudadMunicipio = $ciudadMunicipio;
        $this->ver_domEnv_nombreEstado = $nombreEstado;
        $this->ver_domEnv_entreCalle1 = $entreCalle1;
        $this->ver_domEnv_entreCalle2 = $entreCalle2;
        $this->ver_domEnv_horaEntregaEnvio1 = $horaEntregaEnvio1;
        $this->ver_domEnv_horaEntregaEnvio2 = $horaEntregaEnvio2;

        // PARA ADMIN EXCLUSIVAMENTE Y ENCRIPTADO
        $this->ver_domEnv_horarioEntrega = $horarioEntrega;

        // ENCRIPTADO Y DESENCRIPTADO
        $this->ver_domEnv_noTelefonico_encriptado = $noTelefonico_encriptado;
        $this->ver_domEnv_noTelefonico_desencriptado = $noTelefonico_desencriptado;
        $this->ver_domEnv_nombreDestinatario_encriptado = $nombreDestinatario_encriptado;
        $this->ver_domEnv_nombreDestinatario_desencriptado = $nombreDestinatario_desencriptado;
        $this->ver_domEnv_entreCalles_encriptado = $entreCalles_encriptado;
        $this->ver_domEnv_entreCalles_desencriptado = $entreCalles_desencriptado;
        $this->ver_domEnv_referenciasAdicionales_encriptado = $referenciasAdicionales_encriptado;
        $this->ver_domEnv_referenciasAdicionales_desencriptado = $referenciasAdicionales_desencriptado;
        $this->ver_domEnv_direccionCompleta_encriptado = $direccionCompleta_encriptado;
        $this->ver_domEnv_direccionCompleta_desencriptado = $direccionCompleta_desencriptado;

        return true;
      }else{
        $this->ver_domEnv_respuesta = '1'; // MENSAJE DE INFO
        $this->ver_domEnv_opcion = '1';
        $this->ver_domEnv_mensaje = 'El domicilio de envío no existe.';
        return false;
      }

      $stmt = null;
    }catch(PDOException $error){
      $this->ver_domEnv_respuesta = '1'; // MENSAJE DE INFO
      $this->ver_domEnv_opcion = '2';
      //$this->ver_domEnv_mensaje = 'Error: ' . $error->getMessage();
      $this->ver_domEnv_mensaje = 'Ocurrió un problema al buscar el domicilio de envío.';
      return false;
    }
  }
}
?>