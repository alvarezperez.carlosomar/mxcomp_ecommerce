<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_ticketCompra{
  private $ordenCompra;
  private $fechaCompra;
  private $metodoPago;
  private $totalPagar;
  private $linkDetalles;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correoDestinatario;
  
  public function __construct($ordenCompra, $fechaCompra, $metodoPago, $totalPagar, $linkDetalles, $correoDestinatario){
    $this->ordenCompra = $ordenCompra;
    $this->fechaCompra = $fechaCompra;
    $this->metodoPago = $metodoPago;
    $this->totalPagar = $totalPagar;
    $this->linkDetalles = $linkDetalles;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correoDestinatario = $correoDestinatario;
  }
  
  public function enviarCorreo(){
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');
    
    if(date('Y') > $this->fechaInicioSitioWEB){
      $this->fechaFooter = $this->fechaInicioSitioWEB . "-" . date('Y');
    }else{
      $this->fechaFooter = $this->fechaInicioSitioWEB;
    }

    $asunto = "Ticket de compra #".$this->ordenCompra;

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 800px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td align="right" style="font-size: 14px;"><b>Compra #'.$this->ordenCompra.', '.$this->fechaCompra.'</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 40px 0px 0px 0px;">¡Gracias por comprar en <b>MXcomp</b>!</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 20px 0px 30px 0px;">
                                Elegiste realizar el pago con <b>'.$this->metodoPago.'</b>.';
    
    if($this->metodoPago === "Depósito/Transferencia"){
      $html_correo .= ' Te pedimos realizarlo lo antes posible dentro de las <b>próximas 24 horas</b>, de lo contrario tu compra será cancelada.';
    }else{
      $html_correo .= ' Y ya se encuentra acreditado.';
    }
    
    $html_correo .= '
                              </td>
                            </tr>

                            <tr>
                              <td align="center" bgcolor="#16A085" style="font-size: 16px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;">';
    
    if($this->metodoPago === "Depósito/Transferencia"){
      $html_correo .= '<b>Sólo te falta pagar</b>';
    }else{
      $html_correo .= '<b>Pagaste</b>';
    }
    
    $html_correo .= '
                              </td>
                            </tr>

                            <tr>
                              <td align="center" bgcolor="#A1D6CC" style="font-size: 16px; padding: 10px 20px 10px 20px; color: #15846F; border-radius: 0px 0px 5px 5px;"><b>$ '.number_format($this->totalPagar, 2, '.', ',').' MXN</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 30px 0px 20px 0px;">
                                Para ver';
    
    if($this->metodoPago === "Depósito/Transferencia"){
      $html_correo .= ' el ticket para pagar y';
    }
    
    $html_correo .= ' los detalles de la compra, da clic al botón de abajo:
                              </td>
                            </tr>

                            <tr>
                              <td align="center" style="padding: 0px 0px 20px 0px;">

                                <table border="0" cellpadding="0" cellspacing="0" bgcolor="#2F6EA3" style="border-radius: 5px; margin: 0 auto;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <a href="'.$this->linkDetalles.'" target="_blank" style="display: block; color: #FFFFFF; text-decoration: none; text-transform: none; padding: 15px 50px 15px 50px; border: 0 solid #2F6EA3; font-size: 14px; font-weight: bold;">Ver detalles de la compra</a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                              </td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 10px 0px 0px 0px;">Si tienes problemas con el botón de arriba, copia y pega la siguiente URL en tu navegador web:</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 10px 0px 10px 0px;"><b>'.$this->linkDetalles.'</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 20px 0px 0px 0px;"><b>Gracias</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 0px 0px 0px 0px;"><b>Atte. Equipo de MXcomp</b></td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © '.$this->fechaFooter.' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>';

    try{
      // Configuración del servidor
      $mail->SMTPDebug = 0;                               // Habilitar la salida de depuración detallada
      $mail->isSMTP();                                    // Enviar usando SMTP
      $mail->Host       = 'smtp.ionos.mx';                // Configure el servidor SMTP para enviar
      $mail->SMTPAuth   = true;                           // Habilitar autenticación SMTP
      $mail->Username   = 'no-responder@mxcomp.com.mx';   // SMTP nombre de usuario (correo)
      $mail->Password   = '$N0_r3sp0nd3r_MXc0mp';         // SMTP contraseña
      $mail->SMTPSecure = 'tls';                          // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port       = 587;                            // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('no-responder@mxcomp.com.mx', 'MXcomp');
      $mail->addAddress($this->correoDestinatario);             // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');  
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true);             // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body    = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();
      
      return true;
    }catch(Exception $e){
      //$mensaje = "No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}";
      return false;
    }
  }
}
?>