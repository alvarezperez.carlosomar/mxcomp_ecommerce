<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_enviar_datosFacturacion{
  private $ordenCompra;
  private $fechaProceso;
  private $nombreCliente;
  private $codigoCliente;
  private $razonSocial;
  private $RFC;
  private $usoFactura;
  private $formaPago;
  private $direccionCliente;
  private $correoCliente;
  private $noTelefonicoCliente;
  private $array_productosFactura;
  private $array_costoEnvio;
  private $array_granTotal;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correoDestinatario;

  public function __construct($ordenCompra, $fechaProceso, $nombreCliente, $codigoCliente, $razonSocial, $RFC, $usoFactura, $formaPago, $direccionCliente, $correoCliente, $noTelefonicoCliente, $array_productosFactura, $array_costoEnvio, $array_granTotal){
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();
  
    $this->ordenCompra = $ordenCompra;
    $this->fechaProceso = $fechaProceso;
    $this->nombreCliente = $nombreCliente;
    $this->codigoCliente = $codigoCliente;
    $this->razonSocial = $razonSocial;
    $this->RFC = $RFC;
    $this->usoFactura = $usoFactura;
    $this->formaPago = $formaPago;
    $this->direccionCliente = $direccionCliente;
    $this->correoCliente = $correoCliente;
    $this->noTelefonicoCliente = $noTelefonicoCliente;
    $this->array_productosFactura = $array_productosFactura;
    $this->array_costoEnvio = $array_costoEnvio;
    $this->array_granTotal = $array_granTotal;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correoDestinatario = $_ENV['CORREO_INTERNO'];
  }

  public function enviarCorreo(){
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');

    $this->fechaFooter = date('Y') > $this->fechaInicioSitioWEB ? $this->fechaInicioSitioWEB . '-' . date('Y') : $this->fechaInicioSitioWEB;

    $asunto = 'Cliente solicitó factura';

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 1000px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td align="right" style="font-size: 14px;"><b>Venta #' . $this->ordenCompra . ', ' . $this->fechaProceso . '</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 35px 0px 20px 0px;">El Cliente <b>' . $this->nombreCliente . ' (' . $this->codigoCliente . ')</b> acaba de solicitar se le facture su compra, los datos son los siguientes:</td>
                            </tr>

                            <tr>
                              <td align="left" style="padding: 0px 0px 20px 0px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="max-width: 800px; border-radius: 5px 5px 5px 5px;">
                                  <tbody>
                                    <tr>
                                      <td align="left" bgcolor="#38766A" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;"><b>Datos de facturación</b></td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Nombre o razón social:</b> ' . $this->razonSocial . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>RFC:</b> ' . $this->RFC . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Uso de la factura:</b> ' . $this->usoFactura . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Forma de pago:</b> ' . $this->formaPago . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Dirección del cliente:</b> ' . $this->direccionCliente . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Correo para mandar la factura:</b> ' . $this->correoCliente . '</td>
                                    </tr>
                                    <tr>
                                      <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 15px 20px; color: #424242; border-radius: 0px 0px 5px 5px;"><b>No. telefónico:</b> ' . $this->noTelefonicoCliente . '</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>

                            <tr>
                              <td style="padding: 10px 0px 10px 0px;"></td>
                            </tr>

                            <tr>
                              <td align="center" style="padding: 0px 0px 20px 0px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; font-size: 12px;">
                                  <tbody>
                                    <tr bgcolor="#16A085" style="color: #FFFFFF;">
                                      <th style="padding: 5px;">ITEM</th>
                                      <th style="padding: 5px;">CÓDIGO PRODUCTO</th>
                                      <th style="padding: 5px;">DESCRIPCIÓN</th>
                                      <th style="padding: 5px;">CANT.</th>
                                      <th style="padding: 5px;">VALOR UNI.</th>
                                      <th style="padding: 5px;">IMPORTE</th>
                                      <th style="padding: 5px;">IVA</th>
                                      <th style="padding: 5px;">TOTAL</th>
                                    </tr>';

    $contador = 1;
    $fila = '1';
    foreach($this->array_productosFactura as $datos){
      $codigoProducto = $datos['codigoProducto'];
      $descripcion = $datos['descripcion'];
      $cantidad = $datos['cantidad'];
      $valor_unitario = $datos['valor_unitario'];
      $importe = $datos['importe'];
      $IVA = $datos['IVA'];
      $total = $datos['total'];

      if($fila === '1'){
        $html_correo .= '
                                    <tr bgcolor="#C8C8C8">';
        $fila = '2';
      }else{
        $html_correo .= '
                                    <tr bgcolor="#B0B0B0">';
        $fila = '1';
      }

      $html_correo .= '
                                      <td style="padding: 5px;" align="center">' . $contador . '</td>
                                      <td style="padding: 5px;" align="center">' . $codigoProducto . '</td>
                                      <td style="padding: 5px;">' . $descripcion . '</td>
                                      <td style="padding: 5px;" align="center">' . $cantidad . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($valor_unitario, 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($importe, 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($IVA, 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($total, 2) . '</td>
                                    </tr>';

      $contador++;
    }

    if($fila === '1'){
      $html_correo .= '
                                    <tr bgcolor="#C8C8C8">';
      $fila = '2';
    }else{
      $html_correo .= '
                                    <tr bgcolor="#B0B0B0">';
      $fila = '1';
    }

    $html_correo .= '
                                      <td style="padding: 5px;" align="center">' . $contador . '</td>
                                      <td style="padding: 5px;" align="center">' . $this->array_costoEnvio['codigoProducto'] . '</td>
                                      <td style="padding: 5px;">' . $this->array_costoEnvio['descripcion'] . '</td>
                                      <td style="padding: 5px;" align="center">' . $this->array_costoEnvio['cantidad'] . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($this->array_costoEnvio['valor_unitario'], 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($this->array_costoEnvio['importe'], 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($this->array_costoEnvio['IVA'], 2) . '</td>
                                      <td style="padding: 5px;" align="right">$' . number_format($this->array_costoEnvio['total'], 2) . '</td>
                                    </tr>

                                    <tr bgcolor="#7b7b7b" style="color: #FFFFFF;">
                                      <td style="padding: 5px;"></td>
                                      <td style="padding: 5px;"></td>
                                      <td style="padding: 5px;" align="right"><b>GRAN TOTAL</b></td>
                                      <td style="padding: 5px;" align="center"><b>' . $this->array_granTotal['total_cantidad'] . '</b></td>
                                      <td style="padding: 5px;" align="right"><b>$' . number_format($this->array_granTotal['total_valorUnitario'], 2) . '</b></td>
                                      <td style="padding: 5px;" align="right"><b>$' . number_format($this->array_granTotal['total_importe'], 2) . '</b></td>
                                      <td style="padding: 5px;" align="right"><b>$' . number_format($this->array_granTotal['total_IVA'], 2) . '</b></td>
                                      <td style="padding: 5px;" align="right"><b>$' . number_format($this->array_granTotal['gran_total'], 2) . '</b></td>
                                    </tr>';

    $html_correo .= '
                                  </tbody>
                                </table>
                              </td>
                            </tr>

                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © ' . $this->fechaFooter . ' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>

      </body>
    </html>';

    try{
      // Configuración del servidor
      $mail->SMTPDebug = 0;                               // Habilitar la salida de depuración detallada
      $mail->isSMTP();                                    // Enviar usando SMTP
      $mail->Host       = 'smtp.ionos.mx';                // Configure el servidor SMTP para enviar
      $mail->SMTPAuth   = true;                           // Habilitar autenticación SMTP
      $mail->Username   = 'sitio-web@mxcomp.com.mx';   // SMTP nombre de usuario (correo)
      $mail->Password   = '$S1t10_W3b_MXc0mp';         // SMTP contraseña
      $mail->SMTPSecure = 'tls';                          // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port       = 587;                            // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('sitio-web@mxcomp.com.mx', 'Sitio web MXcomp');
      $mail->addAddress($this->correoDestinatario);             // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');  
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true);             // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body    = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();

      return true;
    }catch(Exception $e){
      //$mensaje = 'No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}';
      return false;
    }
  }
}
?>