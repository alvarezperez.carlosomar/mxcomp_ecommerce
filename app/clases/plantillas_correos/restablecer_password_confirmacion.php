<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_restablecerPassword_confirmacion{
  private $nombreS;
  private $linkAtencionClientes;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correosDestinatario;
  
  public function __construct($nombreS, $linkAtencionClientes, $correosDestinatario){
    $this->nombreS = $nombreS;
    $this->linkAtencionClientes = $linkAtencionClientes;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correosDestinatario = $correosDestinatario;
  }
  
  public function enviarCorreo(){
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');
    
    if(date('Y') > $this->fechaInicioSitioWEB){
      $this->fechaFooter = $this->fechaInicioSitioWEB . "-" . date('Y');
    }else{
      $this->fechaFooter = $this->fechaInicioSitioWEB;
    }

    $asunto = "Tu contraseña a sido cambiada";

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 800px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-size: 14px; padding: 0px 0px 0px 0px;">Hola <b>'.$this->nombreS.'</b>:</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 20px 0px 0px 0px;">Esta es una confirmación de que la contraseña de tu cuenta en <b>MXcomp</b> acaba de ser cambiada.</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 20px 0px 0px 0px;">Si no solicitaste cambiar tu contraseña, ponte en contacto con <a href="'.$this->linkAtencionClientes.'" target="_blank" style="font-weight: bold;">atención a clientes</a>.</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 20px 0px 0px 0px;"><b>Gracias</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 0px 0px 0px 0px;"><b>Atte. Equipo de MXcomp</b></td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © '.$this->fechaFooter.' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>
      </body>
    </html>';

    try{
      // Configuración del servidor
      $mail->SMTPDebug = 0;                               // Habilitar la salida de depuración detallada
      $mail->isSMTP();                                    // Enviar usando SMTP
      $mail->Host       = 'smtp.ionos.mx';                // Configure el servidor SMTP para enviar
      $mail->SMTPAuth   = true;                           // Habilitar autenticación SMTP
      $mail->Username   = 'no-responder@mxcomp.com.mx';   // SMTP nombre de usuario (correo)
      $mail->Password   = '$N0_r3sp0nd3r_MXc0mp';         // SMTP contraseña
      $mail->SMTPSecure = 'tls';                          // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port       = 587;                            // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('no-responder@mxcomp.com.mx', 'MXcomp');
      $mail->addAddress($this->correosDestinatario);             // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');  
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true);             // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body    = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();
      
      return true;
    }catch(Exception $e){
      //$mensaje = "No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}";
      return false;
    }
  }
}
?>