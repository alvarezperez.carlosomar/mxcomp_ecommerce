<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_remisionesPCH{
  private $ordenCompra;
  private $direccionCompleta;
  private $nomDestinatario;
  private $telefono;
  private $entreCalles;
  private $referenciasAdicionales;
  private $metodoEnvio;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correoDestinatario_1;
  private $correoDestinatario_2;
  private $correoDestinatario_3;
  private $arrayAlmacenRemision;
  private $arrayCargoEnvio;
  
  public function __construct($ordenCompra, $direccionCompleta, $nomDestinatario, $telefono, $entreCalles, $referenciasAdicionales, $metodoEnvio, $costoEnvio_total, $arrayAlmacenRemision, $arrayCargoEnvio){
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();
    
    $this->ordenCompra = $ordenCompra;
    $this->direccionCompleta = $direccionCompleta;
    $this->nomDestinatario = $nomDestinatario;
    $this->telefono = $telefono;
    $this->entreCalles = $entreCalles;
    $this->referenciasAdicionales = $referenciasAdicionales;
    $this->metodoEnvio = $metodoEnvio;
    $this->costoEnvio_total = $costoEnvio_total;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correoDestinatario_1 = $_ENV['CORREOS_PCH_1'];
    $this->correoDestinatario_2 = $_ENV['CORREOS_PCH_2'];
    $this->correoDestinatario_3 = $_ENV['CORREOS_PCH_3'];
    $this->arrayAlmacenRemision = $arrayAlmacenRemision;
    $this->arrayCargoEnvio = $arrayCargoEnvio;
  }
  
  public function enviarCorreo(){
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');
    
    if(date('Y') > $this->fechaInicioSitioWEB){
      $this->fechaFooter = $this->fechaInicioSitioWEB . "-" . date('Y');
    }else{
      $this->fechaFooter = $this->fechaInicioSitioWEB;
    }

    $asunto = "Remisiones MXcomp";

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 800px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-size: 14px;">Hola buen día, solicitamos que se modifique el precio para las siguientes remisiones de la venta #'.$this->ordenCompra.', las que pertenezcan a una misma sucursal, consolidarlas y pasarlas a moneda MXN, por favor.</td>
                            </tr>';

    // REMISIONES DE LOS ALMACENES
    if(count($this->arrayAlmacenRemision) !== 0){
      foreach($this->arrayAlmacenRemision as $indice=>$datos){
        $almacen = $datos['almacen'];
        $moneda = $datos['moneda'];
        $remision = $datos['remision'];
        
        $html_correo .= '
                            <tr>
                              <td style="padding: 10px 0px 10px 0px;"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;"><b>Remisión '.$almacen.' ('.$moneda.')</b></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 0px 5px 5px;"><b>'.$remision.'</b></td>
                            </tr>';
      }
    }

    $html_correo .= '
                            <tr>
                              <td style="font-size: 14px; padding: 30px 0px 15px 0px;">Los datos a donde debe llegar el pedido una vez se facture las remisiones, son los siguientes:</td>
                            </tr>

                            <tr>
                              <td align="left" bgcolor="#38766A" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;"><b>Datos de envío</b></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 5px 20px; color: #424242; border-radius: 0px 0px 0px 0px;">'.$this->direccionCompleta.'</td>
                            </tr>';

    if(!is_null($this->entreCalles)){
      $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 5px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Entre calles:</b> '.$this->entreCalles.'</td>
                            </tr>';
    }

    if(!is_null($this->referenciasAdicionales)){
      $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 5px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Referencias adicionales:</b> '.$this->referenciasAdicionales.'</td>
                            </tr>';
    }

    $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 5px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>Quién recibe:</b> '.$this->nomDestinatario.'</td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 10px 20px; color: #424242; border-radius: 0px 0px 5px 5px;"><b>Tel.:</b> '.$this->telefono.'</td>
                            </tr>

                            <tr>
                              <td style="padding: 10px 0px 10px 0px;"></td>
                            </tr>';

    if($this->metodoEnvio === "Paqueteria"){
      if($this->costoEnvio_total === "Envío gratis"){
        $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#38766A" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 5px 5px;"><b>No aplica cargo por envío</b></td>
                            </tr>';
      }else{
        $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#38766A" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;"><b>Aplica cargo por envío</b></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 5px 20px; color: #FFFFFF; border-radius: 0px 0px 0px 0px;"></td>
                            </tr>';
        
        if(count($this->arrayCargoEnvio) !== 0){
          foreach($this->arrayCargoEnvio as $indice=>$datos){
            $almacen = $datos['almacen'];
            $costo = $datos['costo'];

            $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 5px 20px 5px 20px; color: #424242; border-radius: 0px 0px 0px 0px;"><b>'.$almacen.':</b> $ '.$costo.' MN antes de IVA (Envío estándar)</td>
                            </tr>';
          }
          
          $html_correo .= '
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 0px 20px 5px 20px; border-radius: 0px 0px 5px 5px;"></td>
                            </tr>';
        }
        
      }
    }

    $html_correo .= '
                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © '.$this->fechaFooter.' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>

      </body>
    </html>';

    try{
      // Configuración del servidor
      $mail->SMTPDebug = 0;                          // Habilitar la salida de depuración detallada
      $mail->isSMTP();                               // Enviar usando SMTP
      $mail->Host       = 'smtp.ionos.mx';           // Configure el servidor SMTP para enviar
      $mail->SMTPAuth   = true;                      // Habilitar autenticación SMTP
      $mail->Username   = 'sitio-web@mxcomp.com.mx';   // SMTP nombre de usuario (correo)
      $mail->Password   = '$S1t10_W3b_MXc0mp';        // SMTP contraseña
      $mail->SMTPSecure = 'tls';                     // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port       = 587;                       // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('sitio-web@mxcomp.com.mx', 'Sitio web MXcomp');
      $mail->addAddress($this->correoDestinatario_1);             // Agregar un destinatario
      $mail->addAddress($this->correoDestinatario_2);             // Agregar un destinatario
      $mail->addAddress($this->correoDestinatario_3);             // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');  
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true);             // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body    = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();
      
      return true;
    }catch(Exception $e){
      //$mensaje = "No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}";
      return false;
    }
  }
}
?>