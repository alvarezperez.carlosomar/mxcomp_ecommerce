<?php
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

require dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_domicilioEnvio_PCH {
  private $ordenCompra;
  private $calleNumero;
  private $codigoPostal;
  private $colonia;
  private $ciudadMunicipio;
  private $estado;
  private $nomDestinatario;
  private $telefono;
  private $entreCalle1;
  private $entreCalle2;
  private $horaEntregaEnvio_1;
  private $horaEntregaEnvio_2;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correoDestinatario;

  public function __construct($ordenCompra, $calleNumero, $codigoPostal, $colonia, $ciudadMunicipio, $estado, $nomDestinatario, $telefono, $entreCalle1, $entreCalle2, $horaEntregaEnvio_1, $horaEntregaEnvio_2) {
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();

    $this->ordenCompra = $ordenCompra;
    $this->calleNumero = $calleNumero;
    $this->codigoPostal = $codigoPostal;
    $this->colonia = $colonia;
    $this->ciudadMunicipio = $ciudadMunicipio;
    $this->estado = $estado;
    $this->nomDestinatario = $nomDestinatario;
    $this->telefono = $telefono;
    $this->entreCalle1 = $entreCalle1;
    $this->entreCalle2 = $entreCalle2;
    $this->horaEntregaEnvio_1 = $horaEntregaEnvio_1;
    $this->horaEntregaEnvio_2 = $horaEntregaEnvio_2;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correoDestinatario = $_ENV['CORREO_PCH_DOMICILIO'];
  }

  public function enviarCorreo() {
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');

    if (date('Y') > $this->fechaInicioSitioWEB) {
      $this->fechaFooter = $this->fechaInicioSitioWEB . "-" . date('Y');
    } else {
      $this->fechaFooter = $this->fechaInicioSitioWEB;
    }

    $asunto = "Domicilio cliente, MXcomp";

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 800px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td style="font-size: 14px;" colspan="2">Hola buen día, solicitamos que se registre el siguiente domicilio del cliente, que pertenece a nuestra venta #' . $this->ordenCompra . '.</td>
                            </tr>

                            <tr>
                              <td style="padding: 15px 0px 15px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Nombre ubicación</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>Usuario final</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Calle y número</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->calleNumero . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Código postal</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->codigoPostal . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Ciudad</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->ciudadMunicipio . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>País</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>México</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Deleg/Municipio</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->ciudadMunicipio . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Estado</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->estado . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Colonia</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->colonia . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Entre</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->entreCalle1 . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Y</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->entreCalle2 . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Hor. Oficina</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->horaEntregaEnvio_1 . ' - ' . $this->horaEntregaEnvio_2 . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Hor. Comida</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->horaEntregaEnvio_1 . ' - ' . $this->horaEntregaEnvio_2 . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Recibe</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->nomDestinatario . '</b></td>
                            </tr>

                            <tr>
                              <td style="padding: 5px 0px 5px 0px;" colspan="2"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 0px 0px 5px;"><b>Teléfonos</b></td>

                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 5px 5px 0px;"><b>' . $this->telefono . '</b></td>
                            </tr>

                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © ' . $this->fechaFooter . ' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>

      </body>
    </html>';

    try {
      // Configuración del servidor
      $mail->SMTPDebug = 0; // Habilitar la salida de depuración detallada
      $mail->isSMTP(); // Enviar usando SMTP
      $mail->Host = 'smtp.ionos.mx'; // Configure el servidor SMTP para enviar
      $mail->SMTPAuth = true; // Habilitar autenticación SMTP
      $mail->Username = 'sitio-web@mxcomp.com.mx'; // SMTP nombre de usuario (correo)
      $mail->Password = '$S1t10_W3b_MXc0mp'; // SMTP contraseña
      $mail->SMTPSecure = 'tls'; // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port = 587; // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('sitio-web@mxcomp.com.mx', 'Sitio web MXcomp');
      $mail->addAddress($this->correoDestinatario); // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true); // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();

      return true;
    } catch (Exception $e) {
      //$mensaje = "No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}";
      return false;
    }
  }
}
?>