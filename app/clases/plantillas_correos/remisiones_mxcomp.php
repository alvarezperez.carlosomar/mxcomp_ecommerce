<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require dirname(__DIR__, 3) . '/vendor/autoload.php';
require_once dirname(__DIR__, 2) . '/global/config.php';

date_default_timezone_set('America/Mexico_City'); // SE ESTABLECE LA HORA DE MÉXICO

class Correo_remisionesMXcomp{
  private $ordenCompra;
  private $fechaCompra;
  private $metodoPago;
  private $totalPagar;
  private $linkDetalles;
  private $fechaInicioSitioWEB;
  private $fechaFooter;
  private $correoDestinatario;
  private $arrayAlmacenRemision;
  
  public function __construct($ordenCompra, $fechaCompra, $metodoPago, $totalPagar, $linkDetalles, $arrayAlmacenRemision){
    $dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__, 3));
    $dotenv->load();
    
    $this->ordenCompra = $ordenCompra;
    $this->fechaCompra = $fechaCompra;
    $this->metodoPago = $metodoPago;
    $this->totalPagar = $totalPagar;
    $this->linkDetalles = $linkDetalles;
    $this->fechaInicioSitioWEB = FECHA_INICIO_SITIO_WEB;
    $this->correoDestinatario = $_ENV['CORREO_INTERNO'];
    $this->arrayAlmacenRemision = $arrayAlmacenRemision;
  }
  
  public function enviarCorreo(){
    // True para habilitar excepciones
    $mail = new PHPMailer(true);
    $mail->setLanguage('es', dirname(__DIR__, 3) . '/vendor/phpmailer/phpmailer/language/');
    
    if(date('Y') > $this->fechaInicioSitioWEB){
      $this->fechaFooter = $this->fechaInicioSitioWEB . '-' . date('Y');
    }else{
      $this->fechaFooter = $this->fechaInicioSitioWEB;
    }

    $asunto = 'Remisiones de la venta #' . $this->ordenCompra;

    $html_correo = '
    <html>
      <head>
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      </head>
      <body style="margin: 0; padding: 20px 10px 20px 10px; font-family: Montserrat, sans-serif; background-color: #F2F2F2; font-size: 14px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" valign="top">

                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width: 800px;">
                  <tbody>
                    <tr>
                      <td bgcolor="#E4E4E4" style="padding: 40px 30px 40px 30px;">

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tbody>
                            <tr>
                              <td align="right" style="font-size: 14px;"><b>Venta #' . $this->ordenCompra . ', ' . $this->fechaCompra . '</b></td>
                            </tr>';
    
    if($this->metodoPago === 'Depósito/Transferencia'){
      $html_correo .= '
                            <tr>
                              <td style="font-size: 14px; padding: 40px 0px 15px 0px;">Se abaca de realizar una venta en la cual el cliente pagará por <b>' . $this->metodoPago . '</b> la cantidad de:</td>
                            </tr>';
    }else{
      $html_correo .= '
                            <tr>
                              <td style="font-size: 14px; padding: 40px 0px 15px 0px;">Se abaca de realizar una venta en la cual el cliente pagó con <b>' . $this->metodoPago . '</b> la cantidad de:</td>
                            </tr>';
    }

    $html_correo .= '
                            <tr>
                              <td align="center" bgcolor="#4CAF50" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 5px 5px;"><b>$ ' . number_format($this->totalPagar, 2, '.', ',') . ' MXN</b></td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 40px 0px 0px 0px;">Las remisiones que se generaron son las siguientes:</td>
                            </tr>';

    // REMISIONES DE LOS ALMACENES
    if(count($this->arrayAlmacenRemision) !== 0){
      foreach($this->arrayAlmacenRemision as $indice=>$datos){
        $almacen = $datos['nombreAlmacen'];
        $moneda = $datos['monedaRemision'];
        $remision = $datos['remision'];
        
        $html_correo .= '
                            <tr>
                              <td style="padding: 10px 0px 10px 0px;"></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#16A085" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #FFFFFF; border-radius: 5px 5px 0px 0px;"><b>Remisión ' . ucwords(mb_strtolower($almacen)) . ' (' . $moneda . ')</b></td>
                            </tr>
                            <tr>
                              <td align="left" bgcolor="#C5CBCA" style="font-size: 14px; padding: 10px 20px 10px 20px; color: #424242; border-radius: 0px 0px 5px 5px;"><b>' . $remision . '</b></td>
                            </tr>';
      }
    }

    $html_correo .= '
                            <tr>
                              <td style="font-size: 14px; padding: 30px 0px 20px 0px;">Para ver los detalles de la venta, da clic al botón de abajo:</td>
                            </tr>

                            <tr>
                              <td align="center" style="padding: 0px 0px 20px 0px;">

                                <table border="0" cellpadding="0" cellspacing="0" bgcolor="#2F6EA3" style="border-radius: 5px; margin: 0 auto;">
                                  <tbody>
                                    <tr>
                                      <td align="center">
                                        <a href="' . $this->linkDetalles . '" target="_blank" style="display: block; color: #FFFFFF; text-decoration: none; text-transform: none; padding: 15px 50px 15px 50px; border: 0 solid #2F6EA3; font-size: 14px; font-weight: bold;">Ver detalles de la venta</a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                              </td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 10px 0px 0px 0px;">Si tienes problemas con el botón de arriba, copia y pega la siguiente URL en tu navegador web:</td>
                            </tr>

                            <tr>
                              <td style="font-size: 14px; padding: 10px 0px 10px 0px;"><b>' . $this->linkDetalles . '</b></td>
                            </tr>
                          </tbody>
                        </table>

                      </td>
                    </tr>
                    <tr>
                      <td align="center" bgcolor="#525252" style="padding: 30px 20px 60px 20px; color: #FFF; font-size: 11px;">Copyright © ' . $this->fechaFooter . ' MXcomp. Todos los derechos reservados.</td>
                    </tr>
                  </tbody>
                </table>

              </td>
            </tr>
          </tbody>
        </table>

      </body>
    </html>';

    try{
      // Configuración del servidor
      $mail->SMTPDebug = 0;                               // Habilitar la salida de depuración detallada
      $mail->isSMTP();                                    // Enviar usando SMTP
      $mail->Host       = 'smtp.ionos.mx';                // Configure el servidor SMTP para enviar
      $mail->SMTPAuth   = true;                           // Habilitar autenticación SMTP
      $mail->Username   = 'sitio-web@mxcomp.com.mx';   // SMTP nombre de usuario (correo)
      $mail->Password   = '$S1t10_W3b_MXc0mp';         // SMTP contraseña
      $mail->SMTPSecure = 'tls';                          // Habilite el cifrado TLS; Se recomienda `PHPMailer::ENCRYPTION_SMTPS`
      $mail->Port       = 587;                            // Puerto TCP para conectarse, use 465 para `PHPMailer::ENCRYPTION_SMTPS` arriba

      // Destinatarios
      $mail->setFrom('sitio-web@mxcomp.com.mx', 'Sitio web MXcomp');
      $mail->addAddress($this->correoDestinatario);             // Agregar un destinatario
      //$mail->addReplyTo('no-responder@mxcomp.com.mx');  
      //$mail->addCC('cc@example.com');                    // Agregar destinatario para copia
      //$mail->addBCC('bcc@example.com');                  // Agregar destinatario para copia oculta

      // Archivos adjuntos
      //$mail->addAttachment('/var/tmp/file.tar.gz');        // Agregar archivos adjuntos
      //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Nombre opcional

      // Contenido
      $mail->isHTML(true);             // Establecer formato de correo electrónico en HTML
      $mail->Subject = $asunto;
      $mail->Body    = $html_correo;
      $mail->CharSet = 'UTF-8';

      $mail->send();
      
      return true;
    }catch(Exception $e){
      //$mensaje = "No se pudo enviar el mensaje. Error de envío: {$mail->ErrorInfo}";
      return false;
    }
  }
}
?>