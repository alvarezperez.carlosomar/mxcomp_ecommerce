<?php
if(isset($_GET['email']) && isset($_GET['token'])){
  session_start();
  if(isset($_SESSION['__id__'])){
    echo '<script> window.location = "/"; </script>';
  }

  require_once 'funciones/fecha_hora_formatos.php';
  require_once 'funciones/encriptacion.php';
  require_once 'global/config.php'; // NECESARIO PARA HEAD
  require_once 'conn.php';

  $Conn_mxcomp = new Conexion_mxcomp();
  
  date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO
  
  $correo = trim($_GET['email']);
  $token = trim($_GET['token']);
  $error = false;
  $mensaje_error = "";
  $registro_no_existe = false;
  $fechaActivacion_expirada = false;
  $cuentaEliminada = false;
  $fechaEliminacionInfo = "";
  
  try{
    // SE ENCRIPTA EL CORREO CON LA CLAVE DEFAULT
    $correo_consulta = encriptar($correo);
    
    $sql = "SELECT COUNT(id) AS conteo, id, fechaEsperaProceso, fechaEliminacionInfo FROM __usuarios WHERE correo = :correo AND token = :token";
    $stmt = $Conn_mxcomp->pdo->prepare($sql);
    $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
    $stmt->bindParam(':token', $token, PDO::PARAM_STR);
    $stmt->execute();
    $datos_cuenta = $stmt->fetch(PDO::FETCH_ASSOC);
    $usuario_existe = (int) $datos_cuenta['conteo'];
    $idUsuario = (int) $datos_cuenta['id'];
    $fechaEspera = (string) $datos_cuenta['fechaEsperaProceso'];
    $fechaEliminacionInfo = (string) $datos_cuenta['fechaEliminacionInfo'];
    $fechaActual = date("Y-m-d H:i:s");
    
    if($usuario_existe === 1){
      if($fechaActual >= $fechaEliminacionInfo){
        try{
          $Conn_mxcomp->pdo->beginTransaction();
          
          $sql = "DELETE FROM __direcciones WHERE BINARY idUsuario = :idUsuario";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':idUsuario', $idUsuario, PDO::PARAM_INT);
          $stmt->execute();

          $sql = "DELETE FROM __usuarios WHERE BINARY id = :id AND correo = :correo AND token = :token";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
          $stmt->bindParam(':token', $token, PDO::PARAM_STR);
          $stmt->execute();
          
          $Conn_mxcomp->pdo->commit();
          
          $cuentaEliminada = true;
        }catch(PDOException $e){
          $Conn_mxcomp->pdo->rollBack();
          $error = true;
          //$mensaje_error = "Error: " . $e->getMessage();
          $mensaje_error = "Problema al tratar de eliminar la información de la cuenta.";
        }
      }else{
        if($fechaActual > $fechaEspera){
          $fechaActivacion_expirada = true;
        }else{
          $sql = "UPDATE __usuarios SET cuentaActiva = 1, token = NULL, fechaEsperaProceso = NULL, fechaEliminacionInfo = NULL, fechaActualizacion = :fechaActualizacion WHERE BINARY id = :id AND correo = :correo AND token = :token";
          $stmt = $Conn_mxcomp->pdo->prepare($sql);
          $stmt->bindParam(':fechaActualizacion', $fechaActual, PDO::PARAM_STR);
          $stmt->bindParam(':id', $idUsuario, PDO::PARAM_INT);
          $stmt->bindParam(':correo', $correo_consulta, PDO::PARAM_STR);
          $stmt->bindParam(':token', $token, PDO::PARAM_STR);
          $stmt->execute();
        }
      }
    }else{
      $registro_no_existe = true;
    }
    $stmt = null;
  }catch(PDOException $e){
    $error = true;
    //$mensaje_error = "Error: " . $e->getMessage();
    $mensaje_error = "Problema al buscar la cuenta.";
  }
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<?php
  $title = $error ? 'Ocurrió un error' : ( $registro_no_existe ? 'Se activó tu cuenta / Cuenta o token no existen' : ( $cuentaEliminada ? 'Cuenta eliminada' : ( $fechaActivacion_expirada ? 'Tiempo expirado para activación' : '¡Cuenta activada exitosamente!' ) ) );
?>
    <title><?php echo $title; ?></title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
  $navbar = "1";
  $op_navbar = 0;
  
  include 'templates/navbar_formularios.php';
?>

    <section class="p-section-columns">
      <div class="p-contenido-contenedor_formularios">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
<?php
  $titulo_h1 = $error ? 'OCURRIÓ UN ERROR' : ( $registro_no_existe ? 'SE ACTIVÓ TU CUENTA / CUENTA O TOKEN NO EXISTEN' : ( $cuentaEliminada ? 'CUENTA ELIMINADA' : ( $fechaActivacion_expirada ? 'TIEMPO EXPIRADO PARA ACTIVACIÓN' : '¡CUENTA ACTIVADA EXITOSAMENTE!' ) ) );
?>
          <h1 class="p-titulo"><?php echo $titulo_h1; ?></h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-section-div_formularios_contenedor">
<?php
  if($error){
?>
            <h3 class="p-text_p">Ha ocurrido un error, vuelve a dar clic en el botón o enlace que se envió a tu correo.</h3>
            <p class="p-text_p">En el caso de que este aviso siga apareciendo, ponte en contacto con <strong>atención a clientes</strong>, en la pestaña del menú izquierdo.</p>
<?php
  }else if($registro_no_existe){
?>
            <h3 class="p-text_p">El token o la cuenta ya no existen o ya se activó anteriormente.</h3>
            <p class="p-text_p">En el caso de ya haberla activado, <strong>inicia sesión</strong> para ver los productos que tengas agregados en tu carrito y/o para realizar tu primer compra.</p>
            
            <div class="p-buttons p-buttons_right p-cuenta-contenedor_botones p-margin-top_1rem">
              <a href="iniciar-sesion" class="p-button p-button_info">
                <span>
                  <i class="fas fa-user"></i>
                </span>
                <span><b>Iniciar sesión</b></span>
              </a>
            </div>
<?php
  }else if($cuentaEliminada){
?>
            <h3 class="p-text_p">La cuenta fue eliminada porque no se activó a tiempo. Este proceso se realizó para evitar tener cuentas de usuarios falsos.</h3>
            <p class="p-text_p">Si no es tu caso, te pedimos que te vuelvas a registrar y en el momento dado, actives tu cuenta a través del enlace que se te enviará por correo.</p>
            
            <div class="p-buttons p-buttons_right p-cuenta-contenedor_botones p-margin-top_1rem">
              <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
                <span>
                  <i class="fas fa-user-plus"></i>
                </span>
                <span><b>Registrar cuenta</b></span>
              </a>
            </div>
<?php
  }else if($fechaActivacion_expirada){
?>
            <div class="p-notification_contenedor g-notification-correo_success">
              <div class="p-notification p-notification_letter_success">
                <span>
                  <i class="fas fa-check-circle"></i>
                </span>
                <p class="p-notification_p">
                  <span>Se envió un correo electrónico a <b><?php echo $correo; ?></b>.</span>
                  <span>Tienes <b>24 HORAS</b> para activarla. Si no encuentras el correo en tu bandeja de entrada, revisa en la carpeta de Spam.</span>
                  <span>Si no llega, espera unos minutos o vuelve a realizar el proceso. Si el problema sigue apareciendo, manda mensaje a <b>atencion a clientes</b>.</span>
                </p>
              </div>
            </div>
            
            <div class="p-notification_contenedor g-notification-correo_error">
              <div class="p-notification p-notification_letter_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <p class="p-notification_p">
                  <span>No se envio el correo, revisa tu conexión a internet o recarga la página para volver a enviar el correo.</span>
                </p>
              </div>
            </div>
            
            <div class="p-loading-general g-activarCuenta-loading">
              <div></div>
            </div>
            
            <div class="g-activarCuenta-contenedor_informacion">
              <p class="p-text_p">El tiempo establecido para activar tu cuenta en <strong>MXcomp</strong> ha expirado. Aún puedes activarla antes del <b><?php echo fecha_con_hora($fechaEliminacionInfo); ?></b>, después de esa fecha será eliminada.</p>
              <p class="p-text p-text_help p-text_p">Escribe el correo que registraste y revisa tu bandeja de entrada o de spam, para ver el enlace de activación.</p>
              
              <form class="g-activarCuenta-form">
                <div class="p-field p-field_marginBottom">
                  <label class="p-label">
                    <span>Correo electrónico:</span>
                  </label>
                  <div class="p-control">
                    <input type="text" class="p-input g-activarCuenta-correo_input" placeholder="Correo electrónico" autocomplete="off" autofocus="">
                  </div>
                  <p class="p-text p-text_info g-activarCuenta-correo_alert_info">
                    <span>
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span>El correo no cumple con el formato estándar: ejemplo@ejemplo.com</span>
                  </p>
                  <p class="p-text p-text_error g-activarCuenta-correo_alert_error_1">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este correo no se encuentra registrado, revísalo por favor.</span>
                  </p>
                  <p class="p-text p-text_error g-activarCuenta-correo_alert_error_2">
                    <span>
                      <i class="fas fa-times-circle"></i>
                    </span>
                    <span>Este campo se encuentra vacío.</span>
                  </p>
                </div>
                
                <div class="p-buttons">
                  <button type="submit" class="p-button p-button_success p-button_largo g-activarCuenta-enviar_button">
                    <span>
                      <i class="fas fa-envelope"></i>
                    </span>
                    <span><b>Enviar correo</b></span>
                  </button>
                </div>
              </form>
            </div>
<?php
  }else{
?>
            <h3 class="p-text_p">Tu cuenta ha sido activada con éxito, ahora puedes iniciar sesión.</h3>
            
            <div class="p-buttons p-buttons_right p-cuenta-contenedor_botones p-margin-top_1rem">
              <a href="iniciar-sesion" class="p-button p-button_info">
                <span>
                  <i class="fas fa-user"></i>
                </span>
                <span><b>Iniciar sesión</b></span>
              </a>
            </div>
<?php
  }
?>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag_formularios.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php
  if($fechaActivacion_expirada){
?>
    <script src="scripts/activar_cuenta/scripts_activar_cuenta.js?v=1.7"></script>
<?php
  }
?>
<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>
</html><?php
}else{
  echo '<script> window.location = "/"; </script>';
}
?>