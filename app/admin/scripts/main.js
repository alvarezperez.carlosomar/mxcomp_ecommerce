$(document).ready(function(){
  /*
  * MENU LATERAL - Opciones
  */
 
  $('.g-menu-opcion').on('click', function(){
    let id = $(this).attr('id');

    $(this).toggleClass('mx-menulateral-menu_opcion_activo');
    $('.' + id).slideToggle('fast');
  });
  /*
  * NOTIFICACIONES - Clic en boton
  */
  $('#id-notificaciones').on('click', function(){
    $('#id-notificaciones_fondo').toggleClass('mx-nav-opcion_desplegable_activo');
    $('#id-notificaciones_contenedor').toggleClass('mx-nav-opcion_desplegable_activo');
    $(this).toggleClass('mx-nav-opcion_activo');
    $(this).removeClass('mx-nav-opcion_notificaciones');
  });
  /*
  * NOTIFICACIONES - Clic en fondo transparente
  */
  $('#id-notificaciones_fondo').on('click', function(){
    $('#id-notificaciones_fondo').toggleClass('mx-nav-opcion_desplegable_activo');
    $('#id-notificaciones_contenedor').toggleClass('mx-nav-opcion_desplegable_activo');
    $('#id-notificaciones').toggleClass('mx-nav-opcion_activo');
  });
  /*
  * BOTON ARRIBA
  */
  $(window).scroll(function(){
    if($(this).scrollTop() > 100){
      $('#id-boton_up').slideDown('fast');
    }else{
      $('#id-boton_up').slideUp('fast');
    }
  });
  $('#id-boton_up').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });
  /*
  * Botón mostrar/ocultar password
  */
  $(document).on('click', '.g-password-mostrar', function(){
    let id_campo = $(this).attr('id');
    
    $(this).toggleClass('mx-button_change_icon_disabled').toggleClass('mx-button_change_icon_enabled');
    
    if($('.' + id_campo).prop('type') === 'password'){
      $('.' + id_campo).prop('type', 'text');
    }else{
      $('.' + id_campo).prop('type', 'password');
    }
    $('.' + id_campo).focus();
  });
  //////////// SELECT /////////////////
  $(document).on('click', '.g-form-select', function(){
    let padre = $(this).parent();
    
    $(padre).toggleClass('mx-select_arrow');
  });

  $(document).on('blur', '.g-form-select', function(){
    let padre = $(this).parent();
    
    $(padre).removeClass('mx-select_arrow');
  });
});