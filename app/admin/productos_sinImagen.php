<?php
session_start();
include 'global/config.php'; // NECESARIO PARA HEAD
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Admin - Productos sin imagen</title>
  <?php include 'templates/head.php'; ?>

    <style>
    .contenedor{
      margin-bottom: 48px;
      padding: 16px;
      border: 2px solid #13557B;
      border-radius: 5px;
      overflow: hidden;
      overflow-x: auto;
      font-size: .85rem;
    }
    </style>
  </head>
  <body class="mx-body">
<?php
$navbar = "1"; // NO CAMBIAR
$titulo_navbar = "Productos sin imagen";
include 'templates/navbar.php';
?>

    <section class="mx-section-contenido">
      <div class="mx-menulateral-contenedor">
<?php
$menulateral = "1"; // NO CAMBIAR
$opcion = 3;
$sub_opcion = 1;
include 'templates/menu_lateral.php';
?>

      </div>
      <div class="mx-contenido-contenedor">
        <h1 class="mx-titulo-contenido">Productos sin imagen</h1>
        
        <div class="mx-columnas mx-contenedor-elementos_form_marginBottom">
          <div class="mx-columna">
            <div class="mx-contenedor-elementos_form">
              <label class="mx-label"><b>Buscar productos:</b></label>
              <div class="mx-contenedor-input">
                <button class="mx-button mx-button_turquesa" id="id-buscar-productos">Buscar productos</button>
              </div>
              <span class="mx-label" id="id-cantidad-productos">Se encontraron 0 producto(s).</span>
            </div>
          </div>
        </div>
        
        <div class="mx-contenedor-elementos_marginBottom_1" id="id-contenedor-select_marcas"></div>
        
        <div class="contenedor" id="id-contenedor-productos">
          <span>No hay resultados aún.</span>
        </div>
        
      </div>
    </section>

    <div class="mx-button-up" id="id-boton_up">
      <span>
        <i class="fas fa-chevron-up"></i>
      </span>
    </div>

<?php include 'templates/scripts_principales.php'; ?>

    <script>
      $(document).ready(function(){
        $(document).on('click', '#id-buscar-productos', function(){
          $('document, #id-cantidad-productos').html("Cargando información...");
          $('document, #id-contenedor-select_marcas').slideUp('fast');
          $('document, #id-contenedor-select_marcas').html('');
          buscar();
        });

        $(document).on('click', '#id-select', function(){
          $('document, .id-select').toggleClass('mx-select_arrow');
        });

        $(document).on('blur', '#id-select', function(){
          $('document, .id-select').removeClass('mx-select_arrow');
        });

        $(document).on('change', '#id-select', function(){
          let id_marca = $(this).val();

          $('document, #id-cantidad-productos').html("Cargando información...");

          if(id_marca.length !== 0){
            let data = new FormData();

            data.append("accion", "buscar");
            data.append("id_marca", id_marca);

            $('document, #id-contenedor-productos').html('<div class="mx-loading-general" id="id-loading-productos" style="display: block; padding: 0;"><div></div><p><b>Cargando información...</b></p></div>');

            fetch('productos/mostrar_productos_marca.php', {
              method: 'POST',
              body: data
            })
            .then(response => {
              return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
            })
            .then(datos => {
              let respuesta = datos.respuesta,
                  mensaje = datos.mensaje,
                  total = datos.total;

              switch(respuesta){
                case "status":
                  console.log("Status code: ", mensaje);
                  break;

                case "1":
                  setTimeout(function(){
                    $('document, #id-contenedor-productos').html(mensaje);
                    $('document, #id-cantidad-productos').html("Se encontraron " + total + " producto(s).");
                  }, 1500);
                  break;
              }
            })
            .catch(error => {
              console.log("Ocurrió un error en la petición: ", error);
            });
          }else{
            buscar();
          }
        });

        function buscar(){
          let data = new FormData();

          data.append("accion", "buscar");
          
          $('document, #id-contenedor-productos').html('<div class="mx-loading-general" id="id-loading-productos" style="display: block; padding: 0;"><div></div><p><b>Cargando información...</b></p></div>');

          fetch('productos/mostrar_productos.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje,
                total = datos.total;

            switch(respuesta){
              case "status":
                console.log("Status code: ", mensaje);
                break;

              case "1":
                $('document, #id-contenedor-productos').html(mensaje);
                $('document, #id-cantidad-productos').html("Se encontraron " + total + " producto(s).");

                select_marcas();
                break;
            }
          })
          .catch(error => {
            console.log("Ocurrió un error en la petición: ", error);
          });
        }

        function select_marcas(){
          let data = new FormData();

          data.append("accion", "mostrar");

          fetch('productos/mostrar_select_marcas.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;

            switch(respuesta){
              case "status":
                console.log("Status code: ", mensaje);
                break;

              case "1":
                $('document, #id-contenedor-select_marcas').html(mensaje);
                $('document, #id-contenedor-select_marcas').slideDown('fast');
                break;
            }
          })
          .catch(error => {
            console.log("Ocurrió un error en la petición: ", error);
          });
        }
      });
    </script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->
  </body>
</html>