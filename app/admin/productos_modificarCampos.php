<?php
session_start();
include 'global/config.php'; // NECESARIO PARA HEAD
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Admin - Modificar campos</title>
<?php include 'templates/head.php'; ?>

    <style>
      .contenedor-imagen{
        max-width: 150px;
        max-height: 150px;
        padding: .5rem;
        border-radius: 5px;
        border: 2px solid #CCC;
      }
      .contenedor-imagen img{
        width: 100%;
      }
      .mx-texto-span_archivo_ok{
        color: #2A97C7;
      }
      .mx-factura-loading{
        display: flex;
        align-items: center;
        padding: 0;
      }
      .mx-factura-loading div{
        width: auto;
        height: 30px;
      }
      .mx-factura-loading div::after{
        width: 20px;
        height: 20px;
      }
      .mx-factura-loading p{
        width: auto;
        font-size: 10px;
        margin-left: 10px;
      }
    </style>
  </head>
  <body class="mx-body">
<?php
$navbar = "1"; // NO CAMBIAR
$titulo_navbar = "Modificar campos";
include 'templates/navbar.php';
?>

    <section class="mx-section-contenido">
      <div class="mx-menulateral-contenedor">
<?php
$menulateral = "1"; // NO CAMBIAR
$opcion = 3;
$sub_opcion = 2;
include 'templates/menu_lateral.php';
?>

      </div>
      <div class="mx-contenido-contenedor">
        <h1 class="mx-titulo-contenido">Modificar campos</h1>
        
        <form class="mx-columnas mx-contenedor-elementos_form_marginBottom" id="id-buscar-producto_form">
          <div class="mx-columna mx-tres_columnas">
            <div class="mx-contenedor-elementos_form">
              <label class="mx-label" for="id-codigo-producto"><b>Código del producto:</b></label>
              <div class="mx-contenedor-input">
                <input type="text" class="mx-input" id="id-codigo-producto" placeholder="Código del producto" autocomplete="off">
              </div>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_info" id="id-codigo-producto_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span><b>Sólo se permite números y guion corto (-)</b></span>
              </p>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_error" id="id-codigo-producto_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span><b>Este campo se encuentra vacío.</b></span>
              </p>
            </div>
            <button type="submit" class="mx-button mx-button_turquesa">
              <span>
                <i class="fas fa-search"></i>
              </span>
              <span>Buscar producto</span>
            </button>
          </div>
        </form>
        
        <div class="mx-loading-general" id="id-loading-productos" style="padding: 0;">
          <div></div>
        </div>

        <div class="mx-marginTop_1rem mx-marginBottom_2rem" id="id-informacion-producto_success" style="display: none;">
          <div class="mx-notificacion mx-notificacion-letter_success">
            <span>
              <i class="fas fa-check-circle"></i>
            </span>
            <span class="mx-notificacion-p">Se modificó exitosamente la información.</span>
          </div>
        </div>

        <div class="mx-marginTop_1rem mx-marginBottom_2rem mx-seccion-contenedor" id="id-contenedor-informacion_producto" style="display: block;">
          <h4 class="mx-seccion-titulo">Editar información del producto</h4>
          
          <p class="mx-label mx-marginTop_1rem" style="justify-content: center;">No hay información para mostrar.</p>
        </div>
        
        <div class="mx-loading-general" id="id-loading-imagen" style="padding: 0;">
          <div></div>
        </div>
        
        <div class="mx-marginTop_1rem mx-marginBottom_2rem" id="id-campos_imagen_success" style="display: none;">
          <div class="mx-notificacion mx-notificacion-letter_success">
            <span>
              <i class="fas fa-check-circle"></i>
            </span>
            <span class="mx-notificacion-p" id="id-notificacion-success_imagen">Se subió la imagen y se modificó correctamente la información.</span>
          </div>
        </div>
        
        <div class="mx-marginTop_2rem mx-marginBottom_2rem mx-seccion-contenedor" id="id-contenedor-campos_imagen" style="display: block;">
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <p class="mx-label mx-marginTop_1rem" style="justify-content: center;">No hay información para mostrar.</p>
        </div>
        
      </div>
    </section>

    <div class="mx-button-up" id="id-boton_up">
      <span>
        <i class="fas fa-chevron-up"></i>
      </span>
    </div>

<?php include 'templates/scripts_principales.php'; ?>

    <script>
      $(document).ready(function(){
        var informacion_productos_success = false,
            campos_imagen_success = false,
            informacion_productos_loading = false,
            campos_imagen_loading = false;
        ////////////////////// BUSCAR DATOS DEL PRODUCTO //////////////////////
        /*
        * Input - Código del producto - keyup
        */
        $(document).on('keyup', '#id-codigo-producto', function(){
          let codigo_producto = $(this).val();
          
          if(codigo_producto.length !== 0){
            $('document, #id-codigo-producto_alert_error').slideUp('fast');
            $(this).removeClass('mx-input_error');
            
            let data = new FormData();
            data.append('accion', 'validar');
            data.append('valor', codigo_producto);
            
            fetch('validaciones/validar_codigoProducto.php', {
              method: 'POST',
              body: data
            })
            .then(response => {
              return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
            })
            .then(datos => {
              let respuesta = datos.respuesta,
                  mensaje = datos.mensaje;
              
              switch(respuesta){
                case "status":
                  console.log("Status code: ", mensaje);
                  break;

                case "1":
                  $('document, #id-codigo-producto_alert_info').slideUp('fast');
                  break;

                case "2":
                  $('document, #id-codigo-producto_alert_info').slideDown('fast');
                  break;
              }
            })
            .catch(error => {
              console.error("Ocurrió un error en la petición: ", error);
            });
          }else{
            $('document, #id-codigo-producto_alert_info').slideUp('fast');
          }
        });
        /*
        * Button - Buscar el producto - submit
        */
        $(document).on('submit', '#id-buscar-producto_form', function(e){
          informacion_productos_loading = true;
          mostrar_datos_producto();
          e.preventDefault();
        });
        ////////////////////// MODIFICAR CAMPOS //////////////////////
        /*
        * Textarea - Detecta el enter en el campo - keypress
        */
        $(document).on('keypress', '#id-descripcion-producto', function(e){
          if(e.keyCode === 13){
            return false;
          }
        });
        /*
        * Textarea - Descripcion del producto - keyup
        */
        $(document).on('keyup', '#id-descripcion-producto', function(e){
          if(e.keyCode === 13){
            $('document, #id-editar-informacion_submit').click();
          }else{
            let descripcion_producto = $(this).val();
            
            if(descripcion_producto.length !== 0){
              $('document, #id-descripcion-producto_alert_error').slideUp('fast');
              $(this).removeClass('mx-input_error');
              
              let data = new FormData();
              data.append('accion', 'validar');
              data.append('valor', descripcion_producto);
              
              fetch('validaciones/validar_descripcionProducto.php', {
                method: 'POST',
                body: data
              })
              .then(response => {
                return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
              })
              .then(datos => {
                let respuesta = datos.respuesta,
                    mensaje = datos.mensaje;
                
                switch(respuesta){
                  case "status":
                    console.log("Status code: ", mensaje);
                    break;

                  case "1":
                    $('document, #id-descripcion-producto_alert_info').slideUp('fast');
                    break;

                  case "2":
                    $('document, #id-descripcion-producto_alert_info').slideDown('fast');
                    break;
                }
              })
              .catch(error => {
                console.error("Ocurrió un error en la petición: ", error);
              });
            }else{
              $('document, #id-descripcion-producto_alert_info').slideUp('fast');
            }
          }
        });
        /*
        * Input - SKU del fabricante - keyup
        */
        $(document).on('keyup', '#id-sku-fabricante', function(e){
          if(e.keyCode === 13){
            $('document, #id-editar-informacion_submit').click();
          }else{
            let sku_fabricante = $(this).val();

            if(sku_fabricante.length !== 0){
              $('document, #id-sku-fabricante_alert_error').slideUp('fast');
              $(this).removeClass('mx-input_error');

              let data = new FormData();
              data.append('accion', 'validar');
              data.append('valor', sku_fabricante);

              fetch('validaciones/validar_descripcionProducto.php', {
                method: 'POST',
                body: data
              })
              .then(response => {
                return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
              })
              .then(datos => {
                let respuesta = datos.respuesta,
                    mensaje = datos.mensaje;

                switch(respuesta){
                  case "status":
                    console.log("Status code: ", mensaje);
                    break;

                  case "1":
                    $('document, #id-sku-fabricante_alert_info').slideUp('fast');
                    break;

                  case "2":
                    $('document, #id-sku-fabricante_alert_info').slideDown('fast');
                    break;
                }
              })
              .catch(error => {
                console.error("Ocurrió un error en la petición: ", error);
              });
            }else{
              $('document, #id-sku-fabricante_alert_info').slideUp('fast');
            }
          }
        });
        /*
        * Button - Editar informacion del producto - Submit
        */
        $(document).on('click', '#id-editar-informacion_submit', function(){
          let codigo_producto = $('document, #id-codigo-producto_input').val(),
              descripcion_producto = $('document, #id-descripcion-producto').val(),
              sku_fabricante = $('document, #id-sku-fabricante').val(),
              data = new FormData();
          
          data.append("accion", "modificar");
          data.append("codigo_producto", codigo_producto);
          data.append("descripcion_producto", descripcion_producto);
          data.append("sku_fabricante", sku_fabricante);
          
          fetch('productos/modificar_campos_producto.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;
            
            switch(respuesta){
              case "status":
                console.log("Status code: ", mensaje);
                break;

              case "0":
                console.error(mensaje);
                break;

              case "1":
                $('document, #id-descripcion-producto_alert_error').slideDown('fast');
                $('document, #id-descripcion-producto').addClass('mx-input_error');
                $('document, #id-descripcion-producto').focus();
                break;

              case "2":
                $('document, #id-descripcion-producto_alert_info').slideDown('fast');
                $('document, #id-descripcion-producto').focus();
                break;

              case "3":
                $('document, #id-sku-fabricante_alert_error').slideDown('fast');
                $('document, #id-sku-fabricante').addClass('mx-input_error');
                $('document, #id-sku-fabricante').focus();
                break;

              case "4":
                $('document, #id-sku-fabricante_alert_info').slideDown('fast');
                $('document, #id-sku-fabricante').focus();
                break;

              case "5":
                $('document, #id-loading-productos').slideDown('fast');
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": ".5"
                });
                
                setTimeout(function(){
                  $('document, #id-loading-productos').slideUp('fast');
                  $('document, #id-contenedor-informacion_producto').css({
                    "opacity": "1"
                  });
                  $('document, #id-contenedor-informacion_producto').html(mensaje);
                }, 500);
                
                setTimeout(function(){
                  informacion_productos_loading = true;
                  mostrar_datos_producto();
                }, 2500);
                break;

              case "6":
                //console.log(mensaje);
                informacion_productos_success = true;
                informacion_productos_loading = true;
                mostrar_datos_producto();
                break;
            }
          })
          .catch(error => {
            console.error("Ocurrió un error en la petición: ", error);
          });
        });
        ////////////////////// SUBIR IMAGEN //////////////////////
        /*
        * Button - Ejecuta click en input file
        */
        $(document).on('click', '#id-subir-imagen_button', function(){
          $('document, #id-subir-imagen_file').click();
        });
        /*
        * Input - Se detecta la subida del archivo
        */
        $(document).on('change', '#id-subir-imagen_file', function(){
          let nombre, size, extension, nombre_completo, archivo = $(this)[0].files[0];
          
          $('document, #id-subir-imagen_span').slideDown('fast');
          
          if(archivo){
            $('document, #id-subir-imagen_span').html('<span><b>Cargando</b></span><span><i class="fas fa-spinner"></i></span>');
            $('document, #id-subir-imagen_span').addClass('mx-texto-span_archivo_ok');
            
            nombre = archivo.name;
            extension = nombre.split('.').pop();
            extension = extension.toLowerCase();
            size = archivo.size;
            
            // Si es igual a jgp
            if(extension === "jpg"){
              
              // Si es menor a los 5 MB, 5242880 bytes en binario
              if(size < 5242880){
                nombre_completo = archivo.name;
                
                if(nombre.length > 20){
                  nombre = nombre.substr(0, 10) + '...' + nombre.substr(-10);
                }
                
                $('document, #id-subir-imagen_span').html('<span title="' + nombre_completo + '"><b>' + nombre + '</b></span><span><i class="fas fa-check-circle"></i></span>');
                $('document, #id-subir-imagen_span').addClass('mx-texto-span_archivo_ok');
                
                let data = new FormData();
                
                data.append("accion", "mostrar");
                
                fetch('productos/boton_subir_imagen.php', {
                  method: 'POST',
                  body: data
                })
                .then(response => {
                  return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
                })
                .then(datos => {
                  let respuesta = datos.respuesta,
                      mensaje = datos.mensaje;
                  
                  switch(respuesta){
                    case "status":
                      console.log("Status code: ", mensaje);
                      break;

                    case "1":
                      $('document, #id-contenedor-boton_subir_imagen').html(mensaje);
                      $('document, #id-contenedor-boton_subir_imagen').slideDown('fast');
                      break;
                  }
                })
                .catch(error => {
                  console.error("Ocurrió un error en la petición: ", error);
                });
              }else{
                $('document, #id-subir-imagen_span').html('<span><b>El formato del archivo es .' + extension + ', pero supera los 5 MB.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen').html('');
              }
            }else{
              $('document, #id-subir-imagen_span').html('<span><b>El formato de la imagen no es válido, sólo se permite jpg.</b></span><span><i class="fas fa-times-circle"></i></span>');
              $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
              $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
              $('document, #id-contenedor-boton_subir_imagen').html('');
            }
          }else{
            $('document, #id-subir-imagen_span').html('<span><b>No se eligió el archivo.</b></span><span><i class="fas fa-times-circle"></i></span>');
            $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
            $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
            $('document, #id-contenedor-boton_subir_imagen').html('');
          }
        });
        /*
        * Boton - Subir imagen - Submit
        */
        $(document).on('click', '#id-subir-imagen_submit', function(){
          let archivo = $('document, #id-subir-imagen_file')[0].files[0],
              codigo_producto = $('document, #id-codigo-producto_input').val(),
              data = new FormData();
          
          data.append("accion", "subir");
          data.append("archivo", archivo);
          data.append("codigo_producto", codigo_producto);
          
          $('document, #id-subir-imagen_span').slideUp('fast');
          
          fetch('productos/proceso_subir_imagen.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;
            
            switch(respuesta){
              case 'status':
                console.log("Status code: ", mensaje);
                $('document, #id-subir-imagen_file').val('');
                break;

              case '0':
                console.error(mensaje);
                $('document, #id-subir-imagen_file').val('');
                break;

              case '1':
                $('document, #id-loading-imagen').slideDown('fast');
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": ".5"
                });
                
                setTimeout(function(){
                  $('document, #id-loading-imagen').slideUp('fast');
                  $('document, #id-contenedor-campos_imagen').css({
                    "opacity": "1"
                  });
                  $('document, #id-contenedor-campos_imagen').html(mensaje);
                  $('document, #id-subir-imagen_span').slideDown('fast');
                }, 500);
                
                setTimeout(function(){
                  campos_imagen_loading = true;
                  mostrar_datos_producto();
                }, 4500);
                break;

              case '2':
                $('document, #id-subir-imagen_span').html('<span><b>No se eligió el archivo.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen').html('');
                break;

              case '3':
                $('document, #id-subir-imagen_span').html('<span><b>El formato de la imagen no es válido, sólo se permite jpg.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen').html('');
                break;

              case '4':
                let nombre = archivo.name,
                    formato = nombre.split('.').pop();
                
                formato = formato.toLowerCase();
                
                $('document, #id-subir-imagen_span').html('<span><b>El formato del archivo es .' + formato + ', pero supera los 5 MB.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen').html('');
                break;

              case '5':
                $('document, #id-subir-imagen_span').html('<span><b>El archivo no se subió, vuelve a realizar el proceso.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen').html('');
                $('document, #id-subir-imagen_file').val('');
                break;

              case '6':
                $('document, #id-subir-imagen_span').slideDown('fast');
                
                campos_imagen_success = true;
                campos_imagen_loading = true;
                
                $('document, #id-notificacion-success_imagen').text(mensaje);
                mostrar_datos_producto();
                break;
            }
          })
        });
        ////////////////////// MODIFICAR IMAGEN //////////////////////
        /*
        * Button - Ejecuta click en input file
        */
        $(document).on('click', '#id-subir-imagen_adicional_button', function(){
          $('document, #id-subir-imagen_adicional_file').click();
        });
        /*
        * Input - Se detecta la subida del archivo
        */
        $(document).on('change', '#id-subir-imagen_adicional_file', function(){
          let nombre, size, extension, nombre_completo, archivo = $(this)[0].files[0];
          
          $('document, #id-subir-imagen_adicional_span').slideDown('fast');
          
          if(archivo){
            $('document, #id-subir-imagen_adicional_span').html('<span><b>Cargando</b></span><span><i class="fas fa-spinner"></i></span>');
            $('document, #id-subir-imagen_adicional_span').addClass('mx-texto-span_archivo_ok');
            
            nombre = archivo.name;
            extension = nombre.split('.').pop();
            extension = extension.toLowerCase();
            size = archivo.size;
            
            // Si es igual a jgp
            if(extension === "jpg"){
              
              // Si es menor a los 5 MB, 5242880 bytes en binario
              if(size < 5242880){
                nombre_completo = archivo.name;
                
                if(nombre.length > 20){
                  nombre = nombre.substr(0, 10) + '...' + nombre.substr(-10);
                }
                
                $('document, #id-subir-imagen_adicional_span').html('<span title="' + nombre_completo + '"><b>' + nombre + '</b></span><span><i class="fas fa-check-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').addClass('mx-texto-span_archivo_ok');
                
                let data = new FormData();
                
                data.append("accion", "mostrar");
                
                fetch('productos/boton_subir_imagen_adicional.php', {
                  method: 'POST',
                  body: data
                })
                .then(response => {
                  return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
                })
                .then(datos => {
                  let respuesta = datos.respuesta,
                      mensaje = datos.mensaje;
                  
                  switch(respuesta){
                    case "status":
                      console.log("Status code: ", mensaje);
                      break;

                    case "1":
                      $('document, #id-contenedor-boton_subir_imagen_adicional').html(mensaje);
                      $('document, #id-contenedor-boton_subir_imagen_adicional').slideDown('fast');
                      break;
                  }
                })
                .catch(error => {
                  console.error("Ocurrió un error en la petición: ", error);
                });
              }else{
                $('document, #id-subir-imagen_adicional_span').html('<span><b>El formato del archivo es .' + extension + ', pero supera los 5 MB.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
              }
            }else{
              $('document, #id-subir-imagen_adicional_span').html('<span><b>El formato de la imagen no es válido, sólo se permite jpg.</b></span><span><i class="fas fa-times-circle"></i></span>');
              $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
              $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
              $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
            }
          }else{
            $('document, #id-subir-imagen_adicional_span').html('<span><b>No se eligió el archivo.</b></span><span><i class="fas fa-times-circle"></i></span>');
            $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
            $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
            $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
          }
        });
        /*
        * Boton - Subir imagen adicional - Submit
        */
        $(document).on('click', '#id-subir-imagen_adicional_submit', function(){
          let archivo = $('document, #id-subir-imagen_adicional_file')[0].files[0],
              codigo_producto = $('document, #id-codigo-producto_input').val(),
              numero_imagen = $('document, #id-numero-imagen_input').val(),
              data = new FormData();
          
          data.append("accion", "subir");
          data.append("archivo", archivo);
          data.append("codigo_producto", codigo_producto);
          data.append("numero_imagen", numero_imagen);
          
          $('document, #id-subir-imagen_adicional_span').slideUp('fast');
          
          fetch('productos/proceso_subir_imagen_adicional.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;
            
            switch(respuesta){
              case 'status':
                console.log("Status code: ", mensaje);
                $('document, #id-subir-imagen_adicional_file').val('');
                break;

              case '0':
                console.error(mensaje);
                $('document, #id-subir-imagen_adicional_file').val('');
                break;

              case '1':
                $('document, #id-loading-imagen').slideDown('fast');
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": ".5"
                });
                
                setTimeout(function(){
                  $('document, #id-loading-imagen').slideUp('fast');
                  $('document, #id-contenedor-campos_imagen').css({
                    "opacity": "1"
                  });
                  $('document, #id-contenedor-campos_imagen').html(mensaje);
                  $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                }, 500);
                
                setTimeout(function(){
                  campos_imagen_loading = true;
                  mostrar_datos_producto();
                }, 4500);
                break;

              case '2':
                $('document, #id-subir-imagen_adicional_span').html('<span><b>No se eligió el archivo.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
                break;

              case '3':
                $('document, #id-subir-imagen_adicional_span').html('<span><b>El formato de la imagen no es válido, sólo se permite jpg.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
                break;

              case '4':
                let nombre = archivo.name,
                    formato = nombre.split('.').pop();
                
                formato = formato.toLowerCase();
                
                $('document, #id-subir-imagen_adicional_span').html('<span><b>El formato del archivo es .' + formato + ', pero supera los 5 MB.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
                break;

              case '5':
                $('document, #id-subir-imagen_adicional_span').html('<span><b>El archivo no se subió, vuelve a realizar el proceso.</b></span><span><i class="fas fa-times-circle"></i></span>');
                $('document, #id-subir-imagen_adicional_span').removeClass('mx-texto-span_archivo_ok');
                $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').slideUp('fast');
                $('document, #id-contenedor-boton_subir_imagen_adicional').html('');
                $('document, #id-subir-imagen_adicional_file').val('');
                break;

              case '6':
                $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                
                campos_imagen_success = true;
                campos_imagen_loading = true;
                
                $('document, #id-notificacion-success_imagen').text(mensaje);
                mostrar_datos_producto();
                break;
            }
          })
          .catch(error => {
            console.error("Ocurrió un error en la petición: ", error);
          });
        });
        ///////////////////////////// BOTONES DE ELIMINAR /////////////////////////////
        /*
        * Boton - Eliminar todas las imagenes
        */
        $(document).on('click', '#id-eliminar-imagenes', function(){
          let codigo_producto = $('document, #id-codigo-producto_input').val(),
              data = new FormData();
          
          data.append("accion", "eliminar");
          data.append("codigo_producto", codigo_producto);
          
          fetch('productos/proceso_eliminar_imagenes_all.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;
            
            switch(respuesta){
              case "status":
                console.log("Status code: ", mensaje);
                break;

              case "0":
                console.error(mensaje);
                break;

              case "1":
                $('document, #id-loading-imagen').slideDown('fast');
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": ".5"
                });
                
                setTimeout(function(){
                  $('document, #id-loading-imagen').slideUp('fast');
                  $('document, #id-contenedor-campos_imagen').css({
                    "opacity": "1"
                  });
                  $('document, #id-contenedor-campos_imagen').html(mensaje);
                  $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                }, 500);
                
                setTimeout(function(){
                  campos_imagen_loading = true;
                  mostrar_datos_producto();
                }, 4500);
                break;

              case "2":
                campos_imagen_success = true;
                campos_imagen_loading = true;
                
                $('document, #id-notificacion-success_imagen').text(mensaje);
                mostrar_datos_producto();
                break;
            }
          })
          .catch(error => {
            console.error("Ocurrió un error en la petición: ", error);
          });
        });
        /*
        * Boton - Eliminar imagen
        */
        $(document).on('click', '.g-eliminar-imagen', function(){
          let numero_imagen = $(this).attr('id'),
              codigo_producto = $('document, #id-codigo-producto_input').val(),
              data = new FormData();
          
          numero_imagen = numero_imagen.replace('id-eliminar-imagen_', '');
          
          data.append("accion", "eliminar");
          data.append("codigo_producto", codigo_producto);
          data.append("numero_imagen", numero_imagen);
          
          fetch('productos/proceso_eliminar_imagen.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje;
            
            switch(respuesta){
              case "status":
                console.log("Status code: ", mensaje);
                break;

              case "0":
                console.error(mensaje);
                break;

              case "1":
                $('document, #id-loading-imagen').slideDown('fast');
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": ".5"
                });
                
                setTimeout(function(){
                  $('document, #id-loading-imagen').slideUp('fast');
                  $('document, #id-contenedor-campos_imagen').css({
                    "opacity": "1"
                  });
                  $('document, #id-contenedor-campos_imagen').html(mensaje);
                  $('document, #id-subir-imagen_adicional_span').slideDown('fast');
                }, 500);
                
                setTimeout(function(){
                  campos_imagen_loading = true;
                  mostrar_datos_producto();
                }, 4500);
                break;

              case "2":
                campos_imagen_success = true;
                campos_imagen_loading = true;
                
                $('document, #id-notificacion-success_imagen').text(mensaje);
                mostrar_datos_producto();
                break;
            }
          })
          .catch(error => {
            console.error("Ocurrió un error en la petición: ", error);
          });
        });
        ///////////////////////////// FUNCIONES /////////////////////////////
        function mostrar_datos_producto(){
          let codigo_producto = "",
              data = new FormData();
          
          codigo_producto = $('document, #id-codigo-producto').val();
          
          if(codigo_producto === ""){
            codigo_producto = $('document, #id-codigo-producto_input').val();
          }
          
          data.append("accion", "buscar");
          data.append("codigo_producto", codigo_producto);
          
          if(informacion_productos_loading){
            $('document, #id-loading-productos').slideDown('fast');
          }
          $('document, #id-contenedor-informacion_producto').css({
            "opacity": ".5"
          });
          
          if(campos_imagen_loading){
            $('document, #id-loading-imagen').slideDown('fast');
          }
          $('document, #id-contenedor-campos_imagen').css({
            "opacity": ".5"
          });
          
          fetch('productos/mostrar_campos_producto.php', {
            method: 'POST',
            body: data
          })
          .then(response => {
            return response.ok ? response.json() : [{ "respuesta": "status", "mensaje": response.status }];
          })
          .then(datos => {
            let respuesta = datos.respuesta,
                mensaje = datos.mensaje,
                campos_producto = datos.campos_producto,
                campos_imagen = datos.campos_imagen;
            
            switch(respuesta){
              case "status":
                if(informacion_productos_loading){
                  informacion_productos_loading = false;
                  $('document, #id-loading-productos').slideUp('fast');
                }
                
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });
                
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                
                console.log("Status code: ", mensaje);
                break;

              case "0":
                if(informacion_productos_loading){
                  informacion_productos_loading = false;
                  $('document, #id-loading-productos').slideUp('fast');
                }
                
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });
                
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                
                console.error(mensaje);
                break;

              case "1":
                if(informacion_productos_loading){
                  informacion_productos_loading = false;
                  $('document, #id-loading-productos').slideUp('fast');
                }
                
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });
                
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                
                $('document, #id-codigo-producto_alert_error').slideDown('fast');
                $('document, #id-codigo-producto').addClass('mx-input_error');
                $('document, #id-codigo-producto').focus();
                break;

              case "2":
                if(informacion_productos_loading){
                  informacion_productos_loading = false;
                  $('document, #id-loading-productos').slideUp('fast');
                }
                
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });
                
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                
                $('document, #id-codigo-producto_alert_info').slideDown('fast');
                $('document, #id-codigo-producto').focus();
                break;

              case "3":
                if(informacion_productos_loading){
                  informacion_productos_loading = false;
                  $('document, #id-loading-productos').slideUp('fast');
                }
                
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });
                
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                
                $('document, #id-codigo-producto').focus();
                
                $('document, #id-contenedor-informacion_producto').html(campos_producto);
                $('document, #id-contenedor-informacion_producto').css({
                  "opacity": "1"
                });

                $('document, #id-contenedor-campos_imagen').html(campos_imagen);
                $('document, #id-contenedor-campos_imagen').css({
                  "opacity": "1"
                });
                break;

              case "4":
                setTimeout(function(){
                  if(informacion_productos_loading){
                    informacion_productos_loading = false;
                    $('document, #id-loading-productos').slideUp('fast');
                  }
                  
                  if(campos_imagen_loading){
                    campos_imagen_loading = false;
                    $('document, #id-loading-imagen').slideUp('fast');
                  }
                  
                  $('document, #id-contenedor-informacion_producto').html(campos_producto);
                  $('document, #id-contenedor-informacion_producto').css({
                    "opacity": "1"
                  });
                  
                  $('document, #id-contenedor-campos_imagen').html(campos_imagen);
                  $('document, #id-contenedor-campos_imagen').css({
                    "opacity": "1"
                  });
                }, 1000);
                
                if(informacion_productos_success){
                  informacion_productos_success = false;
                  
                  setTimeout(function(){
                    $('document, #id-informacion-producto_success').slideDown('fast');
                  }, 1000);
                  
                  setTimeout(function(){
                    $('document, #id-informacion-producto_success').slideUp('fast');
                  }, 4000);
                }
                
                if(campos_imagen_success){
                  campos_imagen_success = false;
                  
                  setTimeout(function(){
                    $('document, #id-campos_imagen_success').slideDown('fast');
                  }, 1000);
                  
                  setTimeout(function(){
                    $('document, #id-campos_imagen_success').slideUp('fast');
                  }, 4000);
                }
                break;
            }
          })
          .catch(error => {
            console.log("Ocurrió un error en la petición: ", error);
          });
        }
      });
    </script>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->
  </body>
</html>