<?php
require_once dirname(__DIR__, 1) . '/conn.php';

$Conexion_MXcomp = new Conexion_mxcomp();
$Conexion_admin = new Conexion_admin();

date_default_timezone_set('America/Mexico_City');  // HORARIO DE MEXICO

$estado_proceso = "";
$mensaje_error = "";
$productosDesactivados_fechaActualizacion = 0;
$productosDesactivados_fechaCreacion = 0;
$fecha_actual = date("Y-m-d H:i:s");

$fecha_buscar = date("Y-m-d H:i:s", strtotime($fecha_actual . "-15 days"));

try{
  $Conexion_MXcomp->pdo->beginTransaction();

  ///////////// REVISA SI LA FECHA DE ACTUALIZACION, PARA DETERMINAR SI EL PRODUCTO TIENE MAS DE LOS DÍAS INDICADOS
  $sql = "SELECT COUNT(id) FROM __productos WHERE fechaActualizacion < :fecha_buscar AND activo = 1 ORDER BY fechaActualizacion ASC";
  $stmt = $Conexion_MXcomp->pdo->prepare($sql);
  $stmt->bindParam(':fecha_buscar', $fecha_buscar, PDO::PARAM_STR);
  $stmt->execute();
  $productos_existen = (int) $stmt->fetchColumn();
  $productosDesactivados_fechaActualizacion = $productos_existen;
  
  // SI SE ENCUENTRAN PRODUCTOS, ESTOS SE DESACTIVARAN
  if($productos_existen > 0){
    $sql = "UPDATE __productos SET existenciaTotal = 0, almacenes = NULL, activo = 0 WHERE fechaActualizacion < :fecha_buscar";
    $stmt = $Conexion_MXcomp->pdo->prepare($sql);
    $stmt->bindParam(':fecha_buscar', $fecha_buscar, PDO::PARAM_STR);
    $stmt->execute();
  }
  
  ///////////// REVISA SI LA FECHA DE CREACION, PARA DETERMINAR SI EL PRODUCTO TIENE MAS DE LOS DÍAS INDICADOS DESDE QUE SE CREO, PERO DE IGUAL FORMA QUE NO TENGA UNA FECHA DE ACTUALIZACION.
  $sql = "SELECT COUNT(id) FROM __productos WHERE fechaActualizacion IS NULL AND fechaCreacion < :fecha_buscar AND activo = 1 ORDER BY fechaCreacion ASC";
  $stmt = $Conexion_MXcomp->pdo->prepare($sql);
  $stmt->bindParam(':fecha_buscar', $fecha_buscar, PDO::PARAM_STR);
  $stmt->execute();
  $productos_existen = (int) $stmt->fetchColumn();
  $productosDesactivados_fechaCreacion = $productos_existen;
  
  // SI SE ENCUENTRAN PRODUCTOS, ESTOS SE DESACTIVARAN
  if($productos_existen > 0){
    $sql = "UPDATE __productos SET existenciaTotal = 0, almacenes = NULL, activo = 0 WHERE fechaActualizacion IS NULL AND fechaCreacion < :fecha_buscar";
    $stmt = $Conexion_MXcomp->pdo->prepare($sql);
    $stmt->bindParam(':fecha_buscar', $fecha_buscar, PDO::PARAM_STR);
    $stmt->execute();
  }
  
  $estado_proceso = "Exito";
  $mensaje_error = NULL;
  
  $stmt = null;
  $Conexion_MXcomp->pdo->commit();
}catch(PDOException $e){
  $Conexion_MXcomp->pdo->rollBack();
  $estado_proceso = "Error";
  $mensaje_error = $e->getMessage();
  echo $e->getMessage();
}

//GUARDA EL ESTADO DE LA EJECUCION - ADMIN MXCOMP
try{
  $Conexion_admin->pdo->beginTransaction();

  $fechaCreacionBitacora = date("Y-m-d H:i:s");
  $sql = "INSERT INTO __bitacora_desactivacion_productos_desactualizados(estadoProceso, mensajeError, productosDesac_fechActualizacion, productosDesac_fechCreacion, fechaCreacion) VALUES (:estadoProceso, :mensajeError, :productosDesac_fechActualizacion, :productosDesac_fechCreacion, :fechaCreacion);";
  $stmt_admin = $Conexion_admin->pdo->prepare($sql);
  $stmt_admin->bindParam(':estadoProceso', $estado_proceso, PDO::PARAM_STR);
  $stmt_admin->bindParam(':mensajeError', $mensaje_error, PDO::PARAM_STR);
  $stmt_admin->bindParam(':productosDesac_fechActualizacion', $productosDesactivados_fechaActualizacion, PDO::PARAM_INT);
  $stmt_admin->bindParam(':productosDesac_fechCreacion', $productosDesactivados_fechaCreacion, PDO::PARAM_INT);
  $stmt_admin->bindParam(':fechaCreacion', $fechaCreacionBitacora, PDO::PARAM_STR);
  $stmt_admin->execute();

  $stmt_admin = null;
  $Conexion_admin->pdo->commit();
}catch(PDOException $e){
  $Conexion_admin->pdo->rollBack();
  echo $e->getMessage();
}
?>