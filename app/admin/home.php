<?php
include 'global/config.php'; // NECESARIO PARA HEAD
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Admin - Bienvenido</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body class="mx-body">
<?php
$navbar = "1"; // NO CAMBIAR
$titulo_navbar = "Bienvenido";
include 'templates/navbar.php';
?>

    <section class="mx-section-contenido">
      <div class="mx-menulateral-contenedor">
<?php
$menulateral = "1"; // NO CAMBIAR
$opcion = 0;
$sub_opcion = 0;
include 'templates/menu_lateral.php';
?>

      </div>
      
      <div class="mx-contenido-contenedor">
        <h1 class="mx-titulo-contenido">Bienvenido</h1>
      </div>
    </section>

<?php include 'templates/scripts_principales.php'; ?>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->
  </body>
</html>