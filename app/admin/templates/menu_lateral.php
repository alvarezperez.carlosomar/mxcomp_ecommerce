<?php
if(isset($menulateral) && $menulateral === "1"){
?>
        <aside class="mx-menulateral">
          <ul class="mx-menulateral-menu">
            <li class="mx-menulateral-menu_opcion">
              <a class="mx-menulateral-menu_opcion_link mx-menulateral-menu_opcion_inactivo g-menu-opcion<?php echo $opcion === 3 ? ' mx-menulateral-menu_opcion_activo' : ''; ?>" id="id-menu_3">
                <span class="mx-menulateral-menu_icon">
                  <i class="fas fa-boxes"></i>
                </span>
                <span class="mx-menulateral-menu_text">Productos</span>
              </a>
              <ul class="mx-menulateral-submenu id-menu_3"<?php echo $opcion === 3 ? ' style="display: block"' : ''; ?>>
                <li>
                  <a<?php echo $opcion === 3 && $sub_opcion === 1 ? '' : ' href="productos/sin-imagen"'; ?> class="mx-menulateral-submenu_opcion_link<?php echo $opcion === 3 && $sub_opcion === 1 ? ' mx-menulateral-submenu_opcion_link_activo' : ''; ?>">
                    <span class="mx-menulateral-menu_icon">
                      <i class="fas fa-images"></i>
                    </span>
                    <span class="mx-menulateral-menu_text">Sin imagen</span>
                  </a>
                </li>
                <li>
                  <a<?php echo $opcion === 3 && $sub_opcion === 2 ? '' : ' href="productos/modificar-campos"'; ?> class="mx-menulateral-submenu_opcion_link<?php echo $opcion === 3 && $sub_opcion === 2 ? ' mx-menulateral-submenu_opcion_link_activo' : ''; ?>">
                    <span class="mx-menulateral-menu_icon">
                      <i class="fas fa-edit"></i>
                    </span>
                    <span class="mx-menulateral-menu_text">Modificar campos</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </aside>
<?php
}
?>