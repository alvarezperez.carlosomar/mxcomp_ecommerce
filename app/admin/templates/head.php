    <base href="<?php echo HOST_LINK; ?>">
    <meta name="msapplication-tap-highlight" content="no">
    <link rel="manifest" href="manifest.json">
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/favicon.ico" type="image/x-icon">
    <meta name="application-name" content="Administraddor de MXcomp">
    <link rel="icon" sizes="192x192" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/chrome-touch-icon-192x192.png?v=">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="white">
    <meta name="apple-mobile-web-app-title" content="Administrador de MXcomp">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-57x57.png?v=">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-60x60.png?v=">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-72x72.png?v=">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-76x76.png?v=">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-114x114.png?v=">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-120x120.png?v=">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-144x144.png?v=">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-152x152.png?v=">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/apple-touch-icon-180x180.png?v=">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/ms-touch-icon-144x144-precomposed.png?v=">
    <meta name="theme-color" content="#FFFFFF">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/icon-16x16.png?v=">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/icon-32x32.png?v=">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo HOST_LINK_IMAGES_HEAD; ?>images/touch/icon-96x96.png?v=">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu:700&display=swap" rel="stylesheet">
    <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" data-auto-replace-svg="nest"></script>
    <link rel="stylesheet" href="styles/main.css?v=1.6">