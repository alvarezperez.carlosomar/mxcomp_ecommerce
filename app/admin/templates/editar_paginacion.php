<?php
if(isset($_POST['accion']) && $_POST['accion'] === "paginacion"){
  session_start();
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  
  $pagina = trim($_POST['pagina']);
  $registros_por_pagina = (int) $_SESSION['__registros_por_pagina__'];
  
  if($pagina !== "" && validar_campo_numerico($pagina)){
    $_SESSION['__pagina_actual__'] = (int) $pagina;
    
    $total_registros = (int) trim($_SESSION['__total_registros__']);
    $productosPorPagina = (int) trim($registros_por_pagina);
    $paginas = (float) ceil($total_registros / $registros_por_pagina);
    
    if($_SESSION['__pagina_actual__'] !== 0 && $_SESSION['__pagina_actual__'] <= $paginas){
      $respuesta = "1"; // PAGINACION CORRECTA
      $mensaje = "";
    }else{
      $_SESSION['__pagina_actual__'] = 1;
      $respuesta = "2"; // HUBO UN PROBLEMA EN LA PAGINACIÓN
      $mensaje = "Hubo un problema en la paginación.";
    }
  }else{
    $respuesta = "2"; // HUBO UN PROBLEMA EN LA PAGINACIÓN
    $mensaje = "Hubo un problema en la paginación.";
  }
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>