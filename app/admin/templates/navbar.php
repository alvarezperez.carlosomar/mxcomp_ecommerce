<?php
if(isset($navbar) && $navbar === "1"){
?>
    <nav class="mx-nav">
      <div class="mx-nav-start">
        <a class="mx-nav-opcion_logo">
          <img class="mx-nav-opcion_logo_img" src="../images/logo_mxcomp.jpg" alt="MXcomp; la tecnología en tus manos">
        </a>
        <span class="mx-nav-start_titulo"><?php echo $titulo_navbar; ?></span>
      </div>
      <div class="mx-nav-end">
<?php
        /* <div class="mx-nav-opcion_desplegable">
          <a class="mx-nav-opcion mx-nav-opcion_notificaciones" id="id-notificaciones" title="Notificaciones">
            <span>
              <i class="fas fa-lg fa-bell"></i>
            </span>
          </a>
          <div class="mx-nav-opcion_desplegable_fondo" id="id-notificaciones_fondo"></div>
          <div class="mx-nav-opcion_desplegable_contenedor" id="id-notificaciones_contenedor">
            <h3>Notificaciones</h3>
            <div class="mx-nav-notificaciones_contenedor">
              <div class="mx-nav-notificaciones">
                <a class="mx-nav-notificaciones_link">
                  <span class="mx-nav-notificaciones_icon mx-nav-notificaciones_icon_activo">
                    <i class="far fa-circle"></i>
                    <i class="fas fa-circle"></i>
                  </span>
                  <div>
                    <span>Se realizó pago para una venta del usuario ALPECA_1</span>
                    <span><b>9 de Abril 2020</b></span>
                  </div>
                </a>

                <a class="mx-nav-notificaciones_link">
                  <span class="mx-nav-notificaciones_icon">
                    <i class="far fa-circle"></i>
                    <i class="fas fa-circle"></i>
                  </span>
                  <div>
                    <span>Se realizó pago para una venta del usuario ALPECA_2</span>
                    <span><b>2 de Abril 2020</b></span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <a class="mx-nav-opcion" title="Cerrar sesión">
          <span>
            <i class="fas fa-lg fa-sign-out-alt"></i>
          </span>
        </a> */
?>
      </div>
    </nav>
<?php
}
?>