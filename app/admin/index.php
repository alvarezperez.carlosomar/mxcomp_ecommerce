<?php
include 'global/config.php'; // NECESARIO PARA HEAD
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Inicia sesión</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
    <section class="mx-section-formulario_sesion">
      <div class="mx-formulario-sesion_div">
        <h1 class="mx-formulario_titulo">INICIA SESIÓN</h1>
        <div class="mx-formulario_elementos">
          <div class="mx-notificacion mx-notificacion-success">
            <span>Iniciando sesión...</span>
          </div>
          
          <div class="mx-contenedor-elementos_form mx-contenedor-elementos_form_marginBottom">
            <label class="mx-label" for="id-correo"><b>Correo electrónico:</b></label>
            <div class="mx-contenedor-input">
              <input type="text" class="mx-input mx-input_error" id="id-correo" placeholder="Correo electrónico">
            </div>
            <p class="mx-texto mx-texto-oculto mx-texto-notificacion_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span><b>No cumple con el formato estándar: ejemplo@ejemplo.com</b></span>
            </p>
            <p class="mx-texto mx-texto-oculto mx-texto-notificacion_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span><b>Este campo se encuentra vacío.</b></span>
            </p>
          </div>
          
          <div class="mx-contenedor-elementos_form mx-contenedor-elementos_form_marginBottom">
            <label class="mx-label" for="id-password"><b>Contraseña:</b></label>
            <div class="mx-contenedor-input">
              <input type="password" class="mx-input" id="id-password" placeholder="Contraseña" disabled>
              <a class="mx-button mx-button_square mx-button_turquesa mx-buttons_margin_left mx-button_change_icon_disabled">
                <span>
                  <i class="fas fa-eye"></i>
                </span>
                <span>
                  <i class="fas fa-eye-slash"></i>
                </span>
              </a>
            </div>
            <p class="mx-texto mx-texto-oculto mx-texto-notificacion_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span><b>No debe tener espacios, acentos y "ñ". Y al menos un símbolo como: <i>! " @ # $ % * _</i></b></span>
            </p>
            <p class="mx-texto mx-texto-oculto mx-texto-notificacion_error">
              <span>
                <i class="fas fa-times-circle"></i>
              </span>
              <span><b>Este campo se encuentra vacío.</b></span>
            </p>
          </div>
          
          <a class="mx-button mx-button_info mx-button_largo" href="bienvenido">
            <span></span>
            <span><b>Iniciar sesión</b></span>
          </a>
        </div>
      </div>
    </section>

<?php include 'templates/scripts_principales.php'; ?>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-XXXXX-X', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Built with love using Web Starter Kit -->
  </body>
</html>