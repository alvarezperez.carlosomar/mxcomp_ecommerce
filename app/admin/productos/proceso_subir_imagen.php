<?php
if(isset($_POST['accion']) && $_POST['accion'] === "subir"){
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  $codigoProducto = desencriptar(trim($_POST['codigo_producto']));
  
  $Conexion_MXcomp = new Conexion_mxcomp();

  $proceso_correcto = false;
  $mensaje = "";
  
  // REVISA EL CÓDIGO DEL PRODUCTO
  if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
    $codigoProducto = (string) $codigoProducto;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, nombreMarca, nombreImagen, versionImagen FROM __productos WHERE codigoProducto = :codigoProducto";
      $stmt = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];
      
      if($producto_existe === 1){
        $nombreMarca = $datos_producto['nombreMarca'];
        $nombreImagen = $datos_producto['nombreImagen'];
        $versionImagen = $datos_producto['versionImagen'];
        $proceso_correcto = true;
      }else{
        $respuesta = "1"; // El producto no existe
        $mensaje = '
          <h4 class="mx-seccion-titulo">Editar información del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">No se guardó la imagen, porque no se encontró la información necesaria, vuelve a intentarlo.</span>
          </div>';
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar datos del producto.";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "Hay problemas con el código del producto.";
    $proceso_correcto = false;
  }
  
  // REVISA EL ARCHIVO SUBIDO
  if($proceso_correcto){
    if(is_uploaded_file($_FILES['archivo']['tmp_name'])){
      $archivo_size = $_FILES['archivo']['size'];
      $archivo_formato = pathinfo($_FILES['archivo']['name'])['extension'];
      
      if($archivo_formato === "jpg"){
        if($archivo_size < 5242880){
          $proceso_correcto = true;
        }else{
          $respuesta = "4"; // El tamaño del archivo sobrepasa los 5 MB
          $proceso_correcto = false;
        }
      }else{
        $respuesta = "3"; // El formato no es válido
        $proceso_correcto = false;
      }
    }else{
      $respuesta = "2"; // No se cargó ningun archivo
      $proceso_correcto = false;
    }
  }
  
  // SE REALIZAN LOS PROCESOS CORRESPONDIENTES
  if($proceso_correcto){
    $carpeta_imagen = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca;
    $numeroUbicacionImagen = "1"; // UBICACION NUEVA
    
    if(!file_exists($carpeta_imagen)){
      mkdir($carpeta_imagen, 0777, true);
    }
    
    $ubicacion_nombre_imagen = $carpeta_imagen . '/' . $nombreImagen . '.' . $archivo_formato;
    
    if(move_uploaded_file($_FILES['archivo']['tmp_name'], $ubicacion_nombre_imagen)){
      try{
        $Conexion_MXcomp->pdo->beginTransaction();
        
        $tieneImagen = "1";
        
        if(is_null($versionImagen)){
          $versionImagen = "1";
        }else{
          $versionImagen = (int) $versionImagen + 1;
          $versionImagen = (string) $versionImagen;
        }
        
        $cantidadImagenes = "1";
        $noModificarCampos = "1";

        $sql = "UPDATE __productos SET tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, versionImagen = :versionImagen, cantidadImagenes = :cantidadImagenes, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
        $stmt = $Conexion_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
        $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
        $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
        $stmt->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
        $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
        $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt->execute();

        $respuesta = "6"; // Correcto
        $mensaje = "Se subió la imagen y se modificó la información que corresponde.";
        
        $Conexion_MXcomp->pdo->commit();
        $stmt = null;
      }catch(PDOException $error){
        $Conexion_MXcomp->pdo->rollBack();
        $respuesta = "0";
        //$mensaje = "Error: " . $error->getMessage();
        $mensaje = "Problema al realizar las modificaciones";
        unlink($ubicacion_nombre_imagen);
      }
    }else{
      $respuesta = "5"; // No se cargó ningún archivo
      unlink($ubicacion_nombre_imagen);
    }
  }
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>