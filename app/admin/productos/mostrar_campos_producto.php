<?php
if(isset($_POST['accion']) && $_POST['accion'] === "buscar"){
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  $codigoProducto = trim($_POST['codigo_producto']);

  $Conexion_MXcomp = new Conexion_mxcomp();

  $proceso_correcto = false;
  $mensaje = "";
  $campos_producto = '';
  $campos_imagen = '';
  
  // REVISA EL CODIGO DEL PRODUCTO
  if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
    $codigoProducto = (string) $codigoProducto;
    $proceso_correcto = true;
  }else if($codigoProducto === ""){
    $respuesta = "1"; // El campo se encuentra vacío
    $proceso_correcto = false;
  }else{
    $respuesta = "2"; // El campo no respeta la expresion regular
    $proceso_correcto = false;
  }
  
  // SE REALIZAN LAS CONSULTAS CORRESPONDIENTES
  if($proceso_correcto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, skuProveedor, skuFabricante, descripcion, descripcionURL, nombreMarca, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen, cantidadImagenes FROM __productos WHERE codigoProducto = :codigoProducto";
      $stmt = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];
      
      if($producto_existe === 1){
        $skuProveedor = trim($datos_producto['skuProveedor']);
        $skuFabricante = trim($datos_producto['skuFabricante']);
        $descripcion = trim($datos_producto['descripcion']);
        $descripcionURL = trim($datos_producto['descripcionURL']);
        $nombreMarca = trim($datos_producto['nombreMarca']);
        $tieneImagen = (int) trim($datos_producto['tieneImagen']);
        $numeroUbicacionImagen = trim($datos_producto['numeroUbicacionImagen']);
        $nombreImagen = (string) trim($datos_producto['nombreImagen']);
        $versionImagen = (string) trim($datos_producto['versionImagen']);
        $cantidadImagenes = (int) trim($datos_producto['cantidadImagenes']);
        
        $campos_producto = '
          <h4 class="mx-seccion-titulo">Editar información del producto</h4>
          
          <input type="hidden" id="id-codigo-producto_input" value="' . encriptar($codigoProducto) . '">
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom mx-marginTop_1rem">
            <div class="mx-columna mx-tres_columnas">
              <span class="mx-label"><b>Código del producto:</b></span>
              <span class="mx-label">' . $codigoProducto . '</span>
            </div>
            <div class="mx-columna">
              <span class="mx-label"><b>SKU del proveedor:</b></span>
              <span class="mx-label">' . $skuProveedor . '</span>
            </div>
            <div class="mx-columna">
              <span class="mx-label"><b>Marca del producto:</b></span>
              <span class="mx-label">' . $nombreMarca . '</span>
            </div>
          </div>
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom mx-marginTop_1rem">
            <div class="mx-columna">
              <span class="mx-label"><b>Descripción en URL:</b></span>
              <span class="mx-label">' . $descripcionURL . '</span>
            </div>
          </div>
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom mx-marginTop_1rem">
            <div class="mx-columna">
              <label class="mx-label" for="id-descripcion-producto"><b>Descripción:</b></label>
              <div class="mx-contenedor-input">
                <textarea class="mx-input mx-textarea" id="id-descripcion-producto" placeholder="Descripción" autocomplete="off">' . $descripcion . '</textarea>
              </div>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_info" id="id-descripcion-producto_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span><b>Revisa la descripción, hay símbolos o palabras que no son aceptados.</b></span>
              </p>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_error" id="id-descripcion-producto_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span><b>Este campo se encuentra vacío</b></span>
              </p>
            </div>
          </div>

          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom">
            <div class="mx-columna mx-dos_columnas">
              <label class="mx-label" for="id-sku-fabricante"><b>SKU del fabricante:</b></label>
              <div class="mx-contenedor-input">
                <input type="text" class="mx-input" id="id-sku-fabricante" placeholder="SKU del fabricante" autocomplete="off" value="' . $skuFabricante . '">
              </div>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_info" id="id-sku-fabricante_alert_info">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span><b>Revisa la SKU del fabricante, hay símbolos o palabras que no son aceptados.</b></span>
              </p>
              <p class="mx-texto mx-texto-oculto mx-texto-notificacion_error" id="id-sku-fabricante_alert_error">
                <span>
                  <i class="fas fa-times-circle"></i>
                </span>
                <span><b>Este campo se encuentra vacío</b></span>
              </p>
            </div>
          </div>

          <div class="mx-buttons mx-buttons_right">
            <button class="mx-button mx-button_success" id="id-editar-informacion_submit">
              <span>
                <i class="fas fa-edit"></i>
              </span>
              <span><b>Editar información</b></span>
            </button>
          </div>';
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        if($nombreImagen === ""){
          $campos_imagen = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-marginTop_1rem mx-marginBottom_1rem">
            <label class="mx-label"><b>No se pueden subir las imágenes por que no se tiene la SKU del fabricante, ingrésala por favor.</b></label>
          </div>';
        }else{
          if($tieneImagen === 0){
            $campos_imagen = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-marginTop_1rem mx-marginBottom_1rem">
            <label class="mx-label"><b>Se guardará en:</b></label>
            <label class="mx-label" style="opacity: .8;">
              <span>La carpeta ' . $nombreMarca . ' con el nombre: </span>
              <span style="margin-left: .5rem;"><b>' . $nombreImagen . '</b></span>
            </label>
          </div>
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom">
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="../images/no_imagen_disponible.png" alt="El producto no tiene imagen">
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_5_12">
              <label class="mx-label"><b>Subir imagen (Sólo .jpg):</b></label>
              <div class="mx-contenedor-input" style="display: block;">
                <input type="file" id="id-subir-imagen_file" accept=".jpg" name="archivo" hidden>
                <button type="button" class="mx-button mx-button_info" id="id-subir-imagen_button" title="Seleccionar imagen" style="margin-bottom: 0;">
                  <span>
                    <i class="fas fa-file"></i>
                  </span>
                  <span><b>Seleccionar archivo</b></span>
                </button>
                <label class="mx-texto mx-texto-span_archivo_upload" id="id-subir-imagen_span" style="margin-top: .5rem; height: auto;">
                  <span><b>No se eligió el archivo.</b></span>
                  <span>
                    <i class="fas fa-times-circle"></i>
                  </span>
                </label>
              </div>
            </div>
          </div>
          
          <div class="mx-buttons mx-buttons_right" id="id-contenedor-boton_subir_imagen" style="display: none;"></div>';
          }else{
            $numeroUbicacionImagen = (int) $numeroUbicacionImagen;
            
            $campos_imagen = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom">
            <div class="mx-columna mx-col_5_12">';

            switch($numeroUbicacionImagen){
              case 1: // UBICACION NUEVA
                $campos_imagen .= '
              <label class="mx-label"><b>Carpeta / nombre de la imagen:</b></label>
              <label class="mx-label" style="opacity: .8;">' . $nombreMarca . ' / ' . $nombreImagen . '</label>';
              
                $imagen_ubicacion = '../images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen;
                break;
            }

            $campos_imagen .= '
            </div>

            <div class="mx-columna mx-col_3_12">
              <label class="mx-label"><b>Versión de la imagen:</b></label>
              <label class="mx-label" style="opacity: .8;">' . $versionImagen . '</label>
            </div>';
          
            if($cantidadImagenes > 1){
              $campos_imagen .= '
            <div class="mx-columna mx-col_4_12">
              <button type="button" class="mx-button mx-button_delete" id="id-eliminar-imagenes" title="Eliminar todas las imágenes">
                <span>
                  <i class="fas fa-trash"></i>
                </span>
                <span><b>Eliminar todas las imágenes</b></span>
              </button>
            </div>';
            }
            
            $campos_imagen .= '
          </div>
          
          <div class="mx-columnas mx-contenedor-elementos_form_marginBotom">';
          
            if($cantidadImagenes === 1){
              $numero_imagen = (int) $cantidadImagenes + 1; 
              
              $campos_imagen .= '
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . $cantidadImagenes . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
                
                <div style="margin: auto; width: 100%; text-align: center; margin-top: .5rem;">
                  <button type="button" class="mx-button mx-button_delete g-eliminar-imagen" id="id-eliminar-imagen_' . $cantidadImagenes . '" title="Eliminar imagen">
                    <span>
                      <i class="fas fa-trash"></i>
                    </span>
                    <span><b>Eliminar</b></span>
                  </button>
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_5_12">
              <input type="hidden" id="id-numero-imagen_input" value="' . encriptar($numero_imagen) . '">
              
              <label class="mx-label"><b>Subir imagen ' . $numero_imagen . ' (Sólo .jpg):</b></label>
              <div class="mx-contenedor-input" style="display: block;">
                <input type="file" id="id-subir-imagen_adicional_file" accept=".jpg" name="archivo" hidden>
                <button type="button" class="mx-button mx-button_info" id="id-subir-imagen_adicional_button" title="Seleccionar imagen" style="margin-bottom: 0;">
                  <span>
                    <i class="fas fa-file"></i>
                  </span>
                  <span><b>Seleccionar archivo</b></span>
                </button>
                <label class="mx-texto mx-texto-span_archivo_upload" id="id-subir-imagen_adicional_span" style="margin-top: .5rem; height: auto;">
                  <span><b>No se eligió el archivo.</b></span>
                  <span>
                    <i class="fas fa-times-circle"></i>
                  </span>
                </label>
              </div>
            </div>';
            }
            
            if($cantidadImagenes === 2){
              $numero_imagen = (int) $cantidadImagenes + 1; 
              
              $campos_imagen .= '
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . ($cantidadImagenes - 1) . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . $cantidadImagenes . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '_2.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
                
                <div style="margin: auto; width: 100%; text-align: center; margin-top: .5rem;">
                  <button type="button" class="mx-button mx-button_delete g-eliminar-imagen" id="id-eliminar-imagen_' . $cantidadImagenes . '" title="Eliminar imagen">
                    <span>
                      <i class="fas fa-trash"></i>
                    </span>
                    <span><b>Eliminar</b></span>
                  </button>
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_5_12">
              <input type="hidden" id="id-numero-imagen_input" value="' . encriptar($numero_imagen) . '">
              
              <label class="mx-label"><b>Subir imagen ' . $numero_imagen . ' (Sólo .jpg):</b></label>
              <div class="mx-contenedor-input" style="display: block;">
                <input type="file" id="id-subir-imagen_adicional_file" accept=".jpg" name="archivo" hidden>
                <button type="button" class="mx-button mx-button_info" id="id-subir-imagen_adicional_button" title="Seleccionar imagen" style="margin-bottom: 0;">
                  <span>
                    <i class="fas fa-file"></i>
                  </span>
                  <span><b>Seleccionar archivo</b></span>
                </button>
                <label class="mx-texto mx-texto-span_archivo_upload" id="id-subir-imagen_adicional_span" style="margin-top: .5rem; height: auto;">
                  <span><b>No se eligió el archivo.</b></span>
                  <span>
                    <i class="fas fa-times-circle"></i>
                  </span>
                </label>
              </div>
            </div>';
            }
            
            if($cantidadImagenes === 3){
              $campos_imagen .= '
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . ($cantidadImagenes - 2) . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . ($cantidadImagenes - 1) . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '_2.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
              </div>
            </div>
            
            <div class="mx-columna mx-col_3_12">
              <div style="display: flex; align-items: center; justify-content: center; flex-wrap: wrap;">
                <span class="mx-label" style="width: 100%; justify-content: center;"><b>Imagen ' . $cantidadImagenes . '</b></span>
                
                <div class="contenedor-imagen" style="margin: auto;">
                  <img src="' . $imagen_ubicacion . '_3.jpg?v=' . $versionImagen . '" alt="' . $descripcion . '">
                </div>
                
                <div style="margin: auto; width: 100%; text-align: center; margin-top: .5rem;">
                  <button type="button" class="mx-button mx-button_delete g-eliminar-imagen" id="id-eliminar-imagen_' . $cantidadImagenes . '" title="Eliminar imagen">
                    <span>
                      <i class="fas fa-trash"></i>
                    </span>
                    <span><b>Eliminar</b></span>
                  </button>
                </div>
              </div>
            </div>';
            }
            
            $campos_imagen .= '
          </div>';
          
            if($cantidadImagenes === 1 || $cantidadImagenes === 2){
              $campos_imagen .= '
          <div class="mx-buttons mx-buttons_right" id="id-contenedor-boton_subir_imagen_adicional" style="display: none;"></div>';
            }
          }
        }
        
        $respuesta = "4";
      }else{
        $respuesta = "3"; // El producto no existe
        $campos_producto = '
          <h4 class="mx-seccion-titulo">Editar información del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">El producto buscado no existe.</span>
          </div>';

        $campos_imagen = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">No se puede agregar/editar las imágenes del producto.</span>
          </div>';
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al ejecutar consultas.";
    }
  }
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'campos_producto' => $campos_producto,
    'campos_imagen' => $campos_imagen
  ];
  echo json_encode($json);
}
?>