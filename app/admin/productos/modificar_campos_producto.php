<?php
if(isset($_POST['accion']) && $_POST['accion'] === "modificar"){
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/convertir_url.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  $codigoProducto = desencriptar(trim($_POST['codigo_producto']));
  $Input_descripcion = trim($_POST['descripcion_producto']);
  $Input_skuFabricante = trim($_POST['sku_fabricante']);
  
  $Conexion_MXcomp = new Conexion_mxcomp();

  $proceso_correcto = false;
  $mensaje = "";
  
  // REVISA EL CÓDIGO DEL PRODUCTO
  if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
    $codigoProducto = (string) $codigoProducto;
    $proceso_correcto = true;
  }else{
    $respuesta = "0";
    $mensaje = "Hay problemas con el código del producto.";
    $proceso_correcto = false;
  }
  
  // REVISA LA DESCRIPCION DEL PRODUCTO
  if($proceso_correcto){
    if($Input_descripcion !== "" && !validar_descripcion_producto($Input_descripcion)){
      $Input_descripcion = (string) $Input_descripcion;
      $proceso_correcto = true;
    }else if($Input_descripcion === ""){
      $respuesta = "1"; // El campo se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "2"; // El campo no respeta la expresión regular
      $proceso_correcto = false;
    }
  }
  
  // REVISA LA SKU DEL FABRICANTE
  if($proceso_correcto){
    if($Input_skuFabricante !== "" && !validar_descripcion_producto($Input_skuFabricante)){
      $Input_skuFabricante = (string) $Input_skuFabricante;
      $proceso_correcto = true;
    }else if($Input_skuFabricante === ""){
      $respuesta = "3"; // El campo se encuentra vacío
      $proceso_correcto = false;
    }else{
      $respuesta = "4"; // El campo no respeta la expresión regular
      $proceso_correcto = false;
    }
  }
  
  // SE REALIZAN LAS CONSULTAS CORRESPONDIENTES
  if($proceso_correcto){
    try{
      $Conexion_MXcomp->pdo->beginTransaction();
      
      $sql = "SELECT COUNT(Productos.id) AS conteo, Productos.skuProveedor, Productos.skuFabricante, Productos.nombreMarca, Productos.tieneImagen, Productos.numeroUbicacionImagen, Productos.nombreImagen, Productos.versionImagen, Productos.cantidadImagenes, Categorias.nombre AS nombreCategoria, Subcat_tipos.nombre AS nombreSubcategoriaTipo, Subcat_especificas.nombre AS nombreSubcategoriaEspecifica
      
      FROM __productos Productos
      INNER JOIN __categorias Categorias ON Categorias.id = Productos.categoriaID
      INNER JOIN __subcat_tipos Subcat_tipos ON Subcat_tipos.id = Productos.subcat_tipoID
      INNER JOIN __subcat_especificas Subcat_especificas ON Subcat_especificas.id = Productos.subcat_especificaID
      
      WHERE BINARY Productos.codigoProducto = :codigoProducto";
      $stmt = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];
      
      if($producto_existe === 1){
        $skuProveedor = (string) trim($datos_producto['skuProveedor']);
        $BD_skuFabricante = (string) trim($datos_producto['skuFabricante']);
        $nombreMarca = (string) trim($datos_producto['nombreMarca']);
        $tieneImagen = (int) trim($datos_producto['tieneImagen']);
        $numeroUbicacionImagen = (int) trim($datos_producto['numeroUbicacionImagen']);
        $nombreImagen_BD = (string) trim($datos_producto['nombreImagen']);
        $versionImagen = (int) trim($datos_producto['versionImagen']);
        $cantidadImagenes = (int) trim($datos_producto['cantidadImagenes']);
        $nombreCategoria = (string) trim($datos_producto['nombreCategoria']);
        $nombreSubcategoriaTipo = (string) trim($datos_producto['nombreSubcategoriaTipo']);
        $nombreSubcategoriaEspecifica = (string) trim($datos_producto['nombreSubcategoriaEspecifica']);
        $noModificarCampos = "1";
        
        $Input_descripcion_editada = strpos($Input_descripcion, $nombreMarca) ? $Input_descripcion : $Input_descripcion . ' ' . $nombreMarca;
        $Input_descripcion_editada = strpos($Input_descripcion_editada, $nombreCategoria) ? $Input_descripcion_editada : $Input_descripcion_editada . ' ' . $nombreCategoria;
        $Input_descripcion_editada = strpos($Input_descripcion_editada, $nombreSubcategoriaTipo) ? $Input_descripcion_editada : $Input_descripcion_editada . ' ' . $nombreSubcategoriaTipo;
        $Input_descripcion_editada = strpos($Input_descripcion_editada, $nombreSubcategoriaEspecifica) ? $Input_descripcion_editada : $Input_descripcion_editada . ' ' . $nombreSubcategoriaEspecifica;
        $Input_descripcion_editada = $Input_skuFabricante === "" || $Input_skuFabricante === "0" ? $Input_descripcion_editada : ( strpos($Input_descripcion_editada, $Input_skuFabricante) ? $Input_descripcion_editada : $Input_descripcion_editada . ' ' . $Input_skuFabricante );

        $descripcionURL = convertir_URL($Input_descripcion_editada);
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // LA SKU DEL FABRICANTE ES IGUAL A LA DE LA BASE DE DATOS
        if($Input_skuFabricante === $BD_skuFabricante){
          $sql = "UPDATE __productos SET descripcion = :descripcion, descripcionURL = :descripcionURL, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
          $stmt = $Conexion_MXcomp->pdo->prepare($sql);
          $stmt->bindParam(':descripcion', $Input_descripcion, PDO::PARAM_STR);
          $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
          $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
          $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
          $stmt->execute();
          $filas_afectadas = (int) $stmt->execute();
          
          if($filas_afectadas > 0){
            $proceso_correcto = true;
          }else{
            $respuesta = "5"; // No se realizaron los cambios solicitados.
            $mensaje = '
              <h4 class="mx-seccion-titulo">Imagen del producto</h4>

              <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
                <span>
                  <i class="fas fa-info-circle"></i>
                </span>
                <span class="mx-notificacion-p">No se realizaron los cambios solicitados.</span>
              </div>';
            $proceso_correcto = false;
          }
        }else{
          ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
          // LA SKU DEL FABRICANTE NO ES IGUAL A LA DE LA BASE DE DATOS
          
          // SE GENERA EL NOMBRE DE LA IMAGEN APARTIR DE LA SKU DEL FABRICANTE
          $caracteres_reemplazar = array("\\", "/", ":", "?", "*", '"', "<", ">", "|", "#");

          if($Input_skuFabricante === "" || $Input_skuFabricante === "0"){
            $nombreImagen_generado = NULL;
            $Input_skuFabricante = NULL;
          }else{
            $nombreImagen_generado = str_replace($caracteres_reemplazar, "_", $Input_skuFabricante);
          }
          
          /////////////////////////////////////////////////////////////////////////////////////////
          // SE PREGUNTA SI TIENE IMAGEN
          
          if($tieneImagen === 0){
            $sql = "UPDATE __productos SET skuFabricante = :skuFabricante, descripcion = :descripcion, descripcionURL = :descripcionURL, nombreImagen = :nombreImagen, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
            $stmt = $Conexion_MXcomp->pdo->prepare($sql);
            $stmt->bindParam(':skuFabricante', $Input_skuFabricante, PDO::PARAM_STR);
            $stmt->bindParam(':descripcion', $Input_descripcion, PDO::PARAM_STR);
            $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
            $stmt->bindParam(':nombreImagen', $nombreImagen_generado, PDO::PARAM_STR);
            $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
            $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
            $stmt->execute();
            $filas_afectadas = (int) $stmt->execute();
            
            if($filas_afectadas > 0){
              $proceso_correcto = true;
            }else{
              $respuesta = "5"; // No se realizaron los cambios solicitados.
              $mensaje = '
                <h4 class="mx-seccion-titulo">Imagen del producto</h4>

                <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
                  <span>
                    <i class="fas fa-info-circle"></i>
                  </span>
                  <span class="mx-notificacion-p">No se realizaron los cambios solicitados.</span>
                </div>';
              $proceso_correcto = false;
            }
          }else{
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // NO SE GENERO EL NOMBRE DE LA IMAGEN
            
            if(is_null($nombreImagen_generado)){
              $respuesta = "5"; // No se realizaron los cambios solicitados.
              $mensaje = '
                <h4 class="mx-seccion-titulo">Imagen del producto</h4>

                <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
                  <span>
                    <i class="fas fa-info-circle"></i>
                  </span>
                  <span class="mx-notificacion-p">No se realizaron los cambios solicitados.</span>
                </div>';
              $proceso_correcto = false;
            }else{
            // SE GENERO EL NOMBRE DE LA IMAGEN

              switch($numeroUbicacionImagen){
                case 1: // UBICACION NUEVA
                  $versionImagen = (int) $versionImagen + 1;
                  $versionImagen = (string) $versionImagen;
                  
                  $sql = "UPDATE __productos SET skuFabricante = :skuFabricante, descripcion = :descripcion, descripcionURL = :descripcionURL, nombreImagen = :nombreImagen, versionImagen = :versionImagen, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
                  $stmt = $Conexion_MXcomp->pdo->prepare($sql);
                  $stmt->bindParam(':skuFabricante', $Input_skuFabricante, PDO::PARAM_STR);
                  $stmt->bindParam(':descripcion', $Input_descripcion, PDO::PARAM_STR);
                  $stmt->bindParam(':descripcionURL', $descripcionURL, PDO::PARAM_STR);
                  $stmt->bindParam(':nombreImagen', $nombreImagen_generado, PDO::PARAM_STR);
                  $stmt->bindParam(':versionImagen', $versionImagen, PDO::PARAM_STR);
                  $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
                  $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
                  $stmt->execute();
                  $filas_afectadas = (int) $stmt->execute();
                  
                  if($filas_afectadas > 0){
                    $mensaje_correcto = false;
                    
                    switch($cantidadImagenes){
                      case 1:
                        // SE USA EL NOMBRE DE LA IMAGEN QUE TIENE EN LA BASE DE DATOS
                        $imagen_1_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '.jpg';

                        $imagen_1_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '.jpg';

                        // SE RENOMBRA LA IMAGEN
                        if(rename($imagen_1_actual, $imagen_1_nueva)){ // SE RENOMBRA LA IMAGEN 1
                          $mensaje_correcto = true;
                        }else{
                          $mensaje_correcto = false;
                        }
                        break;

                      case 2:
                        // SE USA EL NOMBRE DE LA IMAGEN QUE TIENE EN LA BASE DE DATOS
                        $imagen_1_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '.jpg';
                        $imagen_2_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '_2.jpg';

                        $imagen_1_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '.jpg';
                        $imagen_2_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '_2.jpg';

                        // SE RENOMBRAN LAS IMAGENES
                        if(rename($imagen_1_actual, $imagen_1_nueva)){ // SE RENOMBRA LA IMAGEN 1
                          if(rename($imagen_2_actual, $imagen_2_nueva)){ // SE RENOMBRA LA IMAGEN 2
                            $mensaje_correcto = true;
                          }else{
                            $mensaje_correcto = false;
                          }
                        }else{
                          $mensaje_correcto = false;
                        }
                        break;

                      case 3:
                        // SE USA EL NOMBRE DE LA IMAGEN QUE TIENE EN LA BASE DE DATOS
                        $imagen_1_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '.jpg';
                        $imagen_2_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '_2.jpg';
                        $imagen_3_actual = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_BD . '_3.jpg';

                        $imagen_1_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '.jpg';
                        $imagen_2_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '_2.jpg';
                        $imagen_3_nueva = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen_generado . '_3.jpg';

                        // SE COPIAN LAS IMAGENES
                        if(rename($imagen_1_actual, $imagen_1_nueva)){ // SE RENOMBRA LA IMAGEN 1
                          if(rename($imagen_2_actual, $imagen_2_nueva)){ // SE RENOMBRA LA IMAGEN 2
                            if(rename($imagen_3_actual, $imagen_3_nueva)){ // SE RENOMBRA LA IMAGEN 3
                              $mensaje_correcto = true;
                            }else{
                              $mensaje_correcto = false;
                            }
                          }else{
                            $mensaje_correcto = false;
                          }
                        }else{
                          $mensaje_correcto = false;
                        }
                        break;
                    }
                    
                    if($mensaje_correcto){
                      $proceso_correcto = true;
                    }else{
                      $respuesta = "5"; // No se realizaron los cambios solicitados.
                      $mensaje = '
                        <h4 class="mx-seccion-titulo">Imagen del producto</h4>

                        <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
                          <span>
                            <i class="fas fa-info-circle"></i>
                          </span>
                          <span class="mx-notificacion-p">No se renombraron las imágenes del producto.</span>
                        </div>';
                      $proceso_correcto = false;
                    }
                  }else{
                    $respuesta = "5"; // No se realizaron los cambios solicitados.
                    $mensaje = '
                      <h4 class="mx-seccion-titulo">Imagen del producto</h4>

                      <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
                        <span>
                          <i class="fas fa-info-circle"></i>
                        </span>
                        <span class="mx-notificacion-p">No se realizaron los cambios solicitados.</span>
                      </div>';
                    $proceso_correcto = false;
                  }
                  break;
              }
            }
          }
        }
      }else{
        $respuesta = "5"; // El producto no existe
        $mensaje = '
          <h4 class="mx-seccion-titulo">Editar información del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">No se actualizó la información, porque no se encontró el producto, vuelve a intentarlo.</span>
          </div>';
        $proceso_correcto = false;
      }

      if($proceso_correcto){
        $respuesta = "6";
        $mensaje = "Proceso completado!!";
      }
      
      $Conexion_MXcomp->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conexion_MXcomp->pdo->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al realizar las modificaciones.";
      $proceso_correcto = false;
    }
  }
  
  // MODIFICA LOS CAMPOS DE LOS PRODUCTOS DE LAS ORDENES DE COMPRA
  /* if($proceso_correcto){
    try{
      $conexion_admin->beginTransaction();
      
      $sql = "SELECT descripcion, descripcionURL, nombreMarca, tieneImagen, numeroUbicacionImagen, nombreImagen, versionImagen FROM __productos WHERE codigoProducto = :codigoProducto";
      $stmt = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      
      $descripcion_BD = (string) $datos_producto['descripcion'];
      $descripcionURL_BD = (string) $datos_producto['descripcionURL'];
      $nombreMarca_BD = (string) $datos_producto['nombreMarca'];
      $tieneImagen_BD = (string) $datos_producto['tieneImagen'];
      $numeroUbicacionImagen_BD = $datos_producto['numeroUbicacionImagen'];
      $nombreImagen_BD = (string) $datos_producto['nombreImagen'];
      $versionImagen_BD = (string) $datos_producto['versionImagen'];
      
      ////////////////////////////////
      // ACTUALIZA LOS DATOS DE LOS PRODUCTOS EN LAS ORDENES DE COMPRA
      
      $sql = "UPDATE __orden_compra_productos SET descripcion = :descripcion, descripcionURL = :descripcionURL, nombreMarca = :nombreMarca, tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, nombreImagen = :nombreImagen, versionImagen = :versionImagen WHERE sku = :sku";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':descripcion', $descripcion_BD, PDO::PARAM_STR);
      $stmt->bindParam(':descripcionURL', $descripcionURL_BD, PDO::PARAM_STR);
      $stmt->bindParam(':nombreMarca', $nombreMarca_BD, PDO::PARAM_STR);
      $stmt->bindParam(':tieneImagen', $tieneImagen_BD, PDO::PARAM_STR);
      $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen_BD, PDO::PARAM_STR);
      $stmt->bindParam(':nombreImagen', $nombreImagen_BD, PDO::PARAM_STR);
      $stmt->bindParam(':versionImagen', $versionImagen_BD, PDO::PARAM_STR);
      $stmt->bindParam(':sku', $skuProveedor, PDO::PARAM_STR);
      $stmt->execute();

      $respuesta = "6";
      $mensaje = "Proceso completado!!";
      
      $conexion_admin->commit();
      $stmt = null;
    }catch(PDOException $error){
      $conexion_admin->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al realizar las modificaciones.";
    }
  } */
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>