<?php
if(isset($_POST['accion']) && $_POST['accion'] === "buscar"){
  include dirname(__DIR__, 2) . '/conn.php';
  
  $id_marca = (int) trim($_POST['id_marca']);

  $Conexion_MXcomp = new Conexion_mxcomp();
  
  $sql = "SELECT codigoProducto, skuProveedor, descripcion, nombreMarca FROM __productos WHERE tieneImagen = 0 AND marcaID = :marcaID ORDER BY skuProveedor ASC";
  $stmt = $Conexion_MXcomp->pdo->prepare($sql);
  $stmt->bindParam(':marcaID', $id_marca, PDO::PARAM_INT);
  $stmt->execute();
  
  $incremento = 1;
  $mensaje = '';
  while($datos = $stmt->fetch(PDO::FETCH_ASSOC)){
    $codigoProducto = trim($datos['codigoProducto']);
    $skuProveedor = trim($datos['skuProveedor']);
    $descripcion = trim($datos['descripcion']);
    $nombreMarca = trim($datos['nombreMarca']);
    
    $mensaje .= $incremento . ": " . $descripcion . "<br>";
    $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>Código producto:</b> " . $codigoProducto . "<br>";
    $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>SKU proveedor:</b> " . $skuProveedor . "<br>";
    $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>Nombre marca:</b> " . $nombreMarca . "<br><br>";
    $incremento++;
  }
  
  if($mensaje === ""){
    $mensaje = "No se encontraron productos sin imágen.";
  }
  
  $respuesta = "1";
  $total = $incremento - 1;
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'total' => $total
  ];
  echo json_encode($json);
}
?>