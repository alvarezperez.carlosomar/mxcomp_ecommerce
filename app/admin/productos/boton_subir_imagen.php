<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  $respuesta = "1";
  $mensaje = '
    <button class="mx-button mx-button_success" id="id-subir-imagen_submit">
      <span>
        <i class="fas fa-file-upload"></i>
      </span>
      <span><b>Subir imagen</b></span>
    </button>';
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>