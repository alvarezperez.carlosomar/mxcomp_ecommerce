<?php
if(isset($_POST['accion']) && $_POST['accion'] === "mostrar"){
  include dirname(__DIR__, 2) . '/conn.php';

  $Conexion_MXcomp = new Conexion_mxcomp();
  
  $mensaje = '
    <label class="mx-label"><b>Nombre de la marca:</b></label>
    <div class="mx-contenedor-input">
      <div class="mx-select id-select">
        <select class="mx-input g-form_select" id="id-select">';
  
  $respuesta = "1";
  
  try{
    $sql = "SELECT DISTINCT nombreMarca, marcaID FROM __productos WHERE tieneImagen = 0 ORDER BY nombreMarca ASC";
    $stmt = $Conexion_MXcomp->pdo->prepare($sql);
    $stmt->execute();
    
    $mensaje .= '
          <option value="">Selecciona la marca</option>';
    while($datos = $stmt->fetch(PDO::FETCH_ASSOC)){
      $nombreMarca = trim($datos['nombreMarca']);
      $marcaID = trim($datos['marcaID']);
      
      $mensaje .= '
          <option value="' . $marcaID . '">' . $nombreMarca . '</option>';
    }
    
    $stmt = null;
  }catch(PDOException $error){
    //$msj_error = "Error: " . $error->getMessage();
    $msj_error = "Problema al buscar las marcas";
    
    $mensaje .= '
          <option value="">' . $msj_error . '</option>';
  }
  
  $mensaje .= '
        </select>
      </div>
    </div>';
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>