<?php
if(isset($_POST['accion']) && $_POST['accion'] === "eliminar"){
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  require_once dirname(__DIR__, 2) . '/funciones/encriptacion.php';
  require_once dirname(__DIR__, 2) . '/conn.php';
  
  $codigoProducto = desencriptar(trim($_POST['codigo_producto']));
  $numero_imagen = trim($_POST['numero_imagen']);

  $Conexion_MXcomp = new Conexion_mxcomp();

  $proceso_correcto = false;
  $mensaje = "";
  
  // REVISA EL CÓDIGO DEL PRODUCTO
  if($codigoProducto !== "" && validar_codigoProducto_caracteres($codigoProducto)){
    $codigoProducto = (string) $codigoProducto;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, nombreMarca, numeroUbicacionImagen, nombreImagen, cantidadImagenes FROM __productos WHERE codigoProducto = :codigoProducto";
      $stmt = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt->execute();
      $datos_producto = $stmt->fetch(PDO::FETCH_ASSOC);
      $producto_existe = (int) $datos_producto['conteo'];
      
      if($producto_existe === 1){
        $nombreMarca = trim($datos_producto['nombreMarca']);
        $numeroUbicacionImagen = (int) trim($datos_producto['numeroUbicacionImagen']);
        $nombreImagen = trim($datos_producto['nombreImagen']);
        $cantidadImagenes = (int) trim($datos_producto['cantidadImagenes']);
        $proceso_correcto = true;
      }else{
        $respuesta = "1"; // El producto no existe
        $mensaje = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">No es posible eliminar las imágenes ya que no se encontró la información del producto, vuelve a intentarlo.</span>
          </div>';
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al buscar datos del producto.";
      $proceso_correcto = false;
    }
  }else{
    $respuesta = "0";
    $mensaje = "Hay problemas con el código del producto.";
    $proceso_correcto = false;
  }
  
  // REVISA EL NUMERO DE IMAGEN
  if($proceso_correcto){
    if($numero_imagen !== "" && validar_campo_numerico($numero_imagen)){
      $numero_imagen = (int) $numero_imagen;
      $proceso_correcto = true;
    }else{
      $respuesta = "0";
      $mensaje = "El número de imagen no es numérico o se encuentra vacío.";
      $proceso_correcto = false;
    }
  }
  
  // SE GENERAN LA UBICACION DE LA IMAGEN Y LOS NOMBRES DE ESTAS
  if($proceso_correcto){
    switch($numeroUbicacionImagen){
      case 1: // UBICACION NUEVA
        $carpeta_imagen = '../../images/imagenes_productos/' . $nombreMarca;
        break;
    }
    
    switch($numero_imagen){
      case 1:
        $imagen = $carpeta_imagen . '/' . $nombreImagen . '.jpg';
        break;

      case 2:
        $imagen = $carpeta_imagen . '/' . $nombreImagen . '_2.jpg';
        break;

      case 3:
        $imagen = $carpeta_imagen . '/' . $nombreImagen . '_3.jpg';
        break;
    }
  }
  
  // MODIFICAR CAMPOS DEL PRODUCTO Y ELIMINAR LAS IMAGENES SI LO ANTERIOR TUVO EXITO
  if($proceso_correcto){
    try{
      $Conexion_MXcomp->pdo->beginTransaction();
      
      $noModificarCampos = "1"; // FIJO PARA AMBOS CASOS
      
      if($numero_imagen === 1){
        $tieneImagen = "0";
        $numeroUbicacionImagen = NULL;
        $cantidadImagenes = NULL;
        
        $sql = "UPDATE __productos SET tieneImagen = :tieneImagen, numeroUbicacionImagen = :numeroUbicacionImagen, cantidadImagenes = :cantidadImagenes, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
        $stmt = $Conexion_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':tieneImagen', $tieneImagen, PDO::PARAM_STR);
        $stmt->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_NULL);
        $stmt->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_NULL);
        $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
        $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt->execute();
        $filas_afectadas = (int) $stmt->rowCount();
      }else{
        $cantidadImagenes = (string) $numero_imagen - 1;
        
        $sql = "UPDATE __productos SET cantidadImagenes = :cantidadImagenes, noModificarCampos = :noModificarCampos WHERE codigoProducto = :codigoProducto";
        $stmt = $Conexion_MXcomp->pdo->prepare($sql);
        $stmt->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
        $stmt->bindParam(':noModificarCampos', $noModificarCampos, PDO::PARAM_STR);
        $stmt->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt->execute();
        $filas_afectadas = (int) $stmt->rowCount();
      }
      
      if($filas_afectadas > 0){
        unlink($imagen); // SE ELIMINA LA IMAGEN
        
        $respuesta = "2"; // CORRECTO
        $mensaje = "Se eliminó la imagen correctamente.";
      }else{
        $respuesta = "1"; // No se realizaron los cambios en la base de datos
        $mensaje = '
          <h4 class="mx-seccion-titulo">Imagen del producto</h4>
          
          <div class="mx-notificacion mx-notificacion-letter mx-notificacion-letter_info mx-marginTop_1rem">
            <span>
              <i class="fas fa-info-circle"></i>
            </span>
            <span class="mx-notificacion-p">No se realizó el cambio solicitado.</span>
          </div>';
      }
      
      $Conexion_MXcomp->pdo->commit();
      $stmt = null;
    }catch(PDOException $error){
      $Conexion_MXcomp->pdo->rollBack();
      $respuesta = "0";
      //$mensaje = "Error: " . $error->getMessage();
      $mensaje = "Problema al realizar las modificaciones.";
    }
  }
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje
  ];
  echo json_encode($json);
}
?>