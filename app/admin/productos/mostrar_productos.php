<?php
if(isset($_POST['accion']) && $_POST['accion'] === "buscar"){
  include dirname(__DIR__, 2) . '/conn.php';

  $Conexion_MXcomp = new Conexion_mxcomp();
  
  $sql = "SELECT codigoProducto, skuProveedor, descripcion, nombreMarca, nombreImagen FROM __productos ORDER BY skuProveedor ASC";
  $stmt = $Conexion_MXcomp->pdo->prepare($sql);
  $stmt->execute();

  $incremento = 1;
  $mensaje = '';
  while($datos = $stmt->fetch(PDO::FETCH_ASSOC)){
    $codigoProducto = trim($datos['codigoProducto']);
    $skuProveedor = trim($datos['skuProveedor']);
    $descripcion = trim($datos['descripcion']);
    $nombreMarca = trim($datos['nombreMarca']);
    $nombreImagen = trim($datos['nombreImagen']);
    
    //PRIMERO SE GENERA LA UBICACIÓN DE LA IMAGEN PARA QUE SEA BUSCADA - TIPO 1
    $imagen = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '.jpg';
    $existe_imagen = false;
    $cantidadImagenes = 0;
    
    if(file_exists($imagen)){
      $existe_imagen = true;
      $numeroUbicacionImagen = "1"; // NUEVA UBICACION
      $cantidadImagenes = "1";
      
      // PREGUNTA SI EXISTE LA IMAGEN 2
      $imagen = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '_2.jpg';
      
      if(file_exists($imagen)){
        $cantidadImagenes = "2";
      }
      
      // PREGUNTA SI EXISTE LA IMAGEN 3
      $imagen = dirname(__DIR__, 2) . '/images/imagenes_productos/' . $nombreMarca . '/' . $nombreImagen . '_3.jpg';
      
      if(file_exists($imagen)){
        $cantidadImagenes = "3";
      }
    }else{
      $existe_imagen = false;
      $numeroUbicacionImagen = NULL; // NO EXISTE UBICACION
    }
    
    if($existe_imagen){
      $sql = "SELECT COUNT(id) as conteo FROM __productos WHERE codigoProducto = :codigoProducto AND versionImagen IS NULL";
      $stmt_temporal = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt_temporal->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt_temporal->execute();
      $no_tiene_version = (int) $stmt_temporal->fetchColumn();
      
      if($no_tiene_version === 1){
        $sql = "UPDATE __productos SET tieneImagen = '1', numeroUbicacionImagen = :numeroUbicacionImagen, versionImagen = '1', cantidadImagenes = :cantidadImagenes WHERE codigoProducto = :codigoProducto";
        $stmt_temporal = $Conexion_MXcomp->pdo->prepare($sql);
        $stmt_temporal->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
        $stmt_temporal->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
        $stmt_temporal->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt_temporal->execute();
      }else{
        $sql = "UPDATE __productos SET tieneImagen = '1', numeroUbicacionImagen = :numeroUbicacionImagen, cantidadImagenes = :cantidadImagenes WHERE codigoProducto = :codigoProducto";
        $stmt_temporal = $Conexion_MXcomp->pdo->prepare($sql);
        $stmt_temporal->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_STR);
        $stmt_temporal->bindParam(':cantidadImagenes', $cantidadImagenes, PDO::PARAM_STR);
        $stmt_temporal->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
        $stmt_temporal->execute();
      }
    }else{
      $sql = "UPDATE __productos SET tieneImagen = '0', numeroUbicacionImagen = :numeroUbicacionImagen WHERE codigoProducto = :codigoProducto";
      $stmt_temporal = $Conexion_MXcomp->pdo->prepare($sql);
      $stmt_temporal->bindParam(':numeroUbicacionImagen', $numeroUbicacionImagen, PDO::PARAM_NULL);
      $stmt_temporal->bindParam(':codigoProducto', $codigoProducto, PDO::PARAM_STR);
      $stmt_temporal->execute();
      
      $mensaje .= $incremento . ": " . $descripcion . "<br>";
      $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>Código producto:</b> " . $codigoProducto . "<br>";
      $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>SKU proveedor:</b> " . $skuProveedor . "<br>";
      $mensaje .= "&nbsp;&nbsp;&nbsp;&nbsp; <b>Nombre marca:</b> " . $nombreMarca . "<br><br>";
      $incremento++;
    }
  }
  
  if($mensaje === ""){
    $mensaje = "No se encontraron productos sin imágen.";
  }
  
  $respuesta = "1";
  $total = $incremento - 1;
  
  $json = [
    'respuesta' => $respuesta,
    'mensaje' => $mensaje,
    'total' => $total
  ];
  echo json_encode($json);
}
?>