<?php
if(isset($_POST['accion']) && $_POST['accion'] === "validar"){
  require_once dirname(__DIR__, 2) . '/funciones/validaciones_campos.php';
  $valor = $_POST['valor'];
  
  if(!validar_descripcion_producto($valor)){
    $respuesta = "1"; // ES VÁLIDO EL CAMPO DE TEXTO
  }else{
    $respuesta = "2"; // NO ES VÁLIDO EL CAMPO DE TEXTO
  }
  
  $json = [
    'respuesta' => $respuesta
  ];
  echo json_encode($json);
}
?>