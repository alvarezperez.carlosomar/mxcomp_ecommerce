<?php
session_start();
if(!isset($_SESSION['__id__']) || !isset($_GET['orden_compra'])){
  echo '<script> history.go(-1); </script>';
}

require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

date_default_timezone_set('America/Mexico_City');  // SE ESTABLECE LA HORA DE MÉXICO

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Estado de la compra</title>
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 0;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-cuenta-fondo">
          <h1 class="p-titulo">ESTADO DE LA COMPRA</h1>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
<?php
if(!isset($_SESSION['__id__'])){
?>
          <p class="p-text_p">
            <b>Para poder comprar es necesario que inicies sesión. En caso de no contar con una cuenta, regístrala dando clic en el botón correspondiente.</b>
          </p>
          <div class="p-buttons">
            <a href="iniciar-sesion" class="p-button p-button_info">
              <span>
                <i class="fas fa-user"></i>
              </span>
              <span><b>Iniciar sesión</b></span>
            </a>
            <a href="registrar-cuenta" class="p-button_inverso p-button_info_inverso">
              <span>
                <i class="fas fa-user-plus"></i>
              </span>
              <span><b>Registrar cuenta</b></span>
            </a>
          </div>
<?php
}else{
  $idUser = desencriptar(trim($_SESSION['__id__']));
  $codigoUsuario = desencriptar(trim($_SESSION['__codigo_usu__']));
  $ordenCompra = trim($_GET['orden_compra']);
  
  $proceso_correcto = false;
  $msg_error = "";
  $bandera_info = false;
  
  // REVISA SI EXISTE EL USUARIO
  if(validar_campo_numerico($idUser)){
    $idUser = (int) $idUser;
    
    try{
      $sql = "SELECT COUNT(id) AS conteo, codigoUsuario FROM __usuarios WHERE id = :id AND codigoUsuario = :codigoUsuario";
      $stmt = $conexion->prepare($sql);
      $stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoUsuario', $codigoUsuario, PDO::PARAM_INT);
      $stmt->execute();
      $datos_usuario = $stmt->fetch(PDO::FETCH_ASSOC);
      $usuario_existe = (int) $datos_usuario['conteo'];
      
      if($usuario_existe === 1){
        $codigoUsuario = (string) $datos_usuario['codigoUsuario'];
        $proceso_correcto = true;
      }else{
        $msg_error = "Este usuario no existe.";
        $bandera_info = true;
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$msg_error = "Error: " . $error->getMessage();
      $msg_error = "Hubo un problema al buscar al usuario.";
      $bandera_info = true;
      $proceso_correcto = false;
    }
  }else{
    $msg_error = "No es numérica la clave del usuario.";
    $bandera_info = true;
    $proceso_correcto = false;
  }
  
  // REVISA SI LA ORDEN DE COMPRA ES VÁLIDA
  if($proceso_correcto){
    if(validar_campo_numerico($ordenCompra)){
      $ordenCompra = (int) $ordenCompra;
      $proceso_correcto = true;
    }else{
      $msg_error = "No es numérica la orden de compra.";
      $bandera_info = false;
      $proceso_correcto = false;
    }
  }
  
  // REVISA SI EXISTEN LOS DATOS DE COMPRAS
  if($proceso_correcto){
    try{
      $sql = "SELECT COUNT(id) AS conteo, id, ordenCompra, necesitaFactura FROM __ordenes_compra WHERE clienteID = :clienteID AND codigoCliente = :codigoCliente AND ordenCompra = :ordenCompra";
      $stmt = $conexion_admin->prepare($sql);
      $stmt->bindParam(':clienteID', $idUser, PDO::PARAM_INT);
      $stmt->bindParam(':codigoCliente', $codigoUsuario, PDO::PARAM_STR);
      $stmt->bindParam(':ordenCompra', $ordenCompra, PDO::PARAM_STR);
      $stmt->execute();
      $datos_ordenCompra = $stmt->fetch(PDO::FETCH_ASSOC);
      $orden_compra_existe = (int) $datos_ordenCompra['conteo'];

      if($orden_compra_existe === 1){
        $id = (int) trim($datos_ordenCompra['id']);
        $ordenCompra = (string) trim($datos_ordenCompra['ordenCompra']);
        $necesitaFactura = $datos_ordenCompra['necesitaFactura'];

        $proceso_correcto = true;
      }else{
        $msg_error = "La orden de compra no existe. Si este mensaje vuelve a aparecer ponte en contacto con atención a clientes.";
        $bandera_info = true;
        $proceso_correcto = false;
      }
      
      $stmt = null;
    }catch(PDOException $error){
      //$msg_error = "Error: " . $error->getMessage();
      $msg_error = "Ocurrió un problema al tratar de obtener la información de tu compra. Si este mensaje vuelve a aparecer ponte en contacto con atención a clientes.";
      $bandera_info = true;
      $proceso_correcto = false;
    }
  }
  
  // IMPRIMIR LOS MENSAJES ADECUADOS
  if($proceso_correcto){
?>
          <input type="hidden" value="<?php echo encriptar($ordenCompra); ?>" id="id-comprar-OC">
          <input type="hidden" value="<?php echo encriptar("Tarjeta"); ?>" id="id-comprar-metodoPago">
          
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #3EBA50; font-size: 3rem;">
              <i class="fas fa-check-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem;">¡Compra finalizada! Tu pago fue acreditado.</h3>
          </div>
          
          <div class="p-buttons p-buttons_center">
            <a href="compra-detalles/<?php echo $id . '/' . $ordenCompra; ?>" class="p-button p-button_account p-button_largo">
              <span>
                <i class="far fa-file-alt"></i>
              </span>
              <span><b>Ver detalles de la compra</b></span>
            </a>
          </div>
<?php
    if(is_null($necesitaFactura)){
?>
          <div id="id-contenedor-facturar" style="margin-top: 3rem;">
            <p class="p-text p-text_p">¿Necesitas facturar esta compra?</p>

            <div class="p-buttons p-cuenta-contenedor_botones">
              <button class="p-button p-button_info" id="id-si-facturar">
                <span><b>Sí, necesito facturarla</b></span>
              </button>

              <button class="p-button_inverso p-button_info_inverso" id="id-no-facturar">
                <span><b>No, gracias</b></span>
              </button>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-notificacion-no_facturar" style="margin-top: 3rem;">
            <div class="p-notification p-notification_letter_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <span class="p-notification_p"><b>Tu respuesta fue recibida, no se preguntará de nuevo para esta compra.</b></span>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-notificacion-si_facturar" style="margin-top: 3rem;">
            <div class="p-notification p-notification_letter_success">
              <span>
                <i class="fas fa-check-circle"></i>
              </span>
              <span class="p-notification_p"><b>Se solicitó la facturación de tu compra. Cuando se tenga lista tu factura, te llegará por el correo que nos facilitaste en el formulario.</b></span>
            </div>
          </div>
          
          <div class="p-notification_contenedor" id="id-notificacion-correo_noEnviado" style="margin-top: 1rem;">
            <div class="p-notification p-notification_letter_info">
              <span>
                <i class="fas fa-info-circle"></i>
              </span>
              <span class="p-notification_p"><b>La información se guardó correctamente, pero no se pudo enviar el correo que es para nosotros. Vuelve a realizar el proceso.</b></span>
            </div>
          </div>
          
          <div class="p-modal-derecho_contenedor" id="id-modal-faturacion">
            <div class="p-modal-derecho_contenedor_fondo g-modal-faturacion_cerrar"></div>
            <div class="p-modal-derecho_contenedor_contenido" id="id-modal-faturacion_contenido">
              <div class="p-loading-general" id="id-loading-facturacion" style="margin-top: 3rem;">
                <div></div>
              </div>
              <div class="p-comprar-modal_cont" id="id-modal-faturacion_contenedor_elementos"></div>
            </div>
          </div>
<?php
    }
  }else{
    if($bandera_info){
?>
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #254F95; font-size: 3rem;">
              <i class="fas fa-info-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem; color: #254F95 !important;">¡Upps! Hay un problema...</h3>
            <p class="p-notification p-notification_letter_info" style="width: 100%;"><b><?php echo $msg_error; ?></b></p>
          </div>
<?php
    }else{
?>
          <div style="display: flex; justify-content: center; flex-wrap: wrap;">
            <span style="color: #A11B2C; font-size: 3rem;">
              <i class="fas fa-times-circle"></i>
            </span>
            <h3 class="p-text_help" style="width: 100%; text-align: center; margin-top: 0rem; margin-bottom: 2rem; font-size: 1.2rem; color: #A11B2C !important;">¡Upps! Hay un error...</h3>
            <p class="p-notification p-notification_letter_error" style="width: 100%;"><b><?php echo $msg_error; ?></b></p>
          </div>
<?php
    }
  }
}
?>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

<?php include 'templates/scripts_facturacion.php'; ?>

<?php include 'templates/scripts_seleccionarFacturacion.php'; ?>

  </body>
</html>