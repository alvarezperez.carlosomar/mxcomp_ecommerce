<?php
session_start();
require_once 'funciones/validaciones_campos.php'; // NECESARIO PARA NAVBAR
require_once 'funciones/encriptacion.php'; // NECESARIO PARA NAVBAR
require_once 'global/config.php'; // NECESARIO PARA NAVBAR Y HEAD
require_once 'conn.php'; // NECESARIO PARA NAVBAR

// PRODUCTO - SI EXISTEN LAS UNIDADES PARA EL PRODUCTO, SE ELIMINAN
if(isset($_SESSION['__producto_unidades__'])){
  unset($_SESSION['__producto_unidades__']);
}

// INDEX - SI EXISTE EL ARRAY DE PRODUCTOS, SE ELIMINA
if(isset($_SESSION['__array_productos__'])){
  unset($_SESSION['__array_productos__']);
}

// INDEX - SI EXISTE LA VARIABLE DE RECARGA DE LA PAGINA, SE ELIMINA
if(isset($_SESSION['__contador_recarga__'])){
  unset($_SESSION['__contador_recarga__']);
}
?>
<!DOCTYPE html>
<html lang="es-MX">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Marcas</title>
    <meta name="description" content="Busca el producto que necesitas en las diferentes marcas que manejamos.">
    <meta name="keywords" content="tecnología, software, hardware, mxcomp, mexicomp, ventas en linea, ecommerce, marcas, notificación">
<?php include 'templates/head.php'; ?>

  </head>
  <body>
<?php
$navbar = "1";
$op_navbar = 4;
$op_menu = 0;

include 'templates/navbar.php';
?>

    <section class="p-section-columns">
      <div class="p-menulateral-contenedor">
<?php include 'templates/menu_lateral.php'; ?>

      </div>
      <div class="p-contenido-contenedor">
        <div class="p-section-div_formularios p-contenido-hero p-titulo_fondo p-busqueda-fondo">
          <h1 class="p-titulo">Marcas</h1>
          <h2 class="p-subtitulo p-text_p">Selecciona la marca de la cual quieres ver productos.</h2>
        </div>
        
        <div class="p-section-div_formularios p-contenido-hero">
          <div class="p-box-options_contenedor">
<?php
$Conn_mxcomp = new Conexion_mxcomp();

try{
  $sql = "SELECT id, nombre, activo FROM __marcas ORDER BY nombre ASC";
  $stmt = $Conn_mxcomp->pdo->prepare($sql);
  $stmt->execute();
  while($datos_marcas = $stmt->fetch(PDO::FETCH_ASSOC)){
    if((int) $datos_marcas['activo'] === 1){
      $nombre_imagen = str_replace(" ", "_", trim($datos_marcas['nombre']));

      $imagen = 'images/logos_empresas/' . $nombre_imagen . '.jpg';
      $texto_nombre = "";
      $alt = trim($datos_marcas['nombre']);

      if(trim($datos_marcas['nombre']) === "MERCURY"){
        $imagen = 'images/logos_empresas/MERCUSYS.jpg';
        $alt = 'MERCUSYS';
      }

      if(!file_exists($imagen)){
        $nombre_dividido = explode("_", $nombre_imagen);
        $imagen = 'images/logos_empresas/' . $nombre_dividido[0] . '.jpg';

        if($nombre_dividido[0] === "HP"){
          $nombre_imagen = str_replace("HP ", "", trim($datos_marcas['nombre']));
          $imagen = 'images/logos_empresas/' . $nombre_dividido[0] . '_2.jpg';
          $texto_nombre = $nombre_imagen;
        }

        if($nombre_dividido[0] === "MICROSOFT"){
          $nombre_imagen = str_replace("MICROSOFT ", "", trim($datos_marcas['nombre']));
          $texto_nombre = $nombre_imagen;
        }

        if(!file_exists($imagen)){
          $imagen = "";
          $texto_nombre = trim($datos_marcas['nombre']) . ' (Sin logotipo)';
        }
      }
?>
            <a title="Marca: <?php echo $alt; ?>" class="g-marcas-opciones" data-id="<?php echo trim($datos_marcas['id']); ?>">
              <div class="p-box-options_opcion">
<?php
      if($imagen !== ""){
?>
                <img src="<?php echo $imagen; ?>" alt="Marca: <?php echo $alt; ?>">
<?php
      }
      if($texto_nombre !== ""){
?>
                <span><?php echo $texto_nombre; ?></span>
<?php
      }
?>
              </div>
            </a>
<?php
    }
  }
  
  $stmt = null;
}catch(PDOException $e){
  echo '<p class="p-text_p"><b>Error en la consulta de marcas</b></p>';
}
?>
          </div>
        </div>
      </div>
    </section>
<?php include 'templates/footer_pag.php'; ?>

<?php include 'templates/footer_scripts_jquery.php'; ?>

<?php include 'templates/footer_scripts_principales.php'; ?>

  </body>  
</html>